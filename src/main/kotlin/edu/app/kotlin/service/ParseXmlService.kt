package edu.app.kotlin.service

import edu.app.kotlin.util.XMLUtils
import org.opentravel.ota._2003._05.AcceptablePaymentCardsInfoType

class ParseXmlService {

    fun parseJAXB(xml: String): AcceptablePaymentCardsInfoType = XMLUtils.unmarshal<AcceptablePaymentCardsInfoType>(xml)


}
