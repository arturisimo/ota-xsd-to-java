
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_BankAccountType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_BankAccountType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Checking"/&amp;gt;
 *     &amp;lt;enumeration value="Investment"/&amp;gt;
 *     &amp;lt;enumeration value="Savings"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_BankAccountType_Base")
@XmlEnum
public enum ListBankAccountTypeBase {

    @XmlEnumValue("Checking")
    CHECKING("Checking"),
    @XmlEnumValue("Investment")
    INVESTMENT("Investment"),
    @XmlEnumValue("Savings")
    SAVINGS("Savings"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListBankAccountTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListBankAccountTypeBase fromValue(String v) {
        for (ListBankAccountTypeBase c: ListBankAccountTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
