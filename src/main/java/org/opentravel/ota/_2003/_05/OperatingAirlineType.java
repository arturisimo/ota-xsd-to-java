
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Identifies the operating carrier and flight number.
 * 
 * &lt;p&gt;Clase Java para OperatingAirlineType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OperatingAirlineType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
 *       &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *       &amp;lt;attribute name="MarketingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="BookedRBD" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *       &amp;lt;attribute name="TicketedRBD" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *       &amp;lt;attribute name="FrequentFlierMiles" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to5" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperatingAirlineType")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.GroundLocationType.Airline.class
})
public class OperatingAirlineType
    extends CompanyNameType
{

    @XmlAttribute(name = "FlightNumber")
    protected String flightNumber;
    @XmlAttribute(name = "ResBookDesigCode")
    protected String resBookDesigCode;
    @XmlAttribute(name = "MarketingInd")
    protected Boolean marketingInd;
    @XmlAttribute(name = "BookedRBD")
    protected String bookedRBD;
    @XmlAttribute(name = "TicketedRBD")
    protected String ticketedRBD;
    @XmlAttribute(name = "FrequentFlierMiles")
    protected String frequentFlierMiles;

    /**
     * Obtiene el valor de la propiedad flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad resBookDesigCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResBookDesigCode() {
        return resBookDesigCode;
    }

    /**
     * Define el valor de la propiedad resBookDesigCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResBookDesigCode(String value) {
        this.resBookDesigCode = value;
    }

    /**
     * Obtiene el valor de la propiedad marketingInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingInd() {
        return marketingInd;
    }

    /**
     * Define el valor de la propiedad marketingInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingInd(Boolean value) {
        this.marketingInd = value;
    }

    /**
     * Obtiene el valor de la propiedad bookedRBD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookedRBD() {
        return bookedRBD;
    }

    /**
     * Define el valor de la propiedad bookedRBD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookedRBD(String value) {
        this.bookedRBD = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketedRBD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketedRBD() {
        return ticketedRBD;
    }

    /**
     * Define el valor de la propiedad ticketedRBD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketedRBD(String value) {
        this.ticketedRBD = value;
    }

    /**
     * Obtiene el valor de la propiedad frequentFlierMiles.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequentFlierMiles() {
        return frequentFlierMiles;
    }

    /**
     * Define el valor de la propiedad frequentFlierMiles.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequentFlierMiles(String value) {
        this.frequentFlierMiles = value;
    }

}
