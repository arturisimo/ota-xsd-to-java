
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Type of accommodation including classes of Seat, berth, compartment on a train with seat map detail. 
 * 
 * &lt;p&gt;Clase Java para AccommodationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccommodationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;choice minOccurs="0"&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;element name="Seat" type="{http://www.opentravel.org/OTA/2003/05}SeatAccommodationType"/&amp;gt;
 *             &amp;lt;element name="SeatAvailabilityDetail" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="Car" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Compartment" maxOccurs="unbounded"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="Seat" maxOccurs="unbounded"&amp;gt;
 *                                             &amp;lt;complexType&amp;gt;
 *                                               &amp;lt;complexContent&amp;gt;
 *                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                   &amp;lt;sequence&amp;gt;
 *                                                     &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;/sequence&amp;gt;
 *                                                   &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                                                   &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}SeatPositionType" /&amp;gt;
 *                                                   &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
 *                                                   &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;/restriction&amp;gt;
 *                                               &amp;lt;/complexContent&amp;gt;
 *                                             &amp;lt;/complexType&amp;gt;
 *                                           &amp;lt;/element&amp;gt;
 *                                           &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                         &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                                         &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;element name="Berth" type="{http://www.opentravel.org/OTA/2003/05}BerthAccommodationType"/&amp;gt;
 *             &amp;lt;element name="BerthAvailabilityDetail" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="Car" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Compartment" maxOccurs="unbounded"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="Berth" maxOccurs="unbounded"&amp;gt;
 *                                             &amp;lt;complexType&amp;gt;
 *                                               &amp;lt;complexContent&amp;gt;
 *                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                   &amp;lt;sequence&amp;gt;
 *                                                     &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;/sequence&amp;gt;
 *                                                   &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                                                   &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}BerthPositionType" /&amp;gt;
 *                                                   &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;/restriction&amp;gt;
 *                                               &amp;lt;/complexContent&amp;gt;
 *                                             &amp;lt;/complexType&amp;gt;
 *                                           &amp;lt;/element&amp;gt;
 *                                           &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                         &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                                         &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *         &amp;lt;element name="Class" type="{http://www.opentravel.org/OTA/2003/05}AccommodationClass" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Compartment" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompartmentType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccommodationType", propOrder = {
    "seat",
    "seatAvailabilityDetail",
    "berth",
    "berthAvailabilityDetail",
    "clazz",
    "compartment"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.AccommodationCategoryType.Accommodation.class,
    org.opentravel.ota._2003._05.OTARailScheduleRQ.RailScheduleQuery.RailPrefs.Accommodation.class,
    org.opentravel.ota._2003._05.OTARailShopRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.AvailabilityDetail.Accommodation.class
})
public class AccommodationType {

    @XmlElement(name = "Seat")
    @XmlSchemaType(name = "string")
    protected SeatAccommodationType seat;
    @XmlElement(name = "SeatAvailabilityDetail")
    protected AccommodationType.SeatAvailabilityDetail seatAvailabilityDetail;
    @XmlElement(name = "Berth")
    @XmlSchemaType(name = "string")
    protected BerthAccommodationType berth;
    @XmlElement(name = "BerthAvailabilityDetail")
    protected AccommodationType.BerthAvailabilityDetail berthAvailabilityDetail;
    @XmlElement(name = "Class")
    protected AccommodationClass clazz;
    @XmlElement(name = "Compartment")
    protected AccommodationType.Compartment compartment;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger quantity;

    /**
     * Obtiene el valor de la propiedad seat.
     * 
     * @return
     *     possible object is
     *     {@link SeatAccommodationType }
     *     
     */
    public SeatAccommodationType getSeat() {
        return seat;
    }

    /**
     * Define el valor de la propiedad seat.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatAccommodationType }
     *     
     */
    public void setSeat(SeatAccommodationType value) {
        this.seat = value;
    }

    /**
     * Obtiene el valor de la propiedad seatAvailabilityDetail.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationType.SeatAvailabilityDetail }
     *     
     */
    public AccommodationType.SeatAvailabilityDetail getSeatAvailabilityDetail() {
        return seatAvailabilityDetail;
    }

    /**
     * Define el valor de la propiedad seatAvailabilityDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationType.SeatAvailabilityDetail }
     *     
     */
    public void setSeatAvailabilityDetail(AccommodationType.SeatAvailabilityDetail value) {
        this.seatAvailabilityDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad berth.
     * 
     * @return
     *     possible object is
     *     {@link BerthAccommodationType }
     *     
     */
    public BerthAccommodationType getBerth() {
        return berth;
    }

    /**
     * Define el valor de la propiedad berth.
     * 
     * @param value
     *     allowed object is
     *     {@link BerthAccommodationType }
     *     
     */
    public void setBerth(BerthAccommodationType value) {
        this.berth = value;
    }

    /**
     * Obtiene el valor de la propiedad berthAvailabilityDetail.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationType.BerthAvailabilityDetail }
     *     
     */
    public AccommodationType.BerthAvailabilityDetail getBerthAvailabilityDetail() {
        return berthAvailabilityDetail;
    }

    /**
     * Define el valor de la propiedad berthAvailabilityDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationType.BerthAvailabilityDetail }
     *     
     */
    public void setBerthAvailabilityDetail(AccommodationType.BerthAvailabilityDetail value) {
        this.berthAvailabilityDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad clazz.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationClass }
     *     
     */
    public AccommodationClass getClazz() {
        return clazz;
    }

    /**
     * Define el valor de la propiedad clazz.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationClass }
     *     
     */
    public void setClazz(AccommodationClass value) {
        this.clazz = value;
    }

    /**
     * Obtiene el valor de la propiedad compartment.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationType.Compartment }
     *     
     */
    public AccommodationType.Compartment getCompartment() {
        return compartment;
    }

    /**
     * Define el valor de la propiedad compartment.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationType.Compartment }
     *     
     */
    public void setCompartment(AccommodationType.Compartment value) {
        this.compartment = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Car" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Compartment" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Berth" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                                     &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}BerthPositionType" /&amp;gt;
     *                                     &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                           &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "car"
    })
    public static class BerthAvailabilityDetail {

        @XmlElement(name = "Car")
        protected List<AccommodationType.BerthAvailabilityDetail.Car> car;

        /**
         * Gets the value of the car property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the car property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCar().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AccommodationType.BerthAvailabilityDetail.Car }
         * 
         * 
         */
        public List<AccommodationType.BerthAvailabilityDetail.Car> getCar() {
            if (car == null) {
                car = new ArrayList<AccommodationType.BerthAvailabilityDetail.Car>();
            }
            return this.car;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Compartment" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Berth" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *                           &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}BerthPositionType" /&amp;gt;
         *                           &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *                 &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "compartment",
            "tpaExtensions"
        })
        public static class Car {

            @XmlElement(name = "Compartment", required = true)
            protected List<AccommodationType.BerthAvailabilityDetail.Car.Compartment> compartment;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;
            @XmlAttribute(name = "Number", required = true)
            protected String number;

            /**
             * Gets the value of the compartment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the compartment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCompartment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AccommodationType.BerthAvailabilityDetail.Car.Compartment }
             * 
             * 
             */
            public List<AccommodationType.BerthAvailabilityDetail.Car.Compartment> getCompartment() {
                if (compartment == null) {
                    compartment = new ArrayList<AccommodationType.BerthAvailabilityDetail.Car.Compartment>();
                }
                return this.compartment;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }

            /**
             * Obtiene el valor de la propiedad number.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumber() {
                return number;
            }

            /**
             * Define el valor de la propiedad number.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumber(String value) {
                this.number = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Berth" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
             *                 &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}BerthPositionType" /&amp;gt;
             *                 &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
             *       &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "berth",
                "tpaExtensions"
            })
            public static class Compartment {

                @XmlElement(name = "Berth", required = true)
                protected List<AccommodationType.BerthAvailabilityDetail.Car.Compartment.Berth> berth;
                @XmlElement(name = "TPA_Extensions")
                protected TPAExtensionsType tpaExtensions;
                @XmlAttribute(name = "Number", required = true)
                protected String number;
                @XmlAttribute(name = "Position")
                protected CompartmentPositionType position;

                /**
                 * Gets the value of the berth property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the berth property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getBerth().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link AccommodationType.BerthAvailabilityDetail.Car.Compartment.Berth }
                 * 
                 * 
                 */
                public List<AccommodationType.BerthAvailabilityDetail.Car.Compartment.Berth> getBerth() {
                    if (berth == null) {
                        berth = new ArrayList<AccommodationType.BerthAvailabilityDetail.Car.Compartment.Berth>();
                    }
                    return this.berth;
                }

                /**
                 * Obtiene el valor de la propiedad tpaExtensions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public TPAExtensionsType getTPAExtensions() {
                    return tpaExtensions;
                }

                /**
                 * Define el valor de la propiedad tpaExtensions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public void setTPAExtensions(TPAExtensionsType value) {
                    this.tpaExtensions = value;
                }

                /**
                 * Obtiene el valor de la propiedad number.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumber() {
                    return number;
                }

                /**
                 * Define el valor de la propiedad number.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumber(String value) {
                    this.number = value;
                }

                /**
                 * Obtiene el valor de la propiedad position.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompartmentPositionType }
                 *     
                 */
                public CompartmentPositionType getPosition() {
                    return position;
                }

                /**
                 * Define el valor de la propiedad position.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompartmentPositionType }
                 *     
                 */
                public void setPosition(CompartmentPositionType value) {
                    this.position = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
                 *       &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}BerthPositionType" /&amp;gt;
                 *       &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "tpaExtensions"
                })
                public static class Berth {

                    @XmlElement(name = "TPA_Extensions")
                    protected TPAExtensionsType tpaExtensions;
                    @XmlAttribute(name = "Number", required = true)
                    protected String number;
                    @XmlAttribute(name = "Position")
                    protected BerthPositionType position;
                    @XmlAttribute(name = "AvailableInd", required = true)
                    protected boolean availableInd;

                    /**
                     * Obtiene el valor de la propiedad tpaExtensions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public TPAExtensionsType getTPAExtensions() {
                        return tpaExtensions;
                    }

                    /**
                     * Define el valor de la propiedad tpaExtensions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public void setTPAExtensions(TPAExtensionsType value) {
                        this.tpaExtensions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad number.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNumber() {
                        return number;
                    }

                    /**
                     * Define el valor de la propiedad number.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNumber(String value) {
                        this.number = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad position.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BerthPositionType }
                     *     
                     */
                    public BerthPositionType getPosition() {
                        return position;
                    }

                    /**
                     * Define el valor de la propiedad position.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BerthPositionType }
                     *     
                     */
                    public void setPosition(BerthPositionType value) {
                        this.position = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad availableInd.
                     * 
                     */
                    public boolean isAvailableInd() {
                        return availableInd;
                    }

                    /**
                     * Define el valor de la propiedad availableInd.
                     * 
                     */
                    public void setAvailableInd(boolean value) {
                        this.availableInd = value;
                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompartmentType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Compartment
        extends CompartmentType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Car" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Compartment" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Seat" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                                     &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}SeatPositionType" /&amp;gt;
     *                                     &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
     *                                     &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                           &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "car"
    })
    public static class SeatAvailabilityDetail {

        @XmlElement(name = "Car")
        protected List<AccommodationType.SeatAvailabilityDetail.Car> car;

        /**
         * Gets the value of the car property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the car property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCar().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AccommodationType.SeatAvailabilityDetail.Car }
         * 
         * 
         */
        public List<AccommodationType.SeatAvailabilityDetail.Car> getCar() {
            if (car == null) {
                car = new ArrayList<AccommodationType.SeatAvailabilityDetail.Car>();
            }
            return this.car;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Compartment" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Seat" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *                           &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}SeatPositionType" /&amp;gt;
         *                           &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
         *                           &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *                 &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "compartment",
            "tpaExtensions"
        })
        public static class Car {

            @XmlElement(name = "Compartment", required = true)
            protected List<AccommodationType.SeatAvailabilityDetail.Car.Compartment> compartment;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;
            @XmlAttribute(name = "Number", required = true)
            protected String number;

            /**
             * Gets the value of the compartment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the compartment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCompartment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AccommodationType.SeatAvailabilityDetail.Car.Compartment }
             * 
             * 
             */
            public List<AccommodationType.SeatAvailabilityDetail.Car.Compartment> getCompartment() {
                if (compartment == null) {
                    compartment = new ArrayList<AccommodationType.SeatAvailabilityDetail.Car.Compartment>();
                }
                return this.compartment;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }

            /**
             * Obtiene el valor de la propiedad number.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumber() {
                return number;
            }

            /**
             * Define el valor de la propiedad number.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumber(String value) {
                this.number = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Seat" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
             *                 &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}SeatPositionType" /&amp;gt;
             *                 &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
             *                 &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
             *       &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "seat",
                "tpaExtensions"
            })
            public static class Compartment {

                @XmlElement(name = "Seat", required = true)
                protected List<AccommodationType.SeatAvailabilityDetail.Car.Compartment.Seat> seat;
                @XmlElement(name = "TPA_Extensions")
                protected TPAExtensionsType tpaExtensions;
                @XmlAttribute(name = "Number", required = true)
                protected String number;
                @XmlAttribute(name = "Position")
                protected CompartmentPositionType position;

                /**
                 * Gets the value of the seat property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the seat property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getSeat().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link AccommodationType.SeatAvailabilityDetail.Car.Compartment.Seat }
                 * 
                 * 
                 */
                public List<AccommodationType.SeatAvailabilityDetail.Car.Compartment.Seat> getSeat() {
                    if (seat == null) {
                        seat = new ArrayList<AccommodationType.SeatAvailabilityDetail.Car.Compartment.Seat>();
                    }
                    return this.seat;
                }

                /**
                 * Obtiene el valor de la propiedad tpaExtensions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public TPAExtensionsType getTPAExtensions() {
                    return tpaExtensions;
                }

                /**
                 * Define el valor de la propiedad tpaExtensions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public void setTPAExtensions(TPAExtensionsType value) {
                    this.tpaExtensions = value;
                }

                /**
                 * Obtiene el valor de la propiedad number.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumber() {
                    return number;
                }

                /**
                 * Define el valor de la propiedad number.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumber(String value) {
                    this.number = value;
                }

                /**
                 * Obtiene el valor de la propiedad position.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompartmentPositionType }
                 *     
                 */
                public CompartmentPositionType getPosition() {
                    return position;
                }

                /**
                 * Define el valor de la propiedad position.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompartmentPositionType }
                 *     
                 */
                public void setPosition(CompartmentPositionType value) {
                    this.position = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
                 *       &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}SeatPositionType" /&amp;gt;
                 *       &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
                 *       &amp;lt;attribute name="AvailableInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "tpaExtensions"
                })
                public static class Seat {

                    @XmlElement(name = "TPA_Extensions")
                    protected TPAExtensionsType tpaExtensions;
                    @XmlAttribute(name = "Number", required = true)
                    protected String number;
                    @XmlAttribute(name = "Position")
                    protected SeatPositionType position;
                    @XmlAttribute(name = "Direction")
                    protected SeatDirectionType direction;
                    @XmlAttribute(name = "AvailableInd", required = true)
                    protected boolean availableInd;

                    /**
                     * Obtiene el valor de la propiedad tpaExtensions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public TPAExtensionsType getTPAExtensions() {
                        return tpaExtensions;
                    }

                    /**
                     * Define el valor de la propiedad tpaExtensions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public void setTPAExtensions(TPAExtensionsType value) {
                        this.tpaExtensions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad number.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNumber() {
                        return number;
                    }

                    /**
                     * Define el valor de la propiedad number.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNumber(String value) {
                        this.number = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad position.
                     * 
                     * @return
                     *     possible object is
                     *     {@link SeatPositionType }
                     *     
                     */
                    public SeatPositionType getPosition() {
                        return position;
                    }

                    /**
                     * Define el valor de la propiedad position.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link SeatPositionType }
                     *     
                     */
                    public void setPosition(SeatPositionType value) {
                        this.position = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad direction.
                     * 
                     * @return
                     *     possible object is
                     *     {@link SeatDirectionType }
                     *     
                     */
                    public SeatDirectionType getDirection() {
                        return direction;
                    }

                    /**
                     * Define el valor de la propiedad direction.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link SeatDirectionType }
                     *     
                     */
                    public void setDirection(SeatDirectionType value) {
                        this.direction = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad availableInd.
                     * 
                     */
                    public boolean isAvailableInd() {
                        return availableInd;
                    }

                    /**
                     * Define el valor de la propiedad availableInd.
                     * 
                     */
                    public void setAvailableInd(boolean value) {
                        this.availableInd = value;
                    }

                }

            }

        }

    }

}
