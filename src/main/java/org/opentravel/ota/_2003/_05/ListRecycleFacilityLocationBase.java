
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_RecycleFacilityLocation_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_RecycleFacilityLocation_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="CommonAreas"/&amp;gt;
 *     &amp;lt;enumeration value="Garage"/&amp;gt;
 *     &amp;lt;enumeration value="GuestRoom"/&amp;gt;
 *     &amp;lt;enumeration value="MeetingRoom"/&amp;gt;
 *     &amp;lt;enumeration value="Restaurant"/&amp;gt;
 *     &amp;lt;enumeration value="StaffArea"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_RecycleFacilityLocation_Base")
@XmlEnum
public enum ListRecycleFacilityLocationBase {

    @XmlEnumValue("CommonAreas")
    COMMON_AREAS("CommonAreas"),
    @XmlEnumValue("Garage")
    GARAGE("Garage"),
    @XmlEnumValue("GuestRoom")
    GUEST_ROOM("GuestRoom"),
    @XmlEnumValue("MeetingRoom")
    MEETING_ROOM("MeetingRoom"),
    @XmlEnumValue("Restaurant")
    RESTAURANT("Restaurant"),
    @XmlEnumValue("StaffArea")
    STAFF_AREA("StaffArea"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListRecycleFacilityLocationBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListRecycleFacilityLocationBase fromValue(String v) {
        for (ListRecycleFacilityLocationBase c: ListRecycleFacilityLocationBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
