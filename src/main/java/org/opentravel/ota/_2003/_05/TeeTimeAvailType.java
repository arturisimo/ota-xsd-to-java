
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tee time information associated with an availability request.
 * 
 * &lt;p&gt;Clase Java para TeeTimeAvailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TeeTimeAvailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Golfer" type="{http://www.opentravel.org/OTA/2003/05}GolferType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Discounts" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateQualifierType"&amp;gt;
 *                 &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Amenity" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GolfAmenityType"&amp;gt;
 *                 &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *       &amp;lt;attribute name="RoundID" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="GolferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="NumberOfHoles" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="CartQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ReplayRoundQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="DisabledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TeeTimeAvailType", propOrder = {
    "golfer",
    "discounts",
    "amenity"
})
public class TeeTimeAvailType {

    @XmlElement(name = "Golfer")
    protected List<GolferType> golfer;
    @XmlElement(name = "Discounts")
    protected List<TeeTimeAvailType.Discounts> discounts;
    @XmlElement(name = "Amenity")
    protected List<TeeTimeAvailType.Amenity> amenity;
    @XmlAttribute(name = "RoundID")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger roundID;
    @XmlAttribute(name = "GolferQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger golferQty;
    @XmlAttribute(name = "PackageID")
    protected String packageID;
    @XmlAttribute(name = "NumberOfHoles")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger numberOfHoles;
    @XmlAttribute(name = "CartQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger cartQty;
    @XmlAttribute(name = "ReplayRoundQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger replayRoundQty;
    @XmlAttribute(name = "DisabledInd")
    protected Boolean disabledInd;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;

    /**
     * Gets the value of the golfer property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the golfer property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getGolfer().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GolferType }
     * 
     * 
     */
    public List<GolferType> getGolfer() {
        if (golfer == null) {
            golfer = new ArrayList<GolferType>();
        }
        return this.golfer;
    }

    /**
     * Gets the value of the discounts property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discounts property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDiscounts().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TeeTimeAvailType.Discounts }
     * 
     * 
     */
    public List<TeeTimeAvailType.Discounts> getDiscounts() {
        if (discounts == null) {
            discounts = new ArrayList<TeeTimeAvailType.Discounts>();
        }
        return this.discounts;
    }

    /**
     * Gets the value of the amenity property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the amenity property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAmenity().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TeeTimeAvailType.Amenity }
     * 
     * 
     */
    public List<TeeTimeAvailType.Amenity> getAmenity() {
        if (amenity == null) {
            amenity = new ArrayList<TeeTimeAvailType.Amenity>();
        }
        return this.amenity;
    }

    /**
     * Obtiene el valor de la propiedad roundID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRoundID() {
        return roundID;
    }

    /**
     * Define el valor de la propiedad roundID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRoundID(BigInteger value) {
        this.roundID = value;
    }

    /**
     * Obtiene el valor de la propiedad golferQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGolferQty() {
        return golferQty;
    }

    /**
     * Define el valor de la propiedad golferQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGolferQty(BigInteger value) {
        this.golferQty = value;
    }

    /**
     * Obtiene el valor de la propiedad packageID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageID() {
        return packageID;
    }

    /**
     * Define el valor de la propiedad packageID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageID(String value) {
        this.packageID = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfHoles.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfHoles() {
        return numberOfHoles;
    }

    /**
     * Define el valor de la propiedad numberOfHoles.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfHoles(BigInteger value) {
        this.numberOfHoles = value;
    }

    /**
     * Obtiene el valor de la propiedad cartQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCartQty() {
        return cartQty;
    }

    /**
     * Define el valor de la propiedad cartQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCartQty(BigInteger value) {
        this.cartQty = value;
    }

    /**
     * Obtiene el valor de la propiedad replayRoundQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getReplayRoundQty() {
        return replayRoundQty;
    }

    /**
     * Define el valor de la propiedad replayRoundQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setReplayRoundQty(BigInteger value) {
        this.replayRoundQty = value;
    }

    /**
     * Obtiene el valor de la propiedad disabledInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisabledInd() {
        return disabledInd;
    }

    /**
     * Define el valor de la propiedad disabledInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisabledInd(Boolean value) {
        this.disabledInd = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GolfAmenityType"&amp;gt;
     *       &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Amenity
        extends GolfAmenityType
    {

        @XmlAttribute(name = "GolferRPH")
        protected String golferRPH;

        /**
         * Obtiene el valor de la propiedad golferRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGolferRPH() {
            return golferRPH;
        }

        /**
         * Define el valor de la propiedad golferRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGolferRPH(String value) {
            this.golferRPH = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateQualifierType"&amp;gt;
     *       &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Discounts
        extends RateQualifierType
    {

        @XmlAttribute(name = "GolferRPH")
        protected String golferRPH;

        /**
         * Obtiene el valor de la propiedad golferRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGolferRPH() {
            return golferRPH;
        }

        /**
         * Define el valor de la propiedad golferRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGolferRPH(String value) {
            this.golferRPH = value;
        }

    }

}
