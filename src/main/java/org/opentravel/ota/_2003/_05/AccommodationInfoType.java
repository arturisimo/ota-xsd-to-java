
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides accommodation location, dates and classifications
 * 
 * &lt;p&gt;Clase Java para AccommodationInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccommodationInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Property" type="{http://www.opentravel.org/OTA/2003/05}PropertyIdentityType"/&amp;gt;
 *         &amp;lt;element name="Resort" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResortCodeGroup"/&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DestinationLevelGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AccommodationClass" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AccommodationClassGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SourceIdentification" type="{http://www.opentravel.org/OTA/2003/05}SourceIdentificationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ContentInfo" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}PackageID_RefType" /&amp;gt;
 *       &amp;lt;attribute name="MinChildAge" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *       &amp;lt;attribute name="MaxChildAge" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *       &amp;lt;attribute name="BaseMealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccommodationInfoType", propOrder = {
    "property",
    "resort",
    "accommodationClass",
    "sourceIdentification",
    "contentInfo"
})
@XmlSeeAlso({
    AccommodationDetailType.class
})
public class AccommodationInfoType {

    @XmlElement(name = "Property", required = true)
    protected PropertyIdentityType property;
    @XmlElement(name = "Resort")
    protected AccommodationInfoType.Resort resort;
    @XmlElement(name = "AccommodationClass")
    protected AccommodationInfoType.AccommodationClass accommodationClass;
    @XmlElement(name = "SourceIdentification")
    protected SourceIdentificationType sourceIdentification;
    @XmlElement(name = "ContentInfo")
    protected URLType contentInfo;
    @XmlAttribute(name = "PackageID")
    protected String packageID;
    @XmlAttribute(name = "MinChildAge")
    protected Integer minChildAge;
    @XmlAttribute(name = "MaxChildAge")
    protected Integer maxChildAge;
    @XmlAttribute(name = "BaseMealPlan")
    protected String baseMealPlan;

    /**
     * Obtiene el valor de la propiedad property.
     * 
     * @return
     *     possible object is
     *     {@link PropertyIdentityType }
     *     
     */
    public PropertyIdentityType getProperty() {
        return property;
    }

    /**
     * Define el valor de la propiedad property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropertyIdentityType }
     *     
     */
    public void setProperty(PropertyIdentityType value) {
        this.property = value;
    }

    /**
     * Obtiene el valor de la propiedad resort.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationInfoType.Resort }
     *     
     */
    public AccommodationInfoType.Resort getResort() {
        return resort;
    }

    /**
     * Define el valor de la propiedad resort.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationInfoType.Resort }
     *     
     */
    public void setResort(AccommodationInfoType.Resort value) {
        this.resort = value;
    }

    /**
     * Obtiene el valor de la propiedad accommodationClass.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationInfoType.AccommodationClass }
     *     
     */
    public AccommodationInfoType.AccommodationClass getAccommodationClass() {
        return accommodationClass;
    }

    /**
     * Define el valor de la propiedad accommodationClass.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationInfoType.AccommodationClass }
     *     
     */
    public void setAccommodationClass(AccommodationInfoType.AccommodationClass value) {
        this.accommodationClass = value;
    }

    /**
     * Obtiene el valor de la propiedad sourceIdentification.
     * 
     * @return
     *     possible object is
     *     {@link SourceIdentificationType }
     *     
     */
    public SourceIdentificationType getSourceIdentification() {
        return sourceIdentification;
    }

    /**
     * Define el valor de la propiedad sourceIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceIdentificationType }
     *     
     */
    public void setSourceIdentification(SourceIdentificationType value) {
        this.sourceIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad contentInfo.
     * 
     * @return
     *     possible object is
     *     {@link URLType }
     *     
     */
    public URLType getContentInfo() {
        return contentInfo;
    }

    /**
     * Define el valor de la propiedad contentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link URLType }
     *     
     */
    public void setContentInfo(URLType value) {
        this.contentInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad packageID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageID() {
        return packageID;
    }

    /**
     * Define el valor de la propiedad packageID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageID(String value) {
        this.packageID = value;
    }

    /**
     * Obtiene el valor de la propiedad minChildAge.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinChildAge() {
        return minChildAge;
    }

    /**
     * Define el valor de la propiedad minChildAge.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinChildAge(Integer value) {
        this.minChildAge = value;
    }

    /**
     * Obtiene el valor de la propiedad maxChildAge.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxChildAge() {
        return maxChildAge;
    }

    /**
     * Define el valor de la propiedad maxChildAge.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxChildAge(Integer value) {
        this.maxChildAge = value;
    }

    /**
     * Obtiene el valor de la propiedad baseMealPlan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseMealPlan() {
        return baseMealPlan;
    }

    /**
     * Define el valor de la propiedad baseMealPlan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseMealPlan(String value) {
        this.baseMealPlan = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AccommodationClassGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AccommodationClass {

        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "Name")
        protected String name;
        @XmlAttribute(name = "NationalCode")
        protected String nationalCode;
        @XmlAttribute(name = "OfficialName")
        protected String officialName;

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad nationalCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNationalCode() {
            return nationalCode;
        }

        /**
         * Define el valor de la propiedad nationalCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNationalCode(String value) {
            this.nationalCode = value;
        }

        /**
         * Obtiene el valor de la propiedad officialName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfficialName() {
            return officialName;
        }

        /**
         * Define el valor de la propiedad officialName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfficialName(String value) {
            this.officialName = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResortCodeGroup"/&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DestinationLevelGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Resort {

        @XmlAttribute(name = "ResortCode")
        protected String resortCode;
        @XmlAttribute(name = "ResortName")
        protected String resortName;
        @XmlAttribute(name = "DestinationCode")
        protected String destinationCode;
        @XmlAttribute(name = "DestinationLevel")
        protected DestinationLevelType destinationLevel;
        @XmlAttribute(name = "DestinationName")
        protected String destinationName;

        /**
         * Obtiene el valor de la propiedad resortCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResortCode() {
            return resortCode;
        }

        /**
         * Define el valor de la propiedad resortCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResortCode(String value) {
            this.resortCode = value;
        }

        /**
         * Obtiene el valor de la propiedad resortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResortName() {
            return resortName;
        }

        /**
         * Define el valor de la propiedad resortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResortName(String value) {
            this.resortName = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationCode() {
            return destinationCode;
        }

        /**
         * Define el valor de la propiedad destinationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationCode(String value) {
            this.destinationCode = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationLevel.
         * 
         * @return
         *     possible object is
         *     {@link DestinationLevelType }
         *     
         */
        public DestinationLevelType getDestinationLevel() {
            return destinationLevel;
        }

        /**
         * Define el valor de la propiedad destinationLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link DestinationLevelType }
         *     
         */
        public void setDestinationLevel(DestinationLevelType value) {
            this.destinationLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationName() {
            return destinationName;
        }

        /**
         * Define el valor de la propiedad destinationName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationName(String value) {
            this.destinationName = value;
        }

    }

}
