
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_ApplyToLevel_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_ApplyToLevel_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Cabin"/&amp;gt;
 *     &amp;lt;enumeration value="Facility"/&amp;gt;
 *     &amp;lt;enumeration value="GuestRoom"/&amp;gt;
 *     &amp;lt;enumeration value="MeetingRoom"/&amp;gt;
 *     &amp;lt;enumeration value="Property"/&amp;gt;
 *     &amp;lt;enumeration value="Seat"/&amp;gt;
 *     &amp;lt;enumeration value="Ship"/&amp;gt;
 *     &amp;lt;enumeration value="Unit"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_ApplyToLevel_Base")
@XmlEnum
public enum ListApplyToLevelBase {


    /**
     * An airplane, train or ship cabin.
     * 
     */
    @XmlEnumValue("Cabin")
    CABIN("Cabin"),

    /**
     * An on or offsite facility, such as a restaurant or car rental location.
     * 
     */
    @XmlEnumValue("Facility")
    FACILITY("Facility"),

    /**
     * A guest room.
     * 
     */
    @XmlEnumValue("GuestRoom")
    GUEST_ROOM("GuestRoom"),

    /**
     * A meeting room.
     * 
     */
    @XmlEnumValue("MeetingRoom")
    MEETING_ROOM("MeetingRoom"),

    /**
     * A property, such as a hotel or vacation rental unit.
     * 
     */
    @XmlEnumValue("Property")
    PROPERTY("Property"),

    /**
     * A seat, such as an airplane or train seat.
     * 
     */
    @XmlEnumValue("Seat")
    SEAT("Seat"),

    /**
     * A cruise ship.
     * 
     */
    @XmlEnumValue("Ship")
    SHIP("Ship"),

    /**
     * A vacation rental unit.
     * 
     */
    @XmlEnumValue("Unit")
    UNIT("Unit"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListApplyToLevelBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListApplyToLevelBase fromValue(String v) {
        for (ListApplyToLevelBase c: ListApplyToLevelBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
