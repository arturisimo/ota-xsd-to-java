
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para AccommodationClassEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AccommodationClassEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="FirstClass"/&amp;gt;
 *     &amp;lt;enumeration value="SecondClass"/&amp;gt;
 *     &amp;lt;enumeration value="Premium"/&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="Leisure"/&amp;gt;
 *     &amp;lt;enumeration value="Coach"/&amp;gt;
 *     &amp;lt;enumeration value="Deluxe"/&amp;gt;
 *     &amp;lt;enumeration value="GranClasse"/&amp;gt;
 *     &amp;lt;enumeration value="SoftClass"/&amp;gt;
 *     &amp;lt;enumeration value="HardClass"/&amp;gt;
 *     &amp;lt;enumeration value="SpecialClass"/&amp;gt;
 *     &amp;lt;enumeration value="HighGradeSoftClass"/&amp;gt;
 *     &amp;lt;enumeration value="MixedHardClass"/&amp;gt;
 *     &amp;lt;enumeration value="MixedSoftClass"/&amp;gt;
 *     &amp;lt;enumeration value="SoftCompartmentClass"/&amp;gt;
 *     &amp;lt;enumeration value="HardCompartmentClass"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AccommodationClassEnum")
@XmlEnum
public enum AccommodationClassEnum {

    @XmlEnumValue("FirstClass")
    FIRST_CLASS("FirstClass"),
    @XmlEnumValue("SecondClass")
    SECOND_CLASS("SecondClass"),
    @XmlEnumValue("Premium")
    PREMIUM("Premium"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("Leisure")
    LEISURE("Leisure"),
    @XmlEnumValue("Coach")
    COACH("Coach"),
    @XmlEnumValue("Deluxe")
    DELUXE("Deluxe"),
    @XmlEnumValue("GranClasse")
    GRAN_CLASSE("GranClasse"),
    @XmlEnumValue("SoftClass")
    SOFT_CLASS("SoftClass"),
    @XmlEnumValue("HardClass")
    HARD_CLASS("HardClass"),
    @XmlEnumValue("SpecialClass")
    SPECIAL_CLASS("SpecialClass"),
    @XmlEnumValue("HighGradeSoftClass")
    HIGH_GRADE_SOFT_CLASS("HighGradeSoftClass"),
    @XmlEnumValue("MixedHardClass")
    MIXED_HARD_CLASS("MixedHardClass"),
    @XmlEnumValue("MixedSoftClass")
    MIXED_SOFT_CLASS("MixedSoftClass"),
    @XmlEnumValue("SoftCompartmentClass")
    SOFT_COMPARTMENT_CLASS("SoftCompartmentClass"),
    @XmlEnumValue("HardCompartmentClass")
    HARD_COMPARTMENT_CLASS("HardCompartmentClass"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support. See AccommodationClass. 
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    AccommodationClassEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccommodationClassEnum fromValue(String v) {
        for (AccommodationClassEnum c: AccommodationClassEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
