
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para BerthPositionType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="BerthPositionType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Upper"/&amp;gt;
 *     &amp;lt;enumeration value="Middle"/&amp;gt;
 *     &amp;lt;enumeration value="Lower"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "BerthPositionType")
@XmlEnum
public enum BerthPositionType {

    @XmlEnumValue("Upper")
    UPPER("Upper"),
    @XmlEnumValue("Middle")
    MIDDLE("Middle"),
    @XmlEnumValue("Lower")
    LOWER("Lower");
    private final String value;

    BerthPositionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BerthPositionType fromValue(String v) {
        for (BerthPositionType c: BerthPositionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
