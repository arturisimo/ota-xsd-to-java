
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Dimension measurement system, unit of measure and dimension values with ontology reference.
 * 
 * &lt;p&gt;Clase Java para OntologyDimensionType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OntologyDimensionType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="DimensionUnit" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDimensionUOM"&amp;gt;
 *                 &amp;lt;attribute name="Height" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                 &amp;lt;attribute name="Length" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                 &amp;lt;attribute name="Width" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                 &amp;lt;attribute name="OtherType"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="MeasurementSystem" use="required"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}List_OfferMeasurementSystem"&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OntologyDimensionType", propOrder = {
    "dimensionUnit",
    "ontologyExtension"
})
public class OntologyDimensionType {

    @XmlElement(name = "DimensionUnit")
    protected OntologyDimensionType.DimensionUnit dimensionUnit;
    @XmlElement(name = "OntologyExtension")
    protected OntologyExtensionType ontologyExtension;
    @XmlAttribute(name = "MeasurementSystem", required = true)
    protected ListOfferMeasurementSystem measurementSystem;
    @XmlAttribute(name = "OntologyRefID")
    protected String ontologyRefID;

    /**
     * Obtiene el valor de la propiedad dimensionUnit.
     * 
     * @return
     *     possible object is
     *     {@link OntologyDimensionType.DimensionUnit }
     *     
     */
    public OntologyDimensionType.DimensionUnit getDimensionUnit() {
        return dimensionUnit;
    }

    /**
     * Define el valor de la propiedad dimensionUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyDimensionType.DimensionUnit }
     *     
     */
    public void setDimensionUnit(OntologyDimensionType.DimensionUnit value) {
        this.dimensionUnit = value;
    }

    /**
     * Obtiene el valor de la propiedad ontologyExtension.
     * 
     * @return
     *     possible object is
     *     {@link OntologyExtensionType }
     *     
     */
    public OntologyExtensionType getOntologyExtension() {
        return ontologyExtension;
    }

    /**
     * Define el valor de la propiedad ontologyExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyExtensionType }
     *     
     */
    public void setOntologyExtension(OntologyExtensionType value) {
        this.ontologyExtension = value;
    }

    /**
     * Obtiene el valor de la propiedad measurementSystem.
     * 
     * @return
     *     possible object is
     *     {@link ListOfferMeasurementSystem }
     *     
     */
    public ListOfferMeasurementSystem getMeasurementSystem() {
        return measurementSystem;
    }

    /**
     * Define el valor de la propiedad measurementSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfferMeasurementSystem }
     *     
     */
    public void setMeasurementSystem(ListOfferMeasurementSystem value) {
        this.measurementSystem = value;
    }

    /**
     * Obtiene el valor de la propiedad ontologyRefID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOntologyRefID() {
        return ontologyRefID;
    }

    /**
     * Define el valor de la propiedad ontologyRefID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOntologyRefID(String value) {
        this.ontologyRefID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDimensionUOM"&amp;gt;
     *       &amp;lt;attribute name="Height" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *       &amp;lt;attribute name="Length" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *       &amp;lt;attribute name="Width" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *       &amp;lt;attribute name="OtherType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class DimensionUnit {

        @XmlValue
        protected ListOfferDimensionUOM value;
        @XmlAttribute(name = "Height")
        protected BigDecimal height;
        @XmlAttribute(name = "Length")
        protected BigDecimal length;
        @XmlAttribute(name = "Width")
        protected BigDecimal width;
        @XmlAttribute(name = "OtherType")
        protected String otherType;
        @XmlAttribute(name = "OntologyRefID")
        protected String ontologyRefID;

        /**
         * Source: Unit of Measure (UOM) OpenTravel codelist.
         * 
         * @return
         *     possible object is
         *     {@link ListOfferDimensionUOM }
         *     
         */
        public ListOfferDimensionUOM getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfferDimensionUOM }
         *     
         */
        public void setValue(ListOfferDimensionUOM value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad height.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getHeight() {
            return height;
        }

        /**
         * Define el valor de la propiedad height.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setHeight(BigDecimal value) {
            this.height = value;
        }

        /**
         * Obtiene el valor de la propiedad length.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLength() {
            return length;
        }

        /**
         * Define el valor de la propiedad length.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLength(BigDecimal value) {
            this.length = value;
        }

        /**
         * Obtiene el valor de la propiedad width.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getWidth() {
            return width;
        }

        /**
         * Define el valor de la propiedad width.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setWidth(BigDecimal value) {
            this.width = value;
        }

        /**
         * Obtiene el valor de la propiedad otherType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOtherType() {
            return otherType;
        }

        /**
         * Define el valor de la propiedad otherType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOtherType(String value) {
            this.otherType = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyRefID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOntologyRefID() {
            return ontologyRefID;
        }

        /**
         * Define el valor de la propiedad ontologyRefID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOntologyRefID(String value) {
            this.ontologyRefID = value;
        }

    }

}
