
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;


/**
 * A collection of hotel reviews made by travelers.
 * 
 * &lt;p&gt;Clase Java para ReviewsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ReviewsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Questions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="QuestionCategory" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Categories" type="{http://www.opentravel.org/OTA/2003/05}List_QuestionCategory" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Question" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="QuestionText" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                               &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;attribute name="QuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="AdditionalQuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="QuestionResponseRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;attribute name="MainQuestionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="QuestionResponseTypes" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="QuestionResponseType" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ResponseTypeDetail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="ResponseValue" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="ResponseValueDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="ResponseType"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="Rating"/&amp;gt;
 *                                 &amp;lt;enumeration value="Text"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ReviewSubject" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Hotel" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Review" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ReviewerInfo" type="{http://www.opentravel.org/OTA/2003/05}TravelerType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="ResponseGroups" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AnswerGroupsType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="ResponseGroup" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResponseGroupType"&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="CategoryAggregations" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="CategoryAggregation" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="QuestionCategoryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                               &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="SupplierResponse" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="ReviewID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="ResponseLanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                           &amp;lt;attribute name="MonthYearOfTravel" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
 *                           &amp;lt;attribute name="ReviewDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="ReviewerCompany" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="TripDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                           &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AggregateInfos" type="{http://www.opentravel.org/OTA/2003/05}AggregateInfoType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReviewsType", propOrder = {
    "questions",
    "questionResponseTypes",
    "reviewSubject",
    "aggregateInfos"
})
public class ReviewsType {

    @XmlElement(name = "Questions")
    protected ReviewsType.Questions questions;
    @XmlElement(name = "QuestionResponseTypes")
    protected ReviewsType.QuestionResponseTypes questionResponseTypes;
    @XmlElement(name = "ReviewSubject")
    protected List<ReviewsType.ReviewSubject> reviewSubject;
    @XmlElement(name = "AggregateInfos")
    protected AggregateInfoType aggregateInfos;

    /**
     * Obtiene el valor de la propiedad questions.
     * 
     * @return
     *     possible object is
     *     {@link ReviewsType.Questions }
     *     
     */
    public ReviewsType.Questions getQuestions() {
        return questions;
    }

    /**
     * Define el valor de la propiedad questions.
     * 
     * @param value
     *     allowed object is
     *     {@link ReviewsType.Questions }
     *     
     */
    public void setQuestions(ReviewsType.Questions value) {
        this.questions = value;
    }

    /**
     * Obtiene el valor de la propiedad questionResponseTypes.
     * 
     * @return
     *     possible object is
     *     {@link ReviewsType.QuestionResponseTypes }
     *     
     */
    public ReviewsType.QuestionResponseTypes getQuestionResponseTypes() {
        return questionResponseTypes;
    }

    /**
     * Define el valor de la propiedad questionResponseTypes.
     * 
     * @param value
     *     allowed object is
     *     {@link ReviewsType.QuestionResponseTypes }
     *     
     */
    public void setQuestionResponseTypes(ReviewsType.QuestionResponseTypes value) {
        this.questionResponseTypes = value;
    }

    /**
     * Gets the value of the reviewSubject property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reviewSubject property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getReviewSubject().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ReviewsType.ReviewSubject }
     * 
     * 
     */
    public List<ReviewsType.ReviewSubject> getReviewSubject() {
        if (reviewSubject == null) {
            reviewSubject = new ArrayList<ReviewsType.ReviewSubject>();
        }
        return this.reviewSubject;
    }

    /**
     * Obtiene el valor de la propiedad aggregateInfos.
     * 
     * @return
     *     possible object is
     *     {@link AggregateInfoType }
     *     
     */
    public AggregateInfoType getAggregateInfos() {
        return aggregateInfos;
    }

    /**
     * Define el valor de la propiedad aggregateInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link AggregateInfoType }
     *     
     */
    public void setAggregateInfos(AggregateInfoType value) {
        this.aggregateInfos = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="QuestionResponseType" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ResponseTypeDetail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="ResponseValue" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="ResponseValueDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ResponseType"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="Rating"/&amp;gt;
     *                       &amp;lt;enumeration value="Text"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "questionResponseType"
    })
    public static class QuestionResponseTypes {

        @XmlElement(name = "QuestionResponseType", required = true)
        protected List<ReviewsType.QuestionResponseTypes.QuestionResponseType> questionResponseType;

        /**
         * Gets the value of the questionResponseType property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the questionResponseType property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getQuestionResponseType().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ReviewsType.QuestionResponseTypes.QuestionResponseType }
         * 
         * 
         */
        public List<ReviewsType.QuestionResponseTypes.QuestionResponseType> getQuestionResponseType() {
            if (questionResponseType == null) {
                questionResponseType = new ArrayList<ReviewsType.QuestionResponseTypes.QuestionResponseType>();
            }
            return this.questionResponseType;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ResponseTypeDetail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="ResponseValue" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="ResponseValueDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ResponseType"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="Rating"/&amp;gt;
         *             &amp;lt;enumeration value="Text"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "responseTypeDetail"
        })
        public static class QuestionResponseType {

            @XmlElement(name = "ResponseTypeDetail")
            protected List<ReviewsType.QuestionResponseTypes.QuestionResponseType.ResponseTypeDetail> responseTypeDetail;
            @XmlAttribute(name = "ResponseType")
            protected String responseType;
            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Gets the value of the responseTypeDetail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the responseTypeDetail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getResponseTypeDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ReviewsType.QuestionResponseTypes.QuestionResponseType.ResponseTypeDetail }
             * 
             * 
             */
            public List<ReviewsType.QuestionResponseTypes.QuestionResponseType.ResponseTypeDetail> getResponseTypeDetail() {
                if (responseTypeDetail == null) {
                    responseTypeDetail = new ArrayList<ReviewsType.QuestionResponseTypes.QuestionResponseType.ResponseTypeDetail>();
                }
                return this.responseTypeDetail;
            }

            /**
             * Obtiene el valor de la propiedad responseType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResponseType() {
                return responseType;
            }

            /**
             * Define el valor de la propiedad responseType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResponseType(String value) {
                this.responseType = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="ResponseValue" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="ResponseValueDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ResponseTypeDetail {

                @XmlAttribute(name = "ResponseValue")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger responseValue;
                @XmlAttribute(name = "ResponseValueDescription")
                protected String responseValueDescription;
                @XmlAttribute(name = "Language")
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "language")
                protected String language;

                /**
                 * Obtiene el valor de la propiedad responseValue.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getResponseValue() {
                    return responseValue;
                }

                /**
                 * Define el valor de la propiedad responseValue.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setResponseValue(BigInteger value) {
                    this.responseValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad responseValueDescription.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResponseValueDescription() {
                    return responseValueDescription;
                }

                /**
                 * Define el valor de la propiedad responseValueDescription.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResponseValueDescription(String value) {
                    this.responseValueDescription = value;
                }

                /**
                 * Obtiene el valor de la propiedad language.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLanguage() {
                    return language;
                }

                /**
                 * Define el valor de la propiedad language.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLanguage(String value) {
                    this.language = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="QuestionCategory" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Categories" type="{http://www.opentravel.org/OTA/2003/05}List_QuestionCategory" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Question" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="QuestionText" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                                     &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="QuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="AdditionalQuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="QuestionResponseRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="MainQuestionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "questionCategory",
        "aggregations"
    })
    public static class Questions {

        @XmlElement(name = "QuestionCategory", required = true)
        protected List<ReviewsType.Questions.QuestionCategory> questionCategory;
        @XmlElement(name = "Aggregations")
        protected AggregationType aggregations;

        /**
         * Gets the value of the questionCategory property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the questionCategory property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getQuestionCategory().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ReviewsType.Questions.QuestionCategory }
         * 
         * 
         */
        public List<ReviewsType.Questions.QuestionCategory> getQuestionCategory() {
            if (questionCategory == null) {
                questionCategory = new ArrayList<ReviewsType.Questions.QuestionCategory>();
            }
            return this.questionCategory;
        }

        /**
         * Obtiene el valor de la propiedad aggregations.
         * 
         * @return
         *     possible object is
         *     {@link AggregationType }
         *     
         */
        public AggregationType getAggregations() {
            return aggregations;
        }

        /**
         * Define el valor de la propiedad aggregations.
         * 
         * @param value
         *     allowed object is
         *     {@link AggregationType }
         *     
         */
        public void setAggregations(AggregationType value) {
            this.aggregations = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Categories" type="{http://www.opentravel.org/OTA/2003/05}List_QuestionCategory" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Question" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="QuestionText" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *                           &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="QuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="AdditionalQuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="QuestionResponseRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="MainQuestionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "categories",
            "question",
            "aggregations"
        })
        public static class QuestionCategory {

            @XmlElement(name = "Categories")
            protected ListQuestionCategory categories;
            @XmlElement(name = "Question", required = true)
            protected List<ReviewsType.Questions.QuestionCategory.Question> question;
            @XmlElement(name = "Aggregations")
            protected AggregationType aggregations;
            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Obtiene el valor de la propiedad categories.
             * 
             * @return
             *     possible object is
             *     {@link ListQuestionCategory }
             *     
             */
            public ListQuestionCategory getCategories() {
                return categories;
            }

            /**
             * Define el valor de la propiedad categories.
             * 
             * @param value
             *     allowed object is
             *     {@link ListQuestionCategory }
             *     
             */
            public void setCategories(ListQuestionCategory value) {
                this.categories = value;
            }

            /**
             * Gets the value of the question property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the question property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getQuestion().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ReviewsType.Questions.QuestionCategory.Question }
             * 
             * 
             */
            public List<ReviewsType.Questions.QuestionCategory.Question> getQuestion() {
                if (question == null) {
                    question = new ArrayList<ReviewsType.Questions.QuestionCategory.Question>();
                }
                return this.question;
            }

            /**
             * Obtiene el valor de la propiedad aggregations.
             * 
             * @return
             *     possible object is
             *     {@link AggregationType }
             *     
             */
            public AggregationType getAggregations() {
                return aggregations;
            }

            /**
             * Define el valor de la propiedad aggregations.
             * 
             * @param value
             *     allowed object is
             *     {@link AggregationType }
             *     
             */
            public void setAggregations(AggregationType value) {
                this.aggregations = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="QuestionText" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *                 &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Aggregations" type="{http://www.opentravel.org/OTA/2003/05}AggregationType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="QuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="AdditionalQuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="QuestionResponseRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="MainQuestionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "questionText",
                "aggregations"
            })
            public static class Question {

                @XmlElement(name = "QuestionText")
                protected List<ReviewsType.Questions.QuestionCategory.Question.QuestionText> questionText;
                @XmlElement(name = "Aggregations")
                protected AggregationType aggregations;
                @XmlAttribute(name = "RPH")
                protected String rph;
                @XmlAttribute(name = "QuestionID")
                protected String questionID;
                @XmlAttribute(name = "AdditionalQuestionID")
                protected String additionalQuestionID;
                @XmlAttribute(name = "QuestionResponseRPH")
                protected String questionResponseRPH;
                @XmlAttribute(name = "MainQuestionInd")
                protected Boolean mainQuestionInd;

                /**
                 * Gets the value of the questionText property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the questionText property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getQuestionText().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ReviewsType.Questions.QuestionCategory.Question.QuestionText }
                 * 
                 * 
                 */
                public List<ReviewsType.Questions.QuestionCategory.Question.QuestionText> getQuestionText() {
                    if (questionText == null) {
                        questionText = new ArrayList<ReviewsType.Questions.QuestionCategory.Question.QuestionText>();
                    }
                    return this.questionText;
                }

                /**
                 * Obtiene el valor de la propiedad aggregations.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AggregationType }
                 *     
                 */
                public AggregationType getAggregations() {
                    return aggregations;
                }

                /**
                 * Define el valor de la propiedad aggregations.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AggregationType }
                 *     
                 */
                public void setAggregations(AggregationType value) {
                    this.aggregations = value;
                }

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRPH(String value) {
                    this.rph = value;
                }

                /**
                 * Obtiene el valor de la propiedad questionID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getQuestionID() {
                    return questionID;
                }

                /**
                 * Define el valor de la propiedad questionID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setQuestionID(String value) {
                    this.questionID = value;
                }

                /**
                 * Obtiene el valor de la propiedad additionalQuestionID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAdditionalQuestionID() {
                    return additionalQuestionID;
                }

                /**
                 * Define el valor de la propiedad additionalQuestionID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAdditionalQuestionID(String value) {
                    this.additionalQuestionID = value;
                }

                /**
                 * Obtiene el valor de la propiedad questionResponseRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getQuestionResponseRPH() {
                    return questionResponseRPH;
                }

                /**
                 * Define el valor de la propiedad questionResponseRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setQuestionResponseRPH(String value) {
                    this.questionResponseRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad mainQuestionInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMainQuestionInd() {
                    return mainQuestionInd;
                }

                /**
                 * Define el valor de la propiedad mainQuestionInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMainQuestionInd(Boolean value) {
                    this.mainQuestionInd = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
                 *       &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class QuestionText {

                    @XmlValue
                    protected String value;
                    @XmlAttribute(name = "LanguageCode")
                    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                    @XmlSchemaType(name = "language")
                    protected String languageCode;

                    /**
                     * Obtiene el valor de la propiedad value.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad languageCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLanguageCode() {
                        return languageCode;
                    }

                    /**
                     * Define el valor de la propiedad languageCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLanguageCode(String value) {
                        this.languageCode = value;
                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Hotel" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Review" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ReviewerInfo" type="{http://www.opentravel.org/OTA/2003/05}TravelerType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="ResponseGroups" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AnswerGroupsType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ResponseGroup" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResponseGroupType"&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CategoryAggregations" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="CategoryAggregation" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="QuestionCategoryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="SupplierResponse" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="ReviewID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="ResponseLanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *                 &amp;lt;attribute name="MonthYearOfTravel" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
     *                 &amp;lt;attribute name="ReviewDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="ReviewerCompany" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="TripDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                 &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotel",
        "review"
    })
    public static class ReviewSubject {

        @XmlElement(name = "Hotel")
        protected ReviewsType.ReviewSubject.Hotel hotel;
        @XmlElement(name = "Review")
        protected List<ReviewsType.ReviewSubject.Review> review;

        /**
         * Obtiene el valor de la propiedad hotel.
         * 
         * @return
         *     possible object is
         *     {@link ReviewsType.ReviewSubject.Hotel }
         *     
         */
        public ReviewsType.ReviewSubject.Hotel getHotel() {
            return hotel;
        }

        /**
         * Define el valor de la propiedad hotel.
         * 
         * @param value
         *     allowed object is
         *     {@link ReviewsType.ReviewSubject.Hotel }
         *     
         */
        public void setHotel(ReviewsType.ReviewSubject.Hotel value) {
            this.hotel = value;
        }

        /**
         * Gets the value of the review property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the review property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getReview().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ReviewsType.ReviewSubject.Review }
         * 
         * 
         */
        public List<ReviewsType.ReviewSubject.Review> getReview() {
            if (review == null) {
                review = new ArrayList<ReviewsType.ReviewSubject.Review>();
            }
            return this.review;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Hotel {

            @XmlAttribute(name = "ChainCode")
            protected String chainCode;
            @XmlAttribute(name = "BrandCode")
            protected String brandCode;
            @XmlAttribute(name = "HotelCode")
            protected String hotelCode;
            @XmlAttribute(name = "HotelCityCode")
            protected String hotelCityCode;
            @XmlAttribute(name = "HotelName")
            protected String hotelName;
            @XmlAttribute(name = "HotelCodeContext")
            protected String hotelCodeContext;
            @XmlAttribute(name = "ChainName")
            protected String chainName;
            @XmlAttribute(name = "BrandName")
            protected String brandName;
            @XmlAttribute(name = "AreaID")
            protected String areaID;
            @XmlAttribute(name = "TTIcode")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger ttIcode;

            /**
             * Obtiene el valor de la propiedad chainCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainCode() {
                return chainCode;
            }

            /**
             * Define el valor de la propiedad chainCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainCode(String value) {
                this.chainCode = value;
            }

            /**
             * Obtiene el valor de la propiedad brandCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandCode() {
                return brandCode;
            }

            /**
             * Define el valor de la propiedad brandCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandCode(String value) {
                this.brandCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCode() {
                return hotelCode;
            }

            /**
             * Define el valor de la propiedad hotelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCode(String value) {
                this.hotelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCityCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCityCode() {
                return hotelCityCode;
            }

            /**
             * Define el valor de la propiedad hotelCityCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCityCode(String value) {
                this.hotelCityCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelName() {
                return hotelName;
            }

            /**
             * Define el valor de la propiedad hotelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelName(String value) {
                this.hotelName = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCodeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCodeContext() {
                return hotelCodeContext;
            }

            /**
             * Define el valor de la propiedad hotelCodeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCodeContext(String value) {
                this.hotelCodeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad chainName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainName() {
                return chainName;
            }

            /**
             * Define el valor de la propiedad chainName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainName(String value) {
                this.chainName = value;
            }

            /**
             * Obtiene el valor de la propiedad brandName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandName() {
                return brandName;
            }

            /**
             * Define el valor de la propiedad brandName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandName(String value) {
                this.brandName = value;
            }

            /**
             * Obtiene el valor de la propiedad areaID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAreaID() {
                return areaID;
            }

            /**
             * Define el valor de la propiedad areaID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAreaID(String value) {
                this.areaID = value;
            }

            /**
             * Obtiene el valor de la propiedad ttIcode.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTTIcode() {
                return ttIcode;
            }

            /**
             * Define el valor de la propiedad ttIcode.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTTIcode(BigInteger value) {
                this.ttIcode = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ReviewerInfo" type="{http://www.opentravel.org/OTA/2003/05}TravelerType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="ResponseGroups" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AnswerGroupsType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ResponseGroup" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResponseGroupType"&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CategoryAggregations" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="CategoryAggregation" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="QuestionCategoryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="SupplierResponse" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="ReviewID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="ResponseLanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
         *       &amp;lt;attribute name="MonthYearOfTravel" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
         *       &amp;lt;attribute name="ReviewDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="ReviewerCompany" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="TripDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *       &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "reviewerInfo",
            "responseGroups",
            "categoryAggregations",
            "multimediaDescriptions",
            "supplierResponse"
        })
        public static class Review {

            @XmlElement(name = "ReviewerInfo")
            protected TravelerType reviewerInfo;
            @XmlElement(name = "ResponseGroups")
            protected ReviewsType.ReviewSubject.Review.ResponseGroups responseGroups;
            @XmlElement(name = "CategoryAggregations")
            protected ReviewsType.ReviewSubject.Review.CategoryAggregations categoryAggregations;
            @XmlElement(name = "MultimediaDescriptions")
            protected MultimediaDescriptionsType multimediaDescriptions;
            @XmlElement(name = "SupplierResponse")
            protected List<ParagraphType> supplierResponse;
            @XmlAttribute(name = "ReviewID")
            protected String reviewID;
            @XmlAttribute(name = "ResponseLanguageCode")
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlSchemaType(name = "language")
            protected String responseLanguageCode;
            @XmlAttribute(name = "MonthYearOfTravel")
            protected String monthYearOfTravel;
            @XmlAttribute(name = "ReviewDate")
            protected String reviewDate;
            @XmlAttribute(name = "ReviewerCompany")
            protected String reviewerCompany;
            @XmlAttribute(name = "TripDuration")
            protected Duration tripDuration;
            @XmlAttribute(name = "AggregateRating")
            protected BigDecimal aggregateRating;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad reviewerInfo.
             * 
             * @return
             *     possible object is
             *     {@link TravelerType }
             *     
             */
            public TravelerType getReviewerInfo() {
                return reviewerInfo;
            }

            /**
             * Define el valor de la propiedad reviewerInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link TravelerType }
             *     
             */
            public void setReviewerInfo(TravelerType value) {
                this.reviewerInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad responseGroups.
             * 
             * @return
             *     possible object is
             *     {@link ReviewsType.ReviewSubject.Review.ResponseGroups }
             *     
             */
            public ReviewsType.ReviewSubject.Review.ResponseGroups getResponseGroups() {
                return responseGroups;
            }

            /**
             * Define el valor de la propiedad responseGroups.
             * 
             * @param value
             *     allowed object is
             *     {@link ReviewsType.ReviewSubject.Review.ResponseGroups }
             *     
             */
            public void setResponseGroups(ReviewsType.ReviewSubject.Review.ResponseGroups value) {
                this.responseGroups = value;
            }

            /**
             * Obtiene el valor de la propiedad categoryAggregations.
             * 
             * @return
             *     possible object is
             *     {@link ReviewsType.ReviewSubject.Review.CategoryAggregations }
             *     
             */
            public ReviewsType.ReviewSubject.Review.CategoryAggregations getCategoryAggregations() {
                return categoryAggregations;
            }

            /**
             * Define el valor de la propiedad categoryAggregations.
             * 
             * @param value
             *     allowed object is
             *     {@link ReviewsType.ReviewSubject.Review.CategoryAggregations }
             *     
             */
            public void setCategoryAggregations(ReviewsType.ReviewSubject.Review.CategoryAggregations value) {
                this.categoryAggregations = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaDescriptions.
             * 
             * @return
             *     possible object is
             *     {@link MultimediaDescriptionsType }
             *     
             */
            public MultimediaDescriptionsType getMultimediaDescriptions() {
                return multimediaDescriptions;
            }

            /**
             * Define el valor de la propiedad multimediaDescriptions.
             * 
             * @param value
             *     allowed object is
             *     {@link MultimediaDescriptionsType }
             *     
             */
            public void setMultimediaDescriptions(MultimediaDescriptionsType value) {
                this.multimediaDescriptions = value;
            }

            /**
             * Gets the value of the supplierResponse property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplierResponse property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSupplierResponse().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getSupplierResponse() {
                if (supplierResponse == null) {
                    supplierResponse = new ArrayList<ParagraphType>();
                }
                return this.supplierResponse;
            }

            /**
             * Obtiene el valor de la propiedad reviewID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReviewID() {
                return reviewID;
            }

            /**
             * Define el valor de la propiedad reviewID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReviewID(String value) {
                this.reviewID = value;
            }

            /**
             * Obtiene el valor de la propiedad responseLanguageCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResponseLanguageCode() {
                return responseLanguageCode;
            }

            /**
             * Define el valor de la propiedad responseLanguageCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResponseLanguageCode(String value) {
                this.responseLanguageCode = value;
            }

            /**
             * Obtiene el valor de la propiedad monthYearOfTravel.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMonthYearOfTravel() {
                return monthYearOfTravel;
            }

            /**
             * Define el valor de la propiedad monthYearOfTravel.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMonthYearOfTravel(String value) {
                this.monthYearOfTravel = value;
            }

            /**
             * Obtiene el valor de la propiedad reviewDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReviewDate() {
                return reviewDate;
            }

            /**
             * Define el valor de la propiedad reviewDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReviewDate(String value) {
                this.reviewDate = value;
            }

            /**
             * Obtiene el valor de la propiedad reviewerCompany.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReviewerCompany() {
                return reviewerCompany;
            }

            /**
             * Define el valor de la propiedad reviewerCompany.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReviewerCompany(String value) {
                this.reviewerCompany = value;
            }

            /**
             * Obtiene el valor de la propiedad tripDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getTripDuration() {
                return tripDuration;
            }

            /**
             * Define el valor de la propiedad tripDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setTripDuration(Duration value) {
                this.tripDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad aggregateRating.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAggregateRating() {
                return aggregateRating;
            }

            /**
             * Define el valor de la propiedad aggregateRating.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAggregateRating(BigDecimal value) {
                this.aggregateRating = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="CategoryAggregation" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="QuestionCategoryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "categoryAggregation"
            })
            public static class CategoryAggregations {

                @XmlElement(name = "CategoryAggregation", required = true)
                protected List<ReviewsType.ReviewSubject.Review.CategoryAggregations.CategoryAggregation> categoryAggregation;

                /**
                 * Gets the value of the categoryAggregation property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the categoryAggregation property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getCategoryAggregation().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ReviewsType.ReviewSubject.Review.CategoryAggregations.CategoryAggregation }
                 * 
                 * 
                 */
                public List<ReviewsType.ReviewSubject.Review.CategoryAggregations.CategoryAggregation> getCategoryAggregation() {
                    if (categoryAggregation == null) {
                        categoryAggregation = new ArrayList<ReviewsType.ReviewSubject.Review.CategoryAggregations.CategoryAggregation>();
                    }
                    return this.categoryAggregation;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="QuestionCategoryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class CategoryAggregation {

                    @XmlAttribute(name = "QuestionCategoryRPH")
                    protected String questionCategoryRPH;
                    @XmlAttribute(name = "AggregateRating")
                    protected BigDecimal aggregateRating;

                    /**
                     * Obtiene el valor de la propiedad questionCategoryRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getQuestionCategoryRPH() {
                        return questionCategoryRPH;
                    }

                    /**
                     * Define el valor de la propiedad questionCategoryRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setQuestionCategoryRPH(String value) {
                        this.questionCategoryRPH = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad aggregateRating.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getAggregateRating() {
                        return aggregateRating;
                    }

                    /**
                     * Define el valor de la propiedad aggregateRating.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setAggregateRating(BigDecimal value) {
                        this.aggregateRating = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AnswerGroupsType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ResponseGroup" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResponseGroupType"&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "responseGroup"
            })
            public static class ResponseGroups
                extends AnswerGroupsType
            {

                @XmlElement(name = "ResponseGroup", required = true)
                protected List<ReviewsType.ReviewSubject.Review.ResponseGroups.ResponseGroup> responseGroup;

                /**
                 * Gets the value of the responseGroup property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the responseGroup property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getResponseGroup().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ReviewsType.ReviewSubject.Review.ResponseGroups.ResponseGroup }
                 * 
                 * 
                 */
                public List<ReviewsType.ReviewSubject.Review.ResponseGroups.ResponseGroup> getResponseGroup() {
                    if (responseGroup == null) {
                        responseGroup = new ArrayList<ReviewsType.ReviewSubject.Review.ResponseGroups.ResponseGroup>();
                    }
                    return this.responseGroup;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResponseGroupType"&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ResponseGroup
                    extends ResponseGroupType
                {


                }

            }

        }

    }

}
