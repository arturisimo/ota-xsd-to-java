
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RatePlans"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RatePlan" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="DateRange" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="DestinationSystemsCode" type="{http://www.opentravel.org/OTA/2003/05}DestinationSystemCodesType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="RatePlanCandidates" type="{http://www.opentravel.org/OTA/2003/05}RatePlanCandidatesType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Offers" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Offer" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="OfferCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="HotelRef" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="SegmentCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="PropertyClassCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="ArchitecturalStyleCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="SupplierIntegrationLevel" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MaxResponsesGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attribute name="SummaryOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="SortOrder"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}AlphaLength1"&amp;gt;
 *             &amp;lt;enumeration value="A"/&amp;gt;
 *             &amp;lt;enumeration value="D"/&amp;gt;
 *             &amp;lt;enumeration value="N"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="RequestedCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *       &amp;lt;attribute name="SearchCacheLevel"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="Live"/&amp;gt;
 *             &amp;lt;enumeration value="VeryRecent"/&amp;gt;
 *             &amp;lt;enumeration value="LessRecent"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="MaximumWaitTime" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *       &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "ratePlans"
})
@XmlRootElement(name = "OTA_HotelRatePlanRQ")
public class OTAHotelRatePlanRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "RatePlans", required = true)
    protected OTAHotelRatePlanRQ.RatePlans ratePlans;
    @XmlAttribute(name = "SummaryOnly")
    protected Boolean summaryOnly;
    @XmlAttribute(name = "SortOrder")
    protected String sortOrder;
    @XmlAttribute(name = "RequestedCurrency")
    protected String requestedCurrency;
    @XmlAttribute(name = "SearchCacheLevel")
    protected String searchCacheLevel;
    @XmlAttribute(name = "MaximumWaitTime")
    protected BigDecimal maximumWaitTime;
    @XmlAttribute(name = "MoreDataEchoToken")
    protected String moreDataEchoToken;
    @XmlAttribute(name = "InfoSource")
    protected String infoSource;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad ratePlans.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelRatePlanRQ.RatePlans }
     *     
     */
    public OTAHotelRatePlanRQ.RatePlans getRatePlans() {
        return ratePlans;
    }

    /**
     * Define el valor de la propiedad ratePlans.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelRatePlanRQ.RatePlans }
     *     
     */
    public void setRatePlans(OTAHotelRatePlanRQ.RatePlans value) {
        this.ratePlans = value;
    }

    /**
     * Obtiene el valor de la propiedad summaryOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSummaryOnly() {
        return summaryOnly;
    }

    /**
     * Define el valor de la propiedad summaryOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSummaryOnly(Boolean value) {
        this.summaryOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad sortOrder.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * Define el valor de la propiedad sortOrder.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortOrder(String value) {
        this.sortOrder = value;
    }

    /**
     * Obtiene el valor de la propiedad requestedCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestedCurrency() {
        return requestedCurrency;
    }

    /**
     * Define el valor de la propiedad requestedCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestedCurrency(String value) {
        this.requestedCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad searchCacheLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchCacheLevel() {
        return searchCacheLevel;
    }

    /**
     * Define el valor de la propiedad searchCacheLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchCacheLevel(String value) {
        this.searchCacheLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumWaitTime.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumWaitTime() {
        return maximumWaitTime;
    }

    /**
     * Define el valor de la propiedad maximumWaitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumWaitTime(BigDecimal value) {
        this.maximumWaitTime = value;
    }

    /**
     * Obtiene el valor de la propiedad moreDataEchoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreDataEchoToken() {
        return moreDataEchoToken;
    }

    /**
     * Define el valor de la propiedad moreDataEchoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreDataEchoToken(String value) {
        this.moreDataEchoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad infoSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoSource() {
        return infoSource;
    }

    /**
     * Define el valor de la propiedad infoSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoSource(String value) {
        this.infoSource = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RatePlan" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="DateRange" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="DestinationSystemsCode" type="{http://www.opentravel.org/OTA/2003/05}DestinationSystemCodesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RatePlanCandidates" type="{http://www.opentravel.org/OTA/2003/05}RatePlanCandidatesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Offers" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Offer" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="OfferCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="HotelRef" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *                           &amp;lt;attribute name="SegmentCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="PropertyClassCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="ArchitecturalStyleCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="SupplierIntegrationLevel" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ratePlan"
    })
    public static class RatePlans {

        @XmlElement(name = "RatePlan", required = true)
        protected List<OTAHotelRatePlanRQ.RatePlans.RatePlan> ratePlan;

        /**
         * Gets the value of the ratePlan property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ratePlan property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRatePlan().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelRatePlanRQ.RatePlans.RatePlan }
         * 
         * 
         */
        public List<OTAHotelRatePlanRQ.RatePlans.RatePlan> getRatePlan() {
            if (ratePlan == null) {
                ratePlan = new ArrayList<OTAHotelRatePlanRQ.RatePlans.RatePlan>();
            }
            return this.ratePlan;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="DateRange" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="DestinationSystemsCode" type="{http://www.opentravel.org/OTA/2003/05}DestinationSystemCodesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RatePlanCandidates" type="{http://www.opentravel.org/OTA/2003/05}RatePlanCandidatesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Offers" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Offer" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="OfferCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="HotelRef" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *                 &amp;lt;attribute name="SegmentCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="PropertyClassCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="ArchitecturalStyleCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="SupplierIntegrationLevel" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "dateRange",
            "destinationSystemsCode",
            "ratePlanCandidates",
            "offers",
            "hotelRef",
            "tpaExtensions"
        })
        public static class RatePlan {

            @XmlElement(name = "DateRange")
            protected OTAHotelRatePlanRQ.RatePlans.RatePlan.DateRange dateRange;
            @XmlElement(name = "DestinationSystemsCode")
            protected DestinationSystemCodesType destinationSystemsCode;
            @XmlElement(name = "RatePlanCandidates")
            protected RatePlanCandidatesType ratePlanCandidates;
            @XmlElement(name = "Offers")
            protected OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers offers;
            @XmlElement(name = "HotelRef")
            protected OTAHotelRatePlanRQ.RatePlans.RatePlan.HotelRef hotelRef;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;

            /**
             * Obtiene el valor de la propiedad dateRange.
             * 
             * @return
             *     possible object is
             *     {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.DateRange }
             *     
             */
            public OTAHotelRatePlanRQ.RatePlans.RatePlan.DateRange getDateRange() {
                return dateRange;
            }

            /**
             * Define el valor de la propiedad dateRange.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.DateRange }
             *     
             */
            public void setDateRange(OTAHotelRatePlanRQ.RatePlans.RatePlan.DateRange value) {
                this.dateRange = value;
            }

            /**
             * Obtiene el valor de la propiedad destinationSystemsCode.
             * 
             * @return
             *     possible object is
             *     {@link DestinationSystemCodesType }
             *     
             */
            public DestinationSystemCodesType getDestinationSystemsCode() {
                return destinationSystemsCode;
            }

            /**
             * Define el valor de la propiedad destinationSystemsCode.
             * 
             * @param value
             *     allowed object is
             *     {@link DestinationSystemCodesType }
             *     
             */
            public void setDestinationSystemsCode(DestinationSystemCodesType value) {
                this.destinationSystemsCode = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanCandidates.
             * 
             * @return
             *     possible object is
             *     {@link RatePlanCandidatesType }
             *     
             */
            public RatePlanCandidatesType getRatePlanCandidates() {
                return ratePlanCandidates;
            }

            /**
             * Define el valor de la propiedad ratePlanCandidates.
             * 
             * @param value
             *     allowed object is
             *     {@link RatePlanCandidatesType }
             *     
             */
            public void setRatePlanCandidates(RatePlanCandidatesType value) {
                this.ratePlanCandidates = value;
            }

            /**
             * Obtiene el valor de la propiedad offers.
             * 
             * @return
             *     possible object is
             *     {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers }
             *     
             */
            public OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers getOffers() {
                return offers;
            }

            /**
             * Define el valor de la propiedad offers.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers }
             *     
             */
            public void setOffers(OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers value) {
                this.offers = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelRef.
             * 
             * @return
             *     possible object is
             *     {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.HotelRef }
             *     
             */
            public OTAHotelRatePlanRQ.RatePlans.RatePlan.HotelRef getHotelRef() {
                return hotelRef;
            }

            /**
             * Define el valor de la propiedad hotelRef.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.HotelRef }
             *     
             */
            public void setHotelRef(OTAHotelRatePlanRQ.RatePlans.RatePlan.HotelRef value) {
                this.hotelRef = value;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DateRange {

                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
             *       &amp;lt;attribute name="SegmentCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="PropertyClassCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="ArchitecturalStyleCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="SupplierIntegrationLevel" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class HotelRef {

                @XmlAttribute(name = "SegmentCategoryCode")
                protected String segmentCategoryCode;
                @XmlAttribute(name = "PropertyClassCode")
                protected String propertyClassCode;
                @XmlAttribute(name = "ArchitecturalStyleCode")
                protected String architecturalStyleCode;
                @XmlAttribute(name = "SupplierIntegrationLevel")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger supplierIntegrationLevel;
                @XmlAttribute(name = "ChainCode")
                protected String chainCode;
                @XmlAttribute(name = "BrandCode")
                protected String brandCode;
                @XmlAttribute(name = "HotelCode")
                protected String hotelCode;
                @XmlAttribute(name = "HotelCityCode")
                protected String hotelCityCode;
                @XmlAttribute(name = "HotelName")
                protected String hotelName;
                @XmlAttribute(name = "HotelCodeContext")
                protected String hotelCodeContext;
                @XmlAttribute(name = "ChainName")
                protected String chainName;
                @XmlAttribute(name = "BrandName")
                protected String brandName;
                @XmlAttribute(name = "AreaID")
                protected String areaID;
                @XmlAttribute(name = "TTIcode")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger ttIcode;

                /**
                 * Obtiene el valor de la propiedad segmentCategoryCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSegmentCategoryCode() {
                    return segmentCategoryCode;
                }

                /**
                 * Define el valor de la propiedad segmentCategoryCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSegmentCategoryCode(String value) {
                    this.segmentCategoryCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad propertyClassCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPropertyClassCode() {
                    return propertyClassCode;
                }

                /**
                 * Define el valor de la propiedad propertyClassCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPropertyClassCode(String value) {
                    this.propertyClassCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad architecturalStyleCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getArchitecturalStyleCode() {
                    return architecturalStyleCode;
                }

                /**
                 * Define el valor de la propiedad architecturalStyleCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setArchitecturalStyleCode(String value) {
                    this.architecturalStyleCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad supplierIntegrationLevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getSupplierIntegrationLevel() {
                    return supplierIntegrationLevel;
                }

                /**
                 * Define el valor de la propiedad supplierIntegrationLevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setSupplierIntegrationLevel(BigInteger value) {
                    this.supplierIntegrationLevel = value;
                }

                /**
                 * Obtiene el valor de la propiedad chainCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getChainCode() {
                    return chainCode;
                }

                /**
                 * Define el valor de la propiedad chainCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setChainCode(String value) {
                    this.chainCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad brandCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBrandCode() {
                    return brandCode;
                }

                /**
                 * Define el valor de la propiedad brandCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBrandCode(String value) {
                    this.brandCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelCode() {
                    return hotelCode;
                }

                /**
                 * Define el valor de la propiedad hotelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelCode(String value) {
                    this.hotelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelCityCode() {
                    return hotelCityCode;
                }

                /**
                 * Define el valor de la propiedad hotelCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelCityCode(String value) {
                    this.hotelCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelName() {
                    return hotelName;
                }

                /**
                 * Define el valor de la propiedad hotelName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelName(String value) {
                    this.hotelName = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelCodeContext() {
                    return hotelCodeContext;
                }

                /**
                 * Define el valor de la propiedad hotelCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelCodeContext(String value) {
                    this.hotelCodeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad chainName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getChainName() {
                    return chainName;
                }

                /**
                 * Define el valor de la propiedad chainName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setChainName(String value) {
                    this.chainName = value;
                }

                /**
                 * Obtiene el valor de la propiedad brandName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBrandName() {
                    return brandName;
                }

                /**
                 * Define el valor de la propiedad brandName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBrandName(String value) {
                    this.brandName = value;
                }

                /**
                 * Obtiene el valor de la propiedad areaID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAreaID() {
                    return areaID;
                }

                /**
                 * Define el valor de la propiedad areaID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAreaID(String value) {
                    this.areaID = value;
                }

                /**
                 * Obtiene el valor de la propiedad ttIcode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTTIcode() {
                    return ttIcode;
                }

                /**
                 * Define el valor de la propiedad ttIcode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTTIcode(BigInteger value) {
                    this.ttIcode = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Offer" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="OfferCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offer"
            })
            public static class Offers {

                @XmlElement(name = "Offer", required = true)
                protected List<OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers.Offer> offer;
                @XmlAttribute(name = "SendData")
                protected Boolean sendData;

                /**
                 * Gets the value of the offer property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the offer property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOffer().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers.Offer }
                 * 
                 * 
                 */
                public List<OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers.Offer> getOffer() {
                    if (offer == null) {
                        offer = new ArrayList<OTAHotelRatePlanRQ.RatePlans.RatePlan.Offers.Offer>();
                    }
                    return this.offer;
                }

                /**
                 * Obtiene el valor de la propiedad sendData.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isSendData() {
                    return sendData;
                }

                /**
                 * Define el valor de la propiedad sendData.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setSendData(Boolean value) {
                    this.sendData = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="OfferCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Offer {

                    @XmlAttribute(name = "OfferCode")
                    protected String offerCode;

                    /**
                     * Obtiene el valor de la propiedad offerCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOfferCode() {
                        return offerCode;
                    }

                    /**
                     * Define el valor de la propiedad offerCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOfferCode(String value) {
                        this.offerCode = value;
                    }

                }

            }

        }

    }

}
