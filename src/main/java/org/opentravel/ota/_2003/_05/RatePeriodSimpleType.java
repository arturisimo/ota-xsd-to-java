
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para RatePeriodSimpleType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="RatePeriodSimpleType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Hourly"/&amp;gt;
 *     &amp;lt;enumeration value="Daily"/&amp;gt;
 *     &amp;lt;enumeration value="Weekly"/&amp;gt;
 *     &amp;lt;enumeration value="Monthly"/&amp;gt;
 *     &amp;lt;enumeration value="WeekendDay"/&amp;gt;
 *     &amp;lt;enumeration value="Other"/&amp;gt;
 *     &amp;lt;enumeration value="Package"/&amp;gt;
 *     &amp;lt;enumeration value="Bundle"/&amp;gt;
 *     &amp;lt;enumeration value="Total"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "RatePeriodSimpleType")
@XmlEnum
public enum RatePeriodSimpleType {

    @XmlEnumValue("Hourly")
    HOURLY("Hourly"),
    @XmlEnumValue("Daily")
    DAILY("Daily"),
    @XmlEnumValue("Weekly")
    WEEKLY("Weekly"),
    @XmlEnumValue("Monthly")
    MONTHLY("Monthly"),
    @XmlEnumValue("WeekendDay")
    WEEKEND_DAY("WeekendDay"),
    @XmlEnumValue("Other")
    OTHER("Other"),

    /**
     * The rate period is based on the package.
     * 
     */
    @XmlEnumValue("Package")
    PACKAGE("Package"),

    /**
     * The rate is the same regardless of the number of days the vehicle is rented.
     * 
     */
    @XmlEnumValue("Bundle")
    BUNDLE("Bundle"),

    /**
     * The rate is the total, no specific rate period.
     * 
     */
    @XmlEnumValue("Total")
    TOTAL("Total");
    private final String value;

    RatePeriodSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RatePeriodSimpleType fromValue(String v) {
        for (RatePeriodSimpleType c: RatePeriodSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
