
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The Vehicle Location Services Offered Type is used to define information on the services offered at the rental location. Services can be either on-location or off-location, and may include the rental of special equipment.
 * 
 * &lt;p&gt;Clase Java para VehicleLocationServicesOfferedType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleLocationServicesOfferedType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="OnLocationServices" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="OnLocationService" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="OnLocServiceDesc" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="OperationSchedules" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="OnLocServiceCharges" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="OnLocServiceCharge" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OnLocationServiceID_Type" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OffLocationServices" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="OffLocationService" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="OffLocServiceDesc" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="OperationSchedules" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="OffLocServiceCharges" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="OffLocServiceCharge" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OffLocationServiceID_Type" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SpecialEquipments" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SpecialEquipment" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="EquipDesc" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="EquipCharges" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="EquipCharge" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleLocationServicesOfferedType", propOrder = {
    "onLocationServices",
    "offLocationServices",
    "specialEquipments",
    "tpaExtensions"
})
public class VehicleLocationServicesOfferedType {

    @XmlElement(name = "OnLocationServices")
    protected VehicleLocationServicesOfferedType.OnLocationServices onLocationServices;
    @XmlElement(name = "OffLocationServices")
    protected VehicleLocationServicesOfferedType.OffLocationServices offLocationServices;
    @XmlElement(name = "SpecialEquipments")
    protected VehicleLocationServicesOfferedType.SpecialEquipments specialEquipments;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Obtiene el valor de la propiedad onLocationServices.
     * 
     * @return
     *     possible object is
     *     {@link VehicleLocationServicesOfferedType.OnLocationServices }
     *     
     */
    public VehicleLocationServicesOfferedType.OnLocationServices getOnLocationServices() {
        return onLocationServices;
    }

    /**
     * Define el valor de la propiedad onLocationServices.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleLocationServicesOfferedType.OnLocationServices }
     *     
     */
    public void setOnLocationServices(VehicleLocationServicesOfferedType.OnLocationServices value) {
        this.onLocationServices = value;
    }

    /**
     * Obtiene el valor de la propiedad offLocationServices.
     * 
     * @return
     *     possible object is
     *     {@link VehicleLocationServicesOfferedType.OffLocationServices }
     *     
     */
    public VehicleLocationServicesOfferedType.OffLocationServices getOffLocationServices() {
        return offLocationServices;
    }

    /**
     * Define el valor de la propiedad offLocationServices.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleLocationServicesOfferedType.OffLocationServices }
     *     
     */
    public void setOffLocationServices(VehicleLocationServicesOfferedType.OffLocationServices value) {
        this.offLocationServices = value;
    }

    /**
     * Obtiene el valor de la propiedad specialEquipments.
     * 
     * @return
     *     possible object is
     *     {@link VehicleLocationServicesOfferedType.SpecialEquipments }
     *     
     */
    public VehicleLocationServicesOfferedType.SpecialEquipments getSpecialEquipments() {
        return specialEquipments;
    }

    /**
     * Define el valor de la propiedad specialEquipments.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleLocationServicesOfferedType.SpecialEquipments }
     *     
     */
    public void setSpecialEquipments(VehicleLocationServicesOfferedType.SpecialEquipments value) {
        this.specialEquipments = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OffLocationService" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OffLocServiceDesc" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="OperationSchedules" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="OffLocServiceCharges" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="OffLocServiceCharge" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OffLocationServiceID_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offLocationService"
    })
    public static class OffLocationServices {

        @XmlElement(name = "OffLocationService", required = true)
        protected List<VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService> offLocationService;

        /**
         * Gets the value of the offLocationService property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the offLocationService property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOffLocationService().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService }
         * 
         * 
         */
        public List<VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService> getOffLocationService() {
            if (offLocationService == null) {
                offLocationService = new ArrayList<VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService>();
            }
            return this.offLocationService;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OffLocServiceDesc" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="OperationSchedules" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="OffLocServiceCharges" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="OffLocServiceCharge" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OffLocationServiceID_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "offLocServiceDesc",
            "operationSchedules",
            "offLocServiceCharges"
        })
        public static class OffLocationService {

            @XmlElement(name = "OffLocServiceDesc")
            protected VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceDesc offLocServiceDesc;
            @XmlElement(name = "OperationSchedules")
            protected OperationSchedulesType operationSchedules;
            @XmlElement(name = "OffLocServiceCharges")
            protected VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges offLocServiceCharges;
            @XmlAttribute(name = "Type", required = true)
            protected OffLocationServiceIDType type;

            /**
             * Obtiene el valor de la propiedad offLocServiceDesc.
             * 
             * @return
             *     possible object is
             *     {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceDesc }
             *     
             */
            public VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceDesc getOffLocServiceDesc() {
                return offLocServiceDesc;
            }

            /**
             * Define el valor de la propiedad offLocServiceDesc.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceDesc }
             *     
             */
            public void setOffLocServiceDesc(VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceDesc value) {
                this.offLocServiceDesc = value;
            }

            /**
             * Obtiene el valor de la propiedad operationSchedules.
             * 
             * @return
             *     possible object is
             *     {@link OperationSchedulesType }
             *     
             */
            public OperationSchedulesType getOperationSchedules() {
                return operationSchedules;
            }

            /**
             * Define el valor de la propiedad operationSchedules.
             * 
             * @param value
             *     allowed object is
             *     {@link OperationSchedulesType }
             *     
             */
            public void setOperationSchedules(OperationSchedulesType value) {
                this.operationSchedules = value;
            }

            /**
             * Obtiene el valor de la propiedad offLocServiceCharges.
             * 
             * @return
             *     possible object is
             *     {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges }
             *     
             */
            public VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges getOffLocServiceCharges() {
                return offLocServiceCharges;
            }

            /**
             * Define el valor de la propiedad offLocServiceCharges.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges }
             *     
             */
            public void setOffLocServiceCharges(VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges value) {
                this.offLocServiceCharges = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link OffLocationServiceIDType }
             *     
             */
            public OffLocationServiceIDType getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link OffLocationServiceIDType }
             *     
             */
            public void setType(OffLocationServiceIDType value) {
                this.type = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="OffLocServiceCharge" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offLocServiceCharge"
            })
            public static class OffLocServiceCharges {

                @XmlElement(name = "OffLocServiceCharge", required = true)
                protected List<VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge> offLocServiceCharge;

                /**
                 * Gets the value of the offLocServiceCharge property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the offLocServiceCharge property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOffLocServiceCharge().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge }
                 * 
                 * 
                 */
                public List<VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge> getOffLocServiceCharge() {
                    if (offLocServiceCharge == null) {
                        offLocServiceCharge = new ArrayList<VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge>();
                    }
                    return this.offLocServiceCharge;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "vehicles"
                })
                public static class OffLocServiceCharge
                    extends VehicleChargeType
                {

                    @XmlElement(name = "Vehicles")
                    protected VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge.Vehicles vehicles;

                    /**
                     * Obtiene el valor de la propiedad vehicles.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge.Vehicles }
                     *     
                     */
                    public VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge.Vehicles getVehicles() {
                        return vehicles;
                    }

                    /**
                     * Define el valor de la propiedad vehicles.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge.Vehicles }
                     *     
                     */
                    public void setVehicles(VehicleLocationServicesOfferedType.OffLocationServices.OffLocationService.OffLocServiceCharges.OffLocServiceCharge.Vehicles value) {
                        this.vehicles = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "vehicle"
                    })
                    public static class Vehicles {

                        @XmlElement(name = "Vehicle", required = true)
                        protected List<VehicleCoreType> vehicle;

                        /**
                         * Gets the value of the vehicle property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getVehicle().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link VehicleCoreType }
                         * 
                         * 
                         */
                        public List<VehicleCoreType> getVehicle() {
                            if (vehicle == null) {
                                vehicle = new ArrayList<VehicleCoreType>();
                            }
                            return this.vehicle;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OffLocServiceDesc
                extends FormattedTextType
            {


            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OnLocationService" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OnLocServiceDesc" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="OperationSchedules" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="OnLocServiceCharges" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="OnLocServiceCharge" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OnLocationServiceID_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "onLocationService"
    })
    public static class OnLocationServices {

        @XmlElement(name = "OnLocationService", required = true)
        protected List<VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService> onLocationService;

        /**
         * Gets the value of the onLocationService property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the onLocationService property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOnLocationService().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService }
         * 
         * 
         */
        public List<VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService> getOnLocationService() {
            if (onLocationService == null) {
                onLocationService = new ArrayList<VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService>();
            }
            return this.onLocationService;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OnLocServiceDesc" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="OperationSchedules" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="OnLocServiceCharges" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="OnLocServiceCharge" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OnLocationServiceID_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "onLocServiceDesc",
            "operationSchedules",
            "onLocServiceCharges"
        })
        public static class OnLocationService {

            @XmlElement(name = "OnLocServiceDesc")
            protected VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceDesc onLocServiceDesc;
            @XmlElement(name = "OperationSchedules")
            protected OperationSchedulesType operationSchedules;
            @XmlElement(name = "OnLocServiceCharges")
            protected VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges onLocServiceCharges;
            @XmlAttribute(name = "Type", required = true)
            protected OnLocationServiceIDType type;

            /**
             * Obtiene el valor de la propiedad onLocServiceDesc.
             * 
             * @return
             *     possible object is
             *     {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceDesc }
             *     
             */
            public VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceDesc getOnLocServiceDesc() {
                return onLocServiceDesc;
            }

            /**
             * Define el valor de la propiedad onLocServiceDesc.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceDesc }
             *     
             */
            public void setOnLocServiceDesc(VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceDesc value) {
                this.onLocServiceDesc = value;
            }

            /**
             * Obtiene el valor de la propiedad operationSchedules.
             * 
             * @return
             *     possible object is
             *     {@link OperationSchedulesType }
             *     
             */
            public OperationSchedulesType getOperationSchedules() {
                return operationSchedules;
            }

            /**
             * Define el valor de la propiedad operationSchedules.
             * 
             * @param value
             *     allowed object is
             *     {@link OperationSchedulesType }
             *     
             */
            public void setOperationSchedules(OperationSchedulesType value) {
                this.operationSchedules = value;
            }

            /**
             * Obtiene el valor de la propiedad onLocServiceCharges.
             * 
             * @return
             *     possible object is
             *     {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges }
             *     
             */
            public VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges getOnLocServiceCharges() {
                return onLocServiceCharges;
            }

            /**
             * Define el valor de la propiedad onLocServiceCharges.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges }
             *     
             */
            public void setOnLocServiceCharges(VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges value) {
                this.onLocServiceCharges = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link OnLocationServiceIDType }
             *     
             */
            public OnLocationServiceIDType getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link OnLocationServiceIDType }
             *     
             */
            public void setType(OnLocationServiceIDType value) {
                this.type = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="OnLocServiceCharge" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "onLocServiceCharge"
            })
            public static class OnLocServiceCharges {

                @XmlElement(name = "OnLocServiceCharge", required = true)
                protected List<VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge> onLocServiceCharge;

                /**
                 * Gets the value of the onLocServiceCharge property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the onLocServiceCharge property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOnLocServiceCharge().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge }
                 * 
                 * 
                 */
                public List<VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge> getOnLocServiceCharge() {
                    if (onLocServiceCharge == null) {
                        onLocServiceCharge = new ArrayList<VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge>();
                    }
                    return this.onLocServiceCharge;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "vehicles"
                })
                public static class OnLocServiceCharge
                    extends VehicleChargeType
                {

                    @XmlElement(name = "Vehicles")
                    protected VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge.Vehicles vehicles;

                    /**
                     * Obtiene el valor de la propiedad vehicles.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge.Vehicles }
                     *     
                     */
                    public VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge.Vehicles getVehicles() {
                        return vehicles;
                    }

                    /**
                     * Define el valor de la propiedad vehicles.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge.Vehicles }
                     *     
                     */
                    public void setVehicles(VehicleLocationServicesOfferedType.OnLocationServices.OnLocationService.OnLocServiceCharges.OnLocServiceCharge.Vehicles value) {
                        this.vehicles = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "vehicle"
                    })
                    public static class Vehicles {

                        @XmlElement(name = "Vehicle", required = true)
                        protected List<VehicleCoreType> vehicle;

                        /**
                         * Gets the value of the vehicle property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getVehicle().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link VehicleCoreType }
                         * 
                         * 
                         */
                        public List<VehicleCoreType> getVehicle() {
                            if (vehicle == null) {
                                vehicle = new ArrayList<VehicleCoreType>();
                            }
                            return this.vehicle;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OnLocServiceDesc
                extends FormattedTextType
            {


            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SpecialEquipment" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="EquipDesc" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="EquipCharges" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="EquipCharge" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "specialEquipment"
    })
    public static class SpecialEquipments {

        @XmlElement(name = "SpecialEquipment", required = true)
        protected List<VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment> specialEquipment;

        /**
         * Gets the value of the specialEquipment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specialEquipment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSpecialEquipment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment }
         * 
         * 
         */
        public List<VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment> getSpecialEquipment() {
            if (specialEquipment == null) {
                specialEquipment = new ArrayList<VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment>();
            }
            return this.specialEquipment;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="EquipDesc" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="EquipCharges" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="EquipCharge" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "equipDesc",
            "equipCharges"
        })
        public static class SpecialEquipment {

            @XmlElement(name = "EquipDesc")
            protected FormattedTextType equipDesc;
            @XmlElement(name = "EquipCharges")
            protected VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges equipCharges;
            @XmlAttribute(name = "Type", required = true)
            protected String type;

            /**
             * Obtiene el valor de la propiedad equipDesc.
             * 
             * @return
             *     possible object is
             *     {@link FormattedTextType }
             *     
             */
            public FormattedTextType getEquipDesc() {
                return equipDesc;
            }

            /**
             * Define el valor de la propiedad equipDesc.
             * 
             * @param value
             *     allowed object is
             *     {@link FormattedTextType }
             *     
             */
            public void setEquipDesc(FormattedTextType value) {
                this.equipDesc = value;
            }

            /**
             * Obtiene el valor de la propiedad equipCharges.
             * 
             * @return
             *     possible object is
             *     {@link VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges }
             *     
             */
            public VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges getEquipCharges() {
                return equipCharges;
            }

            /**
             * Define el valor de la propiedad equipCharges.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges }
             *     
             */
            public void setEquipCharges(VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges value) {
                this.equipCharges = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="EquipCharge" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "equipCharge"
            })
            public static class EquipCharges {

                @XmlElement(name = "EquipCharge", required = true)
                protected List<VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge> equipCharge;

                /**
                 * Gets the value of the equipCharge property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the equipCharge property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getEquipCharge().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge }
                 * 
                 * 
                 */
                public List<VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge> getEquipCharge() {
                    if (equipCharge == null) {
                        equipCharge = new ArrayList<VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge>();
                    }
                    return this.equipCharge;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "vehicles"
                })
                public static class EquipCharge
                    extends VehicleChargeType
                {

                    @XmlElement(name = "Vehicles")
                    protected VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge.Vehicles vehicles;

                    /**
                     * Obtiene el valor de la propiedad vehicles.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge.Vehicles }
                     *     
                     */
                    public VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge.Vehicles getVehicles() {
                        return vehicles;
                    }

                    /**
                     * Define el valor de la propiedad vehicles.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge.Vehicles }
                     *     
                     */
                    public void setVehicles(VehicleLocationServicesOfferedType.SpecialEquipments.SpecialEquipment.EquipCharges.EquipCharge.Vehicles value) {
                        this.vehicles = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" maxOccurs="99"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "vehicle"
                    })
                    public static class Vehicles {

                        @XmlElement(name = "Vehicle", required = true)
                        protected List<VehicleCoreType> vehicle;

                        /**
                         * Gets the value of the vehicle property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getVehicle().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link VehicleCoreType }
                         * 
                         * 
                         */
                        public List<VehicleCoreType> getVehicle() {
                            if (vehicle == null) {
                                vehicle = new ArrayList<VehicleCoreType>();
                            }
                            return this.vehicle;
                        }

                    }

                }

            }

        }

    }

}
