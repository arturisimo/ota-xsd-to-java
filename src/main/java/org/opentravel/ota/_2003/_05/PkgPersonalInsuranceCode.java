
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PkgPersonalInsuranceCode.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="PkgPersonalInsuranceCode"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Ski"/&amp;gt;
 *     &amp;lt;enumeration value="Worldwide"/&amp;gt;
 *     &amp;lt;enumeration value="Europe"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "PkgPersonalInsuranceCode")
@XmlEnum
public enum PkgPersonalInsuranceCode {

    @XmlEnumValue("Ski")
    SKI("Ski"),
    @XmlEnumValue("Worldwide")
    WORLDWIDE("Worldwide"),
    @XmlEnumValue("Europe")
    EUROPE("Europe");
    private final String value;

    PkgPersonalInsuranceCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PkgPersonalInsuranceCode fromValue(String v) {
        for (PkgPersonalInsuranceCode c: PkgPersonalInsuranceCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
