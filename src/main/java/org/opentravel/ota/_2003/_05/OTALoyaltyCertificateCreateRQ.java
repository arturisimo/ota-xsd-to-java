
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Customer" maxOccurs="999" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                 &amp;lt;attribute name="FlightRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TravelInfos"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="TravelInfo" type="{http://www.opentravel.org/OTA/2003/05}LoyaltyTravelInfoType" maxOccurs="99"/&amp;gt;
 *                   &amp;lt;element name="LoyaltyCertificateDetail" maxOccurs="999" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LoyaltyCertificateNumberGroup"/&amp;gt;
 *                           &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="CustomerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="FlightRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="AwardType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="CertificateRefNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "customer",
    "travelInfos"
})
@XmlRootElement(name = "OTA_LoyaltyCertificateCreateRQ")
public class OTALoyaltyCertificateCreateRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "Customer")
    protected List<OTALoyaltyCertificateCreateRQ.Customer> customer;
    @XmlElement(name = "TravelInfos", required = true)
    protected OTALoyaltyCertificateCreateRQ.TravelInfos travelInfos;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the customer property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCustomer().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTALoyaltyCertificateCreateRQ.Customer }
     * 
     * 
     */
    public List<OTALoyaltyCertificateCreateRQ.Customer> getCustomer() {
        if (customer == null) {
            customer = new ArrayList<OTALoyaltyCertificateCreateRQ.Customer>();
        }
        return this.customer;
    }

    /**
     * Obtiene el valor de la propiedad travelInfos.
     * 
     * @return
     *     possible object is
     *     {@link OTALoyaltyCertificateCreateRQ.TravelInfos }
     *     
     */
    public OTALoyaltyCertificateCreateRQ.TravelInfos getTravelInfos() {
        return travelInfos;
    }

    /**
     * Define el valor de la propiedad travelInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTALoyaltyCertificateCreateRQ.TravelInfos }
     *     
     */
    public void setTravelInfos(OTALoyaltyCertificateCreateRQ.TravelInfos value) {
        this.travelInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *       &amp;lt;attribute name="FlightRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Customer
        extends PersonNameType
    {

        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "ResBookDesigCode")
        protected String resBookDesigCode;
        @XmlAttribute(name = "FlightRPH")
        protected String flightRPH;

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad resBookDesigCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResBookDesigCode() {
            return resBookDesigCode;
        }

        /**
         * Define el valor de la propiedad resBookDesigCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResBookDesigCode(String value) {
            this.resBookDesigCode = value;
        }

        /**
         * Obtiene el valor de la propiedad flightRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightRPH() {
            return flightRPH;
        }

        /**
         * Define el valor de la propiedad flightRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightRPH(String value) {
            this.flightRPH = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="TravelInfo" type="{http://www.opentravel.org/OTA/2003/05}LoyaltyTravelInfoType" maxOccurs="99"/&amp;gt;
     *         &amp;lt;element name="LoyaltyCertificateDetail" maxOccurs="999" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LoyaltyCertificateNumberGroup"/&amp;gt;
     *                 &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="CustomerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="FlightRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="AwardType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="CertificateRefNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "travelInfo",
        "loyaltyCertificateDetail"
    })
    public static class TravelInfos {

        @XmlElement(name = "TravelInfo", required = true)
        protected List<LoyaltyTravelInfoType> travelInfo;
        @XmlElement(name = "LoyaltyCertificateDetail")
        protected List<OTALoyaltyCertificateCreateRQ.TravelInfos.LoyaltyCertificateDetail> loyaltyCertificateDetail;

        /**
         * Gets the value of the travelInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTravelInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyTravelInfoType }
         * 
         * 
         */
        public List<LoyaltyTravelInfoType> getTravelInfo() {
            if (travelInfo == null) {
                travelInfo = new ArrayList<LoyaltyTravelInfoType>();
            }
            return this.travelInfo;
        }

        /**
         * Gets the value of the loyaltyCertificateDetail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyaltyCertificateDetail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLoyaltyCertificateDetail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTALoyaltyCertificateCreateRQ.TravelInfos.LoyaltyCertificateDetail }
         * 
         * 
         */
        public List<OTALoyaltyCertificateCreateRQ.TravelInfos.LoyaltyCertificateDetail> getLoyaltyCertificateDetail() {
            if (loyaltyCertificateDetail == null) {
                loyaltyCertificateDetail = new ArrayList<OTALoyaltyCertificateCreateRQ.TravelInfos.LoyaltyCertificateDetail>();
            }
            return this.loyaltyCertificateDetail;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LoyaltyCertificateNumberGroup"/&amp;gt;
         *       &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="CustomerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="FlightRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="AwardType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="CertificateRefNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class LoyaltyCertificateDetail {

            @XmlAttribute(name = "PinNumber")
            protected String pinNumber;
            @XmlAttribute(name = "CustomerRPH")
            protected String customerRPH;
            @XmlAttribute(name = "FlightRPH")
            protected String flightRPH;
            @XmlAttribute(name = "AwardType")
            protected String awardType;
            @XmlAttribute(name = "CertificateRefNumber")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger certificateRefNumber;
            @XmlAttribute(name = "CertificateNumber")
            protected String certificateNumber;
            @XmlAttribute(name = "MemberNumber")
            protected String memberNumber;
            @XmlAttribute(name = "ProgramName")
            protected String programName;

            /**
             * Obtiene el valor de la propiedad pinNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPinNumber() {
                return pinNumber;
            }

            /**
             * Define el valor de la propiedad pinNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPinNumber(String value) {
                this.pinNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad customerRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerRPH() {
                return customerRPH;
            }

            /**
             * Define el valor de la propiedad customerRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerRPH(String value) {
                this.customerRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad flightRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlightRPH() {
                return flightRPH;
            }

            /**
             * Define el valor de la propiedad flightRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlightRPH(String value) {
                this.flightRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad awardType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAwardType() {
                return awardType;
            }

            /**
             * Define el valor de la propiedad awardType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAwardType(String value) {
                this.awardType = value;
            }

            /**
             * Obtiene el valor de la propiedad certificateRefNumber.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCertificateRefNumber() {
                return certificateRefNumber;
            }

            /**
             * Define el valor de la propiedad certificateRefNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCertificateRefNumber(BigInteger value) {
                this.certificateRefNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad certificateNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCertificateNumber() {
                return certificateNumber;
            }

            /**
             * Define el valor de la propiedad certificateNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCertificateNumber(String value) {
                this.certificateNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad memberNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMemberNumber() {
                return memberNumber;
            }

            /**
             * Define el valor de la propiedad memberNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMemberNumber(String value) {
                this.memberNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad programName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramName() {
                return programName;
            }

            /**
             * Define el valor de la propiedad programName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramName(String value) {
                this.programName = value;
            }

        }

    }

}
