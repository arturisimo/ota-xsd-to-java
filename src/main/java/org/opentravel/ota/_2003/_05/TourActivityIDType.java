
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Unique identification for the tour/activity, including name, supplier ID and trading partner ID.
 * 
 * &lt;p&gt;Clase Java para TourActivityID_Type complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityID_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="SupplierProductCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="PartnerProductCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="SupplierBrandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *       &amp;lt;attribute name="ShortName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *       &amp;lt;attribute name="TourActivityID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityID_Type", propOrder = {
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTATourActivityAvailRQ.TourActivity.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityAvailRS.TourActivityInfo.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRQ.BookingInfo.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRQ.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRS.Reservation.ReservationInfo.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Summary.BasicInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.BasicInfo.class
})
public class TourActivityIDType {

    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "SupplierProductCode")
    protected String supplierProductCode;
    @XmlAttribute(name = "PartnerProductCode")
    protected String partnerProductCode;
    @XmlAttribute(name = "SupplierBrandCode")
    protected String supplierBrandCode;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "ShortName")
    protected String shortName;
    @XmlAttribute(name = "TourActivityID")
    protected String tourActivityID;

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierProductCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierProductCode() {
        return supplierProductCode;
    }

    /**
     * Define el valor de la propiedad supplierProductCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierProductCode(String value) {
        this.supplierProductCode = value;
    }

    /**
     * Obtiene el valor de la propiedad partnerProductCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerProductCode() {
        return partnerProductCode;
    }

    /**
     * Define el valor de la propiedad partnerProductCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerProductCode(String value) {
        this.partnerProductCode = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierBrandCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierBrandCode() {
        return supplierBrandCode;
    }

    /**
     * Define el valor de la propiedad supplierBrandCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierBrandCode(String value) {
        this.supplierBrandCode = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad shortName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Define el valor de la propiedad shortName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortName(String value) {
        this.shortName = value;
    }

    /**
     * Obtiene el valor de la propiedad tourActivityID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourActivityID() {
        return tourActivityID;
    }

    /**
     * Define el valor de la propiedad tourActivityID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourActivityID(String value) {
        this.tourActivityID = value;
    }

}
