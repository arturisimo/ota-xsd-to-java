
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PickUpDropOffType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="PickUpDropOffType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Airport"/&amp;gt;
 *     &amp;lt;enumeration value="Property"/&amp;gt;
 *     &amp;lt;enumeration value="Resort"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "PickUpDropOffType")
@XmlEnum
public enum PickUpDropOffType {

    @XmlEnumValue("Airport")
    AIRPORT("Airport"),
    @XmlEnumValue("Property")
    PROPERTY("Property"),
    @XmlEnumValue("Resort")
    RESORT("Resort");
    private final String value;

    PickUpDropOffType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PickUpDropOffType fromValue(String v) {
        for (PickUpDropOffType c: PickUpDropOffType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
