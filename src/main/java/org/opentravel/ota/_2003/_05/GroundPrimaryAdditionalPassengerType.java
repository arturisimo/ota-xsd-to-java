
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Information on the one primary passenger and, optionally, several additional passengers.
 * 
 * &lt;p&gt;Clase Java para GroundPrimaryAdditionalPassengerType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundPrimaryAdditionalPassengerType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Primary"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Additional" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AdditionalPersonType" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                 &amp;lt;attribute name="CorpDiscountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                 &amp;lt;attribute name="CorpDiscountNmbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;attribute name="QualificationMethod"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="RT_AirlineTicket"/&amp;gt;
 *                       &amp;lt;enumeration value="CreditCard"/&amp;gt;
 *                       &amp;lt;enumeration value="PassportAndReturnTkt"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundPrimaryAdditionalPassengerType", propOrder = {
    "primary",
    "additional"
})
public class GroundPrimaryAdditionalPassengerType {

    @XmlElement(name = "Primary", required = true)
    protected GroundPrimaryAdditionalPassengerType.Primary primary;
    @XmlElement(name = "Additional")
    protected List<GroundPrimaryAdditionalPassengerType.Additional> additional;

    /**
     * Obtiene el valor de la propiedad primary.
     * 
     * @return
     *     possible object is
     *     {@link GroundPrimaryAdditionalPassengerType.Primary }
     *     
     */
    public GroundPrimaryAdditionalPassengerType.Primary getPrimary() {
        return primary;
    }

    /**
     * Define el valor de la propiedad primary.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundPrimaryAdditionalPassengerType.Primary }
     *     
     */
    public void setPrimary(GroundPrimaryAdditionalPassengerType.Primary value) {
        this.primary = value;
    }

    /**
     * Gets the value of the additional property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additional property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAdditional().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GroundPrimaryAdditionalPassengerType.Additional }
     * 
     * 
     */
    public List<GroundPrimaryAdditionalPassengerType.Additional> getAdditional() {
        if (additional == null) {
            additional = new ArrayList<GroundPrimaryAdditionalPassengerType.Additional>();
        }
        return this.additional;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AdditionalPersonType" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *       &amp;lt;attribute name="CorpDiscountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *       &amp;lt;attribute name="CorpDiscountNmbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="QualificationMethod"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="RT_AirlineTicket"/&amp;gt;
     *             &amp;lt;enumeration value="CreditCard"/&amp;gt;
     *             &amp;lt;enumeration value="PassportAndReturnTkt"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "additionalPersonType"
    })
    public static class Additional
        extends ContactPersonType
    {

        @XmlElement(name = "AdditionalPersonType")
        protected GroundPrimaryAdditionalPassengerType.Additional.AdditionalPersonType additionalPersonType;
        @XmlAttribute(name = "CorpDiscountName")
        protected String corpDiscountName;
        @XmlAttribute(name = "CorpDiscountNmbr")
        protected String corpDiscountNmbr;
        @XmlAttribute(name = "QualificationMethod")
        protected String qualificationMethod;
        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;

        /**
         * Obtiene el valor de la propiedad additionalPersonType.
         * 
         * @return
         *     possible object is
         *     {@link GroundPrimaryAdditionalPassengerType.Additional.AdditionalPersonType }
         *     
         */
        public GroundPrimaryAdditionalPassengerType.Additional.AdditionalPersonType getAdditionalPersonType() {
            return additionalPersonType;
        }

        /**
         * Define el valor de la propiedad additionalPersonType.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundPrimaryAdditionalPassengerType.Additional.AdditionalPersonType }
         *     
         */
        public void setAdditionalPersonType(GroundPrimaryAdditionalPassengerType.Additional.AdditionalPersonType value) {
            this.additionalPersonType = value;
        }

        /**
         * Obtiene el valor de la propiedad corpDiscountName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCorpDiscountName() {
            return corpDiscountName;
        }

        /**
         * Define el valor de la propiedad corpDiscountName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCorpDiscountName(String value) {
            this.corpDiscountName = value;
        }

        /**
         * Obtiene el valor de la propiedad corpDiscountNmbr.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCorpDiscountNmbr() {
            return corpDiscountNmbr;
        }

        /**
         * Define el valor de la propiedad corpDiscountNmbr.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCorpDiscountNmbr(String value) {
            this.corpDiscountNmbr = value;
        }

        /**
         * Obtiene el valor de la propiedad qualificationMethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQualificationMethod() {
            return qualificationMethod;
        }

        /**
         * Define el valor de la propiedad qualificationMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQualificationMethod(String value) {
            this.qualificationMethod = value;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AdditionalPersonType {

            @XmlAttribute(name = "Age")
            protected Integer age;
            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "URI")
            @XmlSchemaType(name = "anyURI")
            protected String uri;
            @XmlAttribute(name = "Quantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger quantity;

            /**
             * Obtiene el valor de la propiedad age.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getAge() {
                return age;
            }

            /**
             * Define el valor de la propiedad age.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setAge(Integer value) {
                this.age = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad uri.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getURI() {
                return uri;
            }

            /**
             * Define el valor de la propiedad uri.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setURI(String value) {
                this.uri = value;
            }

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setQuantity(BigInteger value) {
                this.quantity = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerID"
    })
    public static class Primary
        extends ContactPersonType
    {

        @XmlElement(name = "CustomerID")
        protected UniqueIDType customerID;

        /**
         * Obtiene el valor de la propiedad customerID.
         * 
         * @return
         *     possible object is
         *     {@link UniqueIDType }
         *     
         */
        public UniqueIDType getCustomerID() {
            return customerID;
        }

        /**
         * Define el valor de la propiedad customerID.
         * 
         * @param value
         *     allowed object is
         *     {@link UniqueIDType }
         *     
         */
        public void setCustomerID(UniqueIDType value) {
            this.customerID = value;
        }

    }

}
