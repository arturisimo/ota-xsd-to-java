
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para LocationDetailRequirementAgeInfoType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="LocationDetailRequirementAgeInfoType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="MinimumAge"/&amp;gt;
 *     &amp;lt;enumeration value="MinimumAgeExceptions"/&amp;gt;
 *     &amp;lt;enumeration value="Miscellaneous"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "LocationDetailRequirementAgeInfoType")
@XmlEnum
public enum LocationDetailRequirementAgeInfoType {


    /**
     * Indicates that the associated information describes requirements and restrictions concerning the minimum age permitted for the rental of a vehicle.
     * 
     */
    @XmlEnumValue("MinimumAge")
    MINIMUM_AGE("MinimumAge"),

    /**
     * Indicates that the associated information describes the exceptions to the requirements and restrictions concerning the minimum age permitted for the rental of a vehicle.
     * 
     */
    @XmlEnumValue("MinimumAgeExceptions")
    MINIMUM_AGE_EXCEPTIONS("MinimumAgeExceptions"),

    /**
     * Indicates that the associated information describes miscellaneous requirements and restrictions with regard to the age of the renter of a vehicle.
     * 
     */
    @XmlEnumValue("Miscellaneous")
    MISCELLANEOUS("Miscellaneous");
    private final String value;

    LocationDetailRequirementAgeInfoType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LocationDetailRequirementAgeInfoType fromValue(String v) {
        for (LocationDetailRequirementAgeInfoType c: LocationDetailRequirementAgeInfoType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
