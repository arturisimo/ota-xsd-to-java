
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPaymentCardIssuer.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPaymentCardIssuer"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AmericanExpress"/&amp;gt;
 *     &amp;lt;enumeration value="BankOfAmerica"/&amp;gt;
 *     &amp;lt;enumeration value="BritishAirways"/&amp;gt;
 *     &amp;lt;enumeration value="CapitalOne"/&amp;gt;
 *     &amp;lt;enumeration value="Chase"/&amp;gt;
 *     &amp;lt;enumeration value="Citibank"/&amp;gt;
 *     &amp;lt;enumeration value="ContinentalAirlines"/&amp;gt;
 *     &amp;lt;enumeration value="DeltaAirlines"/&amp;gt;
 *     &amp;lt;enumeration value="DiscoverCard"/&amp;gt;
 *     &amp;lt;enumeration value="Disney"/&amp;gt;
 *     &amp;lt;enumeration value="Eurocard"/&amp;gt;
 *     &amp;lt;enumeration value="Hilton"/&amp;gt;
 *     &amp;lt;enumeration value="Hyatt"/&amp;gt;
 *     &amp;lt;enumeration value="Mariott"/&amp;gt;
 *     &amp;lt;enumeration value="Mastercard"/&amp;gt;
 *     &amp;lt;enumeration value="RitzCarlton"/&amp;gt;
 *     &amp;lt;enumeration value="SouthwestAirlines"/&amp;gt;
 *     &amp;lt;enumeration value="StarwoodHotels"/&amp;gt;
 *     &amp;lt;enumeration value="UnitedAirlines"/&amp;gt;
 *     &amp;lt;enumeration value="USAirways"/&amp;gt;
 *     &amp;lt;enumeration value="VISA"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPaymentCardIssuer")
@XmlEnum
public enum ListOfferPaymentCardIssuer {

    @XmlEnumValue("AmericanExpress")
    AMERICAN_EXPRESS("AmericanExpress"),
    @XmlEnumValue("BankOfAmerica")
    BANK_OF_AMERICA("BankOfAmerica"),
    @XmlEnumValue("BritishAirways")
    BRITISH_AIRWAYS("BritishAirways"),
    @XmlEnumValue("CapitalOne")
    CAPITAL_ONE("CapitalOne"),
    @XmlEnumValue("Chase")
    CHASE("Chase"),
    @XmlEnumValue("Citibank")
    CITIBANK("Citibank"),
    @XmlEnumValue("ContinentalAirlines")
    CONTINENTAL_AIRLINES("ContinentalAirlines"),
    @XmlEnumValue("DeltaAirlines")
    DELTA_AIRLINES("DeltaAirlines"),
    @XmlEnumValue("DiscoverCard")
    DISCOVER_CARD("DiscoverCard"),
    @XmlEnumValue("Disney")
    DISNEY("Disney"),
    @XmlEnumValue("Eurocard")
    EUROCARD("Eurocard"),
    @XmlEnumValue("Hilton")
    HILTON("Hilton"),
    @XmlEnumValue("Hyatt")
    HYATT("Hyatt"),
    @XmlEnumValue("Mariott")
    MARIOTT("Mariott"),
    @XmlEnumValue("Mastercard")
    MASTERCARD("Mastercard"),
    @XmlEnumValue("RitzCarlton")
    RITZ_CARLTON("RitzCarlton"),
    @XmlEnumValue("SouthwestAirlines")
    SOUTHWEST_AIRLINES("SouthwestAirlines"),
    @XmlEnumValue("StarwoodHotels")
    STARWOOD_HOTELS("StarwoodHotels"),
    @XmlEnumValue("UnitedAirlines")
    UNITED_AIRLINES("UnitedAirlines"),
    @XmlEnumValue("USAirways")
    US_AIRWAYS("USAirways"),
    VISA("VISA"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferPaymentCardIssuer(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPaymentCardIssuer fromValue(String v) {
        for (ListOfferPaymentCardIssuer c: ListOfferPaymentCardIssuer.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
