
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Indicates the number of customers taking specific type of personal insurance.
 * 
 * &lt;p&gt;Clase Java para PersonalInsuranceItemType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PersonalInsuranceItemType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CustomerCounts" type="{http://www.opentravel.org/OTA/2003/05}CustomerCountsType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}PkgPersonalInsuranceCode" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalInsuranceItemType", propOrder = {
    "customerCounts"
})
public class PersonalInsuranceItemType {

    @XmlElement(name = "CustomerCounts", required = true)
    protected CustomerCountsType customerCounts;
    @XmlAttribute(name = "Code", required = true)
    protected PkgPersonalInsuranceCode code;

    /**
     * Obtiene el valor de la propiedad customerCounts.
     * 
     * @return
     *     possible object is
     *     {@link CustomerCountsType }
     *     
     */
    public CustomerCountsType getCustomerCounts() {
        return customerCounts;
    }

    /**
     * Define el valor de la propiedad customerCounts.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerCountsType }
     *     
     */
    public void setCustomerCounts(CustomerCountsType value) {
        this.customerCounts = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link PkgPersonalInsuranceCode }
     *     
     */
    public PkgPersonalInsuranceCode getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgPersonalInsuranceCode }
     *     
     */
    public void setCode(PkgPersonalInsuranceCode value) {
        this.code = value;
    }

}
