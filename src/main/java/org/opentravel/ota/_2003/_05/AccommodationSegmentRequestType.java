
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines the required accommodation component of a package holiday.
 * 
 * &lt;p&gt;Clase Java para AccommodationSegmentRequestType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccommodationSegmentRequestType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Identity" type="{http://www.opentravel.org/OTA/2003/05}PropertyIdentityType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
 *         &amp;lt;element name="RoomProfiles" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomProfile" type="{http://www.opentravel.org/OTA/2003/05}RoomProfileType" maxOccurs="9"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MealPlans" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DestinationLevelGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResortCodeGroup"/&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccommodationSegmentRequestType", propOrder = {
    "identity",
    "dateRange",
    "roomProfiles",
    "mealPlans"
})
public class AccommodationSegmentRequestType {

    @XmlElement(name = "Identity")
    protected PropertyIdentityType identity;
    @XmlElement(name = "DateRange", required = true)
    protected DateTimeSpanType dateRange;
    @XmlElement(name = "RoomProfiles")
    protected AccommodationSegmentRequestType.RoomProfiles roomProfiles;
    @XmlElement(name = "MealPlans")
    protected AccommodationSegmentRequestType.MealPlans mealPlans;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "DestinationCode")
    protected String destinationCode;
    @XmlAttribute(name = "DestinationLevel")
    protected DestinationLevelType destinationLevel;
    @XmlAttribute(name = "DestinationName")
    protected String destinationName;
    @XmlAttribute(name = "ResortCode")
    protected String resortCode;
    @XmlAttribute(name = "ResortName")
    protected String resortName;

    /**
     * Obtiene el valor de la propiedad identity.
     * 
     * @return
     *     possible object is
     *     {@link PropertyIdentityType }
     *     
     */
    public PropertyIdentityType getIdentity() {
        return identity;
    }

    /**
     * Define el valor de la propiedad identity.
     * 
     * @param value
     *     allowed object is
     *     {@link PropertyIdentityType }
     *     
     */
    public void setIdentity(PropertyIdentityType value) {
        this.identity = value;
    }

    /**
     * Obtiene el valor de la propiedad dateRange.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeSpanType }
     *     
     */
    public DateTimeSpanType getDateRange() {
        return dateRange;
    }

    /**
     * Define el valor de la propiedad dateRange.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeSpanType }
     *     
     */
    public void setDateRange(DateTimeSpanType value) {
        this.dateRange = value;
    }

    /**
     * Obtiene el valor de la propiedad roomProfiles.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationSegmentRequestType.RoomProfiles }
     *     
     */
    public AccommodationSegmentRequestType.RoomProfiles getRoomProfiles() {
        return roomProfiles;
    }

    /**
     * Define el valor de la propiedad roomProfiles.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationSegmentRequestType.RoomProfiles }
     *     
     */
    public void setRoomProfiles(AccommodationSegmentRequestType.RoomProfiles value) {
        this.roomProfiles = value;
    }

    /**
     * Obtiene el valor de la propiedad mealPlans.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationSegmentRequestType.MealPlans }
     *     
     */
    public AccommodationSegmentRequestType.MealPlans getMealPlans() {
        return mealPlans;
    }

    /**
     * Define el valor de la propiedad mealPlans.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationSegmentRequestType.MealPlans }
     *     
     */
    public void setMealPlans(AccommodationSegmentRequestType.MealPlans value) {
        this.mealPlans = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCode() {
        return destinationCode;
    }

    /**
     * Define el valor de la propiedad destinationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCode(String value) {
        this.destinationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationLevel.
     * 
     * @return
     *     possible object is
     *     {@link DestinationLevelType }
     *     
     */
    public DestinationLevelType getDestinationLevel() {
        return destinationLevel;
    }

    /**
     * Define el valor de la propiedad destinationLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationLevelType }
     *     
     */
    public void setDestinationLevel(DestinationLevelType value) {
        this.destinationLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationName() {
        return destinationName;
    }

    /**
     * Define el valor de la propiedad destinationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationName(String value) {
        this.destinationName = value;
    }

    /**
     * Obtiene el valor de la propiedad resortCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResortCode() {
        return resortCode;
    }

    /**
     * Define el valor de la propiedad resortCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResortCode(String value) {
        this.resortCode = value;
    }

    /**
     * Obtiene el valor de la propiedad resortName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResortName() {
        return resortName;
    }

    /**
     * Define el valor de la propiedad resortName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResortName(String value) {
        this.resortName = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mealPlan"
    })
    public static class MealPlans {

        @XmlElement(name = "MealPlan", required = true)
        protected List<MealPlanType> mealPlan;

        /**
         * Gets the value of the mealPlan property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the mealPlan property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getMealPlan().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link MealPlanType }
         * 
         * 
         */
        public List<MealPlanType> getMealPlan() {
            if (mealPlan == null) {
                mealPlan = new ArrayList<MealPlanType>();
            }
            return this.mealPlan;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomProfile" type="{http://www.opentravel.org/OTA/2003/05}RoomProfileType" maxOccurs="9"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomProfile"
    })
    public static class RoomProfiles {

        @XmlElement(name = "RoomProfile", required = true)
        protected List<RoomProfileType> roomProfile;

        /**
         * Gets the value of the roomProfile property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomProfile property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomProfile().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RoomProfileType }
         * 
         * 
         */
        public List<RoomProfileType> getRoomProfile() {
            if (roomProfile == null) {
                roomProfile = new ArrayList<RoomProfileType>();
            }
            return this.roomProfile;
        }

    }

}
