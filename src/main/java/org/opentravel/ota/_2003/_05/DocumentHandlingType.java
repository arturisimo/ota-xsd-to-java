
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Specifies the document handling information.
 * 
 * &lt;p&gt;Clase Java para DocumentHandlingType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DocumentHandlingType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence minOccurs="0"&amp;gt;
 *         &amp;lt;element name="VendorOption" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="VendorName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DocumentTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="DeliveryMethodCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="DocumentDestination" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="SelectedOptionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="DefaultIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="AddressRequiredIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="AddressRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="EmailRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="TelephoneRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="DocumentLanguage" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentHandlingType", propOrder = {
    "vendorOption"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTACruiseBookingDocumentRQ.CruiseDocument.class,
    org.opentravel.ota._2003._05.OTACruiseBookingDocumentRS.CruiseDocument.class
})
public class DocumentHandlingType {

    @XmlElement(name = "VendorOption")
    protected List<DocumentHandlingType.VendorOption> vendorOption;
    @XmlAttribute(name = "DocumentTypeCode")
    protected String documentTypeCode;
    @XmlAttribute(name = "DeliveryMethodCode")
    protected String deliveryMethodCode;
    @XmlAttribute(name = "DocumentDestination")
    protected String documentDestination;
    @XmlAttribute(name = "SelectedOptionIndicator")
    protected Boolean selectedOptionIndicator;
    @XmlAttribute(name = "DefaultIndicator")
    protected Boolean defaultIndicator;
    @XmlAttribute(name = "AddressRequiredIndicator")
    protected Boolean addressRequiredIndicator;
    @XmlAttribute(name = "AddressRPH")
    protected String addressRPH;
    @XmlAttribute(name = "EmailRPH")
    protected String emailRPH;
    @XmlAttribute(name = "TelephoneRPH")
    protected String telephoneRPH;
    @XmlAttribute(name = "DocumentLanguage")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String documentLanguage;

    /**
     * Gets the value of the vendorOption property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorOption property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getVendorOption().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentHandlingType.VendorOption }
     * 
     * 
     */
    public List<DocumentHandlingType.VendorOption> getVendorOption() {
        if (vendorOption == null) {
            vendorOption = new ArrayList<DocumentHandlingType.VendorOption>();
        }
        return this.vendorOption;
    }

    /**
     * Obtiene el valor de la propiedad documentTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    /**
     * Define el valor de la propiedad documentTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentTypeCode(String value) {
        this.documentTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryMethodCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryMethodCode() {
        return deliveryMethodCode;
    }

    /**
     * Define el valor de la propiedad deliveryMethodCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryMethodCode(String value) {
        this.deliveryMethodCode = value;
    }

    /**
     * Obtiene el valor de la propiedad documentDestination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDestination() {
        return documentDestination;
    }

    /**
     * Define el valor de la propiedad documentDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDestination(String value) {
        this.documentDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad selectedOptionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelectedOptionIndicator() {
        return selectedOptionIndicator;
    }

    /**
     * Define el valor de la propiedad selectedOptionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelectedOptionIndicator(Boolean value) {
        this.selectedOptionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad defaultIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultIndicator() {
        return defaultIndicator;
    }

    /**
     * Define el valor de la propiedad defaultIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultIndicator(Boolean value) {
        this.defaultIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad addressRequiredIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddressRequiredIndicator() {
        return addressRequiredIndicator;
    }

    /**
     * Define el valor de la propiedad addressRequiredIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddressRequiredIndicator(Boolean value) {
        this.addressRequiredIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad addressRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressRPH() {
        return addressRPH;
    }

    /**
     * Define el valor de la propiedad addressRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressRPH(String value) {
        this.addressRPH = value;
    }

    /**
     * Obtiene el valor de la propiedad emailRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailRPH() {
        return emailRPH;
    }

    /**
     * Define el valor de la propiedad emailRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailRPH(String value) {
        this.emailRPH = value;
    }

    /**
     * Obtiene el valor de la propiedad telephoneRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephoneRPH() {
        return telephoneRPH;
    }

    /**
     * Define el valor de la propiedad telephoneRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephoneRPH(String value) {
        this.telephoneRPH = value;
    }

    /**
     * Obtiene el valor de la propiedad documentLanguage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentLanguage() {
        return documentLanguage;
    }

    /**
     * Define el valor de la propiedad documentLanguage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentLanguage(String value) {
        this.documentLanguage = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="VendorName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class VendorOption {

        @XmlAttribute(name = "VendorName")
        protected String vendorName;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad vendorName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * Define el valor de la propiedad vendorName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVendorName(String value) {
            this.vendorName = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }

}
