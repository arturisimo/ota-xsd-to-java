
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PreferLevelType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="PreferLevelType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Only"/&amp;gt;
 *     &amp;lt;enumeration value="Unacceptable"/&amp;gt;
 *     &amp;lt;enumeration value="Preferred"/&amp;gt;
 *     &amp;lt;enumeration value="Required"/&amp;gt;
 *     &amp;lt;enumeration value="NoPreference"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "PreferLevelType")
@XmlEnum
public enum PreferLevelType {


    /**
     * Preference level that indicates request is only for a specific criterion.
     * 
     */
    @XmlEnumValue("Only")
    ONLY("Only"),

    /**
     * Preference level that indicates request is unnacceptable for a specific criterion.
     * 
     * 
     */
    @XmlEnumValue("Unacceptable")
    UNACCEPTABLE("Unacceptable"),

    /**
     * Preference level that indicates request is preferred for a specific criterion.
     * 
     * 
     */
    @XmlEnumValue("Preferred")
    PREFERRED("Preferred"),

    /**
     * Preference level that indicates request is required for a specific criterion.
     * 
     * 
     */
    @XmlEnumValue("Required")
    REQUIRED("Required"),

    /**
     * Preference level that indicates there is no preference.
     * 
     * 
     */
    @XmlEnumValue("NoPreference")
    NO_PREFERENCE("NoPreference");
    private final String value;

    PreferLevelType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PreferLevelType fromValue(String v) {
        for (PreferLevelType c: PreferLevelType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
