
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides information regarding the guest name and the guest's mode of transportation.
 * 
 * &lt;p&gt;Clase Java para GuestType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GuestType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="GuestName" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="GuestTransportation" type="{http://www.opentravel.org/OTA/2003/05}GuestTransportationType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GuestInfoGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GuestType", propOrder = {
    "guestName",
    "guestTransportation"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.CruiseBookingInfoType.GuestPrices.GuestPrice.class
})
public class GuestType {

    @XmlElement(name = "GuestName")
    protected PersonNameType guestName;
    @XmlElement(name = "GuestTransportation")
    protected List<GuestTransportationType> guestTransportation;
    @XmlAttribute(name = "GuestRefNumber")
    protected String guestRefNumber;
    @XmlAttribute(name = "Age")
    protected Integer age;
    @XmlAttribute(name = "Nationality")
    protected String nationality;
    @XmlAttribute(name = "GuestOccupation")
    protected String guestOccupation;
    @XmlAttribute(name = "PersonBirthDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar personBirthDate;
    @XmlAttribute(name = "Gender")
    protected String gender;
    @XmlAttribute(name = "LoyaltyMembershipID")
    protected String loyaltyMembershipID;
    @XmlAttribute(name = "LoyalLevel")
    protected String loyalLevel;
    @XmlAttribute(name = "LoyalLevelCode")
    protected Integer loyalLevelCode;

    /**
     * Obtiene el valor de la propiedad guestName.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameType }
     *     
     */
    public PersonNameType getGuestName() {
        return guestName;
    }

    /**
     * Define el valor de la propiedad guestName.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameType }
     *     
     */
    public void setGuestName(PersonNameType value) {
        this.guestName = value;
    }

    /**
     * Gets the value of the guestTransportation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the guestTransportation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getGuestTransportation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GuestTransportationType }
     * 
     * 
     */
    public List<GuestTransportationType> getGuestTransportation() {
        if (guestTransportation == null) {
            guestTransportation = new ArrayList<GuestTransportationType>();
        }
        return this.guestTransportation;
    }

    /**
     * Obtiene el valor de la propiedad guestRefNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestRefNumber() {
        return guestRefNumber;
    }

    /**
     * Define el valor de la propiedad guestRefNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestRefNumber(String value) {
        this.guestRefNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad age.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAge() {
        return age;
    }

    /**
     * Define el valor de la propiedad age.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAge(Integer value) {
        this.age = value;
    }

    /**
     * Obtiene el valor de la propiedad nationality.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Define el valor de la propiedad nationality.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Obtiene el valor de la propiedad guestOccupation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestOccupation() {
        return guestOccupation;
    }

    /**
     * Define el valor de la propiedad guestOccupation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestOccupation(String value) {
        this.guestOccupation = value;
    }

    /**
     * Obtiene el valor de la propiedad personBirthDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPersonBirthDate() {
        return personBirthDate;
    }

    /**
     * Define el valor de la propiedad personBirthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPersonBirthDate(XMLGregorianCalendar value) {
        this.personBirthDate = value;
    }

    /**
     * Obtiene el valor de la propiedad gender.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Define el valor de la propiedad gender.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Obtiene el valor de la propiedad loyaltyMembershipID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyaltyMembershipID() {
        return loyaltyMembershipID;
    }

    /**
     * Define el valor de la propiedad loyaltyMembershipID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyaltyMembershipID(String value) {
        this.loyaltyMembershipID = value;
    }

    /**
     * Obtiene el valor de la propiedad loyalLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoyalLevel() {
        return loyalLevel;
    }

    /**
     * Define el valor de la propiedad loyalLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoyalLevel(String value) {
        this.loyalLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad loyalLevelCode.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLoyalLevelCode() {
        return loyalLevelCode;
    }

    /**
     * Define el valor de la propiedad loyalLevelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLoyalLevelCode(Integer value) {
        this.loyalLevelCode = value;
    }

}
