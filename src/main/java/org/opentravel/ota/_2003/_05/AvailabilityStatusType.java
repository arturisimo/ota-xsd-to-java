
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para AvailabilityStatusType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AvailabilityStatusType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Open"/&amp;gt;
 *     &amp;lt;enumeration value="Close"/&amp;gt;
 *     &amp;lt;enumeration value="ClosedOnArrival"/&amp;gt;
 *     &amp;lt;enumeration value="ClosedOnArrivalOnRequest"/&amp;gt;
 *     &amp;lt;enumeration value="OnRequest"/&amp;gt;
 *     &amp;lt;enumeration value="RemoveCloseOnly"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AvailabilityStatusType")
@XmlEnum
public enum AvailabilityStatusType {


    /**
     * Inventory is available for sale.
     * 
     */
    @XmlEnumValue("Open")
    OPEN("Open"),

    /**
     * Inventory is not available for sale.
     * 
     */
    @XmlEnumValue("Close")
    CLOSE("Close"),

    /**
     * Inventory is not available for sale to arriving guests.
     * 
     */
    @XmlEnumValue("ClosedOnArrival")
    CLOSED_ON_ARRIVAL("ClosedOnArrival"),

    /**
     * Inventory may not be available for sale to arriving guests.
     * 
     */
    @XmlEnumValue("ClosedOnArrivalOnRequest")
    CLOSED_ON_ARRIVAL_ON_REQUEST("ClosedOnArrivalOnRequest"),

    /**
     * Inventory may be available.
     * 
     */
    @XmlEnumValue("OnRequest")
    ON_REQUEST("OnRequest"),

    /**
     * Remove Close restriction while keeping other restrictions in place.
     * 
     */
    @XmlEnumValue("RemoveCloseOnly")
    REMOVE_CLOSE_ONLY("RemoveCloseOnly");
    private final String value;

    AvailabilityStatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AvailabilityStatusType fromValue(String v) {
        for (AvailabilityStatusType c: AvailabilityStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
