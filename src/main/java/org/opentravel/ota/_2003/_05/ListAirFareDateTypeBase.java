
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_AirFareDateType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_AirFareDateType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="DiscontinueTravel"/&amp;gt;
 *     &amp;lt;enumeration value="EffectiveTravel"/&amp;gt;
 *     &amp;lt;enumeration value="FirstTicketing"/&amp;gt;
 *     &amp;lt;enumeration value="Historic"/&amp;gt;
 *     &amp;lt;enumeration value="LastTicketing"/&amp;gt;
 *     &amp;lt;enumeration value="TravelCompletion"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_AirFareDateType_Base")
@XmlEnum
public enum ListAirFareDateTypeBase {


    /**
     * The last travel date for this fare.
     * 
     */
    @XmlEnumValue("DiscontinueTravel")
    DISCONTINUE_TRAVEL("DiscontinueTravel"),

    /**
     * The first travel date for this fare.
     * 
     */
    @XmlEnumValue("EffectiveTravel")
    EFFECTIVE_TRAVEL("EffectiveTravel"),

    /**
     * The first date for ticketing.
     * 
     */
    @XmlEnumValue("FirstTicketing")
    FIRST_TICKETING("FirstTicketing"),
    @XmlEnumValue("Historic")
    HISTORIC("Historic"),

    /**
     * The last date for ticketing.
     * 
     */
    @XmlEnumValue("LastTicketing")
    LAST_TICKETING("LastTicketing"),

    /**
     * The date by which travel must be completed.
     * 
     */
    @XmlEnumValue("TravelCompletion")
    TRAVEL_COMPLETION("TravelCompletion"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListAirFareDateTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListAirFareDateTypeBase fromValue(String v) {
        for (ListAirFareDateTypeBase c: ListAirFareDateTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
