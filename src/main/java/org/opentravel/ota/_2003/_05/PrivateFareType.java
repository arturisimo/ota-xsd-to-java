
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Rules and other information associated with private fares.
 * 
 * &lt;p&gt;Clase Java para PrivateFareType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PrivateFareType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Airport" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FareDetails" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="GlobalIndicatorCode" type="{http://www.opentravel.org/OTA/2003/05}List_GlobalIndicatorType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Operation" type="{http://www.opentravel.org/OTA/2003/05}List_DataActionType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}List_FareStatus" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="TripType" type="{http://www.opentravel.org/OTA/2003/05}List_AirTripType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Date" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_AirFareDateType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Fare" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="BaseAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="TotalFare" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PassengerType" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="FareDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
 *                 &amp;lt;attribute name="FareType" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to3" /&amp;gt;
 *                 &amp;lt;attribute name="MaxPermittedMileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FilingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RuleInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="AccountCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="PrivateFareCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *       &amp;lt;attribute name="NbrOfCities" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *       &amp;lt;attribute name="RoutingNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
 *       &amp;lt;attribute name="TariffRuleNmbr" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="TarriffRuleDes" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="TicketDesignatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="TicketDesignatorExtension" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="FlightRefRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrivateFareType", propOrder = {
    "airport",
    "fareDetails",
    "filingAirline",
    "marketingAirline",
    "ruleInfo",
    "tpaExtensions"
})
public class PrivateFareType {

    @XmlElement(name = "Airport")
    protected List<PrivateFareType.Airport> airport;
    @XmlElement(name = "FareDetails")
    protected List<PrivateFareType.FareDetails> fareDetails;
    @XmlElement(name = "FilingAirline")
    protected CompanyNameType filingAirline;
    @XmlElement(name = "MarketingAirline")
    protected List<CompanyNameType> marketingAirline;
    @XmlElement(name = "RuleInfo")
    protected PrivateFareType.RuleInfo ruleInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "AccountCode", required = true)
    protected String accountCode;
    @XmlAttribute(name = "PrivateFareCode")
    protected String privateFareCode;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "NbrOfCities")
    protected Integer nbrOfCities;
    @XmlAttribute(name = "ResBookDesigCode")
    protected String resBookDesigCode;
    @XmlAttribute(name = "RoutingNumber")
    protected Integer routingNumber;
    @XmlAttribute(name = "TariffRuleNmbr")
    protected String tariffRuleNmbr;
    @XmlAttribute(name = "TarriffRuleDes")
    protected String tarriffRuleDes;
    @XmlAttribute(name = "TicketDesignatorCode")
    protected String ticketDesignatorCode;
    @XmlAttribute(name = "TicketDesignatorExtension")
    protected String ticketDesignatorExtension;
    @XmlAttribute(name = "FlightRefRPH")
    protected String flightRefRPH;

    /**
     * Gets the value of the airport property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airport property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAirport().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PrivateFareType.Airport }
     * 
     * 
     */
    public List<PrivateFareType.Airport> getAirport() {
        if (airport == null) {
            airport = new ArrayList<PrivateFareType.Airport>();
        }
        return this.airport;
    }

    /**
     * Gets the value of the fareDetails property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareDetails property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFareDetails().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PrivateFareType.FareDetails }
     * 
     * 
     */
    public List<PrivateFareType.FareDetails> getFareDetails() {
        if (fareDetails == null) {
            fareDetails = new ArrayList<PrivateFareType.FareDetails>();
        }
        return this.fareDetails;
    }

    /**
     * Obtiene el valor de la propiedad filingAirline.
     * 
     * @return
     *     possible object is
     *     {@link CompanyNameType }
     *     
     */
    public CompanyNameType getFilingAirline() {
        return filingAirline;
    }

    /**
     * Define el valor de la propiedad filingAirline.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyNameType }
     *     
     */
    public void setFilingAirline(CompanyNameType value) {
        this.filingAirline = value;
    }

    /**
     * Gets the value of the marketingAirline property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingAirline property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getMarketingAirline().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CompanyNameType }
     * 
     * 
     */
    public List<CompanyNameType> getMarketingAirline() {
        if (marketingAirline == null) {
            marketingAirline = new ArrayList<CompanyNameType>();
        }
        return this.marketingAirline;
    }

    /**
     * Obtiene el valor de la propiedad ruleInfo.
     * 
     * @return
     *     possible object is
     *     {@link PrivateFareType.RuleInfo }
     *     
     */
    public PrivateFareType.RuleInfo getRuleInfo() {
        return ruleInfo;
    }

    /**
     * Define el valor de la propiedad ruleInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link PrivateFareType.RuleInfo }
     *     
     */
    public void setRuleInfo(PrivateFareType.RuleInfo value) {
        this.ruleInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Obtiene el valor de la propiedad privateFareCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateFareCode() {
        return privateFareCode;
    }

    /**
     * Define el valor de la propiedad privateFareCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateFareCode(String value) {
        this.privateFareCode = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad nbrOfCities.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNbrOfCities() {
        return nbrOfCities;
    }

    /**
     * Define el valor de la propiedad nbrOfCities.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNbrOfCities(Integer value) {
        this.nbrOfCities = value;
    }

    /**
     * Obtiene el valor de la propiedad resBookDesigCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResBookDesigCode() {
        return resBookDesigCode;
    }

    /**
     * Define el valor de la propiedad resBookDesigCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResBookDesigCode(String value) {
        this.resBookDesigCode = value;
    }

    /**
     * Obtiene el valor de la propiedad routingNumber.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRoutingNumber() {
        return routingNumber;
    }

    /**
     * Define el valor de la propiedad routingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRoutingNumber(Integer value) {
        this.routingNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad tariffRuleNmbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRuleNmbr() {
        return tariffRuleNmbr;
    }

    /**
     * Define el valor de la propiedad tariffRuleNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRuleNmbr(String value) {
        this.tariffRuleNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad tarriffRuleDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarriffRuleDes() {
        return tarriffRuleDes;
    }

    /**
     * Define el valor de la propiedad tarriffRuleDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarriffRuleDes(String value) {
        this.tarriffRuleDes = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketDesignatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDesignatorCode() {
        return ticketDesignatorCode;
    }

    /**
     * Define el valor de la propiedad ticketDesignatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDesignatorCode(String value) {
        this.ticketDesignatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketDesignatorExtension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDesignatorExtension() {
        return ticketDesignatorExtension;
    }

    /**
     * Define el valor de la propiedad ticketDesignatorExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDesignatorExtension(String value) {
        this.ticketDesignatorExtension = value;
    }

    /**
     * Obtiene el valor de la propiedad flightRefRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightRefRPH() {
        return flightRefRPH;
    }

    /**
     * Define el valor de la propiedad flightRefRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightRefRPH(String value) {
        this.flightRefRPH = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Airport {

        @XmlAttribute(name = "LocationCode")
        protected String locationCode;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;

        /**
         * Obtiene el valor de la propiedad locationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocationCode() {
            return locationCode;
        }

        /**
         * Define el valor de la propiedad locationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocationCode(String value) {
            this.locationCode = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="GlobalIndicatorCode" type="{http://www.opentravel.org/OTA/2003/05}List_GlobalIndicatorType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Operation" type="{http://www.opentravel.org/OTA/2003/05}List_DataActionType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}List_FareStatus" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="TripType" type="{http://www.opentravel.org/OTA/2003/05}List_AirTripType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Date" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_AirFareDateType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Fare" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="BaseAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TotalFare" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PassengerType" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="FareDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
     *       &amp;lt;attribute name="FareType" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to3" /&amp;gt;
     *       &amp;lt;attribute name="MaxPermittedMileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "globalIndicatorCode",
        "operation",
        "fareStatus",
        "tripType",
        "date",
        "fare",
        "passengerType"
    })
    public static class FareDetails {

        @XmlElement(name = "GlobalIndicatorCode")
        protected ListGlobalIndicatorType globalIndicatorCode;
        @XmlElement(name = "Operation")
        protected ListDataActionType operation;
        @XmlElement(name = "FareStatus")
        protected ListFareStatus fareStatus;
        @XmlElement(name = "TripType")
        protected ListAirTripType tripType;
        @XmlElement(name = "Date")
        protected List<PrivateFareType.FareDetails.Date> date;
        @XmlElement(name = "Fare")
        protected PrivateFareType.FareDetails.Fare fare;
        @XmlElement(name = "PassengerType")
        protected List<PrivateFareType.FareDetails.PassengerType> passengerType;
        @XmlAttribute(name = "FareBasisCode")
        protected String fareBasisCode;
        @XmlAttribute(name = "FareDescription")
        protected String fareDescription;
        @XmlAttribute(name = "FareType")
        protected String fareType;
        @XmlAttribute(name = "MaxPermittedMileage")
        protected BigInteger maxPermittedMileage;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Obtiene el valor de la propiedad globalIndicatorCode.
         * 
         * @return
         *     possible object is
         *     {@link ListGlobalIndicatorType }
         *     
         */
        public ListGlobalIndicatorType getGlobalIndicatorCode() {
            return globalIndicatorCode;
        }

        /**
         * Define el valor de la propiedad globalIndicatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link ListGlobalIndicatorType }
         *     
         */
        public void setGlobalIndicatorCode(ListGlobalIndicatorType value) {
            this.globalIndicatorCode = value;
        }

        /**
         * Obtiene el valor de la propiedad operation.
         * 
         * @return
         *     possible object is
         *     {@link ListDataActionType }
         *     
         */
        public ListDataActionType getOperation() {
            return operation;
        }

        /**
         * Define el valor de la propiedad operation.
         * 
         * @param value
         *     allowed object is
         *     {@link ListDataActionType }
         *     
         */
        public void setOperation(ListDataActionType value) {
            this.operation = value;
        }

        /**
         * Obtiene el valor de la propiedad fareStatus.
         * 
         * @return
         *     possible object is
         *     {@link ListFareStatus }
         *     
         */
        public ListFareStatus getFareStatus() {
            return fareStatus;
        }

        /**
         * Define el valor de la propiedad fareStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link ListFareStatus }
         *     
         */
        public void setFareStatus(ListFareStatus value) {
            this.fareStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad tripType.
         * 
         * @return
         *     possible object is
         *     {@link ListAirTripType }
         *     
         */
        public ListAirTripType getTripType() {
            return tripType;
        }

        /**
         * Define el valor de la propiedad tripType.
         * 
         * @param value
         *     allowed object is
         *     {@link ListAirTripType }
         *     
         */
        public void setTripType(ListAirTripType value) {
            this.tripType = value;
        }

        /**
         * Gets the value of the date property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the date property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDate().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PrivateFareType.FareDetails.Date }
         * 
         * 
         */
        public List<PrivateFareType.FareDetails.Date> getDate() {
            if (date == null) {
                date = new ArrayList<PrivateFareType.FareDetails.Date>();
            }
            return this.date;
        }

        /**
         * Obtiene el valor de la propiedad fare.
         * 
         * @return
         *     possible object is
         *     {@link PrivateFareType.FareDetails.Fare }
         *     
         */
        public PrivateFareType.FareDetails.Fare getFare() {
            return fare;
        }

        /**
         * Define el valor de la propiedad fare.
         * 
         * @param value
         *     allowed object is
         *     {@link PrivateFareType.FareDetails.Fare }
         *     
         */
        public void setFare(PrivateFareType.FareDetails.Fare value) {
            this.fare = value;
        }

        /**
         * Gets the value of the passengerType property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerType property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPassengerType().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PrivateFareType.FareDetails.PassengerType }
         * 
         * 
         */
        public List<PrivateFareType.FareDetails.PassengerType> getPassengerType() {
            if (passengerType == null) {
                passengerType = new ArrayList<PrivateFareType.FareDetails.PassengerType>();
            }
            return this.passengerType;
        }

        /**
         * Obtiene el valor de la propiedad fareBasisCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareBasisCode() {
            return fareBasisCode;
        }

        /**
         * Define el valor de la propiedad fareBasisCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareBasisCode(String value) {
            this.fareBasisCode = value;
        }

        /**
         * Obtiene el valor de la propiedad fareDescription.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareDescription() {
            return fareDescription;
        }

        /**
         * Define el valor de la propiedad fareDescription.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareDescription(String value) {
            this.fareDescription = value;
        }

        /**
         * Obtiene el valor de la propiedad fareType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareType() {
            return fareType;
        }

        /**
         * Define el valor de la propiedad fareType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareType(String value) {
            this.fareType = value;
        }

        /**
         * Obtiene el valor de la propiedad maxPermittedMileage.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxPermittedMileage() {
            return maxPermittedMileage;
        }

        /**
         * Define el valor de la propiedad maxPermittedMileage.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxPermittedMileage(BigInteger value) {
            this.maxPermittedMileage = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_AirFareDateType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type"
        })
        public static class Date {

            @XmlElement(name = "Type")
            protected ListAirFareDateType type;
            @XmlAttribute(name = "Date")
            protected String date;

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link ListAirFareDateType }
             *     
             */
            public ListAirFareDateType getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link ListAirFareDateType }
             *     
             */
            public void setType(ListAirFareDateType value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad date.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDate() {
                return date;
            }

            /**
             * Define el valor de la propiedad date.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDate(String value) {
                this.date = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="BaseAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TotalFare" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Fare {

            @XmlAttribute(name = "BaseAmount")
            protected BigDecimal baseAmount;
            @XmlAttribute(name = "BaseNUC_Amount")
            protected BigDecimal baseNUCAmount;
            @XmlAttribute(name = "TaxAmount")
            protected BigDecimal taxAmount;
            @XmlAttribute(name = "TotalFare")
            protected BigDecimal totalFare;

            /**
             * Obtiene el valor de la propiedad baseAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBaseAmount() {
                return baseAmount;
            }

            /**
             * Define el valor de la propiedad baseAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBaseAmount(BigDecimal value) {
                this.baseAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad baseNUCAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBaseNUCAmount() {
                return baseNUCAmount;
            }

            /**
             * Define el valor de la propiedad baseNUCAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBaseNUCAmount(BigDecimal value) {
                this.baseNUCAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad taxAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTaxAmount() {
                return taxAmount;
            }

            /**
             * Define el valor de la propiedad taxAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTaxAmount(BigDecimal value) {
                this.taxAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad totalFare.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalFare() {
                return totalFare;
            }

            /**
             * Define el valor de la propiedad totalFare.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalFare(BigDecimal value) {
                this.totalFare = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PassengerType {

            @XmlAttribute(name = "Code")
            protected String code;

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RuleInfo
        extends RuleInfoType
    {


    }

}
