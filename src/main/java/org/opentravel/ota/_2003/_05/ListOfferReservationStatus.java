
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferReservationStatus.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferReservationStatus"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Confirmed"/&amp;gt;
 *     &amp;lt;enumeration value="None"/&amp;gt;
 *     &amp;lt;enumeration value="Tentative"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferReservationStatus")
@XmlEnum
public enum ListOfferReservationStatus {


    /**
     * Reservation has been confirmed.
     * 
     */
    @XmlEnumValue("Confirmed")
    CONFIRMED("Confirmed"),

    /**
     * No reservation.
     * 
     */
    @XmlEnumValue("None")
    NONE("None"),

    /**
     * The reservation is tentative and has been guaranteed.
     * 
     */
    @XmlEnumValue("Tentative")
    TENTATIVE("Tentative"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferReservationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferReservationStatus fromValue(String v) {
        for (ListOfferReservationStatus c: ListOfferReservationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
