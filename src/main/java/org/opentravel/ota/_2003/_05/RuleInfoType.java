
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains summary fare rule information as well as detailed Rule Information for Fare Basis Codes. Information may be actual rules data or the results returned from a rules-based inquiry.
 * 
 * &lt;p&gt;Clase Java para RuleInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RuleInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ResTicketingRules" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AdvResTicketing" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AdvResTicketingType"&amp;gt;
 *                           &amp;lt;attribute name="FirstTicketDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="LastTicketDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="LengthOfStayRules" type="{http://www.opentravel.org/OTA/2003/05}StayRestrictionsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ChargesRules" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="VoluntaryChanges" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="VoluntaryRefunds" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleInfoType", propOrder = {
    "resTicketingRules",
    "lengthOfStayRules",
    "chargesRules",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.RuleInfo.class,
    org.opentravel.ota._2003._05.PrivateFareType.RuleInfo.class,
    org.opentravel.ota._2003._05.FareInfoType.RuleInfo.class
})
public class RuleInfoType {

    @XmlElement(name = "ResTicketingRules")
    protected RuleInfoType.ResTicketingRules resTicketingRules;
    @XmlElement(name = "LengthOfStayRules")
    protected StayRestrictionsType lengthOfStayRules;
    @XmlElement(name = "ChargesRules")
    protected RuleInfoType.ChargesRules chargesRules;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Obtiene el valor de la propiedad resTicketingRules.
     * 
     * @return
     *     possible object is
     *     {@link RuleInfoType.ResTicketingRules }
     *     
     */
    public RuleInfoType.ResTicketingRules getResTicketingRules() {
        return resTicketingRules;
    }

    /**
     * Define el valor de la propiedad resTicketingRules.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleInfoType.ResTicketingRules }
     *     
     */
    public void setResTicketingRules(RuleInfoType.ResTicketingRules value) {
        this.resTicketingRules = value;
    }

    /**
     * Obtiene el valor de la propiedad lengthOfStayRules.
     * 
     * @return
     *     possible object is
     *     {@link StayRestrictionsType }
     *     
     */
    public StayRestrictionsType getLengthOfStayRules() {
        return lengthOfStayRules;
    }

    /**
     * Define el valor de la propiedad lengthOfStayRules.
     * 
     * @param value
     *     allowed object is
     *     {@link StayRestrictionsType }
     *     
     */
    public void setLengthOfStayRules(StayRestrictionsType value) {
        this.lengthOfStayRules = value;
    }

    /**
     * Obtiene el valor de la propiedad chargesRules.
     * 
     * @return
     *     possible object is
     *     {@link RuleInfoType.ChargesRules }
     *     
     */
    public RuleInfoType.ChargesRules getChargesRules() {
        return chargesRules;
    }

    /**
     * Define el valor de la propiedad chargesRules.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleInfoType.ChargesRules }
     *     
     */
    public void setChargesRules(RuleInfoType.ChargesRules value) {
        this.chargesRules = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VoluntaryChanges" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="VoluntaryRefunds" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "voluntaryChanges",
        "voluntaryRefunds"
    })
    public static class ChargesRules {

        @XmlElement(name = "VoluntaryChanges")
        protected VoluntaryChangesType voluntaryChanges;
        @XmlElement(name = "VoluntaryRefunds")
        protected VoluntaryChangesType voluntaryRefunds;

        /**
         * Obtiene el valor de la propiedad voluntaryChanges.
         * 
         * @return
         *     possible object is
         *     {@link VoluntaryChangesType }
         *     
         */
        public VoluntaryChangesType getVoluntaryChanges() {
            return voluntaryChanges;
        }

        /**
         * Define el valor de la propiedad voluntaryChanges.
         * 
         * @param value
         *     allowed object is
         *     {@link VoluntaryChangesType }
         *     
         */
        public void setVoluntaryChanges(VoluntaryChangesType value) {
            this.voluntaryChanges = value;
        }

        /**
         * Obtiene el valor de la propiedad voluntaryRefunds.
         * 
         * @return
         *     possible object is
         *     {@link VoluntaryChangesType }
         *     
         */
        public VoluntaryChangesType getVoluntaryRefunds() {
            return voluntaryRefunds;
        }

        /**
         * Define el valor de la propiedad voluntaryRefunds.
         * 
         * @param value
         *     allowed object is
         *     {@link VoluntaryChangesType }
         *     
         */
        public void setVoluntaryRefunds(VoluntaryChangesType value) {
            this.voluntaryRefunds = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AdvResTicketing" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AdvResTicketingType"&amp;gt;
     *                 &amp;lt;attribute name="FirstTicketDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="LastTicketDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "advResTicketing"
    })
    public static class ResTicketingRules {

        @XmlElement(name = "AdvResTicketing")
        protected RuleInfoType.ResTicketingRules.AdvResTicketing advResTicketing;

        /**
         * Obtiene el valor de la propiedad advResTicketing.
         * 
         * @return
         *     possible object is
         *     {@link RuleInfoType.ResTicketingRules.AdvResTicketing }
         *     
         */
        public RuleInfoType.ResTicketingRules.AdvResTicketing getAdvResTicketing() {
            return advResTicketing;
        }

        /**
         * Define el valor de la propiedad advResTicketing.
         * 
         * @param value
         *     allowed object is
         *     {@link RuleInfoType.ResTicketingRules.AdvResTicketing }
         *     
         */
        public void setAdvResTicketing(RuleInfoType.ResTicketingRules.AdvResTicketing value) {
            this.advResTicketing = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AdvResTicketingType"&amp;gt;
         *       &amp;lt;attribute name="FirstTicketDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="LastTicketDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AdvResTicketing
            extends AdvResTicketingType
        {

            @XmlAttribute(name = "FirstTicketDate")
            protected String firstTicketDate;
            @XmlAttribute(name = "LastTicketDate")
            protected String lastTicketDate;

            /**
             * Obtiene el valor de la propiedad firstTicketDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstTicketDate() {
                return firstTicketDate;
            }

            /**
             * Define el valor de la propiedad firstTicketDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstTicketDate(String value) {
                this.firstTicketDate = value;
            }

            /**
             * Obtiene el valor de la propiedad lastTicketDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastTicketDate() {
                return lastTicketDate;
            }

            /**
             * Define el valor de la propiedad lastTicketDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastTicketDate(String value) {
                this.lastTicketDate = value;
            }

        }

    }

}
