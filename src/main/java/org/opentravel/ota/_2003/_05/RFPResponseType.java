
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides detailed RFP information.
 * 
 * &lt;p&gt;Clase Java para RFP_ResponseType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RFP_ResponseType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RFP_ResponseSegments"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RFP_ResponseSegment" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Sites" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Date" minOccurs="0"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
 *                                                                                 &amp;lt;complexType&amp;gt;
 *                                                                                   &amp;lt;complexContent&amp;gt;
 *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                       &amp;lt;sequence&amp;gt;
 *                                                                                         &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
 *                                                                                           &amp;lt;complexType&amp;gt;
 *                                                                                             &amp;lt;complexContent&amp;gt;
 *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                 &amp;lt;sequence&amp;gt;
 *                                                                                                   &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
 *                                                                                                     &amp;lt;complexType&amp;gt;
 *                                                                                                       &amp;lt;complexContent&amp;gt;
 *                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                           &amp;lt;sequence&amp;gt;
 *                                                                                                             &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
 *                                                                                                               &amp;lt;complexType&amp;gt;
 *                                                                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                                     &amp;lt;sequence&amp;gt;
 *                                                                                                                       &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
 *                                                                                                                         &amp;lt;complexType&amp;gt;
 *                                                                                                                           &amp;lt;complexContent&amp;gt;
 *                                                                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                                               &amp;lt;sequence&amp;gt;
 *                                                                                                                                 &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
 *                                                                                                                                   &amp;lt;complexType&amp;gt;
 *                                                                                                                                     &amp;lt;complexContent&amp;gt;
 *                                                                                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                                                         &amp;lt;sequence&amp;gt;
 *                                                                                                                                           &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
 *                                                                                                                                           &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
 *                                                                                                                                         &amp;lt;/sequence&amp;gt;
 *                                                                                                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                                                                                         &amp;lt;attribute name="OccupancyRate"&amp;gt;
 *                                                                                                                                           &amp;lt;simpleType&amp;gt;
 *                                                                                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                                                                               &amp;lt;enumeration value="Flat"/&amp;gt;
 *                                                                                                                                               &amp;lt;enumeration value="Single"/&amp;gt;
 *                                                                                                                                               &amp;lt;enumeration value="Double"/&amp;gt;
 *                                                                                                                                               &amp;lt;enumeration value="Triple"/&amp;gt;
 *                                                                                                                                               &amp;lt;enumeration value="Quad"/&amp;gt;
 *                                                                                                                                             &amp;lt;/restriction&amp;gt;
 *                                                                                                                                           &amp;lt;/simpleType&amp;gt;
 *                                                                                                                                         &amp;lt;/attribute&amp;gt;
 *                                                                                                                                       &amp;lt;/restriction&amp;gt;
 *                                                                                                                                     &amp;lt;/complexContent&amp;gt;
 *                                                                                                                                   &amp;lt;/complexType&amp;gt;
 *                                                                                                                                 &amp;lt;/element&amp;gt;
 *                                                                                                                               &amp;lt;/sequence&amp;gt;
 *                                                                                                                             &amp;lt;/restriction&amp;gt;
 *                                                                                                                           &amp;lt;/complexContent&amp;gt;
 *                                                                                                                         &amp;lt;/complexType&amp;gt;
 *                                                                                                                       &amp;lt;/element&amp;gt;
 *                                                                                                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                                                                                     &amp;lt;/sequence&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                                                                               &amp;lt;/complexType&amp;gt;
 *                                                                                                             &amp;lt;/element&amp;gt;
 *                                                                                                           &amp;lt;/sequence&amp;gt;
 *                                                                                                         &amp;lt;/restriction&amp;gt;
 *                                                                                                       &amp;lt;/complexContent&amp;gt;
 *                                                                                                     &amp;lt;/complexType&amp;gt;
 *                                                                                                   &amp;lt;/element&amp;gt;
 *                                                                                                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                                                                 &amp;lt;/sequence&amp;gt;
 *                                                                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                                                                                 &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
 *                                                                                                   &amp;lt;simpleType&amp;gt;
 *                                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                                       &amp;lt;enumeration value="First"/&amp;gt;
 *                                                                                                       &amp;lt;enumeration value="Second"/&amp;gt;
 *                                                                                                     &amp;lt;/restriction&amp;gt;
 *                                                                                                   &amp;lt;/simpleType&amp;gt;
 *                                                                                                 &amp;lt;/attribute&amp;gt;
 *                                                                                               &amp;lt;/restriction&amp;gt;
 *                                                                                             &amp;lt;/complexContent&amp;gt;
 *                                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                                         &amp;lt;/element&amp;gt;
 *                                                                                       &amp;lt;/sequence&amp;gt;
 *                                                                                     &amp;lt;/restriction&amp;gt;
 *                                                                                   &amp;lt;/complexContent&amp;gt;
 *                                                                                 &amp;lt;/complexType&amp;gt;
 *                                                                               &amp;lt;/element&amp;gt;
 *                                                                               &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
 *                                                                               &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                     &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
 *                                                                                 &amp;lt;complexType&amp;gt;
 *                                                                                   &amp;lt;complexContent&amp;gt;
 *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                       &amp;lt;sequence&amp;gt;
 *                                                                                         &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
 *                                                                                           &amp;lt;complexType&amp;gt;
 *                                                                                             &amp;lt;complexContent&amp;gt;
 *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                 &amp;lt;sequence&amp;gt;
 *                                                                                                   &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
 *                                                                                                     &amp;lt;complexType&amp;gt;
 *                                                                                                       &amp;lt;complexContent&amp;gt;
 *                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                           &amp;lt;sequence&amp;gt;
 *                                                                                                             &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
 *                                                                                                               &amp;lt;complexType&amp;gt;
 *                                                                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                                     &amp;lt;sequence minOccurs="0"&amp;gt;
 *                                                                                                                       &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
 *                                                                                                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                                                                                     &amp;lt;/sequence&amp;gt;
 *                                                                                                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                                                                     &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
 *                                                                                                                       &amp;lt;simpleType&amp;gt;
 *                                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                                                           &amp;lt;enumeration value="First"/&amp;gt;
 *                                                                                                                           &amp;lt;enumeration value="Second"/&amp;gt;
 *                                                                                                                         &amp;lt;/restriction&amp;gt;
 *                                                                                                                       &amp;lt;/simpleType&amp;gt;
 *                                                                                                                     &amp;lt;/attribute&amp;gt;
 *                                                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                                                                               &amp;lt;/complexType&amp;gt;
 *                                                                                                             &amp;lt;/element&amp;gt;
 *                                                                                                           &amp;lt;/sequence&amp;gt;
 *                                                                                                           &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                                         &amp;lt;/restriction&amp;gt;
 *                                                                                                       &amp;lt;/complexContent&amp;gt;
 *                                                                                                     &amp;lt;/complexType&amp;gt;
 *                                                                                                   &amp;lt;/element&amp;gt;
 *                                                                                                   &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
 *                                                                                                 &amp;lt;/sequence&amp;gt;
 *                                                                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                                                                               &amp;lt;/restriction&amp;gt;
 *                                                                                             &amp;lt;/complexContent&amp;gt;
 *                                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                                         &amp;lt;/element&amp;gt;
 *                                                                                       &amp;lt;/sequence&amp;gt;
 *                                                                                     &amp;lt;/restriction&amp;gt;
 *                                                                                   &amp;lt;/complexContent&amp;gt;
 *                                                                                 &amp;lt;/complexType&amp;gt;
 *                                                                               &amp;lt;/element&amp;gt;
 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                             &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
 *                                                                     &amp;lt;simpleType&amp;gt;
 *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                         &amp;lt;enumeration value="Primary"/&amp;gt;
 *                                                                         &amp;lt;enumeration value="Alternate"/&amp;gt;
 *                                                                         &amp;lt;enumeration value="Other"/&amp;gt;
 *                                                                       &amp;lt;/restriction&amp;gt;
 *                                                                     &amp;lt;/simpleType&amp;gt;
 *                                                                   &amp;lt;/attribute&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;element name="SiteID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;element name="InsuranceInfos" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="InsuranceInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="References" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Reference" maxOccurs="99"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                   &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                   &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                 &amp;lt;/extension&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Answers" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Answer" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                                               &amp;lt;attribute name="CrossSellIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;attribute name="SimilarEventCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="RFP_ID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MessageID" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RFP_ResponseType", propOrder = {
    "rfpResponseSegments",
    "messageID"
})
@XmlSeeAlso({
    OTAHotelRFPMeetingNotifRQ.class
})
public class RFPResponseType {

    @XmlElement(name = "RFP_ResponseSegments", required = true)
    protected RFPResponseType.RFPResponseSegments rfpResponseSegments;
    @XmlElement(name = "MessageID")
    protected RFPResponseType.MessageID messageID;

    /**
     * Obtiene el valor de la propiedad rfpResponseSegments.
     * 
     * @return
     *     possible object is
     *     {@link RFPResponseType.RFPResponseSegments }
     *     
     */
    public RFPResponseType.RFPResponseSegments getRFPResponseSegments() {
        return rfpResponseSegments;
    }

    /**
     * Define el valor de la propiedad rfpResponseSegments.
     * 
     * @param value
     *     allowed object is
     *     {@link RFPResponseType.RFPResponseSegments }
     *     
     */
    public void setRFPResponseSegments(RFPResponseType.RFPResponseSegments value) {
        this.rfpResponseSegments = value;
    }

    /**
     * Obtiene el valor de la propiedad messageID.
     * 
     * @return
     *     possible object is
     *     {@link RFPResponseType.MessageID }
     *     
     */
    public RFPResponseType.MessageID getMessageID() {
        return messageID;
    }

    /**
     * Define el valor de la propiedad messageID.
     * 
     * @param value
     *     allowed object is
     *     {@link RFPResponseType.MessageID }
     *     
     */
    public void setMessageID(RFPResponseType.MessageID value) {
        this.messageID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MessageID
        extends UniqueIDType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RFP_ResponseSegment" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Sites" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Date" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
     *                                                                       &amp;lt;complexType&amp;gt;
     *                                                                         &amp;lt;complexContent&amp;gt;
     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                             &amp;lt;sequence&amp;gt;
     *                                                                               &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
     *                                                                                 &amp;lt;complexType&amp;gt;
     *                                                                                   &amp;lt;complexContent&amp;gt;
     *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                       &amp;lt;sequence&amp;gt;
     *                                                                                         &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
     *                                                                                           &amp;lt;complexType&amp;gt;
     *                                                                                             &amp;lt;complexContent&amp;gt;
     *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                                 &amp;lt;sequence&amp;gt;
     *                                                                                                   &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
     *                                                                                                     &amp;lt;complexType&amp;gt;
     *                                                                                                       &amp;lt;complexContent&amp;gt;
     *                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                                           &amp;lt;sequence&amp;gt;
     *                                                                                                             &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
     *                                                                                                               &amp;lt;complexType&amp;gt;
     *                                                                                                                 &amp;lt;complexContent&amp;gt;
     *                                                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                                                     &amp;lt;sequence&amp;gt;
     *                                                                                                                       &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
     *                                                                                                                         &amp;lt;complexType&amp;gt;
     *                                                                                                                           &amp;lt;complexContent&amp;gt;
     *                                                                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                                                               &amp;lt;sequence&amp;gt;
     *                                                                                                                                 &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
     *                                                                                                                                 &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
     *                                                                                                                               &amp;lt;/sequence&amp;gt;
     *                                                                                                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                                                                                               &amp;lt;attribute name="OccupancyRate"&amp;gt;
     *                                                                                                                                 &amp;lt;simpleType&amp;gt;
     *                                                                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                                                                                     &amp;lt;enumeration value="Flat"/&amp;gt;
     *                                                                                                                                     &amp;lt;enumeration value="Single"/&amp;gt;
     *                                                                                                                                     &amp;lt;enumeration value="Double"/&amp;gt;
     *                                                                                                                                     &amp;lt;enumeration value="Triple"/&amp;gt;
     *                                                                                                                                     &amp;lt;enumeration value="Quad"/&amp;gt;
     *                                                                                                                                   &amp;lt;/restriction&amp;gt;
     *                                                                                                                                 &amp;lt;/simpleType&amp;gt;
     *                                                                                                                               &amp;lt;/attribute&amp;gt;
     *                                                                                                                             &amp;lt;/restriction&amp;gt;
     *                                                                                                                           &amp;lt;/complexContent&amp;gt;
     *                                                                                                                         &amp;lt;/complexType&amp;gt;
     *                                                                                                                       &amp;lt;/element&amp;gt;
     *                                                                                                                     &amp;lt;/sequence&amp;gt;
     *                                                                                                                   &amp;lt;/restriction&amp;gt;
     *                                                                                                                 &amp;lt;/complexContent&amp;gt;
     *                                                                                                               &amp;lt;/complexType&amp;gt;
     *                                                                                                             &amp;lt;/element&amp;gt;
     *                                                                                                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                                                                                           &amp;lt;/sequence&amp;gt;
     *                                                                                                           &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                                                                         &amp;lt;/restriction&amp;gt;
     *                                                                                                       &amp;lt;/complexContent&amp;gt;
     *                                                                                                     &amp;lt;/complexType&amp;gt;
     *                                                                                                   &amp;lt;/element&amp;gt;
     *                                                                                                 &amp;lt;/sequence&amp;gt;
     *                                                                                               &amp;lt;/restriction&amp;gt;
     *                                                                                             &amp;lt;/complexContent&amp;gt;
     *                                                                                           &amp;lt;/complexType&amp;gt;
     *                                                                                         &amp;lt;/element&amp;gt;
     *                                                                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                                                                       &amp;lt;/sequence&amp;gt;
     *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                                                                       &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
     *                                                                                         &amp;lt;simpleType&amp;gt;
     *                                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                                             &amp;lt;enumeration value="First"/&amp;gt;
     *                                                                                             &amp;lt;enumeration value="Second"/&amp;gt;
     *                                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                                         &amp;lt;/simpleType&amp;gt;
     *                                                                                       &amp;lt;/attribute&amp;gt;
     *                                                                                     &amp;lt;/restriction&amp;gt;
     *                                                                                   &amp;lt;/complexContent&amp;gt;
     *                                                                                 &amp;lt;/complexType&amp;gt;
     *                                                                               &amp;lt;/element&amp;gt;
     *                                                                             &amp;lt;/sequence&amp;gt;
     *                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                         &amp;lt;/complexContent&amp;gt;
     *                                                                       &amp;lt;/complexType&amp;gt;
     *                                                                     &amp;lt;/element&amp;gt;
     *                                                                     &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
     *                                                                       &amp;lt;complexType&amp;gt;
     *                                                                         &amp;lt;complexContent&amp;gt;
     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                             &amp;lt;sequence&amp;gt;
     *                                                                               &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
     *                                                                                 &amp;lt;complexType&amp;gt;
     *                                                                                   &amp;lt;complexContent&amp;gt;
     *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                       &amp;lt;sequence&amp;gt;
     *                                                                                         &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
     *                                                                                           &amp;lt;complexType&amp;gt;
     *                                                                                             &amp;lt;complexContent&amp;gt;
     *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                                 &amp;lt;sequence&amp;gt;
     *                                                                                                   &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
     *                                                                                                     &amp;lt;complexType&amp;gt;
     *                                                                                                       &amp;lt;complexContent&amp;gt;
     *                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                                           &amp;lt;sequence minOccurs="0"&amp;gt;
     *                                                                                                             &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
     *                                                                                                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                                                                                           &amp;lt;/sequence&amp;gt;
     *                                                                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                                                                                                           &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                                                                           &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
     *                                                                                                             &amp;lt;simpleType&amp;gt;
     *                                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                                                                 &amp;lt;enumeration value="First"/&amp;gt;
     *                                                                                                                 &amp;lt;enumeration value="Second"/&amp;gt;
     *                                                                                                               &amp;lt;/restriction&amp;gt;
     *                                                                                                             &amp;lt;/simpleType&amp;gt;
     *                                                                                                           &amp;lt;/attribute&amp;gt;
     *                                                                                                         &amp;lt;/restriction&amp;gt;
     *                                                                                                       &amp;lt;/complexContent&amp;gt;
     *                                                                                                     &amp;lt;/complexType&amp;gt;
     *                                                                                                   &amp;lt;/element&amp;gt;
     *                                                                                                 &amp;lt;/sequence&amp;gt;
     *                                                                                                 &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                                               &amp;lt;/restriction&amp;gt;
     *                                                                                             &amp;lt;/complexContent&amp;gt;
     *                                                                                           &amp;lt;/complexType&amp;gt;
     *                                                                                         &amp;lt;/element&amp;gt;
     *                                                                                         &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
     *                                                                                       &amp;lt;/sequence&amp;gt;
     *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                                                                     &amp;lt;/restriction&amp;gt;
     *                                                                                   &amp;lt;/complexContent&amp;gt;
     *                                                                                 &amp;lt;/complexType&amp;gt;
     *                                                                               &amp;lt;/element&amp;gt;
     *                                                                             &amp;lt;/sequence&amp;gt;
     *                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                         &amp;lt;/complexContent&amp;gt;
     *                                                                       &amp;lt;/complexType&amp;gt;
     *                                                                     &amp;lt;/element&amp;gt;
     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                   &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
     *                                                           &amp;lt;simpleType&amp;gt;
     *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                               &amp;lt;enumeration value="Primary"/&amp;gt;
     *                                                               &amp;lt;enumeration value="Alternate"/&amp;gt;
     *                                                               &amp;lt;enumeration value="Other"/&amp;gt;
     *                                                             &amp;lt;/restriction&amp;gt;
     *                                                           &amp;lt;/simpleType&amp;gt;
     *                                                         &amp;lt;/attribute&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="SiteID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="InsuranceInfos" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="InsuranceInfo" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="References" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Reference" maxOccurs="99"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                         &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                         &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Answers" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Answer" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="CrossSellIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="SimilarEventCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RFP_ID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rfpResponseSegment"
    })
    public static class RFPResponseSegments {

        @XmlElement(name = "RFP_ResponseSegment", required = true)
        protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment> rfpResponseSegment;

        /**
         * Gets the value of the rfpResponseSegment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rfpResponseSegment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRFPResponseSegment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment }
         * 
         * 
         */
        public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment> getRFPResponseSegment() {
            if (rfpResponseSegment == null) {
                rfpResponseSegment = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment>();
            }
            return this.rfpResponseSegment;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Sites" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Date" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
         *                                                             &amp;lt;complexType&amp;gt;
         *                                                               &amp;lt;complexContent&amp;gt;
         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                   &amp;lt;sequence&amp;gt;
         *                                                                     &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
         *                                                                       &amp;lt;complexType&amp;gt;
         *                                                                         &amp;lt;complexContent&amp;gt;
         *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                             &amp;lt;sequence&amp;gt;
         *                                                                               &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
         *                                                                                 &amp;lt;complexType&amp;gt;
         *                                                                                   &amp;lt;complexContent&amp;gt;
         *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                                       &amp;lt;sequence&amp;gt;
         *                                                                                         &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
         *                                                                                           &amp;lt;complexType&amp;gt;
         *                                                                                             &amp;lt;complexContent&amp;gt;
         *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                                                 &amp;lt;sequence&amp;gt;
         *                                                                                                   &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
         *                                                                                                     &amp;lt;complexType&amp;gt;
         *                                                                                                       &amp;lt;complexContent&amp;gt;
         *                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                                                           &amp;lt;sequence&amp;gt;
         *                                                                                                             &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
         *                                                                                                               &amp;lt;complexType&amp;gt;
         *                                                                                                                 &amp;lt;complexContent&amp;gt;
         *                                                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                                                                     &amp;lt;sequence&amp;gt;
         *                                                                                                                       &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
         *                                                                                                                       &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
         *                                                                                                                     &amp;lt;/sequence&amp;gt;
         *                                                                                                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                                                                                                     &amp;lt;attribute name="OccupancyRate"&amp;gt;
         *                                                                                                                       &amp;lt;simpleType&amp;gt;
         *                                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                                                                                           &amp;lt;enumeration value="Flat"/&amp;gt;
         *                                                                                                                           &amp;lt;enumeration value="Single"/&amp;gt;
         *                                                                                                                           &amp;lt;enumeration value="Double"/&amp;gt;
         *                                                                                                                           &amp;lt;enumeration value="Triple"/&amp;gt;
         *                                                                                                                           &amp;lt;enumeration value="Quad"/&amp;gt;
         *                                                                                                                         &amp;lt;/restriction&amp;gt;
         *                                                                                                                       &amp;lt;/simpleType&amp;gt;
         *                                                                                                                     &amp;lt;/attribute&amp;gt;
         *                                                                                                                   &amp;lt;/restriction&amp;gt;
         *                                                                                                                 &amp;lt;/complexContent&amp;gt;
         *                                                                                                               &amp;lt;/complexType&amp;gt;
         *                                                                                                             &amp;lt;/element&amp;gt;
         *                                                                                                           &amp;lt;/sequence&amp;gt;
         *                                                                                                         &amp;lt;/restriction&amp;gt;
         *                                                                                                       &amp;lt;/complexContent&amp;gt;
         *                                                                                                     &amp;lt;/complexType&amp;gt;
         *                                                                                                   &amp;lt;/element&amp;gt;
         *                                                                                                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                                                                                                 &amp;lt;/sequence&amp;gt;
         *                                                                                                 &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                                                                               &amp;lt;/restriction&amp;gt;
         *                                                                                             &amp;lt;/complexContent&amp;gt;
         *                                                                                           &amp;lt;/complexType&amp;gt;
         *                                                                                         &amp;lt;/element&amp;gt;
         *                                                                                       &amp;lt;/sequence&amp;gt;
         *                                                                                     &amp;lt;/restriction&amp;gt;
         *                                                                                   &amp;lt;/complexContent&amp;gt;
         *                                                                                 &amp;lt;/complexType&amp;gt;
         *                                                                               &amp;lt;/element&amp;gt;
         *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                                                                             &amp;lt;/sequence&amp;gt;
         *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                                                                             &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
         *                                                                               &amp;lt;simpleType&amp;gt;
         *                                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                                                   &amp;lt;enumeration value="First"/&amp;gt;
         *                                                                                   &amp;lt;enumeration value="Second"/&amp;gt;
         *                                                                                 &amp;lt;/restriction&amp;gt;
         *                                                                               &amp;lt;/simpleType&amp;gt;
         *                                                                             &amp;lt;/attribute&amp;gt;
         *                                                                           &amp;lt;/restriction&amp;gt;
         *                                                                         &amp;lt;/complexContent&amp;gt;
         *                                                                       &amp;lt;/complexType&amp;gt;
         *                                                                     &amp;lt;/element&amp;gt;
         *                                                                   &amp;lt;/sequence&amp;gt;
         *                                                                 &amp;lt;/restriction&amp;gt;
         *                                                               &amp;lt;/complexContent&amp;gt;
         *                                                             &amp;lt;/complexType&amp;gt;
         *                                                           &amp;lt;/element&amp;gt;
         *                                                           &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
         *                                                             &amp;lt;complexType&amp;gt;
         *                                                               &amp;lt;complexContent&amp;gt;
         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                   &amp;lt;sequence&amp;gt;
         *                                                                     &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
         *                                                                       &amp;lt;complexType&amp;gt;
         *                                                                         &amp;lt;complexContent&amp;gt;
         *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                             &amp;lt;sequence&amp;gt;
         *                                                                               &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
         *                                                                                 &amp;lt;complexType&amp;gt;
         *                                                                                   &amp;lt;complexContent&amp;gt;
         *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                                       &amp;lt;sequence&amp;gt;
         *                                                                                         &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
         *                                                                                           &amp;lt;complexType&amp;gt;
         *                                                                                             &amp;lt;complexContent&amp;gt;
         *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                                                 &amp;lt;sequence minOccurs="0"&amp;gt;
         *                                                                                                   &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
         *                                                                                                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                                                                                                 &amp;lt;/sequence&amp;gt;
         *                                                                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *                                                                                                 &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                                                                                 &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
         *                                                                                                   &amp;lt;simpleType&amp;gt;
         *                                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                                                                       &amp;lt;enumeration value="First"/&amp;gt;
         *                                                                                                       &amp;lt;enumeration value="Second"/&amp;gt;
         *                                                                                                     &amp;lt;/restriction&amp;gt;
         *                                                                                                   &amp;lt;/simpleType&amp;gt;
         *                                                                                                 &amp;lt;/attribute&amp;gt;
         *                                                                                               &amp;lt;/restriction&amp;gt;
         *                                                                                             &amp;lt;/complexContent&amp;gt;
         *                                                                                           &amp;lt;/complexType&amp;gt;
         *                                                                                         &amp;lt;/element&amp;gt;
         *                                                                                       &amp;lt;/sequence&amp;gt;
         *                                                                                       &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                                     &amp;lt;/restriction&amp;gt;
         *                                                                                   &amp;lt;/complexContent&amp;gt;
         *                                                                                 &amp;lt;/complexType&amp;gt;
         *                                                                               &amp;lt;/element&amp;gt;
         *                                                                               &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
         *                                                                             &amp;lt;/sequence&amp;gt;
         *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                                                                           &amp;lt;/restriction&amp;gt;
         *                                                                         &amp;lt;/complexContent&amp;gt;
         *                                                                       &amp;lt;/complexType&amp;gt;
         *                                                                     &amp;lt;/element&amp;gt;
         *                                                                   &amp;lt;/sequence&amp;gt;
         *                                                                 &amp;lt;/restriction&amp;gt;
         *                                                               &amp;lt;/complexContent&amp;gt;
         *                                                             &amp;lt;/complexType&amp;gt;
         *                                                           &amp;lt;/element&amp;gt;
         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                         &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
         *                                                 &amp;lt;simpleType&amp;gt;
         *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                     &amp;lt;enumeration value="Primary"/&amp;gt;
         *                                                     &amp;lt;enumeration value="Alternate"/&amp;gt;
         *                                                     &amp;lt;enumeration value="Other"/&amp;gt;
         *                                                   &amp;lt;/restriction&amp;gt;
         *                                                 &amp;lt;/simpleType&amp;gt;
         *                                               &amp;lt;/attribute&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="SiteID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="InsuranceInfos" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="InsuranceInfo" maxOccurs="99" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="References" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Reference" maxOccurs="99"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                               &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                               &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Answers" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Answer" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *                           &amp;lt;attribute name="CrossSellIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="SimilarEventCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RFP_ID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="9" minOccurs="0"/&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "profiles",
            "sites",
            "comment",
            "rfpid",
            "tpaExtensions"
        })
        public static class RFPResponseSegment {

            @XmlElement(name = "Profiles")
            protected ProfilesType profiles;
            @XmlElement(name = "Sites")
            protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites sites;
            @XmlElement(name = "Comment")
            protected List<ParagraphType> comment;
            @XmlElement(name = "RFP_ID")
            protected List<UniqueIDType> rfpid;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;

            /**
             * Obtiene el valor de la propiedad profiles.
             * 
             * @return
             *     possible object is
             *     {@link ProfilesType }
             *     
             */
            public ProfilesType getProfiles() {
                return profiles;
            }

            /**
             * Define el valor de la propiedad profiles.
             * 
             * @param value
             *     allowed object is
             *     {@link ProfilesType }
             *     
             */
            public void setProfiles(ProfilesType value) {
                this.profiles = value;
            }

            /**
             * Obtiene el valor de la propiedad sites.
             * 
             * @return
             *     possible object is
             *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites }
             *     
             */
            public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites getSites() {
                return sites;
            }

            /**
             * Define el valor de la propiedad sites.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites }
             *     
             */
            public void setSites(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites value) {
                this.sites = value;
            }

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<ParagraphType>();
                }
                return this.comment;
            }

            /**
             * Gets the value of the rfpid property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rfpid property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRFPID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link UniqueIDType }
             * 
             * 
             */
            public List<UniqueIDType> getRFPID() {
                if (rfpid == null) {
                    rfpid = new ArrayList<UniqueIDType>();
                }
                return this.rfpid;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Date" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
             *                                                   &amp;lt;complexType&amp;gt;
             *                                                     &amp;lt;complexContent&amp;gt;
             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                         &amp;lt;sequence&amp;gt;
             *                                                           &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
             *                                                             &amp;lt;complexType&amp;gt;
             *                                                               &amp;lt;complexContent&amp;gt;
             *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                   &amp;lt;sequence&amp;gt;
             *                                                                     &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
             *                                                                       &amp;lt;complexType&amp;gt;
             *                                                                         &amp;lt;complexContent&amp;gt;
             *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                             &amp;lt;sequence&amp;gt;
             *                                                                               &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
             *                                                                                 &amp;lt;complexType&amp;gt;
             *                                                                                   &amp;lt;complexContent&amp;gt;
             *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                                       &amp;lt;sequence&amp;gt;
             *                                                                                         &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
             *                                                                                           &amp;lt;complexType&amp;gt;
             *                                                                                             &amp;lt;complexContent&amp;gt;
             *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                                                 &amp;lt;sequence&amp;gt;
             *                                                                                                   &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
             *                                                                                                     &amp;lt;complexType&amp;gt;
             *                                                                                                       &amp;lt;complexContent&amp;gt;
             *                                                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                                                           &amp;lt;sequence&amp;gt;
             *                                                                                                             &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
             *                                                                                                             &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
             *                                                                                                           &amp;lt;/sequence&amp;gt;
             *                                                                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                                                                                           &amp;lt;attribute name="OccupancyRate"&amp;gt;
             *                                                                                                             &amp;lt;simpleType&amp;gt;
             *                                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                                                                                 &amp;lt;enumeration value="Flat"/&amp;gt;
             *                                                                                                                 &amp;lt;enumeration value="Single"/&amp;gt;
             *                                                                                                                 &amp;lt;enumeration value="Double"/&amp;gt;
             *                                                                                                                 &amp;lt;enumeration value="Triple"/&amp;gt;
             *                                                                                                                 &amp;lt;enumeration value="Quad"/&amp;gt;
             *                                                                                                               &amp;lt;/restriction&amp;gt;
             *                                                                                                             &amp;lt;/simpleType&amp;gt;
             *                                                                                                           &amp;lt;/attribute&amp;gt;
             *                                                                                                         &amp;lt;/restriction&amp;gt;
             *                                                                                                       &amp;lt;/complexContent&amp;gt;
             *                                                                                                     &amp;lt;/complexType&amp;gt;
             *                                                                                                   &amp;lt;/element&amp;gt;
             *                                                                                                 &amp;lt;/sequence&amp;gt;
             *                                                                                               &amp;lt;/restriction&amp;gt;
             *                                                                                             &amp;lt;/complexContent&amp;gt;
             *                                                                                           &amp;lt;/complexType&amp;gt;
             *                                                                                         &amp;lt;/element&amp;gt;
             *                                                                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                                                                                       &amp;lt;/sequence&amp;gt;
             *                                                                                       &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                                                                     &amp;lt;/restriction&amp;gt;
             *                                                                                   &amp;lt;/complexContent&amp;gt;
             *                                                                                 &amp;lt;/complexType&amp;gt;
             *                                                                               &amp;lt;/element&amp;gt;
             *                                                                             &amp;lt;/sequence&amp;gt;
             *                                                                           &amp;lt;/restriction&amp;gt;
             *                                                                         &amp;lt;/complexContent&amp;gt;
             *                                                                       &amp;lt;/complexType&amp;gt;
             *                                                                     &amp;lt;/element&amp;gt;
             *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                                                                   &amp;lt;/sequence&amp;gt;
             *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                                                                   &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
             *                                                                     &amp;lt;simpleType&amp;gt;
             *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                                         &amp;lt;enumeration value="First"/&amp;gt;
             *                                                                         &amp;lt;enumeration value="Second"/&amp;gt;
             *                                                                       &amp;lt;/restriction&amp;gt;
             *                                                                     &amp;lt;/simpleType&amp;gt;
             *                                                                   &amp;lt;/attribute&amp;gt;
             *                                                                 &amp;lt;/restriction&amp;gt;
             *                                                               &amp;lt;/complexContent&amp;gt;
             *                                                             &amp;lt;/complexType&amp;gt;
             *                                                           &amp;lt;/element&amp;gt;
             *                                                         &amp;lt;/sequence&amp;gt;
             *                                                       &amp;lt;/restriction&amp;gt;
             *                                                     &amp;lt;/complexContent&amp;gt;
             *                                                   &amp;lt;/complexType&amp;gt;
             *                                                 &amp;lt;/element&amp;gt;
             *                                                 &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
             *                                                   &amp;lt;complexType&amp;gt;
             *                                                     &amp;lt;complexContent&amp;gt;
             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                         &amp;lt;sequence&amp;gt;
             *                                                           &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
             *                                                             &amp;lt;complexType&amp;gt;
             *                                                               &amp;lt;complexContent&amp;gt;
             *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                   &amp;lt;sequence&amp;gt;
             *                                                                     &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
             *                                                                       &amp;lt;complexType&amp;gt;
             *                                                                         &amp;lt;complexContent&amp;gt;
             *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                             &amp;lt;sequence&amp;gt;
             *                                                                               &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
             *                                                                                 &amp;lt;complexType&amp;gt;
             *                                                                                   &amp;lt;complexContent&amp;gt;
             *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                                       &amp;lt;sequence minOccurs="0"&amp;gt;
             *                                                                                         &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
             *                                                                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                                                                                       &amp;lt;/sequence&amp;gt;
             *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
             *                                                                                       &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                                                                       &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
             *                                                                                         &amp;lt;simpleType&amp;gt;
             *                                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                                                             &amp;lt;enumeration value="First"/&amp;gt;
             *                                                                                             &amp;lt;enumeration value="Second"/&amp;gt;
             *                                                                                           &amp;lt;/restriction&amp;gt;
             *                                                                                         &amp;lt;/simpleType&amp;gt;
             *                                                                                       &amp;lt;/attribute&amp;gt;
             *                                                                                     &amp;lt;/restriction&amp;gt;
             *                                                                                   &amp;lt;/complexContent&amp;gt;
             *                                                                                 &amp;lt;/complexType&amp;gt;
             *                                                                               &amp;lt;/element&amp;gt;
             *                                                                             &amp;lt;/sequence&amp;gt;
             *                                                                             &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                                           &amp;lt;/restriction&amp;gt;
             *                                                                         &amp;lt;/complexContent&amp;gt;
             *                                                                       &amp;lt;/complexType&amp;gt;
             *                                                                     &amp;lt;/element&amp;gt;
             *                                                                     &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
             *                                                                   &amp;lt;/sequence&amp;gt;
             *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                                                                 &amp;lt;/restriction&amp;gt;
             *                                                               &amp;lt;/complexContent&amp;gt;
             *                                                             &amp;lt;/complexType&amp;gt;
             *                                                           &amp;lt;/element&amp;gt;
             *                                                         &amp;lt;/sequence&amp;gt;
             *                                                       &amp;lt;/restriction&amp;gt;
             *                                                     &amp;lt;/complexContent&amp;gt;
             *                                                   &amp;lt;/complexType&amp;gt;
             *                                                 &amp;lt;/element&amp;gt;
             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                               &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
             *                                       &amp;lt;simpleType&amp;gt;
             *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                           &amp;lt;enumeration value="Primary"/&amp;gt;
             *                                           &amp;lt;enumeration value="Alternate"/&amp;gt;
             *                                           &amp;lt;enumeration value="Other"/&amp;gt;
             *                                         &amp;lt;/restriction&amp;gt;
             *                                       &amp;lt;/simpleType&amp;gt;
             *                                     &amp;lt;/attribute&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="SiteID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="InsuranceInfos" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="InsuranceInfo" maxOccurs="99" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="References" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Reference" maxOccurs="99"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                     &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                     &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Answers" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Answer" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
             *                 &amp;lt;attribute name="CrossSellIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="SimilarEventCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "site"
            })
            public static class Sites {

                @XmlElement(name = "Site", required = true)
                protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site> site;

                /**
                 * Gets the value of the site property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the site property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getSite().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site }
                 * 
                 * 
                 */
                public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site> getSite() {
                    if (site == null) {
                        site = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site>();
                    }
                    return this.site;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Date" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
                 *                                         &amp;lt;complexType&amp;gt;
                 *                                           &amp;lt;complexContent&amp;gt;
                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                               &amp;lt;sequence&amp;gt;
                 *                                                 &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
                 *                                                   &amp;lt;complexType&amp;gt;
                 *                                                     &amp;lt;complexContent&amp;gt;
                 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                         &amp;lt;sequence&amp;gt;
                 *                                                           &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
                 *                                                             &amp;lt;complexType&amp;gt;
                 *                                                               &amp;lt;complexContent&amp;gt;
                 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                                   &amp;lt;sequence&amp;gt;
                 *                                                                     &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                 *                                                                       &amp;lt;complexType&amp;gt;
                 *                                                                         &amp;lt;complexContent&amp;gt;
                 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                                             &amp;lt;sequence&amp;gt;
                 *                                                                               &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                 *                                                                                 &amp;lt;complexType&amp;gt;
                 *                                                                                   &amp;lt;complexContent&amp;gt;
                 *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                                                       &amp;lt;sequence&amp;gt;
                 *                                                                                         &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                 *                                                                                           &amp;lt;complexType&amp;gt;
                 *                                                                                             &amp;lt;complexContent&amp;gt;
                 *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                                                                 &amp;lt;sequence&amp;gt;
                 *                                                                                                   &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                 *                                                                                                   &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                 *                                                                                                 &amp;lt;/sequence&amp;gt;
                 *                                                                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                                                                                                 &amp;lt;attribute name="OccupancyRate"&amp;gt;
                 *                                                                                                   &amp;lt;simpleType&amp;gt;
                 *                                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                                                                                       &amp;lt;enumeration value="Flat"/&amp;gt;
                 *                                                                                                       &amp;lt;enumeration value="Single"/&amp;gt;
                 *                                                                                                       &amp;lt;enumeration value="Double"/&amp;gt;
                 *                                                                                                       &amp;lt;enumeration value="Triple"/&amp;gt;
                 *                                                                                                       &amp;lt;enumeration value="Quad"/&amp;gt;
                 *                                                                                                     &amp;lt;/restriction&amp;gt;
                 *                                                                                                   &amp;lt;/simpleType&amp;gt;
                 *                                                                                                 &amp;lt;/attribute&amp;gt;
                 *                                                                                               &amp;lt;/restriction&amp;gt;
                 *                                                                                             &amp;lt;/complexContent&amp;gt;
                 *                                                                                           &amp;lt;/complexType&amp;gt;
                 *                                                                                         &amp;lt;/element&amp;gt;
                 *                                                                                       &amp;lt;/sequence&amp;gt;
                 *                                                                                     &amp;lt;/restriction&amp;gt;
                 *                                                                                   &amp;lt;/complexContent&amp;gt;
                 *                                                                                 &amp;lt;/complexType&amp;gt;
                 *                                                                               &amp;lt;/element&amp;gt;
                 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *                                                                             &amp;lt;/sequence&amp;gt;
                 *                                                                             &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                                                                           &amp;lt;/restriction&amp;gt;
                 *                                                                         &amp;lt;/complexContent&amp;gt;
                 *                                                                       &amp;lt;/complexType&amp;gt;
                 *                                                                     &amp;lt;/element&amp;gt;
                 *                                                                   &amp;lt;/sequence&amp;gt;
                 *                                                                 &amp;lt;/restriction&amp;gt;
                 *                                                               &amp;lt;/complexContent&amp;gt;
                 *                                                             &amp;lt;/complexType&amp;gt;
                 *                                                           &amp;lt;/element&amp;gt;
                 *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *                                                         &amp;lt;/sequence&amp;gt;
                 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *                                                         &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                 *                                                           &amp;lt;simpleType&amp;gt;
                 *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                                               &amp;lt;enumeration value="First"/&amp;gt;
                 *                                                               &amp;lt;enumeration value="Second"/&amp;gt;
                 *                                                             &amp;lt;/restriction&amp;gt;
                 *                                                           &amp;lt;/simpleType&amp;gt;
                 *                                                         &amp;lt;/attribute&amp;gt;
                 *                                                       &amp;lt;/restriction&amp;gt;
                 *                                                     &amp;lt;/complexContent&amp;gt;
                 *                                                   &amp;lt;/complexType&amp;gt;
                 *                                                 &amp;lt;/element&amp;gt;
                 *                                               &amp;lt;/sequence&amp;gt;
                 *                                             &amp;lt;/restriction&amp;gt;
                 *                                           &amp;lt;/complexContent&amp;gt;
                 *                                         &amp;lt;/complexType&amp;gt;
                 *                                       &amp;lt;/element&amp;gt;
                 *                                       &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
                 *                                         &amp;lt;complexType&amp;gt;
                 *                                           &amp;lt;complexContent&amp;gt;
                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                               &amp;lt;sequence&amp;gt;
                 *                                                 &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
                 *                                                   &amp;lt;complexType&amp;gt;
                 *                                                     &amp;lt;complexContent&amp;gt;
                 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                         &amp;lt;sequence&amp;gt;
                 *                                                           &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
                 *                                                             &amp;lt;complexType&amp;gt;
                 *                                                               &amp;lt;complexContent&amp;gt;
                 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                                   &amp;lt;sequence&amp;gt;
                 *                                                                     &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                 *                                                                       &amp;lt;complexType&amp;gt;
                 *                                                                         &amp;lt;complexContent&amp;gt;
                 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                                             &amp;lt;sequence minOccurs="0"&amp;gt;
                 *                                                                               &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *                                                                             &amp;lt;/sequence&amp;gt;
                 *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                 *                                                                             &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                                                                             &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                 *                                                                               &amp;lt;simpleType&amp;gt;
                 *                                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                                                                   &amp;lt;enumeration value="First"/&amp;gt;
                 *                                                                                   &amp;lt;enumeration value="Second"/&amp;gt;
                 *                                                                                 &amp;lt;/restriction&amp;gt;
                 *                                                                               &amp;lt;/simpleType&amp;gt;
                 *                                                                             &amp;lt;/attribute&amp;gt;
                 *                                                                           &amp;lt;/restriction&amp;gt;
                 *                                                                         &amp;lt;/complexContent&amp;gt;
                 *                                                                       &amp;lt;/complexType&amp;gt;
                 *                                                                     &amp;lt;/element&amp;gt;
                 *                                                                   &amp;lt;/sequence&amp;gt;
                 *                                                                   &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                                                 &amp;lt;/restriction&amp;gt;
                 *                                                               &amp;lt;/complexContent&amp;gt;
                 *                                                             &amp;lt;/complexType&amp;gt;
                 *                                                           &amp;lt;/element&amp;gt;
                 *                                                           &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                 *                                                         &amp;lt;/sequence&amp;gt;
                 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *                                                       &amp;lt;/restriction&amp;gt;
                 *                                                     &amp;lt;/complexContent&amp;gt;
                 *                                                   &amp;lt;/complexType&amp;gt;
                 *                                                 &amp;lt;/element&amp;gt;
                 *                                               &amp;lt;/sequence&amp;gt;
                 *                                             &amp;lt;/restriction&amp;gt;
                 *                                           &amp;lt;/complexContent&amp;gt;
                 *                                         &amp;lt;/complexType&amp;gt;
                 *                                       &amp;lt;/element&amp;gt;
                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                     &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
                 *                             &amp;lt;simpleType&amp;gt;
                 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                 &amp;lt;enumeration value="Primary"/&amp;gt;
                 *                                 &amp;lt;enumeration value="Alternate"/&amp;gt;
                 *                                 &amp;lt;enumeration value="Other"/&amp;gt;
                 *                               &amp;lt;/restriction&amp;gt;
                 *                             &amp;lt;/simpleType&amp;gt;
                 *                           &amp;lt;/attribute&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="SiteID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="InsuranceInfos" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="InsuranceInfo" maxOccurs="99" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="References" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Reference" maxOccurs="99"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                           &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                           &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Answers" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Answer" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
                 *       &amp;lt;attribute name="CrossSellIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="SimilarEventCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "responseType",
                    "contactInfos",
                    "dates",
                    "comment",
                    "siteID",
                    "insuranceInfos",
                    "references",
                    "additionalInfos",
                    "answers"
                })
                public static class Site {

                    @XmlElement(name = "ResponseType")
                    protected RFPResponseDetailType responseType;
                    @XmlElement(name = "ContactInfos")
                    protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.ContactInfos contactInfos;
                    @XmlElement(name = "Dates")
                    protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates dates;
                    @XmlElement(name = "Comment")
                    protected List<ParagraphType> comment;
                    @XmlElement(name = "SiteID")
                    protected UniqueIDType siteID;
                    @XmlElement(name = "InsuranceInfos")
                    protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos insuranceInfos;
                    @XmlElement(name = "References")
                    protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References references;
                    @XmlElement(name = "AdditionalInfos")
                    protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos additionalInfos;
                    @XmlElement(name = "Answers")
                    protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Answers answers;
                    @XmlAttribute(name = "CrossSellIndicator")
                    protected Boolean crossSellIndicator;
                    @XmlAttribute(name = "SimilarEventCount")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger similarEventCount;
                    @XmlAttribute(name = "ChainCode")
                    protected String chainCode;
                    @XmlAttribute(name = "BrandCode")
                    protected String brandCode;
                    @XmlAttribute(name = "HotelCode")
                    protected String hotelCode;
                    @XmlAttribute(name = "HotelCityCode")
                    protected String hotelCityCode;
                    @XmlAttribute(name = "HotelName")
                    protected String hotelName;
                    @XmlAttribute(name = "HotelCodeContext")
                    protected String hotelCodeContext;
                    @XmlAttribute(name = "ChainName")
                    protected String chainName;
                    @XmlAttribute(name = "BrandName")
                    protected String brandName;
                    @XmlAttribute(name = "AreaID")
                    protected String areaID;
                    @XmlAttribute(name = "TTIcode")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger ttIcode;

                    /**
                     * Obtiene el valor de la propiedad responseType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseDetailType }
                     *     
                     */
                    public RFPResponseDetailType getResponseType() {
                        return responseType;
                    }

                    /**
                     * Define el valor de la propiedad responseType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseDetailType }
                     *     
                     */
                    public void setResponseType(RFPResponseDetailType value) {
                        this.responseType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad contactInfos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.ContactInfos }
                     *     
                     */
                    public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.ContactInfos getContactInfos() {
                        return contactInfos;
                    }

                    /**
                     * Define el valor de la propiedad contactInfos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.ContactInfos }
                     *     
                     */
                    public void setContactInfos(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.ContactInfos value) {
                        this.contactInfos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dates.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates }
                     *     
                     */
                    public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates getDates() {
                        return dates;
                    }

                    /**
                     * Define el valor de la propiedad dates.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates }
                     *     
                     */
                    public void setDates(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates value) {
                        this.dates = value;
                    }

                    /**
                     * Gets the value of the comment property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getComment().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link ParagraphType }
                     * 
                     * 
                     */
                    public List<ParagraphType> getComment() {
                        if (comment == null) {
                            comment = new ArrayList<ParagraphType>();
                        }
                        return this.comment;
                    }

                    /**
                     * Obtiene el valor de la propiedad siteID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link UniqueIDType }
                     *     
                     */
                    public UniqueIDType getSiteID() {
                        return siteID;
                    }

                    /**
                     * Define el valor de la propiedad siteID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link UniqueIDType }
                     *     
                     */
                    public void setSiteID(UniqueIDType value) {
                        this.siteID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad insuranceInfos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos }
                     *     
                     */
                    public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos getInsuranceInfos() {
                        return insuranceInfos;
                    }

                    /**
                     * Define el valor de la propiedad insuranceInfos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos }
                     *     
                     */
                    public void setInsuranceInfos(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos value) {
                        this.insuranceInfos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad references.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References }
                     *     
                     */
                    public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References getReferences() {
                        return references;
                    }

                    /**
                     * Define el valor de la propiedad references.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References }
                     *     
                     */
                    public void setReferences(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References value) {
                        this.references = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad additionalInfos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos }
                     *     
                     */
                    public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos getAdditionalInfos() {
                        return additionalInfos;
                    }

                    /**
                     * Define el valor de la propiedad additionalInfos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos }
                     *     
                     */
                    public void setAdditionalInfos(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos value) {
                        this.additionalInfos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad answers.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Answers }
                     *     
                     */
                    public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Answers getAnswers() {
                        return answers;
                    }

                    /**
                     * Define el valor de la propiedad answers.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Answers }
                     *     
                     */
                    public void setAnswers(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Answers value) {
                        this.answers = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad crossSellIndicator.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isCrossSellIndicator() {
                        return crossSellIndicator;
                    }

                    /**
                     * Define el valor de la propiedad crossSellIndicator.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setCrossSellIndicator(Boolean value) {
                        this.crossSellIndicator = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad similarEventCount.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getSimilarEventCount() {
                        return similarEventCount;
                    }

                    /**
                     * Define el valor de la propiedad similarEventCount.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setSimilarEventCount(BigInteger value) {
                        this.similarEventCount = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad chainCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getChainCode() {
                        return chainCode;
                    }

                    /**
                     * Define el valor de la propiedad chainCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setChainCode(String value) {
                        this.chainCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad brandCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getBrandCode() {
                        return brandCode;
                    }

                    /**
                     * Define el valor de la propiedad brandCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setBrandCode(String value) {
                        this.brandCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad hotelCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getHotelCode() {
                        return hotelCode;
                    }

                    /**
                     * Define el valor de la propiedad hotelCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setHotelCode(String value) {
                        this.hotelCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad hotelCityCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getHotelCityCode() {
                        return hotelCityCode;
                    }

                    /**
                     * Define el valor de la propiedad hotelCityCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setHotelCityCode(String value) {
                        this.hotelCityCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad hotelName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getHotelName() {
                        return hotelName;
                    }

                    /**
                     * Define el valor de la propiedad hotelName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setHotelName(String value) {
                        this.hotelName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad hotelCodeContext.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getHotelCodeContext() {
                        return hotelCodeContext;
                    }

                    /**
                     * Define el valor de la propiedad hotelCodeContext.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setHotelCodeContext(String value) {
                        this.hotelCodeContext = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad chainName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getChainName() {
                        return chainName;
                    }

                    /**
                     * Define el valor de la propiedad chainName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setChainName(String value) {
                        this.chainName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad brandName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getBrandName() {
                        return brandName;
                    }

                    /**
                     * Define el valor de la propiedad brandName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setBrandName(String value) {
                        this.brandName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad areaID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAreaID() {
                        return areaID;
                    }

                    /**
                     * Define el valor de la propiedad areaID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAreaID(String value) {
                        this.areaID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad ttIcode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getTTIcode() {
                        return ttIcode;
                    }

                    /**
                     * Define el valor de la propiedad ttIcode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setTTIcode(BigInteger value) {
                        this.ttIcode = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "additionalInfo"
                    })
                    public static class AdditionalInfos {

                        @XmlElement(name = "AdditionalInfo", required = true)
                        protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos.AdditionalInfo> additionalInfo;

                        /**
                         * Gets the value of the additionalInfo property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additionalInfo property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getAdditionalInfo().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos.AdditionalInfo }
                         * 
                         * 
                         */
                        public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos.AdditionalInfo> getAdditionalInfo() {
                            if (additionalInfo == null) {
                                additionalInfo = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.AdditionalInfos.AdditionalInfo>();
                            }
                            return this.additionalInfo;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class AdditionalInfo {

                            @XmlAttribute(name = "AddtionalInfoCode")
                            protected String addtionalInfoCode;
                            @XmlAttribute(name = "DeliveryMethodCode")
                            protected String deliveryMethodCode;

                            /**
                             * Obtiene el valor de la propiedad addtionalInfoCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAddtionalInfoCode() {
                                return addtionalInfoCode;
                            }

                            /**
                             * Define el valor de la propiedad addtionalInfoCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAddtionalInfoCode(String value) {
                                this.addtionalInfoCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad deliveryMethodCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDeliveryMethodCode() {
                                return deliveryMethodCode;
                            }

                            /**
                             * Define el valor de la propiedad deliveryMethodCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDeliveryMethodCode(String value) {
                                this.deliveryMethodCode = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Answer" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "answer"
                    })
                    public static class Answers {

                        @XmlElement(name = "Answer", required = true)
                        protected List<CustomQuestionType> answer;

                        /**
                         * Gets the value of the answer property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the answer property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getAnswer().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link CustomQuestionType }
                         * 
                         * 
                         */
                        public List<CustomQuestionType> getAnswer() {
                            if (answer == null) {
                                answer = new ArrayList<CustomQuestionType>();
                            }
                            return this.answer;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "contactInfo"
                    })
                    public static class ContactInfos {

                        @XmlElement(name = "ContactInfo", required = true)
                        protected ContactPersonType contactInfo;

                        /**
                         * Obtiene el valor de la propiedad contactInfo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ContactPersonType }
                         *     
                         */
                        public ContactPersonType getContactInfo() {
                            return contactInfo;
                        }

                        /**
                         * Define el valor de la propiedad contactInfo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ContactPersonType }
                         *     
                         */
                        public void setContactInfo(ContactPersonType value) {
                            this.contactInfo = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Date" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
                     *                               &amp;lt;complexType&amp;gt;
                     *                                 &amp;lt;complexContent&amp;gt;
                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                     &amp;lt;sequence&amp;gt;
                     *                                       &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
                     *                                         &amp;lt;complexType&amp;gt;
                     *                                           &amp;lt;complexContent&amp;gt;
                     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                               &amp;lt;sequence&amp;gt;
                     *                                                 &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
                     *                                                   &amp;lt;complexType&amp;gt;
                     *                                                     &amp;lt;complexContent&amp;gt;
                     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                                         &amp;lt;sequence&amp;gt;
                     *                                                           &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                     *                                                             &amp;lt;complexType&amp;gt;
                     *                                                               &amp;lt;complexContent&amp;gt;
                     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                                                   &amp;lt;sequence&amp;gt;
                     *                                                                     &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                     *                                                                       &amp;lt;complexType&amp;gt;
                     *                                                                         &amp;lt;complexContent&amp;gt;
                     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                                                             &amp;lt;sequence&amp;gt;
                     *                                                                               &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                     *                                                                                 &amp;lt;complexType&amp;gt;
                     *                                                                                   &amp;lt;complexContent&amp;gt;
                     *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                                                                       &amp;lt;sequence&amp;gt;
                     *                                                                                         &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                     *                                                                                         &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                     *                                                                                       &amp;lt;/sequence&amp;gt;
                     *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                                                                                       &amp;lt;attribute name="OccupancyRate"&amp;gt;
                     *                                                                                         &amp;lt;simpleType&amp;gt;
                     *                                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                                                                             &amp;lt;enumeration value="Flat"/&amp;gt;
                     *                                                                                             &amp;lt;enumeration value="Single"/&amp;gt;
                     *                                                                                             &amp;lt;enumeration value="Double"/&amp;gt;
                     *                                                                                             &amp;lt;enumeration value="Triple"/&amp;gt;
                     *                                                                                             &amp;lt;enumeration value="Quad"/&amp;gt;
                     *                                                                                           &amp;lt;/restriction&amp;gt;
                     *                                                                                         &amp;lt;/simpleType&amp;gt;
                     *                                                                                       &amp;lt;/attribute&amp;gt;
                     *                                                                                     &amp;lt;/restriction&amp;gt;
                     *                                                                                   &amp;lt;/complexContent&amp;gt;
                     *                                                                                 &amp;lt;/complexType&amp;gt;
                     *                                                                               &amp;lt;/element&amp;gt;
                     *                                                                             &amp;lt;/sequence&amp;gt;
                     *                                                                           &amp;lt;/restriction&amp;gt;
                     *                                                                         &amp;lt;/complexContent&amp;gt;
                     *                                                                       &amp;lt;/complexType&amp;gt;
                     *                                                                     &amp;lt;/element&amp;gt;
                     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                     *                                                                   &amp;lt;/sequence&amp;gt;
                     *                                                                   &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *                                                                 &amp;lt;/restriction&amp;gt;
                     *                                                               &amp;lt;/complexContent&amp;gt;
                     *                                                             &amp;lt;/complexType&amp;gt;
                     *                                                           &amp;lt;/element&amp;gt;
                     *                                                         &amp;lt;/sequence&amp;gt;
                     *                                                       &amp;lt;/restriction&amp;gt;
                     *                                                     &amp;lt;/complexContent&amp;gt;
                     *                                                   &amp;lt;/complexType&amp;gt;
                     *                                                 &amp;lt;/element&amp;gt;
                     *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                     *                                               &amp;lt;/sequence&amp;gt;
                     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                     *                                               &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                     *                                                 &amp;lt;simpleType&amp;gt;
                     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                                     &amp;lt;enumeration value="First"/&amp;gt;
                     *                                                     &amp;lt;enumeration value="Second"/&amp;gt;
                     *                                                   &amp;lt;/restriction&amp;gt;
                     *                                                 &amp;lt;/simpleType&amp;gt;
                     *                                               &amp;lt;/attribute&amp;gt;
                     *                                             &amp;lt;/restriction&amp;gt;
                     *                                           &amp;lt;/complexContent&amp;gt;
                     *                                         &amp;lt;/complexType&amp;gt;
                     *                                       &amp;lt;/element&amp;gt;
                     *                                     &amp;lt;/sequence&amp;gt;
                     *                                   &amp;lt;/restriction&amp;gt;
                     *                                 &amp;lt;/complexContent&amp;gt;
                     *                               &amp;lt;/complexType&amp;gt;
                     *                             &amp;lt;/element&amp;gt;
                     *                             &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
                     *                               &amp;lt;complexType&amp;gt;
                     *                                 &amp;lt;complexContent&amp;gt;
                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                     &amp;lt;sequence&amp;gt;
                     *                                       &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
                     *                                         &amp;lt;complexType&amp;gt;
                     *                                           &amp;lt;complexContent&amp;gt;
                     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                               &amp;lt;sequence&amp;gt;
                     *                                                 &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
                     *                                                   &amp;lt;complexType&amp;gt;
                     *                                                     &amp;lt;complexContent&amp;gt;
                     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                                         &amp;lt;sequence&amp;gt;
                     *                                                           &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                     *                                                             &amp;lt;complexType&amp;gt;
                     *                                                               &amp;lt;complexContent&amp;gt;
                     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                                                   &amp;lt;sequence minOccurs="0"&amp;gt;
                     *                                                                     &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                     *                                                                   &amp;lt;/sequence&amp;gt;
                     *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                     *                                                                   &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                                                                   &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                     *                                                                     &amp;lt;simpleType&amp;gt;
                     *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                                                         &amp;lt;enumeration value="First"/&amp;gt;
                     *                                                                         &amp;lt;enumeration value="Second"/&amp;gt;
                     *                                                                       &amp;lt;/restriction&amp;gt;
                     *                                                                     &amp;lt;/simpleType&amp;gt;
                     *                                                                   &amp;lt;/attribute&amp;gt;
                     *                                                                 &amp;lt;/restriction&amp;gt;
                     *                                                               &amp;lt;/complexContent&amp;gt;
                     *                                                             &amp;lt;/complexType&amp;gt;
                     *                                                           &amp;lt;/element&amp;gt;
                     *                                                         &amp;lt;/sequence&amp;gt;
                     *                                                         &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                                       &amp;lt;/restriction&amp;gt;
                     *                                                     &amp;lt;/complexContent&amp;gt;
                     *                                                   &amp;lt;/complexType&amp;gt;
                     *                                                 &amp;lt;/element&amp;gt;
                     *                                                 &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                     *                                               &amp;lt;/sequence&amp;gt;
                     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                     *                                             &amp;lt;/restriction&amp;gt;
                     *                                           &amp;lt;/complexContent&amp;gt;
                     *                                         &amp;lt;/complexType&amp;gt;
                     *                                       &amp;lt;/element&amp;gt;
                     *                                     &amp;lt;/sequence&amp;gt;
                     *                                   &amp;lt;/restriction&amp;gt;
                     *                                 &amp;lt;/complexContent&amp;gt;
                     *                               &amp;lt;/complexType&amp;gt;
                     *                             &amp;lt;/element&amp;gt;
                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                           &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
                     *                   &amp;lt;simpleType&amp;gt;
                     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                       &amp;lt;enumeration value="Primary"/&amp;gt;
                     *                       &amp;lt;enumeration value="Alternate"/&amp;gt;
                     *                       &amp;lt;enumeration value="Other"/&amp;gt;
                     *                     &amp;lt;/restriction&amp;gt;
                     *                   &amp;lt;/simpleType&amp;gt;
                     *                 &amp;lt;/attribute&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "date"
                    })
                    public static class Dates {

                        @XmlElement(name = "Date")
                        protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date date;

                        /**
                         * Obtiene el valor de la propiedad date.
                         * 
                         * @return
                         *     possible object is
                         *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date }
                         *     
                         */
                        public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date getDate() {
                            return date;
                        }

                        /**
                         * Define el valor de la propiedad date.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date }
                         *     
                         */
                        public void setDate(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date value) {
                            this.date = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
                         *                     &amp;lt;complexType&amp;gt;
                         *                       &amp;lt;complexContent&amp;gt;
                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                           &amp;lt;sequence&amp;gt;
                         *                             &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
                         *                               &amp;lt;complexType&amp;gt;
                         *                                 &amp;lt;complexContent&amp;gt;
                         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                     &amp;lt;sequence&amp;gt;
                         *                                       &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
                         *                                         &amp;lt;complexType&amp;gt;
                         *                                           &amp;lt;complexContent&amp;gt;
                         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                               &amp;lt;sequence&amp;gt;
                         *                                                 &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                         *                                                   &amp;lt;complexType&amp;gt;
                         *                                                     &amp;lt;complexContent&amp;gt;
                         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                                         &amp;lt;sequence&amp;gt;
                         *                                                           &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                         *                                                             &amp;lt;complexType&amp;gt;
                         *                                                               &amp;lt;complexContent&amp;gt;
                         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                                                   &amp;lt;sequence&amp;gt;
                         *                                                                     &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                         *                                                                       &amp;lt;complexType&amp;gt;
                         *                                                                         &amp;lt;complexContent&amp;gt;
                         *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                                                             &amp;lt;sequence&amp;gt;
                         *                                                                               &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                         *                                                                               &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                         *                                                                             &amp;lt;/sequence&amp;gt;
                         *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *                                                                             &amp;lt;attribute name="OccupancyRate"&amp;gt;
                         *                                                                               &amp;lt;simpleType&amp;gt;
                         *                                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                                                                                   &amp;lt;enumeration value="Flat"/&amp;gt;
                         *                                                                                   &amp;lt;enumeration value="Single"/&amp;gt;
                         *                                                                                   &amp;lt;enumeration value="Double"/&amp;gt;
                         *                                                                                   &amp;lt;enumeration value="Triple"/&amp;gt;
                         *                                                                                   &amp;lt;enumeration value="Quad"/&amp;gt;
                         *                                                                                 &amp;lt;/restriction&amp;gt;
                         *                                                                               &amp;lt;/simpleType&amp;gt;
                         *                                                                             &amp;lt;/attribute&amp;gt;
                         *                                                                           &amp;lt;/restriction&amp;gt;
                         *                                                                         &amp;lt;/complexContent&amp;gt;
                         *                                                                       &amp;lt;/complexType&amp;gt;
                         *                                                                     &amp;lt;/element&amp;gt;
                         *                                                                   &amp;lt;/sequence&amp;gt;
                         *                                                                 &amp;lt;/restriction&amp;gt;
                         *                                                               &amp;lt;/complexContent&amp;gt;
                         *                                                             &amp;lt;/complexType&amp;gt;
                         *                                                           &amp;lt;/element&amp;gt;
                         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                         *                                                         &amp;lt;/sequence&amp;gt;
                         *                                                         &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *                                                         &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *                                                       &amp;lt;/restriction&amp;gt;
                         *                                                     &amp;lt;/complexContent&amp;gt;
                         *                                                   &amp;lt;/complexType&amp;gt;
                         *                                                 &amp;lt;/element&amp;gt;
                         *                                               &amp;lt;/sequence&amp;gt;
                         *                                             &amp;lt;/restriction&amp;gt;
                         *                                           &amp;lt;/complexContent&amp;gt;
                         *                                         &amp;lt;/complexType&amp;gt;
                         *                                       &amp;lt;/element&amp;gt;
                         *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                         *                                     &amp;lt;/sequence&amp;gt;
                         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                         *                                     &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                         *                                       &amp;lt;simpleType&amp;gt;
                         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                                           &amp;lt;enumeration value="First"/&amp;gt;
                         *                                           &amp;lt;enumeration value="Second"/&amp;gt;
                         *                                         &amp;lt;/restriction&amp;gt;
                         *                                       &amp;lt;/simpleType&amp;gt;
                         *                                     &amp;lt;/attribute&amp;gt;
                         *                                   &amp;lt;/restriction&amp;gt;
                         *                                 &amp;lt;/complexContent&amp;gt;
                         *                               &amp;lt;/complexType&amp;gt;
                         *                             &amp;lt;/element&amp;gt;
                         *                           &amp;lt;/sequence&amp;gt;
                         *                         &amp;lt;/restriction&amp;gt;
                         *                       &amp;lt;/complexContent&amp;gt;
                         *                     &amp;lt;/complexType&amp;gt;
                         *                   &amp;lt;/element&amp;gt;
                         *                   &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
                         *                     &amp;lt;complexType&amp;gt;
                         *                       &amp;lt;complexContent&amp;gt;
                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                           &amp;lt;sequence&amp;gt;
                         *                             &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
                         *                               &amp;lt;complexType&amp;gt;
                         *                                 &amp;lt;complexContent&amp;gt;
                         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                     &amp;lt;sequence&amp;gt;
                         *                                       &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
                         *                                         &amp;lt;complexType&amp;gt;
                         *                                           &amp;lt;complexContent&amp;gt;
                         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                               &amp;lt;sequence&amp;gt;
                         *                                                 &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                         *                                                   &amp;lt;complexType&amp;gt;
                         *                                                     &amp;lt;complexContent&amp;gt;
                         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                                         &amp;lt;sequence minOccurs="0"&amp;gt;
                         *                                                           &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                         *                                                         &amp;lt;/sequence&amp;gt;
                         *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                         *                                                         &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *                                                         &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                                                         &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *                                                         &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *                                                         &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *                                                         &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                                                         &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *                                                         &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *                                                         &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *                                                         &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                         *                                                           &amp;lt;simpleType&amp;gt;
                         *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                                                               &amp;lt;enumeration value="First"/&amp;gt;
                         *                                                               &amp;lt;enumeration value="Second"/&amp;gt;
                         *                                                             &amp;lt;/restriction&amp;gt;
                         *                                                           &amp;lt;/simpleType&amp;gt;
                         *                                                         &amp;lt;/attribute&amp;gt;
                         *                                                       &amp;lt;/restriction&amp;gt;
                         *                                                     &amp;lt;/complexContent&amp;gt;
                         *                                                   &amp;lt;/complexType&amp;gt;
                         *                                                 &amp;lt;/element&amp;gt;
                         *                                               &amp;lt;/sequence&amp;gt;
                         *                                               &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                                             &amp;lt;/restriction&amp;gt;
                         *                                           &amp;lt;/complexContent&amp;gt;
                         *                                         &amp;lt;/complexType&amp;gt;
                         *                                       &amp;lt;/element&amp;gt;
                         *                                       &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                         *                                     &amp;lt;/sequence&amp;gt;
                         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                         *                                   &amp;lt;/restriction&amp;gt;
                         *                                 &amp;lt;/complexContent&amp;gt;
                         *                               &amp;lt;/complexType&amp;gt;
                         *                             &amp;lt;/element&amp;gt;
                         *                           &amp;lt;/sequence&amp;gt;
                         *                         &amp;lt;/restriction&amp;gt;
                         *                       &amp;lt;/complexContent&amp;gt;
                         *                     &amp;lt;/complexType&amp;gt;
                         *                   &amp;lt;/element&amp;gt;
                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *                 &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                         *       &amp;lt;attribute name="ProposedDateIndicator"&amp;gt;
                         *         &amp;lt;simpleType&amp;gt;
                         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *             &amp;lt;enumeration value="Primary"/&amp;gt;
                         *             &amp;lt;enumeration value="Alternate"/&amp;gt;
                         *             &amp;lt;enumeration value="Other"/&amp;gt;
                         *           &amp;lt;/restriction&amp;gt;
                         *         &amp;lt;/simpleType&amp;gt;
                         *       &amp;lt;/attribute&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "roomBlock",
                            "eventBlock"
                        })
                        public static class Date {

                            @XmlElement(name = "RoomBlock")
                            protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock roomBlock;
                            @XmlElement(name = "EventBlock")
                            protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock eventBlock;
                            @XmlAttribute(name = "ProposedDateIndicator")
                            protected String proposedDateIndicator;
                            @XmlAttribute(name = "Start")
                            protected String start;
                            @XmlAttribute(name = "Duration")
                            protected String duration;
                            @XmlAttribute(name = "End")
                            protected String end;

                            /**
                             * Obtiene el valor de la propiedad roomBlock.
                             * 
                             * @return
                             *     possible object is
                             *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock }
                             *     
                             */
                            public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock getRoomBlock() {
                                return roomBlock;
                            }

                            /**
                             * Define el valor de la propiedad roomBlock.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock }
                             *     
                             */
                            public void setRoomBlock(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock value) {
                                this.roomBlock = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad eventBlock.
                             * 
                             * @return
                             *     possible object is
                             *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock }
                             *     
                             */
                            public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock getEventBlock() {
                                return eventBlock;
                            }

                            /**
                             * Define el valor de la propiedad eventBlock.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock }
                             *     
                             */
                            public void setEventBlock(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock value) {
                                this.eventBlock = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad proposedDateIndicator.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getProposedDateIndicator() {
                                return proposedDateIndicator;
                            }

                            /**
                             * Define el valor de la propiedad proposedDateIndicator.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setProposedDateIndicator(String value) {
                                this.proposedDateIndicator = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad start.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getStart() {
                                return start;
                            }

                            /**
                             * Define el valor de la propiedad start.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setStart(String value) {
                                this.start = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad duration.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDuration() {
                                return duration;
                            }

                            /**
                             * Define el valor de la propiedad duration.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDuration(String value) {
                                this.duration = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad end.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getEnd() {
                                return end;
                            }

                            /**
                             * Define el valor de la propiedad end.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setEnd(String value) {
                                this.end = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="EventDates" minOccurs="0"&amp;gt;
                             *           &amp;lt;complexType&amp;gt;
                             *             &amp;lt;complexContent&amp;gt;
                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                 &amp;lt;sequence&amp;gt;
                             *                   &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
                             *                     &amp;lt;complexType&amp;gt;
                             *                       &amp;lt;complexContent&amp;gt;
                             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                           &amp;lt;sequence&amp;gt;
                             *                             &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
                             *                               &amp;lt;complexType&amp;gt;
                             *                                 &amp;lt;complexContent&amp;gt;
                             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                                     &amp;lt;sequence&amp;gt;
                             *                                       &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                             *                                         &amp;lt;complexType&amp;gt;
                             *                                           &amp;lt;complexContent&amp;gt;
                             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                                               &amp;lt;sequence minOccurs="0"&amp;gt;
                             *                                                 &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                             *                                               &amp;lt;/sequence&amp;gt;
                             *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                             *                                               &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                             *                                               &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                                               &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                             *                                               &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                             *                                               &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                             *                                               &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                                               &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                             *                                               &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                             *                                               &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                             *                                               &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                             *                                                 &amp;lt;simpleType&amp;gt;
                             *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *                                                     &amp;lt;enumeration value="First"/&amp;gt;
                             *                                                     &amp;lt;enumeration value="Second"/&amp;gt;
                             *                                                   &amp;lt;/restriction&amp;gt;
                             *                                                 &amp;lt;/simpleType&amp;gt;
                             *                                               &amp;lt;/attribute&amp;gt;
                             *                                             &amp;lt;/restriction&amp;gt;
                             *                                           &amp;lt;/complexContent&amp;gt;
                             *                                         &amp;lt;/complexType&amp;gt;
                             *                                       &amp;lt;/element&amp;gt;
                             *                                     &amp;lt;/sequence&amp;gt;
                             *                                     &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                                   &amp;lt;/restriction&amp;gt;
                             *                                 &amp;lt;/complexContent&amp;gt;
                             *                               &amp;lt;/complexType&amp;gt;
                             *                             &amp;lt;/element&amp;gt;
                             *                             &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                             *                           &amp;lt;/sequence&amp;gt;
                             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                             *                         &amp;lt;/restriction&amp;gt;
                             *                       &amp;lt;/complexContent&amp;gt;
                             *                     &amp;lt;/complexType&amp;gt;
                             *                   &amp;lt;/element&amp;gt;
                             *                 &amp;lt;/sequence&amp;gt;
                             *               &amp;lt;/restriction&amp;gt;
                             *             &amp;lt;/complexContent&amp;gt;
                             *           &amp;lt;/complexType&amp;gt;
                             *         &amp;lt;/element&amp;gt;
                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *       &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "eventDates",
                                "comment"
                            })
                            public static class EventBlock {

                                @XmlElement(name = "EventDates")
                                protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates eventDates;
                                @XmlElement(name = "Comment")
                                protected List<ParagraphType> comment;
                                @XmlAttribute(name = "EventName")
                                protected String eventName;

                                /**
                                 * Obtiene el valor de la propiedad eventDates.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates }
                                 *     
                                 */
                                public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates getEventDates() {
                                    return eventDates;
                                }

                                /**
                                 * Define el valor de la propiedad eventDates.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates }
                                 *     
                                 */
                                public void setEventDates(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates value) {
                                    this.eventDates = value;
                                }

                                /**
                                 * Gets the value of the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getComment().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link ParagraphType }
                                 * 
                                 * 
                                 */
                                public List<ParagraphType> getComment() {
                                    if (comment == null) {
                                        comment = new ArrayList<ParagraphType>();
                                    }
                                    return this.comment;
                                }

                                /**
                                 * Obtiene el valor de la propiedad eventName.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getEventName() {
                                    return eventName;
                                }

                                /**
                                 * Define el valor de la propiedad eventName.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setEventName(String value) {
                                    this.eventName = value;
                                }


                                /**
                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                 * 
                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                 * 
                                 * &lt;pre&gt;
                                 * &amp;lt;complexType&amp;gt;
                                 *   &amp;lt;complexContent&amp;gt;
                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *       &amp;lt;sequence&amp;gt;
                                 *         &amp;lt;element name="EventDate" maxOccurs="unbounded"&amp;gt;
                                 *           &amp;lt;complexType&amp;gt;
                                 *             &amp;lt;complexContent&amp;gt;
                                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                 &amp;lt;sequence&amp;gt;
                                 *                   &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
                                 *                     &amp;lt;complexType&amp;gt;
                                 *                       &amp;lt;complexContent&amp;gt;
                                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                           &amp;lt;sequence&amp;gt;
                                 *                             &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                                 *                               &amp;lt;complexType&amp;gt;
                                 *                                 &amp;lt;complexContent&amp;gt;
                                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                                     &amp;lt;sequence minOccurs="0"&amp;gt;
                                 *                                       &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                 *                                     &amp;lt;/sequence&amp;gt;
                                 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                                 *                                     &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                 *                                     &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *                                     &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                 *                                     &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                 *                                     &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                 *                                     &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *                                     &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                                 *                                     &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                 *                                     &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                 *                                     &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                                 *                                       &amp;lt;simpleType&amp;gt;
                                 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                 *                                           &amp;lt;enumeration value="First"/&amp;gt;
                                 *                                           &amp;lt;enumeration value="Second"/&amp;gt;
                                 *                                         &amp;lt;/restriction&amp;gt;
                                 *                                       &amp;lt;/simpleType&amp;gt;
                                 *                                     &amp;lt;/attribute&amp;gt;
                                 *                                   &amp;lt;/restriction&amp;gt;
                                 *                                 &amp;lt;/complexContent&amp;gt;
                                 *                               &amp;lt;/complexType&amp;gt;
                                 *                             &amp;lt;/element&amp;gt;
                                 *                           &amp;lt;/sequence&amp;gt;
                                 *                           &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *                         &amp;lt;/restriction&amp;gt;
                                 *                       &amp;lt;/complexContent&amp;gt;
                                 *                     &amp;lt;/complexType&amp;gt;
                                 *                   &amp;lt;/element&amp;gt;
                                 *                   &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                                 *                 &amp;lt;/sequence&amp;gt;
                                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                                 *               &amp;lt;/restriction&amp;gt;
                                 *             &amp;lt;/complexContent&amp;gt;
                                 *           &amp;lt;/complexType&amp;gt;
                                 *         &amp;lt;/element&amp;gt;
                                 *       &amp;lt;/sequence&amp;gt;
                                 *     &amp;lt;/restriction&amp;gt;
                                 *   &amp;lt;/complexContent&amp;gt;
                                 * &amp;lt;/complexType&amp;gt;
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "eventDate"
                                })
                                public static class EventDates {

                                    @XmlElement(name = "EventDate", required = true)
                                    protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate> eventDate;

                                    /**
                                     * Gets the value of the eventDate property.
                                     * 
                                     * &lt;p&gt;
                                     * This accessor method returns a reference to the live list,
                                     * not a snapshot. Therefore any modification you make to the
                                     * returned list will be present inside the JAXB object.
                                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventDate property.
                                     * 
                                     * &lt;p&gt;
                                     * For example, to add a new item, do as follows:
                                     * &lt;pre&gt;
                                     *    getEventDate().add(newItem);
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     * &lt;p&gt;
                                     * Objects of the following type(s) are allowed in the list
                                     * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate }
                                     * 
                                     * 
                                     */
                                    public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate> getEventDate() {
                                        if (eventDate == null) {
                                            eventDate = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate>();
                                        }
                                        return this.eventDate;
                                    }


                                    /**
                                     * &lt;p&gt;Clase Java para anonymous complex type.
                                     * 
                                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                     * 
                                     * &lt;pre&gt;
                                     * &amp;lt;complexType&amp;gt;
                                     *   &amp;lt;complexContent&amp;gt;
                                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *       &amp;lt;sequence&amp;gt;
                                     *         &amp;lt;element name="EventDateFunctions" minOccurs="0"&amp;gt;
                                     *           &amp;lt;complexType&amp;gt;
                                     *             &amp;lt;complexContent&amp;gt;
                                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *                 &amp;lt;sequence&amp;gt;
                                     *                   &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                                     *                     &amp;lt;complexType&amp;gt;
                                     *                       &amp;lt;complexContent&amp;gt;
                                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *                           &amp;lt;sequence minOccurs="0"&amp;gt;
                                     *                             &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                     *                           &amp;lt;/sequence&amp;gt;
                                     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                                     *                           &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                     *                           &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                     *                           &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                     *                           &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                     *                           &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                     *                           &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                     *                           &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                                     *                           &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                     *                           &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                     *                           &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                                     *                             &amp;lt;simpleType&amp;gt;
                                     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                     *                                 &amp;lt;enumeration value="First"/&amp;gt;
                                     *                                 &amp;lt;enumeration value="Second"/&amp;gt;
                                     *                               &amp;lt;/restriction&amp;gt;
                                     *                             &amp;lt;/simpleType&amp;gt;
                                     *                           &amp;lt;/attribute&amp;gt;
                                     *                         &amp;lt;/restriction&amp;gt;
                                     *                       &amp;lt;/complexContent&amp;gt;
                                     *                     &amp;lt;/complexType&amp;gt;
                                     *                   &amp;lt;/element&amp;gt;
                                     *                 &amp;lt;/sequence&amp;gt;
                                     *                 &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                     *               &amp;lt;/restriction&amp;gt;
                                     *             &amp;lt;/complexContent&amp;gt;
                                     *           &amp;lt;/complexType&amp;gt;
                                     *         &amp;lt;/element&amp;gt;
                                     *         &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                                     *       &amp;lt;/sequence&amp;gt;
                                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                                     *     &amp;lt;/restriction&amp;gt;
                                     *   &amp;lt;/complexContent&amp;gt;
                                     * &amp;lt;/complexType&amp;gt;
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     */
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @XmlType(name = "", propOrder = {
                                        "eventDateFunctions",
                                        "eventCharges"
                                    })
                                    public static class EventDate {

                                        @XmlElement(name = "EventDateFunctions")
                                        protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions eventDateFunctions;
                                        @XmlElement(name = "EventCharges")
                                        protected EventChargeType eventCharges;
                                        @XmlAttribute(name = "Start")
                                        protected String start;
                                        @XmlAttribute(name = "Duration")
                                        protected String duration;
                                        @XmlAttribute(name = "End")
                                        protected String end;

                                        /**
                                         * Obtiene el valor de la propiedad eventDateFunctions.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions }
                                         *     
                                         */
                                        public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions getEventDateFunctions() {
                                            return eventDateFunctions;
                                        }

                                        /**
                                         * Define el valor de la propiedad eventDateFunctions.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions }
                                         *     
                                         */
                                        public void setEventDateFunctions(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions value) {
                                            this.eventDateFunctions = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad eventCharges.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link EventChargeType }
                                         *     
                                         */
                                        public EventChargeType getEventCharges() {
                                            return eventCharges;
                                        }

                                        /**
                                         * Define el valor de la propiedad eventCharges.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link EventChargeType }
                                         *     
                                         */
                                        public void setEventCharges(EventChargeType value) {
                                            this.eventCharges = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad start.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getStart() {
                                            return start;
                                        }

                                        /**
                                         * Define el valor de la propiedad start.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setStart(String value) {
                                            this.start = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad duration.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getDuration() {
                                            return duration;
                                        }

                                        /**
                                         * Define el valor de la propiedad duration.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setDuration(String value) {
                                            this.duration = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad end.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getEnd() {
                                            return end;
                                        }

                                        /**
                                         * Define el valor de la propiedad end.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setEnd(String value) {
                                            this.end = value;
                                        }


                                        /**
                                         * &lt;p&gt;Clase Java para anonymous complex type.
                                         * 
                                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                         * 
                                         * &lt;pre&gt;
                                         * &amp;lt;complexType&amp;gt;
                                         *   &amp;lt;complexContent&amp;gt;
                                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                         *       &amp;lt;sequence&amp;gt;
                                         *         &amp;lt;element name="EventDateFunction" maxOccurs="unbounded"&amp;gt;
                                         *           &amp;lt;complexType&amp;gt;
                                         *             &amp;lt;complexContent&amp;gt;
                                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                         *                 &amp;lt;sequence minOccurs="0"&amp;gt;
                                         *                   &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                         *                 &amp;lt;/sequence&amp;gt;
                                         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                                         *                 &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                         *                 &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                         *                 &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                         *                 &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                         *                 &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                         *                 &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                         *                 &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                                         *                 &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                         *                 &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                         *                 &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                                         *                   &amp;lt;simpleType&amp;gt;
                                         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                         *                       &amp;lt;enumeration value="First"/&amp;gt;
                                         *                       &amp;lt;enumeration value="Second"/&amp;gt;
                                         *                     &amp;lt;/restriction&amp;gt;
                                         *                   &amp;lt;/simpleType&amp;gt;
                                         *                 &amp;lt;/attribute&amp;gt;
                                         *               &amp;lt;/restriction&amp;gt;
                                         *             &amp;lt;/complexContent&amp;gt;
                                         *           &amp;lt;/complexType&amp;gt;
                                         *         &amp;lt;/element&amp;gt;
                                         *       &amp;lt;/sequence&amp;gt;
                                         *       &amp;lt;attribute name="ChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                         *     &amp;lt;/restriction&amp;gt;
                                         *   &amp;lt;/complexContent&amp;gt;
                                         * &amp;lt;/complexType&amp;gt;
                                         * &lt;/pre&gt;
                                         * 
                                         * 
                                         */
                                        @XmlAccessorType(XmlAccessType.FIELD)
                                        @XmlType(name = "", propOrder = {
                                            "eventDateFunction"
                                        })
                                        public static class EventDateFunctions {

                                            @XmlElement(name = "EventDateFunction", required = true)
                                            protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions.EventDateFunction> eventDateFunction;
                                            @XmlAttribute(name = "ChargeType")
                                            protected String chargeType;

                                            /**
                                             * Gets the value of the eventDateFunction property.
                                             * 
                                             * &lt;p&gt;
                                             * This accessor method returns a reference to the live list,
                                             * not a snapshot. Therefore any modification you make to the
                                             * returned list will be present inside the JAXB object.
                                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventDateFunction property.
                                             * 
                                             * &lt;p&gt;
                                             * For example, to add a new item, do as follows:
                                             * &lt;pre&gt;
                                             *    getEventDateFunction().add(newItem);
                                             * &lt;/pre&gt;
                                             * 
                                             * 
                                             * &lt;p&gt;
                                             * Objects of the following type(s) are allowed in the list
                                             * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions.EventDateFunction }
                                             * 
                                             * 
                                             */
                                            public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions.EventDateFunction> getEventDateFunction() {
                                                if (eventDateFunction == null) {
                                                    eventDateFunction = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.EventBlock.EventDates.EventDate.EventDateFunctions.EventDateFunction>();
                                                }
                                                return this.eventDateFunction;
                                            }

                                            /**
                                             * Obtiene el valor de la propiedad chargeType.
                                             * 
                                             * @return
                                             *     possible object is
                                             *     {@link String }
                                             *     
                                             */
                                            public String getChargeType() {
                                                return chargeType;
                                            }

                                            /**
                                             * Define el valor de la propiedad chargeType.
                                             * 
                                             * @param value
                                             *     allowed object is
                                             *     {@link String }
                                             *     
                                             */
                                            public void setChargeType(String value) {
                                                this.chargeType = value;
                                            }


                                            /**
                                             * &lt;p&gt;Clase Java para anonymous complex type.
                                             * 
                                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                             * 
                                             * &lt;pre&gt;
                                             * &amp;lt;complexType&amp;gt;
                                             *   &amp;lt;complexContent&amp;gt;
                                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                             *       &amp;lt;sequence minOccurs="0"&amp;gt;
                                             *         &amp;lt;element name="EventCharges" type="{http://www.opentravel.org/OTA/2003/05}EventChargeType" minOccurs="0"/&amp;gt;
                                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                             *       &amp;lt;/sequence&amp;gt;
                                             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                                             *       &amp;lt;attribute name="EventName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                             *       &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                             *       &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                             *       &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                                             *       &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                             *       &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                             *       &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                                             *       &amp;lt;attribute name="MeetingRoomName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                             *       &amp;lt;attribute name="MeetingRoomCapacity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                             *       &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                                             *         &amp;lt;simpleType&amp;gt;
                                             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                             *             &amp;lt;enumeration value="First"/&amp;gt;
                                             *             &amp;lt;enumeration value="Second"/&amp;gt;
                                             *           &amp;lt;/restriction&amp;gt;
                                             *         &amp;lt;/simpleType&amp;gt;
                                             *       &amp;lt;/attribute&amp;gt;
                                             *     &amp;lt;/restriction&amp;gt;
                                             *   &amp;lt;/complexContent&amp;gt;
                                             * &amp;lt;/complexType&amp;gt;
                                             * &lt;/pre&gt;
                                             * 
                                             * 
                                             */
                                            @XmlAccessorType(XmlAccessType.FIELD)
                                            @XmlType(name = "", propOrder = {
                                                "eventCharges",
                                                "comment"
                                            })
                                            public static class EventDateFunction {

                                                @XmlElement(name = "EventCharges")
                                                protected EventChargeType eventCharges;
                                                @XmlElement(name = "Comment")
                                                protected List<ParagraphType> comment;
                                                @XmlAttribute(name = "EventName")
                                                protected String eventName;
                                                @XmlAttribute(name = "EventType")
                                                protected String eventType;
                                                @XmlAttribute(name = "StartTime")
                                                @XmlSchemaType(name = "time")
                                                protected XMLGregorianCalendar startTime;
                                                @XmlAttribute(name = "EndTime")
                                                @XmlSchemaType(name = "time")
                                                protected XMLGregorianCalendar endTime;
                                                @XmlAttribute(name = "AttendeeQuantity")
                                                @XmlSchemaType(name = "nonNegativeInteger")
                                                protected BigInteger attendeeQuantity;
                                                @XmlAttribute(name = "RoomSetup")
                                                protected String roomSetup;
                                                @XmlAttribute(name = "TwentyFourHourHold")
                                                protected Boolean twentyFourHourHold;
                                                @XmlAttribute(name = "MeetingRoomName")
                                                protected String meetingRoomName;
                                                @XmlAttribute(name = "MeetingRoomCapacity")
                                                @XmlSchemaType(name = "nonNegativeInteger")
                                                protected BigInteger meetingRoomCapacity;
                                                @XmlAttribute(name = "AvailabilityOptionType")
                                                protected String availabilityOptionType;
                                                @XmlAttribute(name = "UnitOfMeasureQuantity")
                                                protected BigDecimal unitOfMeasureQuantity;
                                                @XmlAttribute(name = "UnitOfMeasure")
                                                protected String unitOfMeasure;
                                                @XmlAttribute(name = "UnitOfMeasureCode")
                                                protected String unitOfMeasureCode;

                                                /**
                                                 * Obtiene el valor de la propiedad eventCharges.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link EventChargeType }
                                                 *     
                                                 */
                                                public EventChargeType getEventCharges() {
                                                    return eventCharges;
                                                }

                                                /**
                                                 * Define el valor de la propiedad eventCharges.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link EventChargeType }
                                                 *     
                                                 */
                                                public void setEventCharges(EventChargeType value) {
                                                    this.eventCharges = value;
                                                }

                                                /**
                                                 * Gets the value of the comment property.
                                                 * 
                                                 * &lt;p&gt;
                                                 * This accessor method returns a reference to the live list,
                                                 * not a snapshot. Therefore any modification you make to the
                                                 * returned list will be present inside the JAXB object.
                                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                                 * 
                                                 * &lt;p&gt;
                                                 * For example, to add a new item, do as follows:
                                                 * &lt;pre&gt;
                                                 *    getComment().add(newItem);
                                                 * &lt;/pre&gt;
                                                 * 
                                                 * 
                                                 * &lt;p&gt;
                                                 * Objects of the following type(s) are allowed in the list
                                                 * {@link ParagraphType }
                                                 * 
                                                 * 
                                                 */
                                                public List<ParagraphType> getComment() {
                                                    if (comment == null) {
                                                        comment = new ArrayList<ParagraphType>();
                                                    }
                                                    return this.comment;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad eventName.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getEventName() {
                                                    return eventName;
                                                }

                                                /**
                                                 * Define el valor de la propiedad eventName.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setEventName(String value) {
                                                    this.eventName = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad eventType.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getEventType() {
                                                    return eventType;
                                                }

                                                /**
                                                 * Define el valor de la propiedad eventType.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setEventType(String value) {
                                                    this.eventType = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad startTime.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link XMLGregorianCalendar }
                                                 *     
                                                 */
                                                public XMLGregorianCalendar getStartTime() {
                                                    return startTime;
                                                }

                                                /**
                                                 * Define el valor de la propiedad startTime.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link XMLGregorianCalendar }
                                                 *     
                                                 */
                                                public void setStartTime(XMLGregorianCalendar value) {
                                                    this.startTime = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad endTime.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link XMLGregorianCalendar }
                                                 *     
                                                 */
                                                public XMLGregorianCalendar getEndTime() {
                                                    return endTime;
                                                }

                                                /**
                                                 * Define el valor de la propiedad endTime.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link XMLGregorianCalendar }
                                                 *     
                                                 */
                                                public void setEndTime(XMLGregorianCalendar value) {
                                                    this.endTime = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad attendeeQuantity.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link BigInteger }
                                                 *     
                                                 */
                                                public BigInteger getAttendeeQuantity() {
                                                    return attendeeQuantity;
                                                }

                                                /**
                                                 * Define el valor de la propiedad attendeeQuantity.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link BigInteger }
                                                 *     
                                                 */
                                                public void setAttendeeQuantity(BigInteger value) {
                                                    this.attendeeQuantity = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad roomSetup.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getRoomSetup() {
                                                    return roomSetup;
                                                }

                                                /**
                                                 * Define el valor de la propiedad roomSetup.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setRoomSetup(String value) {
                                                    this.roomSetup = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad twentyFourHourHold.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link Boolean }
                                                 *     
                                                 */
                                                public Boolean isTwentyFourHourHold() {
                                                    return twentyFourHourHold;
                                                }

                                                /**
                                                 * Define el valor de la propiedad twentyFourHourHold.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link Boolean }
                                                 *     
                                                 */
                                                public void setTwentyFourHourHold(Boolean value) {
                                                    this.twentyFourHourHold = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad meetingRoomName.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getMeetingRoomName() {
                                                    return meetingRoomName;
                                                }

                                                /**
                                                 * Define el valor de la propiedad meetingRoomName.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setMeetingRoomName(String value) {
                                                    this.meetingRoomName = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad meetingRoomCapacity.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link BigInteger }
                                                 *     
                                                 */
                                                public BigInteger getMeetingRoomCapacity() {
                                                    return meetingRoomCapacity;
                                                }

                                                /**
                                                 * Define el valor de la propiedad meetingRoomCapacity.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link BigInteger }
                                                 *     
                                                 */
                                                public void setMeetingRoomCapacity(BigInteger value) {
                                                    this.meetingRoomCapacity = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad availabilityOptionType.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getAvailabilityOptionType() {
                                                    return availabilityOptionType;
                                                }

                                                /**
                                                 * Define el valor de la propiedad availabilityOptionType.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setAvailabilityOptionType(String value) {
                                                    this.availabilityOptionType = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad unitOfMeasureQuantity.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link BigDecimal }
                                                 *     
                                                 */
                                                public BigDecimal getUnitOfMeasureQuantity() {
                                                    return unitOfMeasureQuantity;
                                                }

                                                /**
                                                 * Define el valor de la propiedad unitOfMeasureQuantity.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link BigDecimal }
                                                 *     
                                                 */
                                                public void setUnitOfMeasureQuantity(BigDecimal value) {
                                                    this.unitOfMeasureQuantity = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad unitOfMeasure.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getUnitOfMeasure() {
                                                    return unitOfMeasure;
                                                }

                                                /**
                                                 * Define el valor de la propiedad unitOfMeasure.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setUnitOfMeasure(String value) {
                                                    this.unitOfMeasure = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad unitOfMeasureCode.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getUnitOfMeasureCode() {
                                                    return unitOfMeasureCode;
                                                }

                                                /**
                                                 * Define el valor de la propiedad unitOfMeasureCode.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setUnitOfMeasureCode(String value) {
                                                    this.unitOfMeasureCode = value;
                                                }

                                            }

                                        }

                                    }

                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="StayDates" minOccurs="0"&amp;gt;
                             *           &amp;lt;complexType&amp;gt;
                             *             &amp;lt;complexContent&amp;gt;
                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                 &amp;lt;sequence&amp;gt;
                             *                   &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
                             *                     &amp;lt;complexType&amp;gt;
                             *                       &amp;lt;complexContent&amp;gt;
                             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                           &amp;lt;sequence&amp;gt;
                             *                             &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
                             *                               &amp;lt;complexType&amp;gt;
                             *                                 &amp;lt;complexContent&amp;gt;
                             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                                     &amp;lt;sequence&amp;gt;
                             *                                       &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                             *                                         &amp;lt;complexType&amp;gt;
                             *                                           &amp;lt;complexContent&amp;gt;
                             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                                               &amp;lt;sequence&amp;gt;
                             *                                                 &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                             *                                                   &amp;lt;complexType&amp;gt;
                             *                                                     &amp;lt;complexContent&amp;gt;
                             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                                                         &amp;lt;sequence&amp;gt;
                             *                                                           &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                             *                                                             &amp;lt;complexType&amp;gt;
                             *                                                               &amp;lt;complexContent&amp;gt;
                             *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                                                                   &amp;lt;sequence&amp;gt;
                             *                                                                     &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                             *                                                                     &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                             *                                                                   &amp;lt;/sequence&amp;gt;
                             *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                             *                                                                   &amp;lt;attribute name="OccupancyRate"&amp;gt;
                             *                                                                     &amp;lt;simpleType&amp;gt;
                             *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *                                                                         &amp;lt;enumeration value="Flat"/&amp;gt;
                             *                                                                         &amp;lt;enumeration value="Single"/&amp;gt;
                             *                                                                         &amp;lt;enumeration value="Double"/&amp;gt;
                             *                                                                         &amp;lt;enumeration value="Triple"/&amp;gt;
                             *                                                                         &amp;lt;enumeration value="Quad"/&amp;gt;
                             *                                                                       &amp;lt;/restriction&amp;gt;
                             *                                                                     &amp;lt;/simpleType&amp;gt;
                             *                                                                   &amp;lt;/attribute&amp;gt;
                             *                                                                 &amp;lt;/restriction&amp;gt;
                             *                                                               &amp;lt;/complexContent&amp;gt;
                             *                                                             &amp;lt;/complexType&amp;gt;
                             *                                                           &amp;lt;/element&amp;gt;
                             *                                                         &amp;lt;/sequence&amp;gt;
                             *                                                       &amp;lt;/restriction&amp;gt;
                             *                                                     &amp;lt;/complexContent&amp;gt;
                             *                                                   &amp;lt;/complexType&amp;gt;
                             *                                                 &amp;lt;/element&amp;gt;
                             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                             *                                               &amp;lt;/sequence&amp;gt;
                             *                                               &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                             *                                               &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                             *                                             &amp;lt;/restriction&amp;gt;
                             *                                           &amp;lt;/complexContent&amp;gt;
                             *                                         &amp;lt;/complexType&amp;gt;
                             *                                       &amp;lt;/element&amp;gt;
                             *                                     &amp;lt;/sequence&amp;gt;
                             *                                   &amp;lt;/restriction&amp;gt;
                             *                                 &amp;lt;/complexContent&amp;gt;
                             *                               &amp;lt;/complexType&amp;gt;
                             *                             &amp;lt;/element&amp;gt;
                             *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                             *                           &amp;lt;/sequence&amp;gt;
                             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                             *                           &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                             *                             &amp;lt;simpleType&amp;gt;
                             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *                                 &amp;lt;enumeration value="First"/&amp;gt;
                             *                                 &amp;lt;enumeration value="Second"/&amp;gt;
                             *                               &amp;lt;/restriction&amp;gt;
                             *                             &amp;lt;/simpleType&amp;gt;
                             *                           &amp;lt;/attribute&amp;gt;
                             *                         &amp;lt;/restriction&amp;gt;
                             *                       &amp;lt;/complexContent&amp;gt;
                             *                     &amp;lt;/complexType&amp;gt;
                             *                   &amp;lt;/element&amp;gt;
                             *                 &amp;lt;/sequence&amp;gt;
                             *               &amp;lt;/restriction&amp;gt;
                             *             &amp;lt;/complexContent&amp;gt;
                             *           &amp;lt;/complexType&amp;gt;
                             *         &amp;lt;/element&amp;gt;
                             *         &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "stayDates",
                                "fees",
                                "comments"
                            })
                            public static class RoomBlock {

                                @XmlElement(name = "StayDates")
                                protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates stayDates;
                                @XmlElement(name = "Fees")
                                protected FeesType fees;
                                @XmlElement(name = "Comments")
                                protected List<ParagraphType> comments;

                                /**
                                 * Obtiene el valor de la propiedad stayDates.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates }
                                 *     
                                 */
                                public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates getStayDates() {
                                    return stayDates;
                                }

                                /**
                                 * Define el valor de la propiedad stayDates.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates }
                                 *     
                                 */
                                public void setStayDates(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates value) {
                                    this.stayDates = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad fees.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link FeesType }
                                 *     
                                 */
                                public FeesType getFees() {
                                    return fees;
                                }

                                /**
                                 * Define el valor de la propiedad fees.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link FeesType }
                                 *     
                                 */
                                public void setFees(FeesType value) {
                                    this.fees = value;
                                }

                                /**
                                 * Gets the value of the comments property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comments property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getComments().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link ParagraphType }
                                 * 
                                 * 
                                 */
                                public List<ParagraphType> getComments() {
                                    if (comments == null) {
                                        comments = new ArrayList<ParagraphType>();
                                    }
                                    return this.comments;
                                }


                                /**
                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                 * 
                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                 * 
                                 * &lt;pre&gt;
                                 * &amp;lt;complexType&amp;gt;
                                 *   &amp;lt;complexContent&amp;gt;
                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *       &amp;lt;sequence&amp;gt;
                                 *         &amp;lt;element name="StayDate" maxOccurs="unbounded"&amp;gt;
                                 *           &amp;lt;complexType&amp;gt;
                                 *             &amp;lt;complexContent&amp;gt;
                                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                 &amp;lt;sequence&amp;gt;
                                 *                   &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
                                 *                     &amp;lt;complexType&amp;gt;
                                 *                       &amp;lt;complexContent&amp;gt;
                                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                           &amp;lt;sequence&amp;gt;
                                 *                             &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                                 *                               &amp;lt;complexType&amp;gt;
                                 *                                 &amp;lt;complexContent&amp;gt;
                                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                                     &amp;lt;sequence&amp;gt;
                                 *                                       &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                                 *                                         &amp;lt;complexType&amp;gt;
                                 *                                           &amp;lt;complexContent&amp;gt;
                                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                                               &amp;lt;sequence&amp;gt;
                                 *                                                 &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                                 *                                                   &amp;lt;complexType&amp;gt;
                                 *                                                     &amp;lt;complexContent&amp;gt;
                                 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                                                         &amp;lt;sequence&amp;gt;
                                 *                                                           &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                                 *                                                           &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                                 *                                                         &amp;lt;/sequence&amp;gt;
                                 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                 *                                                         &amp;lt;attribute name="OccupancyRate"&amp;gt;
                                 *                                                           &amp;lt;simpleType&amp;gt;
                                 *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                 *                                                               &amp;lt;enumeration value="Flat"/&amp;gt;
                                 *                                                               &amp;lt;enumeration value="Single"/&amp;gt;
                                 *                                                               &amp;lt;enumeration value="Double"/&amp;gt;
                                 *                                                               &amp;lt;enumeration value="Triple"/&amp;gt;
                                 *                                                               &amp;lt;enumeration value="Quad"/&amp;gt;
                                 *                                                             &amp;lt;/restriction&amp;gt;
                                 *                                                           &amp;lt;/simpleType&amp;gt;
                                 *                                                         &amp;lt;/attribute&amp;gt;
                                 *                                                       &amp;lt;/restriction&amp;gt;
                                 *                                                     &amp;lt;/complexContent&amp;gt;
                                 *                                                   &amp;lt;/complexType&amp;gt;
                                 *                                                 &amp;lt;/element&amp;gt;
                                 *                                               &amp;lt;/sequence&amp;gt;
                                 *                                             &amp;lt;/restriction&amp;gt;
                                 *                                           &amp;lt;/complexContent&amp;gt;
                                 *                                         &amp;lt;/complexType&amp;gt;
                                 *                                       &amp;lt;/element&amp;gt;
                                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                 *                                     &amp;lt;/sequence&amp;gt;
                                 *                                     &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                 *                                     &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                                 *                                   &amp;lt;/restriction&amp;gt;
                                 *                                 &amp;lt;/complexContent&amp;gt;
                                 *                               &amp;lt;/complexType&amp;gt;
                                 *                             &amp;lt;/element&amp;gt;
                                 *                           &amp;lt;/sequence&amp;gt;
                                 *                         &amp;lt;/restriction&amp;gt;
                                 *                       &amp;lt;/complexContent&amp;gt;
                                 *                     &amp;lt;/complexType&amp;gt;
                                 *                   &amp;lt;/element&amp;gt;
                                 *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                 *                 &amp;lt;/sequence&amp;gt;
                                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                                 *                 &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                                 *                   &amp;lt;simpleType&amp;gt;
                                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                 *                       &amp;lt;enumeration value="First"/&amp;gt;
                                 *                       &amp;lt;enumeration value="Second"/&amp;gt;
                                 *                     &amp;lt;/restriction&amp;gt;
                                 *                   &amp;lt;/simpleType&amp;gt;
                                 *                 &amp;lt;/attribute&amp;gt;
                                 *               &amp;lt;/restriction&amp;gt;
                                 *             &amp;lt;/complexContent&amp;gt;
                                 *           &amp;lt;/complexType&amp;gt;
                                 *         &amp;lt;/element&amp;gt;
                                 *       &amp;lt;/sequence&amp;gt;
                                 *     &amp;lt;/restriction&amp;gt;
                                 *   &amp;lt;/complexContent&amp;gt;
                                 * &amp;lt;/complexType&amp;gt;
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "stayDate"
                                })
                                public static class StayDates {

                                    @XmlElement(name = "StayDate", required = true)
                                    protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate> stayDate;

                                    /**
                                     * Gets the value of the stayDate property.
                                     * 
                                     * &lt;p&gt;
                                     * This accessor method returns a reference to the live list,
                                     * not a snapshot. Therefore any modification you make to the
                                     * returned list will be present inside the JAXB object.
                                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stayDate property.
                                     * 
                                     * &lt;p&gt;
                                     * For example, to add a new item, do as follows:
                                     * &lt;pre&gt;
                                     *    getStayDate().add(newItem);
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     * &lt;p&gt;
                                     * Objects of the following type(s) are allowed in the list
                                     * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate }
                                     * 
                                     * 
                                     */
                                    public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate> getStayDate() {
                                        if (stayDate == null) {
                                            stayDate = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate>();
                                        }
                                        return this.stayDate;
                                    }


                                    /**
                                     * &lt;p&gt;Clase Java para anonymous complex type.
                                     * 
                                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                     * 
                                     * &lt;pre&gt;
                                     * &amp;lt;complexType&amp;gt;
                                     *   &amp;lt;complexContent&amp;gt;
                                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *       &amp;lt;sequence&amp;gt;
                                     *         &amp;lt;element name="StayDateRooms" minOccurs="0"&amp;gt;
                                     *           &amp;lt;complexType&amp;gt;
                                     *             &amp;lt;complexContent&amp;gt;
                                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *                 &amp;lt;sequence&amp;gt;
                                     *                   &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                                     *                     &amp;lt;complexType&amp;gt;
                                     *                       &amp;lt;complexContent&amp;gt;
                                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *                           &amp;lt;sequence&amp;gt;
                                     *                             &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                                     *                               &amp;lt;complexType&amp;gt;
                                     *                                 &amp;lt;complexContent&amp;gt;
                                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *                                     &amp;lt;sequence&amp;gt;
                                     *                                       &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                                     *                                         &amp;lt;complexType&amp;gt;
                                     *                                           &amp;lt;complexContent&amp;gt;
                                     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *                                               &amp;lt;sequence&amp;gt;
                                     *                                                 &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                                     *                                                 &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                                     *                                               &amp;lt;/sequence&amp;gt;
                                     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                     *                                               &amp;lt;attribute name="OccupancyRate"&amp;gt;
                                     *                                                 &amp;lt;simpleType&amp;gt;
                                     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                     *                                                     &amp;lt;enumeration value="Flat"/&amp;gt;
                                     *                                                     &amp;lt;enumeration value="Single"/&amp;gt;
                                     *                                                     &amp;lt;enumeration value="Double"/&amp;gt;
                                     *                                                     &amp;lt;enumeration value="Triple"/&amp;gt;
                                     *                                                     &amp;lt;enumeration value="Quad"/&amp;gt;
                                     *                                                   &amp;lt;/restriction&amp;gt;
                                     *                                                 &amp;lt;/simpleType&amp;gt;
                                     *                                               &amp;lt;/attribute&amp;gt;
                                     *                                             &amp;lt;/restriction&amp;gt;
                                     *                                           &amp;lt;/complexContent&amp;gt;
                                     *                                         &amp;lt;/complexType&amp;gt;
                                     *                                       &amp;lt;/element&amp;gt;
                                     *                                     &amp;lt;/sequence&amp;gt;
                                     *                                   &amp;lt;/restriction&amp;gt;
                                     *                                 &amp;lt;/complexContent&amp;gt;
                                     *                               &amp;lt;/complexType&amp;gt;
                                     *                             &amp;lt;/element&amp;gt;
                                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                     *                           &amp;lt;/sequence&amp;gt;
                                     *                           &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                     *                           &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                                     *                         &amp;lt;/restriction&amp;gt;
                                     *                       &amp;lt;/complexContent&amp;gt;
                                     *                     &amp;lt;/complexType&amp;gt;
                                     *                   &amp;lt;/element&amp;gt;
                                     *                 &amp;lt;/sequence&amp;gt;
                                     *               &amp;lt;/restriction&amp;gt;
                                     *             &amp;lt;/complexContent&amp;gt;
                                     *           &amp;lt;/complexType&amp;gt;
                                     *         &amp;lt;/element&amp;gt;
                                     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                     *       &amp;lt;/sequence&amp;gt;
                                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                                     *       &amp;lt;attribute name="AvailabilityOptionType"&amp;gt;
                                     *         &amp;lt;simpleType&amp;gt;
                                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                     *             &amp;lt;enumeration value="First"/&amp;gt;
                                     *             &amp;lt;enumeration value="Second"/&amp;gt;
                                     *           &amp;lt;/restriction&amp;gt;
                                     *         &amp;lt;/simpleType&amp;gt;
                                     *       &amp;lt;/attribute&amp;gt;
                                     *     &amp;lt;/restriction&amp;gt;
                                     *   &amp;lt;/complexContent&amp;gt;
                                     * &amp;lt;/complexType&amp;gt;
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     */
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @XmlType(name = "", propOrder = {
                                        "stayDateRooms",
                                        "comment"
                                    })
                                    public static class StayDate {

                                        @XmlElement(name = "StayDateRooms")
                                        protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms stayDateRooms;
                                        @XmlElement(name = "Comment")
                                        protected List<ParagraphType> comment;
                                        @XmlAttribute(name = "AvailabilityOptionType")
                                        protected String availabilityOptionType;
                                        @XmlAttribute(name = "Start")
                                        protected String start;
                                        @XmlAttribute(name = "Duration")
                                        protected String duration;
                                        @XmlAttribute(name = "End")
                                        protected String end;

                                        /**
                                         * Obtiene el valor de la propiedad stayDateRooms.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms }
                                         *     
                                         */
                                        public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms getStayDateRooms() {
                                            return stayDateRooms;
                                        }

                                        /**
                                         * Define el valor de la propiedad stayDateRooms.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms }
                                         *     
                                         */
                                        public void setStayDateRooms(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms value) {
                                            this.stayDateRooms = value;
                                        }

                                        /**
                                         * Gets the value of the comment property.
                                         * 
                                         * &lt;p&gt;
                                         * This accessor method returns a reference to the live list,
                                         * not a snapshot. Therefore any modification you make to the
                                         * returned list will be present inside the JAXB object.
                                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                         * 
                                         * &lt;p&gt;
                                         * For example, to add a new item, do as follows:
                                         * &lt;pre&gt;
                                         *    getComment().add(newItem);
                                         * &lt;/pre&gt;
                                         * 
                                         * 
                                         * &lt;p&gt;
                                         * Objects of the following type(s) are allowed in the list
                                         * {@link ParagraphType }
                                         * 
                                         * 
                                         */
                                        public List<ParagraphType> getComment() {
                                            if (comment == null) {
                                                comment = new ArrayList<ParagraphType>();
                                            }
                                            return this.comment;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad availabilityOptionType.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getAvailabilityOptionType() {
                                            return availabilityOptionType;
                                        }

                                        /**
                                         * Define el valor de la propiedad availabilityOptionType.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setAvailabilityOptionType(String value) {
                                            this.availabilityOptionType = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad start.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getStart() {
                                            return start;
                                        }

                                        /**
                                         * Define el valor de la propiedad start.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setStart(String value) {
                                            this.start = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad duration.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getDuration() {
                                            return duration;
                                        }

                                        /**
                                         * Define el valor de la propiedad duration.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setDuration(String value) {
                                            this.duration = value;
                                        }

                                        /**
                                         * Obtiene el valor de la propiedad end.
                                         * 
                                         * @return
                                         *     possible object is
                                         *     {@link String }
                                         *     
                                         */
                                        public String getEnd() {
                                            return end;
                                        }

                                        /**
                                         * Define el valor de la propiedad end.
                                         * 
                                         * @param value
                                         *     allowed object is
                                         *     {@link String }
                                         *     
                                         */
                                        public void setEnd(String value) {
                                            this.end = value;
                                        }


                                        /**
                                         * &lt;p&gt;Clase Java para anonymous complex type.
                                         * 
                                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                         * 
                                         * &lt;pre&gt;
                                         * &amp;lt;complexType&amp;gt;
                                         *   &amp;lt;complexContent&amp;gt;
                                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                         *       &amp;lt;sequence&amp;gt;
                                         *         &amp;lt;element name="StayDateRoom" maxOccurs="unbounded"&amp;gt;
                                         *           &amp;lt;complexType&amp;gt;
                                         *             &amp;lt;complexContent&amp;gt;
                                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                         *                 &amp;lt;sequence&amp;gt;
                                         *                   &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                                         *                     &amp;lt;complexType&amp;gt;
                                         *                       &amp;lt;complexContent&amp;gt;
                                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                         *                           &amp;lt;sequence&amp;gt;
                                         *                             &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                                         *                               &amp;lt;complexType&amp;gt;
                                         *                                 &amp;lt;complexContent&amp;gt;
                                         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                         *                                     &amp;lt;sequence&amp;gt;
                                         *                                       &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                                         *                                       &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                                         *                                     &amp;lt;/sequence&amp;gt;
                                         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                         *                                     &amp;lt;attribute name="OccupancyRate"&amp;gt;
                                         *                                       &amp;lt;simpleType&amp;gt;
                                         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                         *                                           &amp;lt;enumeration value="Flat"/&amp;gt;
                                         *                                           &amp;lt;enumeration value="Single"/&amp;gt;
                                         *                                           &amp;lt;enumeration value="Double"/&amp;gt;
                                         *                                           &amp;lt;enumeration value="Triple"/&amp;gt;
                                         *                                           &amp;lt;enumeration value="Quad"/&amp;gt;
                                         *                                         &amp;lt;/restriction&amp;gt;
                                         *                                       &amp;lt;/simpleType&amp;gt;
                                         *                                     &amp;lt;/attribute&amp;gt;
                                         *                                   &amp;lt;/restriction&amp;gt;
                                         *                                 &amp;lt;/complexContent&amp;gt;
                                         *                               &amp;lt;/complexType&amp;gt;
                                         *                             &amp;lt;/element&amp;gt;
                                         *                           &amp;lt;/sequence&amp;gt;
                                         *                         &amp;lt;/restriction&amp;gt;
                                         *                       &amp;lt;/complexContent&amp;gt;
                                         *                     &amp;lt;/complexType&amp;gt;
                                         *                   &amp;lt;/element&amp;gt;
                                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                         *                 &amp;lt;/sequence&amp;gt;
                                         *                 &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                         *                 &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                                         *               &amp;lt;/restriction&amp;gt;
                                         *             &amp;lt;/complexContent&amp;gt;
                                         *           &amp;lt;/complexType&amp;gt;
                                         *         &amp;lt;/element&amp;gt;
                                         *       &amp;lt;/sequence&amp;gt;
                                         *     &amp;lt;/restriction&amp;gt;
                                         *   &amp;lt;/complexContent&amp;gt;
                                         * &amp;lt;/complexType&amp;gt;
                                         * &lt;/pre&gt;
                                         * 
                                         * 
                                         */
                                        @XmlAccessorType(XmlAccessType.FIELD)
                                        @XmlType(name = "", propOrder = {
                                            "stayDateRoom"
                                        })
                                        public static class StayDateRooms {

                                            @XmlElement(name = "StayDateRoom", required = true)
                                            protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom> stayDateRoom;

                                            /**
                                             * Gets the value of the stayDateRoom property.
                                             * 
                                             * &lt;p&gt;
                                             * This accessor method returns a reference to the live list,
                                             * not a snapshot. Therefore any modification you make to the
                                             * returned list will be present inside the JAXB object.
                                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stayDateRoom property.
                                             * 
                                             * &lt;p&gt;
                                             * For example, to add a new item, do as follows:
                                             * &lt;pre&gt;
                                             *    getStayDateRoom().add(newItem);
                                             * &lt;/pre&gt;
                                             * 
                                             * 
                                             * &lt;p&gt;
                                             * Objects of the following type(s) are allowed in the list
                                             * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom }
                                             * 
                                             * 
                                             */
                                            public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom> getStayDateRoom() {
                                                if (stayDateRoom == null) {
                                                    stayDateRoom = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom>();
                                                }
                                                return this.stayDateRoom;
                                            }


                                            /**
                                             * &lt;p&gt;Clase Java para anonymous complex type.
                                             * 
                                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                             * 
                                             * &lt;pre&gt;
                                             * &amp;lt;complexType&amp;gt;
                                             *   &amp;lt;complexContent&amp;gt;
                                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                             *       &amp;lt;sequence&amp;gt;
                                             *         &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                                             *           &amp;lt;complexType&amp;gt;
                                             *             &amp;lt;complexContent&amp;gt;
                                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                             *                 &amp;lt;sequence&amp;gt;
                                             *                   &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                                             *                     &amp;lt;complexType&amp;gt;
                                             *                       &amp;lt;complexContent&amp;gt;
                                             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                             *                           &amp;lt;sequence&amp;gt;
                                             *                             &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                                             *                             &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                                             *                           &amp;lt;/sequence&amp;gt;
                                             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                             *                           &amp;lt;attribute name="OccupancyRate"&amp;gt;
                                             *                             &amp;lt;simpleType&amp;gt;
                                             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                             *                                 &amp;lt;enumeration value="Flat"/&amp;gt;
                                             *                                 &amp;lt;enumeration value="Single"/&amp;gt;
                                             *                                 &amp;lt;enumeration value="Double"/&amp;gt;
                                             *                                 &amp;lt;enumeration value="Triple"/&amp;gt;
                                             *                                 &amp;lt;enumeration value="Quad"/&amp;gt;
                                             *                               &amp;lt;/restriction&amp;gt;
                                             *                             &amp;lt;/simpleType&amp;gt;
                                             *                           &amp;lt;/attribute&amp;gt;
                                             *                         &amp;lt;/restriction&amp;gt;
                                             *                       &amp;lt;/complexContent&amp;gt;
                                             *                     &amp;lt;/complexType&amp;gt;
                                             *                   &amp;lt;/element&amp;gt;
                                             *                 &amp;lt;/sequence&amp;gt;
                                             *               &amp;lt;/restriction&amp;gt;
                                             *             &amp;lt;/complexContent&amp;gt;
                                             *           &amp;lt;/complexType&amp;gt;
                                             *         &amp;lt;/element&amp;gt;
                                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                                             *       &amp;lt;/sequence&amp;gt;
                                             *       &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                             *       &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                                             *     &amp;lt;/restriction&amp;gt;
                                             *   &amp;lt;/complexContent&amp;gt;
                                             * &amp;lt;/complexType&amp;gt;
                                             * &lt;/pre&gt;
                                             * 
                                             * 
                                             */
                                            @XmlAccessorType(XmlAccessType.FIELD)
                                            @XmlType(name = "", propOrder = {
                                                "rates",
                                                "comment"
                                            })
                                            public static class StayDateRoom {

                                                @XmlElement(name = "Rates")
                                                protected RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates rates;
                                                @XmlElement(name = "Comment")
                                                protected List<ParagraphType> comment;
                                                @XmlAttribute(name = "RoomType")
                                                protected String roomType;
                                                @XmlAttribute(name = "NumberOfUnits")
                                                protected BigInteger numberOfUnits;

                                                /**
                                                 * Obtiene el valor de la propiedad rates.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates }
                                                 *     
                                                 */
                                                public RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates getRates() {
                                                    return rates;
                                                }

                                                /**
                                                 * Define el valor de la propiedad rates.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates }
                                                 *     
                                                 */
                                                public void setRates(RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates value) {
                                                    this.rates = value;
                                                }

                                                /**
                                                 * Gets the value of the comment property.
                                                 * 
                                                 * &lt;p&gt;
                                                 * This accessor method returns a reference to the live list,
                                                 * not a snapshot. Therefore any modification you make to the
                                                 * returned list will be present inside the JAXB object.
                                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                                 * 
                                                 * &lt;p&gt;
                                                 * For example, to add a new item, do as follows:
                                                 * &lt;pre&gt;
                                                 *    getComment().add(newItem);
                                                 * &lt;/pre&gt;
                                                 * 
                                                 * 
                                                 * &lt;p&gt;
                                                 * Objects of the following type(s) are allowed in the list
                                                 * {@link ParagraphType }
                                                 * 
                                                 * 
                                                 */
                                                public List<ParagraphType> getComment() {
                                                    if (comment == null) {
                                                        comment = new ArrayList<ParagraphType>();
                                                    }
                                                    return this.comment;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad roomType.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public String getRoomType() {
                                                    return roomType;
                                                }

                                                /**
                                                 * Define el valor de la propiedad roomType.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link String }
                                                 *     
                                                 */
                                                public void setRoomType(String value) {
                                                    this.roomType = value;
                                                }

                                                /**
                                                 * Obtiene el valor de la propiedad numberOfUnits.
                                                 * 
                                                 * @return
                                                 *     possible object is
                                                 *     {@link BigInteger }
                                                 *     
                                                 */
                                                public BigInteger getNumberOfUnits() {
                                                    return numberOfUnits;
                                                }

                                                /**
                                                 * Define el valor de la propiedad numberOfUnits.
                                                 * 
                                                 * @param value
                                                 *     allowed object is
                                                 *     {@link BigInteger }
                                                 *     
                                                 */
                                                public void setNumberOfUnits(BigInteger value) {
                                                    this.numberOfUnits = value;
                                                }


                                                /**
                                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                                 * 
                                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                                 * 
                                                 * &lt;pre&gt;
                                                 * &amp;lt;complexType&amp;gt;
                                                 *   &amp;lt;complexContent&amp;gt;
                                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                                 *       &amp;lt;sequence&amp;gt;
                                                 *         &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                                                 *           &amp;lt;complexType&amp;gt;
                                                 *             &amp;lt;complexContent&amp;gt;
                                                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                                 *                 &amp;lt;sequence&amp;gt;
                                                 *                   &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                                                 *                   &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                                                 *                 &amp;lt;/sequence&amp;gt;
                                                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                                 *                 &amp;lt;attribute name="OccupancyRate"&amp;gt;
                                                 *                   &amp;lt;simpleType&amp;gt;
                                                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                                 *                       &amp;lt;enumeration value="Flat"/&amp;gt;
                                                 *                       &amp;lt;enumeration value="Single"/&amp;gt;
                                                 *                       &amp;lt;enumeration value="Double"/&amp;gt;
                                                 *                       &amp;lt;enumeration value="Triple"/&amp;gt;
                                                 *                       &amp;lt;enumeration value="Quad"/&amp;gt;
                                                 *                     &amp;lt;/restriction&amp;gt;
                                                 *                   &amp;lt;/simpleType&amp;gt;
                                                 *                 &amp;lt;/attribute&amp;gt;
                                                 *               &amp;lt;/restriction&amp;gt;
                                                 *             &amp;lt;/complexContent&amp;gt;
                                                 *           &amp;lt;/complexType&amp;gt;
                                                 *         &amp;lt;/element&amp;gt;
                                                 *       &amp;lt;/sequence&amp;gt;
                                                 *     &amp;lt;/restriction&amp;gt;
                                                 *   &amp;lt;/complexContent&amp;gt;
                                                 * &amp;lt;/complexType&amp;gt;
                                                 * &lt;/pre&gt;
                                                 * 
                                                 * 
                                                 */
                                                @XmlAccessorType(XmlAccessType.FIELD)
                                                @XmlType(name = "", propOrder = {
                                                    "rate"
                                                })
                                                public static class Rates {

                                                    @XmlElement(name = "Rate", required = true)
                                                    protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates.Rate> rate;

                                                    /**
                                                     * Gets the value of the rate property.
                                                     * 
                                                     * &lt;p&gt;
                                                     * This accessor method returns a reference to the live list,
                                                     * not a snapshot. Therefore any modification you make to the
                                                     * returned list will be present inside the JAXB object.
                                                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rate property.
                                                     * 
                                                     * &lt;p&gt;
                                                     * For example, to add a new item, do as follows:
                                                     * &lt;pre&gt;
                                                     *    getRate().add(newItem);
                                                     * &lt;/pre&gt;
                                                     * 
                                                     * 
                                                     * &lt;p&gt;
                                                     * Objects of the following type(s) are allowed in the list
                                                     * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates.Rate }
                                                     * 
                                                     * 
                                                     */
                                                    public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates.Rate> getRate() {
                                                        if (rate == null) {
                                                            rate = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.Dates.Date.RoomBlock.StayDates.StayDate.StayDateRooms.StayDateRoom.Rates.Rate>();
                                                        }
                                                        return this.rate;
                                                    }


                                                    /**
                                                     * &lt;p&gt;Clase Java para anonymous complex type.
                                                     * 
                                                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                                     * 
                                                     * &lt;pre&gt;
                                                     * &amp;lt;complexType&amp;gt;
                                                     *   &amp;lt;complexContent&amp;gt;
                                                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                                     *       &amp;lt;sequence&amp;gt;
                                                     *         &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                                                     *         &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                                                     *       &amp;lt;/sequence&amp;gt;
                                                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                                     *       &amp;lt;attribute name="OccupancyRate"&amp;gt;
                                                     *         &amp;lt;simpleType&amp;gt;
                                                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                                     *             &amp;lt;enumeration value="Flat"/&amp;gt;
                                                     *             &amp;lt;enumeration value="Single"/&amp;gt;
                                                     *             &amp;lt;enumeration value="Double"/&amp;gt;
                                                     *             &amp;lt;enumeration value="Triple"/&amp;gt;
                                                     *             &amp;lt;enumeration value="Quad"/&amp;gt;
                                                     *           &amp;lt;/restriction&amp;gt;
                                                     *         &amp;lt;/simpleType&amp;gt;
                                                     *       &amp;lt;/attribute&amp;gt;
                                                     *     &amp;lt;/restriction&amp;gt;
                                                     *   &amp;lt;/complexContent&amp;gt;
                                                     * &amp;lt;/complexType&amp;gt;
                                                     * &lt;/pre&gt;
                                                     * 
                                                     * 
                                                     */
                                                    @XmlAccessorType(XmlAccessType.FIELD)
                                                    @XmlType(name = "", propOrder = {
                                                        "taxes",
                                                        "fees"
                                                    })
                                                    public static class Rate {

                                                        @XmlElement(name = "Taxes")
                                                        protected TaxesType taxes;
                                                        @XmlElement(name = "Fees")
                                                        protected FeesType fees;
                                                        @XmlAttribute(name = "OccupancyRate")
                                                        protected String occupancyRate;
                                                        @XmlAttribute(name = "CurrencyCode")
                                                        protected String currencyCode;
                                                        @XmlAttribute(name = "DecimalPlaces")
                                                        @XmlSchemaType(name = "nonNegativeInteger")
                                                        protected BigInteger decimalPlaces;
                                                        @XmlAttribute(name = "Amount")
                                                        protected BigDecimal amount;

                                                        /**
                                                         * Obtiene el valor de la propiedad taxes.
                                                         * 
                                                         * @return
                                                         *     possible object is
                                                         *     {@link TaxesType }
                                                         *     
                                                         */
                                                        public TaxesType getTaxes() {
                                                            return taxes;
                                                        }

                                                        /**
                                                         * Define el valor de la propiedad taxes.
                                                         * 
                                                         * @param value
                                                         *     allowed object is
                                                         *     {@link TaxesType }
                                                         *     
                                                         */
                                                        public void setTaxes(TaxesType value) {
                                                            this.taxes = value;
                                                        }

                                                        /**
                                                         * Obtiene el valor de la propiedad fees.
                                                         * 
                                                         * @return
                                                         *     possible object is
                                                         *     {@link FeesType }
                                                         *     
                                                         */
                                                        public FeesType getFees() {
                                                            return fees;
                                                        }

                                                        /**
                                                         * Define el valor de la propiedad fees.
                                                         * 
                                                         * @param value
                                                         *     allowed object is
                                                         *     {@link FeesType }
                                                         *     
                                                         */
                                                        public void setFees(FeesType value) {
                                                            this.fees = value;
                                                        }

                                                        /**
                                                         * Obtiene el valor de la propiedad occupancyRate.
                                                         * 
                                                         * @return
                                                         *     possible object is
                                                         *     {@link String }
                                                         *     
                                                         */
                                                        public String getOccupancyRate() {
                                                            return occupancyRate;
                                                        }

                                                        /**
                                                         * Define el valor de la propiedad occupancyRate.
                                                         * 
                                                         * @param value
                                                         *     allowed object is
                                                         *     {@link String }
                                                         *     
                                                         */
                                                        public void setOccupancyRate(String value) {
                                                            this.occupancyRate = value;
                                                        }

                                                        /**
                                                         * Obtiene el valor de la propiedad currencyCode.
                                                         * 
                                                         * @return
                                                         *     possible object is
                                                         *     {@link String }
                                                         *     
                                                         */
                                                        public String getCurrencyCode() {
                                                            return currencyCode;
                                                        }

                                                        /**
                                                         * Define el valor de la propiedad currencyCode.
                                                         * 
                                                         * @param value
                                                         *     allowed object is
                                                         *     {@link String }
                                                         *     
                                                         */
                                                        public void setCurrencyCode(String value) {
                                                            this.currencyCode = value;
                                                        }

                                                        /**
                                                         * Obtiene el valor de la propiedad decimalPlaces.
                                                         * 
                                                         * @return
                                                         *     possible object is
                                                         *     {@link BigInteger }
                                                         *     
                                                         */
                                                        public BigInteger getDecimalPlaces() {
                                                            return decimalPlaces;
                                                        }

                                                        /**
                                                         * Define el valor de la propiedad decimalPlaces.
                                                         * 
                                                         * @param value
                                                         *     allowed object is
                                                         *     {@link BigInteger }
                                                         *     
                                                         */
                                                        public void setDecimalPlaces(BigInteger value) {
                                                            this.decimalPlaces = value;
                                                        }

                                                        /**
                                                         * Obtiene el valor de la propiedad amount.
                                                         * 
                                                         * @return
                                                         *     possible object is
                                                         *     {@link BigDecimal }
                                                         *     
                                                         */
                                                        public BigDecimal getAmount() {
                                                            return amount;
                                                        }

                                                        /**
                                                         * Define el valor de la propiedad amount.
                                                         * 
                                                         * @param value
                                                         *     allowed object is
                                                         *     {@link BigDecimal }
                                                         *     
                                                         */
                                                        public void setAmount(BigDecimal value) {
                                                            this.amount = value;
                                                        }

                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="InsuranceInfo" maxOccurs="99" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "insuranceInfo",
                        "comments"
                    })
                    public static class InsuranceInfos {

                        @XmlElement(name = "InsuranceInfo")
                        protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos.InsuranceInfo> insuranceInfo;
                        @XmlElement(name = "Comments")
                        protected List<ParagraphType> comments;

                        /**
                         * Gets the value of the insuranceInfo property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the insuranceInfo property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getInsuranceInfo().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos.InsuranceInfo }
                         * 
                         * 
                         */
                        public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos.InsuranceInfo> getInsuranceInfo() {
                            if (insuranceInfo == null) {
                                insuranceInfo = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.InsuranceInfos.InsuranceInfo>();
                            }
                            return this.insuranceInfo;
                        }

                        /**
                         * Gets the value of the comments property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comments property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getComments().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link ParagraphType }
                         * 
                         * 
                         */
                        public List<ParagraphType> getComments() {
                            if (comments == null) {
                                comments = new ArrayList<ParagraphType>();
                            }
                            return this.comments;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *       &amp;lt;attribute name="InsuranceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class InsuranceInfo {

                            @XmlAttribute(name = "InsuranceTypeCode")
                            protected String insuranceTypeCode;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "DecimalPlaces")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger decimalPlaces;
                            @XmlAttribute(name = "Amount")
                            protected BigDecimal amount;

                            /**
                             * Obtiene el valor de la propiedad insuranceTypeCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getInsuranceTypeCode() {
                                return insuranceTypeCode;
                            }

                            /**
                             * Define el valor de la propiedad insuranceTypeCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setInsuranceTypeCode(String value) {
                                this.insuranceTypeCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad currencyCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Define el valor de la propiedad currencyCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad decimalPlaces.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDecimalPlaces() {
                                return decimalPlaces;
                            }

                            /**
                             * Define el valor de la propiedad decimalPlaces.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDecimalPlaces(BigInteger value) {
                                this.decimalPlaces = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setAmount(BigDecimal value) {
                                this.amount = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Reference" maxOccurs="99"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                 &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                 &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "reference"
                    })
                    public static class References {

                        @XmlElement(name = "Reference", required = true)
                        protected List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References.Reference> reference;

                        /**
                         * Gets the value of the reference property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reference property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getReference().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References.Reference }
                         * 
                         * 
                         */
                        public List<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References.Reference> getReference() {
                            if (reference == null) {
                                reference = new ArrayList<RFPResponseType.RFPResponseSegments.RFPResponseSegment.Sites.Site.References.Reference>();
                            }
                            return this.reference;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                         *       &amp;lt;attribute name="MeetingName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *       &amp;lt;attribute name="MeetingHost" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *       &amp;lt;attribute name="MeetingType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class Reference
                            extends ContactPersonType
                        {

                            @XmlAttribute(name = "MeetingName")
                            protected String meetingName;
                            @XmlAttribute(name = "MeetingHost")
                            protected String meetingHost;
                            @XmlAttribute(name = "MeetingType")
                            protected String meetingType;
                            @XmlAttribute(name = "Start")
                            protected String start;
                            @XmlAttribute(name = "Duration")
                            protected String duration;
                            @XmlAttribute(name = "End")
                            protected String end;

                            /**
                             * Obtiene el valor de la propiedad meetingName.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getMeetingName() {
                                return meetingName;
                            }

                            /**
                             * Define el valor de la propiedad meetingName.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setMeetingName(String value) {
                                this.meetingName = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad meetingHost.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getMeetingHost() {
                                return meetingHost;
                            }

                            /**
                             * Define el valor de la propiedad meetingHost.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setMeetingHost(String value) {
                                this.meetingHost = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad meetingType.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getMeetingType() {
                                return meetingType;
                            }

                            /**
                             * Define el valor de la propiedad meetingType.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setMeetingType(String value) {
                                this.meetingType = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad start.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getStart() {
                                return start;
                            }

                            /**
                             * Define el valor de la propiedad start.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setStart(String value) {
                                this.start = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad duration.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDuration() {
                                return duration;
                            }

                            /**
                             * Define el valor de la propiedad duration.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDuration(String value) {
                                this.duration = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad end.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getEnd() {
                                return end;
                            }

                            /**
                             * Define el valor de la propiedad end.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setEnd(String value) {
                                this.end = value;
                            }

                        }

                    }

                }

            }

        }

    }

}
