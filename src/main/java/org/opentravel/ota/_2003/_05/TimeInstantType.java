
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.Duration;


/**
 * Specifies a time window.
 * 
 * &lt;p&gt;Clase Java para TimeInstantType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TimeInstantType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;DateOrDateTimeType"&amp;gt;
 *       &amp;lt;attribute name="WindowBefore" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *       &amp;lt;attribute name="WindowAfter" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *       &amp;lt;attribute name="CrossDateAllowedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeInstantType", propOrder = {
    "value"
})
public class TimeInstantType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "WindowBefore")
    protected Duration windowBefore;
    @XmlAttribute(name = "WindowAfter")
    protected Duration windowAfter;
    @XmlAttribute(name = "CrossDateAllowedIndicator")
    protected Boolean crossDateAllowedIndicator;

    /**
     * A construct to validate either a date or a dateTime value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad windowBefore.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getWindowBefore() {
        return windowBefore;
    }

    /**
     * Define el valor de la propiedad windowBefore.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setWindowBefore(Duration value) {
        this.windowBefore = value;
    }

    /**
     * Obtiene el valor de la propiedad windowAfter.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getWindowAfter() {
        return windowAfter;
    }

    /**
     * Define el valor de la propiedad windowAfter.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setWindowAfter(Duration value) {
        this.windowAfter = value;
    }

    /**
     * Obtiene el valor de la propiedad crossDateAllowedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCrossDateAllowedIndicator() {
        return crossDateAllowedIndicator;
    }

    /**
     * Define el valor de la propiedad crossDateAllowedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCrossDateAllowedIndicator(Boolean value) {
        this.crossDateAllowedIndicator = value;
    }

}
