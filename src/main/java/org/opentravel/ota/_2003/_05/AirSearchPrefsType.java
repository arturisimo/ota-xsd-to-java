
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Defines user preferences to be used in conducting a search.
 * 
 * &lt;p&gt;Clase Java para AirSearchPrefsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirSearchPrefsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="VendorPref" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNamePrefType"&amp;gt;
 *                 &amp;lt;attribute name="AllianceAllowedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="LoyaltyAllowedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="AwardOnlyFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FlightTypePref" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FlightTypePrefGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FareRestrictPref" maxOccurs="4" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AdvResTicketing" type="{http://www.opentravel.org/OTA/2003/05}AdvResTicketingType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="StayRestrictions" type="{http://www.opentravel.org/OTA/2003/05}StayRestrictionsType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="VoluntaryChanges" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FareRestrictPrefGroup"/&amp;gt;
 *                 &amp;lt;attribute name="FareDisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                 &amp;lt;attribute name="CurrencyOverride" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="EquipPref" type="{http://www.opentravel.org/OTA/2003/05}EquipmentTypePref" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CabinPref" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CabinPrefGroup"/&amp;gt;
 *                 &amp;lt;attribute name="CabinSubtype" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TicketDistribPref" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TicketDistribPrefType"&amp;gt;
 *                 &amp;lt;attribute name="LastTicketDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                 &amp;lt;attribute name="FirstTicketDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BookingSeatPref" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassPrefOptionalGroup"/&amp;gt;
 *                 &amp;lt;attribute name="SeatsNeeded" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
 *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *       &amp;lt;attribute name="ETicketDesired" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="MaxStopsQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirSearchPrefsType", propOrder = {
    "vendorPref",
    "flightTypePref",
    "fareRestrictPref",
    "equipPref",
    "cabinPref",
    "ticketDistribPref",
    "bookingSeatPref"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirFareDisplayRQ.TravelPreferences.class,
    org.opentravel.ota._2003._05.OTAAirGetOfferRQ.RequestCriterion.TravelPreferences.class,
    org.opentravel.ota._2003._05.OTAAirLowFareSearchRQ.TravelPreferences.class
})
public class AirSearchPrefsType {

    @XmlElement(name = "VendorPref")
    protected List<AirSearchPrefsType.VendorPref> vendorPref;
    @XmlElement(name = "FlightTypePref")
    protected List<AirSearchPrefsType.FlightTypePref> flightTypePref;
    @XmlElement(name = "FareRestrictPref")
    protected List<AirSearchPrefsType.FareRestrictPref> fareRestrictPref;
    @XmlElement(name = "EquipPref")
    protected List<EquipmentTypePref> equipPref;
    @XmlElement(name = "CabinPref")
    protected List<AirSearchPrefsType.CabinPref> cabinPref;
    @XmlElement(name = "TicketDistribPref")
    protected List<AirSearchPrefsType.TicketDistribPref> ticketDistribPref;
    @XmlElement(name = "BookingSeatPref")
    protected AirSearchPrefsType.BookingSeatPref bookingSeatPref;
    @XmlAttribute(name = "OnTimeRate")
    protected BigDecimal onTimeRate;
    @XmlAttribute(name = "ETicketDesired")
    protected Boolean eTicketDesired;
    @XmlAttribute(name = "MaxStopsQuantity")
    protected Integer maxStopsQuantity;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;
    @XmlAttribute(name = "SmokingAllowed")
    protected Boolean smokingAllowed;

    /**
     * Gets the value of the vendorPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getVendorPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirSearchPrefsType.VendorPref }
     * 
     * 
     */
    public List<AirSearchPrefsType.VendorPref> getVendorPref() {
        if (vendorPref == null) {
            vendorPref = new ArrayList<AirSearchPrefsType.VendorPref>();
        }
        return this.vendorPref;
    }

    /**
     * Gets the value of the flightTypePref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightTypePref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFlightTypePref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirSearchPrefsType.FlightTypePref }
     * 
     * 
     */
    public List<AirSearchPrefsType.FlightTypePref> getFlightTypePref() {
        if (flightTypePref == null) {
            flightTypePref = new ArrayList<AirSearchPrefsType.FlightTypePref>();
        }
        return this.flightTypePref;
    }

    /**
     * Gets the value of the fareRestrictPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareRestrictPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFareRestrictPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirSearchPrefsType.FareRestrictPref }
     * 
     * 
     */
    public List<AirSearchPrefsType.FareRestrictPref> getFareRestrictPref() {
        if (fareRestrictPref == null) {
            fareRestrictPref = new ArrayList<AirSearchPrefsType.FareRestrictPref>();
        }
        return this.fareRestrictPref;
    }

    /**
     * Gets the value of the equipPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the equipPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getEquipPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentTypePref }
     * 
     * 
     */
    public List<EquipmentTypePref> getEquipPref() {
        if (equipPref == null) {
            equipPref = new ArrayList<EquipmentTypePref>();
        }
        return this.equipPref;
    }

    /**
     * Gets the value of the cabinPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cabinPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCabinPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirSearchPrefsType.CabinPref }
     * 
     * 
     */
    public List<AirSearchPrefsType.CabinPref> getCabinPref() {
        if (cabinPref == null) {
            cabinPref = new ArrayList<AirSearchPrefsType.CabinPref>();
        }
        return this.cabinPref;
    }

    /**
     * Gets the value of the ticketDistribPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ticketDistribPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTicketDistribPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirSearchPrefsType.TicketDistribPref }
     * 
     * 
     */
    public List<AirSearchPrefsType.TicketDistribPref> getTicketDistribPref() {
        if (ticketDistribPref == null) {
            ticketDistribPref = new ArrayList<AirSearchPrefsType.TicketDistribPref>();
        }
        return this.ticketDistribPref;
    }

    /**
     * Obtiene el valor de la propiedad bookingSeatPref.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchPrefsType.BookingSeatPref }
     *     
     */
    public AirSearchPrefsType.BookingSeatPref getBookingSeatPref() {
        return bookingSeatPref;
    }

    /**
     * Define el valor de la propiedad bookingSeatPref.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchPrefsType.BookingSeatPref }
     *     
     */
    public void setBookingSeatPref(AirSearchPrefsType.BookingSeatPref value) {
        this.bookingSeatPref = value;
    }

    /**
     * Obtiene el valor de la propiedad onTimeRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOnTimeRate() {
        return onTimeRate;
    }

    /**
     * Define el valor de la propiedad onTimeRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOnTimeRate(BigDecimal value) {
        this.onTimeRate = value;
    }

    /**
     * Obtiene el valor de la propiedad eTicketDesired.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isETicketDesired() {
        return eTicketDesired;
    }

    /**
     * Define el valor de la propiedad eTicketDesired.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setETicketDesired(Boolean value) {
        this.eTicketDesired = value;
    }

    /**
     * Obtiene el valor de la propiedad maxStopsQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxStopsQuantity() {
        return maxStopsQuantity;
    }

    /**
     * Define el valor de la propiedad maxStopsQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxStopsQuantity(Integer value) {
        this.maxStopsQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }

    /**
     * Obtiene el valor de la propiedad smokingAllowed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSmokingAllowed() {
        return smokingAllowed;
    }

    /**
     * Define el valor de la propiedad smokingAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmokingAllowed(Boolean value) {
        this.smokingAllowed = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassPrefOptionalGroup"/&amp;gt;
     *       &amp;lt;attribute name="SeatsNeeded" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BookingSeatPref {

        @XmlAttribute(name = "SeatsNeeded")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger seatsNeeded;
        @XmlAttribute(name = "ResBookDesigCode")
        protected String resBookDesigCode;
        @XmlAttribute(name = "ResBookDesigCodeType")
        protected String resBookDesigCodeType;
        @XmlAttribute(name = "PreferLevel")
        protected PreferLevelType preferLevel;

        /**
         * Obtiene el valor de la propiedad seatsNeeded.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSeatsNeeded() {
            return seatsNeeded;
        }

        /**
         * Define el valor de la propiedad seatsNeeded.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSeatsNeeded(BigInteger value) {
            this.seatsNeeded = value;
        }

        /**
         * Obtiene el valor de la propiedad resBookDesigCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResBookDesigCode() {
            return resBookDesigCode;
        }

        /**
         * Define el valor de la propiedad resBookDesigCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResBookDesigCode(String value) {
            this.resBookDesigCode = value;
        }

        /**
         * Obtiene el valor de la propiedad resBookDesigCodeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResBookDesigCodeType() {
            return resBookDesigCodeType;
        }

        /**
         * Define el valor de la propiedad resBookDesigCodeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResBookDesigCodeType(String value) {
            this.resBookDesigCodeType = value;
        }

        /**
         * Obtiene el valor de la propiedad preferLevel.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreferLevel() {
            return preferLevel;
        }

        /**
         * Define el valor de la propiedad preferLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreferLevel(PreferLevelType value) {
            this.preferLevel = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CabinPrefGroup"/&amp;gt;
     *       &amp;lt;attribute name="CabinSubtype" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CabinPref {

        @XmlAttribute(name = "CabinSubtype")
        protected String cabinSubtype;
        @XmlAttribute(name = "Cabin")
        protected String cabin;
        @XmlAttribute(name = "PreferLevel")
        protected PreferLevelType preferLevel;

        /**
         * Obtiene el valor de la propiedad cabinSubtype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCabinSubtype() {
            return cabinSubtype;
        }

        /**
         * Define el valor de la propiedad cabinSubtype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCabinSubtype(String value) {
            this.cabinSubtype = value;
        }

        /**
         * Obtiene el valor de la propiedad cabin.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCabin() {
            return cabin;
        }

        /**
         * Define el valor de la propiedad cabin.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCabin(String value) {
            this.cabin = value;
        }

        /**
         * Obtiene el valor de la propiedad preferLevel.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreferLevel() {
            return preferLevel;
        }

        /**
         * Define el valor de la propiedad preferLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreferLevel(PreferLevelType value) {
            this.preferLevel = value;
        }

    }


    /**
     * Container used for specifying or limiting acceptable fare restrictions.
     * 
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AdvResTicketing" type="{http://www.opentravel.org/OTA/2003/05}AdvResTicketingType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="StayRestrictions" type="{http://www.opentravel.org/OTA/2003/05}StayRestrictionsType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="VoluntaryChanges" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FareRestrictPrefGroup"/&amp;gt;
     *       &amp;lt;attribute name="FareDisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *       &amp;lt;attribute name="CurrencyOverride" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "advResTicketing",
        "stayRestrictions",
        "voluntaryChanges"
    })
    public static class FareRestrictPref {

        @XmlElement(name = "AdvResTicketing")
        protected AdvResTicketingType advResTicketing;
        @XmlElement(name = "StayRestrictions")
        protected StayRestrictionsType stayRestrictions;
        @XmlElement(name = "VoluntaryChanges")
        protected VoluntaryChangesType voluntaryChanges;
        @XmlAttribute(name = "FareDisplayCurrency")
        protected String fareDisplayCurrency;
        @XmlAttribute(name = "CurrencyOverride")
        protected String currencyOverride;
        @XmlAttribute(name = "PreferLevel")
        protected PreferLevelType preferLevel;
        @XmlAttribute(name = "FareRestriction")
        protected String fareRestriction;
        @XmlAttribute(name = "Date")
        protected String date;

        /**
         * Obtiene el valor de la propiedad advResTicketing.
         * 
         * @return
         *     possible object is
         *     {@link AdvResTicketingType }
         *     
         */
        public AdvResTicketingType getAdvResTicketing() {
            return advResTicketing;
        }

        /**
         * Define el valor de la propiedad advResTicketing.
         * 
         * @param value
         *     allowed object is
         *     {@link AdvResTicketingType }
         *     
         */
        public void setAdvResTicketing(AdvResTicketingType value) {
            this.advResTicketing = value;
        }

        /**
         * Obtiene el valor de la propiedad stayRestrictions.
         * 
         * @return
         *     possible object is
         *     {@link StayRestrictionsType }
         *     
         */
        public StayRestrictionsType getStayRestrictions() {
            return stayRestrictions;
        }

        /**
         * Define el valor de la propiedad stayRestrictions.
         * 
         * @param value
         *     allowed object is
         *     {@link StayRestrictionsType }
         *     
         */
        public void setStayRestrictions(StayRestrictionsType value) {
            this.stayRestrictions = value;
        }

        /**
         * Obtiene el valor de la propiedad voluntaryChanges.
         * 
         * @return
         *     possible object is
         *     {@link VoluntaryChangesType }
         *     
         */
        public VoluntaryChangesType getVoluntaryChanges() {
            return voluntaryChanges;
        }

        /**
         * Define el valor de la propiedad voluntaryChanges.
         * 
         * @param value
         *     allowed object is
         *     {@link VoluntaryChangesType }
         *     
         */
        public void setVoluntaryChanges(VoluntaryChangesType value) {
            this.voluntaryChanges = value;
        }

        /**
         * Obtiene el valor de la propiedad fareDisplayCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareDisplayCurrency() {
            return fareDisplayCurrency;
        }

        /**
         * Define el valor de la propiedad fareDisplayCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareDisplayCurrency(String value) {
            this.fareDisplayCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyOverride.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyOverride() {
            return currencyOverride;
        }

        /**
         * Define el valor de la propiedad currencyOverride.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyOverride(String value) {
            this.currencyOverride = value;
        }

        /**
         * Obtiene el valor de la propiedad preferLevel.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreferLevel() {
            return preferLevel;
        }

        /**
         * Define el valor de la propiedad preferLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreferLevel(PreferLevelType value) {
            this.preferLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad fareRestriction.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareRestriction() {
            return fareRestriction;
        }

        /**
         * Define el valor de la propiedad fareRestriction.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareRestriction(String value) {
            this.fareRestriction = value;
        }

        /**
         * Obtiene el valor de la propiedad date.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDate() {
            return date;
        }

        /**
         * Define el valor de la propiedad date.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDate(String value) {
            this.date = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FlightTypePrefGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FlightTypePref {

        @XmlAttribute(name = "FlightType")
        protected FlightTypeType flightType;
        @XmlAttribute(name = "MaxConnections")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger maxConnections;
        @XmlAttribute(name = "NonScheduledFltInfo")
        protected String nonScheduledFltInfo;
        @XmlAttribute(name = "BackhaulIndicator")
        protected Boolean backhaulIndicator;
        @XmlAttribute(name = "GroundTransportIndicator")
        protected Boolean groundTransportIndicator;
        @XmlAttribute(name = "DirectAndNonStopOnlyInd")
        protected Boolean directAndNonStopOnlyInd;
        @XmlAttribute(name = "NonStopsOnlyInd")
        protected Boolean nonStopsOnlyInd;
        @XmlAttribute(name = "OnlineConnectionsOnlyInd")
        protected Boolean onlineConnectionsOnlyInd;
        @XmlAttribute(name = "RoutingType")
        protected String routingType;
        @XmlAttribute(name = "ExcludeTrainInd")
        protected Boolean excludeTrainInd;
        @XmlAttribute(name = "PreferLevel")
        protected PreferLevelType preferLevel;

        /**
         * Obtiene el valor de la propiedad flightType.
         * 
         * @return
         *     possible object is
         *     {@link FlightTypeType }
         *     
         */
        public FlightTypeType getFlightType() {
            return flightType;
        }

        /**
         * Define el valor de la propiedad flightType.
         * 
         * @param value
         *     allowed object is
         *     {@link FlightTypeType }
         *     
         */
        public void setFlightType(FlightTypeType value) {
            this.flightType = value;
        }

        /**
         * Obtiene el valor de la propiedad maxConnections.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxConnections() {
            return maxConnections;
        }

        /**
         * Define el valor de la propiedad maxConnections.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxConnections(BigInteger value) {
            this.maxConnections = value;
        }

        /**
         * Obtiene el valor de la propiedad nonScheduledFltInfo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNonScheduledFltInfo() {
            return nonScheduledFltInfo;
        }

        /**
         * Define el valor de la propiedad nonScheduledFltInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNonScheduledFltInfo(String value) {
            this.nonScheduledFltInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad backhaulIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isBackhaulIndicator() {
            return backhaulIndicator;
        }

        /**
         * Define el valor de la propiedad backhaulIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setBackhaulIndicator(Boolean value) {
            this.backhaulIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad groundTransportIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGroundTransportIndicator() {
            return groundTransportIndicator;
        }

        /**
         * Define el valor de la propiedad groundTransportIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGroundTransportIndicator(Boolean value) {
            this.groundTransportIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad directAndNonStopOnlyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDirectAndNonStopOnlyInd() {
            return directAndNonStopOnlyInd;
        }

        /**
         * Define el valor de la propiedad directAndNonStopOnlyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDirectAndNonStopOnlyInd(Boolean value) {
            this.directAndNonStopOnlyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonStopsOnlyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonStopsOnlyInd() {
            return nonStopsOnlyInd;
        }

        /**
         * Define el valor de la propiedad nonStopsOnlyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonStopsOnlyInd(Boolean value) {
            this.nonStopsOnlyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad onlineConnectionsOnlyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOnlineConnectionsOnlyInd() {
            return onlineConnectionsOnlyInd;
        }

        /**
         * Define el valor de la propiedad onlineConnectionsOnlyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOnlineConnectionsOnlyInd(Boolean value) {
            this.onlineConnectionsOnlyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad routingType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoutingType() {
            return routingType;
        }

        /**
         * Define el valor de la propiedad routingType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoutingType(String value) {
            this.routingType = value;
        }

        /**
         * Obtiene el valor de la propiedad excludeTrainInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExcludeTrainInd() {
            return excludeTrainInd;
        }

        /**
         * Define el valor de la propiedad excludeTrainInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExcludeTrainInd(Boolean value) {
            this.excludeTrainInd = value;
        }

        /**
         * Obtiene el valor de la propiedad preferLevel.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreferLevel() {
            return preferLevel;
        }

        /**
         * Define el valor de la propiedad preferLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreferLevel(PreferLevelType value) {
            this.preferLevel = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TicketDistribPrefType"&amp;gt;
     *       &amp;lt;attribute name="LastTicketDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *       &amp;lt;attribute name="FirstTicketDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TicketDistribPref
        extends TicketDistribPrefType
    {

        @XmlAttribute(name = "LastTicketDate")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastTicketDate;
        @XmlAttribute(name = "FirstTicketDate")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar firstTicketDate;

        /**
         * Obtiene el valor de la propiedad lastTicketDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastTicketDate() {
            return lastTicketDate;
        }

        /**
         * Define el valor de la propiedad lastTicketDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastTicketDate(XMLGregorianCalendar value) {
            this.lastTicketDate = value;
        }

        /**
         * Obtiene el valor de la propiedad firstTicketDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFirstTicketDate() {
            return firstTicketDate;
        }

        /**
         * Define el valor de la propiedad firstTicketDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFirstTicketDate(XMLGregorianCalendar value) {
            this.firstTicketDate = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNamePrefType"&amp;gt;
     *       &amp;lt;attribute name="AllianceAllowedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="LoyaltyAllowedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="AwardOnlyFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class VendorPref
        extends CompanyNamePrefType
    {

        @XmlAttribute(name = "AllianceAllowedInd")
        protected Boolean allianceAllowedInd;
        @XmlAttribute(name = "LoyaltyAllowedInd")
        protected Boolean loyaltyAllowedInd;
        @XmlAttribute(name = "AwardOnlyFareInd")
        protected Boolean awardOnlyFareInd;

        /**
         * Obtiene el valor de la propiedad allianceAllowedInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAllianceAllowedInd() {
            return allianceAllowedInd;
        }

        /**
         * Define el valor de la propiedad allianceAllowedInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAllianceAllowedInd(Boolean value) {
            this.allianceAllowedInd = value;
        }

        /**
         * Obtiene el valor de la propiedad loyaltyAllowedInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isLoyaltyAllowedInd() {
            return loyaltyAllowedInd;
        }

        /**
         * Define el valor de la propiedad loyaltyAllowedInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setLoyaltyAllowedInd(Boolean value) {
            this.loyaltyAllowedInd = value;
        }

        /**
         * Obtiene el valor de la propiedad awardOnlyFareInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAwardOnlyFareInd() {
            return awardOnlyFareInd;
        }

        /**
         * Define el valor de la propiedad awardOnlyFareInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAwardOnlyFareInd(Boolean value) {
            this.awardOnlyFareInd = value;
        }

    }

}
