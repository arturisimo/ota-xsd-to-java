
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para FareStatusType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="FareStatusType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="constructed"/&amp;gt;
 *     &amp;lt;enumeration value="published"/&amp;gt;
 *     &amp;lt;enumeration value="created"/&amp;gt;
 *     &amp;lt;enumeration value="fareByRule"/&amp;gt;
 *     &amp;lt;enumeration value="fareByRulePrivate"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "FareStatusType")
@XmlEnum
public enum FareStatusType {

    @XmlEnumValue("constructed")
    CONSTRUCTED("constructed"),
    @XmlEnumValue("published")
    PUBLISHED("published"),
    @XmlEnumValue("created")
    CREATED("created"),

    /**
     * Specifies that the fare was built based on rules.
     * 
     * 
     */
    @XmlEnumValue("fareByRule")
    FARE_BY_RULE("fareByRule"),

    /**
     * The private fare was built by rules.
     * 
     */
    @XmlEnumValue("fareByRulePrivate")
    FARE_BY_RULE_PRIVATE("fareByRulePrivate");
    private final String value;

    FareStatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FareStatusType fromValue(String v) {
        for (FareStatusType c: FareStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
