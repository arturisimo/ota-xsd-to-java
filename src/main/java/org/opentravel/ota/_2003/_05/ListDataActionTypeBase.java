
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_DataActionType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_DataActionType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Create/Add"/&amp;gt;
 *     &amp;lt;enumeration value="Read/Query"/&amp;gt;
 *     &amp;lt;enumeration value="Update/Modify"/&amp;gt;
 *     &amp;lt;enumeration value="Cancel"/&amp;gt;
 *     &amp;lt;enumeration value="Delete"/&amp;gt;
 *     &amp;lt;enumeration value="Replace"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_DataActionType_Base")
@XmlEnum
public enum ListDataActionTypeBase {


    /**
     * Add specified data whether the data already exists or not.
     * 
     */
    @XmlEnumValue("Create/Add")
    CREATE_ADD("Create/Add"),
    @XmlEnumValue("Read/Query")
    READ_QUERY("Read/Query"),

    /**
     * Add specified data if it does not exist or update data where it does exist.	
     * 
     */
    @XmlEnumValue("Update/Modify")
    UPDATE_MODIFY("Update/Modify"),

    /**
     * Cancel an existing item.
     * 
     */
    @XmlEnumValue("Cancel")
    CANCEL("Cancel"),

    /**
     * Remove specified data.
     * 
     */
    @XmlEnumValue("Delete")
    DELETE("Delete"),

    /**
     * Overlay existing data with specified data.
     * 
     */
    @XmlEnumValue("Replace")
    REPLACE("Replace"),
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListDataActionTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListDataActionTypeBase fromValue(String v) {
        for (ListDataActionTypeBase c: ListDataActionTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
