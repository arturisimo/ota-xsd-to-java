
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferBookingMethod.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferBookingMethod"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="CorporateTravelPlanner"/&amp;gt;
 *     &amp;lt;enumeration value="Online"/&amp;gt;
 *     &amp;lt;enumeration value="TravelAgency"/&amp;gt;
 *     &amp;lt;enumeration value="TravelAgent"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferBookingMethod")
@XmlEnum
public enum ListOfferBookingMethod {

    @XmlEnumValue("CorporateTravelPlanner")
    CORPORATE_TRAVEL_PLANNER("CorporateTravelPlanner"),
    @XmlEnumValue("Online")
    ONLINE("Online"),
    @XmlEnumValue("TravelAgency")
    TRAVEL_AGENCY("TravelAgency"),
    @XmlEnumValue("TravelAgent")
    TRAVEL_AGENT("TravelAgent"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferBookingMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferBookingMethod fromValue(String v) {
        for (ListOfferBookingMethod c: ListOfferBookingMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
