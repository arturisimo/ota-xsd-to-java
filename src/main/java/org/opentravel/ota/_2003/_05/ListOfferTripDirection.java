
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferTripDirection.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferTripDirection"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="OneWay"/&amp;gt;
 *     &amp;lt;enumeration value="Outbound"/&amp;gt;
 *     &amp;lt;enumeration value="Return"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferTripDirection")
@XmlEnum
public enum ListOfferTripDirection {

    @XmlEnumValue("OneWay")
    ONE_WAY("OneWay"),
    @XmlEnumValue("Outbound")
    OUTBOUND("Outbound"),
    @XmlEnumValue("Return")
    RETURN("Return");
    private final String value;

    ListOfferTripDirection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferTripDirection fromValue(String v) {
        for (ListOfferTripDirection c: ListOfferTripDirection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
