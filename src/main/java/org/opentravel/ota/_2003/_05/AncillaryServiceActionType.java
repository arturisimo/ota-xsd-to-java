
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Airline or ATPCO encoded service family with product group(s), sub-group(s) with required, include or exclude indicators.
 * 
 * &lt;p&gt;Clase Java para AncillaryServiceActionType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AncillaryServiceActionType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ProductGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="CodeSource"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *                                 &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="ResultsAction" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="CodeSource"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="ResultsAction" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="CodeSource"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="ServiceCode" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceFamilyEnum" /&amp;gt;
 *       &amp;lt;attribute name="ExtServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="ResultsAction" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AncillaryServiceActionType", propOrder = {
    "productGroup"
})
public class AncillaryServiceActionType {

    @XmlElement(name = "ProductGroup")
    protected List<AncillaryServiceActionType.ProductGroup> productGroup;
    @XmlAttribute(name = "CodeSource")
    protected String codeSource;
    @XmlAttribute(name = "ServiceCode")
    protected AncillaryServiceFamilyEnum serviceCode;
    @XmlAttribute(name = "ExtServiceCode")
    protected String extServiceCode;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "ResultsAction")
    protected IncludeExcludeType resultsAction;

    /**
     * Gets the value of the productGroup property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the productGroup property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getProductGroup().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AncillaryServiceActionType.ProductGroup }
     * 
     * 
     */
    public List<AncillaryServiceActionType.ProductGroup> getProductGroup() {
        if (productGroup == null) {
            productGroup = new ArrayList<AncillaryServiceActionType.ProductGroup>();
        }
        return this.productGroup;
    }

    /**
     * Obtiene el valor de la propiedad codeSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeSource() {
        return codeSource;
    }

    /**
     * Define el valor de la propiedad codeSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeSource(String value) {
        this.codeSource = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceCode.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServiceFamilyEnum }
     *     
     */
    public AncillaryServiceFamilyEnum getServiceCode() {
        return serviceCode;
    }

    /**
     * Define el valor de la propiedad serviceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServiceFamilyEnum }
     *     
     */
    public void setServiceCode(AncillaryServiceFamilyEnum value) {
        this.serviceCode = value;
    }

    /**
     * Obtiene el valor de la propiedad extServiceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtServiceCode() {
        return extServiceCode;
    }

    /**
     * Define el valor de la propiedad extServiceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtServiceCode(String value) {
        this.extServiceCode = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad resultsAction.
     * 
     * @return
     *     possible object is
     *     {@link IncludeExcludeType }
     *     
     */
    public IncludeExcludeType getResultsAction() {
        return resultsAction;
    }

    /**
     * Define el valor de la propiedad resultsAction.
     * 
     * @param value
     *     allowed object is
     *     {@link IncludeExcludeType }
     *     
     */
    public void setResultsAction(IncludeExcludeType value) {
        this.resultsAction = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="CodeSource"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
     *                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="ResultsAction" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="CodeSource"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
     *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ResultsAction" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subGroup"
    })
    public static class ProductGroup {

        @XmlElement(name = "SubGroup")
        protected List<AncillaryServiceActionType.ProductGroup.SubGroup> subGroup;
        @XmlAttribute(name = "CodeSource")
        protected String codeSource;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "Description")
        protected String description;
        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "BrandedFareName")
        protected String brandedFareName;
        @XmlAttribute(name = "ResultsAction")
        protected IncludeExcludeType resultsAction;

        /**
         * Gets the value of the subGroup property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the subGroup property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSubGroup().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AncillaryServiceActionType.ProductGroup.SubGroup }
         * 
         * 
         */
        public List<AncillaryServiceActionType.ProductGroup.SubGroup> getSubGroup() {
            if (subGroup == null) {
                subGroup = new ArrayList<AncillaryServiceActionType.ProductGroup.SubGroup>();
            }
            return this.subGroup;
        }

        /**
         * Obtiene el valor de la propiedad codeSource.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeSource() {
            return codeSource;
        }

        /**
         * Define el valor de la propiedad codeSource.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeSource(String value) {
            this.codeSource = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad brandedFareName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandedFareName() {
            return brandedFareName;
        }

        /**
         * Define el valor de la propiedad brandedFareName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandedFareName(String value) {
            this.brandedFareName = value;
        }

        /**
         * Obtiene el valor de la propiedad resultsAction.
         * 
         * @return
         *     possible object is
         *     {@link IncludeExcludeType }
         *     
         */
        public IncludeExcludeType getResultsAction() {
            return resultsAction;
        }

        /**
         * Define el valor de la propiedad resultsAction.
         * 
         * @param value
         *     allowed object is
         *     {@link IncludeExcludeType }
         *     
         */
        public void setResultsAction(IncludeExcludeType value) {
            this.resultsAction = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="CodeSource"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
         *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="ResultsAction" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SubGroup {

            @XmlAttribute(name = "CodeSource")
            protected String codeSource;
            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "AdditionalCode")
            protected String additionalCode;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "RPH")
            protected String rph;
            @XmlAttribute(name = "ResultsAction")
            protected IncludeExcludeType resultsAction;

            /**
             * Obtiene el valor de la propiedad codeSource.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeSource() {
                return codeSource;
            }

            /**
             * Define el valor de la propiedad codeSource.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeSource(String value) {
                this.codeSource = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad additionalCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdditionalCode() {
                return additionalCode;
            }

            /**
             * Define el valor de la propiedad additionalCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdditionalCode(String value) {
                this.additionalCode = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }

            /**
             * Obtiene el valor de la propiedad resultsAction.
             * 
             * @return
             *     possible object is
             *     {@link IncludeExcludeType }
             *     
             */
            public IncludeExcludeType getResultsAction() {
                return resultsAction;
            }

            /**
             * Define el valor de la propiedad resultsAction.
             * 
             * @param value
             *     allowed object is
             *     {@link IncludeExcludeType }
             *     
             */
            public void setResultsAction(IncludeExcludeType value) {
                this.resultsAction = value;
            }

        }

    }

}
