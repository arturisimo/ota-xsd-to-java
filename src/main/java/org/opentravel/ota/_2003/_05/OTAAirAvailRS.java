
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="OriginDestinationInformation" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="OriginDestinationOptions"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;simpleContent&amp;gt;
 *                                                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
 *                                                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/simpleContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
 *                                                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
 *                                                 &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                                                 &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
 *                                                 &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                 &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                                 &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                                 &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                 &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                                                 &amp;lt;attribute name="OptionalServicesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;/extension&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "originDestinationInformation",
    "comment",
    "errors"
})
@XmlRootElement(name = "OTA_AirAvailRS")
public class OTAAirAvailRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "OriginDestinationInformation")
    protected List<OTAAirAvailRS.OriginDestinationInformation> originDestinationInformation;
    @XmlElement(name = "Comment")
    protected FreeTextType comment;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOriginDestinationInformation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirAvailRS.OriginDestinationInformation }
     * 
     * 
     */
    public List<OTAAirAvailRS.OriginDestinationInformation> getOriginDestinationInformation() {
        if (originDestinationInformation == null) {
            originDestinationInformation = new ArrayList<OTAAirAvailRS.OriginDestinationInformation>();
        }
        return this.originDestinationInformation;
    }

    /**
     * Obtiene el valor de la propiedad comment.
     * 
     * @return
     *     possible object is
     *     {@link FreeTextType }
     *     
     */
    public FreeTextType getComment() {
        return comment;
    }

    /**
     * Define el valor de la propiedad comment.
     * 
     * @param value
     *     allowed object is
     *     {@link FreeTextType }
     *     
     */
    public void setComment(FreeTextType value) {
        this.comment = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginDestinationOptions"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
     *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                                     &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                                     &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
     *                                     &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                     &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                                     &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                                     &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                     &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *                                     &amp;lt;attribute name="OptionalServicesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestinationOptions"
    })
    public static class OriginDestinationInformation
        extends OriginDestinationInformationType
    {

        @XmlElement(name = "OriginDestinationOptions", required = true)
        protected OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions;
        @XmlAttribute(name = "SameAirportInd")
        protected Boolean sameAirportInd;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Obtiene el valor de la propiedad originDestinationOptions.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions }
         *     
         */
        public OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions getOriginDestinationOptions() {
            return originDestinationOptions;
        }

        /**
         * Define el valor de la propiedad originDestinationOptions.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions }
         *     
         */
        public void setOriginDestinationOptions(OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions value) {
            this.originDestinationOptions = value;
        }

        /**
         * Obtiene el valor de la propiedad sameAirportInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSameAirportInd() {
            return sameAirportInd;
        }

        /**
         * Define el valor de la propiedad sameAirportInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSameAirportInd(Boolean value) {
            this.sameAirportInd = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
         *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
         *                           &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                           &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *                           &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
         *                           &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                           &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                           &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                           &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                           &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
         *                           &amp;lt;attribute name="OptionalServicesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "originDestinationOption"
        })
        public static class OriginDestinationOptions {

            @XmlElement(name = "OriginDestinationOption", required = true)
            protected List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption> originDestinationOption;

            /**
             * Gets the value of the originDestinationOption property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationOption property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getOriginDestinationOption().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption }
             * 
             * 
             */
            public List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption> getOriginDestinationOption() {
                if (originDestinationOption == null) {
                    originDestinationOption = new ArrayList<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption>();
                }
                return this.originDestinationOption;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
             *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
             *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
             *                 &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
             *                 &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
             *                 &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                 &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *                 &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *                 &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                 &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
             *                 &amp;lt;attribute name="OptionalServicesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "flightSegment"
            })
            public static class OriginDestinationOption {

                @XmlElement(name = "FlightSegment", required = true)
                protected List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment> flightSegment;

                /**
                 * Gets the value of the flightSegment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightSegment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getFlightSegment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment }
                 * 
                 * 
                 */
                public List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment> getFlightSegment() {
                    if (flightSegment == null) {
                        flightSegment = new ArrayList<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment>();
                    }
                    return this.flightSegment;
                }


                /**
                 * Flight segment information returned for an availability request including ancillary information. 
                 * 
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
                 *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
                 *       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                 *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
                 *       &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
                 *       &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *       &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                 *       &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                 *       &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *       &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
                 *       &amp;lt;attribute name="OptionalServicesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "trafficRestrictionInfo",
                    "comment",
                    "marketingCabin",
                    "bookingClassAvail",
                    "stopLocation"
                })
                public static class FlightSegment
                    extends FlightSegmentType
                {

                    @XmlElement(name = "TrafficRestrictionInfo")
                    protected List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo> trafficRestrictionInfo;
                    @XmlElement(name = "Comment")
                    protected List<FreeTextType> comment;
                    @XmlElement(name = "MarketingCabin")
                    protected List<MarketingCabinType> marketingCabin;
                    @XmlElement(name = "BookingClassAvail")
                    protected List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail> bookingClassAvail;
                    @XmlElement(name = "StopLocation")
                    protected List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation> stopLocation;
                    @XmlAttribute(name = "JourneyDuration")
                    protected Duration journeyDuration;
                    @XmlAttribute(name = "OnTimeRate")
                    protected BigDecimal onTimeRate;
                    @XmlAttribute(name = "Ticket")
                    protected TicketType ticket;
                    @XmlAttribute(name = "ParticipationLevelCode")
                    protected String participationLevelCode;
                    @XmlAttribute(name = "GroundDuration")
                    protected Duration groundDuration;
                    @XmlAttribute(name = "AccumulatedDuration")
                    protected Duration accumulatedDuration;
                    @XmlAttribute(name = "Distance")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger distance;
                    @XmlAttribute(name = "CodeshareInd")
                    protected Boolean codeshareInd;
                    @XmlAttribute(name = "FlifoInd")
                    protected Boolean flifoInd;
                    @XmlAttribute(name = "DateChangeNbr")
                    protected String dateChangeNbr;
                    @XmlAttribute(name = "SequenceNumber")
                    protected Integer sequenceNumber;
                    @XmlAttribute(name = "OptionalServicesInd")
                    protected Boolean optionalServicesInd;
                    @XmlAttribute(name = "SmokingAllowed")
                    protected Boolean smokingAllowed;

                    /**
                     * Gets the value of the trafficRestrictionInfo property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the trafficRestrictionInfo property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getTrafficRestrictionInfo().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo }
                     * 
                     * 
                     */
                    public List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo> getTrafficRestrictionInfo() {
                        if (trafficRestrictionInfo == null) {
                            trafficRestrictionInfo = new ArrayList<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo>();
                        }
                        return this.trafficRestrictionInfo;
                    }

                    /**
                     * Gets the value of the comment property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getComment().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link FreeTextType }
                     * 
                     * 
                     */
                    public List<FreeTextType> getComment() {
                        if (comment == null) {
                            comment = new ArrayList<FreeTextType>();
                        }
                        return this.comment;
                    }

                    /**
                     * Gets the value of the marketingCabin property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingCabin property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getMarketingCabin().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link MarketingCabinType }
                     * 
                     * 
                     */
                    public List<MarketingCabinType> getMarketingCabin() {
                        if (marketingCabin == null) {
                            marketingCabin = new ArrayList<MarketingCabinType>();
                        }
                        return this.marketingCabin;
                    }

                    /**
                     * Gets the value of the bookingClassAvail property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingClassAvail property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getBookingClassAvail().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail }
                     * 
                     * 
                     */
                    public List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail> getBookingClassAvail() {
                        if (bookingClassAvail == null) {
                            bookingClassAvail = new ArrayList<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail>();
                        }
                        return this.bookingClassAvail;
                    }

                    /**
                     * Gets the value of the stopLocation property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stopLocation property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getStopLocation().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation }
                     * 
                     * 
                     */
                    public List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation> getStopLocation() {
                        if (stopLocation == null) {
                            stopLocation = new ArrayList<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation>();
                        }
                        return this.stopLocation;
                    }

                    /**
                     * Obtiene el valor de la propiedad journeyDuration.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Duration }
                     *     
                     */
                    public Duration getJourneyDuration() {
                        return journeyDuration;
                    }

                    /**
                     * Define el valor de la propiedad journeyDuration.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Duration }
                     *     
                     */
                    public void setJourneyDuration(Duration value) {
                        this.journeyDuration = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad onTimeRate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getOnTimeRate() {
                        return onTimeRate;
                    }

                    /**
                     * Define el valor de la propiedad onTimeRate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setOnTimeRate(BigDecimal value) {
                        this.onTimeRate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad ticket.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TicketType }
                     *     
                     */
                    public TicketType getTicket() {
                        return ticket;
                    }

                    /**
                     * Define el valor de la propiedad ticket.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TicketType }
                     *     
                     */
                    public void setTicket(TicketType value) {
                        this.ticket = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participationLevelCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipationLevelCode() {
                        return participationLevelCode;
                    }

                    /**
                     * Define el valor de la propiedad participationLevelCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipationLevelCode(String value) {
                        this.participationLevelCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groundDuration.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Duration }
                     *     
                     */
                    public Duration getGroundDuration() {
                        return groundDuration;
                    }

                    /**
                     * Define el valor de la propiedad groundDuration.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Duration }
                     *     
                     */
                    public void setGroundDuration(Duration value) {
                        this.groundDuration = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad accumulatedDuration.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Duration }
                     *     
                     */
                    public Duration getAccumulatedDuration() {
                        return accumulatedDuration;
                    }

                    /**
                     * Define el valor de la propiedad accumulatedDuration.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Duration }
                     *     
                     */
                    public void setAccumulatedDuration(Duration value) {
                        this.accumulatedDuration = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad distance.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getDistance() {
                        return distance;
                    }

                    /**
                     * Define el valor de la propiedad distance.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setDistance(BigInteger value) {
                        this.distance = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad codeshareInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isCodeshareInd() {
                        return codeshareInd;
                    }

                    /**
                     * Define el valor de la propiedad codeshareInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setCodeshareInd(Boolean value) {
                        this.codeshareInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad flifoInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isFlifoInd() {
                        return flifoInd;
                    }

                    /**
                     * Define el valor de la propiedad flifoInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setFlifoInd(Boolean value) {
                        this.flifoInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dateChangeNbr.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDateChangeNbr() {
                        return dateChangeNbr;
                    }

                    /**
                     * Define el valor de la propiedad dateChangeNbr.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDateChangeNbr(String value) {
                        this.dateChangeNbr = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad sequenceNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getSequenceNumber() {
                        return sequenceNumber;
                    }

                    /**
                     * Define el valor de la propiedad sequenceNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setSequenceNumber(Integer value) {
                        this.sequenceNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad optionalServicesInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isOptionalServicesInd() {
                        return optionalServicesInd;
                    }

                    /**
                     * Define el valor de la propiedad optionalServicesInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setOptionalServicesInd(Boolean value) {
                        this.optionalServicesInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad smokingAllowed.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isSmokingAllowed() {
                        return smokingAllowed;
                    }

                    /**
                     * Define el valor de la propiedad smokingAllowed.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setSmokingAllowed(Boolean value) {
                        this.smokingAllowed = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
                     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "meal"
                    })
                    public static class BookingClassAvail {

                        @XmlElement(name = "Meal")
                        protected List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail.Meal> meal;
                        @XmlAttribute(name = "RPH")
                        protected String rph;
                        @XmlAttribute(name = "ResBookDesigCode")
                        protected String resBookDesigCode;
                        @XmlAttribute(name = "ResBookDesigQuantity")
                        protected String resBookDesigQuantity;
                        @XmlAttribute(name = "ResBookDesigStatusCode")
                        protected String resBookDesigStatusCode;

                        /**
                         * Gets the value of the meal property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the meal property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getMeal().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail.Meal }
                         * 
                         * 
                         */
                        public List<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail.Meal> getMeal() {
                            if (meal == null) {
                                meal = new ArrayList<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail.Meal>();
                            }
                            return this.meal;
                        }

                        /**
                         * Obtiene el valor de la propiedad rph.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRPH() {
                            return rph;
                        }

                        /**
                         * Define el valor de la propiedad rph.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRPH(String value) {
                            this.rph = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad resBookDesigCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getResBookDesigCode() {
                            return resBookDesigCode;
                        }

                        /**
                         * Define el valor de la propiedad resBookDesigCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setResBookDesigCode(String value) {
                            this.resBookDesigCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad resBookDesigQuantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getResBookDesigQuantity() {
                            return resBookDesigQuantity;
                        }

                        /**
                         * Define el valor de la propiedad resBookDesigQuantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setResBookDesigQuantity(String value) {
                            this.resBookDesigQuantity = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad resBookDesigStatusCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getResBookDesigStatusCode() {
                            return resBookDesigStatusCode;
                        }

                        /**
                         * Define el valor de la propiedad resBookDesigStatusCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setResBookDesigStatusCode(String value) {
                            this.resBookDesigStatusCode = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attribute name="MealService" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class Meal {

                            @XmlAttribute(name = "MealService", required = true)
                            protected String mealService;

                            /**
                             * Obtiene el valor de la propiedad mealService.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getMealService() {
                                return mealService;
                            }

                            /**
                             * Define el valor de la propiedad mealService.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setMealService(String value) {
                                this.mealService = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class StopLocation {

                        @XmlAttribute(name = "LocationCode")
                        protected String locationCode;
                        @XmlAttribute(name = "CodeContext")
                        protected String codeContext;

                        /**
                         * Obtiene el valor de la propiedad locationCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLocationCode() {
                            return locationCode;
                        }

                        /**
                         * Define el valor de la propiedad locationCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLocationCode(String value) {
                            this.locationCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad codeContext.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCodeContext() {
                            return codeContext;
                        }

                        /**
                         * Define el valor de la propiedad codeContext.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCodeContext(String value) {
                            this.codeContext = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
                     *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class TrafficRestrictionInfo
                        extends FreeTextType
                    {

                        @XmlAttribute(name = "Code")
                        protected String code;

                        /**
                         * Obtiene el valor de la propiedad code.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCode() {
                            return code;
                        }

                        /**
                         * Define el valor de la propiedad code.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCode(String value) {
                            this.code = value;
                        }

                    }

                }

            }

        }

    }

}
