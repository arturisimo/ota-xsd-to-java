
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="ProcessingInformation" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                   &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                   &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                   &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="TourActivityInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="BasicInfo"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Schedule"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                       &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Detail" maxOccurs="365" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType"&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="AvailableQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Accessibility" type="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="CategoryAndType" type="{http://www.opentravel.org/OTA/2003/05}TourActivityCategoryType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="CommissionInfo" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}TourActivityDescriptionType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="DiscountPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                                       &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Program" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="DiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                       &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                             &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Location" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="Location" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="Type"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="PickUp"/&amp;gt;
 *                                             &amp;lt;enumeration value="DropOff"/&amp;gt;
 *                                             &amp;lt;enumeration value="Both"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Summary" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;simpleContent&amp;gt;
 *                                                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/simpleContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Rule" type="{http://www.opentravel.org/OTA/2003/05}TourActivityExtraRule" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="PartnerCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Insurance" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;simpleContent&amp;gt;
 *                                               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/simpleContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="OwnInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="SupplierInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="LanguageSpoken" maxOccurs="9" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;simpleContent&amp;gt;
 *                           &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                             &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/simpleContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;simpleContent&amp;gt;
 *                                               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/simpleContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;simpleContent&amp;gt;
 *                                               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/simpleContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="PickupDropoff" type="{http://www.opentravel.org/OTA/2003/05}TourActivityTransType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="Policies" type="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="SupplierOperator" maxOccurs="2" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType"&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="AlternateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "processingInformation",
    "tourActivityInfo",
    "tpaExtensions",
    "errors"
})
@XmlRootElement(name = "OTA_TourActivityAvailRS")
public class OTATourActivityAvailRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "ProcessingInformation")
    protected OTATourActivityAvailRS.ProcessingInformation processingInformation;
    @XmlElement(name = "TourActivityInfo")
    protected List<OTATourActivityAvailRS.TourActivityInfo> tourActivityInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInformation.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityAvailRS.ProcessingInformation }
     *     
     */
    public OTATourActivityAvailRS.ProcessingInformation getProcessingInformation() {
        return processingInformation;
    }

    /**
     * Define el valor de la propiedad processingInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityAvailRS.ProcessingInformation }
     *     
     */
    public void setProcessingInformation(OTATourActivityAvailRS.ProcessingInformation value) {
        this.processingInformation = value;
    }

    /**
     * Gets the value of the tourActivityInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the tourActivityInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTourActivityInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTATourActivityAvailRS.TourActivityInfo }
     * 
     * 
     */
    public List<OTATourActivityAvailRS.TourActivityInfo> getTourActivityInfo() {
        if (tourActivityInfo == null) {
            tourActivityInfo = new ArrayList<OTATourActivityAvailRS.TourActivityInfo>();
        }
        return this.tourActivityInfo;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInformation {

        @XmlAttribute(name = "LanguageCode")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String languageCode;
        @XmlAttribute(name = "MoreDataEchoToken")
        protected String moreDataEchoToken;
        @XmlAttribute(name = "PricingCurrency")
        protected String pricingCurrency;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;

        /**
         * Obtiene el valor de la propiedad languageCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageCode() {
            return languageCode;
        }

        /**
         * Define el valor de la propiedad languageCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageCode(String value) {
            this.languageCode = value;
        }

        /**
         * Obtiene el valor de la propiedad moreDataEchoToken.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoreDataEchoToken() {
            return moreDataEchoToken;
        }

        /**
         * Define el valor de la propiedad moreDataEchoToken.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoreDataEchoToken(String value) {
            this.moreDataEchoToken = value;
        }

        /**
         * Obtiene el valor de la propiedad pricingCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricingCurrency() {
            return pricingCurrency;
        }

        /**
         * Define el valor de la propiedad pricingCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricingCurrency(String value) {
            this.pricingCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BasicInfo"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Schedule"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Detail" maxOccurs="365" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="AvailableQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Accessibility" type="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CategoryAndType" type="{http://www.opentravel.org/OTA/2003/05}TourActivityCategoryType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CommissionInfo" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}TourActivityDescriptionType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="DiscountPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                           &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Program" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="DiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                           &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Location" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Location" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="Type"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="PickUp"/&amp;gt;
     *                                 &amp;lt;enumeration value="DropOff"/&amp;gt;
     *                                 &amp;lt;enumeration value="Both"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Summary" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Rule" type="{http://www.opentravel.org/OTA/2003/05}TourActivityExtraRule" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="PartnerCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Insurance" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                                     &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="OwnInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="SupplierInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="LanguageSpoken" maxOccurs="9" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                                     &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PickupDropoff" type="{http://www.opentravel.org/OTA/2003/05}TourActivityTransType" maxOccurs="2" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Policies" type="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="SupplierOperator" maxOccurs="2" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="AlternateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "basicInfo",
        "schedule",
        "accessibility",
        "categoryAndType",
        "commissionInfo",
        "description",
        "discount",
        "extra",
        "insurance",
        "languageSpoken",
        "location",
        "pricing",
        "pickupDropoff",
        "policies",
        "supplierOperator"
    })
    public static class TourActivityInfo {

        @XmlElement(name = "BasicInfo", required = true)
        protected OTATourActivityAvailRS.TourActivityInfo.BasicInfo basicInfo;
        @XmlElement(name = "Schedule", required = true)
        protected OTATourActivityAvailRS.TourActivityInfo.Schedule schedule;
        @XmlElement(name = "Accessibility")
        protected TourActivityAccessibilityType accessibility;
        @XmlElement(name = "CategoryAndType")
        protected TourActivityCategoryType categoryAndType;
        @XmlElement(name = "CommissionInfo")
        protected List<CommissionType> commissionInfo;
        @XmlElement(name = "Description")
        protected TourActivityDescriptionType description;
        @XmlElement(name = "Discount")
        protected List<OTATourActivityAvailRS.TourActivityInfo.Discount> discount;
        @XmlElement(name = "Extra")
        protected List<OTATourActivityAvailRS.TourActivityInfo.Extra> extra;
        @XmlElement(name = "Insurance")
        protected List<OTATourActivityAvailRS.TourActivityInfo.Insurance> insurance;
        @XmlElement(name = "LanguageSpoken")
        protected List<OTATourActivityAvailRS.TourActivityInfo.LanguageSpoken> languageSpoken;
        @XmlElement(name = "Location")
        protected TourActivityLocationType location;
        @XmlElement(name = "Pricing")
        protected OTATourActivityAvailRS.TourActivityInfo.Pricing pricing;
        @XmlElement(name = "PickupDropoff")
        protected List<TourActivityTransType> pickupDropoff;
        @XmlElement(name = "Policies")
        protected TourActivityPolicyType policies;
        @XmlElement(name = "SupplierOperator")
        protected List<OTATourActivityAvailRS.TourActivityInfo.SupplierOperator> supplierOperator;
        @XmlAttribute(name = "AlternateInd")
        protected Boolean alternateInd;

        /**
         * Obtiene el valor de la propiedad basicInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityAvailRS.TourActivityInfo.BasicInfo }
         *     
         */
        public OTATourActivityAvailRS.TourActivityInfo.BasicInfo getBasicInfo() {
            return basicInfo;
        }

        /**
         * Define el valor de la propiedad basicInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityAvailRS.TourActivityInfo.BasicInfo }
         *     
         */
        public void setBasicInfo(OTATourActivityAvailRS.TourActivityInfo.BasicInfo value) {
            this.basicInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad schedule.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityAvailRS.TourActivityInfo.Schedule }
         *     
         */
        public OTATourActivityAvailRS.TourActivityInfo.Schedule getSchedule() {
            return schedule;
        }

        /**
         * Define el valor de la propiedad schedule.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityAvailRS.TourActivityInfo.Schedule }
         *     
         */
        public void setSchedule(OTATourActivityAvailRS.TourActivityInfo.Schedule value) {
            this.schedule = value;
        }

        /**
         * Obtiene el valor de la propiedad accessibility.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityAccessibilityType }
         *     
         */
        public TourActivityAccessibilityType getAccessibility() {
            return accessibility;
        }

        /**
         * Define el valor de la propiedad accessibility.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityAccessibilityType }
         *     
         */
        public void setAccessibility(TourActivityAccessibilityType value) {
            this.accessibility = value;
        }

        /**
         * Obtiene el valor de la propiedad categoryAndType.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityCategoryType }
         *     
         */
        public TourActivityCategoryType getCategoryAndType() {
            return categoryAndType;
        }

        /**
         * Define el valor de la propiedad categoryAndType.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityCategoryType }
         *     
         */
        public void setCategoryAndType(TourActivityCategoryType value) {
            this.categoryAndType = value;
        }

        /**
         * Gets the value of the commissionInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the commissionInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCommissionInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CommissionType }
         * 
         * 
         */
        public List<CommissionType> getCommissionInfo() {
            if (commissionInfo == null) {
                commissionInfo = new ArrayList<CommissionType>();
            }
            return this.commissionInfo;
        }

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityDescriptionType }
         *     
         */
        public TourActivityDescriptionType getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityDescriptionType }
         *     
         */
        public void setDescription(TourActivityDescriptionType value) {
            this.description = value;
        }

        /**
         * Gets the value of the discount property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discount property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDiscount().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRS.TourActivityInfo.Discount }
         * 
         * 
         */
        public List<OTATourActivityAvailRS.TourActivityInfo.Discount> getDiscount() {
            if (discount == null) {
                discount = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Discount>();
            }
            return this.discount;
        }

        /**
         * Gets the value of the extra property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the extra property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getExtra().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRS.TourActivityInfo.Extra }
         * 
         * 
         */
        public List<OTATourActivityAvailRS.TourActivityInfo.Extra> getExtra() {
            if (extra == null) {
                extra = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Extra>();
            }
            return this.extra;
        }

        /**
         * Gets the value of the insurance property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the insurance property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getInsurance().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRS.TourActivityInfo.Insurance }
         * 
         * 
         */
        public List<OTATourActivityAvailRS.TourActivityInfo.Insurance> getInsurance() {
            if (insurance == null) {
                insurance = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Insurance>();
            }
            return this.insurance;
        }

        /**
         * Gets the value of the languageSpoken property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the languageSpoken property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLanguageSpoken().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRS.TourActivityInfo.LanguageSpoken }
         * 
         * 
         */
        public List<OTATourActivityAvailRS.TourActivityInfo.LanguageSpoken> getLanguageSpoken() {
            if (languageSpoken == null) {
                languageSpoken = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.LanguageSpoken>();
            }
            return this.languageSpoken;
        }

        /**
         * Obtiene el valor de la propiedad location.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityLocationType }
         *     
         */
        public TourActivityLocationType getLocation() {
            return location;
        }

        /**
         * Define el valor de la propiedad location.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityLocationType }
         *     
         */
        public void setLocation(TourActivityLocationType value) {
            this.location = value;
        }

        /**
         * Obtiene el valor de la propiedad pricing.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing }
         *     
         */
        public OTATourActivityAvailRS.TourActivityInfo.Pricing getPricing() {
            return pricing;
        }

        /**
         * Define el valor de la propiedad pricing.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing }
         *     
         */
        public void setPricing(OTATourActivityAvailRS.TourActivityInfo.Pricing value) {
            this.pricing = value;
        }

        /**
         * Gets the value of the pickupDropoff property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pickupDropoff property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPickupDropoff().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityTransType }
         * 
         * 
         */
        public List<TourActivityTransType> getPickupDropoff() {
            if (pickupDropoff == null) {
                pickupDropoff = new ArrayList<TourActivityTransType>();
            }
            return this.pickupDropoff;
        }

        /**
         * Obtiene el valor de la propiedad policies.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityPolicyType }
         *     
         */
        public TourActivityPolicyType getPolicies() {
            return policies;
        }

        /**
         * Define el valor de la propiedad policies.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityPolicyType }
         *     
         */
        public void setPolicies(TourActivityPolicyType value) {
            this.policies = value;
        }

        /**
         * Gets the value of the supplierOperator property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplierOperator property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSupplierOperator().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRS.TourActivityInfo.SupplierOperator }
         * 
         * 
         */
        public List<OTATourActivityAvailRS.TourActivityInfo.SupplierOperator> getSupplierOperator() {
            if (supplierOperator == null) {
                supplierOperator = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.SupplierOperator>();
            }
            return this.supplierOperator;
        }

        /**
         * Obtiene el valor de la propiedad alternateInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAlternateInd() {
            return alternateInd;
        }

        /**
         * Define el valor de la propiedad alternateInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAlternateInd(Boolean value) {
            this.alternateInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class BasicInfo
            extends TourActivityIDType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="DiscountPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *                 &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Program" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="DiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                 &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotion",
            "program"
        })
        public static class Discount {

            @XmlElement(name = "Promotion")
            protected List<OTATourActivityAvailRS.TourActivityInfo.Discount.Promotion> promotion;
            @XmlElement(name = "Program")
            protected OTATourActivityAvailRS.TourActivityInfo.Discount.Program program;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Gets the value of the promotion property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotion property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotion().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityAvailRS.TourActivityInfo.Discount.Promotion }
             * 
             * 
             */
            public List<OTATourActivityAvailRS.TourActivityInfo.Discount.Promotion> getPromotion() {
                if (promotion == null) {
                    promotion = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Discount.Promotion>();
                }
                return this.promotion;
            }

            /**
             * Obtiene el valor de la propiedad program.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Discount.Program }
             *     
             */
            public OTATourActivityAvailRS.TourActivityInfo.Discount.Program getProgram() {
                return program;
            }

            /**
             * Define el valor de la propiedad program.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Discount.Program }
             *     
             */
            public void setProgram(OTATourActivityAvailRS.TourActivityInfo.Discount.Program value) {
                this.program = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="DiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *       &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "provider"
            })
            public static class Program {

                @XmlElement(name = "Provider")
                protected CompanyNameType provider;
                @XmlAttribute(name = "Name")
                protected String name;
                @XmlAttribute(name = "Description")
                protected String description;
                @XmlAttribute(name = "DiscountPercentage")
                protected BigDecimal discountPercentage;
                @XmlAttribute(name = "DiscountAmount")
                protected BigDecimal discountAmount;

                /**
                 * Obtiene el valor de la propiedad provider.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public CompanyNameType getProvider() {
                    return provider;
                }

                /**
                 * Define el valor de la propiedad provider.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public void setProvider(CompanyNameType value) {
                    this.provider = value;
                }

                /**
                 * Obtiene el valor de la propiedad name.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Define el valor de la propiedad name.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

                /**
                 * Obtiene el valor de la propiedad discountPercentage.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDiscountPercentage() {
                    return discountPercentage;
                }

                /**
                 * Define el valor de la propiedad discountPercentage.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDiscountPercentage(BigDecimal value) {
                    this.discountPercentage = value;
                }

                /**
                 * Obtiene el valor de la propiedad discountAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDiscountAmount() {
                    return discountAmount;
                }

                /**
                 * Define el valor de la propiedad discountAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDiscountAmount(BigDecimal value) {
                    this.discountAmount = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="DiscountPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
             *       &amp;lt;attribute name="DiscountAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Promotion {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "StartDate")
                protected String startDate;
                @XmlAttribute(name = "EndDate")
                protected String endDate;
                @XmlAttribute(name = "Disclaimer")
                protected String disclaimer;
                @XmlAttribute(name = "DiscountPercentage")
                protected BigDecimal discountPercentage;
                @XmlAttribute(name = "DiscountAmount")
                protected BigDecimal discountAmount;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad startDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStartDate() {
                    return startDate;
                }

                /**
                 * Define el valor de la propiedad startDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStartDate(String value) {
                    this.startDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad endDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndDate() {
                    return endDate;
                }

                /**
                 * Define el valor de la propiedad endDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndDate(String value) {
                    this.endDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad disclaimer.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDisclaimer() {
                    return disclaimer;
                }

                /**
                 * Define el valor de la propiedad disclaimer.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDisclaimer(String value) {
                    this.disclaimer = value;
                }

                /**
                 * Obtiene el valor de la propiedad discountPercentage.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDiscountPercentage() {
                    return discountPercentage;
                }

                /**
                 * Define el valor de la propiedad discountPercentage.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDiscountPercentage(BigDecimal value) {
                    this.discountPercentage = value;
                }

                /**
                 * Obtiene el valor de la propiedad discountAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDiscountAmount() {
                    return discountAmount;
                }

                /**
                 * Define el valor de la propiedad discountAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDiscountAmount(BigDecimal value) {
                    this.discountAmount = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Location" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Location" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="Type"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="PickUp"/&amp;gt;
         *                       &amp;lt;enumeration value="DropOff"/&amp;gt;
         *                       &amp;lt;enumeration value="Both"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Summary" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Rule" type="{http://www.opentravel.org/OTA/2003/05}TourActivityExtraRule" maxOccurs="99" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="PartnerCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "deposit",
            "location",
            "pricing",
            "rule"
        })
        public static class Extra {

            @XmlElement(name = "Deposit")
            protected OTATourActivityAvailRS.TourActivityInfo.Extra.Deposit deposit;
            @XmlElement(name = "Location")
            protected List<OTATourActivityAvailRS.TourActivityInfo.Extra.Location> location;
            @XmlElement(name = "Pricing")
            protected OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing pricing;
            @XmlElement(name = "Rule")
            protected List<TourActivityExtraRule> rule;
            @XmlAttribute(name = "SupplierCode")
            protected String supplierCode;
            @XmlAttribute(name = "PartnerCode")
            protected String partnerCode;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "AdditionalInfo")
            protected String additionalInfo;
            @XmlAttribute(name = "Quantity")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger quantity;
            @XmlAttribute(name = "RequiredInd")
            protected Boolean requiredInd;
            @XmlAttribute(name = "ReserveInd")
            protected Boolean reserveInd;

            /**
             * Obtiene el valor de la propiedad deposit.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Deposit }
             *     
             */
            public OTATourActivityAvailRS.TourActivityInfo.Extra.Deposit getDeposit() {
                return deposit;
            }

            /**
             * Define el valor de la propiedad deposit.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Deposit }
             *     
             */
            public void setDeposit(OTATourActivityAvailRS.TourActivityInfo.Extra.Deposit value) {
                this.deposit = value;
            }

            /**
             * Gets the value of the location property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the location property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getLocation().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Location }
             * 
             * 
             */
            public List<OTATourActivityAvailRS.TourActivityInfo.Extra.Location> getLocation() {
                if (location == null) {
                    location = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Extra.Location>();
                }
                return this.location;
            }

            /**
             * Obtiene el valor de la propiedad pricing.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing }
             *     
             */
            public OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing getPricing() {
                return pricing;
            }

            /**
             * Define el valor de la propiedad pricing.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing }
             *     
             */
            public void setPricing(OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing value) {
                this.pricing = value;
            }

            /**
             * Gets the value of the rule property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rule property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRule().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link TourActivityExtraRule }
             * 
             * 
             */
            public List<TourActivityExtraRule> getRule() {
                if (rule == null) {
                    rule = new ArrayList<TourActivityExtraRule>();
                }
                return this.rule;
            }

            /**
             * Obtiene el valor de la propiedad supplierCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSupplierCode() {
                return supplierCode;
            }

            /**
             * Define el valor de la propiedad supplierCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSupplierCode(String value) {
                this.supplierCode = value;
            }

            /**
             * Obtiene el valor de la propiedad partnerCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPartnerCode() {
                return partnerCode;
            }

            /**
             * Define el valor de la propiedad partnerCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPartnerCode(String value) {
                this.partnerCode = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad additionalInfo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdditionalInfo() {
                return additionalInfo;
            }

            /**
             * Define el valor de la propiedad additionalInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdditionalInfo(String value) {
                this.additionalInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setQuantity(BigInteger value) {
                this.quantity = value;
            }

            /**
             * Obtiene el valor de la propiedad requiredInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRequiredInd() {
                return requiredInd;
            }

            /**
             * Define el valor de la propiedad requiredInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRequiredInd(Boolean value) {
                this.requiredInd = value;
            }

            /**
             * Obtiene el valor de la propiedad reserveInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReserveInd() {
                return reserveInd;
            }

            /**
             * Define el valor de la propiedad reserveInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReserveInd(Boolean value) {
                this.reserveInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount"
            })
            public static class Deposit
                extends AcceptedPaymentsType
            {

                @XmlElement(name = "Amount")
                protected TourActivityChargeType amount;

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setAmount(TourActivityChargeType value) {
                    this.amount = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Location" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="Type"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="PickUp"/&amp;gt;
             *             &amp;lt;enumeration value="DropOff"/&amp;gt;
             *             &amp;lt;enumeration value="Both"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Location {

                @XmlAttribute(name = "Location")
                protected String location;
                @XmlAttribute(name = "Type")
                protected String type;

                /**
                 * Obtiene el valor de la propiedad location.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocation() {
                    return location;
                }

                /**
                 * Define el valor de la propiedad location.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocation(String value) {
                    this.location = value;
                }

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Summary" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "summary",
                "detail"
            })
            public static class Pricing {

                @XmlElement(name = "Summary")
                protected TourActivityChargeType summary;
                @XmlElement(name = "Detail")
                protected List<OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail> detail;

                /**
                 * Obtiene el valor de la propiedad summary.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getSummary() {
                    return summary;
                }

                /**
                 * Define el valor de la propiedad summary.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setSummary(TourActivityChargeType value) {
                    this.summary = value;
                }

                /**
                 * Gets the value of the detail property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detail property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getDetail().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail }
                 * 
                 * 
                 */
                public List<OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail> getDetail() {
                    if (detail == null) {
                        detail = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail>();
                    }
                    return this.detail;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "qualifierInfo",
                    "price",
                    "tpaExtensions"
                })
                public static class Detail {

                    @XmlElement(name = "QualifierInfo")
                    protected OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail.QualifierInfo qualifierInfo;
                    @XmlElement(name = "Price")
                    protected TourActivityChargeType price;
                    @XmlElement(name = "TPA_Extensions")
                    protected TPAExtensionsType tpaExtensions;
                    @XmlAttribute(name = "Age")
                    protected Integer age;

                    /**
                     * Obtiene el valor de la propiedad qualifierInfo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail.QualifierInfo }
                     *     
                     */
                    public OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail.QualifierInfo getQualifierInfo() {
                        return qualifierInfo;
                    }

                    /**
                     * Define el valor de la propiedad qualifierInfo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail.QualifierInfo }
                     *     
                     */
                    public void setQualifierInfo(OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail.QualifierInfo value) {
                        this.qualifierInfo = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad price.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityChargeType }
                     *     
                     */
                    public TourActivityChargeType getPrice() {
                        return price;
                    }

                    /**
                     * Define el valor de la propiedad price.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityChargeType }
                     *     
                     */
                    public void setPrice(TourActivityChargeType value) {
                        this.price = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad tpaExtensions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public TPAExtensionsType getTPAExtensions() {
                        return tpaExtensions;
                    }

                    /**
                     * Define el valor de la propiedad tpaExtensions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public void setTPAExtensions(TPAExtensionsType value) {
                        this.tpaExtensions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad age.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getAge() {
                        return age;
                    }

                    /**
                     * Define el valor de la propiedad age.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setAge(Integer value) {
                        this.age = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class QualifierInfo
                        extends AgeQualifierType
                    {


                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="OwnInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="SupplierInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pricing",
            "providerName",
            "coverageLimit"
        })
        public static class Insurance {

            @XmlElement(name = "Pricing")
            protected OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing pricing;
            @XmlElement(name = "ProviderName")
            protected CompanyNameType providerName;
            @XmlElement(name = "CoverageLimit")
            protected CoverageLimitType coverageLimit;
            @XmlAttribute(name = "OwnInsuranceInd")
            protected Boolean ownInsuranceInd;
            @XmlAttribute(name = "SupplierInd")
            protected Boolean supplierInd;

            /**
             * Obtiene el valor de la propiedad pricing.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing }
             *     
             */
            public OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing getPricing() {
                return pricing;
            }

            /**
             * Define el valor de la propiedad pricing.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing }
             *     
             */
            public void setPricing(OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing value) {
                this.pricing = value;
            }

            /**
             * Obtiene el valor de la propiedad providerName.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getProviderName() {
                return providerName;
            }

            /**
             * Define el valor de la propiedad providerName.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setProviderName(CompanyNameType value) {
                this.providerName = value;
            }

            /**
             * Obtiene el valor de la propiedad coverageLimit.
             * 
             * @return
             *     possible object is
             *     {@link CoverageLimitType }
             *     
             */
            public CoverageLimitType getCoverageLimit() {
                return coverageLimit;
            }

            /**
             * Define el valor de la propiedad coverageLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link CoverageLimitType }
             *     
             */
            public void setCoverageLimit(CoverageLimitType value) {
                this.coverageLimit = value;
            }

            /**
             * Obtiene el valor de la propiedad ownInsuranceInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isOwnInsuranceInd() {
                return ownInsuranceInd;
            }

            /**
             * Define el valor de la propiedad ownInsuranceInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setOwnInsuranceInd(Boolean value) {
                this.ownInsuranceInd = value;
            }

            /**
             * Obtiene el valor de la propiedad supplierInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSupplierInd() {
                return supplierInd;
            }

            /**
             * Define el valor de la propiedad supplierInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSupplierInd(Boolean value) {
                this.supplierInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pricingType"
            })
            public static class Pricing
                extends TourActivityChargeType
            {

                @XmlElement(name = "PricingType")
                protected OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing.PricingType pricingType;

                /**
                 * Obtiene el valor de la propiedad pricingType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing.PricingType }
                 *     
                 */
                public OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing.PricingType getPricingType() {
                    return pricingType;
                }

                /**
                 * Define el valor de la propiedad pricingType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing.PricingType }
                 *     
                 */
                public void setPricingType(OTATourActivityAvailRS.TourActivityInfo.Insurance.Pricing.PricingType value) {
                    this.pricingType = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                 *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class PricingType {

                    @XmlValue
                    protected TourActivityPricingTypeEnum value;
                    @XmlAttribute(name = "Extension")
                    protected String extension;

                    /**
                     * Tour and activity pricing options.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public TourActivityPricingTypeEnum getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public void setValue(TourActivityPricingTypeEnum value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad extension.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExtension() {
                        return extension;
                    }

                    /**
                     * Define el valor de la propiedad extension.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExtension(String value) {
                        this.extension = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class LanguageSpoken {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "Code")
            protected String code;

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "summary",
            "participantCategory",
            "group"
        })
        public static class Pricing {

            @XmlElement(name = "Summary")
            protected OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary summary;
            @XmlElement(name = "ParticipantCategory")
            protected List<OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory> participantCategory;
            @XmlElement(name = "Group")
            protected List<OTATourActivityAvailRS.TourActivityInfo.Pricing.Group> group;

            /**
             * Obtiene el valor de la propiedad summary.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary }
             *     
             */
            public OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary getSummary() {
                return summary;
            }

            /**
             * Define el valor de la propiedad summary.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary }
             *     
             */
            public void setSummary(OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary value) {
                this.summary = value;
            }

            /**
             * Gets the value of the participantCategory property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantCategory property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getParticipantCategory().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory }
             * 
             * 
             */
            public List<OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory> getParticipantCategory() {
                if (participantCategory == null) {
                    participantCategory = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory>();
                }
                return this.participantCategory;
            }

            /**
             * Gets the value of the group property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the group property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getGroup().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.Group }
             * 
             * 
             */
            public List<OTATourActivityAvailRS.TourActivityInfo.Pricing.Group> getGroup() {
                if (group == null) {
                    group = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Pricing.Group>();
                }
                return this.group;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "price"
            })
            public static class Group {

                @XmlElement(name = "Price")
                protected TourActivityChargeType price;
                @XmlAttribute(name = "GroupCode")
                protected String groupCode;
                @XmlAttribute(name = "GroupName")
                protected String groupName;
                @XmlAttribute(name = "MinGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger minGroupSize;
                @XmlAttribute(name = "MaxGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maxGroupSize;
                @XmlAttribute(name = "KnownGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger knownGroupSize;

                /**
                 * Obtiene el valor de la propiedad price.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getPrice() {
                    return price;
                }

                /**
                 * Define el valor de la propiedad price.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setPrice(TourActivityChargeType value) {
                    this.price = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupCode() {
                    return groupCode;
                }

                /**
                 * Define el valor de la propiedad groupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupCode(String value) {
                    this.groupCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupName() {
                    return groupName;
                }

                /**
                 * Define el valor de la propiedad groupName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupName(String value) {
                    this.groupName = value;
                }

                /**
                 * Obtiene el valor de la propiedad minGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMinGroupSize() {
                    return minGroupSize;
                }

                /**
                 * Define el valor de la propiedad minGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMinGroupSize(BigInteger value) {
                    this.minGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad maxGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaxGroupSize() {
                    return maxGroupSize;
                }

                /**
                 * Define el valor de la propiedad maxGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaxGroupSize(BigInteger value) {
                    this.maxGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad knownGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getKnownGroupSize() {
                    return knownGroupSize;
                }

                /**
                 * Define el valor de la propiedad knownGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setKnownGroupSize(BigInteger value) {
                    this.knownGroupSize = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "qualifierInfo",
                "price",
                "tpaExtensions"
            })
            public static class ParticipantCategory {

                @XmlElement(name = "QualifierInfo")
                protected OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo qualifierInfo;
                @XmlElement(name = "Price")
                protected TourActivityChargeType price;
                @XmlElement(name = "TPA_Extensions")
                protected TPAExtensionsType tpaExtensions;
                @XmlAttribute(name = "Age")
                protected Integer age;

                /**
                 * Obtiene el valor de la propiedad qualifierInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo }
                 *     
                 */
                public OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo getQualifierInfo() {
                    return qualifierInfo;
                }

                /**
                 * Define el valor de la propiedad qualifierInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo }
                 *     
                 */
                public void setQualifierInfo(OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo value) {
                    this.qualifierInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad price.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getPrice() {
                    return price;
                }

                /**
                 * Define el valor de la propiedad price.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setPrice(TourActivityChargeType value) {
                    this.price = value;
                }

                /**
                 * Obtiene el valor de la propiedad tpaExtensions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public TPAExtensionsType getTPAExtensions() {
                    return tpaExtensions;
                }

                /**
                 * Define el valor de la propiedad tpaExtensions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public void setTPAExtensions(TPAExtensionsType value) {
                    this.tpaExtensions = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setAge(Integer value) {
                    this.age = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class QualifierInfo
                    extends AgeQualifierType
                {


                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pricingType"
            })
            public static class Summary
                extends TourActivityChargeType
            {

                @XmlElement(name = "PricingType")
                protected OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary.PricingType pricingType;

                /**
                 * Obtiene el valor de la propiedad pricingType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary.PricingType }
                 *     
                 */
                public OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary.PricingType getPricingType() {
                    return pricingType;
                }

                /**
                 * Define el valor de la propiedad pricingType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary.PricingType }
                 *     
                 */
                public void setPricingType(OTATourActivityAvailRS.TourActivityInfo.Pricing.Summary.PricingType value) {
                    this.pricingType = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                 *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class PricingType {

                    @XmlValue
                    protected TourActivityPricingTypeEnum value;
                    @XmlAttribute(name = "Extension")
                    protected String extension;

                    /**
                     * Tour and activity pricing options.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public TourActivityPricingTypeEnum getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public void setValue(TourActivityPricingTypeEnum value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad extension.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExtension() {
                        return extension;
                    }

                    /**
                     * Define el valor de la propiedad extension.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExtension(String value) {
                        this.extension = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Detail" maxOccurs="365" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="AvailableQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "summary",
            "detail"
        })
        public static class Schedule {

            @XmlElement(name = "Summary")
            protected OTATourActivityAvailRS.TourActivityInfo.Schedule.Summary summary;
            @XmlElement(name = "Detail")
            protected List<OTATourActivityAvailRS.TourActivityInfo.Schedule.Detail> detail;
            @XmlAttribute(name = "AvailableQty")
            protected Integer availableQty;

            /**
             * Obtiene el valor de la propiedad summary.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Schedule.Summary }
             *     
             */
            public OTATourActivityAvailRS.TourActivityInfo.Schedule.Summary getSummary() {
                return summary;
            }

            /**
             * Define el valor de la propiedad summary.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRS.TourActivityInfo.Schedule.Summary }
             *     
             */
            public void setSummary(OTATourActivityAvailRS.TourActivityInfo.Schedule.Summary value) {
                this.summary = value;
            }

            /**
             * Gets the value of the detail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityAvailRS.TourActivityInfo.Schedule.Detail }
             * 
             * 
             */
            public List<OTATourActivityAvailRS.TourActivityInfo.Schedule.Detail> getDetail() {
                if (detail == null) {
                    detail = new ArrayList<OTATourActivityAvailRS.TourActivityInfo.Schedule.Detail>();
                }
                return this.detail;
            }

            /**
             * Obtiene el valor de la propiedad availableQty.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getAvailableQty() {
                return availableQty;
            }

            /**
             * Define el valor de la propiedad availableQty.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setAvailableQty(Integer value) {
                this.availableQty = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Detail
                extends OperationScheduleType
            {


            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Summary {

                @XmlAttribute(name = "Description")
                protected String description;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SupplierOperator
            extends TourActivitySupplierType
        {


        }

    }

}
