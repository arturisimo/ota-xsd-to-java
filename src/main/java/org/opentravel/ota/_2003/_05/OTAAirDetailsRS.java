
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="FlightDetails" maxOccurs="150" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="FlightLegDetails" maxOccurs="10"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="DepartureAirport" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ArrivalAirport" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="OperatingAirline" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="MarketingCabinAvailability" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CabinAvailabilityType"&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
 *                             &amp;lt;attribute name="SequenceNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                             &amp;lt;attribute name="FlightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                             &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                             &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                             &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
 *                             &amp;lt;attribute name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                             &amp;lt;attribute name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                             &amp;lt;attribute name="DepartureDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                             &amp;lt;attribute name="LegDistance" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="DateChangeNbr"&amp;gt;
 *                               &amp;lt;simpleType&amp;gt;
 *                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                   &amp;lt;enumeration value="-1"/&amp;gt;
 *                                   &amp;lt;enumeration value="+1"/&amp;gt;
 *                                   &amp;lt;enumeration value="+2"/&amp;gt;
 *                                 &amp;lt;/restriction&amp;gt;
 *                               &amp;lt;/simpleType&amp;gt;
 *                             &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirDetailsRSAttributes"/&amp;gt;
 *                   &amp;lt;attribute name="CabinQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "flightDetails",
    "errors"
})
@XmlRootElement(name = "OTA_AirDetailsRS")
public class OTAAirDetailsRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "FlightDetails")
    protected List<OTAAirDetailsRS.FlightDetails> flightDetails;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the flightDetails property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightDetails property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFlightDetails().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirDetailsRS.FlightDetails }
     * 
     * 
     */
    public List<OTAAirDetailsRS.FlightDetails> getFlightDetails() {
        if (flightDetails == null) {
            flightDetails = new ArrayList<OTAAirDetailsRS.FlightDetails>();
        }
        return this.flightDetails;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FlightLegDetails" maxOccurs="10"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="DepartureAirport" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ArrivalAirport" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="OperatingAirline" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="MarketingCabinAvailability" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CabinAvailabilityType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
     *                 &amp;lt;attribute name="SequenceNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                 &amp;lt;attribute name="FlightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                 &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                 &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
     *                 &amp;lt;attribute name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="DepartureDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                 &amp;lt;attribute name="LegDistance" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="DateChangeNbr"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="-1"/&amp;gt;
     *                       &amp;lt;enumeration value="+1"/&amp;gt;
     *                       &amp;lt;enumeration value="+2"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirDetailsRSAttributes"/&amp;gt;
     *       &amp;lt;attribute name="CabinQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightLegDetails"
    })
    public static class FlightDetails {

        @XmlElement(name = "FlightLegDetails", required = true)
        protected List<OTAAirDetailsRS.FlightDetails.FlightLegDetails> flightLegDetails;
        @XmlAttribute(name = "CabinQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger cabinQty;
        @XmlAttribute(name = "TotalFlightTime")
        protected Duration totalFlightTime;
        @XmlAttribute(name = "TotalGroundTime")
        protected Duration totalGroundTime;
        @XmlAttribute(name = "TotalTripTime")
        protected Duration totalTripTime;
        @XmlAttribute(name = "TotalMiles")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger totalMiles;

        /**
         * Gets the value of the flightLegDetails property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightLegDetails property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFlightLegDetails().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirDetailsRS.FlightDetails.FlightLegDetails }
         * 
         * 
         */
        public List<OTAAirDetailsRS.FlightDetails.FlightLegDetails> getFlightLegDetails() {
            if (flightLegDetails == null) {
                flightLegDetails = new ArrayList<OTAAirDetailsRS.FlightDetails.FlightLegDetails>();
            }
            return this.flightLegDetails;
        }

        /**
         * Obtiene el valor de la propiedad cabinQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCabinQty() {
            return cabinQty;
        }

        /**
         * Define el valor de la propiedad cabinQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCabinQty(BigInteger value) {
            this.cabinQty = value;
        }

        /**
         * Obtiene el valor de la propiedad totalFlightTime.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getTotalFlightTime() {
            return totalFlightTime;
        }

        /**
         * Define el valor de la propiedad totalFlightTime.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setTotalFlightTime(Duration value) {
            this.totalFlightTime = value;
        }

        /**
         * Obtiene el valor de la propiedad totalGroundTime.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getTotalGroundTime() {
            return totalGroundTime;
        }

        /**
         * Define el valor de la propiedad totalGroundTime.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setTotalGroundTime(Duration value) {
            this.totalGroundTime = value;
        }

        /**
         * Obtiene el valor de la propiedad totalTripTime.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getTotalTripTime() {
            return totalTripTime;
        }

        /**
         * Define el valor de la propiedad totalTripTime.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setTotalTripTime(Duration value) {
            this.totalTripTime = value;
        }

        /**
         * Obtiene el valor de la propiedad totalMiles.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalMiles() {
            return totalMiles;
        }

        /**
         * Define el valor de la propiedad totalMiles.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalMiles(BigInteger value) {
            this.totalMiles = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="DepartureAirport" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ArrivalAirport" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="OperatingAirline" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="MarketingCabinAvailability" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CabinAvailabilityType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
         *       &amp;lt;attribute name="SequenceNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *       &amp;lt;attribute name="FlightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *       &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *       &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
         *       &amp;lt;attribute name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="DepartureDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *       &amp;lt;attribute name="LegDistance" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="DateChangeNbr"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="-1"/&amp;gt;
         *             &amp;lt;enumeration value="+1"/&amp;gt;
         *             &amp;lt;enumeration value="+2"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "departureAirport",
            "arrivalAirport",
            "marketingAirline",
            "operatingAirline",
            "equipment",
            "comment",
            "marketingCabinAvailability",
            "warnings"
        })
        public static class FlightLegDetails {

            @XmlElement(name = "DepartureAirport")
            protected OTAAirDetailsRS.FlightDetails.FlightLegDetails.DepartureAirport departureAirport;
            @XmlElement(name = "ArrivalAirport")
            protected OTAAirDetailsRS.FlightDetails.FlightLegDetails.ArrivalAirport arrivalAirport;
            @XmlElement(name = "MarketingAirline")
            protected CompanyNameType marketingAirline;
            @XmlElement(name = "OperatingAirline")
            protected OperatingAirlineType operatingAirline;
            @XmlElement(name = "Equipment")
            protected EquipmentType equipment;
            @XmlElement(name = "Comment")
            protected List<FreeTextType> comment;
            @XmlElement(name = "MarketingCabinAvailability")
            protected List<OTAAirDetailsRS.FlightDetails.FlightLegDetails.MarketingCabinAvailability> marketingCabinAvailability;
            @XmlElement(name = "Warnings")
            protected WarningsType warnings;
            @XmlAttribute(name = "SequenceNumber", required = true)
            protected BigInteger sequenceNumber;
            @XmlAttribute(name = "FlightNumber", required = true)
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger flightNumber;
            @XmlAttribute(name = "JourneyDuration")
            protected Duration journeyDuration;
            @XmlAttribute(name = "GroundDuration")
            protected Duration groundDuration;
            @XmlAttribute(name = "OnTimeRate")
            protected BigDecimal onTimeRate;
            @XmlAttribute(name = "Ticket")
            protected TicketType ticket;
            @XmlAttribute(name = "DepartureDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar departureDateTime;
            @XmlAttribute(name = "ArrivalDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar arrivalDateTime;
            @XmlAttribute(name = "DepartureDayOfWeek")
            protected DayOfWeekType departureDayOfWeek;
            @XmlAttribute(name = "LegDistance")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger legDistance;
            @XmlAttribute(name = "DateChangeNbr")
            protected String dateChangeNbr;
            @XmlAttribute(name = "SmokingAllowed")
            protected Boolean smokingAllowed;

            /**
             * Obtiene el valor de la propiedad departureAirport.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirDetailsRS.FlightDetails.FlightLegDetails.DepartureAirport }
             *     
             */
            public OTAAirDetailsRS.FlightDetails.FlightLegDetails.DepartureAirport getDepartureAirport() {
                return departureAirport;
            }

            /**
             * Define el valor de la propiedad departureAirport.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirDetailsRS.FlightDetails.FlightLegDetails.DepartureAirport }
             *     
             */
            public void setDepartureAirport(OTAAirDetailsRS.FlightDetails.FlightLegDetails.DepartureAirport value) {
                this.departureAirport = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalAirport.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirDetailsRS.FlightDetails.FlightLegDetails.ArrivalAirport }
             *     
             */
            public OTAAirDetailsRS.FlightDetails.FlightLegDetails.ArrivalAirport getArrivalAirport() {
                return arrivalAirport;
            }

            /**
             * Define el valor de la propiedad arrivalAirport.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirDetailsRS.FlightDetails.FlightLegDetails.ArrivalAirport }
             *     
             */
            public void setArrivalAirport(OTAAirDetailsRS.FlightDetails.FlightLegDetails.ArrivalAirport value) {
                this.arrivalAirport = value;
            }

            /**
             * Obtiene el valor de la propiedad marketingAirline.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getMarketingAirline() {
                return marketingAirline;
            }

            /**
             * Define el valor de la propiedad marketingAirline.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setMarketingAirline(CompanyNameType value) {
                this.marketingAirline = value;
            }

            /**
             * Obtiene el valor de la propiedad operatingAirline.
             * 
             * @return
             *     possible object is
             *     {@link OperatingAirlineType }
             *     
             */
            public OperatingAirlineType getOperatingAirline() {
                return operatingAirline;
            }

            /**
             * Define el valor de la propiedad operatingAirline.
             * 
             * @param value
             *     allowed object is
             *     {@link OperatingAirlineType }
             *     
             */
            public void setOperatingAirline(OperatingAirlineType value) {
                this.operatingAirline = value;
            }

            /**
             * Obtiene el valor de la propiedad equipment.
             * 
             * @return
             *     possible object is
             *     {@link EquipmentType }
             *     
             */
            public EquipmentType getEquipment() {
                return equipment;
            }

            /**
             * Define el valor de la propiedad equipment.
             * 
             * @param value
             *     allowed object is
             *     {@link EquipmentType }
             *     
             */
            public void setEquipment(EquipmentType value) {
                this.equipment = value;
            }

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link FreeTextType }
             * 
             * 
             */
            public List<FreeTextType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<FreeTextType>();
                }
                return this.comment;
            }

            /**
             * Gets the value of the marketingCabinAvailability property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingCabinAvailability property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMarketingCabinAvailability().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirDetailsRS.FlightDetails.FlightLegDetails.MarketingCabinAvailability }
             * 
             * 
             */
            public List<OTAAirDetailsRS.FlightDetails.FlightLegDetails.MarketingCabinAvailability> getMarketingCabinAvailability() {
                if (marketingCabinAvailability == null) {
                    marketingCabinAvailability = new ArrayList<OTAAirDetailsRS.FlightDetails.FlightLegDetails.MarketingCabinAvailability>();
                }
                return this.marketingCabinAvailability;
            }

            /**
             * Obtiene el valor de la propiedad warnings.
             * 
             * @return
             *     possible object is
             *     {@link WarningsType }
             *     
             */
            public WarningsType getWarnings() {
                return warnings;
            }

            /**
             * Define el valor de la propiedad warnings.
             * 
             * @param value
             *     allowed object is
             *     {@link WarningsType }
             *     
             */
            public void setWarnings(WarningsType value) {
                this.warnings = value;
            }

            /**
             * Obtiene el valor de la propiedad sequenceNumber.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSequenceNumber() {
                return sequenceNumber;
            }

            /**
             * Define el valor de la propiedad sequenceNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSequenceNumber(BigInteger value) {
                this.sequenceNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad flightNumber.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFlightNumber() {
                return flightNumber;
            }

            /**
             * Define el valor de la propiedad flightNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFlightNumber(BigInteger value) {
                this.flightNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad journeyDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getJourneyDuration() {
                return journeyDuration;
            }

            /**
             * Define el valor de la propiedad journeyDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setJourneyDuration(Duration value) {
                this.journeyDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad groundDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getGroundDuration() {
                return groundDuration;
            }

            /**
             * Define el valor de la propiedad groundDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setGroundDuration(Duration value) {
                this.groundDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad onTimeRate.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getOnTimeRate() {
                return onTimeRate;
            }

            /**
             * Define el valor de la propiedad onTimeRate.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setOnTimeRate(BigDecimal value) {
                this.onTimeRate = value;
            }

            /**
             * Obtiene el valor de la propiedad ticket.
             * 
             * @return
             *     possible object is
             *     {@link TicketType }
             *     
             */
            public TicketType getTicket() {
                return ticket;
            }

            /**
             * Define el valor de la propiedad ticket.
             * 
             * @param value
             *     allowed object is
             *     {@link TicketType }
             *     
             */
            public void setTicket(TicketType value) {
                this.ticket = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDepartureDateTime() {
                return departureDateTime;
            }

            /**
             * Define el valor de la propiedad departureDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDepartureDateTime(XMLGregorianCalendar value) {
                this.departureDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getArrivalDateTime() {
                return arrivalDateTime;
            }

            /**
             * Define el valor de la propiedad arrivalDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setArrivalDateTime(XMLGregorianCalendar value) {
                this.arrivalDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDayOfWeek.
             * 
             * @return
             *     possible object is
             *     {@link DayOfWeekType }
             *     
             */
            public DayOfWeekType getDepartureDayOfWeek() {
                return departureDayOfWeek;
            }

            /**
             * Define el valor de la propiedad departureDayOfWeek.
             * 
             * @param value
             *     allowed object is
             *     {@link DayOfWeekType }
             *     
             */
            public void setDepartureDayOfWeek(DayOfWeekType value) {
                this.departureDayOfWeek = value;
            }

            /**
             * Obtiene el valor de la propiedad legDistance.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLegDistance() {
                return legDistance;
            }

            /**
             * Define el valor de la propiedad legDistance.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLegDistance(BigInteger value) {
                this.legDistance = value;
            }

            /**
             * Obtiene el valor de la propiedad dateChangeNbr.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateChangeNbr() {
                return dateChangeNbr;
            }

            /**
             * Define el valor de la propiedad dateChangeNbr.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateChangeNbr(String value) {
                this.dateChangeNbr = value;
            }

            /**
             * Obtiene el valor de la propiedad smokingAllowed.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSmokingAllowed() {
                return smokingAllowed;
            }

            /**
             * Define el valor de la propiedad smokingAllowed.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSmokingAllowed(Boolean value) {
                this.smokingAllowed = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ArrivalAirport {

                @XmlAttribute(name = "LocationCode")
                protected String locationCode;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "Terminal")
                protected String terminal;
                @XmlAttribute(name = "Gate")
                protected String gate;

                /**
                 * Obtiene el valor de la propiedad locationCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationCode() {
                    return locationCode;
                }

                /**
                 * Define el valor de la propiedad locationCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationCode(String value) {
                    this.locationCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad terminal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTerminal() {
                    return terminal;
                }

                /**
                 * Define el valor de la propiedad terminal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTerminal(String value) {
                    this.terminal = value;
                }

                /**
                 * Obtiene el valor de la propiedad gate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGate() {
                    return gate;
                }

                /**
                 * Define el valor de la propiedad gate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGate(String value) {
                    this.gate = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DepartureAirport {

                @XmlAttribute(name = "LocationCode")
                protected String locationCode;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "Terminal")
                protected String terminal;
                @XmlAttribute(name = "Gate")
                protected String gate;

                /**
                 * Obtiene el valor de la propiedad locationCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationCode() {
                    return locationCode;
                }

                /**
                 * Define el valor de la propiedad locationCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationCode(String value) {
                    this.locationCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad terminal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTerminal() {
                    return terminal;
                }

                /**
                 * Define el valor de la propiedad terminal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTerminal(String value) {
                    this.terminal = value;
                }

                /**
                 * Obtiene el valor de la propiedad gate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGate() {
                    return gate;
                }

                /**
                 * Define el valor de la propiedad gate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGate(String value) {
                    this.gate = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CabinAvailabilityType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MarketingCabinAvailability
                extends CabinAvailabilityType
            {


            }

        }

    }

}
