
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type"/&amp;gt;
 *         &amp;lt;element name="ProcessingInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirProcessingInfoGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MultimodalOffer" type="{http://www.opentravel.org/OTA/2003/05}MultiModalOfferType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="OriginDestinationInformation" maxOccurs="10"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="RefNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SpecificFlightInfo" type="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TravelPreferences" maxOccurs="10" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType"&amp;gt;
 *                 &amp;lt;attribute name="FlexDatePref"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Outbound"/&amp;gt;
 *                       &amp;lt;enumeration value="Return"/&amp;gt;
 *                       &amp;lt;enumeration value="Both"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="FlexWeekendIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="FlexLevelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NoFareBreakIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="OriginDestinationRPHs" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TravelerInfoSummary"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TravelerInfoSummaryType"&amp;gt;
 *                 &amp;lt;attribute name="TicketingCountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                 &amp;lt;attribute name="SpecificPTC_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ArrangerInfoSummary" type="{http://www.opentravel.org/OTA/2003/05}AirArrangerType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MaxResponsesGroup"/&amp;gt;
 *       &amp;lt;attribute name="DirectFlightsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="AvailableFlightsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "processingInfo",
    "multimodalOffer",
    "originDestinationInformation",
    "specificFlightInfo",
    "travelPreferences",
    "travelerInfoSummary",
    "arrangerInfoSummary"
})
@XmlRootElement(name = "OTA_AirLowFareSearchRQ")
public class OTAAirLowFareSearchRQ {

    @XmlElement(name = "POS", required = true)
    protected POSType pos;
    @XmlElement(name = "ProcessingInfo")
    protected OTAAirLowFareSearchRQ.ProcessingInfo processingInfo;
    @XmlElement(name = "MultimodalOffer")
    protected MultiModalOfferType multimodalOffer;
    @XmlElement(name = "OriginDestinationInformation", required = true)
    protected List<OTAAirLowFareSearchRQ.OriginDestinationInformation> originDestinationInformation;
    @XmlElement(name = "SpecificFlightInfo")
    protected SpecificFlightInfoType specificFlightInfo;
    @XmlElement(name = "TravelPreferences")
    protected List<OTAAirLowFareSearchRQ.TravelPreferences> travelPreferences;
    @XmlElement(name = "TravelerInfoSummary", required = true)
    protected OTAAirLowFareSearchRQ.TravelerInfoSummary travelerInfoSummary;
    @XmlElement(name = "ArrangerInfoSummary")
    protected AirArrangerType arrangerInfoSummary;
    @XmlAttribute(name = "DirectFlightsOnly")
    protected Boolean directFlightsOnly;
    @XmlAttribute(name = "AvailableFlightsOnly")
    protected Boolean availableFlightsOnly;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirLowFareSearchRQ.ProcessingInfo }
     *     
     */
    public OTAAirLowFareSearchRQ.ProcessingInfo getProcessingInfo() {
        return processingInfo;
    }

    /**
     * Define el valor de la propiedad processingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirLowFareSearchRQ.ProcessingInfo }
     *     
     */
    public void setProcessingInfo(OTAAirLowFareSearchRQ.ProcessingInfo value) {
        this.processingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad multimodalOffer.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType }
     *     
     */
    public MultiModalOfferType getMultimodalOffer() {
        return multimodalOffer;
    }

    /**
     * Define el valor de la propiedad multimodalOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType }
     *     
     */
    public void setMultimodalOffer(MultiModalOfferType value) {
        this.multimodalOffer = value;
    }

    /**
     * Gets the value of the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOriginDestinationInformation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirLowFareSearchRQ.OriginDestinationInformation }
     * 
     * 
     */
    public List<OTAAirLowFareSearchRQ.OriginDestinationInformation> getOriginDestinationInformation() {
        if (originDestinationInformation == null) {
            originDestinationInformation = new ArrayList<OTAAirLowFareSearchRQ.OriginDestinationInformation>();
        }
        return this.originDestinationInformation;
    }

    /**
     * Obtiene el valor de la propiedad specificFlightInfo.
     * 
     * @return
     *     possible object is
     *     {@link SpecificFlightInfoType }
     *     
     */
    public SpecificFlightInfoType getSpecificFlightInfo() {
        return specificFlightInfo;
    }

    /**
     * Define el valor de la propiedad specificFlightInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificFlightInfoType }
     *     
     */
    public void setSpecificFlightInfo(SpecificFlightInfoType value) {
        this.specificFlightInfo = value;
    }

    /**
     * Gets the value of the travelPreferences property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelPreferences property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTravelPreferences().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirLowFareSearchRQ.TravelPreferences }
     * 
     * 
     */
    public List<OTAAirLowFareSearchRQ.TravelPreferences> getTravelPreferences() {
        if (travelPreferences == null) {
            travelPreferences = new ArrayList<OTAAirLowFareSearchRQ.TravelPreferences>();
        }
        return this.travelPreferences;
    }

    /**
     * Obtiene el valor de la propiedad travelerInfoSummary.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirLowFareSearchRQ.TravelerInfoSummary }
     *     
     */
    public OTAAirLowFareSearchRQ.TravelerInfoSummary getTravelerInfoSummary() {
        return travelerInfoSummary;
    }

    /**
     * Define el valor de la propiedad travelerInfoSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirLowFareSearchRQ.TravelerInfoSummary }
     *     
     */
    public void setTravelerInfoSummary(OTAAirLowFareSearchRQ.TravelerInfoSummary value) {
        this.travelerInfoSummary = value;
    }

    /**
     * Obtiene el valor de la propiedad arrangerInfoSummary.
     * 
     * @return
     *     possible object is
     *     {@link AirArrangerType }
     *     
     */
    public AirArrangerType getArrangerInfoSummary() {
        return arrangerInfoSummary;
    }

    /**
     * Define el valor de la propiedad arrangerInfoSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link AirArrangerType }
     *     
     */
    public void setArrangerInfoSummary(AirArrangerType value) {
        this.arrangerInfoSummary = value;
    }

    /**
     * Obtiene el valor de la propiedad directFlightsOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectFlightsOnly() {
        return directFlightsOnly;
    }

    /**
     * Define el valor de la propiedad directFlightsOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectFlightsOnly(Boolean value) {
        this.directFlightsOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad availableFlightsOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAvailableFlightsOnly() {
        return availableFlightsOnly;
    }

    /**
     * Define el valor de la propiedad availableFlightsOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAvailableFlightsOnly(Boolean value) {
        this.availableFlightsOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="RefNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alternateLocationInfo",
        "tpaExtensions"
    })
    public static class OriginDestinationInformation
        extends OriginDestinationInformationType
    {

        @XmlElement(name = "AlternateLocationInfo")
        protected OTAAirLowFareSearchRQ.OriginDestinationInformation.AlternateLocationInfo alternateLocationInfo;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;
        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "RefNumber")
        protected Integer refNumber;

        /**
         * Obtiene el valor de la propiedad alternateLocationInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirLowFareSearchRQ.OriginDestinationInformation.AlternateLocationInfo }
         *     
         */
        public OTAAirLowFareSearchRQ.OriginDestinationInformation.AlternateLocationInfo getAlternateLocationInfo() {
            return alternateLocationInfo;
        }

        /**
         * Define el valor de la propiedad alternateLocationInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirLowFareSearchRQ.OriginDestinationInformation.AlternateLocationInfo }
         *     
         */
        public void setAlternateLocationInfo(OTAAirLowFareSearchRQ.OriginDestinationInformation.AlternateLocationInfo value) {
            this.alternateLocationInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad refNumber.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRefNumber() {
            return refNumber;
        }

        /**
         * Define el valor de la propiedad refNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRefNumber(Integer value) {
            this.refNumber = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AlternateLocationInfo {

            @XmlAttribute(name = "OriginLocation")
            protected List<String> originLocation;
            @XmlAttribute(name = "DestinationLocation")
            protected List<String> destinationLocation;

            /**
             * Gets the value of the originLocation property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originLocation property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getOriginLocation().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getOriginLocation() {
                if (originLocation == null) {
                    originLocation = new ArrayList<String>();
                }
                return this.originLocation;
            }

            /**
             * Gets the value of the destinationLocation property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the destinationLocation property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDestinationLocation().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getDestinationLocation() {
                if (destinationLocation == null) {
                    destinationLocation = new ArrayList<String>();
                }
                return this.destinationLocation;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirProcessingInfoGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInfo {

        @XmlAttribute(name = "TargetSource")
        protected String targetSource;
        @XmlAttribute(name = "FlightSvcInfoIndicator")
        protected Boolean flightSvcInfoIndicator;
        @XmlAttribute(name = "DisplayOrder")
        protected DisplayOrderType displayOrder;
        @XmlAttribute(name = "ReducedDataIndicator")
        protected Boolean reducedDataIndicator;
        @XmlAttribute(name = "BaseFaresOnlyIndicator")
        protected Boolean baseFaresOnlyIndicator;
        @XmlAttribute(name = "SearchType")
        protected String searchType;
        @XmlAttribute(name = "AvailabilityIndicator")
        protected Boolean availabilityIndicator;

        /**
         * Obtiene el valor de la propiedad targetSource.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTargetSource() {
            return targetSource;
        }

        /**
         * Define el valor de la propiedad targetSource.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTargetSource(String value) {
            this.targetSource = value;
        }

        /**
         * Obtiene el valor de la propiedad flightSvcInfoIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFlightSvcInfoIndicator() {
            return flightSvcInfoIndicator;
        }

        /**
         * Define el valor de la propiedad flightSvcInfoIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFlightSvcInfoIndicator(Boolean value) {
            this.flightSvcInfoIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad displayOrder.
         * 
         * @return
         *     possible object is
         *     {@link DisplayOrderType }
         *     
         */
        public DisplayOrderType getDisplayOrder() {
            return displayOrder;
        }

        /**
         * Define el valor de la propiedad displayOrder.
         * 
         * @param value
         *     allowed object is
         *     {@link DisplayOrderType }
         *     
         */
        public void setDisplayOrder(DisplayOrderType value) {
            this.displayOrder = value;
        }

        /**
         * Obtiene el valor de la propiedad reducedDataIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReducedDataIndicator() {
            return reducedDataIndicator;
        }

        /**
         * Define el valor de la propiedad reducedDataIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReducedDataIndicator(Boolean value) {
            this.reducedDataIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad baseFaresOnlyIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isBaseFaresOnlyIndicator() {
            return baseFaresOnlyIndicator;
        }

        /**
         * Define el valor de la propiedad baseFaresOnlyIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setBaseFaresOnlyIndicator(Boolean value) {
            this.baseFaresOnlyIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad searchType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSearchType() {
            return searchType;
        }

        /**
         * Define el valor de la propiedad searchType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSearchType(String value) {
            this.searchType = value;
        }

        /**
         * Obtiene el valor de la propiedad availabilityIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAvailabilityIndicator() {
            return availabilityIndicator;
        }

        /**
         * Define el valor de la propiedad availabilityIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAvailabilityIndicator(Boolean value) {
            this.availabilityIndicator = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType"&amp;gt;
     *       &amp;lt;attribute name="FlexDatePref"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Outbound"/&amp;gt;
     *             &amp;lt;enumeration value="Return"/&amp;gt;
     *             &amp;lt;enumeration value="Both"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="FlexWeekendIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="FlexLevelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NoFareBreakIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="OriginDestinationRPHs" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TravelPreferences
        extends AirSearchPrefsType
    {

        @XmlAttribute(name = "FlexDatePref")
        protected String flexDatePref;
        @XmlAttribute(name = "FlexWeekendIndicator")
        protected Boolean flexWeekendIndicator;
        @XmlAttribute(name = "FlexLevelIndicator")
        protected Boolean flexLevelIndicator;
        @XmlAttribute(name = "NoFareBreakIndicator")
        protected Boolean noFareBreakIndicator;
        @XmlAttribute(name = "OriginDestinationRPHs")
        protected List<String> originDestinationRPHs;

        /**
         * Obtiene el valor de la propiedad flexDatePref.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlexDatePref() {
            return flexDatePref;
        }

        /**
         * Define el valor de la propiedad flexDatePref.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlexDatePref(String value) {
            this.flexDatePref = value;
        }

        /**
         * Obtiene el valor de la propiedad flexWeekendIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFlexWeekendIndicator() {
            return flexWeekendIndicator;
        }

        /**
         * Define el valor de la propiedad flexWeekendIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFlexWeekendIndicator(Boolean value) {
            this.flexWeekendIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad flexLevelIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFlexLevelIndicator() {
            return flexLevelIndicator;
        }

        /**
         * Define el valor de la propiedad flexLevelIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFlexLevelIndicator(Boolean value) {
            this.flexLevelIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad noFareBreakIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNoFareBreakIndicator() {
            return noFareBreakIndicator;
        }

        /**
         * Define el valor de la propiedad noFareBreakIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNoFareBreakIndicator(Boolean value) {
            this.noFareBreakIndicator = value;
        }

        /**
         * Gets the value of the originDestinationRPHs property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationRPHs property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestinationRPHs().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getOriginDestinationRPHs() {
            if (originDestinationRPHs == null) {
                originDestinationRPHs = new ArrayList<String>();
            }
            return this.originDestinationRPHs;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TravelerInfoSummaryType"&amp;gt;
     *       &amp;lt;attribute name="TicketingCountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *       &amp;lt;attribute name="SpecificPTC_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TravelerInfoSummary
        extends TravelerInfoSummaryType
    {

        @XmlAttribute(name = "TicketingCountryCode")
        protected String ticketingCountryCode;
        @XmlAttribute(name = "SpecificPTC_Indicator")
        protected Boolean specificPTCIndicator;

        /**
         * Obtiene el valor de la propiedad ticketingCountryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketingCountryCode() {
            return ticketingCountryCode;
        }

        /**
         * Define el valor de la propiedad ticketingCountryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketingCountryCode(String value) {
            this.ticketingCountryCode = value;
        }

        /**
         * Obtiene el valor de la propiedad specificPTCIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSpecificPTCIndicator() {
            return specificPTCIndicator;
        }

        /**
         * Define el valor de la propiedad specificPTCIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSpecificPTCIndicator(Boolean value) {
            this.specificPTCIndicator = value;
        }

    }

}
