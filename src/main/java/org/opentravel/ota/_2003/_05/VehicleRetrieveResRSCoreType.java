
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The VehicleRetrieveResRSCoreType complex type identifies the core, or common, information that is associated with a retrieved reservation, or a list of reservations when one exact match could not be identified.
 * 
 * &lt;p&gt;Clase Java para VehicleRetrieveResRSCoreType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleRetrieveResRSCoreType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;element name="VehReservation" type="{http://www.opentravel.org/OTA/2003/05}VehicleReservationType"/&amp;gt;
 *           &amp;lt;element name="VehResSummaries"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="VehResSummary" type="{http://www.opentravel.org/OTA/2003/05}VehicleReservationSummaryType" maxOccurs="99"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleRetrieveResRSCoreType", propOrder = {
    "vehReservation",
    "vehResSummaries"
})
public class VehicleRetrieveResRSCoreType {

    @XmlElement(name = "VehReservation")
    protected VehicleReservationType vehReservation;
    @XmlElement(name = "VehResSummaries")
    protected VehicleRetrieveResRSCoreType.VehResSummaries vehResSummaries;

    /**
     * Obtiene el valor de la propiedad vehReservation.
     * 
     * @return
     *     possible object is
     *     {@link VehicleReservationType }
     *     
     */
    public VehicleReservationType getVehReservation() {
        return vehReservation;
    }

    /**
     * Define el valor de la propiedad vehReservation.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleReservationType }
     *     
     */
    public void setVehReservation(VehicleReservationType value) {
        this.vehReservation = value;
    }

    /**
     * Obtiene el valor de la propiedad vehResSummaries.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRetrieveResRSCoreType.VehResSummaries }
     *     
     */
    public VehicleRetrieveResRSCoreType.VehResSummaries getVehResSummaries() {
        return vehResSummaries;
    }

    /**
     * Define el valor de la propiedad vehResSummaries.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRetrieveResRSCoreType.VehResSummaries }
     *     
     */
    public void setVehResSummaries(VehicleRetrieveResRSCoreType.VehResSummaries value) {
        this.vehResSummaries = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VehResSummary" type="{http://www.opentravel.org/OTA/2003/05}VehicleReservationSummaryType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehResSummary"
    })
    public static class VehResSummaries {

        @XmlElement(name = "VehResSummary", required = true)
        protected List<VehicleReservationSummaryType> vehResSummary;

        /**
         * Gets the value of the vehResSummary property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehResSummary property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVehResSummary().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleReservationSummaryType }
         * 
         * 
         */
        public List<VehicleReservationSummaryType> getVehResSummary() {
            if (vehResSummary == null) {
                vehResSummary = new ArrayList<VehicleReservationSummaryType>();
            }
            return this.vehResSummary;
        }

    }

}
