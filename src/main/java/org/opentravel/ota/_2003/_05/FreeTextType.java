
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Textual information to provide descriptions and/or additional information.
 * 
 * &lt;p&gt;Clase Java para FreeTextType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FreeTextType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LanguageGroup"/&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreeTextType", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo.class,
    org.opentravel.ota._2003._05.OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.Comment.class,
    org.opentravel.ota._2003._05.OTACancelRQ.Reasons.Reason.class,
    org.opentravel.ota._2003._05.OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo.class,
    WarningType.class,
    ErrorType.class,
    org.opentravel.ota._2003._05.IndCoverageReqsType.PreexistingConditions.PreexistingCondition.class,
    org.opentravel.ota._2003._05.PTCFareBreakdownType.Endorsements.Endorsement.class,
    CertificationType.class,
    CommissionInfoType.class,
    org.opentravel.ota._2003._05.FareRuleResponseInfoType.AdvisoryInfo.class,
    org.opentravel.ota._2003._05.FareCodeOptionType.FareRemark.class,
    org.opentravel.ota._2003._05.PlanRestrictionType.PlanRestriction.class,
    org.opentravel.ota._2003._05.TicketingInfoType.TicketAdvisory.class
})
public class FreeTextType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Language")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad language.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Define el valor de la propiedad language.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

}
