
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para HotelComponentSearchType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="HotelComponentSearchType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AvailPrice"/&amp;gt;
 *     &amp;lt;enumeration value="Avail"/&amp;gt;
 *     &amp;lt;enumeration value="PropertyList"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "HotelComponentSearchType")
@XmlEnum
public enum HotelComponentSearchType {

    @XmlEnumValue("AvailPrice")
    AVAIL_PRICE("AvailPrice"),
    @XmlEnumValue("Avail")
    AVAIL("Avail"),
    @XmlEnumValue("PropertyList")
    PROPERTY_LIST("PropertyList");
    private final String value;

    HotelComponentSearchType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HotelComponentSearchType fromValue(String v) {
        for (HotelComponentSearchType c: HotelComponentSearchType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
