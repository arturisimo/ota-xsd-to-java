
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para DeckType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="DeckType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Regular-OneLevelOnly"/&amp;gt;
 *     &amp;lt;enumeration value="LowerLevel"/&amp;gt;
 *     &amp;lt;enumeration value="UpperLevel"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "DeckType")
@XmlEnum
public enum DeckType {

    @XmlEnumValue("Regular-OneLevelOnly")
    REGULAR_ONE_LEVEL_ONLY("Regular-OneLevelOnly"),
    @XmlEnumValue("LowerLevel")
    LOWER_LEVEL("LowerLevel"),
    @XmlEnumValue("UpperLevel")
    UPPER_LEVEL("UpperLevel");
    private final String value;

    DeckType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeckType fromValue(String v) {
        for (DeckType c: DeckType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
