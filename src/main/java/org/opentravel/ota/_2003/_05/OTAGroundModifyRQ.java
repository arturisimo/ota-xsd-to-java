
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ModifyType" type="{http://www.opentravel.org/OTA/2003/05}List_TransactionAction_Base" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type"/&amp;gt;
 *         &amp;lt;element name="Reservation" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ReferenceID"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                           &amp;lt;attribute name="CancelInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;choice&amp;gt;
 *                     &amp;lt;element name="Service"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_GroundServiceProvided" minOccurs="0" form="qualified"/&amp;gt;
 *                               &amp;lt;element name="ServiceLevel" type="{http://www.opentravel.org/OTA/2003/05}List_LevelOfService" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Locations" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Dropoff" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="Pickup" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="Stops" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
 *                                                           &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="VehicleSize" type="{http://www.opentravel.org/OTA/2003/05}List_VehSize" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="VehicleType" type="{http://www.opentravel.org/OTA/2003/05}List_VehCategory" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Languages" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Language" maxOccurs="unbounded"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                                                 &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="ChildCarSeatInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="PetFriendlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="FuelEfficientVehInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="MaximumPassengers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="MaximumBaggage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="GreetingSignInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="GreetingSignName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="MeetAndGreetInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="PersonalGreeterInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="MultilingualInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="ShortDescription"&amp;gt;
 *                               &amp;lt;simpleType&amp;gt;
 *                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                   &amp;lt;minLength value="0"/&amp;gt;
 *                                   &amp;lt;maxLength value="250"/&amp;gt;
 *                                 &amp;lt;/restriction&amp;gt;
 *                               &amp;lt;/simpleType&amp;gt;
 *                             &amp;lt;/attribute&amp;gt;
 *                             &amp;lt;attribute name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="DisabilityVehicleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Shuttle"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundShuttleType"&amp;gt;
 *                             &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/choice&amp;gt;
 *                   &amp;lt;element name="Passenger" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Primary" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Additional" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="AdditionalType" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="CorpDiscountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                     &amp;lt;attribute name="CorpDiscountNmbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                     &amp;lt;attribute name="QualificationMethod"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="RT_AirlineTicket"/&amp;gt;
 *                                           &amp;lt;enumeration value="CreditCard"/&amp;gt;
 *                                           &amp;lt;enumeration value="PassportAndReturnTkt"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="RateQualifier" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Payment" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" maxOccurs="unbounded"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Payment" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReqRespVersion"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modifyType",
    "pos",
    "reservation",
    "payment",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_GroundModifyRQ")
public class OTAGroundModifyRQ {

    @XmlElement(name = "ModifyType")
    @XmlSchemaType(name = "string")
    protected ListTransactionActionBase modifyType;
    @XmlElement(name = "POS", required = true)
    protected POSType pos;
    @XmlElement(name = "Reservation", required = true)
    protected List<OTAGroundModifyRQ.Reservation> reservation;
    @XmlElement(name = "Payment")
    protected OTAGroundModifyRQ.Payment payment;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "ReqRespVersion")
    protected String reqRespVersion;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad modifyType.
     * 
     * @return
     *     possible object is
     *     {@link ListTransactionActionBase }
     *     
     */
    public ListTransactionActionBase getModifyType() {
        return modifyType;
    }

    /**
     * Define el valor de la propiedad modifyType.
     * 
     * @param value
     *     allowed object is
     *     {@link ListTransactionActionBase }
     *     
     */
    public void setModifyType(ListTransactionActionBase value) {
        this.modifyType = value;
    }

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Gets the value of the reservation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reservation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getReservation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAGroundModifyRQ.Reservation }
     * 
     * 
     */
    public List<OTAGroundModifyRQ.Reservation> getReservation() {
        if (reservation == null) {
            reservation = new ArrayList<OTAGroundModifyRQ.Reservation>();
        }
        return this.reservation;
    }

    /**
     * Obtiene el valor de la propiedad payment.
     * 
     * @return
     *     possible object is
     *     {@link OTAGroundModifyRQ.Payment }
     *     
     */
    public OTAGroundModifyRQ.Payment getPayment() {
        return payment;
    }

    /**
     * Define el valor de la propiedad payment.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAGroundModifyRQ.Payment }
     *     
     */
    public void setPayment(OTAGroundModifyRQ.Payment value) {
        this.payment = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad reqRespVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqRespVersion() {
        return reqRespVersion;
    }

    /**
     * Define el valor de la propiedad reqRespVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqRespVersion(String value) {
        this.reqRespVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payment"
    })
    public static class Payment {

        @XmlElement(name = "Payment", required = true)
        protected List<PaymentFormType> payment;

        /**
         * Gets the value of the payment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the payment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPayment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PaymentFormType }
         * 
         * 
         */
        public List<PaymentFormType> getPayment() {
            if (payment == null) {
                payment = new ArrayList<PaymentFormType>();
            }
            return this.payment;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ReferenceID"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                 &amp;lt;attribute name="CancelInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;choice&amp;gt;
     *           &amp;lt;element name="Service"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;complexContent&amp;gt;
     *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                   &amp;lt;sequence&amp;gt;
     *                     &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_GroundServiceProvided" minOccurs="0" form="qualified"/&amp;gt;
     *                     &amp;lt;element name="ServiceLevel" type="{http://www.opentravel.org/OTA/2003/05}List_LevelOfService" minOccurs="0"/&amp;gt;
     *                     &amp;lt;element name="Locations" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                             &amp;lt;sequence&amp;gt;
     *                               &amp;lt;element name="Dropoff" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
     *                               &amp;lt;element name="Pickup" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
     *                               &amp;lt;element name="Stops" minOccurs="0"&amp;gt;
     *                                 &amp;lt;complexType&amp;gt;
     *                                   &amp;lt;complexContent&amp;gt;
     *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                       &amp;lt;sequence&amp;gt;
     *                                         &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
     *                                           &amp;lt;complexType&amp;gt;
     *                                             &amp;lt;complexContent&amp;gt;
     *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
     *                                                 &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                               &amp;lt;/extension&amp;gt;
     *                                             &amp;lt;/complexContent&amp;gt;
     *                                           &amp;lt;/complexType&amp;gt;
     *                                         &amp;lt;/element&amp;gt;
     *                                       &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;/restriction&amp;gt;
     *                                   &amp;lt;/complexContent&amp;gt;
     *                                 &amp;lt;/complexType&amp;gt;
     *                               &amp;lt;/element&amp;gt;
     *                             &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;/restriction&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                     &amp;lt;element name="VehicleSize" type="{http://www.opentravel.org/OTA/2003/05}List_VehSize" minOccurs="0"/&amp;gt;
     *                     &amp;lt;element name="VehicleType" type="{http://www.opentravel.org/OTA/2003/05}List_VehCategory" minOccurs="0"/&amp;gt;
     *                     &amp;lt;element name="Languages" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                             &amp;lt;sequence&amp;gt;
     *                               &amp;lt;element name="Language" maxOccurs="unbounded"&amp;gt;
     *                                 &amp;lt;complexType&amp;gt;
     *                                   &amp;lt;complexContent&amp;gt;
     *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                       &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *                                       &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;/restriction&amp;gt;
     *                                   &amp;lt;/complexContent&amp;gt;
     *                                 &amp;lt;/complexType&amp;gt;
     *                               &amp;lt;/element&amp;gt;
     *                             &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;/restriction&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                   &amp;lt;/sequence&amp;gt;
     *                   &amp;lt;attribute name="ChildCarSeatInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="PetFriendlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="FuelEfficientVehInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="MaximumPassengers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                   &amp;lt;attribute name="MaximumBaggage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                   &amp;lt;attribute name="GreetingSignInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="GreetingSignName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                   &amp;lt;attribute name="MeetAndGreetInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="PersonalGreeterInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="MultilingualInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="ShortDescription"&amp;gt;
     *                     &amp;lt;simpleType&amp;gt;
     *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                         &amp;lt;minLength value="0"/&amp;gt;
     *                         &amp;lt;maxLength value="250"/&amp;gt;
     *                       &amp;lt;/restriction&amp;gt;
     *                     &amp;lt;/simpleType&amp;gt;
     *                   &amp;lt;/attribute&amp;gt;
     *                   &amp;lt;attribute name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                   &amp;lt;attribute name="DisabilityVehicleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                   &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;/restriction&amp;gt;
     *               &amp;lt;/complexContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *           &amp;lt;element name="Shuttle"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;complexContent&amp;gt;
     *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundShuttleType"&amp;gt;
     *                   &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;/extension&amp;gt;
     *               &amp;lt;/complexContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *         &amp;lt;/choice&amp;gt;
     *         &amp;lt;element name="Passenger" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Primary" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Additional" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="AdditionalType" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="CorpDiscountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                           &amp;lt;attribute name="CorpDiscountNmbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="QualificationMethod"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="RT_AirlineTicket"/&amp;gt;
     *                                 &amp;lt;enumeration value="CreditCard"/&amp;gt;
     *                                 &amp;lt;enumeration value="PassportAndReturnTkt"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="RateQualifier" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Payment" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" maxOccurs="unbounded"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "referenceID",
        "service",
        "shuttle",
        "passenger",
        "rateQualifier",
        "payment"
    })
    public static class Reservation {

        @XmlElement(name = "ReferenceID", required = true)
        protected OTAGroundModifyRQ.Reservation.ReferenceID referenceID;
        @XmlElement(name = "Service")
        protected OTAGroundModifyRQ.Reservation.Service service;
        @XmlElement(name = "Shuttle")
        protected OTAGroundModifyRQ.Reservation.Shuttle shuttle;
        @XmlElement(name = "Passenger")
        protected OTAGroundModifyRQ.Reservation.Passenger passenger;
        @XmlElement(name = "RateQualifier")
        protected List<OTAGroundModifyRQ.Reservation.RateQualifier> rateQualifier;
        @XmlElement(name = "Payment")
        protected OTAGroundModifyRQ.Reservation.Payment payment;

        /**
         * Obtiene el valor de la propiedad referenceID.
         * 
         * @return
         *     possible object is
         *     {@link OTAGroundModifyRQ.Reservation.ReferenceID }
         *     
         */
        public OTAGroundModifyRQ.Reservation.ReferenceID getReferenceID() {
            return referenceID;
        }

        /**
         * Define el valor de la propiedad referenceID.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGroundModifyRQ.Reservation.ReferenceID }
         *     
         */
        public void setReferenceID(OTAGroundModifyRQ.Reservation.ReferenceID value) {
            this.referenceID = value;
        }

        /**
         * Obtiene el valor de la propiedad service.
         * 
         * @return
         *     possible object is
         *     {@link OTAGroundModifyRQ.Reservation.Service }
         *     
         */
        public OTAGroundModifyRQ.Reservation.Service getService() {
            return service;
        }

        /**
         * Define el valor de la propiedad service.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGroundModifyRQ.Reservation.Service }
         *     
         */
        public void setService(OTAGroundModifyRQ.Reservation.Service value) {
            this.service = value;
        }

        /**
         * Obtiene el valor de la propiedad shuttle.
         * 
         * @return
         *     possible object is
         *     {@link OTAGroundModifyRQ.Reservation.Shuttle }
         *     
         */
        public OTAGroundModifyRQ.Reservation.Shuttle getShuttle() {
            return shuttle;
        }

        /**
         * Define el valor de la propiedad shuttle.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGroundModifyRQ.Reservation.Shuttle }
         *     
         */
        public void setShuttle(OTAGroundModifyRQ.Reservation.Shuttle value) {
            this.shuttle = value;
        }

        /**
         * Obtiene el valor de la propiedad passenger.
         * 
         * @return
         *     possible object is
         *     {@link OTAGroundModifyRQ.Reservation.Passenger }
         *     
         */
        public OTAGroundModifyRQ.Reservation.Passenger getPassenger() {
            return passenger;
        }

        /**
         * Define el valor de la propiedad passenger.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGroundModifyRQ.Reservation.Passenger }
         *     
         */
        public void setPassenger(OTAGroundModifyRQ.Reservation.Passenger value) {
            this.passenger = value;
        }

        /**
         * Gets the value of the rateQualifier property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rateQualifier property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRateQualifier().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAGroundModifyRQ.Reservation.RateQualifier }
         * 
         * 
         */
        public List<OTAGroundModifyRQ.Reservation.RateQualifier> getRateQualifier() {
            if (rateQualifier == null) {
                rateQualifier = new ArrayList<OTAGroundModifyRQ.Reservation.RateQualifier>();
            }
            return this.rateQualifier;
        }

        /**
         * Obtiene el valor de la propiedad payment.
         * 
         * @return
         *     possible object is
         *     {@link OTAGroundModifyRQ.Reservation.Payment }
         *     
         */
        public OTAGroundModifyRQ.Reservation.Payment getPayment() {
            return payment;
        }

        /**
         * Define el valor de la propiedad payment.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGroundModifyRQ.Reservation.Payment }
         *     
         */
        public void setPayment(OTAGroundModifyRQ.Reservation.Payment value) {
            this.payment = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Primary" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Additional" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="AdditionalType" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="CorpDiscountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                 &amp;lt;attribute name="CorpDiscountNmbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="QualificationMethod"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="RT_AirlineTicket"/&amp;gt;
         *                       &amp;lt;enumeration value="CreditCard"/&amp;gt;
         *                       &amp;lt;enumeration value="PassportAndReturnTkt"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "primary",
            "additional"
        })
        public static class Passenger {

            @XmlElement(name = "Primary")
            protected OTAGroundModifyRQ.Reservation.Passenger.Primary primary;
            @XmlElement(name = "Additional")
            protected List<OTAGroundModifyRQ.Reservation.Passenger.Additional> additional;

            /**
             * Obtiene el valor de la propiedad primary.
             * 
             * @return
             *     possible object is
             *     {@link OTAGroundModifyRQ.Reservation.Passenger.Primary }
             *     
             */
            public OTAGroundModifyRQ.Reservation.Passenger.Primary getPrimary() {
                return primary;
            }

            /**
             * Define el valor de la propiedad primary.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAGroundModifyRQ.Reservation.Passenger.Primary }
             *     
             */
            public void setPrimary(OTAGroundModifyRQ.Reservation.Passenger.Primary value) {
                this.primary = value;
            }

            /**
             * Gets the value of the additional property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additional property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAdditional().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAGroundModifyRQ.Reservation.Passenger.Additional }
             * 
             * 
             */
            public List<OTAGroundModifyRQ.Reservation.Passenger.Additional> getAdditional() {
                if (additional == null) {
                    additional = new ArrayList<OTAGroundModifyRQ.Reservation.Passenger.Additional>();
                }
                return this.additional;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="AdditionalType" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="CorpDiscountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *       &amp;lt;attribute name="CorpDiscountNmbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="QualificationMethod"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="RT_AirlineTicket"/&amp;gt;
             *             &amp;lt;enumeration value="CreditCard"/&amp;gt;
             *             &amp;lt;enumeration value="PassportAndReturnTkt"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "additionalType"
            })
            public static class Additional
                extends ContactPersonType
            {

                @XmlElement(name = "AdditionalType")
                protected OTAGroundModifyRQ.Reservation.Passenger.Additional.AdditionalType additionalType;
                @XmlAttribute(name = "CorpDiscountName")
                protected String corpDiscountName;
                @XmlAttribute(name = "CorpDiscountNmbr")
                protected String corpDiscountNmbr;
                @XmlAttribute(name = "QualificationMethod")
                protected String qualificationMethod;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad additionalType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAGroundModifyRQ.Reservation.Passenger.Additional.AdditionalType }
                 *     
                 */
                public OTAGroundModifyRQ.Reservation.Passenger.Additional.AdditionalType getAdditionalType() {
                    return additionalType;
                }

                /**
                 * Define el valor de la propiedad additionalType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAGroundModifyRQ.Reservation.Passenger.Additional.AdditionalType }
                 *     
                 */
                public void setAdditionalType(OTAGroundModifyRQ.Reservation.Passenger.Additional.AdditionalType value) {
                    this.additionalType = value;
                }

                /**
                 * Obtiene el valor de la propiedad corpDiscountName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCorpDiscountName() {
                    return corpDiscountName;
                }

                /**
                 * Define el valor de la propiedad corpDiscountName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCorpDiscountName(String value) {
                    this.corpDiscountName = value;
                }

                /**
                 * Obtiene el valor de la propiedad corpDiscountNmbr.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCorpDiscountNmbr() {
                    return corpDiscountNmbr;
                }

                /**
                 * Define el valor de la propiedad corpDiscountNmbr.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCorpDiscountNmbr(String value) {
                    this.corpDiscountNmbr = value;
                }

                /**
                 * Obtiene el valor de la propiedad qualificationMethod.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getQualificationMethod() {
                    return qualificationMethod;
                }

                /**
                 * Define el valor de la propiedad qualificationMethod.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setQualificationMethod(String value) {
                    this.qualificationMethod = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerCountGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class AdditionalType {

                    @XmlAttribute(name = "Age")
                    protected Integer age;
                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "CodeContext")
                    protected String codeContext;
                    @XmlAttribute(name = "URI")
                    @XmlSchemaType(name = "anyURI")
                    protected String uri;
                    @XmlAttribute(name = "Quantity")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger quantity;

                    /**
                     * Obtiene el valor de la propiedad age.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getAge() {
                        return age;
                    }

                    /**
                     * Define el valor de la propiedad age.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setAge(Integer value) {
                        this.age = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad codeContext.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCodeContext() {
                        return codeContext;
                    }

                    /**
                     * Define el valor de la propiedad codeContext.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCodeContext(String value) {
                        this.codeContext = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad uri.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getURI() {
                        return uri;
                    }

                    /**
                     * Define el valor de la propiedad uri.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setURI(String value) {
                        this.uri = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad quantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getQuantity() {
                        return quantity;
                    }

                    /**
                     * Define el valor de la propiedad quantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setQuantity(BigInteger value) {
                        this.quantity = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "customerID"
            })
            public static class Primary
                extends ContactPersonType
            {

                @XmlElement(name = "CustomerID")
                protected UniqueIDType customerID;

                /**
                 * Obtiene el valor de la propiedad customerID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link UniqueIDType }
                 *     
                 */
                public UniqueIDType getCustomerID() {
                    return customerID;
                }

                /**
                 * Define el valor de la propiedad customerID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link UniqueIDType }
                 *     
                 */
                public void setCustomerID(UniqueIDType value) {
                    this.customerID = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" maxOccurs="unbounded"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payment"
        })
        public static class Payment {

            @XmlElement(name = "Payment", required = true)
            protected List<PaymentFormType> payment;

            /**
             * Gets the value of the payment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the payment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPayment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PaymentFormType }
             * 
             * 
             */
            public List<PaymentFormType> getPayment() {
                if (payment == null) {
                    payment = new ArrayList<PaymentFormType>();
                }
                return this.payment;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class RateQualifier
            extends GroundRateQualifierType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *       &amp;lt;attribute name="CancelInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ReferenceID
            extends UniqueIDType
        {

            @XmlAttribute(name = "CancelInd")
            protected Boolean cancelInd;

            /**
             * Obtiene el valor de la propiedad cancelInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isCancelInd() {
                return cancelInd;
            }

            /**
             * Define el valor de la propiedad cancelInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setCancelInd(Boolean value) {
                this.cancelInd = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_GroundServiceProvided" minOccurs="0" form="qualified"/&amp;gt;
         *         &amp;lt;element name="ServiceLevel" type="{http://www.opentravel.org/OTA/2003/05}List_LevelOfService" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Locations" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Dropoff" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Pickup" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Stops" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
         *                                     &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="VehicleSize" type="{http://www.opentravel.org/OTA/2003/05}List_VehSize" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="VehicleType" type="{http://www.opentravel.org/OTA/2003/05}List_VehCategory" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Languages" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Language" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
         *                           &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ChildCarSeatInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PetFriendlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FuelEfficientVehInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="MaximumPassengers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="MaximumBaggage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="GreetingSignInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="GreetingSignName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="MeetAndGreetInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PersonalGreeterInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="MultilingualInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ShortDescription"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;minLength value="0"/&amp;gt;
         *             &amp;lt;maxLength value="250"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="DisabilityVehicleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "serviceLevel",
            "locations",
            "vehicleSize",
            "vehicleType",
            "languages"
        })
        public static class Service {

            @XmlElement(name = "Type")
            protected ListGroundServiceProvided type;
            @XmlElement(name = "ServiceLevel")
            protected ListLevelOfService serviceLevel;
            @XmlElement(name = "Locations")
            protected OTAGroundModifyRQ.Reservation.Service.Locations locations;
            @XmlElement(name = "VehicleSize")
            protected ListVehSize vehicleSize;
            @XmlElement(name = "VehicleType")
            protected ListVehCategory vehicleType;
            @XmlElement(name = "Languages")
            protected OTAGroundModifyRQ.Reservation.Service.Languages languages;
            @XmlAttribute(name = "ChildCarSeatInd")
            protected Boolean childCarSeatInd;
            @XmlAttribute(name = "PetFriendlyInd")
            protected Boolean petFriendlyInd;
            @XmlAttribute(name = "FuelEfficientVehInd")
            protected Boolean fuelEfficientVehInd;
            @XmlAttribute(name = "MaximumPassengers")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger maximumPassengers;
            @XmlAttribute(name = "MaximumBaggage")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger maximumBaggage;
            @XmlAttribute(name = "GreetingSignInd")
            protected Boolean greetingSignInd;
            @XmlAttribute(name = "GreetingSignName")
            protected String greetingSignName;
            @XmlAttribute(name = "MeetAndGreetInd")
            protected Boolean meetAndGreetInd;
            @XmlAttribute(name = "PersonalGreeterInd")
            protected Boolean personalGreeterInd;
            @XmlAttribute(name = "MultilingualInd")
            protected Boolean multilingualInd;
            @XmlAttribute(name = "ShortDescription")
            protected String shortDescription;
            @XmlAttribute(name = "Notes")
            protected String notes;
            @XmlAttribute(name = "DisabilityVehicleInd")
            protected Boolean disabilityVehicleInd;
            @XmlAttribute(name = "PassengerRPH")
            protected String passengerRPH;

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link ListGroundServiceProvided }
             *     
             */
            public ListGroundServiceProvided getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link ListGroundServiceProvided }
             *     
             */
            public void setType(ListGroundServiceProvided value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad serviceLevel.
             * 
             * @return
             *     possible object is
             *     {@link ListLevelOfService }
             *     
             */
            public ListLevelOfService getServiceLevel() {
                return serviceLevel;
            }

            /**
             * Define el valor de la propiedad serviceLevel.
             * 
             * @param value
             *     allowed object is
             *     {@link ListLevelOfService }
             *     
             */
            public void setServiceLevel(ListLevelOfService value) {
                this.serviceLevel = value;
            }

            /**
             * Obtiene el valor de la propiedad locations.
             * 
             * @return
             *     possible object is
             *     {@link OTAGroundModifyRQ.Reservation.Service.Locations }
             *     
             */
            public OTAGroundModifyRQ.Reservation.Service.Locations getLocations() {
                return locations;
            }

            /**
             * Define el valor de la propiedad locations.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAGroundModifyRQ.Reservation.Service.Locations }
             *     
             */
            public void setLocations(OTAGroundModifyRQ.Reservation.Service.Locations value) {
                this.locations = value;
            }

            /**
             * Obtiene el valor de la propiedad vehicleSize.
             * 
             * @return
             *     possible object is
             *     {@link ListVehSize }
             *     
             */
            public ListVehSize getVehicleSize() {
                return vehicleSize;
            }

            /**
             * Define el valor de la propiedad vehicleSize.
             * 
             * @param value
             *     allowed object is
             *     {@link ListVehSize }
             *     
             */
            public void setVehicleSize(ListVehSize value) {
                this.vehicleSize = value;
            }

            /**
             * Obtiene el valor de la propiedad vehicleType.
             * 
             * @return
             *     possible object is
             *     {@link ListVehCategory }
             *     
             */
            public ListVehCategory getVehicleType() {
                return vehicleType;
            }

            /**
             * Define el valor de la propiedad vehicleType.
             * 
             * @param value
             *     allowed object is
             *     {@link ListVehCategory }
             *     
             */
            public void setVehicleType(ListVehCategory value) {
                this.vehicleType = value;
            }

            /**
             * Obtiene el valor de la propiedad languages.
             * 
             * @return
             *     possible object is
             *     {@link OTAGroundModifyRQ.Reservation.Service.Languages }
             *     
             */
            public OTAGroundModifyRQ.Reservation.Service.Languages getLanguages() {
                return languages;
            }

            /**
             * Define el valor de la propiedad languages.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAGroundModifyRQ.Reservation.Service.Languages }
             *     
             */
            public void setLanguages(OTAGroundModifyRQ.Reservation.Service.Languages value) {
                this.languages = value;
            }

            /**
             * Obtiene el valor de la propiedad childCarSeatInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isChildCarSeatInd() {
                return childCarSeatInd;
            }

            /**
             * Define el valor de la propiedad childCarSeatInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setChildCarSeatInd(Boolean value) {
                this.childCarSeatInd = value;
            }

            /**
             * Obtiene el valor de la propiedad petFriendlyInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPetFriendlyInd() {
                return petFriendlyInd;
            }

            /**
             * Define el valor de la propiedad petFriendlyInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPetFriendlyInd(Boolean value) {
                this.petFriendlyInd = value;
            }

            /**
             * Obtiene el valor de la propiedad fuelEfficientVehInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFuelEfficientVehInd() {
                return fuelEfficientVehInd;
            }

            /**
             * Define el valor de la propiedad fuelEfficientVehInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFuelEfficientVehInd(Boolean value) {
                this.fuelEfficientVehInd = value;
            }

            /**
             * Obtiene el valor de la propiedad maximumPassengers.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaximumPassengers() {
                return maximumPassengers;
            }

            /**
             * Define el valor de la propiedad maximumPassengers.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaximumPassengers(BigInteger value) {
                this.maximumPassengers = value;
            }

            /**
             * Obtiene el valor de la propiedad maximumBaggage.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaximumBaggage() {
                return maximumBaggage;
            }

            /**
             * Define el valor de la propiedad maximumBaggage.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaximumBaggage(BigInteger value) {
                this.maximumBaggage = value;
            }

            /**
             * Obtiene el valor de la propiedad greetingSignInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isGreetingSignInd() {
                return greetingSignInd;
            }

            /**
             * Define el valor de la propiedad greetingSignInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setGreetingSignInd(Boolean value) {
                this.greetingSignInd = value;
            }

            /**
             * Obtiene el valor de la propiedad greetingSignName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGreetingSignName() {
                return greetingSignName;
            }

            /**
             * Define el valor de la propiedad greetingSignName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGreetingSignName(String value) {
                this.greetingSignName = value;
            }

            /**
             * Obtiene el valor de la propiedad meetAndGreetInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isMeetAndGreetInd() {
                return meetAndGreetInd;
            }

            /**
             * Define el valor de la propiedad meetAndGreetInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setMeetAndGreetInd(Boolean value) {
                this.meetAndGreetInd = value;
            }

            /**
             * Obtiene el valor de la propiedad personalGreeterInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPersonalGreeterInd() {
                return personalGreeterInd;
            }

            /**
             * Define el valor de la propiedad personalGreeterInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPersonalGreeterInd(Boolean value) {
                this.personalGreeterInd = value;
            }

            /**
             * Obtiene el valor de la propiedad multilingualInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isMultilingualInd() {
                return multilingualInd;
            }

            /**
             * Define el valor de la propiedad multilingualInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setMultilingualInd(Boolean value) {
                this.multilingualInd = value;
            }

            /**
             * Obtiene el valor de la propiedad shortDescription.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortDescription() {
                return shortDescription;
            }

            /**
             * Define el valor de la propiedad shortDescription.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortDescription(String value) {
                this.shortDescription = value;
            }

            /**
             * Obtiene el valor de la propiedad notes.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNotes() {
                return notes;
            }

            /**
             * Define el valor de la propiedad notes.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNotes(String value) {
                this.notes = value;
            }

            /**
             * Obtiene el valor de la propiedad disabilityVehicleInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDisabilityVehicleInd() {
                return disabilityVehicleInd;
            }

            /**
             * Define el valor de la propiedad disabilityVehicleInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDisabilityVehicleInd(Boolean value) {
                this.disabilityVehicleInd = value;
            }

            /**
             * Obtiene el valor de la propiedad passengerRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassengerRPH() {
                return passengerRPH;
            }

            /**
             * Define el valor de la propiedad passengerRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassengerRPH(String value) {
                this.passengerRPH = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Language" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
             *                 &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "language"
            })
            public static class Languages {

                @XmlElement(name = "Language", required = true)
                protected List<OTAGroundModifyRQ.Reservation.Service.Languages.Language> language;

                /**
                 * Gets the value of the language property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the language property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getLanguage().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAGroundModifyRQ.Reservation.Service.Languages.Language }
                 * 
                 * 
                 */
                public List<OTAGroundModifyRQ.Reservation.Service.Languages.Language> getLanguage() {
                    if (language == null) {
                        language = new ArrayList<OTAGroundModifyRQ.Reservation.Service.Languages.Language>();
                    }
                    return this.language;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
                 *       &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Language {

                    @XmlAttribute(name = "Language")
                    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                    @XmlSchemaType(name = "language")
                    protected String language;
                    @XmlAttribute(name = "PrimaryLangInd")
                    protected Boolean primaryLangInd;

                    /**
                     * Obtiene el valor de la propiedad language.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLanguage() {
                        return language;
                    }

                    /**
                     * Define el valor de la propiedad language.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLanguage(String value) {
                        this.language = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad primaryLangInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isPrimaryLangInd() {
                        return primaryLangInd;
                    }

                    /**
                     * Define el valor de la propiedad primaryLangInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setPrimaryLangInd(Boolean value) {
                        this.primaryLangInd = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Dropoff" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Pickup" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Stops" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
             *                           &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "dropoff",
                "pickup",
                "stops"
            })
            public static class Locations {

                @XmlElement(name = "Dropoff")
                protected GroundLocationType dropoff;
                @XmlElement(name = "Pickup")
                protected GroundLocationType pickup;
                @XmlElement(name = "Stops")
                protected OTAGroundModifyRQ.Reservation.Service.Locations.Stops stops;

                /**
                 * Obtiene el valor de la propiedad dropoff.
                 * 
                 * @return
                 *     possible object is
                 *     {@link GroundLocationType }
                 *     
                 */
                public GroundLocationType getDropoff() {
                    return dropoff;
                }

                /**
                 * Define el valor de la propiedad dropoff.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link GroundLocationType }
                 *     
                 */
                public void setDropoff(GroundLocationType value) {
                    this.dropoff = value;
                }

                /**
                 * Obtiene el valor de la propiedad pickup.
                 * 
                 * @return
                 *     possible object is
                 *     {@link GroundLocationType }
                 *     
                 */
                public GroundLocationType getPickup() {
                    return pickup;
                }

                /**
                 * Define el valor de la propiedad pickup.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link GroundLocationType }
                 *     
                 */
                public void setPickup(GroundLocationType value) {
                    this.pickup = value;
                }

                /**
                 * Obtiene el valor de la propiedad stops.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAGroundModifyRQ.Reservation.Service.Locations.Stops }
                 *     
                 */
                public OTAGroundModifyRQ.Reservation.Service.Locations.Stops getStops() {
                    return stops;
                }

                /**
                 * Define el valor de la propiedad stops.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAGroundModifyRQ.Reservation.Service.Locations.Stops }
                 *     
                 */
                public void setStops(OTAGroundModifyRQ.Reservation.Service.Locations.Stops value) {
                    this.stops = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
                 *                 &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "stop"
                })
                public static class Stops {

                    @XmlElement(name = "Stop", required = true)
                    protected List<OTAGroundModifyRQ.Reservation.Service.Locations.Stops.Stop> stop;

                    /**
                     * Gets the value of the stop property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stop property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getStop().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAGroundModifyRQ.Reservation.Service.Locations.Stops.Stop }
                     * 
                     * 
                     */
                    public List<OTAGroundModifyRQ.Reservation.Service.Locations.Stops.Stop> getStop() {
                        if (stop == null) {
                            stop = new ArrayList<OTAGroundModifyRQ.Reservation.Service.Locations.Stops.Stop>();
                        }
                        return this.stop;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
                     *       &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Stop
                        extends GroundLocationType
                    {

                        @XmlAttribute(name = "Sequence")
                        @XmlSchemaType(name = "positiveInteger")
                        protected BigInteger sequence;

                        /**
                         * Obtiene el valor de la propiedad sequence.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getSequence() {
                            return sequence;
                        }

                        /**
                         * Define el valor de la propiedad sequence.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setSequence(BigInteger value) {
                            this.sequence = value;
                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundShuttleType"&amp;gt;
         *       &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Shuttle
            extends GroundShuttleType
        {

            @XmlAttribute(name = "PassengerRPH")
            protected String passengerRPH;

            /**
             * Obtiene el valor de la propiedad passengerRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassengerRPH() {
                return passengerRPH;
            }

            /**
             * Define el valor de la propiedad passengerRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassengerRPH(String value) {
                this.passengerRPH = value;
            }

        }

    }

}
