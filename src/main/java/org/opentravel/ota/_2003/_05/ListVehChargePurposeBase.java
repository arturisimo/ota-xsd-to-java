
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_VehChargePurpose_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_VehChargePurpose_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AdditionalDay"/&amp;gt;
 *     &amp;lt;enumeration value="AdditionalDistance"/&amp;gt;
 *     &amp;lt;enumeration value="AdditionalDrive"/&amp;gt;
 *     &amp;lt;enumeration value="AdditionalHour"/&amp;gt;
 *     &amp;lt;enumeration value="AdditionalPassengers"/&amp;gt;
 *     &amp;lt;enumeration value="AdditionalWeek"/&amp;gt;
 *     &amp;lt;enumeration value="Adjustment"/&amp;gt;
 *     &amp;lt;enumeration value="AirConditioningSurcharge"/&amp;gt;
 *     &amp;lt;enumeration value="AirportFee"/&amp;gt;
 *     &amp;lt;enumeration value="BaseRate"/&amp;gt;
 *     &amp;lt;enumeration value="CarSeatFee"/&amp;gt;
 *     &amp;lt;enumeration value="CleaningFee"/&amp;gt;
 *     &amp;lt;enumeration value="ContractFee"/&amp;gt;
 *     &amp;lt;enumeration value="Coverage"/&amp;gt;
 *     &amp;lt;enumeration value="CustomerDropOff"/&amp;gt;
 *     &amp;lt;enumeration value="CustomerPickup"/&amp;gt;
 *     &amp;lt;enumeration value="Discount"/&amp;gt;
 *     &amp;lt;enumeration value="DriverWaitTime"/&amp;gt;
 *     &amp;lt;enumeration value="Drop"/&amp;gt;
 *     &amp;lt;enumeration value="EarlyAM_Fee"/&amp;gt;
 *     &amp;lt;enumeration value="Equipment"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraBaggage"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraGratuity"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraStop"/&amp;gt;
 *     &amp;lt;enumeration value="Fee"/&amp;gt;
 *     &amp;lt;enumeration value="Fuel"/&amp;gt;
 *     &amp;lt;enumeration value="FuelSurcharge"/&amp;gt;
 *     &amp;lt;enumeration value="Gratuity"/&amp;gt;
 *     &amp;lt;enumeration value="Greeter"/&amp;gt;
 *     &amp;lt;enumeration value="HolidaySurcharge"/&amp;gt;
 *     &amp;lt;enumeration value="LatePM_Fee"/&amp;gt;
 *     &amp;lt;enumeration value="Mandatory"/&amp;gt;
 *     &amp;lt;enumeration value="MandatoryChargesTotal"/&amp;gt;
 *     &amp;lt;enumeration value="MeetAndGreet"/&amp;gt;
 *     &amp;lt;enumeration value="Optional"/&amp;gt;
 *     &amp;lt;enumeration value="Parking"/&amp;gt;
 *     &amp;lt;enumeration value="ParkingFees"/&amp;gt;
 *     &amp;lt;enumeration value="PayOnArrivalAmount"/&amp;gt;
 *     &amp;lt;enumeration value="PetSurcharge"/&amp;gt;
 *     &amp;lt;enumeration value="Phone"/&amp;gt;
 *     &amp;lt;enumeration value="PrepaidFuel"/&amp;gt;
 *     &amp;lt;enumeration value="PrepayAmount"/&amp;gt;
 *     &amp;lt;enumeration value="PushCart"/&amp;gt;
 *     &amp;lt;enumeration value="RateOverride"/&amp;gt;
 *     &amp;lt;enumeration value="RegistrationFee"/&amp;gt;
 *     &amp;lt;enumeration value="Representative"/&amp;gt;
 *     &amp;lt;enumeration value="Senior"/&amp;gt;
 *     &amp;lt;enumeration value="SpecialRequest"/&amp;gt;
 *     &amp;lt;enumeration value="StandardGratuity"/&amp;gt;
 *     &amp;lt;enumeration value="Stop"/&amp;gt;
 *     &amp;lt;enumeration value="Subtotal"/&amp;gt;
 *     &amp;lt;enumeration value="Surcharge"/&amp;gt;
 *     &amp;lt;enumeration value="SurfaceTransportationCharge(STC)"/&amp;gt;
 *     &amp;lt;enumeration value="Tax"/&amp;gt;
 *     &amp;lt;enumeration value="Tip"/&amp;gt;
 *     &amp;lt;enumeration value="Tolls"/&amp;gt;
 *     &amp;lt;enumeration value="TravelTimeFee"/&amp;gt;
 *     &amp;lt;enumeration value="VehicleCollection"/&amp;gt;
 *     &amp;lt;enumeration value="VehicleDelivery"/&amp;gt;
 *     &amp;lt;enumeration value="VehicleLicenseFee"/&amp;gt;
 *     &amp;lt;enumeration value="VehicleRental"/&amp;gt;
 *     &amp;lt;enumeration value="WaitTime"/&amp;gt;
 *     &amp;lt;enumeration value="WinterServiceCharge"/&amp;gt;
 *     &amp;lt;enumeration value="YoungDriver"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_VehChargePurpose_Base")
@XmlEnum
public enum ListVehChargePurposeBase {

    @XmlEnumValue("AdditionalDay")
    ADDITIONAL_DAY("AdditionalDay"),
    @XmlEnumValue("AdditionalDistance")
    ADDITIONAL_DISTANCE("AdditionalDistance"),
    @XmlEnumValue("AdditionalDrive")
    ADDITIONAL_DRIVE("AdditionalDrive"),
    @XmlEnumValue("AdditionalHour")
    ADDITIONAL_HOUR("AdditionalHour"),
    @XmlEnumValue("AdditionalPassengers")
    ADDITIONAL_PASSENGERS("AdditionalPassengers"),
    @XmlEnumValue("AdditionalWeek")
    ADDITIONAL_WEEK("AdditionalWeek"),
    @XmlEnumValue("Adjustment")
    ADJUSTMENT("Adjustment"),
    @XmlEnumValue("AirConditioningSurcharge")
    AIR_CONDITIONING_SURCHARGE("AirConditioningSurcharge"),
    @XmlEnumValue("AirportFee")
    AIRPORT_FEE("AirportFee"),
    @XmlEnumValue("BaseRate")
    BASE_RATE("BaseRate"),
    @XmlEnumValue("CarSeatFee")
    CAR_SEAT_FEE("CarSeatFee"),
    @XmlEnumValue("CleaningFee")
    CLEANING_FEE("CleaningFee"),
    @XmlEnumValue("ContractFee")
    CONTRACT_FEE("ContractFee"),
    @XmlEnumValue("Coverage")
    COVERAGE("Coverage"),
    @XmlEnumValue("CustomerDropOff")
    CUSTOMER_DROP_OFF("CustomerDropOff"),
    @XmlEnumValue("CustomerPickup")
    CUSTOMER_PICKUP("CustomerPickup"),
    @XmlEnumValue("Discount")
    DISCOUNT("Discount"),
    @XmlEnumValue("DriverWaitTime")
    DRIVER_WAIT_TIME("DriverWaitTime"),
    @XmlEnumValue("Drop")
    DROP("Drop"),
    @XmlEnumValue("EarlyAM_Fee")
    EARLY_AM_FEE("EarlyAM_Fee"),
    @XmlEnumValue("Equipment")
    EQUIPMENT("Equipment"),
    @XmlEnumValue("ExtraBaggage")
    EXTRA_BAGGAGE("ExtraBaggage"),
    @XmlEnumValue("ExtraGratuity")
    EXTRA_GRATUITY("ExtraGratuity"),
    @XmlEnumValue("ExtraStop")
    EXTRA_STOP("ExtraStop"),
    @XmlEnumValue("Fee")
    FEE("Fee"),
    @XmlEnumValue("Fuel")
    FUEL("Fuel"),
    @XmlEnumValue("FuelSurcharge")
    FUEL_SURCHARGE("FuelSurcharge"),
    @XmlEnumValue("Gratuity")
    GRATUITY("Gratuity"),
    @XmlEnumValue("Greeter")
    GREETER("Greeter"),
    @XmlEnumValue("HolidaySurcharge")
    HOLIDAY_SURCHARGE("HolidaySurcharge"),
    @XmlEnumValue("LatePM_Fee")
    LATE_PM_FEE("LatePM_Fee"),
    @XmlEnumValue("Mandatory")
    MANDATORY("Mandatory"),
    @XmlEnumValue("MandatoryChargesTotal")
    MANDATORY_CHARGES_TOTAL("MandatoryChargesTotal"),
    @XmlEnumValue("MeetAndGreet")
    MEET_AND_GREET("MeetAndGreet"),
    @XmlEnumValue("Optional")
    OPTIONAL("Optional"),
    @XmlEnumValue("Parking")
    PARKING("Parking"),
    @XmlEnumValue("ParkingFees")
    PARKING_FEES("ParkingFees"),
    @XmlEnumValue("PayOnArrivalAmount")
    PAY_ON_ARRIVAL_AMOUNT("PayOnArrivalAmount"),
    @XmlEnumValue("PetSurcharge")
    PET_SURCHARGE("PetSurcharge"),
    @XmlEnumValue("Phone")
    PHONE("Phone"),
    @XmlEnumValue("PrepaidFuel")
    PREPAID_FUEL("PrepaidFuel"),
    @XmlEnumValue("PrepayAmount")
    PREPAY_AMOUNT("PrepayAmount"),
    @XmlEnumValue("PushCart")
    PUSH_CART("PushCart"),
    @XmlEnumValue("RateOverride")
    RATE_OVERRIDE("RateOverride"),
    @XmlEnumValue("RegistrationFee")
    REGISTRATION_FEE("RegistrationFee"),
    @XmlEnumValue("Representative")
    REPRESENTATIVE("Representative"),
    @XmlEnumValue("Senior")
    SENIOR("Senior"),
    @XmlEnumValue("SpecialRequest")
    SPECIAL_REQUEST("SpecialRequest"),
    @XmlEnumValue("StandardGratuity")
    STANDARD_GRATUITY("StandardGratuity"),
    @XmlEnumValue("Stop")
    STOP("Stop"),
    @XmlEnumValue("Subtotal")
    SUBTOTAL("Subtotal"),
    @XmlEnumValue("Surcharge")
    SURCHARGE("Surcharge"),
    @XmlEnumValue("SurfaceTransportationCharge(STC)")
    SURFACE_TRANSPORTATION_CHARGE_STC("SurfaceTransportationCharge(STC)"),
    @XmlEnumValue("Tax")
    TAX("Tax"),
    @XmlEnumValue("Tip")
    TIP("Tip"),
    @XmlEnumValue("Tolls")
    TOLLS("Tolls"),
    @XmlEnumValue("TravelTimeFee")
    TRAVEL_TIME_FEE("TravelTimeFee"),
    @XmlEnumValue("VehicleCollection")
    VEHICLE_COLLECTION("VehicleCollection"),
    @XmlEnumValue("VehicleDelivery")
    VEHICLE_DELIVERY("VehicleDelivery"),
    @XmlEnumValue("VehicleLicenseFee")
    VEHICLE_LICENSE_FEE("VehicleLicenseFee"),
    @XmlEnumValue("VehicleRental")
    VEHICLE_RENTAL("VehicleRental"),
    @XmlEnumValue("WaitTime")
    WAIT_TIME("WaitTime"),
    @XmlEnumValue("WinterServiceCharge")
    WINTER_SERVICE_CHARGE("WinterServiceCharge"),
    @XmlEnumValue("YoungDriver")
    YOUNG_DRIVER("YoungDriver"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListVehChargePurposeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListVehChargePurposeBase fromValue(String v) {
        for (ListVehChargePurposeBase c: ListVehChargePurposeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
