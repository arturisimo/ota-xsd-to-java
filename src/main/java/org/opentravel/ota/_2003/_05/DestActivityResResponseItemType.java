
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * The item-level response to a reservation request.
 * 
 * &lt;p&gt;Clase Java para DestActivityResResponseItemType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DestActivityResResponseItemType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="PickupLocation" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReservationItemReferences"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ReservationItemReference" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="PickupTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DestActivityResResponseItemType", propOrder = {
    "pickupLocation",
    "reservationItemReferences"
})
public class DestActivityResResponseItemType {

    @XmlElement(name = "PickupLocation")
    protected ParagraphType pickupLocation;
    @XmlElement(name = "ReservationItemReferences", required = true)
    protected DestActivityResResponseItemType.ReservationItemReferences reservationItemReferences;
    @XmlAttribute(name = "PickupTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar pickupTime;

    /**
     * Obtiene el valor de la propiedad pickupLocation.
     * 
     * @return
     *     possible object is
     *     {@link ParagraphType }
     *     
     */
    public ParagraphType getPickupLocation() {
        return pickupLocation;
    }

    /**
     * Define el valor de la propiedad pickupLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link ParagraphType }
     *     
     */
    public void setPickupLocation(ParagraphType value) {
        this.pickupLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationItemReferences.
     * 
     * @return
     *     possible object is
     *     {@link DestActivityResResponseItemType.ReservationItemReferences }
     *     
     */
    public DestActivityResResponseItemType.ReservationItemReferences getReservationItemReferences() {
        return reservationItemReferences;
    }

    /**
     * Define el valor de la propiedad reservationItemReferences.
     * 
     * @param value
     *     allowed object is
     *     {@link DestActivityResResponseItemType.ReservationItemReferences }
     *     
     */
    public void setReservationItemReferences(DestActivityResResponseItemType.ReservationItemReferences value) {
        this.reservationItemReferences = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPickupTime() {
        return pickupTime;
    }

    /**
     * Define el valor de la propiedad pickupTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPickupTime(XMLGregorianCalendar value) {
        this.pickupTime = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ReservationItemReference" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reservationItemReference"
    })
    public static class ReservationItemReferences {

        @XmlElement(name = "ReservationItemReference", required = true)
        protected List<UniqueIDType> reservationItemReference;

        /**
         * Gets the value of the reservationItemReference property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reservationItemReference property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getReservationItemReference().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link UniqueIDType }
         * 
         * 
         */
        public List<UniqueIDType> getReservationItemReference() {
            if (reservationItemReference == null) {
                reservationItemReference = new ArrayList<UniqueIDType>();
            }
            return this.reservationItemReference;
        }

    }

}
