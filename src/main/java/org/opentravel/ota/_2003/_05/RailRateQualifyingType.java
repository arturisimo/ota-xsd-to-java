
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines rate qualifying information such as travel purpose, promotion codes and discount types that may affect the fare.
 * 
 * &lt;p&gt;Clase Java para RailRateQualifyingType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RailRateQualifyingType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="TravelPurpose" type="{http://www.opentravel.org/OTA/2003/05}TravelPurposeType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DiscountType" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CodeGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailRateQualifyingType", propOrder = {
    "travelPurpose",
    "discountType"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTARailShopRQ.RateQualifier.class
})
public class RailRateQualifyingType {

    @XmlElement(name = "TravelPurpose")
    protected TravelPurposeType travelPurpose;
    @XmlElement(name = "DiscountType")
    protected RailRateQualifyingType.DiscountType discountType;
    @XmlAttribute(name = "PromotionCode")
    protected String promotionCode;

    /**
     * Obtiene el valor de la propiedad travelPurpose.
     * 
     * @return
     *     possible object is
     *     {@link TravelPurposeType }
     *     
     */
    public TravelPurposeType getTravelPurpose() {
        return travelPurpose;
    }

    /**
     * Define el valor de la propiedad travelPurpose.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelPurposeType }
     *     
     */
    public void setTravelPurpose(TravelPurposeType value) {
        this.travelPurpose = value;
    }

    /**
     * Obtiene el valor de la propiedad discountType.
     * 
     * @return
     *     possible object is
     *     {@link RailRateQualifyingType.DiscountType }
     *     
     */
    public RailRateQualifyingType.DiscountType getDiscountType() {
        return discountType;
    }

    /**
     * Define el valor de la propiedad discountType.
     * 
     * @param value
     *     allowed object is
     *     {@link RailRateQualifyingType.DiscountType }
     *     
     */
    public void setDiscountType(RailRateQualifyingType.DiscountType value) {
        this.discountType = value;
    }

    /**
     * Obtiene el valor de la propiedad promotionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     * Define el valor de la propiedad promotionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionCode(String value) {
        this.promotionCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CodeGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DiscountType {

        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

    }

}
