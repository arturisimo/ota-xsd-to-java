
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para Enum_SeatMapOperations.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="Enum_SeatMapOperations"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="GetSeatAssignment"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatMap"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatMapForPNR"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatMapWithRules"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatPurchases"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatMapForSeatType"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatZones"/&amp;gt;
 *     &amp;lt;enumeration value="GetSeatMapForUpgrade"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "Enum_SeatMapOperations")
@XmlEnum
public enum EnumSeatMapOperations {


    /**
     * Get a seat map for one or more already assigned seats. [Flight, PNR, Traveler]
     * 
     */
    @XmlEnumValue("GetSeatAssignment")
    GET_SEAT_ASSIGNMENT("GetSeatAssignment"),

    /**
     * Get a seat map for the specified criteria [Flight, PNR, Traveler].
     * 
     */
    @XmlEnumValue("GetSeatMap")
    GET_SEAT_MAP("GetSeatMap"),

    /**
     * Get a seat map fpr a specified passenger name record. [Multiple crafts, multi flight segments]
     * 
     */
    @XmlEnumValue("GetSeatMapForPNR")
    GET_SEAT_MAP_FOR_PNR("GetSeatMapForPNR"),

    /**
     * Get a seat map that includes fare, upgrade and other associated rules for each seat.
     * 
     */
    @XmlEnumValue("GetSeatMapWithRules")
    GET_SEAT_MAP_WITH_RULES("GetSeatMapWithRules"),

    /**
     * Get a list of purchased seats for travelers and/or by aircraft.
     * 
     */
    @XmlEnumValue("GetSeatPurchases")
    GET_SEAT_PURCHASES("GetSeatPurchases"),

    /**
     * Get a seat map for a specified seat type and/or traveler special requirements.
     * 
     */
    @XmlEnumValue("GetSeatMapForSeatType")
    GET_SEAT_MAP_FOR_SEAT_TYPE("GetSeatMapForSeatType"),

    /**
     * Get a seat map organized by one or more seating zones.
     * 
     */
    @XmlEnumValue("GetSeatZones")
    GET_SEAT_ZONES("GetSeatZones"),

    /**
     * Get a seat map for an upgrade (which may be voluntary or in-voluntary.
     * 
     */
    @XmlEnumValue("GetSeatMapForUpgrade")
    GET_SEAT_MAP_FOR_UPGRADE("GetSeatMapForUpgrade"),

    /**
     * It is strongly recommended that you submit a comment to have any of your extended list values permanently added to the OpenTravel specification to support maximum trading partner interoperability. http://www.opentraveldevelopersnetwork.com/specificationcomments/2/entercomment.html
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    EnumSeatMapOperations(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumSeatMapOperations fromValue(String v) {
        for (EnumSeatMapOperations c: EnumSeatMapOperations.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
