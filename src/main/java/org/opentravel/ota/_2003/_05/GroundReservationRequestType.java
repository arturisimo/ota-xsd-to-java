
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * A unique reservation ID and/or search criteria used to return existing reservation details.
 * 
 * &lt;p&gt;Clase Java para GroundReservationRequestType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundReservationRequestType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Confirmation" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
 *         &amp;lt;element name="Passengers" type="{http://www.opentravel.org/OTA/2003/05}GroundPrimaryAdditionalPassengerType"/&amp;gt;
 *         &amp;lt;element name="Locations" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationsType"/&amp;gt;
 *         &amp;lt;element name="VehicleMakeModel" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleMakeModelGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RateQualifier" type="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ServiceCharges" type="{http://www.opentravel.org/OTA/2003/05}GroundServiceChargesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}GroundFeesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TotalCharge" type="{http://www.opentravel.org/OTA/2003/05}GroundTotalChargeType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Restrictions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
 *                           &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PaymentRules" type="{http://www.opentravel.org/OTA/2003/05}PaymentRulesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundReservationRequestType", propOrder = {
    "confirmation",
    "passengers",
    "locations",
    "vehicleMakeModel",
    "rateQualifier",
    "serviceCharges",
    "fees",
    "totalCharge",
    "restrictions",
    "paymentRules",
    "comments"
})
public class GroundReservationRequestType {

    @XmlElement(name = "Confirmation", required = true)
    protected UniqueIDType confirmation;
    @XmlElement(name = "Passengers", required = true)
    protected GroundPrimaryAdditionalPassengerType passengers;
    @XmlElement(name = "Locations", required = true)
    protected GroundLocationsType locations;
    @XmlElement(name = "VehicleMakeModel")
    protected GroundReservationRequestType.VehicleMakeModel vehicleMakeModel;
    @XmlElement(name = "RateQualifier")
    protected GroundRateQualifierType rateQualifier;
    @XmlElement(name = "ServiceCharges")
    protected GroundServiceChargesType serviceCharges;
    @XmlElement(name = "Fees")
    protected GroundFeesType fees;
    @XmlElement(name = "TotalCharge")
    protected GroundTotalChargeType totalCharge;
    @XmlElement(name = "Restrictions")
    protected GroundReservationRequestType.Restrictions restrictions;
    @XmlElement(name = "PaymentRules")
    protected PaymentRulesType paymentRules;
    @XmlElement(name = "Comments")
    protected List<FreeTextType> comments;

    /**
     * Obtiene el valor de la propiedad confirmation.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getConfirmation() {
        return confirmation;
    }

    /**
     * Define el valor de la propiedad confirmation.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setConfirmation(UniqueIDType value) {
        this.confirmation = value;
    }

    /**
     * Obtiene el valor de la propiedad passengers.
     * 
     * @return
     *     possible object is
     *     {@link GroundPrimaryAdditionalPassengerType }
     *     
     */
    public GroundPrimaryAdditionalPassengerType getPassengers() {
        return passengers;
    }

    /**
     * Define el valor de la propiedad passengers.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundPrimaryAdditionalPassengerType }
     *     
     */
    public void setPassengers(GroundPrimaryAdditionalPassengerType value) {
        this.passengers = value;
    }

    /**
     * Obtiene el valor de la propiedad locations.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationsType }
     *     
     */
    public GroundLocationsType getLocations() {
        return locations;
    }

    /**
     * Define el valor de la propiedad locations.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationsType }
     *     
     */
    public void setLocations(GroundLocationsType value) {
        this.locations = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleMakeModel.
     * 
     * @return
     *     possible object is
     *     {@link GroundReservationRequestType.VehicleMakeModel }
     *     
     */
    public GroundReservationRequestType.VehicleMakeModel getVehicleMakeModel() {
        return vehicleMakeModel;
    }

    /**
     * Define el valor de la propiedad vehicleMakeModel.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundReservationRequestType.VehicleMakeModel }
     *     
     */
    public void setVehicleMakeModel(GroundReservationRequestType.VehicleMakeModel value) {
        this.vehicleMakeModel = value;
    }

    /**
     * Obtiene el valor de la propiedad rateQualifier.
     * 
     * @return
     *     possible object is
     *     {@link GroundRateQualifierType }
     *     
     */
    public GroundRateQualifierType getRateQualifier() {
        return rateQualifier;
    }

    /**
     * Define el valor de la propiedad rateQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundRateQualifierType }
     *     
     */
    public void setRateQualifier(GroundRateQualifierType value) {
        this.rateQualifier = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceCharges.
     * 
     * @return
     *     possible object is
     *     {@link GroundServiceChargesType }
     *     
     */
    public GroundServiceChargesType getServiceCharges() {
        return serviceCharges;
    }

    /**
     * Define el valor de la propiedad serviceCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundServiceChargesType }
     *     
     */
    public void setServiceCharges(GroundServiceChargesType value) {
        this.serviceCharges = value;
    }

    /**
     * Obtiene el valor de la propiedad fees.
     * 
     * @return
     *     possible object is
     *     {@link GroundFeesType }
     *     
     */
    public GroundFeesType getFees() {
        return fees;
    }

    /**
     * Define el valor de la propiedad fees.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundFeesType }
     *     
     */
    public void setFees(GroundFeesType value) {
        this.fees = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCharge.
     * 
     * @return
     *     possible object is
     *     {@link GroundTotalChargeType }
     *     
     */
    public GroundTotalChargeType getTotalCharge() {
        return totalCharge;
    }

    /**
     * Define el valor de la propiedad totalCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundTotalChargeType }
     *     
     */
    public void setTotalCharge(GroundTotalChargeType value) {
        this.totalCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad restrictions.
     * 
     * @return
     *     possible object is
     *     {@link GroundReservationRequestType.Restrictions }
     *     
     */
    public GroundReservationRequestType.Restrictions getRestrictions() {
        return restrictions;
    }

    /**
     * Define el valor de la propiedad restrictions.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundReservationRequestType.Restrictions }
     *     
     */
    public void setRestrictions(GroundReservationRequestType.Restrictions value) {
        this.restrictions = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentRules.
     * 
     * @return
     *     possible object is
     *     {@link PaymentRulesType }
     *     
     */
    public PaymentRulesType getPaymentRules() {
        return paymentRules;
    }

    /**
     * Define el valor de la propiedad paymentRules.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentRulesType }
     *     
     */
    public void setPaymentRules(PaymentRulesType value) {
        this.paymentRules = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comments property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getComments().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FreeTextType }
     * 
     * 
     */
    public List<FreeTextType> getComments() {
        if (comments == null) {
            comments = new ArrayList<FreeTextType>();
        }
        return this.comments;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
     *                 &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "advanceBooking"
    })
    public static class Restrictions {

        @XmlElement(name = "AdvanceBooking")
        protected GroundReservationRequestType.Restrictions.AdvanceBooking advanceBooking;
        @XmlAttribute(name = "AdvancedBookingInd")
        protected Boolean advancedBookingInd;
        @XmlAttribute(name = "CorporateRateInd")
        protected Boolean corporateRateInd;
        @XmlAttribute(name = "GuaranteeReqInd")
        protected Boolean guaranteeReqInd;
        @XmlAttribute(name = "CancellationPenaltyInd")
        protected Boolean cancellationPenaltyInd;
        @XmlAttribute(name = "ModificationPenaltyInd")
        protected Boolean modificationPenaltyInd;

        /**
         * Obtiene el valor de la propiedad advanceBooking.
         * 
         * @return
         *     possible object is
         *     {@link GroundReservationRequestType.Restrictions.AdvanceBooking }
         *     
         */
        public GroundReservationRequestType.Restrictions.AdvanceBooking getAdvanceBooking() {
            return advanceBooking;
        }

        /**
         * Define el valor de la propiedad advanceBooking.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundReservationRequestType.Restrictions.AdvanceBooking }
         *     
         */
        public void setAdvanceBooking(GroundReservationRequestType.Restrictions.AdvanceBooking value) {
            this.advanceBooking = value;
        }

        /**
         * Obtiene el valor de la propiedad advancedBookingInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAdvancedBookingInd() {
            return advancedBookingInd;
        }

        /**
         * Define el valor de la propiedad advancedBookingInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAdvancedBookingInd(Boolean value) {
            this.advancedBookingInd = value;
        }

        /**
         * Obtiene el valor de la propiedad corporateRateInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCorporateRateInd() {
            return corporateRateInd;
        }

        /**
         * Define el valor de la propiedad corporateRateInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCorporateRateInd(Boolean value) {
            this.corporateRateInd = value;
        }

        /**
         * Obtiene el valor de la propiedad guaranteeReqInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteeReqInd() {
            return guaranteeReqInd;
        }

        /**
         * Define el valor de la propiedad guaranteeReqInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteeReqInd(Boolean value) {
            this.guaranteeReqInd = value;
        }

        /**
         * Obtiene el valor de la propiedad cancellationPenaltyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCancellationPenaltyInd() {
            return cancellationPenaltyInd;
        }

        /**
         * Define el valor de la propiedad cancellationPenaltyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCancellationPenaltyInd(Boolean value) {
            this.cancellationPenaltyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad modificationPenaltyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isModificationPenaltyInd() {
            return modificationPenaltyInd;
        }

        /**
         * Define el valor de la propiedad modificationPenaltyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setModificationPenaltyInd(Boolean value) {
            this.modificationPenaltyInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
         *       &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AdvanceBooking {

            @XmlAttribute(name = "RequiredInd")
            protected Boolean requiredInd;
            @XmlAttribute(name = "AbsoluteDeadline")
            protected String absoluteDeadline;
            @XmlAttribute(name = "OffsetTimeUnit")
            protected TimeUnitType offsetTimeUnit;
            @XmlAttribute(name = "OffsetUnitMultiplier")
            protected Integer offsetUnitMultiplier;
            @XmlAttribute(name = "OffsetDropTime")
            protected String offsetDropTime;

            /**
             * Obtiene el valor de la propiedad requiredInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRequiredInd() {
                return requiredInd;
            }

            /**
             * Define el valor de la propiedad requiredInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRequiredInd(Boolean value) {
                this.requiredInd = value;
            }

            /**
             * Obtiene el valor de la propiedad absoluteDeadline.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAbsoluteDeadline() {
                return absoluteDeadline;
            }

            /**
             * Define el valor de la propiedad absoluteDeadline.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAbsoluteDeadline(String value) {
                this.absoluteDeadline = value;
            }

            /**
             * Obtiene el valor de la propiedad offsetTimeUnit.
             * 
             * @return
             *     possible object is
             *     {@link TimeUnitType }
             *     
             */
            public TimeUnitType getOffsetTimeUnit() {
                return offsetTimeUnit;
            }

            /**
             * Define el valor de la propiedad offsetTimeUnit.
             * 
             * @param value
             *     allowed object is
             *     {@link TimeUnitType }
             *     
             */
            public void setOffsetTimeUnit(TimeUnitType value) {
                this.offsetTimeUnit = value;
            }

            /**
             * Obtiene el valor de la propiedad offsetUnitMultiplier.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getOffsetUnitMultiplier() {
                return offsetUnitMultiplier;
            }

            /**
             * Define el valor de la propiedad offsetUnitMultiplier.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setOffsetUnitMultiplier(Integer value) {
                this.offsetUnitMultiplier = value;
            }

            /**
             * Obtiene el valor de la propiedad offsetDropTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOffsetDropTime() {
                return offsetDropTime;
            }

            /**
             * Define el valor de la propiedad offsetDropTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOffsetDropTime(String value) {
                this.offsetDropTime = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleMakeModelGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class VehicleMakeModel {

        @XmlAttribute(name = "Name", required = true)
        protected String name;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "ModelYear")
        @XmlSchemaType(name = "gYear")
        protected XMLGregorianCalendar modelYear;

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad modelYear.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getModelYear() {
            return modelYear;
        }

        /**
         * Define el valor de la propiedad modelYear.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setModelYear(XMLGregorianCalendar value) {
            this.modelYear = value;
        }

    }

}
