
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Physically challenged accessibility preferences.
 * 
 * &lt;p&gt;Clase Java para TourActivityAccessibilityReqType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityAccessibilityReqType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="MobilityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="SightInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityAccessibilityReqType")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTATourActivityBookRQ.BookingInfo.AccessibilityRequirement.class
})
public class TourActivityAccessibilityReqType {

    @XmlAttribute(name = "MobilityInd")
    protected Boolean mobilityInd;
    @XmlAttribute(name = "SightInd")
    protected Boolean sightInd;

    /**
     * Obtiene el valor de la propiedad mobilityInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMobilityInd() {
        return mobilityInd;
    }

    /**
     * Define el valor de la propiedad mobilityInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMobilityInd(Boolean value) {
        this.mobilityInd = value;
    }

    /**
     * Obtiene el valor de la propiedad sightInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSightInd() {
        return sightInd;
    }

    /**
     * Define el valor de la propiedad sightInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSightInd(Boolean value) {
        this.sightInd = value;
    }

}
