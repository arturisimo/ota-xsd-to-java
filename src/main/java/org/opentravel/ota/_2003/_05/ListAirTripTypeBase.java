
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_AirTripType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_AirTripType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Circle"/&amp;gt;
 *     &amp;lt;enumeration value="Inbound"/&amp;gt;
 *     &amp;lt;enumeration value="NonDirectional"/&amp;gt;
 *     &amp;lt;enumeration value="OneWay"/&amp;gt;
 *     &amp;lt;enumeration value="OneWayOnly"/&amp;gt;
 *     &amp;lt;enumeration value="OpenJaw"/&amp;gt;
 *     &amp;lt;enumeration value="Outbound"/&amp;gt;
 *     &amp;lt;enumeration value="OutboundSeasonRoundtrip"/&amp;gt;
 *     &amp;lt;enumeration value="Return"/&amp;gt;
 *     &amp;lt;enumeration value="Roundtrip"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_AirTripType_Base")
@XmlEnum
public enum ListAirTripTypeBase {


    /**
     * Identifies a circle trip type.
     * 
     */
    @XmlEnumValue("Circle")
    CIRCLE("Circle"),

    /**
     * The direction for the fare is inbound.
     * 
     */
    @XmlEnumValue("Inbound")
    INBOUND("Inbound"),

    /**
     * There is no direction specified for the fare.
     * 
     */
    @XmlEnumValue("NonDirectional")
    NON_DIRECTIONAL("NonDirectional"),

    /**
     * Identifies a one way trip type.
     * 
     */
    @XmlEnumValue("OneWay")
    ONE_WAY("OneWay"),

    /**
     * Cannot be doubled to create a roundtrip.
     * 
     */
    @XmlEnumValue("OneWayOnly")
    ONE_WAY_ONLY("OneWayOnly"),

    /**
     * Identifies an open jaw trip type.
     * 
     */
    @XmlEnumValue("OpenJaw")
    OPEN_JAW("OpenJaw"),

    /**
     * The direction for the fare is outbound.
     * 
     */
    @XmlEnumValue("Outbound")
    OUTBOUND("Outbound"),

    /**
     * The direction for the fare is outbound seasonal roundtrip.
     * 
     */
    @XmlEnumValue("OutboundSeasonRoundtrip")
    OUTBOUND_SEASON_ROUNDTRIP("OutboundSeasonRoundtrip"),

    /**
     * Identifies a return trip type.
     * 
     */
    @XmlEnumValue("Return")
    RETURN("Return"),

    /**
     * Identifies travel from one point to another point and return to the original point. (The outbound fare shall be used also for the inbound fare component for the purpose of determining if the pricing unit is a round trip).
     * 
     */
    @XmlEnumValue("Roundtrip")
    ROUNDTRIP("Roundtrip"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListAirTripTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListAirTripTypeBase fromValue(String v) {
        for (ListAirTripTypeBase c: ListAirTripTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
