
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Use: This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" enumeration in combination with the Code Extension fields to exchange a value that is not in the list and is known to your trading partners.
 * 
 * &lt;p&gt;Clase Java para List_LoyaltyPrgCurrency complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="List_LoyaltyPrgCurrency"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="List" type="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency_Base"/&amp;gt;
 *         &amp;lt;element name="Extension" type="{http://www.opentravel.org/OTA/2003/05}CodeListSummaryExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "List_LoyaltyPrgCurrency", propOrder = {
    "list",
    "extension"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.CodeListFeeType.Amount.AlternateCurrency.class,
    org.opentravel.ota._2003._05.CodeListFeeType.Taxes.Amount.AlternateCurrency.class
})
public class ListLoyaltyPrgCurrency {

    @XmlElement(name = "List", required = true)
    @XmlSchemaType(name = "string")
    protected ListLoyaltyPrgCurrencyBase list;
    @XmlElement(name = "Extension")
    protected CodeListSummaryExtension extension;

    /**
     * Obtiene el valor de la propiedad list.
     * 
     * @return
     *     possible object is
     *     {@link ListLoyaltyPrgCurrencyBase }
     *     
     */
    public ListLoyaltyPrgCurrencyBase getList() {
        return list;
    }

    /**
     * Define el valor de la propiedad list.
     * 
     * @param value
     *     allowed object is
     *     {@link ListLoyaltyPrgCurrencyBase }
     *     
     */
    public void setList(ListLoyaltyPrgCurrencyBase value) {
        this.list = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link CodeListSummaryExtension }
     *     
     */
    public CodeListSummaryExtension getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeListSummaryExtension }
     *     
     */
    public void setExtension(CodeListSummaryExtension value) {
        this.extension = value;
    }

}
