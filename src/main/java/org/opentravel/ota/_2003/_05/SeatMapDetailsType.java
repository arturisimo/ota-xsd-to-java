
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Details of a seat map for a particular aircraft
 * 
 * &lt;p&gt;Clase Java para SeatMapDetailsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="SeatMapDetailsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CabinClass" maxOccurs="5"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CabinClassDetailType"&amp;gt;
 *                 &amp;lt;attribute name="StartingRow" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
 *                 &amp;lt;attribute name="EndingRow" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="TravelerRefNumberRPHs" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeatMapDetailsType", propOrder = {
    "cabinClass"
})
public class SeatMapDetailsType {

    @XmlElement(name = "CabinClass", required = true)
    protected List<SeatMapDetailsType.CabinClass> cabinClass;
    @XmlAttribute(name = "TravelerRefNumberRPHs")
    protected List<String> travelerRefNumberRPHs;

    /**
     * Gets the value of the cabinClass property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cabinClass property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCabinClass().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SeatMapDetailsType.CabinClass }
     * 
     * 
     */
    public List<SeatMapDetailsType.CabinClass> getCabinClass() {
        if (cabinClass == null) {
            cabinClass = new ArrayList<SeatMapDetailsType.CabinClass>();
        }
        return this.cabinClass;
    }

    /**
     * Gets the value of the travelerRefNumberRPHs property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelerRefNumberRPHs property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTravelerRefNumberRPHs().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTravelerRefNumberRPHs() {
        if (travelerRefNumberRPHs == null) {
            travelerRefNumberRPHs = new ArrayList<String>();
        }
        return this.travelerRefNumberRPHs;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CabinClassDetailType"&amp;gt;
     *       &amp;lt;attribute name="StartingRow" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
     *       &amp;lt;attribute name="EndingRow" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CabinClass
        extends CabinClassDetailType
    {

        @XmlAttribute(name = "StartingRow")
        protected String startingRow;
        @XmlAttribute(name = "EndingRow")
        protected String endingRow;

        /**
         * Obtiene el valor de la propiedad startingRow.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartingRow() {
            return startingRow;
        }

        /**
         * Define el valor de la propiedad startingRow.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartingRow(String value) {
            this.startingRow = value;
        }

        /**
         * Obtiene el valor de la propiedad endingRow.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndingRow() {
            return endingRow;
        }

        /**
         * Define el valor de la propiedad endingRow.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndingRow(String value) {
            this.endingRow = value;
        }

    }

}
