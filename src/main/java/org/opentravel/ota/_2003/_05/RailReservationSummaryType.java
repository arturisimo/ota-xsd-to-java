
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Defines the summary data that identifies a reservation. This information also helps in selecting a specific reservation.
 * 
 * &lt;p&gt;Clase Java para RailReservationSummaryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RailReservationSummaryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ODInfo" maxOccurs="9"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                   &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                   &amp;lt;element name="TrainSegment" maxOccurs="9"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="DepartureStation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                             &amp;lt;element name="ArrivalStation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                             &amp;lt;element name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
 *                             &amp;lt;element name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
 *                             &amp;lt;element name="TrainIdentification" type="{http://www.opentravel.org/OTA/2003/05}TrainIdentificationType"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PassengerInfo" type="{http://www.opentravel.org/OTA/2003/05}RailPassengerCategoryDetailType" maxOccurs="99"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="BookingReferenceID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="DateBooked" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *       &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}TransactionStatusType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailReservationSummaryType", propOrder = {
    "odInfo",
    "passengerInfo",
    "tpaExtensions"
})
public class RailReservationSummaryType {

    @XmlElement(name = "ODInfo", required = true)
    protected List<RailReservationSummaryType.ODInfo> odInfo;
    @XmlElement(name = "PassengerInfo", required = true)
    protected List<RailPassengerCategoryDetailType> passengerInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "BookingReferenceID")
    protected String bookingReferenceID;
    @XmlAttribute(name = "DateBooked")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateBooked;
    @XmlAttribute(name = "Status")
    protected TransactionStatusType status;

    /**
     * Gets the value of the odInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the odInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getODInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RailReservationSummaryType.ODInfo }
     * 
     * 
     */
    public List<RailReservationSummaryType.ODInfo> getODInfo() {
        if (odInfo == null) {
            odInfo = new ArrayList<RailReservationSummaryType.ODInfo>();
        }
        return this.odInfo;
    }

    /**
     * Gets the value of the passengerInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPassengerInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RailPassengerCategoryDetailType }
     * 
     * 
     */
    public List<RailPassengerCategoryDetailType> getPassengerInfo() {
        if (passengerInfo == null) {
            passengerInfo = new ArrayList<RailPassengerCategoryDetailType>();
        }
        return this.passengerInfo;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingReferenceID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingReferenceID() {
        return bookingReferenceID;
    }

    /**
     * Define el valor de la propiedad bookingReferenceID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingReferenceID(String value) {
        this.bookingReferenceID = value;
    }

    /**
     * Obtiene el valor de la propiedad dateBooked.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateBooked() {
        return dateBooked;
    }

    /**
     * Define el valor de la propiedad dateBooked.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateBooked(XMLGregorianCalendar value) {
        this.dateBooked = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusType }
     *     
     */
    public TransactionStatusType getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusType }
     *     
     */
    public void setStatus(TransactionStatusType value) {
        this.status = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *         &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *         &amp;lt;element name="TrainSegment" maxOccurs="9"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="DepartureStation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *                   &amp;lt;element name="ArrivalStation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *                   &amp;lt;element name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
     *                   &amp;lt;element name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
     *                   &amp;lt;element name="TrainIdentification" type="{http://www.opentravel.org/OTA/2003/05}TrainIdentificationType"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originLocation",
        "destinationLocation",
        "trainSegment"
    })
    public static class ODInfo {

        @XmlElement(name = "OriginLocation", required = true)
        protected LocationType originLocation;
        @XmlElement(name = "DestinationLocation", required = true)
        protected LocationType destinationLocation;
        @XmlElement(name = "TrainSegment", required = true)
        protected List<RailReservationSummaryType.ODInfo.TrainSegment> trainSegment;

        /**
         * Obtiene el valor de la propiedad originLocation.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getOriginLocation() {
            return originLocation;
        }

        /**
         * Define el valor de la propiedad originLocation.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setOriginLocation(LocationType value) {
            this.originLocation = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationLocation.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getDestinationLocation() {
            return destinationLocation;
        }

        /**
         * Define el valor de la propiedad destinationLocation.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setDestinationLocation(LocationType value) {
            this.destinationLocation = value;
        }

        /**
         * Gets the value of the trainSegment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the trainSegment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTrainSegment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RailReservationSummaryType.ODInfo.TrainSegment }
         * 
         * 
         */
        public List<RailReservationSummaryType.ODInfo.TrainSegment> getTrainSegment() {
            if (trainSegment == null) {
                trainSegment = new ArrayList<RailReservationSummaryType.ODInfo.TrainSegment>();
            }
            return this.trainSegment;
        }


        /**
         * Defines train segment summary information, including departure and arrival stations, departure and arrival dates/times, and train identification information.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="DepartureStation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
         *         &amp;lt;element name="ArrivalStation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
         *         &amp;lt;element name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
         *         &amp;lt;element name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
         *         &amp;lt;element name="TrainIdentification" type="{http://www.opentravel.org/OTA/2003/05}TrainIdentificationType"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "departureStation",
            "arrivalStation",
            "departureDateTime",
            "arrivalDateTime",
            "trainIdentification"
        })
        public static class TrainSegment {

            @XmlElement(name = "DepartureStation", required = true)
            protected LocationType departureStation;
            @XmlElement(name = "ArrivalStation", required = true)
            protected LocationType arrivalStation;
            @XmlElement(name = "DepartureDateTime", required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar departureDateTime;
            @XmlElement(name = "ArrivalDateTime", required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar arrivalDateTime;
            @XmlElement(name = "TrainIdentification", required = true)
            protected TrainIdentificationType trainIdentification;

            /**
             * Obtiene el valor de la propiedad departureStation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDepartureStation() {
                return departureStation;
            }

            /**
             * Define el valor de la propiedad departureStation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDepartureStation(LocationType value) {
                this.departureStation = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalStation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getArrivalStation() {
                return arrivalStation;
            }

            /**
             * Define el valor de la propiedad arrivalStation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setArrivalStation(LocationType value) {
                this.arrivalStation = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDepartureDateTime() {
                return departureDateTime;
            }

            /**
             * Define el valor de la propiedad departureDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDepartureDateTime(XMLGregorianCalendar value) {
                this.departureDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getArrivalDateTime() {
                return arrivalDateTime;
            }

            /**
             * Define el valor de la propiedad arrivalDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setArrivalDateTime(XMLGregorianCalendar value) {
                this.arrivalDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad trainIdentification.
             * 
             * @return
             *     possible object is
             *     {@link TrainIdentificationType }
             *     
             */
            public TrainIdentificationType getTrainIdentification() {
                return trainIdentification;
            }

            /**
             * Define el valor de la propiedad trainIdentification.
             * 
             * @param value
             *     allowed object is
             *     {@link TrainIdentificationType }
             *     
             */
            public void setTrainIdentification(TrainIdentificationType value) {
                this.trainIdentification = value;
            }

        }

    }

}
