
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type"/&amp;gt;
 *         &amp;lt;element name="LocationDetails"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="LocationDetail" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ProcessingInfo"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProcessingInfoGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Address" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressInfoType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Position" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
 *                                               &amp;lt;attribute name="AccuracyLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                               &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Telephone" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneAttributesGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="MovePhoneNbrToPNR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Policies" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Policy" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                               &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                               &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Keywords" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Keyword" maxOccurs="30"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                               &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
 *                                               &amp;lt;attribute name="AttachToKeyword" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                               &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="OperationSchedules" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType"&amp;gt;
 *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="SpecialEquipments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="SpecialEquipment" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType" minOccurs="0"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="GDS_TypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                               &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                               &amp;lt;attribute name="FreeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;attribute name="ReadEquipPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Vehicles" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationVehiclesType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                     &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="AgeRequirements" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAgeRequirementsType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="AdditionalDriverRequirements" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAdditionalDriverRequirementsType"&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="CreditCardRequirements" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="CreditCardPurpose" maxOccurs="3"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="CreditCardCode" maxOccurs="99"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attribute name="CardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="PurposeType"&amp;gt;
 *                                                 &amp;lt;simpleType&amp;gt;
 *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                     &amp;lt;enumeration value="Deposit"/&amp;gt;
 *                                                     &amp;lt;enumeration value="Guarantee"/&amp;gt;
 *                                                     &amp;lt;enumeration value="Payment"/&amp;gt;
 *                                                   &amp;lt;/restriction&amp;gt;
 *                                                 &amp;lt;/simpleType&amp;gt;
 *                                               &amp;lt;/attribute&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ShuttleInfo" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="ShuttleDesc" maxOccurs="6" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
 *                                               &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailShuttleInfoType" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Type"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Non-terminal"/&amp;gt;
 *                                           &amp;lt;enumeration value="In-terminal"/&amp;gt;
 *                                           &amp;lt;enumeration value="CarRentalShuttle-OffAirport"/&amp;gt;
 *                                           &amp;lt;enumeration value="CarRentalShuttle-InTerminal"/&amp;gt;
 *                                           &amp;lt;enumeration value="CarRentalShuttle-OnAirport"/&amp;gt;
 *                                           &amp;lt;enumeration value="AirportShuttle-OnAirport"/&amp;gt;
 *                                           &amp;lt;enumeration value="CallForShuttle"/&amp;gt;
 *                                           &amp;lt;enumeration value="AirportShuttleThenRentalShuttle"/&amp;gt;
 *                                           &amp;lt;enumeration value="AirportShuttle-OffAirport"/&amp;gt;
 *                                           &amp;lt;enumeration value="AirportShuttle-InTerminal"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="AdditionalFees" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationAdditionalFeesType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="AdditionalFeeInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                     &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Liabilities" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationLiabilitiesType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                     &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="LoyaltyProgram" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
 *                                                         &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="ProgramID" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="VehicleVendorSupplier" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Vehicles"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehLocDetailsGrp"/&amp;gt;
 *                           &amp;lt;attribute name="CounterLoc" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="PrimaryLocCodeForBranch" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="CorporateLocInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="DistanceUnit" type="{http://www.opentravel.org/OTA/2003/05}DistanceUnitNameType" /&amp;gt;
 *                           &amp;lt;attribute name="WrittenConfirmationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="ViewershipRuleID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="PrimaryAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="CountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                           &amp;lt;attribute name="StateCode" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
 *                           &amp;lt;attribute name="CityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="AssocTerminalList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to32" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="LocationQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReqRespVersion"/&amp;gt;
 *       &amp;lt;attribute name="ActionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "locationDetails"
})
@XmlRootElement(name = "OTA_VehLocDetailsNotifRQ")
public class OTAVehLocDetailsNotifRQ {

    @XmlElement(name = "POS", required = true)
    protected POSType pos;
    @XmlElement(name = "LocationDetails", required = true)
    protected OTAVehLocDetailsNotifRQ.LocationDetails locationDetails;
    @XmlAttribute(name = "ActionCode")
    protected String actionCode;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "ReqRespVersion")
    protected String reqRespVersion;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad locationDetails.
     * 
     * @return
     *     possible object is
     *     {@link OTAVehLocDetailsNotifRQ.LocationDetails }
     *     
     */
    public OTAVehLocDetailsNotifRQ.LocationDetails getLocationDetails() {
        return locationDetails;
    }

    /**
     * Define el valor de la propiedad locationDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAVehLocDetailsNotifRQ.LocationDetails }
     *     
     */
    public void setLocationDetails(OTAVehLocDetailsNotifRQ.LocationDetails value) {
        this.locationDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad reqRespVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqRespVersion() {
        return reqRespVersion;
    }

    /**
     * Define el valor de la propiedad reqRespVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqRespVersion(String value) {
        this.reqRespVersion = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="LocationDetail" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ProcessingInfo"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProcessingInfoGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Address" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressInfoType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Position" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="AccuracyLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Telephone" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneAttributesGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="MovePhoneNbrToPNR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Policies" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Policy" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Keywords" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Keyword" maxOccurs="30"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="AttachToKeyword" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                     &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="OperationSchedules" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType"&amp;gt;
     *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="SpecialEquipments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="SpecialEquipment" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="GDS_TypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                     &amp;lt;attribute name="FreeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="ReadEquipPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Vehicles" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationVehiclesType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                           &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AgeRequirements" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAgeRequirementsType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AdditionalDriverRequirements" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAdditionalDriverRequirementsType"&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CreditCardRequirements" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="CreditCardPurpose" maxOccurs="3"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="CreditCardCode" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="CardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="PurposeType"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;enumeration value="Deposit"/&amp;gt;
     *                                           &amp;lt;enumeration value="Guarantee"/&amp;gt;
     *                                           &amp;lt;enumeration value="Payment"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ShuttleInfo" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ShuttleDesc" maxOccurs="6" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
     *                                     &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailShuttleInfoType" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Type"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Non-terminal"/&amp;gt;
     *                                 &amp;lt;enumeration value="In-terminal"/&amp;gt;
     *                                 &amp;lt;enumeration value="CarRentalShuttle-OffAirport"/&amp;gt;
     *                                 &amp;lt;enumeration value="CarRentalShuttle-InTerminal"/&amp;gt;
     *                                 &amp;lt;enumeration value="CarRentalShuttle-OnAirport"/&amp;gt;
     *                                 &amp;lt;enumeration value="AirportShuttle-OnAirport"/&amp;gt;
     *                                 &amp;lt;enumeration value="CallForShuttle"/&amp;gt;
     *                                 &amp;lt;enumeration value="AirportShuttleThenRentalShuttle"/&amp;gt;
     *                                 &amp;lt;enumeration value="AirportShuttle-OffAirport"/&amp;gt;
     *                                 &amp;lt;enumeration value="AirportShuttle-InTerminal"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AdditionalFees" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationAdditionalFeesType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="AdditionalFeeInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                           &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Liabilities" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationLiabilitiesType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                           &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="LoyaltyProgram" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
     *                                               &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="ProgramID" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="VehicleVendorSupplier" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Vehicles"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehLocDetailsGrp"/&amp;gt;
     *                 &amp;lt;attribute name="CounterLoc" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="PrimaryLocCodeForBranch" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="CorporateLocInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="DistanceUnit" type="{http://www.opentravel.org/OTA/2003/05}DistanceUnitNameType" /&amp;gt;
     *                 &amp;lt;attribute name="WrittenConfirmationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ViewershipRuleID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="PrimaryAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="CountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *                 &amp;lt;attribute name="StateCode" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
     *                 &amp;lt;attribute name="CityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="AssocTerminalList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to32" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="LocationQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationDetail"
    })
    public static class LocationDetails {

        @XmlElement(name = "LocationDetail", required = true)
        protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail> locationDetail;
        @XmlAttribute(name = "LocationQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger locationQuantity;

        /**
         * Gets the value of the locationDetail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the locationDetail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLocationDetail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail }
         * 
         * 
         */
        public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail> getLocationDetail() {
            if (locationDetail == null) {
                locationDetail = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail>();
            }
            return this.locationDetail;
        }

        /**
         * Obtiene el valor de la propiedad locationQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getLocationQuantity() {
            return locationQuantity;
        }

        /**
         * Define el valor de la propiedad locationQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setLocationQuantity(BigInteger value) {
            this.locationQuantity = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ProcessingInfo"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProcessingInfoGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Address" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressInfoType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Position" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
         *                           &amp;lt;attribute name="AccuracyLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Telephone" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneAttributesGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="MovePhoneNbrToPNR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Policies" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Policy" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Keywords" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Keyword" maxOccurs="30"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="AttachToKeyword" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                           &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="OperationSchedules" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType"&amp;gt;
         *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="SpecialEquipments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="SpecialEquipment" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="GDS_TypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                           &amp;lt;attribute name="FreeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="ReadEquipPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Vehicles" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationVehiclesType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *                 &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AgeRequirements" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAgeRequirementsType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AdditionalDriverRequirements" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAdditionalDriverRequirementsType"&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CreditCardRequirements" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="CreditCardPurpose" maxOccurs="3"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="CreditCardCode" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="CardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="PurposeType"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;enumeration value="Deposit"/&amp;gt;
         *                                 &amp;lt;enumeration value="Guarantee"/&amp;gt;
         *                                 &amp;lt;enumeration value="Payment"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ShuttleInfo" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ShuttleDesc" maxOccurs="6" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
         *                           &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailShuttleInfoType" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Type"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Non-terminal"/&amp;gt;
         *                       &amp;lt;enumeration value="In-terminal"/&amp;gt;
         *                       &amp;lt;enumeration value="CarRentalShuttle-OffAirport"/&amp;gt;
         *                       &amp;lt;enumeration value="CarRentalShuttle-InTerminal"/&amp;gt;
         *                       &amp;lt;enumeration value="CarRentalShuttle-OnAirport"/&amp;gt;
         *                       &amp;lt;enumeration value="AirportShuttle-OnAirport"/&amp;gt;
         *                       &amp;lt;enumeration value="CallForShuttle"/&amp;gt;
         *                       &amp;lt;enumeration value="AirportShuttleThenRentalShuttle"/&amp;gt;
         *                       &amp;lt;enumeration value="AirportShuttle-OffAirport"/&amp;gt;
         *                       &amp;lt;enumeration value="AirportShuttle-InTerminal"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AdditionalFees" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationAdditionalFeesType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="AdditionalFeeInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *                 &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Liabilities" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationLiabilitiesType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *                 &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="LoyaltyProgram" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
         *                                     &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="ProgramID" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="VehicleVendorSupplier" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Vehicles"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehLocDetailsGrp"/&amp;gt;
         *       &amp;lt;attribute name="CounterLoc" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="PrimaryLocCodeForBranch" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="CorporateLocInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="DistanceUnit" type="{http://www.opentravel.org/OTA/2003/05}DistanceUnitNameType" /&amp;gt;
         *       &amp;lt;attribute name="WrittenConfirmationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ViewershipRuleID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="PrimaryAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="CountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
         *       &amp;lt;attribute name="StateCode" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
         *       &amp;lt;attribute name="CityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="AssocTerminalList" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to32" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "processingInfo",
            "address",
            "telephone",
            "policies",
            "keywords",
            "operationSchedules",
            "specialEquipments",
            "vehicles",
            "ageRequirements",
            "additionalDriverRequirements",
            "creditCardRequirements",
            "shuttleInfo",
            "additionalFees",
            "liabilities",
            "loyaltyProgram",
            "vehicleVendorSupplier"
        })
        public static class LocationDetail {

            @XmlElement(name = "ProcessingInfo", required = true)
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ProcessingInfo processingInfo;
            @XmlElement(name = "Address")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address> address;
            @XmlElement(name = "Telephone")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Telephone> telephone;
            @XmlElement(name = "Policies")
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies policies;
            @XmlElement(name = "Keywords")
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords keywords;
            @XmlElement(name = "OperationSchedules")
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.OperationSchedules operationSchedules;
            @XmlElement(name = "SpecialEquipments")
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments specialEquipments;
            @XmlElement(name = "Vehicles")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Vehicles> vehicles;
            @XmlElement(name = "AgeRequirements")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AgeRequirements> ageRequirements;
            @XmlElement(name = "AdditionalDriverRequirements")
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalDriverRequirements additionalDriverRequirements;
            @XmlElement(name = "CreditCardRequirements")
            protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements creditCardRequirements;
            @XmlElement(name = "ShuttleInfo")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo> shuttleInfo;
            @XmlElement(name = "AdditionalFees")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalFees> additionalFees;
            @XmlElement(name = "Liabilities")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Liabilities> liabilities;
            @XmlElement(name = "LoyaltyProgram")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram> loyaltyProgram;
            @XmlElement(name = "VehicleVendorSupplier")
            protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier> vehicleVendorSupplier;
            @XmlAttribute(name = "CounterLoc")
            protected String counterLoc;
            @XmlAttribute(name = "PrimaryLocCodeForBranch")
            protected String primaryLocCodeForBranch;
            @XmlAttribute(name = "CorporateLocInd")
            protected Boolean corporateLocInd;
            @XmlAttribute(name = "DistanceUnit")
            protected DistanceUnitNameType distanceUnit;
            @XmlAttribute(name = "WrittenConfirmationInd")
            protected Boolean writtenConfirmationInd;
            @XmlAttribute(name = "ViewershipRuleID")
            protected String viewershipRuleID;
            @XmlAttribute(name = "PrimaryAirportCode")
            protected String primaryAirportCode;
            @XmlAttribute(name = "RecordID")
            protected String recordID;
            @XmlAttribute(name = "CountryCode")
            protected String countryCode;
            @XmlAttribute(name = "StateCode")
            protected String stateCode;
            @XmlAttribute(name = "CityCode")
            protected String cityCode;
            @XmlAttribute(name = "AssocTerminalList")
            protected List<String> assocTerminalList;
            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "ExtendedLocatonCode")
            protected String extendedLocatonCode;
            @XmlAttribute(name = "AssocAirportLocList")
            protected List<String> assocAirportLocList;

            /**
             * Obtiene el valor de la propiedad processingInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ProcessingInfo }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ProcessingInfo getProcessingInfo() {
                return processingInfo;
            }

            /**
             * Define el valor de la propiedad processingInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ProcessingInfo }
             *     
             */
            public void setProcessingInfo(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ProcessingInfo value) {
                this.processingInfo = value;
            }

            /**
             * Gets the value of the address property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the address property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAddress().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address> getAddress() {
                if (address == null) {
                    address = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address>();
                }
                return this.address;
            }

            /**
             * Gets the value of the telephone property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the telephone property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTelephone().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Telephone }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Telephone> getTelephone() {
                if (telephone == null) {
                    telephone = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Telephone>();
                }
                return this.telephone;
            }

            /**
             * Obtiene el valor de la propiedad policies.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies getPolicies() {
                return policies;
            }

            /**
             * Define el valor de la propiedad policies.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies }
             *     
             */
            public void setPolicies(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies value) {
                this.policies = value;
            }

            /**
             * Obtiene el valor de la propiedad keywords.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords getKeywords() {
                return keywords;
            }

            /**
             * Define el valor de la propiedad keywords.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords }
             *     
             */
            public void setKeywords(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords value) {
                this.keywords = value;
            }

            /**
             * Obtiene el valor de la propiedad operationSchedules.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.OperationSchedules }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.OperationSchedules getOperationSchedules() {
                return operationSchedules;
            }

            /**
             * Define el valor de la propiedad operationSchedules.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.OperationSchedules }
             *     
             */
            public void setOperationSchedules(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.OperationSchedules value) {
                this.operationSchedules = value;
            }

            /**
             * Obtiene el valor de la propiedad specialEquipments.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments getSpecialEquipments() {
                return specialEquipments;
            }

            /**
             * Define el valor de la propiedad specialEquipments.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments }
             *     
             */
            public void setSpecialEquipments(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments value) {
                this.specialEquipments = value;
            }

            /**
             * Gets the value of the vehicles property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicles property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getVehicles().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Vehicles }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Vehicles> getVehicles() {
                if (vehicles == null) {
                    vehicles = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Vehicles>();
                }
                return this.vehicles;
            }

            /**
             * Gets the value of the ageRequirements property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ageRequirements property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAgeRequirements().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AgeRequirements }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AgeRequirements> getAgeRequirements() {
                if (ageRequirements == null) {
                    ageRequirements = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AgeRequirements>();
                }
                return this.ageRequirements;
            }

            /**
             * Obtiene el valor de la propiedad additionalDriverRequirements.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalDriverRequirements }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalDriverRequirements getAdditionalDriverRequirements() {
                return additionalDriverRequirements;
            }

            /**
             * Define el valor de la propiedad additionalDriverRequirements.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalDriverRequirements }
             *     
             */
            public void setAdditionalDriverRequirements(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalDriverRequirements value) {
                this.additionalDriverRequirements = value;
            }

            /**
             * Obtiene el valor de la propiedad creditCardRequirements.
             * 
             * @return
             *     possible object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements }
             *     
             */
            public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements getCreditCardRequirements() {
                return creditCardRequirements;
            }

            /**
             * Define el valor de la propiedad creditCardRequirements.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements }
             *     
             */
            public void setCreditCardRequirements(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements value) {
                this.creditCardRequirements = value;
            }

            /**
             * Gets the value of the shuttleInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the shuttleInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getShuttleInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo> getShuttleInfo() {
                if (shuttleInfo == null) {
                    shuttleInfo = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo>();
                }
                return this.shuttleInfo;
            }

            /**
             * Gets the value of the additionalFees property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additionalFees property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAdditionalFees().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalFees }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalFees> getAdditionalFees() {
                if (additionalFees == null) {
                    additionalFees = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.AdditionalFees>();
                }
                return this.additionalFees;
            }

            /**
             * Gets the value of the liabilities property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the liabilities property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getLiabilities().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Liabilities }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Liabilities> getLiabilities() {
                if (liabilities == null) {
                    liabilities = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Liabilities>();
                }
                return this.liabilities;
            }

            /**
             * Gets the value of the loyaltyProgram property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyaltyProgram property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getLoyaltyProgram().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram> getLoyaltyProgram() {
                if (loyaltyProgram == null) {
                    loyaltyProgram = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram>();
                }
                return this.loyaltyProgram;
            }

            /**
             * Gets the value of the vehicleVendorSupplier property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicleVendorSupplier property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getVehicleVendorSupplier().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier }
             * 
             * 
             */
            public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier> getVehicleVendorSupplier() {
                if (vehicleVendorSupplier == null) {
                    vehicleVendorSupplier = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier>();
                }
                return this.vehicleVendorSupplier;
            }

            /**
             * Obtiene el valor de la propiedad counterLoc.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCounterLoc() {
                return counterLoc;
            }

            /**
             * Define el valor de la propiedad counterLoc.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCounterLoc(String value) {
                this.counterLoc = value;
            }

            /**
             * Obtiene el valor de la propiedad primaryLocCodeForBranch.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryLocCodeForBranch() {
                return primaryLocCodeForBranch;
            }

            /**
             * Define el valor de la propiedad primaryLocCodeForBranch.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryLocCodeForBranch(String value) {
                this.primaryLocCodeForBranch = value;
            }

            /**
             * Obtiene el valor de la propiedad corporateLocInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isCorporateLocInd() {
                return corporateLocInd;
            }

            /**
             * Define el valor de la propiedad corporateLocInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setCorporateLocInd(Boolean value) {
                this.corporateLocInd = value;
            }

            /**
             * Obtiene el valor de la propiedad distanceUnit.
             * 
             * @return
             *     possible object is
             *     {@link DistanceUnitNameType }
             *     
             */
            public DistanceUnitNameType getDistanceUnit() {
                return distanceUnit;
            }

            /**
             * Define el valor de la propiedad distanceUnit.
             * 
             * @param value
             *     allowed object is
             *     {@link DistanceUnitNameType }
             *     
             */
            public void setDistanceUnit(DistanceUnitNameType value) {
                this.distanceUnit = value;
            }

            /**
             * Obtiene el valor de la propiedad writtenConfirmationInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isWrittenConfirmationInd() {
                return writtenConfirmationInd;
            }

            /**
             * Define el valor de la propiedad writtenConfirmationInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setWrittenConfirmationInd(Boolean value) {
                this.writtenConfirmationInd = value;
            }

            /**
             * Obtiene el valor de la propiedad viewershipRuleID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getViewershipRuleID() {
                return viewershipRuleID;
            }

            /**
             * Define el valor de la propiedad viewershipRuleID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setViewershipRuleID(String value) {
                this.viewershipRuleID = value;
            }

            /**
             * Obtiene el valor de la propiedad primaryAirportCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryAirportCode() {
                return primaryAirportCode;
            }

            /**
             * Define el valor de la propiedad primaryAirportCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryAirportCode(String value) {
                this.primaryAirportCode = value;
            }

            /**
             * Obtiene el valor de la propiedad recordID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRecordID() {
                return recordID;
            }

            /**
             * Define el valor de la propiedad recordID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRecordID(String value) {
                this.recordID = value;
            }

            /**
             * Obtiene el valor de la propiedad countryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Define el valor de la propiedad countryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad stateCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStateCode() {
                return stateCode;
            }

            /**
             * Define el valor de la propiedad stateCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStateCode(String value) {
                this.stateCode = value;
            }

            /**
             * Obtiene el valor de la propiedad cityCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityCode() {
                return cityCode;
            }

            /**
             * Define el valor de la propiedad cityCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityCode(String value) {
                this.cityCode = value;
            }

            /**
             * Gets the value of the assocTerminalList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the assocTerminalList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAssocTerminalList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getAssocTerminalList() {
                if (assocTerminalList == null) {
                    assocTerminalList = new ArrayList<String>();
                }
                return this.assocTerminalList;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad extendedLocatonCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExtendedLocatonCode() {
                return extendedLocatonCode;
            }

            /**
             * Define el valor de la propiedad extendedLocatonCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExtendedLocatonCode(String value) {
                this.extendedLocatonCode = value;
            }

            /**
             * Gets the value of the assocAirportLocList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the assocAirportLocList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAssocAirportLocList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getAssocAirportLocList() {
                if (assocAirportLocList == null) {
                    assocAirportLocList = new ArrayList<String>();
                }
                return this.assocAirportLocList;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAdditionalDriverRequirementsType"&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AdditionalDriverRequirements
                extends VehicleAdditionalDriverRequirementsType
            {

                @XmlAttribute(name = "Action")
                protected ActionType action;

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationAdditionalFeesType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="AdditionalFeeInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *       &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "additionalFeeInfo"
            })
            public static class AdditionalFees
                extends VehicleLocationAdditionalFeesType
            {

                @XmlElement(name = "AdditionalFeeInfo")
                protected FormattedTextType additionalFeeInfo;
                @XmlAttribute(name = "Type")
                protected String type;
                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "RecordID")
                protected String recordID;
                @XmlAttribute(name = "PolicyNumber")
                protected String policyNumber;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad additionalFeeInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public FormattedTextType getAdditionalFeeInfo() {
                    return additionalFeeInfo;
                }

                /**
                 * Define el valor de la propiedad additionalFeeInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public void setAdditionalFeeInfo(FormattedTextType value) {
                    this.additionalFeeInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad recordID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRecordID() {
                    return recordID;
                }

                /**
                 * Define el valor de la propiedad recordID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRecordID(String value) {
                    this.recordID = value;
                }

                /**
                 * Obtiene el valor de la propiedad policyNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPolicyNumber() {
                    return policyNumber;
                }

                /**
                 * Define el valor de la propiedad policyNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPolicyNumber(String value) {
                    this.policyNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressInfoType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Position" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
             *                 &amp;lt;attribute name="AccuracyLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "position"
            })
            public static class Address
                extends AddressInfoType
            {

                @XmlElement(name = "Position")
                protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address.Position position;
                @XmlAttribute(name = "LocationName")
                protected String locationName;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad position.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address.Position }
                 *     
                 */
                public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address.Position getPosition() {
                    return position;
                }

                /**
                 * Define el valor de la propiedad position.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address.Position }
                 *     
                 */
                public void setPosition(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Address.Position value) {
                    this.position = value;
                }

                /**
                 * Obtiene el valor de la propiedad locationName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationName() {
                    return locationName;
                }

                /**
                 * Define el valor de la propiedad locationName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationName(String value) {
                    this.locationName = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
                 *       &amp;lt;attribute name="AccuracyLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Position {

                    @XmlAttribute(name = "AccuracyLevelCode")
                    protected String accuracyLevelCode;
                    @XmlAttribute(name = "Action")
                    protected String action;
                    @XmlAttribute(name = "Latitude")
                    protected String latitude;
                    @XmlAttribute(name = "Longitude")
                    protected String longitude;
                    @XmlAttribute(name = "Altitude")
                    protected String altitude;
                    @XmlAttribute(name = "AltitudeUnitOfMeasureCode")
                    protected String altitudeUnitOfMeasureCode;
                    @XmlAttribute(name = "PositionAccuracyCode")
                    protected String positionAccuracyCode;

                    /**
                     * Obtiene el valor de la propiedad accuracyLevelCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAccuracyLevelCode() {
                        return accuracyLevelCode;
                    }

                    /**
                     * Define el valor de la propiedad accuracyLevelCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAccuracyLevelCode(String value) {
                        this.accuracyLevelCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad action.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAction() {
                        return action;
                    }

                    /**
                     * Define el valor de la propiedad action.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAction(String value) {
                        this.action = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad latitude.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLatitude() {
                        return latitude;
                    }

                    /**
                     * Define el valor de la propiedad latitude.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLatitude(String value) {
                        this.latitude = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad longitude.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLongitude() {
                        return longitude;
                    }

                    /**
                     * Define el valor de la propiedad longitude.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLongitude(String value) {
                        this.longitude = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad altitude.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAltitude() {
                        return altitude;
                    }

                    /**
                     * Define el valor de la propiedad altitude.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAltitude(String value) {
                        this.altitude = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad altitudeUnitOfMeasureCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAltitudeUnitOfMeasureCode() {
                        return altitudeUnitOfMeasureCode;
                    }

                    /**
                     * Define el valor de la propiedad altitudeUnitOfMeasureCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAltitudeUnitOfMeasureCode(String value) {
                        this.altitudeUnitOfMeasureCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad positionAccuracyCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPositionAccuracyCode() {
                        return positionAccuracyCode;
                    }

                    /**
                     * Define el valor de la propiedad positionAccuracyCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPositionAccuracyCode(String value) {
                        this.positionAccuracyCode = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleAgeRequirementsType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AgeRequirements
                extends VehicleAgeRequirementsType
            {

                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="CreditCardPurpose" maxOccurs="3"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="CreditCardCode" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="CardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="PurposeType"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;enumeration value="Deposit"/&amp;gt;
             *                       &amp;lt;enumeration value="Guarantee"/&amp;gt;
             *                       &amp;lt;enumeration value="Payment"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "creditCardPurpose",
                "creditCardInfo"
            })
            public static class CreditCardRequirements {

                @XmlElement(name = "CreditCardPurpose", required = true)
                protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose> creditCardPurpose;
                @XmlElement(name = "CreditCardInfo")
                protected List<FormattedTextType> creditCardInfo;
                @XmlAttribute(name = "PolicyNumber")
                protected String policyNumber;
                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Gets the value of the creditCardPurpose property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the creditCardPurpose property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getCreditCardPurpose().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose }
                 * 
                 * 
                 */
                public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose> getCreditCardPurpose() {
                    if (creditCardPurpose == null) {
                        creditCardPurpose = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose>();
                    }
                    return this.creditCardPurpose;
                }

                /**
                 * Gets the value of the creditCardInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the creditCardInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getCreditCardInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link FormattedTextType }
                 * 
                 * 
                 */
                public List<FormattedTextType> getCreditCardInfo() {
                    if (creditCardInfo == null) {
                        creditCardInfo = new ArrayList<FormattedTextType>();
                    }
                    return this.creditCardInfo;
                }

                /**
                 * Obtiene el valor de la propiedad policyNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPolicyNumber() {
                    return policyNumber;
                }

                /**
                 * Define el valor de la propiedad policyNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPolicyNumber(String value) {
                    this.policyNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="CreditCardCode" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="CardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="PurposeType"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;enumeration value="Deposit"/&amp;gt;
                 *             &amp;lt;enumeration value="Guarantee"/&amp;gt;
                 *             &amp;lt;enumeration value="Payment"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "creditCardCode"
                })
                public static class CreditCardPurpose {

                    @XmlElement(name = "CreditCardCode", required = true)
                    protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose.CreditCardCode> creditCardCode;
                    @XmlAttribute(name = "PurposeType")
                    protected String purposeType;

                    /**
                     * Gets the value of the creditCardCode property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the creditCardCode property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getCreditCardCode().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose.CreditCardCode }
                     * 
                     * 
                     */
                    public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose.CreditCardCode> getCreditCardCode() {
                        if (creditCardCode == null) {
                            creditCardCode = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.CreditCardRequirements.CreditCardPurpose.CreditCardCode>();
                        }
                        return this.creditCardCode;
                    }

                    /**
                     * Obtiene el valor de la propiedad purposeType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPurposeType() {
                        return purposeType;
                    }

                    /**
                     * Define el valor de la propiedad purposeType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPurposeType(String value) {
                        this.purposeType = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="CardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class CreditCardCode {

                        @XmlAttribute(name = "CardCode")
                        protected String cardCode;

                        /**
                         * Obtiene el valor de la propiedad cardCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCardCode() {
                            return cardCode;
                        }

                        /**
                         * Define el valor de la propiedad cardCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCardCode(String value) {
                            this.cardCode = value;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Keyword" maxOccurs="30"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="AttachToKeyword" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "keyword"
            })
            public static class Keywords {

                @XmlElement(name = "Keyword", required = true)
                protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords.Keyword> keyword;

                /**
                 * Gets the value of the keyword property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the keyword property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getKeyword().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords.Keyword }
                 * 
                 * 
                 */
                public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords.Keyword> getKeyword() {
                    if (keyword == null) {
                        keyword = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Keywords.Keyword>();
                    }
                    return this.keyword;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="AttachToKeyword" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Keyword
                    extends ParagraphType
                {

                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "PolicyNumber")
                    protected String policyNumber;
                    @XmlAttribute(name = "AttachToKeyword")
                    protected String attachToKeyword;
                    @XmlAttribute(name = "Action")
                    protected ActionType action;
                    @XmlAttribute(name = "Start")
                    protected String start;
                    @XmlAttribute(name = "Duration")
                    protected String duration;
                    @XmlAttribute(name = "End")
                    protected String end;

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad policyNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPolicyNumber() {
                        return policyNumber;
                    }

                    /**
                     * Define el valor de la propiedad policyNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPolicyNumber(String value) {
                        this.policyNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad attachToKeyword.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAttachToKeyword() {
                        return attachToKeyword;
                    }

                    /**
                     * Define el valor de la propiedad attachToKeyword.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAttachToKeyword(String value) {
                        this.attachToKeyword = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad action.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ActionType }
                     *     
                     */
                    public ActionType getAction() {
                        return action;
                    }

                    /**
                     * Define el valor de la propiedad action.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ActionType }
                     *     
                     */
                    public void setAction(ActionType value) {
                        this.action = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad start.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getStart() {
                        return start;
                    }

                    /**
                     * Define el valor de la propiedad start.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setStart(String value) {
                        this.start = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad duration.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDuration() {
                        return duration;
                    }

                    /**
                     * Define el valor de la propiedad duration.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDuration(String value) {
                        this.duration = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad end.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEnd() {
                        return end;
                    }

                    /**
                     * Define el valor de la propiedad end.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEnd(String value) {
                        this.end = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationLiabilitiesType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *       &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Liabilities
                extends VehicleLocationLiabilitiesType
            {

                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "RecordID")
                protected String recordID;
                @XmlAttribute(name = "PolicyNumber")
                protected String policyNumber;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad recordID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRecordID() {
                    return recordID;
                }

                /**
                 * Define el valor de la propiedad recordID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRecordID(String value) {
                    this.recordID = value;
                }

                /**
                 * Obtiene el valor de la propiedad policyNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPolicyNumber() {
                    return policyNumber;
                }

                /**
                 * Define el valor de la propiedad policyNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPolicyNumber(String value) {
                    this.policyNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
             *                           &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="ProgramID" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "vehicles"
            })
            public static class LoyaltyProgram {

                @XmlElement(name = "Vehicles")
                protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles vehicles;
                @XmlAttribute(name = "ProgramID", required = true)
                protected String programID;
                @XmlAttribute(name = "Text")
                protected String text;
                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad vehicles.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles }
                 *     
                 */
                public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles getVehicles() {
                    return vehicles;
                }

                /**
                 * Define el valor de la propiedad vehicles.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles }
                 *     
                 */
                public void setVehicles(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles value) {
                    this.vehicles = value;
                }

                /**
                 * Obtiene el valor de la propiedad programID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramID() {
                    return programID;
                }

                /**
                 * Define el valor de la propiedad programID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramID(String value) {
                    this.programID = value;
                }

                /**
                 * Obtiene el valor de la propiedad text.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getText() {
                    return text;
                }

                /**
                 * Define el valor de la propiedad text.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setText(String value) {
                    this.text = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
                 *                 &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "vehicle"
                })
                public static class Vehicles {

                    @XmlElement(name = "Vehicle", required = true)
                    protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles.Vehicle> vehicle;

                    /**
                     * Gets the value of the vehicle property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getVehicle().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles.Vehicle }
                     * 
                     * 
                     */
                    public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles.Vehicle> getVehicle() {
                        if (vehicle == null) {
                            vehicle = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.LoyaltyProgram.Vehicles.Vehicle>();
                        }
                        return this.vehicle;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
                     *       &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Vehicle
                        extends VehicleCoreType
                    {

                        @XmlAttribute(name = "IncludeExclude")
                        protected IncludeExcludeType includeExclude;

                        /**
                         * Obtiene el valor de la propiedad includeExclude.
                         * 
                         * @return
                         *     possible object is
                         *     {@link IncludeExcludeType }
                         *     
                         */
                        public IncludeExcludeType getIncludeExclude() {
                            return includeExclude;
                        }

                        /**
                         * Define el valor de la propiedad includeExclude.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link IncludeExcludeType }
                         *     
                         */
                        public void setIncludeExclude(IncludeExcludeType value) {
                            this.includeExclude = value;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType"&amp;gt;
             *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OperationSchedules
                extends OperationSchedulesType
            {

                @XmlAttribute(name = "PolicyNumber")
                protected String policyNumber;
                @XmlAttribute(name = "Text")
                protected String text;
                @XmlAttribute(name = "Action")
                protected ActionType action;

                /**
                 * Obtiene el valor de la propiedad policyNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPolicyNumber() {
                    return policyNumber;
                }

                /**
                 * Define el valor de la propiedad policyNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPolicyNumber(String value) {
                    this.policyNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad text.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getText() {
                    return text;
                }

                /**
                 * Define el valor de la propiedad text.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setText(String value) {
                    this.text = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Policy" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "policy"
            })
            public static class Policies {

                @XmlElement(name = "Policy", required = true)
                protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies.Policy> policy;

                /**
                 * Gets the value of the policy property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the policy property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getPolicy().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies.Policy }
                 * 
                 * 
                 */
                public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies.Policy> getPolicy() {
                    if (policy == null) {
                        policy = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Policies.Policy>();
                    }
                    return this.policy;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Policy
                    extends ParagraphType
                {

                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "PolicyNumber")
                    protected String policyNumber;
                    @XmlAttribute(name = "Action")
                    protected ActionType action;
                    @XmlAttribute(name = "Start")
                    protected String start;
                    @XmlAttribute(name = "Duration")
                    protected String duration;
                    @XmlAttribute(name = "End")
                    protected String end;

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad policyNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPolicyNumber() {
                        return policyNumber;
                    }

                    /**
                     * Define el valor de la propiedad policyNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPolicyNumber(String value) {
                        this.policyNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad action.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ActionType }
                     *     
                     */
                    public ActionType getAction() {
                        return action;
                    }

                    /**
                     * Define el valor de la propiedad action.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ActionType }
                     *     
                     */
                    public void setAction(ActionType value) {
                        this.action = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad start.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getStart() {
                        return start;
                    }

                    /**
                     * Define el valor de la propiedad start.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setStart(String value) {
                        this.start = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad duration.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDuration() {
                        return duration;
                    }

                    /**
                     * Define el valor de la propiedad duration.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDuration(String value) {
                        this.duration = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad end.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEnd() {
                        return end;
                    }

                    /**
                     * Define el valor de la propiedad end.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEnd(String value) {
                        this.end = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProcessingInfoGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ProcessingInfo {

                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;
                @XmlAttribute(name = "CompanyShortName")
                protected String companyShortName;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

                /**
                 * Obtiene el valor de la propiedad companyShortName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCompanyShortName() {
                    return companyShortName;
                }

                /**
                 * Define el valor de la propiedad companyShortName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCompanyShortName(String value) {
                    this.companyShortName = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ShuttleDesc" maxOccurs="6" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
             *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailShuttleInfoType" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Type"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Non-terminal"/&amp;gt;
             *             &amp;lt;enumeration value="In-terminal"/&amp;gt;
             *             &amp;lt;enumeration value="CarRentalShuttle-OffAirport"/&amp;gt;
             *             &amp;lt;enumeration value="CarRentalShuttle-InTerminal"/&amp;gt;
             *             &amp;lt;enumeration value="CarRentalShuttle-OnAirport"/&amp;gt;
             *             &amp;lt;enumeration value="AirportShuttle-OnAirport"/&amp;gt;
             *             &amp;lt;enumeration value="CallForShuttle"/&amp;gt;
             *             &amp;lt;enumeration value="AirportShuttleThenRentalShuttle"/&amp;gt;
             *             &amp;lt;enumeration value="AirportShuttle-OffAirport"/&amp;gt;
             *             &amp;lt;enumeration value="AirportShuttle-InTerminal"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "shuttleDesc"
            })
            public static class ShuttleInfo {

                @XmlElement(name = "ShuttleDesc")
                protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo.ShuttleDesc> shuttleDesc;
                @XmlAttribute(name = "Type")
                protected String type;
                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Gets the value of the shuttleDesc property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the shuttleDesc property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getShuttleDesc().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo.ShuttleDesc }
                 * 
                 * 
                 */
                public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo.ShuttleDesc> getShuttleDesc() {
                    if (shuttleDesc == null) {
                        shuttleDesc = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.ShuttleInfo.ShuttleDesc>();
                    }
                    return this.shuttleDesc;
                }

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
                 *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailShuttleInfoType" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ShuttleDesc
                    extends FormattedTextType
                {

                    @XmlAttribute(name = "Type")
                    protected LocationDetailShuttleInfoType type;

                    /**
                     * Obtiene el valor de la propiedad type.
                     * 
                     * @return
                     *     possible object is
                     *     {@link LocationDetailShuttleInfoType }
                     *     
                     */
                    public LocationDetailShuttleInfoType getType() {
                        return type;
                    }

                    /**
                     * Define el valor de la propiedad type.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link LocationDetailShuttleInfoType }
                     *     
                     */
                    public void setType(LocationDetailShuttleInfoType value) {
                        this.type = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="SpecialEquipment" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="GDS_TypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                 &amp;lt;attribute name="FreeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="ReadEquipPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "specialEquipment"
            })
            public static class SpecialEquipments {

                @XmlElement(name = "SpecialEquipment", required = true)
                protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments.SpecialEquipment> specialEquipment;
                @XmlAttribute(name = "PolicyNumber")
                protected String policyNumber;
                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Gets the value of the specialEquipment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specialEquipment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getSpecialEquipment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments.SpecialEquipment }
                 * 
                 * 
                 */
                public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments.SpecialEquipment> getSpecialEquipment() {
                    if (specialEquipment == null) {
                        specialEquipment = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.SpecialEquipments.SpecialEquipment>();
                    }
                    return this.specialEquipment;
                }

                /**
                 * Obtiene el valor de la propiedad policyNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPolicyNumber() {
                    return policyNumber;
                }

                /**
                 * Define el valor de la propiedad policyNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPolicyNumber(String value) {
                    this.policyNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="GDS_TypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *       &amp;lt;attribute name="FreeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="ReadEquipPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "charge"
                })
                public static class SpecialEquipment
                    extends VehicleEquipmentType
                {

                    @XmlElement(name = "Charge")
                    protected VehicleChargeType charge;
                    @XmlAttribute(name = "GDS_TypeCode")
                    protected String gdsTypeCode;
                    @XmlAttribute(name = "Name")
                    protected String name;
                    @XmlAttribute(name = "FreeInd")
                    protected Boolean freeInd;
                    @XmlAttribute(name = "ReadEquipPolicyInd")
                    protected Boolean readEquipPolicyInd;

                    /**
                     * Obtiene el valor de la propiedad charge.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VehicleChargeType }
                     *     
                     */
                    public VehicleChargeType getCharge() {
                        return charge;
                    }

                    /**
                     * Define el valor de la propiedad charge.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VehicleChargeType }
                     *     
                     */
                    public void setCharge(VehicleChargeType value) {
                        this.charge = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad gdsTypeCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGDSTypeCode() {
                        return gdsTypeCode;
                    }

                    /**
                     * Define el valor de la propiedad gdsTypeCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGDSTypeCode(String value) {
                        this.gdsTypeCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad name.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getName() {
                        return name;
                    }

                    /**
                     * Define el valor de la propiedad name.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setName(String value) {
                        this.name = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad freeInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isFreeInd() {
                        return freeInd;
                    }

                    /**
                     * Define el valor de la propiedad freeInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setFreeInd(Boolean value) {
                        this.freeInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad readEquipPolicyInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isReadEquipPolicyInd() {
                        return readEquipPolicyInd;
                    }

                    /**
                     * Define el valor de la propiedad readEquipPolicyInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setReadEquipPolicyInd(Boolean value) {
                        this.readEquipPolicyInd = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneAttributesGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="MovePhoneNbrToPNR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Telephone {

                @XmlAttribute(name = "MovePhoneNbrToPNR_Ind")
                protected Boolean movePhoneNbrToPNRInd;
                @XmlAttribute(name = "PhoneLocationType")
                protected String phoneLocationType;
                @XmlAttribute(name = "PhoneTechType")
                protected String phoneTechType;
                @XmlAttribute(name = "PhoneUseType")
                protected String phoneUseType;
                @XmlAttribute(name = "CountryAccessCode")
                protected String countryAccessCode;
                @XmlAttribute(name = "AreaCityCode")
                protected String areaCityCode;
                @XmlAttribute(name = "PhoneNumber", required = true)
                protected String phoneNumber;
                @XmlAttribute(name = "Extension")
                protected String extension;
                @XmlAttribute(name = "PIN")
                protected String pin;
                @XmlAttribute(name = "Remark")
                protected String remark;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad movePhoneNbrToPNRInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMovePhoneNbrToPNRInd() {
                    return movePhoneNbrToPNRInd;
                }

                /**
                 * Define el valor de la propiedad movePhoneNbrToPNRInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMovePhoneNbrToPNRInd(Boolean value) {
                    this.movePhoneNbrToPNRInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneLocationType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneLocationType() {
                    return phoneLocationType;
                }

                /**
                 * Define el valor de la propiedad phoneLocationType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneLocationType(String value) {
                    this.phoneLocationType = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneTechType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneTechType() {
                    return phoneTechType;
                }

                /**
                 * Define el valor de la propiedad phoneTechType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneTechType(String value) {
                    this.phoneTechType = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneUseType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneUseType() {
                    return phoneUseType;
                }

                /**
                 * Define el valor de la propiedad phoneUseType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneUseType(String value) {
                    this.phoneUseType = value;
                }

                /**
                 * Obtiene el valor de la propiedad countryAccessCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryAccessCode() {
                    return countryAccessCode;
                }

                /**
                 * Define el valor de la propiedad countryAccessCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryAccessCode(String value) {
                    this.countryAccessCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad areaCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAreaCityCode() {
                    return areaCityCode;
                }

                /**
                 * Define el valor de la propiedad areaCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAreaCityCode(String value) {
                    this.areaCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNumber() {
                    return phoneNumber;
                }

                /**
                 * Define el valor de la propiedad phoneNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNumber(String value) {
                    this.phoneNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad extension.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExtension() {
                    return extension;
                }

                /**
                 * Define el valor de la propiedad extension.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExtension(String value) {
                    this.extension = value;
                }

                /**
                 * Obtiene el valor de la propiedad pin.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPIN() {
                    return pin;
                }

                /**
                 * Define el valor de la propiedad pin.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPIN(String value) {
                    this.pin = value;
                }

                /**
                 * Obtiene el valor de la propiedad remark.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRemark() {
                    return remark;
                }

                /**
                 * Define el valor de la propiedad remark.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRemark(String value) {
                    this.remark = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Vehicles"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "vehicles"
            })
            public static class VehicleVendorSupplier {

                @XmlElement(name = "Vehicles", required = true)
                protected OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles vehicles;
                @XmlAttribute(name = "CompanyShortName")
                protected String companyShortName;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "CountryCode")
                protected String countryCode;

                /**
                 * Obtiene el valor de la propiedad vehicles.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles }
                 *     
                 */
                public OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles getVehicles() {
                    return vehicles;
                }

                /**
                 * Define el valor de la propiedad vehicles.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles }
                 *     
                 */
                public void setVehicles(OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles value) {
                    this.vehicles = value;
                }

                /**
                 * Obtiene el valor de la propiedad companyShortName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCompanyShortName() {
                    return companyShortName;
                }

                /**
                 * Define el valor de la propiedad companyShortName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCompanyShortName(String value) {
                    this.companyShortName = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad countryCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryCode() {
                    return countryCode;
                }

                /**
                 * Define el valor de la propiedad countryCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryCode(String value) {
                    this.countryCode = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "vehicle"
                })
                public static class Vehicles {

                    @XmlElement(name = "Vehicle", required = true)
                    protected List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles.Vehicle> vehicle;

                    /**
                     * Gets the value of the vehicle property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getVehicle().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles.Vehicle }
                     * 
                     * 
                     */
                    public List<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles.Vehicle> getVehicle() {
                        if (vehicle == null) {
                            vehicle = new ArrayList<OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.VehicleVendorSupplier.Vehicles.Vehicle>();
                        }
                        return this.vehicle;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType"&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Vehicle
                        extends VehicleCoreType
                    {


                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleLocationVehiclesType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="PolicyNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
             *       &amp;lt;attribute name="RecordID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Vehicles
                extends VehicleLocationVehiclesType
            {

                @XmlAttribute(name = "PolicyNumber")
                protected String policyNumber;
                @XmlAttribute(name = "Text")
                protected String text;
                @XmlAttribute(name = "Action")
                protected ActionType action;
                @XmlAttribute(name = "RecordID")
                protected String recordID;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad policyNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPolicyNumber() {
                    return policyNumber;
                }

                /**
                 * Define el valor de la propiedad policyNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPolicyNumber(String value) {
                    this.policyNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad text.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getText() {
                    return text;
                }

                /**
                 * Define el valor de la propiedad text.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setText(String value) {
                    this.text = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

                /**
                 * Obtiene el valor de la propiedad recordID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRecordID() {
                    return recordID;
                }

                /**
                 * Define el valor de la propiedad recordID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRecordID(String value) {
                    this.recordID = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }

    }

}
