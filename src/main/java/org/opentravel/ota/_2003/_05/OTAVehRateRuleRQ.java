
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type"/&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;element name="Reference" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
 *           &amp;lt;element name="RentalInfo"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="VehRentalCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType"/&amp;gt;
 *                     &amp;lt;element name="VehicleInfo" type="{http://www.opentravel.org/OTA/2003/05}VehiclePrefType"/&amp;gt;
 *                     &amp;lt;element name="SpecialEquipPrefs" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="SpecialEquipPref" maxOccurs="15"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentPrefGroup"/&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="CustLoyalty" maxOccurs="4" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="RateQualifier" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RateQualifierCoreGroup"/&amp;gt;
 *                             &amp;lt;attribute name="RateAuthorizationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                             &amp;lt;attribute name="VendorRateID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                             &amp;lt;attribute name="RateModifiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="OffLocService" type="{http://www.opentravel.org/OTA/2003/05}OffLocationServiceType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="ArrivalDetails" type="{http://www.opentravel.org/OTA/2003/05}VehicleArrivalDetailsType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="TourInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleTourInfoType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}InventoryStatusType" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReqRespVersion"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "reference",
    "rentalInfo"
})
@XmlRootElement(name = "OTA_VehRateRuleRQ")
public class OTAVehRateRuleRQ {

    @XmlElement(name = "POS", required = true)
    protected POSType pos;
    @XmlElement(name = "Reference")
    protected UniqueIDType reference;
    @XmlElement(name = "RentalInfo")
    protected OTAVehRateRuleRQ.RentalInfo rentalInfo;
    @XmlAttribute(name = "CompanyShortName")
    protected String companyShortName;
    @XmlAttribute(name = "TravelSector")
    protected String travelSector;
    @XmlAttribute(name = "Code")
    protected String code;
    @XmlAttribute(name = "CodeContext")
    protected String codeContext;
    @XmlAttribute(name = "CountryCode")
    protected String countryCode;
    @XmlAttribute(name = "ReqRespVersion")
    protected String reqRespVersion;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad reference.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getReference() {
        return reference;
    }

    /**
     * Define el valor de la propiedad reference.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setReference(UniqueIDType value) {
        this.reference = value;
    }

    /**
     * Obtiene el valor de la propiedad rentalInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAVehRateRuleRQ.RentalInfo }
     *     
     */
    public OTAVehRateRuleRQ.RentalInfo getRentalInfo() {
        return rentalInfo;
    }

    /**
     * Define el valor de la propiedad rentalInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAVehRateRuleRQ.RentalInfo }
     *     
     */
    public void setRentalInfo(OTAVehRateRuleRQ.RentalInfo value) {
        this.rentalInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad companyShortName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyShortName() {
        return companyShortName;
    }

    /**
     * Define el valor de la propiedad companyShortName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyShortName(String value) {
        this.companyShortName = value;
    }

    /**
     * Obtiene el valor de la propiedad travelSector.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelSector() {
        return travelSector;
    }

    /**
     * Define el valor de la propiedad travelSector.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelSector(String value) {
        this.travelSector = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad codeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeContext() {
        return codeContext;
    }

    /**
     * Define el valor de la propiedad codeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeContext(String value) {
        this.codeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad reqRespVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqRespVersion() {
        return reqRespVersion;
    }

    /**
     * Define el valor de la propiedad reqRespVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqRespVersion(String value) {
        this.reqRespVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VehRentalCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType"/&amp;gt;
     *         &amp;lt;element name="VehicleInfo" type="{http://www.opentravel.org/OTA/2003/05}VehiclePrefType"/&amp;gt;
     *         &amp;lt;element name="SpecialEquipPrefs" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="SpecialEquipPref" maxOccurs="15"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentPrefGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CustLoyalty" maxOccurs="4" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="RateQualifier" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RateQualifierCoreGroup"/&amp;gt;
     *                 &amp;lt;attribute name="RateAuthorizationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="VendorRateID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="RateModifiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="OffLocService" type="{http://www.opentravel.org/OTA/2003/05}OffLocationServiceType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ArrivalDetails" type="{http://www.opentravel.org/OTA/2003/05}VehicleArrivalDetailsType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="TourInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleTourInfoType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CustomerID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}InventoryStatusType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehRentalCore",
        "vehicleInfo",
        "specialEquipPrefs",
        "custLoyalty",
        "rateQualifier",
        "offLocService",
        "arrivalDetails",
        "tourInfo",
        "customerID",
        "tpaExtensions"
    })
    public static class RentalInfo {

        @XmlElement(name = "VehRentalCore", required = true)
        protected VehicleRentalCoreType vehRentalCore;
        @XmlElement(name = "VehicleInfo", required = true)
        protected VehiclePrefType vehicleInfo;
        @XmlElement(name = "SpecialEquipPrefs")
        protected OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs specialEquipPrefs;
        @XmlElement(name = "CustLoyalty")
        protected List<OTAVehRateRuleRQ.RentalInfo.CustLoyalty> custLoyalty;
        @XmlElement(name = "RateQualifier")
        protected OTAVehRateRuleRQ.RentalInfo.RateQualifier rateQualifier;
        @XmlElement(name = "OffLocService")
        protected OffLocationServiceType offLocService;
        @XmlElement(name = "ArrivalDetails")
        protected VehicleArrivalDetailsType arrivalDetails;
        @XmlElement(name = "TourInfo")
        protected VehicleTourInfoType tourInfo;
        @XmlElement(name = "CustomerID")
        protected UniqueIDType customerID;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;
        @XmlAttribute(name = "Status")
        protected InventoryStatusType status;

        /**
         * Obtiene el valor de la propiedad vehRentalCore.
         * 
         * @return
         *     possible object is
         *     {@link VehicleRentalCoreType }
         *     
         */
        public VehicleRentalCoreType getVehRentalCore() {
            return vehRentalCore;
        }

        /**
         * Define el valor de la propiedad vehRentalCore.
         * 
         * @param value
         *     allowed object is
         *     {@link VehicleRentalCoreType }
         *     
         */
        public void setVehRentalCore(VehicleRentalCoreType value) {
            this.vehRentalCore = value;
        }

        /**
         * Obtiene el valor de la propiedad vehicleInfo.
         * 
         * @return
         *     possible object is
         *     {@link VehiclePrefType }
         *     
         */
        public VehiclePrefType getVehicleInfo() {
            return vehicleInfo;
        }

        /**
         * Define el valor de la propiedad vehicleInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link VehiclePrefType }
         *     
         */
        public void setVehicleInfo(VehiclePrefType value) {
            this.vehicleInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad specialEquipPrefs.
         * 
         * @return
         *     possible object is
         *     {@link OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs }
         *     
         */
        public OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs getSpecialEquipPrefs() {
            return specialEquipPrefs;
        }

        /**
         * Define el valor de la propiedad specialEquipPrefs.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs }
         *     
         */
        public void setSpecialEquipPrefs(OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs value) {
            this.specialEquipPrefs = value;
        }

        /**
         * Gets the value of the custLoyalty property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the custLoyalty property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCustLoyalty().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAVehRateRuleRQ.RentalInfo.CustLoyalty }
         * 
         * 
         */
        public List<OTAVehRateRuleRQ.RentalInfo.CustLoyalty> getCustLoyalty() {
            if (custLoyalty == null) {
                custLoyalty = new ArrayList<OTAVehRateRuleRQ.RentalInfo.CustLoyalty>();
            }
            return this.custLoyalty;
        }

        /**
         * Obtiene el valor de la propiedad rateQualifier.
         * 
         * @return
         *     possible object is
         *     {@link OTAVehRateRuleRQ.RentalInfo.RateQualifier }
         *     
         */
        public OTAVehRateRuleRQ.RentalInfo.RateQualifier getRateQualifier() {
            return rateQualifier;
        }

        /**
         * Define el valor de la propiedad rateQualifier.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAVehRateRuleRQ.RentalInfo.RateQualifier }
         *     
         */
        public void setRateQualifier(OTAVehRateRuleRQ.RentalInfo.RateQualifier value) {
            this.rateQualifier = value;
        }

        /**
         * Obtiene el valor de la propiedad offLocService.
         * 
         * @return
         *     possible object is
         *     {@link OffLocationServiceType }
         *     
         */
        public OffLocationServiceType getOffLocService() {
            return offLocService;
        }

        /**
         * Define el valor de la propiedad offLocService.
         * 
         * @param value
         *     allowed object is
         *     {@link OffLocationServiceType }
         *     
         */
        public void setOffLocService(OffLocationServiceType value) {
            this.offLocService = value;
        }

        /**
         * Obtiene el valor de la propiedad arrivalDetails.
         * 
         * @return
         *     possible object is
         *     {@link VehicleArrivalDetailsType }
         *     
         */
        public VehicleArrivalDetailsType getArrivalDetails() {
            return arrivalDetails;
        }

        /**
         * Define el valor de la propiedad arrivalDetails.
         * 
         * @param value
         *     allowed object is
         *     {@link VehicleArrivalDetailsType }
         *     
         */
        public void setArrivalDetails(VehicleArrivalDetailsType value) {
            this.arrivalDetails = value;
        }

        /**
         * Obtiene el valor de la propiedad tourInfo.
         * 
         * @return
         *     possible object is
         *     {@link VehicleTourInfoType }
         *     
         */
        public VehicleTourInfoType getTourInfo() {
            return tourInfo;
        }

        /**
         * Define el valor de la propiedad tourInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link VehicleTourInfoType }
         *     
         */
        public void setTourInfo(VehicleTourInfoType value) {
            this.tourInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad customerID.
         * 
         * @return
         *     possible object is
         *     {@link UniqueIDType }
         *     
         */
        public UniqueIDType getCustomerID() {
            return customerID;
        }

        /**
         * Define el valor de la propiedad customerID.
         * 
         * @param value
         *     allowed object is
         *     {@link UniqueIDType }
         *     
         */
        public void setCustomerID(UniqueIDType value) {
            this.customerID = value;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link InventoryStatusType }
         *     
         */
        public InventoryStatusType getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link InventoryStatusType }
         *     
         */
        public void setStatus(InventoryStatusType value) {
            this.status = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CustLoyalty {

            @XmlAttribute(name = "ShareSynchInd")
            protected String shareSynchInd;
            @XmlAttribute(name = "ShareMarketInd")
            protected String shareMarketInd;
            @XmlAttribute(name = "ProgramID")
            protected String programID;
            @XmlAttribute(name = "MembershipID")
            protected String membershipID;
            @XmlAttribute(name = "TravelSector")
            protected String travelSector;
            @XmlAttribute(name = "VendorCode")
            protected List<String> vendorCode;
            @XmlAttribute(name = "PrimaryLoyaltyIndicator")
            protected Boolean primaryLoyaltyIndicator;
            @XmlAttribute(name = "AllianceLoyaltyLevelName")
            protected String allianceLoyaltyLevelName;
            @XmlAttribute(name = "CustomerType")
            protected String customerType;
            @XmlAttribute(name = "CustomerValue")
            protected String customerValue;
            @XmlAttribute(name = "Password")
            protected String password;
            @XmlAttribute(name = "LoyalLevel")
            protected String loyalLevel;
            @XmlAttribute(name = "LoyalLevelCode")
            protected Integer loyalLevelCode;
            @XmlAttribute(name = "SingleVendorInd")
            protected String singleVendorInd;
            @XmlAttribute(name = "SignupDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar signupDate;
            @XmlAttribute(name = "EffectiveDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar effectiveDate;
            @XmlAttribute(name = "ExpireDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar expireDate;
            @XmlAttribute(name = "ExpireDateExclusiveIndicator")
            protected Boolean expireDateExclusiveIndicator;
            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Obtiene el valor de la propiedad shareSynchInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareSynchInd() {
                return shareSynchInd;
            }

            /**
             * Define el valor de la propiedad shareSynchInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareSynchInd(String value) {
                this.shareSynchInd = value;
            }

            /**
             * Obtiene el valor de la propiedad shareMarketInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareMarketInd() {
                return shareMarketInd;
            }

            /**
             * Define el valor de la propiedad shareMarketInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareMarketInd(String value) {
                this.shareMarketInd = value;
            }

            /**
             * Obtiene el valor de la propiedad programID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramID() {
                return programID;
            }

            /**
             * Define el valor de la propiedad programID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramID(String value) {
                this.programID = value;
            }

            /**
             * Obtiene el valor de la propiedad membershipID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMembershipID() {
                return membershipID;
            }

            /**
             * Define el valor de la propiedad membershipID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMembershipID(String value) {
                this.membershipID = value;
            }

            /**
             * Obtiene el valor de la propiedad travelSector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelSector() {
                return travelSector;
            }

            /**
             * Define el valor de la propiedad travelSector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelSector(String value) {
                this.travelSector = value;
            }

            /**
             * Gets the value of the vendorCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getVendorCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getVendorCode() {
                if (vendorCode == null) {
                    vendorCode = new ArrayList<String>();
                }
                return this.vendorCode;
            }

            /**
             * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPrimaryLoyaltyIndicator() {
                return primaryLoyaltyIndicator;
            }

            /**
             * Define el valor de la propiedad primaryLoyaltyIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPrimaryLoyaltyIndicator(Boolean value) {
                this.primaryLoyaltyIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAllianceLoyaltyLevelName() {
                return allianceLoyaltyLevelName;
            }

            /**
             * Define el valor de la propiedad allianceLoyaltyLevelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAllianceLoyaltyLevelName(String value) {
                this.allianceLoyaltyLevelName = value;
            }

            /**
             * Obtiene el valor de la propiedad customerType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerType() {
                return customerType;
            }

            /**
             * Define el valor de la propiedad customerType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerType(String value) {
                this.customerType = value;
            }

            /**
             * Obtiene el valor de la propiedad customerValue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerValue() {
                return customerValue;
            }

            /**
             * Define el valor de la propiedad customerValue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerValue(String value) {
                this.customerValue = value;
            }

            /**
             * Obtiene el valor de la propiedad password.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassword() {
                return password;
            }

            /**
             * Define el valor de la propiedad password.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassword(String value) {
                this.password = value;
            }

            /**
             * Obtiene el valor de la propiedad loyalLevel.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLoyalLevel() {
                return loyalLevel;
            }

            /**
             * Define el valor de la propiedad loyalLevel.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLoyalLevel(String value) {
                this.loyalLevel = value;
            }

            /**
             * Obtiene el valor de la propiedad loyalLevelCode.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getLoyalLevelCode() {
                return loyalLevelCode;
            }

            /**
             * Define el valor de la propiedad loyalLevelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setLoyalLevelCode(Integer value) {
                this.loyalLevelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad singleVendorInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSingleVendorInd() {
                return singleVendorInd;
            }

            /**
             * Define el valor de la propiedad singleVendorInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSingleVendorInd(String value) {
                this.singleVendorInd = value;
            }

            /**
             * Obtiene el valor de la propiedad signupDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getSignupDate() {
                return signupDate;
            }

            /**
             * Define el valor de la propiedad signupDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setSignupDate(XMLGregorianCalendar value) {
                this.signupDate = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEffectiveDate() {
                return effectiveDate;
            }

            /**
             * Define el valor de la propiedad effectiveDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEffectiveDate(XMLGregorianCalendar value) {
                this.effectiveDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpireDate() {
                return expireDate;
            }

            /**
             * Define el valor de la propiedad expireDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpireDate(XMLGregorianCalendar value) {
                this.expireDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExpireDateExclusiveIndicator() {
                return expireDateExclusiveIndicator;
            }

            /**
             * Define el valor de la propiedad expireDateExclusiveIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExpireDateExclusiveIndicator(Boolean value) {
                this.expireDateExclusiveIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RateQualifierCoreGroup"/&amp;gt;
         *       &amp;lt;attribute name="RateAuthorizationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="VendorRateID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="RateModifiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class RateQualifier {

            @XmlAttribute(name = "RateAuthorizationCode")
            protected String rateAuthorizationCode;
            @XmlAttribute(name = "VendorRateID")
            protected String vendorRateID;
            @XmlAttribute(name = "RateModifiedInd")
            protected Boolean rateModifiedInd;
            @XmlAttribute(name = "TravelPurpose")
            protected String travelPurpose;
            @XmlAttribute(name = "RateCategory")
            protected String rateCategory;
            @XmlAttribute(name = "CorpDiscountNmbr")
            protected String corpDiscountNmbr;
            @XmlAttribute(name = "PromotionCode")
            protected String promotionCode;
            @XmlAttribute(name = "PromotionVendorCode")
            protected List<String> promotionVendorCode;
            @XmlAttribute(name = "RateQualifier")
            protected String rateQualifier;
            @XmlAttribute(name = "RatePeriod")
            protected RatePeriodSimpleType ratePeriod;
            @XmlAttribute(name = "GuaranteedInd")
            protected Boolean guaranteedInd;

            /**
             * Obtiene el valor de la propiedad rateAuthorizationCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRateAuthorizationCode() {
                return rateAuthorizationCode;
            }

            /**
             * Define el valor de la propiedad rateAuthorizationCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRateAuthorizationCode(String value) {
                this.rateAuthorizationCode = value;
            }

            /**
             * Obtiene el valor de la propiedad vendorRateID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVendorRateID() {
                return vendorRateID;
            }

            /**
             * Define el valor de la propiedad vendorRateID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVendorRateID(String value) {
                this.vendorRateID = value;
            }

            /**
             * Obtiene el valor de la propiedad rateModifiedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRateModifiedInd() {
                return rateModifiedInd;
            }

            /**
             * Define el valor de la propiedad rateModifiedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRateModifiedInd(Boolean value) {
                this.rateModifiedInd = value;
            }

            /**
             * Obtiene el valor de la propiedad travelPurpose.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelPurpose() {
                return travelPurpose;
            }

            /**
             * Define el valor de la propiedad travelPurpose.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelPurpose(String value) {
                this.travelPurpose = value;
            }

            /**
             * Obtiene el valor de la propiedad rateCategory.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRateCategory() {
                return rateCategory;
            }

            /**
             * Define el valor de la propiedad rateCategory.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRateCategory(String value) {
                this.rateCategory = value;
            }

            /**
             * Obtiene el valor de la propiedad corpDiscountNmbr.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpDiscountNmbr() {
                return corpDiscountNmbr;
            }

            /**
             * Define el valor de la propiedad corpDiscountNmbr.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpDiscountNmbr(String value) {
                this.corpDiscountNmbr = value;
            }

            /**
             * Obtiene el valor de la propiedad promotionCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPromotionCode() {
                return promotionCode;
            }

            /**
             * Define el valor de la propiedad promotionCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPromotionCode(String value) {
                this.promotionCode = value;
            }

            /**
             * Gets the value of the promotionVendorCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionVendorCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotionVendorCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPromotionVendorCode() {
                if (promotionVendorCode == null) {
                    promotionVendorCode = new ArrayList<String>();
                }
                return this.promotionVendorCode;
            }

            /**
             * Obtiene el valor de la propiedad rateQualifier.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRateQualifier() {
                return rateQualifier;
            }

            /**
             * Define el valor de la propiedad rateQualifier.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRateQualifier(String value) {
                this.rateQualifier = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePeriod.
             * 
             * @return
             *     possible object is
             *     {@link RatePeriodSimpleType }
             *     
             */
            public RatePeriodSimpleType getRatePeriod() {
                return ratePeriod;
            }

            /**
             * Define el valor de la propiedad ratePeriod.
             * 
             * @param value
             *     allowed object is
             *     {@link RatePeriodSimpleType }
             *     
             */
            public void setRatePeriod(RatePeriodSimpleType value) {
                this.ratePeriod = value;
            }

            /**
             * Obtiene el valor de la propiedad guaranteedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isGuaranteedInd() {
                return guaranteedInd;
            }

            /**
             * Define el valor de la propiedad guaranteedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setGuaranteedInd(Boolean value) {
                this.guaranteedInd = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="SpecialEquipPref" maxOccurs="15"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentPrefGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "specialEquipPref"
        })
        public static class SpecialEquipPrefs {

            @XmlElement(name = "SpecialEquipPref", required = true)
            protected List<OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs.SpecialEquipPref> specialEquipPref;

            /**
             * Gets the value of the specialEquipPref property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specialEquipPref property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSpecialEquipPref().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs.SpecialEquipPref }
             * 
             * 
             */
            public List<OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs.SpecialEquipPref> getSpecialEquipPref() {
                if (specialEquipPref == null) {
                    specialEquipPref = new ArrayList<OTAVehRateRuleRQ.RentalInfo.SpecialEquipPrefs.SpecialEquipPref>();
                }
                return this.specialEquipPref;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleEquipmentPrefGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class SpecialEquipPref {

                @XmlAttribute(name = "EquipType", required = true)
                protected String equipType;
                @XmlAttribute(name = "Quantity")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger quantity;
                @XmlAttribute(name = "PreferLevel")
                protected PreferLevelType preferLevel;
                @XmlAttribute(name = "Action")
                protected ActionType action;

                /**
                 * Obtiene el valor de la propiedad equipType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEquipType() {
                    return equipType;
                }

                /**
                 * Define el valor de la propiedad equipType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEquipType(String value) {
                    this.equipType = value;
                }

                /**
                 * Obtiene el valor de la propiedad quantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getQuantity() {
                    return quantity;
                }

                /**
                 * Define el valor de la propiedad quantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setQuantity(BigInteger value) {
                    this.quantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad preferLevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PreferLevelType }
                 *     
                 */
                public PreferLevelType getPreferLevel() {
                    return preferLevel;
                }

                /**
                 * Define el valor de la propiedad preferLevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PreferLevelType }
                 *     
                 */
                public void setPreferLevel(PreferLevelType value) {
                    this.preferLevel = value;
                }

                /**
                 * Obtiene el valor de la propiedad action.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionType }
                 *     
                 */
                public ActionType getAction() {
                    return action;
                }

                /**
                 * Define el valor de la propiedad action.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionType }
                 *     
                 */
                public void setAction(ActionType value) {
                    this.action = value;
                }

            }

        }

    }

}
