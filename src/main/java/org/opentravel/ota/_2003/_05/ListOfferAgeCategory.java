
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferAgeCategory.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferAgeCategory"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Adult"/&amp;gt;
 *     &amp;lt;enumeration value="Child"/&amp;gt;
 *     &amp;lt;enumeration value="Infant"/&amp;gt;
 *     &amp;lt;enumeration value="Over10"/&amp;gt;
 *     &amp;lt;enumeration value="Over12"/&amp;gt;
 *     &amp;lt;enumeration value="Over21"/&amp;gt;
 *     &amp;lt;enumeration value="Over65"/&amp;gt;
 *     &amp;lt;enumeration value="Senior"/&amp;gt;
 *     &amp;lt;enumeration value="Under2"/&amp;gt;
 *     &amp;lt;enumeration value="Under10"/&amp;gt;
 *     &amp;lt;enumeration value="Under12"/&amp;gt;
 *     &amp;lt;enumeration value="Under17"/&amp;gt;
 *     &amp;lt;enumeration value="Under18"/&amp;gt;
 *     &amp;lt;enumeration value="Under21"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferAgeCategory")
@XmlEnum
public enum ListOfferAgeCategory {

    @XmlEnumValue("Adult")
    ADULT("Adult"),
    @XmlEnumValue("Child")
    CHILD("Child"),
    @XmlEnumValue("Infant")
    INFANT("Infant"),
    @XmlEnumValue("Over10")
    OVER_10("Over10"),
    @XmlEnumValue("Over12")
    OVER_12("Over12"),
    @XmlEnumValue("Over21")
    OVER_21("Over21"),
    @XmlEnumValue("Over65")
    OVER_65("Over65"),
    @XmlEnumValue("Senior")
    SENIOR("Senior"),
    @XmlEnumValue("Under2")
    UNDER_2("Under2"),
    @XmlEnumValue("Under10")
    UNDER_10("Under10"),
    @XmlEnumValue("Under12")
    UNDER_12("Under12"),
    @XmlEnumValue("Under17")
    UNDER_17("Under17"),
    @XmlEnumValue("Under18")
    UNDER_18("Under18"),
    @XmlEnumValue("Under21")
    UNDER_21("Under21"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferAgeCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferAgeCategory fromValue(String v) {
        for (ListOfferAgeCategory c: ListOfferAgeCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
