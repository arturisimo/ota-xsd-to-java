
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferTypesRequested.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferTypesRequested"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Activity"/&amp;gt;
 *     &amp;lt;enumeration value="Flight"/&amp;gt;
 *     &amp;lt;enumeration value="Cruise"/&amp;gt;
 *     &amp;lt;enumeration value="DayTour"/&amp;gt;
 *     &amp;lt;enumeration value="GroundTransportation"/&amp;gt;
 *     &amp;lt;enumeration value="Lodging"/&amp;gt;
 *     &amp;lt;enumeration value="Merchandise"/&amp;gt;
 *     &amp;lt;enumeration value="PackageTour"/&amp;gt;
 *     &amp;lt;enumeration value="Rail"/&amp;gt;
 *     &amp;lt;enumeration value="VehicleRental"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferTypesRequested")
@XmlEnum
public enum ListOfferTypesRequested {

    @XmlEnumValue("Activity")
    ACTIVITY("Activity"),
    @XmlEnumValue("Flight")
    FLIGHT("Flight"),
    @XmlEnumValue("Cruise")
    CRUISE("Cruise"),
    @XmlEnumValue("DayTour")
    DAY_TOUR("DayTour"),
    @XmlEnumValue("GroundTransportation")
    GROUND_TRANSPORTATION("GroundTransportation"),
    @XmlEnumValue("Lodging")
    LODGING("Lodging"),
    @XmlEnumValue("Merchandise")
    MERCHANDISE("Merchandise"),
    @XmlEnumValue("PackageTour")
    PACKAGE_TOUR("PackageTour"),
    @XmlEnumValue("Rail")
    RAIL("Rail"),
    @XmlEnumValue("VehicleRental")
    VEHICLE_RENTAL("VehicleRental"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferTypesRequested(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferTypesRequested fromValue(String v) {
        for (ListOfferTypesRequested c: ListOfferTypesRequested.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
