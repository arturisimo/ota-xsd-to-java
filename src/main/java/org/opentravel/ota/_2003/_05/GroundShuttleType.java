
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Shuttle service information.
 * 
 * &lt;p&gt;Clase Java para GroundShuttleType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundShuttleType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Vehicle" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_VehCategory" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="VehicleSize" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;simpleContent&amp;gt;
 *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_VehSize"&amp;gt;
 *                           &amp;lt;attribute name="MaxPassengerCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="MaxBaggageCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/simpleContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="DisabilityInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ServiceType" type="{http://www.opentravel.org/OTA/2003/05}List_GroundServiceProvided" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ServiceLocation" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulePlusChargeType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Operator" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0" form="qualified"/&amp;gt;
 *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ShuttleRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="OriginInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DestinationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="TicketReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ReservationReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TicketIssueInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundShuttleType", propOrder = {
    "vehicle",
    "serviceType",
    "serviceLocation",
    "multimediaDescriptions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAGroundBookRQ.GroundReservation.Shuttle.class,
    org.opentravel.ota._2003._05.OTAGroundModifyRQ.Reservation.Shuttle.class
})
public class GroundShuttleType {

    @XmlElement(name = "Vehicle")
    protected GroundShuttleType.Vehicle vehicle;
    @XmlElement(name = "ServiceType")
    protected ListGroundServiceProvided serviceType;
    @XmlElement(name = "ServiceLocation")
    protected List<GroundShuttleType.ServiceLocation> serviceLocation;
    @XmlElement(name = "MultimediaDescriptions")
    protected MultimediaDescriptionsType multimediaDescriptions;
    @XmlAttribute(name = "TicketReqInd")
    protected Boolean ticketReqInd;
    @XmlAttribute(name = "ReservationReqInd")
    protected Boolean reservationReqInd;
    @XmlAttribute(name = "TicketIssueInd")
    protected Boolean ticketIssueInd;

    /**
     * Obtiene el valor de la propiedad vehicle.
     * 
     * @return
     *     possible object is
     *     {@link GroundShuttleType.Vehicle }
     *     
     */
    public GroundShuttleType.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Define el valor de la propiedad vehicle.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundShuttleType.Vehicle }
     *     
     */
    public void setVehicle(GroundShuttleType.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceType.
     * 
     * @return
     *     possible object is
     *     {@link ListGroundServiceProvided }
     *     
     */
    public ListGroundServiceProvided getServiceType() {
        return serviceType;
    }

    /**
     * Define el valor de la propiedad serviceType.
     * 
     * @param value
     *     allowed object is
     *     {@link ListGroundServiceProvided }
     *     
     */
    public void setServiceType(ListGroundServiceProvided value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the serviceLocation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the serviceLocation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getServiceLocation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GroundShuttleType.ServiceLocation }
     * 
     * 
     */
    public List<GroundShuttleType.ServiceLocation> getServiceLocation() {
        if (serviceLocation == null) {
            serviceLocation = new ArrayList<GroundShuttleType.ServiceLocation>();
        }
        return this.serviceLocation;
    }

    /**
     * Obtiene el valor de la propiedad multimediaDescriptions.
     * 
     * @return
     *     possible object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public MultimediaDescriptionsType getMultimediaDescriptions() {
        return multimediaDescriptions;
    }

    /**
     * Define el valor de la propiedad multimediaDescriptions.
     * 
     * @param value
     *     allowed object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public void setMultimediaDescriptions(MultimediaDescriptionsType value) {
        this.multimediaDescriptions = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketReqInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTicketReqInd() {
        return ticketReqInd;
    }

    /**
     * Define el valor de la propiedad ticketReqInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketReqInd(Boolean value) {
        this.ticketReqInd = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationReqInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReservationReqInd() {
        return reservationReqInd;
    }

    /**
     * Define el valor de la propiedad reservationReqInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReservationReqInd(Boolean value) {
        this.reservationReqInd = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketIssueInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTicketIssueInd() {
        return ticketIssueInd;
    }

    /**
     * Define el valor de la propiedad ticketIssueInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketIssueInd(Boolean value) {
        this.ticketIssueInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulePlusChargeType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Operator" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0" form="qualified"/&amp;gt;
     *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ShuttleRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="OriginInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DestinationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "schedule",
        "operator",
        "contact"
    })
    public static class ServiceLocation
        extends GroundLocationType
    {

        @XmlElement(name = "Schedule")
        protected List<OperationSchedulePlusChargeType> schedule;
        @XmlElement(name = "Operator")
        protected CompanyNameType operator;
        @XmlElement(name = "Contact")
        protected List<ContactPersonType> contact;
        @XmlAttribute(name = "Sequence")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger sequence;
        @XmlAttribute(name = "PickupInd")
        protected Boolean pickupInd;
        @XmlAttribute(name = "ShuttleRPH")
        protected String shuttleRPH;
        @XmlAttribute(name = "OriginInd")
        protected Boolean originInd;
        @XmlAttribute(name = "DestinationInd")
        protected Boolean destinationInd;

        /**
         * Gets the value of the schedule property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the schedule property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSchedule().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OperationSchedulePlusChargeType }
         * 
         * 
         */
        public List<OperationSchedulePlusChargeType> getSchedule() {
            if (schedule == null) {
                schedule = new ArrayList<OperationSchedulePlusChargeType>();
            }
            return this.schedule;
        }

        /**
         * Obtiene el valor de la propiedad operator.
         * 
         * @return
         *     possible object is
         *     {@link CompanyNameType }
         *     
         */
        public CompanyNameType getOperator() {
            return operator;
        }

        /**
         * Define el valor de la propiedad operator.
         * 
         * @param value
         *     allowed object is
         *     {@link CompanyNameType }
         *     
         */
        public void setOperator(CompanyNameType value) {
            this.operator = value;
        }

        /**
         * Gets the value of the contact property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getContact().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ContactPersonType }
         * 
         * 
         */
        public List<ContactPersonType> getContact() {
            if (contact == null) {
                contact = new ArrayList<ContactPersonType>();
            }
            return this.contact;
        }

        /**
         * Obtiene el valor de la propiedad sequence.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequence() {
            return sequence;
        }

        /**
         * Define el valor de la propiedad sequence.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequence(BigInteger value) {
            this.sequence = value;
        }

        /**
         * Obtiene el valor de la propiedad pickupInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPickupInd() {
            return pickupInd;
        }

        /**
         * Define el valor de la propiedad pickupInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPickupInd(Boolean value) {
            this.pickupInd = value;
        }

        /**
         * Obtiene el valor de la propiedad shuttleRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShuttleRPH() {
            return shuttleRPH;
        }

        /**
         * Define el valor de la propiedad shuttleRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShuttleRPH(String value) {
            this.shuttleRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad originInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOriginInd() {
            return originInd;
        }

        /**
         * Define el valor de la propiedad originInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOriginInd(Boolean value) {
            this.originInd = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDestinationInd() {
            return destinationInd;
        }

        /**
         * Define el valor de la propiedad destinationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDestinationInd(Boolean value) {
            this.destinationInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_VehCategory" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="VehicleSize" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_VehSize"&amp;gt;
     *                 &amp;lt;attribute name="MaxPassengerCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="MaxBaggageCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="DisabilityInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "type",
        "vehicleSize"
    })
    public static class Vehicle {

        @XmlElement(name = "Type")
        protected ListVehCategory type;
        @XmlElement(name = "VehicleSize")
        protected GroundShuttleType.Vehicle.VehicleSize vehicleSize;
        @XmlAttribute(name = "DisabilityInd", required = true)
        protected boolean disabilityInd;
        @XmlAttribute(name = "ID")
        protected String id;

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link ListVehCategory }
         *     
         */
        public ListVehCategory getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link ListVehCategory }
         *     
         */
        public void setType(ListVehCategory value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad vehicleSize.
         * 
         * @return
         *     possible object is
         *     {@link GroundShuttleType.Vehicle.VehicleSize }
         *     
         */
        public GroundShuttleType.Vehicle.VehicleSize getVehicleSize() {
            return vehicleSize;
        }

        /**
         * Define el valor de la propiedad vehicleSize.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundShuttleType.Vehicle.VehicleSize }
         *     
         */
        public void setVehicleSize(GroundShuttleType.Vehicle.VehicleSize value) {
            this.vehicleSize = value;
        }

        /**
         * Obtiene el valor de la propiedad disabilityInd.
         * 
         */
        public boolean isDisabilityInd() {
            return disabilityInd;
        }

        /**
         * Define el valor de la propiedad disabilityInd.
         * 
         */
        public void setDisabilityInd(boolean value) {
            this.disabilityInd = value;
        }

        /**
         * Obtiene el valor de la propiedad id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getID() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setID(String value) {
            this.id = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_VehSize"&amp;gt;
         *       &amp;lt;attribute name="MaxPassengerCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="MaxBaggageCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class VehicleSize
            extends ListVehSize
        {

            @XmlAttribute(name = "MaxPassengerCapacity")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger maxPassengerCapacity;
            @XmlAttribute(name = "MaxBaggageCapacity")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger maxBaggageCapacity;

            /**
             * Obtiene el valor de la propiedad maxPassengerCapacity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxPassengerCapacity() {
                return maxPassengerCapacity;
            }

            /**
             * Define el valor de la propiedad maxPassengerCapacity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxPassengerCapacity(BigInteger value) {
                this.maxPassengerCapacity = value;
            }

            /**
             * Obtiene el valor de la propiedad maxBaggageCapacity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxBaggageCapacity() {
                return maxBaggageCapacity;
            }

            /**
             * Define el valor de la propiedad maxBaggageCapacity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxBaggageCapacity(BigInteger value) {
                this.maxBaggageCapacity = value;
            }

        }

    }

}
