
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para ActionType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="ActionType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Add-Update"/&amp;gt;
 *     &amp;lt;enumeration value="Cancel"/&amp;gt;
 *     &amp;lt;enumeration value="Delete"/&amp;gt;
 *     &amp;lt;enumeration value="Add"/&amp;gt;
 *     &amp;lt;enumeration value="Replace"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "ActionType")
@XmlEnum
public enum ActionType {


    /**
     * Typically used to add an item where it does not exist or to update an item where it does exist.
     * 
     */
    @XmlEnumValue("Add-Update")
    ADD_UPDATE("Add-Update"),

    /**
     * Typically used to cancel an existing item.
     * 
     */
    @XmlEnumValue("Cancel")
    CANCEL("Cancel"),

    /**
     * Typically used to remove specified data.
     * 
     */
    @XmlEnumValue("Delete")
    DELETE("Delete"),

    /**
     * Typically used to add data whether data already exists or not.
     * 
     */
    @XmlEnumValue("Add")
    ADD("Add"),

    /**
     * Typically used to overlay existing data.
     * 
     */
    @XmlEnumValue("Replace")
    REPLACE("Replace");
    private final String value;

    ActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionType fromValue(String v) {
        for (ActionType c: ActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
