
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferRateCategory.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferRateCategory"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Association"/&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="Convention"/&amp;gt;
 *     &amp;lt;enumeration value="Corporate"/&amp;gt;
 *     &amp;lt;enumeration value="Employee"/&amp;gt;
 *     &amp;lt;enumeration value="Government"/&amp;gt;
 *     &amp;lt;enumeration value="Leisure"/&amp;gt;
 *     &amp;lt;enumeration value="Negotiated"/&amp;gt;
 *     &amp;lt;enumeration value="Promotional"/&amp;gt;
 *     &amp;lt;enumeration value="Standard"/&amp;gt;
 *     &amp;lt;enumeration value="VIP"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferRateCategory")
@XmlEnum
public enum ListOfferRateCategory {

    @XmlEnumValue("Association")
    ASSOCIATION("Association"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("Convention")
    CONVENTION("Convention"),
    @XmlEnumValue("Corporate")
    CORPORATE("Corporate"),
    @XmlEnumValue("Employee")
    EMPLOYEE("Employee"),
    @XmlEnumValue("Government")
    GOVERNMENT("Government"),
    @XmlEnumValue("Leisure")
    LEISURE("Leisure"),
    @XmlEnumValue("Negotiated")
    NEGOTIATED("Negotiated"),
    @XmlEnumValue("Promotional")
    PROMOTIONAL("Promotional"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    VIP("VIP"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferRateCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferRateCategory fromValue(String v) {
        for (ListOfferRateCategory c: ListOfferRateCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
