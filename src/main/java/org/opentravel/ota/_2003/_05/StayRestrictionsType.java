
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type defining Min and Max Stay Restrictions.
 * 
 * &lt;p&gt;Clase Java para StayRestrictionsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="StayRestrictionsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence minOccurs="0"&amp;gt;
 *         &amp;lt;element name="MinimumStay" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="ReturnTimeOfDay" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="MinStay" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                 &amp;lt;attribute name="StayUnit" type="{http://www.opentravel.org/OTA/2003/05}StayUnitType" /&amp;gt;
 *                 &amp;lt;attribute name="MinStayDate" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="ComplicatedRulesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MaximumStay" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="ReturnType"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="C"/&amp;gt;
 *                       &amp;lt;enumeration value="S"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="ReturnTimeOfDay" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="MaxStay" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                 &amp;lt;attribute name="StayUnit" type="{http://www.opentravel.org/OTA/2003/05}StayUnitType" /&amp;gt;
 *                 &amp;lt;attribute name="MaxStayDate" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="ComplicatedRulesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="StayRestrictionsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StayRestrictionsType", propOrder = {
    "minimumStay",
    "maximumStay",
    "tpaExtensions"
})
public class StayRestrictionsType {

    @XmlElement(name = "MinimumStay")
    protected StayRestrictionsType.MinimumStay minimumStay;
    @XmlElement(name = "MaximumStay")
    protected StayRestrictionsType.MaximumStay maximumStay;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "StayRestrictionsInd")
    protected Boolean stayRestrictionsInd;

    /**
     * Obtiene el valor de la propiedad minimumStay.
     * 
     * @return
     *     possible object is
     *     {@link StayRestrictionsType.MinimumStay }
     *     
     */
    public StayRestrictionsType.MinimumStay getMinimumStay() {
        return minimumStay;
    }

    /**
     * Define el valor de la propiedad minimumStay.
     * 
     * @param value
     *     allowed object is
     *     {@link StayRestrictionsType.MinimumStay }
     *     
     */
    public void setMinimumStay(StayRestrictionsType.MinimumStay value) {
        this.minimumStay = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumStay.
     * 
     * @return
     *     possible object is
     *     {@link StayRestrictionsType.MaximumStay }
     *     
     */
    public StayRestrictionsType.MaximumStay getMaximumStay() {
        return maximumStay;
    }

    /**
     * Define el valor de la propiedad maximumStay.
     * 
     * @param value
     *     allowed object is
     *     {@link StayRestrictionsType.MaximumStay }
     *     
     */
    public void setMaximumStay(StayRestrictionsType.MaximumStay value) {
        this.maximumStay = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad stayRestrictionsInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStayRestrictionsInd() {
        return stayRestrictionsInd;
    }

    /**
     * Define el valor de la propiedad stayRestrictionsInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStayRestrictionsInd(Boolean value) {
        this.stayRestrictionsInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="ReturnType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="C"/&amp;gt;
     *             &amp;lt;enumeration value="S"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="ReturnTimeOfDay" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="MaxStay" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *       &amp;lt;attribute name="StayUnit" type="{http://www.opentravel.org/OTA/2003/05}StayUnitType" /&amp;gt;
     *       &amp;lt;attribute name="MaxStayDate" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="ComplicatedRulesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MaximumStay {

        @XmlAttribute(name = "ReturnType")
        protected String returnType;
        @XmlAttribute(name = "ReturnTimeOfDay")
        protected String returnTimeOfDay;
        @XmlAttribute(name = "MaxStay")
        protected Integer maxStay;
        @XmlAttribute(name = "StayUnit")
        protected StayUnitType stayUnit;
        @XmlAttribute(name = "MaxStayDate")
        protected String maxStayDate;
        @XmlAttribute(name = "ComplicatedRulesInd")
        protected Boolean complicatedRulesInd;

        /**
         * Obtiene el valor de la propiedad returnType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnType() {
            return returnType;
        }

        /**
         * Define el valor de la propiedad returnType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnType(String value) {
            this.returnType = value;
        }

        /**
         * Obtiene el valor de la propiedad returnTimeOfDay.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnTimeOfDay() {
            return returnTimeOfDay;
        }

        /**
         * Define el valor de la propiedad returnTimeOfDay.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnTimeOfDay(String value) {
            this.returnTimeOfDay = value;
        }

        /**
         * Obtiene el valor de la propiedad maxStay.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxStay() {
            return maxStay;
        }

        /**
         * Define el valor de la propiedad maxStay.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxStay(Integer value) {
            this.maxStay = value;
        }

        /**
         * Obtiene el valor de la propiedad stayUnit.
         * 
         * @return
         *     possible object is
         *     {@link StayUnitType }
         *     
         */
        public StayUnitType getStayUnit() {
            return stayUnit;
        }

        /**
         * Define el valor de la propiedad stayUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link StayUnitType }
         *     
         */
        public void setStayUnit(StayUnitType value) {
            this.stayUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad maxStayDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaxStayDate() {
            return maxStayDate;
        }

        /**
         * Define el valor de la propiedad maxStayDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaxStayDate(String value) {
            this.maxStayDate = value;
        }

        /**
         * Obtiene el valor de la propiedad complicatedRulesInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isComplicatedRulesInd() {
            return complicatedRulesInd;
        }

        /**
         * Define el valor de la propiedad complicatedRulesInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setComplicatedRulesInd(Boolean value) {
            this.complicatedRulesInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="ReturnTimeOfDay" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="MinStay" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *       &amp;lt;attribute name="StayUnit" type="{http://www.opentravel.org/OTA/2003/05}StayUnitType" /&amp;gt;
     *       &amp;lt;attribute name="MinStayDate" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="ComplicatedRulesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MinimumStay {

        @XmlAttribute(name = "ReturnTimeOfDay")
        protected String returnTimeOfDay;
        @XmlAttribute(name = "MinStay")
        protected Integer minStay;
        @XmlAttribute(name = "StayUnit")
        protected StayUnitType stayUnit;
        @XmlAttribute(name = "MinStayDate")
        protected String minStayDate;
        @XmlAttribute(name = "ComplicatedRulesInd")
        protected Boolean complicatedRulesInd;

        /**
         * Obtiene el valor de la propiedad returnTimeOfDay.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnTimeOfDay() {
            return returnTimeOfDay;
        }

        /**
         * Define el valor de la propiedad returnTimeOfDay.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnTimeOfDay(String value) {
            this.returnTimeOfDay = value;
        }

        /**
         * Obtiene el valor de la propiedad minStay.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMinStay() {
            return minStay;
        }

        /**
         * Define el valor de la propiedad minStay.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMinStay(Integer value) {
            this.minStay = value;
        }

        /**
         * Obtiene el valor de la propiedad stayUnit.
         * 
         * @return
         *     possible object is
         *     {@link StayUnitType }
         *     
         */
        public StayUnitType getStayUnit() {
            return stayUnit;
        }

        /**
         * Define el valor de la propiedad stayUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link StayUnitType }
         *     
         */
        public void setStayUnit(StayUnitType value) {
            this.stayUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad minStayDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMinStayDate() {
            return minStayDate;
        }

        /**
         * Define el valor de la propiedad minStayDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMinStayDate(String value) {
            this.minStayDate = value;
        }

        /**
         * Obtiene el valor de la propiedad complicatedRulesInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isComplicatedRulesInd() {
            return complicatedRulesInd;
        }

        /**
         * Define el valor de la propiedad complicatedRulesInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setComplicatedRulesInd(Boolean value) {
            this.complicatedRulesInd = value;
        }

    }

}
