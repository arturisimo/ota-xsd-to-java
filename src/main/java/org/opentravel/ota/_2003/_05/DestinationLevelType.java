
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para DestinationLevelType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="DestinationLevelType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Continent"/&amp;gt;
 *     &amp;lt;enumeration value="Country"/&amp;gt;
 *     &amp;lt;enumeration value="State"/&amp;gt;
 *     &amp;lt;enumeration value="Area"/&amp;gt;
 *     &amp;lt;enumeration value="SubArea"/&amp;gt;
 *     &amp;lt;enumeration value="Resort"/&amp;gt;
 *     &amp;lt;enumeration value="District"/&amp;gt;
 *     &amp;lt;enumeration value="Region"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "DestinationLevelType")
@XmlEnum
public enum DestinationLevelType {

    @XmlEnumValue("Continent")
    CONTINENT("Continent"),
    @XmlEnumValue("Country")
    COUNTRY("Country"),
    @XmlEnumValue("State")
    STATE("State"),
    @XmlEnumValue("Area")
    AREA("Area"),
    @XmlEnumValue("SubArea")
    SUB_AREA("SubArea"),
    @XmlEnumValue("Resort")
    RESORT("Resort"),
    @XmlEnumValue("District")
    DISTRICT("District"),
    @XmlEnumValue("Region")
    REGION("Region");
    private final String value;

    DestinationLevelType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DestinationLevelType fromValue(String v) {
        for (DestinationLevelType c: DestinationLevelType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
