
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Contains pre-selected components and information about the guests and payments made on the existing reservation.
 * 
 * &lt;p&gt;Clase Java para DynamicPkgType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DynamicPkgType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Components" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="HotelComponent" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelReservationType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AirComponent" type="{http://www.opentravel.org/OTA/2003/05}AirComponentType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="PackageOptionComponent" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="PackageOptions" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="PackageOption" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
 *                           &amp;lt;attribute name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="CarComponent" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Vehicle" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleReservationRQCoreType"&amp;gt;
 *                                     &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
 *                           &amp;lt;attribute name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ResGuests" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ResGuest" type="{http://www.opentravel.org/OTA/2003/05}DynamicPkgGuestType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="GlobalInfo" type="{http://www.opentravel.org/OTA/2003/05}DynamicPkgGlobalInfoType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgActionStatusGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicPkgType", propOrder = {
    "components",
    "resGuests",
    "globalInfo"
})
public class DynamicPkgType {

    @XmlElement(name = "Components")
    protected DynamicPkgType.Components components;
    @XmlElement(name = "ResGuests")
    protected DynamicPkgType.ResGuests resGuests;
    @XmlElement(name = "GlobalInfo")
    protected DynamicPkgGlobalInfoType globalInfo;
    @XmlAttribute(name = "DynamicPkgAction")
    protected TransactionActionType dynamicPkgAction;
    @XmlAttribute(name = "DynamicPkgStatus")
    protected TransactionStatusType dynamicPkgStatus;

    /**
     * Obtiene el valor de la propiedad components.
     * 
     * @return
     *     possible object is
     *     {@link DynamicPkgType.Components }
     *     
     */
    public DynamicPkgType.Components getComponents() {
        return components;
    }

    /**
     * Define el valor de la propiedad components.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicPkgType.Components }
     *     
     */
    public void setComponents(DynamicPkgType.Components value) {
        this.components = value;
    }

    /**
     * Obtiene el valor de la propiedad resGuests.
     * 
     * @return
     *     possible object is
     *     {@link DynamicPkgType.ResGuests }
     *     
     */
    public DynamicPkgType.ResGuests getResGuests() {
        return resGuests;
    }

    /**
     * Define el valor de la propiedad resGuests.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicPkgType.ResGuests }
     *     
     */
    public void setResGuests(DynamicPkgType.ResGuests value) {
        this.resGuests = value;
    }

    /**
     * Obtiene el valor de la propiedad globalInfo.
     * 
     * @return
     *     possible object is
     *     {@link DynamicPkgGlobalInfoType }
     *     
     */
    public DynamicPkgGlobalInfoType getGlobalInfo() {
        return globalInfo;
    }

    /**
     * Define el valor de la propiedad globalInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicPkgGlobalInfoType }
     *     
     */
    public void setGlobalInfo(DynamicPkgGlobalInfoType value) {
        this.globalInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad dynamicPkgAction.
     * 
     * @return
     *     possible object is
     *     {@link TransactionActionType }
     *     
     */
    public TransactionActionType getDynamicPkgAction() {
        return dynamicPkgAction;
    }

    /**
     * Define el valor de la propiedad dynamicPkgAction.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionActionType }
     *     
     */
    public void setDynamicPkgAction(TransactionActionType value) {
        this.dynamicPkgAction = value;
    }

    /**
     * Obtiene el valor de la propiedad dynamicPkgStatus.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusType }
     *     
     */
    public TransactionStatusType getDynamicPkgStatus() {
        return dynamicPkgStatus;
    }

    /**
     * Define el valor de la propiedad dynamicPkgStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusType }
     *     
     */
    public void setDynamicPkgStatus(TransactionStatusType value) {
        this.dynamicPkgStatus = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="HotelComponent" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelReservationType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AirComponent" type="{http://www.opentravel.org/OTA/2003/05}AirComponentType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PackageOptionComponent" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="PackageOptions" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PackageOption" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
     *                 &amp;lt;attribute name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CarComponent" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Vehicle" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleReservationRQCoreType"&amp;gt;
     *                           &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
     *                 &amp;lt;attribute name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotelComponent",
        "airComponent",
        "packageOptionComponent",
        "carComponent"
    })
    public static class Components {

        @XmlElement(name = "HotelComponent")
        protected List<DynamicPkgType.Components.HotelComponent> hotelComponent;
        @XmlElement(name = "AirComponent")
        protected List<AirComponentType> airComponent;
        @XmlElement(name = "PackageOptionComponent")
        protected List<DynamicPkgType.Components.PackageOptionComponent> packageOptionComponent;
        @XmlElement(name = "CarComponent")
        protected List<DynamicPkgType.Components.CarComponent> carComponent;

        /**
         * Gets the value of the hotelComponent property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the hotelComponent property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getHotelComponent().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link DynamicPkgType.Components.HotelComponent }
         * 
         * 
         */
        public List<DynamicPkgType.Components.HotelComponent> getHotelComponent() {
            if (hotelComponent == null) {
                hotelComponent = new ArrayList<DynamicPkgType.Components.HotelComponent>();
            }
            return this.hotelComponent;
        }

        /**
         * Gets the value of the airComponent property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airComponent property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAirComponent().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirComponentType }
         * 
         * 
         */
        public List<AirComponentType> getAirComponent() {
            if (airComponent == null) {
                airComponent = new ArrayList<AirComponentType>();
            }
            return this.airComponent;
        }

        /**
         * Gets the value of the packageOptionComponent property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the packageOptionComponent property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPackageOptionComponent().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link DynamicPkgType.Components.PackageOptionComponent }
         * 
         * 
         */
        public List<DynamicPkgType.Components.PackageOptionComponent> getPackageOptionComponent() {
            if (packageOptionComponent == null) {
                packageOptionComponent = new ArrayList<DynamicPkgType.Components.PackageOptionComponent>();
            }
            return this.packageOptionComponent;
        }

        /**
         * Gets the value of the carComponent property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the carComponent property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCarComponent().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link DynamicPkgType.Components.CarComponent }
         * 
         * 
         */
        public List<DynamicPkgType.Components.CarComponent> getCarComponent() {
            if (carComponent == null) {
                carComponent = new ArrayList<DynamicPkgType.Components.CarComponent>();
            }
            return this.carComponent;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Vehicle" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleReservationRQCoreType"&amp;gt;
         *                 &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
         *       &amp;lt;attribute name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID",
            "vehicle"
        })
        public static class CarComponent {

            @XmlElement(name = "UniqueID")
            protected List<UniqueIDType> uniqueID;
            @XmlElement(name = "Vehicle")
            protected DynamicPkgType.Components.CarComponent.Vehicle vehicle;
            @XmlAttribute(name = "CreateDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar createDateTime;
            @XmlAttribute(name = "HoldDuration")
            protected Duration holdDuration;
            @XmlAttribute(name = "QuoteID")
            protected String quoteID;
            @XmlAttribute(name = "DynamicPkgAction")
            protected TransactionActionType dynamicPkgAction;
            @XmlAttribute(name = "DynamicPkgStatus")
            protected TransactionStatusType dynamicPkgStatus;

            /**
             * Gets the value of the uniqueID property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the uniqueID property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getUniqueID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link UniqueIDType }
             * 
             * 
             */
            public List<UniqueIDType> getUniqueID() {
                if (uniqueID == null) {
                    uniqueID = new ArrayList<UniqueIDType>();
                }
                return this.uniqueID;
            }

            /**
             * Obtiene el valor de la propiedad vehicle.
             * 
             * @return
             *     possible object is
             *     {@link DynamicPkgType.Components.CarComponent.Vehicle }
             *     
             */
            public DynamicPkgType.Components.CarComponent.Vehicle getVehicle() {
                return vehicle;
            }

            /**
             * Define el valor de la propiedad vehicle.
             * 
             * @param value
             *     allowed object is
             *     {@link DynamicPkgType.Components.CarComponent.Vehicle }
             *     
             */
            public void setVehicle(DynamicPkgType.Components.CarComponent.Vehicle value) {
                this.vehicle = value;
            }

            /**
             * Obtiene el valor de la propiedad createDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCreateDateTime() {
                return createDateTime;
            }

            /**
             * Define el valor de la propiedad createDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCreateDateTime(XMLGregorianCalendar value) {
                this.createDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad holdDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getHoldDuration() {
                return holdDuration;
            }

            /**
             * Define el valor de la propiedad holdDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setHoldDuration(Duration value) {
                this.holdDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad quoteID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuoteID() {
                return quoteID;
            }

            /**
             * Define el valor de la propiedad quoteID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuoteID(String value) {
                this.quoteID = value;
            }

            /**
             * Obtiene el valor de la propiedad dynamicPkgAction.
             * 
             * @return
             *     possible object is
             *     {@link TransactionActionType }
             *     
             */
            public TransactionActionType getDynamicPkgAction() {
                return dynamicPkgAction;
            }

            /**
             * Define el valor de la propiedad dynamicPkgAction.
             * 
             * @param value
             *     allowed object is
             *     {@link TransactionActionType }
             *     
             */
            public void setDynamicPkgAction(TransactionActionType value) {
                this.dynamicPkgAction = value;
            }

            /**
             * Obtiene el valor de la propiedad dynamicPkgStatus.
             * 
             * @return
             *     possible object is
             *     {@link TransactionStatusType }
             *     
             */
            public TransactionStatusType getDynamicPkgStatus() {
                return dynamicPkgStatus;
            }

            /**
             * Define el valor de la propiedad dynamicPkgStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link TransactionStatusType }
             *     
             */
            public void setDynamicPkgStatus(TransactionStatusType value) {
                this.dynamicPkgStatus = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleReservationRQCoreType"&amp;gt;
             *       &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Vehicle
                extends VehicleReservationRQCoreType
            {

                @XmlAttribute(name = "QuoteID")
                protected String quoteID;

                /**
                 * Obtiene el valor de la propiedad quoteID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getQuoteID() {
                    return quoteID;
                }

                /**
                 * Define el valor de la propiedad quoteID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setQuoteID(String value) {
                    this.quoteID = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelReservationType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class HotelComponent
            extends HotelReservationType
        {

            @XmlAttribute(name = "HoldDuration")
            protected Duration holdDuration;
            @XmlAttribute(name = "QuoteID")
            protected String quoteID;
            @XmlAttribute(name = "DynamicPkgAction")
            protected TransactionActionType dynamicPkgAction;
            @XmlAttribute(name = "DynamicPkgStatus")
            protected TransactionStatusType dynamicPkgStatus;

            /**
             * Obtiene el valor de la propiedad holdDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getHoldDuration() {
                return holdDuration;
            }

            /**
             * Define el valor de la propiedad holdDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setHoldDuration(Duration value) {
                this.holdDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad quoteID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuoteID() {
                return quoteID;
            }

            /**
             * Define el valor de la propiedad quoteID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuoteID(String value) {
                this.quoteID = value;
            }

            /**
             * Obtiene el valor de la propiedad dynamicPkgAction.
             * 
             * @return
             *     possible object is
             *     {@link TransactionActionType }
             *     
             */
            public TransactionActionType getDynamicPkgAction() {
                return dynamicPkgAction;
            }

            /**
             * Define el valor de la propiedad dynamicPkgAction.
             * 
             * @param value
             *     allowed object is
             *     {@link TransactionActionType }
             *     
             */
            public void setDynamicPkgAction(TransactionActionType value) {
                this.dynamicPkgAction = value;
            }

            /**
             * Obtiene el valor de la propiedad dynamicPkgStatus.
             * 
             * @return
             *     possible object is
             *     {@link TransactionStatusType }
             *     
             */
            public TransactionStatusType getDynamicPkgStatus() {
                return dynamicPkgStatus;
            }

            /**
             * Define el valor de la propiedad dynamicPkgStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link TransactionStatusType }
             *     
             */
            public void setDynamicPkgStatus(TransactionStatusType value) {
                this.dynamicPkgStatus = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="PackageOptions" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PackageOption" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DynamicPkgComponentGroup"/&amp;gt;
         *       &amp;lt;attribute name="CreateDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID",
            "packageOptions"
        })
        public static class PackageOptionComponent {

            @XmlElement(name = "UniqueID")
            protected List<UniqueIDType> uniqueID;
            @XmlElement(name = "PackageOptions")
            protected DynamicPkgType.Components.PackageOptionComponent.PackageOptions packageOptions;
            @XmlAttribute(name = "CreateDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar createDateTime;
            @XmlAttribute(name = "HoldDuration")
            protected Duration holdDuration;
            @XmlAttribute(name = "QuoteID")
            protected String quoteID;
            @XmlAttribute(name = "DynamicPkgAction")
            protected TransactionActionType dynamicPkgAction;
            @XmlAttribute(name = "DynamicPkgStatus")
            protected TransactionStatusType dynamicPkgStatus;

            /**
             * Gets the value of the uniqueID property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the uniqueID property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getUniqueID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link UniqueIDType }
             * 
             * 
             */
            public List<UniqueIDType> getUniqueID() {
                if (uniqueID == null) {
                    uniqueID = new ArrayList<UniqueIDType>();
                }
                return this.uniqueID;
            }

            /**
             * Obtiene el valor de la propiedad packageOptions.
             * 
             * @return
             *     possible object is
             *     {@link DynamicPkgType.Components.PackageOptionComponent.PackageOptions }
             *     
             */
            public DynamicPkgType.Components.PackageOptionComponent.PackageOptions getPackageOptions() {
                return packageOptions;
            }

            /**
             * Define el valor de la propiedad packageOptions.
             * 
             * @param value
             *     allowed object is
             *     {@link DynamicPkgType.Components.PackageOptionComponent.PackageOptions }
             *     
             */
            public void setPackageOptions(DynamicPkgType.Components.PackageOptionComponent.PackageOptions value) {
                this.packageOptions = value;
            }

            /**
             * Obtiene el valor de la propiedad createDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCreateDateTime() {
                return createDateTime;
            }

            /**
             * Define el valor de la propiedad createDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCreateDateTime(XMLGregorianCalendar value) {
                this.createDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad holdDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getHoldDuration() {
                return holdDuration;
            }

            /**
             * Define el valor de la propiedad holdDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setHoldDuration(Duration value) {
                this.holdDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad quoteID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuoteID() {
                return quoteID;
            }

            /**
             * Define el valor de la propiedad quoteID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuoteID(String value) {
                this.quoteID = value;
            }

            /**
             * Obtiene el valor de la propiedad dynamicPkgAction.
             * 
             * @return
             *     possible object is
             *     {@link TransactionActionType }
             *     
             */
            public TransactionActionType getDynamicPkgAction() {
                return dynamicPkgAction;
            }

            /**
             * Define el valor de la propiedad dynamicPkgAction.
             * 
             * @param value
             *     allowed object is
             *     {@link TransactionActionType }
             *     
             */
            public void setDynamicPkgAction(TransactionActionType value) {
                this.dynamicPkgAction = value;
            }

            /**
             * Obtiene el valor de la propiedad dynamicPkgStatus.
             * 
             * @return
             *     possible object is
             *     {@link TransactionStatusType }
             *     
             */
            public TransactionStatusType getDynamicPkgStatus() {
                return dynamicPkgStatus;
            }

            /**
             * Define el valor de la propiedad dynamicPkgStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link TransactionStatusType }
             *     
             */
            public void setDynamicPkgStatus(TransactionStatusType value) {
                this.dynamicPkgStatus = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PackageOption" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "packageOption"
            })
            public static class PackageOptions {

                @XmlElement(name = "PackageOption")
                protected List<PackageOptionType> packageOption;

                /**
                 * Gets the value of the packageOption property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the packageOption property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getPackageOption().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PackageOptionType }
                 * 
                 * 
                 */
                public List<PackageOptionType> getPackageOption() {
                    if (packageOption == null) {
                        packageOption = new ArrayList<PackageOptionType>();
                    }
                    return this.packageOption;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ResGuest" type="{http://www.opentravel.org/OTA/2003/05}DynamicPkgGuestType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resGuest"
    })
    public static class ResGuests {

        @XmlElement(name = "ResGuest", required = true)
        protected List<DynamicPkgGuestType> resGuest;

        /**
         * Gets the value of the resGuest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the resGuest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getResGuest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link DynamicPkgGuestType }
         * 
         * 
         */
        public List<DynamicPkgGuestType> getResGuest() {
            if (resGuest == null) {
                resGuest = new ArrayList<DynamicPkgGuestType>();
            }
            return this.resGuest;
        }

    }

}
