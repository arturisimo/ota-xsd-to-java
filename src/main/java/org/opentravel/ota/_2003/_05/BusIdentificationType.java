
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines bus identification information.
 * 
 * &lt;p&gt;Clase Java para BusIdentificationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="BusIdentificationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="BusNumber"&amp;gt;
 *           &amp;lt;simpleType&amp;gt;
 *             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *               &amp;lt;minLength value="1"/&amp;gt;
 *               &amp;lt;maxLength value="8"/&amp;gt;
 *             &amp;lt;/restriction&amp;gt;
 *           &amp;lt;/simpleType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="NetworkCode" type="{http://www.opentravel.org/OTA/2003/05}NetworkCodeType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="BusTypeCode" type="{http://www.opentravel.org/OTA/2003/05}CodeOrStringType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusIdentificationType", propOrder = {
    "busNumber",
    "networkCode"
})
public class BusIdentificationType {

    @XmlElement(name = "BusNumber", required = true)
    protected String busNumber;
    @XmlElement(name = "NetworkCode", required = true)
    protected NetworkCodeType networkCode;
    @XmlAttribute(name = "BusTypeCode")
    protected String busTypeCode;

    /**
     * Obtiene el valor de la propiedad busNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusNumber() {
        return busNumber;
    }

    /**
     * Define el valor de la propiedad busNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusNumber(String value) {
        this.busNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad networkCode.
     * 
     * @return
     *     possible object is
     *     {@link NetworkCodeType }
     *     
     */
    public NetworkCodeType getNetworkCode() {
        return networkCode;
    }

    /**
     * Define el valor de la propiedad networkCode.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkCodeType }
     *     
     */
    public void setNetworkCode(NetworkCodeType value) {
        this.networkCode = value;
    }

    /**
     * Obtiene el valor de la propiedad busTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusTypeCode() {
        return busTypeCode;
    }

    /**
     * Define el valor de la propiedad busTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusTypeCode(String value) {
        this.busTypeCode = value;
    }

}
