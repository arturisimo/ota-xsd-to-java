
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Baggage type, quantity, weight, size with ontology reference.
 * 
 * &lt;p&gt;Clase Java para OntologyBaggageType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OntologyBaggageType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="TotalWeight" type="{http://www.opentravel.org/OTA/2003/05}OntologyWeightType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Item"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCodeType"&amp;gt;
 *                           &amp;lt;attribute name="SpecialItemInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Size" type="{http://www.opentravel.org/OTA/2003/05}OntologyDimensionType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Weight" type="{http://www.opentravel.org/OTA/2003/05}OntologyWeightType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="SpecialItemInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TotalPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OntologyBaggageType", propOrder = {
    "totalWeight",
    "detail",
    "ontologyExtension"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.MultiModalOfferType.TripCharacteristics.Baggage.class
})
public class OntologyBaggageType {

    @XmlElement(name = "TotalWeight")
    protected OntologyWeightType totalWeight;
    @XmlElement(name = "Detail")
    protected List<OntologyBaggageType.Detail> detail;
    @XmlElement(name = "OntologyExtension")
    protected OntologyExtensionType ontologyExtension;
    @XmlAttribute(name = "SpecialItemInd")
    protected Boolean specialItemInd;
    @XmlAttribute(name = "TotalPieces")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger totalPieces;

    /**
     * Obtiene el valor de la propiedad totalWeight.
     * 
     * @return
     *     possible object is
     *     {@link OntologyWeightType }
     *     
     */
    public OntologyWeightType getTotalWeight() {
        return totalWeight;
    }

    /**
     * Define el valor de la propiedad totalWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyWeightType }
     *     
     */
    public void setTotalWeight(OntologyWeightType value) {
        this.totalWeight = value;
    }

    /**
     * Gets the value of the detail property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detail property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDetail().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OntologyBaggageType.Detail }
     * 
     * 
     */
    public List<OntologyBaggageType.Detail> getDetail() {
        if (detail == null) {
            detail = new ArrayList<OntologyBaggageType.Detail>();
        }
        return this.detail;
    }

    /**
     * Obtiene el valor de la propiedad ontologyExtension.
     * 
     * @return
     *     possible object is
     *     {@link OntologyExtensionType }
     *     
     */
    public OntologyExtensionType getOntologyExtension() {
        return ontologyExtension;
    }

    /**
     * Define el valor de la propiedad ontologyExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyExtensionType }
     *     
     */
    public void setOntologyExtension(OntologyExtensionType value) {
        this.ontologyExtension = value;
    }

    /**
     * Obtiene el valor de la propiedad specialItemInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSpecialItemInd() {
        return specialItemInd;
    }

    /**
     * Define el valor de la propiedad specialItemInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpecialItemInd(Boolean value) {
        this.specialItemInd = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPieces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalPieces() {
        return totalPieces;
    }

    /**
     * Define el valor de la propiedad totalPieces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalPieces(BigInteger value) {
        this.totalPieces = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Item"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCodeType"&amp;gt;
     *                 &amp;lt;attribute name="SpecialItemInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Size" type="{http://www.opentravel.org/OTA/2003/05}OntologyDimensionType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Weight" type="{http://www.opentravel.org/OTA/2003/05}OntologyWeightType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item",
        "size",
        "weight",
        "ontologyExtension"
    })
    public static class Detail {

        @XmlElement(name = "Item", required = true)
        protected OntologyBaggageType.Detail.Item item;
        @XmlElement(name = "Size")
        protected OntologyDimensionType size;
        @XmlElement(name = "Weight")
        protected OntologyWeightType weight;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;

        /**
         * Obtiene el valor de la propiedad item.
         * 
         * @return
         *     possible object is
         *     {@link OntologyBaggageType.Detail.Item }
         *     
         */
        public OntologyBaggageType.Detail.Item getItem() {
            return item;
        }

        /**
         * Define el valor de la propiedad item.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyBaggageType.Detail.Item }
         *     
         */
        public void setItem(OntologyBaggageType.Detail.Item value) {
            this.item = value;
        }

        /**
         * Obtiene el valor de la propiedad size.
         * 
         * @return
         *     possible object is
         *     {@link OntologyDimensionType }
         *     
         */
        public OntologyDimensionType getSize() {
            return size;
        }

        /**
         * Define el valor de la propiedad size.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyDimensionType }
         *     
         */
        public void setSize(OntologyDimensionType value) {
            this.size = value;
        }

        /**
         * Obtiene el valor de la propiedad weight.
         * 
         * @return
         *     possible object is
         *     {@link OntologyWeightType }
         *     
         */
        public OntologyWeightType getWeight() {
            return weight;
        }

        /**
         * Define el valor de la propiedad weight.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyWeightType }
         *     
         */
        public void setWeight(OntologyWeightType value) {
            this.weight = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCodeType"&amp;gt;
         *       &amp;lt;attribute name="SpecialItemInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Item
            extends OntologyCodeType
        {

            @XmlAttribute(name = "SpecialItemInd")
            protected Boolean specialItemInd;

            /**
             * Obtiene el valor de la propiedad specialItemInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSpecialItemInd() {
                return specialItemInd;
            }

            /**
             * Define el valor de la propiedad specialItemInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSpecialItemInd(Boolean value) {
                this.specialItemInd = value;
            }

        }

    }

}
