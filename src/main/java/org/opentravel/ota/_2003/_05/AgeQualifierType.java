
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * An extensible list of age qualifiers.
 * 
 * &lt;p&gt;Clase Java para AgeQualifierType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AgeQualifierType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifyingEnum"&amp;gt;
 *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgeQualifierType", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityAvailRS.TourActivityInfo.Extra.Pricing.Detail.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityAvailRS.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.ParticipantInfo.Category.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityModifyRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityModifyRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.ParticipantInfo.Category.QualifierInfo.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.Pricing.ParticipantCategory.QualifierInfo.class,
    org.opentravel.ota._2003._05.TourActivityParticipantCategoryType.QualifierInfo.class,
    org.opentravel.ota._2003._05.TourActivityParticipantType.CategoryInfo.class
})
public class AgeQualifierType {

    @XmlValue
    protected AgeQualifyingEnum value;
    @XmlAttribute(name = "Extension")
    protected String extension;

    /**
     * An extensible list of age qualifying types.
     * 
     * @return
     *     possible object is
     *     {@link AgeQualifyingEnum }
     *     
     */
    public AgeQualifyingEnum getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link AgeQualifyingEnum }
     *     
     */
    public void setValue(AgeQualifyingEnum value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

}
