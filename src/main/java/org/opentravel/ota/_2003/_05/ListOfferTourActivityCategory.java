
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferTourActivityCategory.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferTourActivityCategory"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Accessible"/&amp;gt;
 *     &amp;lt;enumeration value="Adventure"/&amp;gt;
 *     &amp;lt;enumeration value="Cultural"/&amp;gt;
 *     &amp;lt;enumeration value="EcoAdventure"/&amp;gt;
 *     &amp;lt;enumeration value="Educational"/&amp;gt;
 *     &amp;lt;enumeration value="Family"/&amp;gt;
 *     &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
 *     &amp;lt;enumeration value="Group"/&amp;gt;
 *     &amp;lt;enumeration value="Romantic"/&amp;gt;
 *     &amp;lt;enumeration value="SportsAndRecreation"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferTourActivityCategory")
@XmlEnum
public enum ListOfferTourActivityCategory {

    @XmlEnumValue("Accessible")
    ACCESSIBLE("Accessible"),
    @XmlEnumValue("Adventure")
    ADVENTURE("Adventure"),
    @XmlEnumValue("Cultural")
    CULTURAL("Cultural"),
    @XmlEnumValue("EcoAdventure")
    ECO_ADVENTURE("EcoAdventure"),
    @XmlEnumValue("Educational")
    EDUCATIONAL("Educational"),

    /**
     * The tour and/or activity content and extertion level are family friendly for children, adults and seniors.
     * 
     */
    @XmlEnumValue("Family")
    FAMILY("Family"),
    @XmlEnumValue("FoodAndBeverage")
    FOOD_AND_BEVERAGE("FoodAndBeverage"),

    /**
     * A day tour or activity that is well-suited for larger parties, e.g. 8+ attendees within a group.
     * 
     */
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Romantic")
    ROMANTIC("Romantic"),
    @XmlEnumValue("SportsAndRecreation")
    SPORTS_AND_RECREATION("SportsAndRecreation"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferTourActivityCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferTourActivityCategory fromValue(String v) {
        for (ListOfferTourActivityCategory c: ListOfferTourActivityCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
