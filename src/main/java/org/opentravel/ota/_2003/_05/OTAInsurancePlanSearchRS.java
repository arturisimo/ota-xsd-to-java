
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="AvailablePlans"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="AvailablePlan" maxOccurs="999"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="PlanDetail" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="PlanDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ProviderDetail" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="ProviderDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="UnderwriterDetail" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="UnderwriterCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="UnderwriterDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="PlanRestrictions" type="{http://www.opentravel.org/OTA/2003/05}PlanRestrictionType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PlanID_Group"/&amp;gt;
 *                             &amp;lt;attribute name="Featured" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="PreferLevel" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "availablePlans",
    "errors"
})
@XmlRootElement(name = "OTA_InsurancePlanSearchRS")
public class OTAInsurancePlanSearchRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "AvailablePlans")
    protected OTAInsurancePlanSearchRS.AvailablePlans availablePlans;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad availablePlans.
     * 
     * @return
     *     possible object is
     *     {@link OTAInsurancePlanSearchRS.AvailablePlans }
     *     
     */
    public OTAInsurancePlanSearchRS.AvailablePlans getAvailablePlans() {
        return availablePlans;
    }

    /**
     * Define el valor de la propiedad availablePlans.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAInsurancePlanSearchRS.AvailablePlans }
     *     
     */
    public void setAvailablePlans(OTAInsurancePlanSearchRS.AvailablePlans value) {
        this.availablePlans = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AvailablePlan" maxOccurs="999"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PlanDetail" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PlanDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ProviderDetail" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="ProviderDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="UnderwriterDetail" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="UnderwriterCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="UnderwriterDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="PlanRestrictions" type="{http://www.opentravel.org/OTA/2003/05}PlanRestrictionType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PlanID_Group"/&amp;gt;
     *                 &amp;lt;attribute name="Featured" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PreferLevel" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "availablePlan"
    })
    public static class AvailablePlans {

        @XmlElement(name = "AvailablePlan", required = true)
        protected List<OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan> availablePlan;

        /**
         * Gets the value of the availablePlan property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the availablePlan property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAvailablePlan().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan }
         * 
         * 
         */
        public List<OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan> getAvailablePlan() {
            if (availablePlan == null) {
                availablePlan = new ArrayList<OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan>();
            }
            return this.availablePlan;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PlanDetail" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PlanDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ProviderDetail" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="ProviderDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="UnderwriterDetail" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="UnderwriterCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="UnderwriterDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="PlanRestrictions" type="{http://www.opentravel.org/OTA/2003/05}PlanRestrictionType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PlanID_Group"/&amp;gt;
         *       &amp;lt;attribute name="Featured" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PreferLevel" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "planDetail",
            "providerDetail",
            "underwriterDetail",
            "planRestrictions"
        })
        public static class AvailablePlan {

            @XmlElement(name = "PlanDetail")
            protected OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.PlanDetail planDetail;
            @XmlElement(name = "ProviderDetail")
            protected OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.ProviderDetail providerDetail;
            @XmlElement(name = "UnderwriterDetail")
            protected OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.UnderwriterDetail underwriterDetail;
            @XmlElement(name = "PlanRestrictions")
            protected PlanRestrictionType planRestrictions;
            @XmlAttribute(name = "Featured")
            protected Boolean featured;
            @XmlAttribute(name = "PreferLevel")
            protected PreferLevelType preferLevel;
            @XmlAttribute(name = "EffectiveDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar effectiveDate;
            @XmlAttribute(name = "ExpireDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar expireDate;
            @XmlAttribute(name = "ExpireDateExclusiveInd")
            protected Boolean expireDateExclusiveInd;
            @XmlAttribute(name = "PlanID", required = true)
            protected String planID;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Type")
            protected String type;
            @XmlAttribute(name = "TypeID")
            protected String typeID;

            /**
             * Obtiene el valor de la propiedad planDetail.
             * 
             * @return
             *     possible object is
             *     {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.PlanDetail }
             *     
             */
            public OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.PlanDetail getPlanDetail() {
                return planDetail;
            }

            /**
             * Define el valor de la propiedad planDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.PlanDetail }
             *     
             */
            public void setPlanDetail(OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.PlanDetail value) {
                this.planDetail = value;
            }

            /**
             * Obtiene el valor de la propiedad providerDetail.
             * 
             * @return
             *     possible object is
             *     {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.ProviderDetail }
             *     
             */
            public OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.ProviderDetail getProviderDetail() {
                return providerDetail;
            }

            /**
             * Define el valor de la propiedad providerDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.ProviderDetail }
             *     
             */
            public void setProviderDetail(OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.ProviderDetail value) {
                this.providerDetail = value;
            }

            /**
             * Obtiene el valor de la propiedad underwriterDetail.
             * 
             * @return
             *     possible object is
             *     {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.UnderwriterDetail }
             *     
             */
            public OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.UnderwriterDetail getUnderwriterDetail() {
                return underwriterDetail;
            }

            /**
             * Define el valor de la propiedad underwriterDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.UnderwriterDetail }
             *     
             */
            public void setUnderwriterDetail(OTAInsurancePlanSearchRS.AvailablePlans.AvailablePlan.UnderwriterDetail value) {
                this.underwriterDetail = value;
            }

            /**
             * Obtiene el valor de la propiedad planRestrictions.
             * 
             * @return
             *     possible object is
             *     {@link PlanRestrictionType }
             *     
             */
            public PlanRestrictionType getPlanRestrictions() {
                return planRestrictions;
            }

            /**
             * Define el valor de la propiedad planRestrictions.
             * 
             * @param value
             *     allowed object is
             *     {@link PlanRestrictionType }
             *     
             */
            public void setPlanRestrictions(PlanRestrictionType value) {
                this.planRestrictions = value;
            }

            /**
             * Obtiene el valor de la propiedad featured.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFeatured() {
                return featured;
            }

            /**
             * Define el valor de la propiedad featured.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFeatured(Boolean value) {
                this.featured = value;
            }

            /**
             * Obtiene el valor de la propiedad preferLevel.
             * 
             * @return
             *     possible object is
             *     {@link PreferLevelType }
             *     
             */
            public PreferLevelType getPreferLevel() {
                return preferLevel;
            }

            /**
             * Define el valor de la propiedad preferLevel.
             * 
             * @param value
             *     allowed object is
             *     {@link PreferLevelType }
             *     
             */
            public void setPreferLevel(PreferLevelType value) {
                this.preferLevel = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEffectiveDate() {
                return effectiveDate;
            }

            /**
             * Define el valor de la propiedad effectiveDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEffectiveDate(XMLGregorianCalendar value) {
                this.effectiveDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpireDate() {
                return expireDate;
            }

            /**
             * Define el valor de la propiedad expireDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpireDate(XMLGregorianCalendar value) {
                this.expireDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDateExclusiveInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExpireDateExclusiveInd() {
                return expireDateExclusiveInd;
            }

            /**
             * Define el valor de la propiedad expireDateExclusiveInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExpireDateExclusiveInd(Boolean value) {
                this.expireDateExclusiveInd = value;
            }

            /**
             * Obtiene el valor de la propiedad planID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlanID() {
                return planID;
            }

            /**
             * Define el valor de la propiedad planID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlanID(String value) {
                this.planID = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad typeID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTypeID() {
                return typeID;
            }

            /**
             * Define el valor de la propiedad typeID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTypeID(String value) {
                this.typeID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PlanDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="QuoteDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="BookingDetailURL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "planDescription",
                "quoteDetailURL",
                "bookingDetailURL"
            })
            public static class PlanDetail {

                @XmlElement(name = "PlanDescription")
                protected FormattedTextType planDescription;
                @XmlElement(name = "QuoteDetailURL")
                protected URLType quoteDetailURL;
                @XmlElement(name = "BookingDetailURL")
                protected URLType bookingDetailURL;

                /**
                 * Obtiene el valor de la propiedad planDescription.
                 * 
                 * @return
                 *     possible object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public FormattedTextType getPlanDescription() {
                    return planDescription;
                }

                /**
                 * Define el valor de la propiedad planDescription.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public void setPlanDescription(FormattedTextType value) {
                    this.planDescription = value;
                }

                /**
                 * Obtiene el valor de la propiedad quoteDetailURL.
                 * 
                 * @return
                 *     possible object is
                 *     {@link URLType }
                 *     
                 */
                public URLType getQuoteDetailURL() {
                    return quoteDetailURL;
                }

                /**
                 * Define el valor de la propiedad quoteDetailURL.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link URLType }
                 *     
                 */
                public void setQuoteDetailURL(URLType value) {
                    this.quoteDetailURL = value;
                }

                /**
                 * Obtiene el valor de la propiedad bookingDetailURL.
                 * 
                 * @return
                 *     possible object is
                 *     {@link URLType }
                 *     
                 */
                public URLType getBookingDetailURL() {
                    return bookingDetailURL;
                }

                /**
                 * Define el valor de la propiedad bookingDetailURL.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link URLType }
                 *     
                 */
                public void setBookingDetailURL(URLType value) {
                    this.bookingDetailURL = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ProviderCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="ProviderDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "providerCompany",
                "providerDescription"
            })
            public static class ProviderDetail {

                @XmlElement(name = "ProviderCompany")
                protected CompanyNameType providerCompany;
                @XmlElement(name = "ProviderDescription")
                protected FormattedTextType providerDescription;

                /**
                 * Obtiene el valor de la propiedad providerCompany.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public CompanyNameType getProviderCompany() {
                    return providerCompany;
                }

                /**
                 * Define el valor de la propiedad providerCompany.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public void setProviderCompany(CompanyNameType value) {
                    this.providerCompany = value;
                }

                /**
                 * Obtiene el valor de la propiedad providerDescription.
                 * 
                 * @return
                 *     possible object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public FormattedTextType getProviderDescription() {
                    return providerDescription;
                }

                /**
                 * Define el valor de la propiedad providerDescription.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public void setProviderDescription(FormattedTextType value) {
                    this.providerDescription = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="UnderwriterCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="UnderwriterDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "underwriterCompany",
                "underwriterDescription"
            })
            public static class UnderwriterDetail {

                @XmlElement(name = "UnderwriterCompany")
                protected CompanyNameType underwriterCompany;
                @XmlElement(name = "UnderwriterDescription")
                protected FormattedTextType underwriterDescription;

                /**
                 * Obtiene el valor de la propiedad underwriterCompany.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public CompanyNameType getUnderwriterCompany() {
                    return underwriterCompany;
                }

                /**
                 * Define el valor de la propiedad underwriterCompany.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public void setUnderwriterCompany(CompanyNameType value) {
                    this.underwriterCompany = value;
                }

                /**
                 * Obtiene el valor de la propiedad underwriterDescription.
                 * 
                 * @return
                 *     possible object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public FormattedTextType getUnderwriterDescription() {
                    return underwriterDescription;
                }

                /**
                 * Define el valor de la propiedad underwriterDescription.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link FormattedTextType }
                 *     
                 */
                public void setUnderwriterDescription(FormattedTextType value) {
                    this.underwriterDescription = value;
                }

            }

        }

    }

}
