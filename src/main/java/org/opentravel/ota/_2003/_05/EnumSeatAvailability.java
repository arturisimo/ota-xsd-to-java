
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para Enum_SeatAvailability.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="Enum_SeatAvailability"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AdvancedSeatSelectionSeat "/&amp;gt;
 *     &amp;lt;enumeration value="CourtesyReservedSeat"/&amp;gt;
 *     &amp;lt;enumeration value="DownlineProtectedSeats"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraSeat"/&amp;gt;
 *     &amp;lt;enumeration value="FreeSeat"/&amp;gt;
 *     &amp;lt;enumeration value="GroupPreallocationSeat"/&amp;gt;
 *     &amp;lt;enumeration value="NoSeatHere"/&amp;gt;
 *     &amp;lt;enumeration value="ProtectedSeat"/&amp;gt;
 *     &amp;lt;enumeration value="SeatAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="SeatBlocked_AirportUse"/&amp;gt;
 *     &amp;lt;enumeration value="SeatBlocked_Deadload"/&amp;gt;
 *     &amp;lt;enumeration value="SeatBlocked_LocalPassengerBoarded"/&amp;gt;
 *     &amp;lt;enumeration value="SeatBlocked_OtherReason"/&amp;gt;
 *     &amp;lt;enumeration value="SeatBlocked_ThroughPassengerBoarded"/&amp;gt;
 *     &amp;lt;enumeration value="SeatHeld"/&amp;gt;
 *     &amp;lt;enumeration value="SeatNotAvailableForInterlineThroughCheckin"/&amp;gt;
 *     &amp;lt;enumeration value="SeatNotAvailableForPartnerAirlineUse "/&amp;gt;
 *     &amp;lt;enumeration value="SeatProtectedForCodeSharing"/&amp;gt;
 *     &amp;lt;enumeration value="SeatOccupied"/&amp;gt;
 *     &amp;lt;enumeration value="SeatOccupied_MedicalReasons"/&amp;gt;
 *     &amp;lt;enumeration value="SeatReserved"/&amp;gt;
 *     &amp;lt;enumeration value="SeatReserved_Generic"/&amp;gt;
 *     &amp;lt;enumeration value="TransitPassengerOrLoad"/&amp;gt;
 *     &amp;lt;enumeration value="UplineProtectedSeat "/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "Enum_SeatAvailability")
@XmlEnum
public enum EnumSeatAvailability {

    @XmlEnumValue("AdvancedSeatSelectionSeat ")
    ADVANCED_SEAT_SELECTION_SEAT("AdvancedSeatSelectionSeat "),
    @XmlEnumValue("CourtesyReservedSeat")
    COURTESY_RESERVED_SEAT("CourtesyReservedSeat"),
    @XmlEnumValue("DownlineProtectedSeats")
    DOWNLINE_PROTECTED_SEATS("DownlineProtectedSeats"),
    @XmlEnumValue("ExtraSeat")
    EXTRA_SEAT("ExtraSeat"),
    @XmlEnumValue("FreeSeat")
    FREE_SEAT("FreeSeat"),
    @XmlEnumValue("GroupPreallocationSeat")
    GROUP_PREALLOCATION_SEAT("GroupPreallocationSeat"),
    @XmlEnumValue("NoSeatHere")
    NO_SEAT_HERE("NoSeatHere"),
    @XmlEnumValue("ProtectedSeat")
    PROTECTED_SEAT("ProtectedSeat"),
    @XmlEnumValue("SeatAvailable")
    SEAT_AVAILABLE("SeatAvailable"),
    @XmlEnumValue("SeatBlocked_AirportUse")
    SEAT_BLOCKED_AIRPORT_USE("SeatBlocked_AirportUse"),
    @XmlEnumValue("SeatBlocked_Deadload")
    SEAT_BLOCKED_DEADLOAD("SeatBlocked_Deadload"),
    @XmlEnumValue("SeatBlocked_LocalPassengerBoarded")
    SEAT_BLOCKED_LOCAL_PASSENGER_BOARDED("SeatBlocked_LocalPassengerBoarded"),
    @XmlEnumValue("SeatBlocked_OtherReason")
    SEAT_BLOCKED_OTHER_REASON("SeatBlocked_OtherReason"),
    @XmlEnumValue("SeatBlocked_ThroughPassengerBoarded")
    SEAT_BLOCKED_THROUGH_PASSENGER_BOARDED("SeatBlocked_ThroughPassengerBoarded"),
    @XmlEnumValue("SeatHeld")
    SEAT_HELD("SeatHeld"),
    @XmlEnumValue("SeatNotAvailableForInterlineThroughCheckin")
    SEAT_NOT_AVAILABLE_FOR_INTERLINE_THROUGH_CHECKIN("SeatNotAvailableForInterlineThroughCheckin"),
    @XmlEnumValue("SeatNotAvailableForPartnerAirlineUse ")
    SEAT_NOT_AVAILABLE_FOR_PARTNER_AIRLINE_USE("SeatNotAvailableForPartnerAirlineUse "),
    @XmlEnumValue("SeatProtectedForCodeSharing")
    SEAT_PROTECTED_FOR_CODE_SHARING("SeatProtectedForCodeSharing"),
    @XmlEnumValue("SeatOccupied")
    SEAT_OCCUPIED("SeatOccupied"),
    @XmlEnumValue("SeatOccupied_MedicalReasons")
    SEAT_OCCUPIED_MEDICAL_REASONS("SeatOccupied_MedicalReasons"),
    @XmlEnumValue("SeatReserved")
    SEAT_RESERVED("SeatReserved"),
    @XmlEnumValue("SeatReserved_Generic")
    SEAT_RESERVED_GENERIC("SeatReserved_Generic"),
    @XmlEnumValue("TransitPassengerOrLoad")
    TRANSIT_PASSENGER_OR_LOAD("TransitPassengerOrLoad"),
    @XmlEnumValue("UplineProtectedSeat ")
    UPLINE_PROTECTED_SEAT("UplineProtectedSeat "),

    /**
     * It is strongly recommended that you submit a comment to have any of your extended list values permanently added to the OpenTravel specification to support maximum trading partner interoperability. http://www.opentraveldevelopersnetwork.com/specificationcomments/2/entercomment.html
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    EnumSeatAvailability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumSeatAvailability fromValue(String v) {
        for (EnumSeatAvailability c: EnumSeatAvailability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
