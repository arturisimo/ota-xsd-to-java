
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_RatePlanType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_RatePlanType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Association"/&amp;gt;
 *     &amp;lt;enumeration value="ClubConcierge"/&amp;gt;
 *     &amp;lt;enumeration value="Consortia"/&amp;gt;
 *     &amp;lt;enumeration value="Contract"/&amp;gt;
 *     &amp;lt;enumeration value="Convention"/&amp;gt;
 *     &amp;lt;enumeration value="Corporate"/&amp;gt;
 *     &amp;lt;enumeration value="DayRate"/&amp;gt;
 *     &amp;lt;enumeration value="DistressedInventory"/&amp;gt;
 *     &amp;lt;enumeration value="FamilyPlan"/&amp;gt;
 *     &amp;lt;enumeration value="Government"/&amp;gt;
 *     &amp;lt;enumeration value="Group"/&amp;gt;
 *     &amp;lt;enumeration value="LastRoomAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="Military"/&amp;gt;
 *     &amp;lt;enumeration value="Monthly"/&amp;gt;
 *     &amp;lt;enumeration value="Multi-DayPackage"/&amp;gt;
 *     &amp;lt;enumeration value="Negotiated"/&amp;gt;
 *     &amp;lt;enumeration value="Net"/&amp;gt;
 *     &amp;lt;enumeration value="NonLastRoomAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="Package"/&amp;gt;
 *     &amp;lt;enumeration value="Promotional"/&amp;gt;
 *     &amp;lt;enumeration value="PromotionalPackageTour"/&amp;gt;
 *     &amp;lt;enumeration value="Published"/&amp;gt;
 *     &amp;lt;enumeration value="RegularRack"/&amp;gt;
 *     &amp;lt;enumeration value="SeniorCitizen"/&amp;gt;
 *     &amp;lt;enumeration value="TourWholesale"/&amp;gt;
 *     &amp;lt;enumeration value="TravelIndustry"/&amp;gt;
 *     &amp;lt;enumeration value="Weekend"/&amp;gt;
 *     &amp;lt;enumeration value="Weekly"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_RatePlanType_Base")
@XmlEnum
public enum ListRatePlanTypeBase {

    @XmlEnumValue("Association")
    ASSOCIATION("Association"),
    @XmlEnumValue("ClubConcierge")
    CLUB_CONCIERGE("ClubConcierge"),
    @XmlEnumValue("Consortia")
    CONSORTIA("Consortia"),
    @XmlEnumValue("Contract")
    CONTRACT("Contract"),
    @XmlEnumValue("Convention")
    CONVENTION("Convention"),
    @XmlEnumValue("Corporate")
    CORPORATE("Corporate"),
    @XmlEnumValue("DayRate")
    DAY_RATE("DayRate"),
    @XmlEnumValue("DistressedInventory")
    DISTRESSED_INVENTORY("DistressedInventory"),
    @XmlEnumValue("FamilyPlan")
    FAMILY_PLAN("FamilyPlan"),
    @XmlEnumValue("Government")
    GOVERNMENT("Government"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("LastRoomAvailable")
    LAST_ROOM_AVAILABLE("LastRoomAvailable"),
    @XmlEnumValue("Military")
    MILITARY("Military"),
    @XmlEnumValue("Monthly")
    MONTHLY("Monthly"),
    @XmlEnumValue("Multi-DayPackage")
    MULTI_DAY_PACKAGE("Multi-DayPackage"),
    @XmlEnumValue("Negotiated")
    NEGOTIATED("Negotiated"),
    @XmlEnumValue("Net")
    NET("Net"),
    @XmlEnumValue("NonLastRoomAvailable")
    NON_LAST_ROOM_AVAILABLE("NonLastRoomAvailable"),
    @XmlEnumValue("Package")
    PACKAGE("Package"),
    @XmlEnumValue("Promotional")
    PROMOTIONAL("Promotional"),
    @XmlEnumValue("PromotionalPackageTour")
    PROMOTIONAL_PACKAGE_TOUR("PromotionalPackageTour"),
    @XmlEnumValue("Published")
    PUBLISHED("Published"),
    @XmlEnumValue("RegularRack")
    REGULAR_RACK("RegularRack"),
    @XmlEnumValue("SeniorCitizen")
    SENIOR_CITIZEN("SeniorCitizen"),
    @XmlEnumValue("TourWholesale")
    TOUR_WHOLESALE("TourWholesale"),
    @XmlEnumValue("TravelIndustry")
    TRAVEL_INDUSTRY("TravelIndustry"),
    @XmlEnumValue("Weekend")
    WEEKEND("Weekend"),
    @XmlEnumValue("Weekly")
    WEEKLY("Weekly"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListRatePlanTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListRatePlanTypeBase fromValue(String v) {
        for (ListRatePlanTypeBase c: ListRatePlanTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
