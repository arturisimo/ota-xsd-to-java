
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferTripPurpose.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferTripPurpose"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="BereavmentOrEmergency"/&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="BusinessAndPersonal"/&amp;gt;
 *     &amp;lt;enumeration value="CharterOrGroup"/&amp;gt;
 *     &amp;lt;enumeration value="ConferenceOrEvent"/&amp;gt;
 *     &amp;lt;enumeration value="Personal"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferTripPurpose")
@XmlEnum
public enum ListOfferTripPurpose {

    @XmlEnumValue("BereavmentOrEmergency")
    BEREAVMENT_OR_EMERGENCY("BereavmentOrEmergency"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("BusinessAndPersonal")
    BUSINESS_AND_PERSONAL("BusinessAndPersonal"),
    @XmlEnumValue("CharterOrGroup")
    CHARTER_OR_GROUP("CharterOrGroup"),
    @XmlEnumValue("ConferenceOrEvent")
    CONFERENCE_OR_EVENT("ConferenceOrEvent"),
    @XmlEnumValue("Personal")
    PERSONAL("Personal");
    private final String value;

    ListOfferTripPurpose(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferTripPurpose fromValue(String v) {
        for (ListOfferTripPurpose c: ListOfferTripPurpose.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
