
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="OriginDestinationInformation" maxOccurs="99" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RailOriginDestinationInformationType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="JourneySegment" maxOccurs="10"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="TrainSegment" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="TrainOrigin" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="TrainDestination" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="RailAmenities" type="{http://www.opentravel.org/OTA/2003/05}RailAmenityType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="AvailabilityDetail" maxOccurs="99"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="ClassCode" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
 *                                                                   &amp;lt;/extension&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                             &amp;lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *                                                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" minOccurs="0"/&amp;gt;
 *                                                             &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                           &amp;lt;attribute name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="ReservationType"&amp;gt;
 *                                                             &amp;lt;simpleType&amp;gt;
 *                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                 &amp;lt;enumeration value="Reservable"/&amp;gt;
 *                                                                 &amp;lt;enumeration value="NotReservable"/&amp;gt;
 *                                                               &amp;lt;/restriction&amp;gt;
 *                                                             &amp;lt;/simpleType&amp;gt;
 *                                                           &amp;lt;/attribute&amp;gt;
 *                                                           &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="IsBicycleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                           &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="VendorMessages" type="{http://www.opentravel.org/OTA/2003/05}VendorMessagesType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                                                 &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;attribute name="RestrictedServiceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;attribute name="CancelledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="BusSegment" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BusSegmentType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="ClassCodes" maxOccurs="99"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
 *                                                           &amp;lt;attribute name="ReservationClass" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="ReservationType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="LastHoldDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                             &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                             &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/extension&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "originDestinationInformation",
    "errors"
})
@XmlRootElement(name = "OTA_RailAvailRS")
public class OTARailAvailRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "OriginDestinationInformation")
    protected List<OTARailAvailRS.OriginDestinationInformation> originDestinationInformation;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOriginDestinationInformation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTARailAvailRS.OriginDestinationInformation }
     * 
     * 
     */
    public List<OTARailAvailRS.OriginDestinationInformation> getOriginDestinationInformation() {
        if (originDestinationInformation == null) {
            originDestinationInformation = new ArrayList<OTARailAvailRS.OriginDestinationInformation>();
        }
        return this.originDestinationInformation;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RailOriginDestinationInformationType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="JourneySegment" maxOccurs="10"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="TrainSegment" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="TrainOrigin" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="TrainDestination" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="RailAmenities" type="{http://www.opentravel.org/OTA/2003/05}RailAmenityType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="AvailabilityDetail" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="ClassCode" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
     *                                                 &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" minOccurs="0"/&amp;gt;
     *                                                 &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                               &amp;lt;attribute name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="ReservationType"&amp;gt;
     *                                                 &amp;lt;simpleType&amp;gt;
     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                     &amp;lt;enumeration value="Reservable"/&amp;gt;
     *                                                     &amp;lt;enumeration value="NotReservable"/&amp;gt;
     *                                                   &amp;lt;/restriction&amp;gt;
     *                                                 &amp;lt;/simpleType&amp;gt;
     *                                               &amp;lt;/attribute&amp;gt;
     *                                               &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="IsBicycleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                               &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="VendorMessages" type="{http://www.opentravel.org/OTA/2003/05}VendorMessagesType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                                     &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="RestrictedServiceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="CancelledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="BusSegment" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BusSegmentType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="ClassCodes" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
     *                                               &amp;lt;attribute name="ReservationClass" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="ReservationType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="LastHoldDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                 &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                 &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestinationOption"
    })
    public static class OriginDestinationInformation
        extends RailOriginDestinationInformationType
    {

        @XmlElement(name = "OriginDestinationOption", required = true)
        protected List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption> originDestinationOption;

        /**
         * Gets the value of the originDestinationOption property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationOption property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestinationOption().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption }
         * 
         * 
         */
        public List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption> getOriginDestinationOption() {
            if (originDestinationOption == null) {
                originDestinationOption = new ArrayList<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption>();
            }
            return this.originDestinationOption;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="JourneySegment" maxOccurs="10"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="TrainSegment" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="TrainOrigin" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="TrainDestination" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="RailAmenities" type="{http://www.opentravel.org/OTA/2003/05}RailAmenityType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="AvailabilityDetail" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="ClassCode" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
         *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" minOccurs="0"/&amp;gt;
         *                                       &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                     &amp;lt;attribute name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="ReservationType"&amp;gt;
         *                                       &amp;lt;simpleType&amp;gt;
         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                           &amp;lt;enumeration value="Reservable"/&amp;gt;
         *                                           &amp;lt;enumeration value="NotReservable"/&amp;gt;
         *                                         &amp;lt;/restriction&amp;gt;
         *                                       &amp;lt;/simpleType&amp;gt;
         *                                     &amp;lt;/attribute&amp;gt;
         *                                     &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="IsBicycleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                     &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="VendorMessages" type="{http://www.opentravel.org/OTA/2003/05}VendorMessagesType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *                           &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="RestrictedServiceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="CancelledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="BusSegment" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BusSegmentType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="ClassCodes" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
         *                                     &amp;lt;attribute name="ReservationClass" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="ReservationType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="LastHoldDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *       &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "originLocation",
            "destinationLocation",
            "journeySegment",
            "remark"
        })
        public static class OriginDestinationOption {

            @XmlElement(name = "OriginLocation")
            protected LocationType originLocation;
            @XmlElement(name = "DestinationLocation")
            protected LocationType destinationLocation;
            @XmlElement(name = "JourneySegment", required = true)
            protected List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment> journeySegment;
            @XmlElement(name = "Remark")
            protected List<WarningType> remark;
            @XmlAttribute(name = "LastHoldDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar lastHoldDate;
            @XmlAttribute(name = "JourneyDuration")
            protected Duration journeyDuration;
            @XmlAttribute(name = "AlternativeInd")
            protected Boolean alternativeInd;

            /**
             * Obtiene el valor de la propiedad originLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getOriginLocation() {
                return originLocation;
            }

            /**
             * Define el valor de la propiedad originLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setOriginLocation(LocationType value) {
                this.originLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad destinationLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDestinationLocation() {
                return destinationLocation;
            }

            /**
             * Define el valor de la propiedad destinationLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDestinationLocation(LocationType value) {
                this.destinationLocation = value;
            }

            /**
             * Gets the value of the journeySegment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the journeySegment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getJourneySegment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment }
             * 
             * 
             */
            public List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment> getJourneySegment() {
                if (journeySegment == null) {
                    journeySegment = new ArrayList<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment>();
                }
                return this.journeySegment;
            }

            /**
             * Gets the value of the remark property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the remark property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRemark().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link WarningType }
             * 
             * 
             */
            public List<WarningType> getRemark() {
                if (remark == null) {
                    remark = new ArrayList<WarningType>();
                }
                return this.remark;
            }

            /**
             * Obtiene el valor de la propiedad lastHoldDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLastHoldDate() {
                return lastHoldDate;
            }

            /**
             * Define el valor de la propiedad lastHoldDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLastHoldDate(XMLGregorianCalendar value) {
                this.lastHoldDate = value;
            }

            /**
             * Obtiene el valor de la propiedad journeyDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getJourneyDuration() {
                return journeyDuration;
            }

            /**
             * Define el valor de la propiedad journeyDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setJourneyDuration(Duration value) {
                this.journeyDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad alternativeInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAlternativeInd() {
                return alternativeInd;
            }

            /**
             * Define el valor de la propiedad alternativeInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAlternativeInd(Boolean value) {
                this.alternativeInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="TrainSegment" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="TrainOrigin" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="TrainDestination" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="RailAmenities" type="{http://www.opentravel.org/OTA/2003/05}RailAmenityType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="AvailabilityDetail" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="ClassCode" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
             *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" minOccurs="0"/&amp;gt;
             *                             &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                           &amp;lt;attribute name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="ReservationType"&amp;gt;
             *                             &amp;lt;simpleType&amp;gt;
             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                 &amp;lt;enumeration value="Reservable"/&amp;gt;
             *                                 &amp;lt;enumeration value="NotReservable"/&amp;gt;
             *                               &amp;lt;/restriction&amp;gt;
             *                             &amp;lt;/simpleType&amp;gt;
             *                           &amp;lt;/attribute&amp;gt;
             *                           &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="IsBicycleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                           &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="VendorMessages" type="{http://www.opentravel.org/OTA/2003/05}VendorMessagesType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
             *                 &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="RestrictedServiceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="CancelledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="BusSegment" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BusSegmentType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="ClassCodes" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
             *                           &amp;lt;attribute name="ReservationClass" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="ReservationType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "trainSegment",
                "busSegment"
            })
            public static class JourneySegment {

                @XmlElement(name = "TrainSegment")
                protected OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment trainSegment;
                @XmlElement(name = "BusSegment")
                protected OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment busSegment;

                /**
                 * Obtiene el valor de la propiedad trainSegment.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment }
                 *     
                 */
                public OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment getTrainSegment() {
                    return trainSegment;
                }

                /**
                 * Define el valor de la propiedad trainSegment.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment }
                 *     
                 */
                public void setTrainSegment(OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment value) {
                    this.trainSegment = value;
                }

                /**
                 * Obtiene el valor de la propiedad busSegment.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment }
                 *     
                 */
                public OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment getBusSegment() {
                    return busSegment;
                }

                /**
                 * Define el valor de la propiedad busSegment.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment }
                 *     
                 */
                public void setBusSegment(OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment value) {
                    this.busSegment = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BusSegmentType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="ClassCodes" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
                 *                 &amp;lt;attribute name="ReservationClass" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="ReservationType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "classCodes",
                    "remarks"
                })
                public static class BusSegment
                    extends BusSegmentType
                {

                    @XmlElement(name = "ClassCodes", required = true)
                    protected List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.ClassCodes> classCodes;
                    @XmlElement(name = "Remarks")
                    protected OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.Remarks remarks;

                    /**
                     * Gets the value of the classCodes property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the classCodes property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getClassCodes().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.ClassCodes }
                     * 
                     * 
                     */
                    public List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.ClassCodes> getClassCodes() {
                        if (classCodes == null) {
                            classCodes = new ArrayList<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.ClassCodes>();
                        }
                        return this.classCodes;
                    }

                    /**
                     * Obtiene el valor de la propiedad remarks.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.Remarks }
                     *     
                     */
                    public OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.Remarks getRemarks() {
                        return remarks;
                    }

                    /**
                     * Define el valor de la propiedad remarks.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.Remarks }
                     *     
                     */
                    public void setRemarks(OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.Remarks value) {
                        this.remarks = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
                     *       &amp;lt;attribute name="ReservationClass" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="ReservationType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class ClassCodes
                        extends ClassCodeType
                    {

                        @XmlAttribute(name = "ReservationClass", required = true)
                        protected String reservationClass;
                        @XmlAttribute(name = "ReservationType", required = true)
                        protected String reservationType;
                        @XmlAttribute(name = "VehicleType")
                        protected String vehicleType;
                        @XmlAttribute(name = "AlternativeInd")
                        protected Boolean alternativeInd;

                        /**
                         * Obtiene el valor de la propiedad reservationClass.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getReservationClass() {
                            return reservationClass;
                        }

                        /**
                         * Define el valor de la propiedad reservationClass.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setReservationClass(String value) {
                            this.reservationClass = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad reservationType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getReservationType() {
                            return reservationType;
                        }

                        /**
                         * Define el valor de la propiedad reservationType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setReservationType(String value) {
                            this.reservationType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad vehicleType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getVehicleType() {
                            return vehicleType;
                        }

                        /**
                         * Define el valor de la propiedad vehicleType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setVehicleType(String value) {
                            this.vehicleType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad alternativeInd.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isAlternativeInd() {
                            return alternativeInd;
                        }

                        /**
                         * Define el valor de la propiedad alternativeInd.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setAlternativeInd(Boolean value) {
                            this.alternativeInd = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "remark"
                    })
                    public static class Remarks {

                        @XmlElement(name = "Remark", required = true)
                        protected List<WarningType> remark;

                        /**
                         * Gets the value of the remark property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the remark property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getRemark().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link WarningType }
                         * 
                         * 
                         */
                        public List<WarningType> getRemark() {
                            if (remark == null) {
                                remark = new ArrayList<WarningType>();
                            }
                            return this.remark;
                        }

                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="TrainOrigin" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="TrainDestination" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="RailAmenities" type="{http://www.opentravel.org/OTA/2003/05}RailAmenityType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="AvailabilityDetail" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="ClassCode" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
                 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" minOccurs="0"/&amp;gt;
                 *                   &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *                 &amp;lt;attribute name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="ReservationType"&amp;gt;
                 *                   &amp;lt;simpleType&amp;gt;
                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                       &amp;lt;enumeration value="Reservable"/&amp;gt;
                 *                       &amp;lt;enumeration value="NotReservable"/&amp;gt;
                 *                     &amp;lt;/restriction&amp;gt;
                 *                   &amp;lt;/simpleType&amp;gt;
                 *                 &amp;lt;/attribute&amp;gt;
                 *                 &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="IsBicycleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                 &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="VendorMessages" type="{http://www.opentravel.org/OTA/2003/05}VendorMessagesType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="Remarks" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
                 *       &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="RestrictedServiceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="CancelledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "trainOrigin",
                    "trainDestination",
                    "railAmenities",
                    "availabilityDetail",
                    "vendorMessages",
                    "remarks"
                })
                public static class TrainSegment
                    extends TrainSegmentType
                {

                    @XmlElement(name = "TrainOrigin")
                    protected LocationType trainOrigin;
                    @XmlElement(name = "TrainDestination")
                    protected LocationType trainDestination;
                    @XmlElement(name = "RailAmenities")
                    protected RailAmenityType railAmenities;
                    @XmlElement(name = "AvailabilityDetail", required = true)
                    protected List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail> availabilityDetail;
                    @XmlElement(name = "VendorMessages")
                    protected VendorMessagesType vendorMessages;
                    @XmlElement(name = "Remarks")
                    protected OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.Remarks remarks;
                    @XmlAttribute(name = "OnTimeRate")
                    protected BigDecimal onTimeRate;
                    @XmlAttribute(name = "AlternativeInd")
                    protected Boolean alternativeInd;
                    @XmlAttribute(name = "RestrictedServiceInd")
                    protected Boolean restrictedServiceInd;
                    @XmlAttribute(name = "CancelledInd")
                    protected Boolean cancelledInd;

                    /**
                     * Obtiene el valor de la propiedad trainOrigin.
                     * 
                     * @return
                     *     possible object is
                     *     {@link LocationType }
                     *     
                     */
                    public LocationType getTrainOrigin() {
                        return trainOrigin;
                    }

                    /**
                     * Define el valor de la propiedad trainOrigin.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link LocationType }
                     *     
                     */
                    public void setTrainOrigin(LocationType value) {
                        this.trainOrigin = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad trainDestination.
                     * 
                     * @return
                     *     possible object is
                     *     {@link LocationType }
                     *     
                     */
                    public LocationType getTrainDestination() {
                        return trainDestination;
                    }

                    /**
                     * Define el valor de la propiedad trainDestination.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link LocationType }
                     *     
                     */
                    public void setTrainDestination(LocationType value) {
                        this.trainDestination = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad railAmenities.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RailAmenityType }
                     *     
                     */
                    public RailAmenityType getRailAmenities() {
                        return railAmenities;
                    }

                    /**
                     * Define el valor de la propiedad railAmenities.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RailAmenityType }
                     *     
                     */
                    public void setRailAmenities(RailAmenityType value) {
                        this.railAmenities = value;
                    }

                    /**
                     * Gets the value of the availabilityDetail property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the availabilityDetail property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getAvailabilityDetail().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail }
                     * 
                     * 
                     */
                    public List<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail> getAvailabilityDetail() {
                        if (availabilityDetail == null) {
                            availabilityDetail = new ArrayList<OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail>();
                        }
                        return this.availabilityDetail;
                    }

                    /**
                     * Obtiene el valor de la propiedad vendorMessages.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VendorMessagesType }
                     *     
                     */
                    public VendorMessagesType getVendorMessages() {
                        return vendorMessages;
                    }

                    /**
                     * Define el valor de la propiedad vendorMessages.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VendorMessagesType }
                     *     
                     */
                    public void setVendorMessages(VendorMessagesType value) {
                        this.vendorMessages = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad remarks.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.Remarks }
                     *     
                     */
                    public OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.Remarks getRemarks() {
                        return remarks;
                    }

                    /**
                     * Define el valor de la propiedad remarks.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.Remarks }
                     *     
                     */
                    public void setRemarks(OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.Remarks value) {
                        this.remarks = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad onTimeRate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getOnTimeRate() {
                        return onTimeRate;
                    }

                    /**
                     * Define el valor de la propiedad onTimeRate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setOnTimeRate(BigDecimal value) {
                        this.onTimeRate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad alternativeInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isAlternativeInd() {
                        return alternativeInd;
                    }

                    /**
                     * Define el valor de la propiedad alternativeInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setAlternativeInd(Boolean value) {
                        this.alternativeInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad restrictedServiceInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isRestrictedServiceInd() {
                        return restrictedServiceInd;
                    }

                    /**
                     * Define el valor de la propiedad restrictedServiceInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setRestrictedServiceInd(Boolean value) {
                        this.restrictedServiceInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad cancelledInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isCancelledInd() {
                        return cancelledInd;
                    }

                    /**
                     * Define el valor de la propiedad cancelledInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setCancelledInd(Boolean value) {
                        this.cancelledInd = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="ClassCode" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
                     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AncillaryService" minOccurs="0"/&amp;gt;
                     *         &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *       &amp;lt;attribute name="ReservationClass" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="ReservationType"&amp;gt;
                     *         &amp;lt;simpleType&amp;gt;
                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *             &amp;lt;enumeration value="Reservable"/&amp;gt;
                     *             &amp;lt;enumeration value="NotReservable"/&amp;gt;
                     *           &amp;lt;/restriction&amp;gt;
                     *         &amp;lt;/simpleType&amp;gt;
                     *       &amp;lt;/attribute&amp;gt;
                     *       &amp;lt;attribute name="VehicleType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="IsBicycleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *       &amp;lt;attribute name="AlternativeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "classCode",
                        "reference",
                        "ancillaryService",
                        "accommodation"
                    })
                    public static class AvailabilityDetail {

                        @XmlElement(name = "ClassCode")
                        protected OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail.ClassCode classCode;
                        @XmlElement(name = "Reference")
                        protected String reference;
                        @XmlElement(name = "AncillaryService")
                        protected AncillaryService ancillaryService;
                        @XmlElement(name = "Accommodation")
                        protected AccommodationType accommodation;
                        @XmlAttribute(name = "ReservationClass")
                        protected String reservationClass;
                        @XmlAttribute(name = "ReservationType")
                        protected String reservationType;
                        @XmlAttribute(name = "VehicleType")
                        protected String vehicleType;
                        @XmlAttribute(name = "IsBicycleInd")
                        protected Boolean isBicycleInd;
                        @XmlAttribute(name = "AlternativeInd")
                        protected Boolean alternativeInd;

                        /**
                         * Obtiene el valor de la propiedad classCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail.ClassCode }
                         *     
                         */
                        public OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail.ClassCode getClassCode() {
                            return classCode;
                        }

                        /**
                         * Define el valor de la propiedad classCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail.ClassCode }
                         *     
                         */
                        public void setClassCode(OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail.ClassCode value) {
                            this.classCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad reference.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getReference() {
                            return reference;
                        }

                        /**
                         * Define el valor de la propiedad reference.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setReference(String value) {
                            this.reference = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad ancillaryService.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AncillaryService }
                         *     
                         */
                        public AncillaryService getAncillaryService() {
                            return ancillaryService;
                        }

                        /**
                         * Define el valor de la propiedad ancillaryService.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AncillaryService }
                         *     
                         */
                        public void setAncillaryService(AncillaryService value) {
                            this.ancillaryService = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad accommodation.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AccommodationType }
                         *     
                         */
                        public AccommodationType getAccommodation() {
                            return accommodation;
                        }

                        /**
                         * Define el valor de la propiedad accommodation.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AccommodationType }
                         *     
                         */
                        public void setAccommodation(AccommodationType value) {
                            this.accommodation = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad reservationClass.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getReservationClass() {
                            return reservationClass;
                        }

                        /**
                         * Define el valor de la propiedad reservationClass.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setReservationClass(String value) {
                            this.reservationClass = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad reservationType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getReservationType() {
                            return reservationType;
                        }

                        /**
                         * Define el valor de la propiedad reservationType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setReservationType(String value) {
                            this.reservationType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad vehicleType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getVehicleType() {
                            return vehicleType;
                        }

                        /**
                         * Define el valor de la propiedad vehicleType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setVehicleType(String value) {
                            this.vehicleType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad isBicycleInd.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isIsBicycleInd() {
                            return isBicycleInd;
                        }

                        /**
                         * Define el valor de la propiedad isBicycleInd.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setIsBicycleInd(Boolean value) {
                            this.isBicycleInd = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad alternativeInd.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isAlternativeInd() {
                            return alternativeInd;
                        }

                        /**
                         * Define el valor de la propiedad alternativeInd.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setAlternativeInd(Boolean value) {
                            this.alternativeInd = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ClassCodeType"&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class ClassCode
                            extends ClassCodeType
                        {


                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Remark" type="{http://www.opentravel.org/OTA/2003/05}WarningType" maxOccurs="99"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "remark"
                    })
                    public static class Remarks {

                        @XmlElement(name = "Remark", required = true)
                        protected List<WarningType> remark;

                        /**
                         * Gets the value of the remark property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the remark property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getRemark().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link WarningType }
                         * 
                         * 
                         */
                        public List<WarningType> getRemark() {
                            if (remark == null) {
                                remark = new ArrayList<WarningType>();
                            }
                            return this.remark;
                        }

                    }

                }

            }

        }

    }

}
