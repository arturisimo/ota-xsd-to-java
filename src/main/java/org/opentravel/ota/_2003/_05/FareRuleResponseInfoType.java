
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * The collection of Fare Rule requests and the applicable Fare Rule categories.
 * 
 * &lt;p&gt;Clase Java para FareRuleResponseInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FareRuleResponseInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FareRuleInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
 *                 &amp;lt;attribute name="LanguageRequested" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                 &amp;lt;attribute name="LanguageReturned" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FareRules" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"/&amp;gt;
 *         &amp;lt;element name="Routing" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Info" maxOccurs="99" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
 *                 &amp;lt;attribute name="RoutingConstructedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumPermittedMileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                 &amp;lt;attribute name="RoutingRestriction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AdvisoryInfo" maxOccurs="15" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
 *                 &amp;lt;attribute name="AdvisoryCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareRuleResponseInfoType", propOrder = {
    "fareRuleInfo",
    "fareRules",
    "routing",
    "advisoryInfo"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirRulesRS.FareRuleResponseInfo.class
})
public class FareRuleResponseInfoType {

    @XmlElement(name = "FareRuleInfo", required = true)
    protected FareRuleResponseInfoType.FareRuleInfo fareRuleInfo;
    @XmlElement(name = "FareRules", required = true)
    protected FormattedTextType fareRules;
    @XmlElement(name = "Routing")
    protected List<FareRuleResponseInfoType.Routing> routing;
    @XmlElement(name = "AdvisoryInfo")
    protected List<FareRuleResponseInfoType.AdvisoryInfo> advisoryInfo;

    /**
     * Obtiene el valor de la propiedad fareRuleInfo.
     * 
     * @return
     *     possible object is
     *     {@link FareRuleResponseInfoType.FareRuleInfo }
     *     
     */
    public FareRuleResponseInfoType.FareRuleInfo getFareRuleInfo() {
        return fareRuleInfo;
    }

    /**
     * Define el valor de la propiedad fareRuleInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRuleResponseInfoType.FareRuleInfo }
     *     
     */
    public void setFareRuleInfo(FareRuleResponseInfoType.FareRuleInfo value) {
        this.fareRuleInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad fareRules.
     * 
     * @return
     *     possible object is
     *     {@link FormattedTextType }
     *     
     */
    public FormattedTextType getFareRules() {
        return fareRules;
    }

    /**
     * Define el valor de la propiedad fareRules.
     * 
     * @param value
     *     allowed object is
     *     {@link FormattedTextType }
     *     
     */
    public void setFareRules(FormattedTextType value) {
        this.fareRules = value;
    }

    /**
     * Gets the value of the routing property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the routing property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRouting().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareRuleResponseInfoType.Routing }
     * 
     * 
     */
    public List<FareRuleResponseInfoType.Routing> getRouting() {
        if (routing == null) {
            routing = new ArrayList<FareRuleResponseInfoType.Routing>();
        }
        return this.routing;
    }

    /**
     * Gets the value of the advisoryInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the advisoryInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAdvisoryInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareRuleResponseInfoType.AdvisoryInfo }
     * 
     * 
     */
    public List<FareRuleResponseInfoType.AdvisoryInfo> getAdvisoryInfo() {
        if (advisoryInfo == null) {
            advisoryInfo = new ArrayList<FareRuleResponseInfoType.AdvisoryInfo>();
        }
        return this.advisoryInfo;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
     *       &amp;lt;attribute name="AdvisoryCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AdvisoryInfo
        extends FreeTextType
    {

        @XmlAttribute(name = "AdvisoryCode")
        protected String advisoryCode;

        /**
         * Obtiene el valor de la propiedad advisoryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdvisoryCode() {
            return advisoryCode;
        }

        /**
         * Define el valor de la propiedad advisoryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdvisoryCode(String value) {
            this.advisoryCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
     *       &amp;lt;attribute name="LanguageRequested" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="LanguageReturned" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FareRuleInfo
        extends FareInfoType
    {

        @XmlAttribute(name = "LanguageRequested")
        protected String languageRequested;
        @XmlAttribute(name = "LanguageReturned")
        protected String languageReturned;

        /**
         * Obtiene el valor de la propiedad languageRequested.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageRequested() {
            return languageRequested;
        }

        /**
         * Define el valor de la propiedad languageRequested.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageRequested(String value) {
            this.languageRequested = value;
        }

        /**
         * Obtiene el valor de la propiedad languageReturned.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageReturned() {
            return languageReturned;
        }

        /**
         * Define el valor de la propiedad languageReturned.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageReturned(String value) {
            this.languageReturned = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Info" maxOccurs="99" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
     *       &amp;lt;attribute name="RoutingConstructedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="MaximumPermittedMileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *       &amp;lt;attribute name="RoutingRestriction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "info"
    })
    public static class Routing {

        @XmlElement(name = "Info")
        protected List<FareRuleResponseInfoType.Routing.Info> info;
        @XmlAttribute(name = "Number")
        protected Integer number;
        @XmlAttribute(name = "RoutingConstructedInd")
        protected Boolean routingConstructedInd;
        @XmlAttribute(name = "MaximumPermittedMileage")
        protected BigInteger maximumPermittedMileage;
        @XmlAttribute(name = "RoutingRestriction")
        protected String routingRestriction;

        /**
         * Gets the value of the info property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the info property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FareRuleResponseInfoType.Routing.Info }
         * 
         * 
         */
        public List<FareRuleResponseInfoType.Routing.Info> getInfo() {
            if (info == null) {
                info = new ArrayList<FareRuleResponseInfoType.Routing.Info>();
            }
            return this.info;
        }

        /**
         * Obtiene el valor de la propiedad number.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNumber() {
            return number;
        }

        /**
         * Define el valor de la propiedad number.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNumber(Integer value) {
            this.number = value;
        }

        /**
         * Obtiene el valor de la propiedad routingConstructedInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRoutingConstructedInd() {
            return routingConstructedInd;
        }

        /**
         * Define el valor de la propiedad routingConstructedInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRoutingConstructedInd(Boolean value) {
            this.routingConstructedInd = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumPermittedMileage.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaximumPermittedMileage() {
            return maximumPermittedMileage;
        }

        /**
         * Define el valor de la propiedad maximumPermittedMileage.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaximumPermittedMileage(BigInteger value) {
            this.maximumPermittedMileage = value;
        }

        /**
         * Obtiene el valor de la propiedad routingRestriction.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoutingRestriction() {
            return routingRestriction;
        }

        /**
         * Define el valor de la propiedad routingRestriction.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoutingRestriction(String value) {
            this.routingRestriction = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Info {

            @XmlAttribute(name = "Direction")
            protected String direction;
            @XmlAttribute(name = "Text")
            protected String text;

            /**
             * Obtiene el valor de la propiedad direction.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDirection() {
                return direction;
            }

            /**
             * Define el valor de la propiedad direction.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDirection(String value) {
                this.direction = value;
            }

            /**
             * Obtiene el valor de la propiedad text.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getText() {
                return text;
            }

            /**
             * Define el valor de la propiedad text.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setText(String value) {
                this.text = value;
            }

        }

    }

}
