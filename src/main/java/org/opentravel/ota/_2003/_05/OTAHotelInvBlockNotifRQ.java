
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="InvBlocks" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="HotelRef" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="InvBlockDates" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockDatesGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="InvBlock" maxOccurs="99" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}InvBlockType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Guarantee" type="{http://www.opentravel.org/OTA/2003/05}GuaranteeType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="MarketCode" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                     &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                     &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "invBlocks",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_HotelInvBlockNotifRQ")
public class OTAHotelInvBlockNotifRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "InvBlocks")
    protected OTAHotelInvBlockNotifRQ.InvBlocks invBlocks;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlocks.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelInvBlockNotifRQ.InvBlocks }
     *     
     */
    public OTAHotelInvBlockNotifRQ.InvBlocks getInvBlocks() {
        return invBlocks;
    }

    /**
     * Define el valor de la propiedad invBlocks.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelInvBlockNotifRQ.InvBlocks }
     *     
     */
    public void setInvBlocks(OTAHotelInvBlockNotifRQ.InvBlocks value) {
        this.invBlocks = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="HotelRef" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="InvBlockDates" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockDatesGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="InvBlock" maxOccurs="99" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}InvBlockType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Guarantee" type="{http://www.opentravel.org/OTA/2003/05}GuaranteeType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="MarketCode" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                           &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                           &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotelRef",
        "invBlockDates",
        "invBlock",
        "contacts"
    })
    public static class InvBlocks {

        @XmlElement(name = "HotelRef")
        protected OTAHotelInvBlockNotifRQ.InvBlocks.HotelRef hotelRef;
        @XmlElement(name = "InvBlockDates")
        protected OTAHotelInvBlockNotifRQ.InvBlocks.InvBlockDates invBlockDates;
        @XmlElement(name = "InvBlock")
        protected List<OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock> invBlock;
        @XmlElement(name = "Contacts")
        protected OTAHotelInvBlockNotifRQ.InvBlocks.Contacts contacts;
        @XmlAttribute(name = "BookingStatus")
        protected String bookingStatus;
        @XmlAttribute(name = "InvBlockTypeCode")
        protected String invBlockTypeCode;
        @XmlAttribute(name = "InvBlockCode")
        protected String invBlockCode;
        @XmlAttribute(name = "InvBlockGroupingCode")
        protected String invBlockGroupingCode;
        @XmlAttribute(name = "InvBlockName")
        protected String invBlockName;
        @XmlAttribute(name = "InvBlockLongName")
        protected String invBlockLongName;
        @XmlAttribute(name = "InvBlockStatusCode")
        protected String invBlockStatusCode;
        @XmlAttribute(name = "PMS_InvBlockID")
        protected String pmsInvBlockID;
        @XmlAttribute(name = "OpportunityID")
        protected String opportunityID;
        @XmlAttribute(name = "InvBlockCompanyID")
        protected String invBlockCompanyID;
        @XmlAttribute(name = "RestrictedBookingCodeList")
        protected List<String> restrictedBookingCodeList;
        @XmlAttribute(name = "RestrictedViewingCodeList")
        protected List<String> restrictedViewingCodeList;
        @XmlAttribute(name = "TransactionAction")
        protected TransactionActionType transactionAction;
        @XmlAttribute(name = "TransactionDetail")
        protected String transactionDetail;
        @XmlAttribute(name = "QuoteID")
        protected String quoteID;

        /**
         * Obtiene el valor de la propiedad hotelRef.
         * 
         * @return
         *     possible object is
         *     {@link OTAHotelInvBlockNotifRQ.InvBlocks.HotelRef }
         *     
         */
        public OTAHotelInvBlockNotifRQ.InvBlocks.HotelRef getHotelRef() {
            return hotelRef;
        }

        /**
         * Define el valor de la propiedad hotelRef.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAHotelInvBlockNotifRQ.InvBlocks.HotelRef }
         *     
         */
        public void setHotelRef(OTAHotelInvBlockNotifRQ.InvBlocks.HotelRef value) {
            this.hotelRef = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockDates.
         * 
         * @return
         *     possible object is
         *     {@link OTAHotelInvBlockNotifRQ.InvBlocks.InvBlockDates }
         *     
         */
        public OTAHotelInvBlockNotifRQ.InvBlocks.InvBlockDates getInvBlockDates() {
            return invBlockDates;
        }

        /**
         * Define el valor de la propiedad invBlockDates.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAHotelInvBlockNotifRQ.InvBlocks.InvBlockDates }
         *     
         */
        public void setInvBlockDates(OTAHotelInvBlockNotifRQ.InvBlocks.InvBlockDates value) {
            this.invBlockDates = value;
        }

        /**
         * Gets the value of the invBlock property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the invBlock property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getInvBlock().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock }
         * 
         * 
         */
        public List<OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock> getInvBlock() {
            if (invBlock == null) {
                invBlock = new ArrayList<OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock>();
            }
            return this.invBlock;
        }

        /**
         * Obtiene el valor de la propiedad contacts.
         * 
         * @return
         *     possible object is
         *     {@link OTAHotelInvBlockNotifRQ.InvBlocks.Contacts }
         *     
         */
        public OTAHotelInvBlockNotifRQ.InvBlocks.Contacts getContacts() {
            return contacts;
        }

        /**
         * Define el valor de la propiedad contacts.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAHotelInvBlockNotifRQ.InvBlocks.Contacts }
         *     
         */
        public void setContacts(OTAHotelInvBlockNotifRQ.InvBlocks.Contacts value) {
            this.contacts = value;
        }

        /**
         * Obtiene el valor de la propiedad bookingStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBookingStatus() {
            return bookingStatus;
        }

        /**
         * Define el valor de la propiedad bookingStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBookingStatus(String value) {
            this.bookingStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockTypeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockTypeCode() {
            return invBlockTypeCode;
        }

        /**
         * Define el valor de la propiedad invBlockTypeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockTypeCode(String value) {
            this.invBlockTypeCode = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockCode() {
            return invBlockCode;
        }

        /**
         * Define el valor de la propiedad invBlockCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockCode(String value) {
            this.invBlockCode = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockGroupingCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockGroupingCode() {
            return invBlockGroupingCode;
        }

        /**
         * Define el valor de la propiedad invBlockGroupingCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockGroupingCode(String value) {
            this.invBlockGroupingCode = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockName() {
            return invBlockName;
        }

        /**
         * Define el valor de la propiedad invBlockName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockName(String value) {
            this.invBlockName = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockLongName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockLongName() {
            return invBlockLongName;
        }

        /**
         * Define el valor de la propiedad invBlockLongName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockLongName(String value) {
            this.invBlockLongName = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockStatusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockStatusCode() {
            return invBlockStatusCode;
        }

        /**
         * Define el valor de la propiedad invBlockStatusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockStatusCode(String value) {
            this.invBlockStatusCode = value;
        }

        /**
         * Obtiene el valor de la propiedad pmsInvBlockID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPMSInvBlockID() {
            return pmsInvBlockID;
        }

        /**
         * Define el valor de la propiedad pmsInvBlockID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPMSInvBlockID(String value) {
            this.pmsInvBlockID = value;
        }

        /**
         * Obtiene el valor de la propiedad opportunityID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOpportunityID() {
            return opportunityID;
        }

        /**
         * Define el valor de la propiedad opportunityID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOpportunityID(String value) {
            this.opportunityID = value;
        }

        /**
         * Obtiene el valor de la propiedad invBlockCompanyID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvBlockCompanyID() {
            return invBlockCompanyID;
        }

        /**
         * Define el valor de la propiedad invBlockCompanyID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvBlockCompanyID(String value) {
            this.invBlockCompanyID = value;
        }

        /**
         * Gets the value of the restrictedBookingCodeList property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restrictedBookingCodeList property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRestrictedBookingCodeList().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getRestrictedBookingCodeList() {
            if (restrictedBookingCodeList == null) {
                restrictedBookingCodeList = new ArrayList<String>();
            }
            return this.restrictedBookingCodeList;
        }

        /**
         * Gets the value of the restrictedViewingCodeList property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restrictedViewingCodeList property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRestrictedViewingCodeList().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getRestrictedViewingCodeList() {
            if (restrictedViewingCodeList == null) {
                restrictedViewingCodeList = new ArrayList<String>();
            }
            return this.restrictedViewingCodeList;
        }

        /**
         * Obtiene el valor de la propiedad transactionAction.
         * 
         * @return
         *     possible object is
         *     {@link TransactionActionType }
         *     
         */
        public TransactionActionType getTransactionAction() {
            return transactionAction;
        }

        /**
         * Define el valor de la propiedad transactionAction.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionActionType }
         *     
         */
        public void setTransactionAction(TransactionActionType value) {
            this.transactionAction = value;
        }

        /**
         * Obtiene el valor de la propiedad transactionDetail.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionDetail() {
            return transactionDetail;
        }

        /**
         * Define el valor de la propiedad transactionDetail.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionDetail(String value) {
            this.transactionDetail = value;
        }

        /**
         * Obtiene el valor de la propiedad quoteID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuoteID() {
            return quoteID;
        }

        /**
         * Define el valor de la propiedad quoteID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuoteID(String value) {
            this.quoteID = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contact"
        })
        public static class Contacts {

            @XmlElement(name = "Contact", required = true)
            protected List<ContactPersonType> contact;

            /**
             * Gets the value of the contact property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getContact().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ContactPersonType }
             * 
             * 
             */
            public List<ContactPersonType> getContact() {
                if (contact == null) {
                    contact = new ArrayList<ContactPersonType>();
                }
                return this.contact;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class HotelRef {

            @XmlAttribute(name = "ChainCode")
            protected String chainCode;
            @XmlAttribute(name = "BrandCode")
            protected String brandCode;
            @XmlAttribute(name = "HotelCode")
            protected String hotelCode;
            @XmlAttribute(name = "HotelCityCode")
            protected String hotelCityCode;
            @XmlAttribute(name = "HotelName")
            protected String hotelName;
            @XmlAttribute(name = "HotelCodeContext")
            protected String hotelCodeContext;
            @XmlAttribute(name = "ChainName")
            protected String chainName;
            @XmlAttribute(name = "BrandName")
            protected String brandName;
            @XmlAttribute(name = "AreaID")
            protected String areaID;
            @XmlAttribute(name = "TTIcode")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger ttIcode;

            /**
             * Obtiene el valor de la propiedad chainCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainCode() {
                return chainCode;
            }

            /**
             * Define el valor de la propiedad chainCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainCode(String value) {
                this.chainCode = value;
            }

            /**
             * Obtiene el valor de la propiedad brandCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandCode() {
                return brandCode;
            }

            /**
             * Define el valor de la propiedad brandCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandCode(String value) {
                this.brandCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCode() {
                return hotelCode;
            }

            /**
             * Define el valor de la propiedad hotelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCode(String value) {
                this.hotelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCityCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCityCode() {
                return hotelCityCode;
            }

            /**
             * Define el valor de la propiedad hotelCityCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCityCode(String value) {
                this.hotelCityCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelName() {
                return hotelName;
            }

            /**
             * Define el valor de la propiedad hotelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelName(String value) {
                this.hotelName = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCodeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCodeContext() {
                return hotelCodeContext;
            }

            /**
             * Define el valor de la propiedad hotelCodeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCodeContext(String value) {
                this.hotelCodeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad chainName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainName() {
                return chainName;
            }

            /**
             * Define el valor de la propiedad chainName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainName(String value) {
                this.chainName = value;
            }

            /**
             * Obtiene el valor de la propiedad brandName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandName() {
                return brandName;
            }

            /**
             * Define el valor de la propiedad brandName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandName(String value) {
                this.brandName = value;
            }

            /**
             * Obtiene el valor de la propiedad areaID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAreaID() {
                return areaID;
            }

            /**
             * Define el valor de la propiedad areaID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAreaID(String value) {
                this.areaID = value;
            }

            /**
             * Obtiene el valor de la propiedad ttIcode.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTTIcode() {
                return ttIcode;
            }

            /**
             * Define el valor de la propiedad ttIcode.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTTIcode(BigInteger value) {
                this.ttIcode = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}InvBlockType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Guarantee" type="{http://www.opentravel.org/OTA/2003/05}GuaranteeType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="MarketCode" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                 &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                 &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "guarantee",
            "marketCode"
        })
        public static class InvBlock
            extends InvBlockType
        {

            @XmlElement(name = "Guarantee")
            protected GuaranteeType guarantee;
            @XmlElement(name = "MarketCode")
            protected List<OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock.MarketCode> marketCode;

            /**
             * Obtiene el valor de la propiedad guarantee.
             * 
             * @return
             *     possible object is
             *     {@link GuaranteeType }
             *     
             */
            public GuaranteeType getGuarantee() {
                return guarantee;
            }

            /**
             * Define el valor de la propiedad guarantee.
             * 
             * @param value
             *     allowed object is
             *     {@link GuaranteeType }
             *     
             */
            public void setGuarantee(GuaranteeType value) {
                this.guarantee = value;
            }

            /**
             * Gets the value of the marketCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMarketCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock.MarketCode }
             * 
             * 
             */
            public List<OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock.MarketCode> getMarketCode() {
                if (marketCode == null) {
                    marketCode = new ArrayList<OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock.MarketCode>();
                }
                return this.marketCode;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *       &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *       &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MarketCode {

                @XmlAttribute(name = "MarketCode")
                protected String marketCode;
                @XmlAttribute(name = "MarketCodeName")
                protected String marketCodeName;
                @XmlAttribute(name = "CommissionableIndicator")
                protected Boolean commissionableIndicator;
                @XmlAttribute(name = "SourceOfBusiness")
                protected String sourceOfBusiness;

                /**
                 * Obtiene el valor de la propiedad marketCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMarketCode() {
                    return marketCode;
                }

                /**
                 * Define el valor de la propiedad marketCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMarketCode(String value) {
                    this.marketCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad marketCodeName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMarketCodeName() {
                    return marketCodeName;
                }

                /**
                 * Define el valor de la propiedad marketCodeName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMarketCodeName(String value) {
                    this.marketCodeName = value;
                }

                /**
                 * Obtiene el valor de la propiedad commissionableIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isCommissionableIndicator() {
                    return commissionableIndicator;
                }

                /**
                 * Define el valor de la propiedad commissionableIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setCommissionableIndicator(Boolean value) {
                    this.commissionableIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad sourceOfBusiness.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSourceOfBusiness() {
                    return sourceOfBusiness;
                }

                /**
                 * Define el valor de la propiedad sourceOfBusiness.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSourceOfBusiness(String value) {
                    this.sourceOfBusiness = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockDatesGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class InvBlockDates {

            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;
            @XmlAttribute(name = "EndDateExtensionIndicator")
            protected Boolean endDateExtensionIndicator;
            @XmlAttribute(name = "AbsoluteCutoff")
            protected String absoluteCutoff;
            @XmlAttribute(name = "OffsetDuration")
            protected Duration offsetDuration;
            @XmlAttribute(name = "OffsetCalculationMode")
            protected String offsetCalculationMode;

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

            /**
             * Obtiene el valor de la propiedad endDateExtensionIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isEndDateExtensionIndicator() {
                return endDateExtensionIndicator;
            }

            /**
             * Define el valor de la propiedad endDateExtensionIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setEndDateExtensionIndicator(Boolean value) {
                this.endDateExtensionIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad absoluteCutoff.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAbsoluteCutoff() {
                return absoluteCutoff;
            }

            /**
             * Define el valor de la propiedad absoluteCutoff.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAbsoluteCutoff(String value) {
                this.absoluteCutoff = value;
            }

            /**
             * Obtiene el valor de la propiedad offsetDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getOffsetDuration() {
                return offsetDuration;
            }

            /**
             * Define el valor de la propiedad offsetDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setOffsetDuration(Duration value) {
                this.offsetDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad offsetCalculationMode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOffsetCalculationMode() {
                return offsetCalculationMode;
            }

            /**
             * Define el valor de la propiedad offsetCalculationMode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOffsetCalculationMode(String value) {
                this.offsetCalculationMode = value;
            }

        }

    }

}
