
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}POS_Type"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PriceStructure" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="ExcludePointsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="TicketingCountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                 &amp;lt;attribute name="TicketingCityCode"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaNumericLength3to5"&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                 &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RequestCriterion"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ConfirmedBookingInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="AirItinerary" type="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}TravelerInfoType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Arranger" type="{http://www.opentravel.org/OTA/2003/05}AirArrangerType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Ticketing" type="{http://www.opentravel.org/OTA/2003/05}TicketingInfoType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="PricingIndicator" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PricingInfoGroup"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Account" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="StatisticalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="ValidatingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;attribute name="PriceType" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="NUC_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                     &amp;lt;attribute name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="PurchasedOffers" type="{http://www.opentravel.org/OTA/2003/05}AirPurchasedOfferType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="BookingReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="LastModified" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AirItinerary" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ItineraryUpgrade" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="UpgradeSegment" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="ItinerarySegmentRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                               &amp;lt;attribute name="MarketingResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                                               &amp;lt;attribute name="OperatingResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Itinerary" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType"&amp;gt;
 *                                     &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Arranger" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirArrangerType"&amp;gt;
 *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="BaggageInfo" type="{http://www.opentravel.org/OTA/2003/05}BaggageQueryType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceActionType" maxOccurs="999" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="OriginDestination" maxOccurs="10" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PresentedPurchased" maxOccurs="999" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="LandProductInfo" type="{http://www.opentravel.org/OTA/2003/05}AirLandProductType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="PurchasedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="SeatInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="DetailedInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSeatMarketingClassType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="TotalReqSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="ChangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="SpecificFlight" type="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Traveler" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;choice&amp;gt;
 *                             &amp;lt;element name="PassengerTypeQuantity" maxOccurs="10"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="AirTraveler" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirTravelerType"&amp;gt;
 *                                     &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;attribute name="IsLeadInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/choice&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TravelPreferences" maxOccurs="10" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType"&amp;gt;
 *                           &amp;lt;attribute name="FlexDatePref"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="Outbound"/&amp;gt;
 *                                 &amp;lt;enumeration value="Return"/&amp;gt;
 *                                 &amp;lt;enumeration value="Both"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="FlexWeekendInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="FlexLevelInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="NoFareBreakInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="OriginDestinationRPHs" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TripInsurance" type="{http://www.opentravel.org/OTA/2003/05}AirInsuranceOfferType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="OfferStage"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Shopping"/&amp;gt;
 *                       &amp;lt;enumeration value="BookingConfirmation"/&amp;gt;
 *                       &amp;lt;enumeration value="PostConfirmation"/&amp;gt;
 *                       &amp;lt;enumeration value="CheckIn"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="TripInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="CodeSource" use="required"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attribute name="MaxOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "priceStructure",
    "requestCriterion",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_AirGetOfferRQ")
public class OTAAirGetOfferRQ {

    @XmlElement(name = "POS", required = true)
    protected OTAAirGetOfferRQ.POS pos;
    @XmlElement(name = "PriceStructure")
    protected OTAAirGetOfferRQ.PriceStructure priceStructure;
    @XmlElement(name = "RequestCriterion", required = true)
    protected OTAAirGetOfferRQ.RequestCriterion requestCriterion;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "MaxOfferQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxOfferQty;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirGetOfferRQ.POS }
     *     
     */
    public OTAAirGetOfferRQ.POS getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirGetOfferRQ.POS }
     *     
     */
    public void setPOS(OTAAirGetOfferRQ.POS value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad priceStructure.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirGetOfferRQ.PriceStructure }
     *     
     */
    public OTAAirGetOfferRQ.PriceStructure getPriceStructure() {
        return priceStructure;
    }

    /**
     * Define el valor de la propiedad priceStructure.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirGetOfferRQ.PriceStructure }
     *     
     */
    public void setPriceStructure(OTAAirGetOfferRQ.PriceStructure value) {
        this.priceStructure = value;
    }

    /**
     * Obtiene el valor de la propiedad requestCriterion.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirGetOfferRQ.RequestCriterion }
     *     
     */
    public OTAAirGetOfferRQ.RequestCriterion getRequestCriterion() {
        return requestCriterion;
    }

    /**
     * Define el valor de la propiedad requestCriterion.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirGetOfferRQ.RequestCriterion }
     *     
     */
    public void setRequestCriterion(OTAAirGetOfferRQ.RequestCriterion value) {
        this.requestCriterion = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad maxOfferQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxOfferQty() {
        return maxOfferQty;
    }

    /**
     * Define el valor de la propiedad maxOfferQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxOfferQty(BigInteger value) {
        this.maxOfferQty = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}POS_Type"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class POS
        extends POSType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="ExcludePointsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="TicketingCountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *       &amp;lt;attribute name="TicketingCityCode"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaNumericLength3to5"&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PriceStructure {

        @XmlAttribute(name = "ExcludePointsInd")
        protected Boolean excludePointsInd;
        @XmlAttribute(name = "TicketingCountryCode")
        protected String ticketingCountryCode;
        @XmlAttribute(name = "TicketingCityCode")
        protected String ticketingCityCode;
        @XmlAttribute(name = "DisplayCurrency")
        protected String displayCurrency;
        @XmlAttribute(name = "PricingCurrency")
        protected String pricingCurrency;

        /**
         * Obtiene el valor de la propiedad excludePointsInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExcludePointsInd() {
            return excludePointsInd;
        }

        /**
         * Define el valor de la propiedad excludePointsInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExcludePointsInd(Boolean value) {
            this.excludePointsInd = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketingCountryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketingCountryCode() {
            return ticketingCountryCode;
        }

        /**
         * Define el valor de la propiedad ticketingCountryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketingCountryCode(String value) {
            this.ticketingCountryCode = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketingCityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketingCityCode() {
            return ticketingCityCode;
        }

        /**
         * Define el valor de la propiedad ticketingCityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketingCityCode(String value) {
            this.ticketingCityCode = value;
        }

        /**
         * Obtiene el valor de la propiedad displayCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayCurrency() {
            return displayCurrency;
        }

        /**
         * Define el valor de la propiedad displayCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayCurrency(String value) {
            this.displayCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad pricingCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricingCurrency() {
            return pricingCurrency;
        }

        /**
         * Define el valor de la propiedad pricingCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricingCurrency(String value) {
            this.pricingCurrency = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ConfirmedBookingInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="AirItinerary" type="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}TravelerInfoType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Arranger" type="{http://www.opentravel.org/OTA/2003/05}AirArrangerType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Ticketing" type="{http://www.opentravel.org/OTA/2003/05}TicketingInfoType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PricingIndicator" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PricingInfoGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Account" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="StatisticalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="ValidatingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="PriceType" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="NUC_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                           &amp;lt;attribute name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="PurchasedOffers" type="{http://www.opentravel.org/OTA/2003/05}AirPurchasedOfferType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="BookingReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="LastModified" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AirItinerary" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ItineraryUpgrade" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="UpgradeSegment" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="ItinerarySegmentRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="MarketingResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                                     &amp;lt;attribute name="OperatingResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Itinerary" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType"&amp;gt;
     *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Arranger" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirArrangerType"&amp;gt;
     *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="BaggageInfo" type="{http://www.opentravel.org/OTA/2003/05}BaggageQueryType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceActionType" maxOccurs="999" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="OriginDestination" maxOccurs="10" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PresentedPurchased" maxOccurs="999" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="LandProductInfo" type="{http://www.opentravel.org/OTA/2003/05}AirLandProductType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="PurchasedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="SeatInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="DetailedInfo" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSeatMarketingClassType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="TotalReqSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="ChangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="SpecificFlight" type="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Traveler" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;choice&amp;gt;
     *                   &amp;lt;element name="PassengerTypeQuantity" maxOccurs="10"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AirTraveler" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirTravelerType"&amp;gt;
     *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="IsLeadInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/choice&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TravelPreferences" maxOccurs="10" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType"&amp;gt;
     *                 &amp;lt;attribute name="FlexDatePref"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="Outbound"/&amp;gt;
     *                       &amp;lt;enumeration value="Return"/&amp;gt;
     *                       &amp;lt;enumeration value="Both"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="FlexWeekendInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="FlexLevelInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="NoFareBreakInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="OriginDestinationRPHs" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TripInsurance" type="{http://www.opentravel.org/OTA/2003/05}AirInsuranceOfferType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="OfferStage"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Shopping"/&amp;gt;
     *             &amp;lt;enumeration value="BookingConfirmation"/&amp;gt;
     *             &amp;lt;enumeration value="PostConfirmation"/&amp;gt;
     *             &amp;lt;enumeration value="CheckIn"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="TripInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="CodeSource" use="required"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
     *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmedBookingInfo",
        "airItinerary",
        "arranger",
        "baggageInfo",
        "includeExclude",
        "originDestination",
        "presentedPurchased",
        "pricingQualifier",
        "seatInfo",
        "specificFlight",
        "traveler",
        "travelPreferences",
        "tripInsurance",
        "tpaExtensions"
    })
    public static class RequestCriterion {

        @XmlElement(name = "ConfirmedBookingInfo")
        protected OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo confirmedBookingInfo;
        @XmlElement(name = "AirItinerary")
        protected OTAAirGetOfferRQ.RequestCriterion.AirItinerary airItinerary;
        @XmlElement(name = "Arranger")
        protected OTAAirGetOfferRQ.RequestCriterion.Arranger arranger;
        @XmlElement(name = "BaggageInfo")
        protected List<BaggageQueryType> baggageInfo;
        @XmlElement(name = "IncludeExclude")
        protected List<AncillaryServiceActionType> includeExclude;
        @XmlElement(name = "OriginDestination")
        protected List<OTAAirGetOfferRQ.RequestCriterion.OriginDestination> originDestination;
        @XmlElement(name = "PresentedPurchased")
        protected List<OTAAirGetOfferRQ.RequestCriterion.PresentedPurchased> presentedPurchased;
        @XmlElement(name = "PricingQualifier")
        protected List<AirPricingQualifierType> pricingQualifier;
        @XmlElement(name = "SeatInfo")
        protected OTAAirGetOfferRQ.RequestCriterion.SeatInfo seatInfo;
        @XmlElement(name = "SpecificFlight")
        protected SpecificFlightInfoType specificFlight;
        @XmlElement(name = "Traveler")
        protected OTAAirGetOfferRQ.RequestCriterion.Traveler traveler;
        @XmlElement(name = "TravelPreferences")
        protected List<OTAAirGetOfferRQ.RequestCriterion.TravelPreferences> travelPreferences;
        @XmlElement(name = "TripInsurance")
        protected AirInsuranceOfferType tripInsurance;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;
        @XmlAttribute(name = "OfferStage")
        protected String offerStage;
        @XmlAttribute(name = "TripInsuranceInd")
        protected Boolean tripInsuranceInd;
        @XmlAttribute(name = "CodeSource", required = true)
        protected String codeSource;

        /**
         * Obtiene el valor de la propiedad confirmedBookingInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo }
         *     
         */
        public OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo getConfirmedBookingInfo() {
            return confirmedBookingInfo;
        }

        /**
         * Define el valor de la propiedad confirmedBookingInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo }
         *     
         */
        public void setConfirmedBookingInfo(OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo value) {
            this.confirmedBookingInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad airItinerary.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.AirItinerary }
         *     
         */
        public OTAAirGetOfferRQ.RequestCriterion.AirItinerary getAirItinerary() {
            return airItinerary;
        }

        /**
         * Define el valor de la propiedad airItinerary.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.AirItinerary }
         *     
         */
        public void setAirItinerary(OTAAirGetOfferRQ.RequestCriterion.AirItinerary value) {
            this.airItinerary = value;
        }

        /**
         * Obtiene el valor de la propiedad arranger.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.Arranger }
         *     
         */
        public OTAAirGetOfferRQ.RequestCriterion.Arranger getArranger() {
            return arranger;
        }

        /**
         * Define el valor de la propiedad arranger.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.Arranger }
         *     
         */
        public void setArranger(OTAAirGetOfferRQ.RequestCriterion.Arranger value) {
            this.arranger = value;
        }

        /**
         * Gets the value of the baggageInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baggageInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getBaggageInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link BaggageQueryType }
         * 
         * 
         */
        public List<BaggageQueryType> getBaggageInfo() {
            if (baggageInfo == null) {
                baggageInfo = new ArrayList<BaggageQueryType>();
            }
            return this.baggageInfo;
        }

        /**
         * Gets the value of the includeExclude property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the includeExclude property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getIncludeExclude().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AncillaryServiceActionType }
         * 
         * 
         */
        public List<AncillaryServiceActionType> getIncludeExclude() {
            if (includeExclude == null) {
                includeExclude = new ArrayList<AncillaryServiceActionType>();
            }
            return this.includeExclude;
        }

        /**
         * Gets the value of the originDestination property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestination property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestination().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirGetOfferRQ.RequestCriterion.OriginDestination }
         * 
         * 
         */
        public List<OTAAirGetOfferRQ.RequestCriterion.OriginDestination> getOriginDestination() {
            if (originDestination == null) {
                originDestination = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.OriginDestination>();
            }
            return this.originDestination;
        }

        /**
         * Gets the value of the presentedPurchased property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the presentedPurchased property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPresentedPurchased().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirGetOfferRQ.RequestCriterion.PresentedPurchased }
         * 
         * 
         */
        public List<OTAAirGetOfferRQ.RequestCriterion.PresentedPurchased> getPresentedPurchased() {
            if (presentedPurchased == null) {
                presentedPurchased = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.PresentedPurchased>();
            }
            return this.presentedPurchased;
        }

        /**
         * Gets the value of the pricingQualifier property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pricingQualifier property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPricingQualifier().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirPricingQualifierType }
         * 
         * 
         */
        public List<AirPricingQualifierType> getPricingQualifier() {
            if (pricingQualifier == null) {
                pricingQualifier = new ArrayList<AirPricingQualifierType>();
            }
            return this.pricingQualifier;
        }

        /**
         * Obtiene el valor de la propiedad seatInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.SeatInfo }
         *     
         */
        public OTAAirGetOfferRQ.RequestCriterion.SeatInfo getSeatInfo() {
            return seatInfo;
        }

        /**
         * Define el valor de la propiedad seatInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.SeatInfo }
         *     
         */
        public void setSeatInfo(OTAAirGetOfferRQ.RequestCriterion.SeatInfo value) {
            this.seatInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad specificFlight.
         * 
         * @return
         *     possible object is
         *     {@link SpecificFlightInfoType }
         *     
         */
        public SpecificFlightInfoType getSpecificFlight() {
            return specificFlight;
        }

        /**
         * Define el valor de la propiedad specificFlight.
         * 
         * @param value
         *     allowed object is
         *     {@link SpecificFlightInfoType }
         *     
         */
        public void setSpecificFlight(SpecificFlightInfoType value) {
            this.specificFlight = value;
        }

        /**
         * Obtiene el valor de la propiedad traveler.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.Traveler }
         *     
         */
        public OTAAirGetOfferRQ.RequestCriterion.Traveler getTraveler() {
            return traveler;
        }

        /**
         * Define el valor de la propiedad traveler.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirGetOfferRQ.RequestCriterion.Traveler }
         *     
         */
        public void setTraveler(OTAAirGetOfferRQ.RequestCriterion.Traveler value) {
            this.traveler = value;
        }

        /**
         * Gets the value of the travelPreferences property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelPreferences property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTravelPreferences().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirGetOfferRQ.RequestCriterion.TravelPreferences }
         * 
         * 
         */
        public List<OTAAirGetOfferRQ.RequestCriterion.TravelPreferences> getTravelPreferences() {
            if (travelPreferences == null) {
                travelPreferences = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.TravelPreferences>();
            }
            return this.travelPreferences;
        }

        /**
         * Obtiene el valor de la propiedad tripInsurance.
         * 
         * @return
         *     possible object is
         *     {@link AirInsuranceOfferType }
         *     
         */
        public AirInsuranceOfferType getTripInsurance() {
            return tripInsurance;
        }

        /**
         * Define el valor de la propiedad tripInsurance.
         * 
         * @param value
         *     allowed object is
         *     {@link AirInsuranceOfferType }
         *     
         */
        public void setTripInsurance(AirInsuranceOfferType value) {
            this.tripInsurance = value;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

        /**
         * Obtiene el valor de la propiedad offerStage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfferStage() {
            return offerStage;
        }

        /**
         * Define el valor de la propiedad offerStage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfferStage(String value) {
            this.offerStage = value;
        }

        /**
         * Obtiene el valor de la propiedad tripInsuranceInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isTripInsuranceInd() {
            return tripInsuranceInd;
        }

        /**
         * Define el valor de la propiedad tripInsuranceInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setTripInsuranceInd(Boolean value) {
            this.tripInsuranceInd = value;
        }

        /**
         * Obtiene el valor de la propiedad codeSource.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeSource() {
            return codeSource;
        }

        /**
         * Define el valor de la propiedad codeSource.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeSource(String value) {
            this.codeSource = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ItineraryUpgrade" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="UpgradeSegment" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="ItinerarySegmentRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="MarketingResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *                           &amp;lt;attribute name="OperatingResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Itinerary" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType"&amp;gt;
         *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "itineraryUpgrade",
            "itinerary"
        })
        public static class AirItinerary {

            @XmlElement(name = "ItineraryUpgrade")
            protected OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade itineraryUpgrade;
            @XmlElement(name = "Itinerary")
            protected List<OTAAirGetOfferRQ.RequestCriterion.AirItinerary.Itinerary> itinerary;

            /**
             * Obtiene el valor de la propiedad itineraryUpgrade.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade }
             *     
             */
            public OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade getItineraryUpgrade() {
                return itineraryUpgrade;
            }

            /**
             * Define el valor de la propiedad itineraryUpgrade.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade }
             *     
             */
            public void setItineraryUpgrade(OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade value) {
                this.itineraryUpgrade = value;
            }

            /**
             * Gets the value of the itinerary property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the itinerary property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getItinerary().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirGetOfferRQ.RequestCriterion.AirItinerary.Itinerary }
             * 
             * 
             */
            public List<OTAAirGetOfferRQ.RequestCriterion.AirItinerary.Itinerary> getItinerary() {
                if (itinerary == null) {
                    itinerary = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.AirItinerary.Itinerary>();
                }
                return this.itinerary;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType"&amp;gt;
             *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Itinerary
                extends PricedItineraryType
            {

                @XmlAttribute(name = "RPH")
                protected Integer rph;

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setRPH(Integer value) {
                    this.rph = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="UpgradeSegment" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="ItinerarySegmentRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="MarketingResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
             *                 &amp;lt;attribute name="OperatingResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "upgradeSegment"
            })
            public static class ItineraryUpgrade {

                @XmlElement(name = "UpgradeSegment", required = true)
                protected List<OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade.UpgradeSegment> upgradeSegment;

                /**
                 * Gets the value of the upgradeSegment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the upgradeSegment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getUpgradeSegment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade.UpgradeSegment }
                 * 
                 * 
                 */
                public List<OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade.UpgradeSegment> getUpgradeSegment() {
                    if (upgradeSegment == null) {
                        upgradeSegment = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.AirItinerary.ItineraryUpgrade.UpgradeSegment>();
                    }
                    return this.upgradeSegment;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="ItinerarySegmentRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="MarketingResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                 *       &amp;lt;attribute name="OperatingResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class UpgradeSegment {

                    @XmlAttribute(name = "ItinerarySegmentRPH", required = true)
                    protected String itinerarySegmentRPH;
                    @XmlAttribute(name = "MarketingResBookDesigCode")
                    protected String marketingResBookDesigCode;
                    @XmlAttribute(name = "OperatingResBookDesigCode")
                    protected String operatingResBookDesigCode;

                    /**
                     * Obtiene el valor de la propiedad itinerarySegmentRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getItinerarySegmentRPH() {
                        return itinerarySegmentRPH;
                    }

                    /**
                     * Define el valor de la propiedad itinerarySegmentRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setItinerarySegmentRPH(String value) {
                        this.itinerarySegmentRPH = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad marketingResBookDesigCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getMarketingResBookDesigCode() {
                        return marketingResBookDesigCode;
                    }

                    /**
                     * Define el valor de la propiedad marketingResBookDesigCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setMarketingResBookDesigCode(String value) {
                        this.marketingResBookDesigCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad operatingResBookDesigCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOperatingResBookDesigCode() {
                        return operatingResBookDesigCode;
                    }

                    /**
                     * Define el valor de la propiedad operatingResBookDesigCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOperatingResBookDesigCode(String value) {
                        this.operatingResBookDesigCode = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirArrangerType"&amp;gt;
         *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Arranger
            extends AirArrangerType
        {

            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="AirItinerary" type="{http://www.opentravel.org/OTA/2003/05}PricedItineraryType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}TravelerInfoType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Arranger" type="{http://www.opentravel.org/OTA/2003/05}AirArrangerType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Payment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Ticketing" type="{http://www.opentravel.org/OTA/2003/05}TicketingInfoType" maxOccurs="99" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PricingIndicator" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PricingInfoGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Account" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="StatisticalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="ValidatingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="PriceType" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="NUC_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                 &amp;lt;attribute name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="PurchasedOffers" type="{http://www.opentravel.org/OTA/2003/05}AirPurchasedOfferType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="BookingReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="99" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="LastModified" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "airItinerary",
            "traveler",
            "arranger",
            "payment",
            "ticketing",
            "pricing",
            "purchasedOffers",
            "bookingReferenceID"
        })
        public static class ConfirmedBookingInfo {

            @XmlElement(name = "AirItinerary")
            protected PricedItineraryType airItinerary;
            @XmlElement(name = "Traveler")
            protected TravelerInfoType traveler;
            @XmlElement(name = "Arranger")
            protected AirArrangerType arranger;
            @XmlElement(name = "Payment")
            protected PaymentFormType payment;
            @XmlElement(name = "Ticketing")
            protected List<TicketingInfoType> ticketing;
            @XmlElement(name = "Pricing")
            protected OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing pricing;
            @XmlElement(name = "PurchasedOffers")
            protected AirPurchasedOfferType purchasedOffers;
            @XmlElement(name = "BookingReferenceID")
            protected List<UniqueIDType> bookingReferenceID;
            @XmlAttribute(name = "LastModified")
            protected String lastModified;

            /**
             * Obtiene el valor de la propiedad airItinerary.
             * 
             * @return
             *     possible object is
             *     {@link PricedItineraryType }
             *     
             */
            public PricedItineraryType getAirItinerary() {
                return airItinerary;
            }

            /**
             * Define el valor de la propiedad airItinerary.
             * 
             * @param value
             *     allowed object is
             *     {@link PricedItineraryType }
             *     
             */
            public void setAirItinerary(PricedItineraryType value) {
                this.airItinerary = value;
            }

            /**
             * Obtiene el valor de la propiedad traveler.
             * 
             * @return
             *     possible object is
             *     {@link TravelerInfoType }
             *     
             */
            public TravelerInfoType getTraveler() {
                return traveler;
            }

            /**
             * Define el valor de la propiedad traveler.
             * 
             * @param value
             *     allowed object is
             *     {@link TravelerInfoType }
             *     
             */
            public void setTraveler(TravelerInfoType value) {
                this.traveler = value;
            }

            /**
             * Obtiene el valor de la propiedad arranger.
             * 
             * @return
             *     possible object is
             *     {@link AirArrangerType }
             *     
             */
            public AirArrangerType getArranger() {
                return arranger;
            }

            /**
             * Define el valor de la propiedad arranger.
             * 
             * @param value
             *     allowed object is
             *     {@link AirArrangerType }
             *     
             */
            public void setArranger(AirArrangerType value) {
                this.arranger = value;
            }

            /**
             * Obtiene el valor de la propiedad payment.
             * 
             * @return
             *     possible object is
             *     {@link PaymentFormType }
             *     
             */
            public PaymentFormType getPayment() {
                return payment;
            }

            /**
             * Define el valor de la propiedad payment.
             * 
             * @param value
             *     allowed object is
             *     {@link PaymentFormType }
             *     
             */
            public void setPayment(PaymentFormType value) {
                this.payment = value;
            }

            /**
             * Gets the value of the ticketing property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ticketing property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTicketing().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link TicketingInfoType }
             * 
             * 
             */
            public List<TicketingInfoType> getTicketing() {
                if (ticketing == null) {
                    ticketing = new ArrayList<TicketingInfoType>();
                }
                return this.ticketing;
            }

            /**
             * Obtiene el valor de la propiedad pricing.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing }
             *     
             */
            public OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing getPricing() {
                return pricing;
            }

            /**
             * Define el valor de la propiedad pricing.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing }
             *     
             */
            public void setPricing(OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing value) {
                this.pricing = value;
            }

            /**
             * Obtiene el valor de la propiedad purchasedOffers.
             * 
             * @return
             *     possible object is
             *     {@link AirPurchasedOfferType }
             *     
             */
            public AirPurchasedOfferType getPurchasedOffers() {
                return purchasedOffers;
            }

            /**
             * Define el valor de la propiedad purchasedOffers.
             * 
             * @param value
             *     allowed object is
             *     {@link AirPurchasedOfferType }
             *     
             */
            public void setPurchasedOffers(AirPurchasedOfferType value) {
                this.purchasedOffers = value;
            }

            /**
             * Gets the value of the bookingReferenceID property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingReferenceID property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBookingReferenceID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link UniqueIDType }
             * 
             * 
             */
            public List<UniqueIDType> getBookingReferenceID() {
                if (bookingReferenceID == null) {
                    bookingReferenceID = new ArrayList<UniqueIDType>();
                }
                return this.bookingReferenceID;
            }

            /**
             * Obtiene el valor de la propiedad lastModified.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastModified() {
                return lastModified;
            }

            /**
             * Define el valor de la propiedad lastModified.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastModified(String value) {
                this.lastModified = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PricingIndicator" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PricingInfoGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Account" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="StatisticalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="ValidatingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="PriceType" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="NUC_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *       &amp;lt;attribute name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pricingIndicator",
                "account",
                "comment"
            })
            public static class Pricing {

                @XmlElement(name = "PricingIndicator")
                protected List<OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.PricingIndicator> pricingIndicator;
                @XmlElement(name = "Account")
                protected List<OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.Account> account;
                @XmlElement(name = "Comment")
                protected List<FreeTextType> comment;
                @XmlAttribute(name = "StatisticalCode")
                protected String statisticalCode;
                @XmlAttribute(name = "ValidatingAirlineCode")
                protected String validatingAirlineCode;
                @XmlAttribute(name = "DepartureDate")
                protected String departureDate;
                @XmlAttribute(name = "PriceType")
                protected String priceType;
                @XmlAttribute(name = "NUC_Rate")
                protected BigDecimal nucRate;
                @XmlAttribute(name = "ExchangeRate")
                protected BigDecimal exchangeRate;

                /**
                 * Gets the value of the pricingIndicator property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pricingIndicator property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getPricingIndicator().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.PricingIndicator }
                 * 
                 * 
                 */
                public List<OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.PricingIndicator> getPricingIndicator() {
                    if (pricingIndicator == null) {
                        pricingIndicator = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.PricingIndicator>();
                    }
                    return this.pricingIndicator;
                }

                /**
                 * Gets the value of the account property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the account property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getAccount().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.Account }
                 * 
                 * 
                 */
                public List<OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.Account> getAccount() {
                    if (account == null) {
                        account = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.ConfirmedBookingInfo.Pricing.Account>();
                    }
                    return this.account;
                }

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link FreeTextType }
                 * 
                 * 
                 */
                public List<FreeTextType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<FreeTextType>();
                    }
                    return this.comment;
                }

                /**
                 * Obtiene el valor de la propiedad statisticalCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStatisticalCode() {
                    return statisticalCode;
                }

                /**
                 * Define el valor de la propiedad statisticalCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStatisticalCode(String value) {
                    this.statisticalCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad validatingAirlineCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValidatingAirlineCode() {
                    return validatingAirlineCode;
                }

                /**
                 * Define el valor de la propiedad validatingAirlineCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValidatingAirlineCode(String value) {
                    this.validatingAirlineCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad departureDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDepartureDate() {
                    return departureDate;
                }

                /**
                 * Define el valor de la propiedad departureDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDepartureDate(String value) {
                    this.departureDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad priceType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPriceType() {
                    return priceType;
                }

                /**
                 * Define el valor de la propiedad priceType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPriceType(String value) {
                    this.priceType = value;
                }

                /**
                 * Obtiene el valor de la propiedad nucRate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getNUCRate() {
                    return nucRate;
                }

                /**
                 * Define el valor de la propiedad nucRate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setNUCRate(BigDecimal value) {
                    this.nucRate = value;
                }

                /**
                 * Obtiene el valor de la propiedad exchangeRate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getExchangeRate() {
                    return exchangeRate;
                }

                /**
                 * Define el valor de la propiedad exchangeRate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setExchangeRate(BigDecimal value) {
                    this.exchangeRate = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Account {

                    @XmlAttribute(name = "Code", required = true)
                    protected String code;

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PricingInfoGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class PricingIndicator {

                    @XmlAttribute(name = "Type", required = true)
                    protected String type;
                    @XmlAttribute(name = "ExcludeInd")
                    protected Boolean excludeInd;
                    @XmlAttribute(name = "Qualifier")
                    protected String qualifier;

                    /**
                     * Obtiene el valor de la propiedad type.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Define el valor de la propiedad type.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad excludeInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isExcludeInd() {
                        return excludeInd;
                    }

                    /**
                     * Define el valor de la propiedad excludeInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setExcludeInd(Boolean value) {
                        this.excludeInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad qualifier.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getQualifier() {
                        return qualifier;
                    }

                    /**
                     * Define el valor de la propiedad qualifier.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setQualifier(String value) {
                        this.qualifier = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "alternateLocationInfo",
            "tpaExtensions"
        })
        public static class OriginDestination
            extends OriginDestinationInformationType
        {

            @XmlElement(name = "AlternateLocationInfo")
            protected OTAAirGetOfferRQ.RequestCriterion.OriginDestination.AlternateLocationInfo alternateLocationInfo;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;
            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Obtiene el valor de la propiedad alternateLocationInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirGetOfferRQ.RequestCriterion.OriginDestination.AlternateLocationInfo }
             *     
             */
            public OTAAirGetOfferRQ.RequestCriterion.OriginDestination.AlternateLocationInfo getAlternateLocationInfo() {
                return alternateLocationInfo;
            }

            /**
             * Define el valor de la propiedad alternateLocationInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirGetOfferRQ.RequestCriterion.OriginDestination.AlternateLocationInfo }
             *     
             */
            public void setAlternateLocationInfo(OTAAirGetOfferRQ.RequestCriterion.OriginDestination.AlternateLocationInfo value) {
                this.alternateLocationInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AlternateLocationInfo {

                @XmlAttribute(name = "OriginLocation")
                protected List<String> originLocation;
                @XmlAttribute(name = "DestinationLocation")
                protected List<String> destinationLocation;

                /**
                 * Gets the value of the originLocation property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originLocation property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOriginLocation().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getOriginLocation() {
                    if (originLocation == null) {
                        originLocation = new ArrayList<String>();
                    }
                    return this.originLocation;
                }

                /**
                 * Gets the value of the destinationLocation property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the destinationLocation property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getDestinationLocation().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getDestinationLocation() {
                    if (destinationLocation == null) {
                        destinationLocation = new ArrayList<String>();
                    }
                    return this.destinationLocation;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="LandProductInfo" type="{http://www.opentravel.org/OTA/2003/05}AirLandProductType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="PurchasedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "landProductInfo"
        })
        public static class PresentedPurchased
            extends AncillaryServiceDetailType
        {

            @XmlElement(name = "LandProductInfo")
            protected AirLandProductType landProductInfo;
            @XmlAttribute(name = "PurchasedInd")
            protected Boolean purchasedInd;

            /**
             * Obtiene el valor de la propiedad landProductInfo.
             * 
             * @return
             *     possible object is
             *     {@link AirLandProductType }
             *     
             */
            public AirLandProductType getLandProductInfo() {
                return landProductInfo;
            }

            /**
             * Define el valor de la propiedad landProductInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link AirLandProductType }
             *     
             */
            public void setLandProductInfo(AirLandProductType value) {
                this.landProductInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad purchasedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPurchasedInd() {
                return purchasedInd;
            }

            /**
             * Define el valor de la propiedad purchasedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPurchasedInd(Boolean value) {
                this.purchasedInd = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="DetailedInfo" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSeatMarketingClassType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="TotalReqSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="ChangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "detailedInfo"
        })
        public static class SeatInfo {

            @XmlElement(name = "DetailedInfo")
            protected List<OTAAirGetOfferRQ.RequestCriterion.SeatInfo.DetailedInfo> detailedInfo;
            @XmlAttribute(name = "TotalReqSeatQty")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger totalReqSeatQty;
            @XmlAttribute(name = "ChangeInd")
            protected Boolean changeInd;

            /**
             * Gets the value of the detailedInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detailedInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDetailedInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirGetOfferRQ.RequestCriterion.SeatInfo.DetailedInfo }
             * 
             * 
             */
            public List<OTAAirGetOfferRQ.RequestCriterion.SeatInfo.DetailedInfo> getDetailedInfo() {
                if (detailedInfo == null) {
                    detailedInfo = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.SeatInfo.DetailedInfo>();
                }
                return this.detailedInfo;
            }

            /**
             * Obtiene el valor de la propiedad totalReqSeatQty.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTotalReqSeatQty() {
                return totalReqSeatQty;
            }

            /**
             * Define el valor de la propiedad totalReqSeatQty.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTotalReqSeatQty(BigInteger value) {
                this.totalReqSeatQty = value;
            }

            /**
             * Obtiene el valor de la propiedad changeInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isChangeInd() {
                return changeInd;
            }

            /**
             * Define el valor de la propiedad changeInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setChangeInd(Boolean value) {
                this.changeInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSeatMarketingClassType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DetailedInfo
                extends AirSeatMarketingClassType
            {


            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType"&amp;gt;
         *       &amp;lt;attribute name="FlexDatePref"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="Outbound"/&amp;gt;
         *             &amp;lt;enumeration value="Return"/&amp;gt;
         *             &amp;lt;enumeration value="Both"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="FlexWeekendInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FlexLevelInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="NoFareBreakInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="OriginDestinationRPHs" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class TravelPreferences
            extends AirSearchPrefsType
        {

            @XmlAttribute(name = "FlexDatePref")
            protected String flexDatePref;
            @XmlAttribute(name = "FlexWeekendInd")
            protected Boolean flexWeekendInd;
            @XmlAttribute(name = "FlexLevelInd")
            protected Boolean flexLevelInd;
            @XmlAttribute(name = "NoFareBreakInd")
            protected Boolean noFareBreakInd;
            @XmlAttribute(name = "OriginDestinationRPHs")
            protected List<String> originDestinationRPHs;

            /**
             * Obtiene el valor de la propiedad flexDatePref.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlexDatePref() {
                return flexDatePref;
            }

            /**
             * Define el valor de la propiedad flexDatePref.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlexDatePref(String value) {
                this.flexDatePref = value;
            }

            /**
             * Obtiene el valor de la propiedad flexWeekendInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFlexWeekendInd() {
                return flexWeekendInd;
            }

            /**
             * Define el valor de la propiedad flexWeekendInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFlexWeekendInd(Boolean value) {
                this.flexWeekendInd = value;
            }

            /**
             * Obtiene el valor de la propiedad flexLevelInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFlexLevelInd() {
                return flexLevelInd;
            }

            /**
             * Define el valor de la propiedad flexLevelInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFlexLevelInd(Boolean value) {
                this.flexLevelInd = value;
            }

            /**
             * Obtiene el valor de la propiedad noFareBreakInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isNoFareBreakInd() {
                return noFareBreakInd;
            }

            /**
             * Define el valor de la propiedad noFareBreakInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setNoFareBreakInd(Boolean value) {
                this.noFareBreakInd = value;
            }

            /**
             * Gets the value of the originDestinationRPHs property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationRPHs property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getOriginDestinationRPHs().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getOriginDestinationRPHs() {
                if (originDestinationRPHs == null) {
                    originDestinationRPHs = new ArrayList<String>();
                }
                return this.originDestinationRPHs;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;choice&amp;gt;
         *         &amp;lt;element name="PassengerTypeQuantity" maxOccurs="10"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AirTraveler" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirTravelerType"&amp;gt;
         *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="IsLeadInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/choice&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "passengerTypeQuantity",
            "airTraveler"
        })
        public static class Traveler {

            @XmlElement(name = "PassengerTypeQuantity")
            protected List<OTAAirGetOfferRQ.RequestCriterion.Traveler.PassengerTypeQuantity> passengerTypeQuantity;
            @XmlElement(name = "AirTraveler")
            protected List<OTAAirGetOfferRQ.RequestCriterion.Traveler.AirTraveler> airTraveler;

            /**
             * Gets the value of the passengerTypeQuantity property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerTypeQuantity property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPassengerTypeQuantity().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirGetOfferRQ.RequestCriterion.Traveler.PassengerTypeQuantity }
             * 
             * 
             */
            public List<OTAAirGetOfferRQ.RequestCriterion.Traveler.PassengerTypeQuantity> getPassengerTypeQuantity() {
                if (passengerTypeQuantity == null) {
                    passengerTypeQuantity = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.Traveler.PassengerTypeQuantity>();
                }
                return this.passengerTypeQuantity;
            }

            /**
             * Gets the value of the airTraveler property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airTraveler property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAirTraveler().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirGetOfferRQ.RequestCriterion.Traveler.AirTraveler }
             * 
             * 
             */
            public List<OTAAirGetOfferRQ.RequestCriterion.Traveler.AirTraveler> getAirTraveler() {
                if (airTraveler == null) {
                    airTraveler = new ArrayList<OTAAirGetOfferRQ.RequestCriterion.Traveler.AirTraveler>();
                }
                return this.airTraveler;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirTravelerType"&amp;gt;
             *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="IsLeadInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AirTraveler
                extends AirTravelerType
            {

                @XmlAttribute(name = "RPH")
                protected String rph;
                @XmlAttribute(name = "IsLeadInd")
                protected Boolean isLeadInd;

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRPH(String value) {
                    this.rph = value;
                }

                /**
                 * Obtiene el valor de la propiedad isLeadInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIsLeadInd() {
                    return isLeadInd;
                }

                /**
                 * Define el valor de la propiedad isLeadInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIsLeadInd(Boolean value) {
                    this.isLeadInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class PassengerTypeQuantity
                extends PassengerTypeQuantityType
            {


            }

        }

    }

}
