
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MultimodalOffer" type="{http://www.opentravel.org/OTA/2003/05}MultiModalOfferType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PackageRequest"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="DateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Routing" maxOccurs="2" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Departure" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                             &amp;lt;element name="Arrival" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_OptionalGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
 *                 &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="ShortDescription" type="{http://www.opentravel.org/OTA/2003/05}ShortDescriptionType" /&amp;gt;
 *                 &amp;lt;attribute name="BoardCode" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
 *                 &amp;lt;attribute name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="BrandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="FreeChildQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                 &amp;lt;attribute name="CabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CustomerCounts" type="{http://www.opentravel.org/OTA/2003/05}CustomerCountsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReqRespVersion"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MaxResponsesGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "multimodalOffer",
    "packageRequest",
    "customerCounts",
    "url",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_PkgAvailRQ")
public class OTAPkgAvailRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "MultimodalOffer")
    protected MultiModalOfferType multimodalOffer;
    @XmlElement(name = "PackageRequest", required = true)
    protected OTAPkgAvailRQ.PackageRequest packageRequest;
    @XmlElement(name = "CustomerCounts")
    protected CustomerCountsType customerCounts;
    @XmlElement(name = "URL")
    protected URLType url;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "ReqRespVersion")
    protected String reqRespVersion;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad multimodalOffer.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType }
     *     
     */
    public MultiModalOfferType getMultimodalOffer() {
        return multimodalOffer;
    }

    /**
     * Define el valor de la propiedad multimodalOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType }
     *     
     */
    public void setMultimodalOffer(MultiModalOfferType value) {
        this.multimodalOffer = value;
    }

    /**
     * Obtiene el valor de la propiedad packageRequest.
     * 
     * @return
     *     possible object is
     *     {@link OTAPkgAvailRQ.PackageRequest }
     *     
     */
    public OTAPkgAvailRQ.PackageRequest getPackageRequest() {
        return packageRequest;
    }

    /**
     * Define el valor de la propiedad packageRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAPkgAvailRQ.PackageRequest }
     *     
     */
    public void setPackageRequest(OTAPkgAvailRQ.PackageRequest value) {
        this.packageRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad customerCounts.
     * 
     * @return
     *     possible object is
     *     {@link CustomerCountsType }
     *     
     */
    public CustomerCountsType getCustomerCounts() {
        return customerCounts;
    }

    /**
     * Define el valor de la propiedad customerCounts.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerCountsType }
     *     
     */
    public void setCustomerCounts(CustomerCountsType value) {
        this.customerCounts = value;
    }

    /**
     * Obtiene el valor de la propiedad url.
     * 
     * @return
     *     possible object is
     *     {@link URLType }
     *     
     */
    public URLType getURL() {
        return url;
    }

    /**
     * Define el valor de la propiedad url.
     * 
     * @param value
     *     allowed object is
     *     {@link URLType }
     *     
     */
    public void setURL(URLType value) {
        this.url = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad reqRespVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqRespVersion() {
        return reqRespVersion;
    }

    /**
     * Define el valor de la propiedad reqRespVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqRespVersion(String value) {
        this.reqRespVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="DateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Routing" maxOccurs="2" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Departure" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *                   &amp;lt;element name="Arrival" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_OptionalGroup"/&amp;gt;
     *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
     *       &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ShortDescription" type="{http://www.opentravel.org/OTA/2003/05}ShortDescriptionType" /&amp;gt;
     *       &amp;lt;attribute name="BoardCode" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
     *       &amp;lt;attribute name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="BrandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="FreeChildQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *       &amp;lt;attribute name="CabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dateRange",
        "routing"
    })
    public static class PackageRequest {

        @XmlElement(name = "DateRange")
        protected DateTimeSpanType dateRange;
        @XmlElement(name = "Routing")
        protected List<OTAPkgAvailRQ.PackageRequest.Routing> routing;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "TravelCode")
        protected String travelCode;
        @XmlAttribute(name = "TourCode")
        protected String tourCode;
        @XmlAttribute(name = "ShortDescription")
        protected String shortDescription;
        @XmlAttribute(name = "BoardCode")
        protected String boardCode;
        @XmlAttribute(name = "PromotionCode")
        protected String promotionCode;
        @XmlAttribute(name = "BrandCode")
        protected String brandCode;
        @XmlAttribute(name = "ProductCode")
        protected String productCode;
        @XmlAttribute(name = "FreeChildQuantity")
        protected Integer freeChildQuantity;
        @XmlAttribute(name = "CabinType")
        protected String cabinType;
        @XmlAttribute(name = "ID")
        protected String id;

        /**
         * Obtiene el valor de la propiedad dateRange.
         * 
         * @return
         *     possible object is
         *     {@link DateTimeSpanType }
         *     
         */
        public DateTimeSpanType getDateRange() {
            return dateRange;
        }

        /**
         * Define el valor de la propiedad dateRange.
         * 
         * @param value
         *     allowed object is
         *     {@link DateTimeSpanType }
         *     
         */
        public void setDateRange(DateTimeSpanType value) {
            this.dateRange = value;
        }

        /**
         * Gets the value of the routing property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the routing property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRouting().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAPkgAvailRQ.PackageRequest.Routing }
         * 
         * 
         */
        public List<OTAPkgAvailRQ.PackageRequest.Routing> getRouting() {
            if (routing == null) {
                routing = new ArrayList<OTAPkgAvailRQ.PackageRequest.Routing>();
            }
            return this.routing;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad travelCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelCode() {
            return travelCode;
        }

        /**
         * Define el valor de la propiedad travelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelCode(String value) {
            this.travelCode = value;
        }

        /**
         * Obtiene el valor de la propiedad tourCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTourCode() {
            return tourCode;
        }

        /**
         * Define el valor de la propiedad tourCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTourCode(String value) {
            this.tourCode = value;
        }

        /**
         * Obtiene el valor de la propiedad shortDescription.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShortDescription() {
            return shortDescription;
        }

        /**
         * Define el valor de la propiedad shortDescription.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShortDescription(String value) {
            this.shortDescription = value;
        }

        /**
         * Obtiene el valor de la propiedad boardCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBoardCode() {
            return boardCode;
        }

        /**
         * Define el valor de la propiedad boardCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBoardCode(String value) {
            this.boardCode = value;
        }

        /**
         * Obtiene el valor de la propiedad promotionCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPromotionCode() {
            return promotionCode;
        }

        /**
         * Define el valor de la propiedad promotionCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPromotionCode(String value) {
            this.promotionCode = value;
        }

        /**
         * Obtiene el valor de la propiedad brandCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandCode() {
            return brandCode;
        }

        /**
         * Define el valor de la propiedad brandCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandCode(String value) {
            this.brandCode = value;
        }

        /**
         * Obtiene el valor de la propiedad productCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductCode() {
            return productCode;
        }

        /**
         * Define el valor de la propiedad productCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductCode(String value) {
            this.productCode = value;
        }

        /**
         * Obtiene el valor de la propiedad freeChildQuantity.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getFreeChildQuantity() {
            return freeChildQuantity;
        }

        /**
         * Define el valor de la propiedad freeChildQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setFreeChildQuantity(Integer value) {
            this.freeChildQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad cabinType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCabinType() {
            return cabinType;
        }

        /**
         * Define el valor de la propiedad cabinType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCabinType(String value) {
            this.cabinType = value;
        }

        /**
         * Obtiene el valor de la propiedad id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getID() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setID(String value) {
            this.id = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Departure" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
         *         &amp;lt;element name="Arrival" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "departure",
            "arrival"
        })
        public static class Routing {

            @XmlElement(name = "Departure", required = true)
            protected LocationType departure;
            @XmlElement(name = "Arrival", required = true)
            protected LocationType arrival;
            @XmlAttribute(name = "Direction")
            protected AirTripType direction;

            /**
             * Obtiene el valor de la propiedad departure.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDeparture() {
                return departure;
            }

            /**
             * Define el valor de la propiedad departure.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDeparture(LocationType value) {
                this.departure = value;
            }

            /**
             * Obtiene el valor de la propiedad arrival.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getArrival() {
                return arrival;
            }

            /**
             * Define el valor de la propiedad arrival.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setArrival(LocationType value) {
                this.arrival = value;
            }

            /**
             * Obtiene el valor de la propiedad direction.
             * 
             * @return
             *     possible object is
             *     {@link AirTripType }
             *     
             */
            public AirTripType getDirection() {
                return direction;
            }

            /**
             * Define el valor de la propiedad direction.
             * 
             * @param value
             *     allowed object is
             *     {@link AirTripType }
             *     
             */
            public void setDirection(AirTripType value) {
                this.direction = value;
            }

        }

    }

}
