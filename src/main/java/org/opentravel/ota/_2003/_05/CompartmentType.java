
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * A compartment type that supports open enumerations.
 * 
 * &lt;p&gt;Clase Java para CompartmentType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CompartmentType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompartmentTypeEnum"&amp;gt;
 *       &amp;lt;attribute name="extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompartmentType", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.AccommodationType.Compartment.class,
    org.opentravel.ota._2003._05.RailAccommDetailType.Compartment.class
})
public class CompartmentType {

    @XmlValue
    protected CompartmentTypeEnum value;
    @XmlAttribute(name = "extension")
    protected String extension;

    /**
     * Rail compartment types with an "Other" value to support an open enumeration list as agreed upon between trading partners.
     * 
     * @return
     *     possible object is
     *     {@link CompartmentTypeEnum }
     *     
     */
    public CompartmentTypeEnum getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link CompartmentTypeEnum }
     *     
     */
    public void setValue(CompartmentTypeEnum value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

}
