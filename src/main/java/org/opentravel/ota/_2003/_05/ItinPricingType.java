
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Pricing information for the travel itinerary.
 * 
 * &lt;p&gt;Clase Java para ItinPricingType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ItinPricingType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Cost"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TotalType"&amp;gt;
 *                 &amp;lt;attribute name="FormOfPaymentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="ItemRPH_List" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *       &amp;lt;attribute name="AssociatedItemRPH"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;list itemType="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItinPricingType", propOrder = {
    "cost",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.ReservationItemsType.ItemPricing.class
})
public class ItinPricingType {

    @XmlElement(name = "Cost", required = true)
    protected ItinPricingType.Cost cost;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "ItemRPH_List")
    protected List<String> itemRPHList;
    @XmlAttribute(name = "AssociatedItemRPH")
    protected List<String> associatedItemRPH;

    /**
     * Obtiene el valor de la propiedad cost.
     * 
     * @return
     *     possible object is
     *     {@link ItinPricingType.Cost }
     *     
     */
    public ItinPricingType.Cost getCost() {
        return cost;
    }

    /**
     * Define el valor de la propiedad cost.
     * 
     * @param value
     *     allowed object is
     *     {@link ItinPricingType.Cost }
     *     
     */
    public void setCost(ItinPricingType.Cost value) {
        this.cost = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Gets the value of the itemRPHList property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the itemRPHList property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getItemRPHList().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getItemRPHList() {
        if (itemRPHList == null) {
            itemRPHList = new ArrayList<String>();
        }
        return this.itemRPHList;
    }

    /**
     * Gets the value of the associatedItemRPH property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the associatedItemRPH property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAssociatedItemRPH().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAssociatedItemRPH() {
        if (associatedItemRPH == null) {
            associatedItemRPH = new ArrayList<String>();
        }
        return this.associatedItemRPH;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TotalType"&amp;gt;
     *       &amp;lt;attribute name="FormOfPaymentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Cost
        extends TotalType
    {

        @XmlAttribute(name = "FormOfPaymentRPH")
        protected String formOfPaymentRPH;

        /**
         * Obtiene el valor de la propiedad formOfPaymentRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFormOfPaymentRPH() {
            return formOfPaymentRPH;
        }

        /**
         * Define el valor de la propiedad formOfPaymentRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFormOfPaymentRPH(String value) {
            this.formOfPaymentRPH = value;
        }

    }

}
