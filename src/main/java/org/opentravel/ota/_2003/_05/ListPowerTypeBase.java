
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_PowerType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_PowerType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Diesel"/&amp;gt;
 *     &amp;lt;enumeration value="Electric"/&amp;gt;
 *     &amp;lt;enumeration value="Gas"/&amp;gt;
 *     &amp;lt;enumeration value="Oil"/&amp;gt;
 *     &amp;lt;enumeration value="Solar"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_PowerType_Base")
@XmlEnum
public enum ListPowerTypeBase {

    @XmlEnumValue("Diesel")
    DIESEL("Diesel"),
    @XmlEnumValue("Electric")
    ELECTRIC("Electric"),
    @XmlEnumValue("Gas")
    GAS("Gas"),
    @XmlEnumValue("Oil")
    OIL("Oil"),
    @XmlEnumValue("Solar")
    SOLAR("Solar"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListPowerTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListPowerTypeBase fromValue(String v) {
        for (ListPowerTypeBase c: ListPowerTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
