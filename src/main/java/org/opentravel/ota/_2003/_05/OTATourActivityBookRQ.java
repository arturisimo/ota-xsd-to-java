
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ContactDetail" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BookingInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="BasicInfo"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ParticipantInfo" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Category" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                     &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Group" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                               &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                     &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                     &amp;lt;attribute name="Gender"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Male"/&amp;gt;
 *                                           &amp;lt;enumeration value="Female"/&amp;gt;
 *                                           &amp;lt;enumeration value="Unknown"/&amp;gt;
 *                                           &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
 *                                           &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                                     &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PaymentInfo" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Schedule"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AccessibilityRequirement" maxOccurs="999" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityReqType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                           &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Insurance" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                           &amp;lt;attribute name="PlanID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="NegotiatedRate" type="{http://www.opentravel.org/OTA/2003/05}TourActivityNegotiatedPricing" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="PickupDropoff" maxOccurs="2" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityTransRequestType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ParticipantList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                               &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "contactDetail",
    "bookingInfo",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_TourActivityBookRQ")
public class OTATourActivityBookRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "ContactDetail")
    protected ContactPersonType contactDetail;
    @XmlElement(name = "BookingInfo", required = true)
    protected OTATourActivityBookRQ.BookingInfo bookingInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad contactDetail.
     * 
     * @return
     *     possible object is
     *     {@link ContactPersonType }
     *     
     */
    public ContactPersonType getContactDetail() {
        return contactDetail;
    }

    /**
     * Define el valor de la propiedad contactDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPersonType }
     *     
     */
    public void setContactDetail(ContactPersonType value) {
        this.contactDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityBookRQ.BookingInfo }
     *     
     */
    public OTATourActivityBookRQ.BookingInfo getBookingInfo() {
        return bookingInfo;
    }

    /**
     * Define el valor de la propiedad bookingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityBookRQ.BookingInfo }
     *     
     */
    public void setBookingInfo(OTATourActivityBookRQ.BookingInfo value) {
        this.bookingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BasicInfo"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ParticipantInfo" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Category" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Group" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                           &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                           &amp;lt;attribute name="Gender"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Male"/&amp;gt;
     *                                 &amp;lt;enumeration value="Female"/&amp;gt;
     *                                 &amp;lt;enumeration value="Unknown"/&amp;gt;
     *                                 &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
     *                                 &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *                           &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PaymentInfo" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Schedule"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AccessibilityRequirement" maxOccurs="999" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityReqType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                 &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Insurance" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                 &amp;lt;attribute name="PlanID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="NegotiatedRate" type="{http://www.opentravel.org/OTA/2003/05}TourActivityNegotiatedPricing" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PickupDropoff" maxOccurs="2" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityTransRequestType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ParticipantList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                                     &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "basicInfo",
        "participantInfo",
        "paymentInfo",
        "schedule",
        "accessibilityRequirement",
        "discount",
        "extra",
        "insurance",
        "location",
        "negotiatedRate",
        "pickupDropoff",
        "pricing",
        "supplierOperator",
        "tpaExtensions"
    })
    public static class BookingInfo {

        @XmlElement(name = "BasicInfo", required = true)
        protected OTATourActivityBookRQ.BookingInfo.BasicInfo basicInfo;
        @XmlElement(name = "ParticipantInfo", required = true)
        protected List<OTATourActivityBookRQ.BookingInfo.ParticipantInfo> participantInfo;
        @XmlElement(name = "PaymentInfo", required = true)
        protected List<OTATourActivityBookRQ.BookingInfo.PaymentInfo> paymentInfo;
        @XmlElement(name = "Schedule", required = true)
        protected OTATourActivityBookRQ.BookingInfo.Schedule schedule;
        @XmlElement(name = "AccessibilityRequirement")
        protected List<OTATourActivityBookRQ.BookingInfo.AccessibilityRequirement> accessibilityRequirement;
        @XmlElement(name = "Discount")
        protected List<OTATourActivityBookRQ.BookingInfo.Discount> discount;
        @XmlElement(name = "Extra")
        protected List<OTATourActivityBookRQ.BookingInfo.Extra> extra;
        @XmlElement(name = "Insurance")
        protected List<OTATourActivityBookRQ.BookingInfo.Insurance> insurance;
        @XmlElement(name = "Location")
        protected TourActivityLocationType location;
        @XmlElement(name = "NegotiatedRate")
        protected List<TourActivityNegotiatedPricing> negotiatedRate;
        @XmlElement(name = "PickupDropoff")
        protected List<OTATourActivityBookRQ.BookingInfo.PickupDropoff> pickupDropoff;
        @XmlElement(name = "Pricing")
        protected OTATourActivityBookRQ.BookingInfo.Pricing pricing;
        @XmlElement(name = "SupplierOperator")
        protected List<TourActivitySupplierType> supplierOperator;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;

        /**
         * Obtiene el valor de la propiedad basicInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityBookRQ.BookingInfo.BasicInfo }
         *     
         */
        public OTATourActivityBookRQ.BookingInfo.BasicInfo getBasicInfo() {
            return basicInfo;
        }

        /**
         * Define el valor de la propiedad basicInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityBookRQ.BookingInfo.BasicInfo }
         *     
         */
        public void setBasicInfo(OTATourActivityBookRQ.BookingInfo.BasicInfo value) {
            this.basicInfo = value;
        }

        /**
         * Gets the value of the participantInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getParticipantInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.ParticipantInfo> getParticipantInfo() {
            if (participantInfo == null) {
                participantInfo = new ArrayList<OTATourActivityBookRQ.BookingInfo.ParticipantInfo>();
            }
            return this.participantInfo;
        }

        /**
         * Gets the value of the paymentInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPaymentInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.PaymentInfo }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.PaymentInfo> getPaymentInfo() {
            if (paymentInfo == null) {
                paymentInfo = new ArrayList<OTATourActivityBookRQ.BookingInfo.PaymentInfo>();
            }
            return this.paymentInfo;
        }

        /**
         * Obtiene el valor de la propiedad schedule.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityBookRQ.BookingInfo.Schedule }
         *     
         */
        public OTATourActivityBookRQ.BookingInfo.Schedule getSchedule() {
            return schedule;
        }

        /**
         * Define el valor de la propiedad schedule.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityBookRQ.BookingInfo.Schedule }
         *     
         */
        public void setSchedule(OTATourActivityBookRQ.BookingInfo.Schedule value) {
            this.schedule = value;
        }

        /**
         * Gets the value of the accessibilityRequirement property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the accessibilityRequirement property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAccessibilityRequirement().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.AccessibilityRequirement }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.AccessibilityRequirement> getAccessibilityRequirement() {
            if (accessibilityRequirement == null) {
                accessibilityRequirement = new ArrayList<OTATourActivityBookRQ.BookingInfo.AccessibilityRequirement>();
            }
            return this.accessibilityRequirement;
        }

        /**
         * Gets the value of the discount property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discount property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDiscount().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.Discount }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.Discount> getDiscount() {
            if (discount == null) {
                discount = new ArrayList<OTATourActivityBookRQ.BookingInfo.Discount>();
            }
            return this.discount;
        }

        /**
         * Gets the value of the extra property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the extra property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getExtra().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.Extra }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.Extra> getExtra() {
            if (extra == null) {
                extra = new ArrayList<OTATourActivityBookRQ.BookingInfo.Extra>();
            }
            return this.extra;
        }

        /**
         * Gets the value of the insurance property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the insurance property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getInsurance().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.Insurance }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.Insurance> getInsurance() {
            if (insurance == null) {
                insurance = new ArrayList<OTATourActivityBookRQ.BookingInfo.Insurance>();
            }
            return this.insurance;
        }

        /**
         * Obtiene el valor de la propiedad location.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityLocationType }
         *     
         */
        public TourActivityLocationType getLocation() {
            return location;
        }

        /**
         * Define el valor de la propiedad location.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityLocationType }
         *     
         */
        public void setLocation(TourActivityLocationType value) {
            this.location = value;
        }

        /**
         * Gets the value of the negotiatedRate property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the negotiatedRate property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getNegotiatedRate().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityNegotiatedPricing }
         * 
         * 
         */
        public List<TourActivityNegotiatedPricing> getNegotiatedRate() {
            if (negotiatedRate == null) {
                negotiatedRate = new ArrayList<TourActivityNegotiatedPricing>();
            }
            return this.negotiatedRate;
        }

        /**
         * Gets the value of the pickupDropoff property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pickupDropoff property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPickupDropoff().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityBookRQ.BookingInfo.PickupDropoff }
         * 
         * 
         */
        public List<OTATourActivityBookRQ.BookingInfo.PickupDropoff> getPickupDropoff() {
            if (pickupDropoff == null) {
                pickupDropoff = new ArrayList<OTATourActivityBookRQ.BookingInfo.PickupDropoff>();
            }
            return this.pickupDropoff;
        }

        /**
         * Obtiene el valor de la propiedad pricing.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityBookRQ.BookingInfo.Pricing }
         *     
         */
        public OTATourActivityBookRQ.BookingInfo.Pricing getPricing() {
            return pricing;
        }

        /**
         * Define el valor de la propiedad pricing.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityBookRQ.BookingInfo.Pricing }
         *     
         */
        public void setPricing(OTATourActivityBookRQ.BookingInfo.Pricing value) {
            this.pricing = value;
        }

        /**
         * Gets the value of the supplierOperator property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplierOperator property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSupplierOperator().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivitySupplierType }
         * 
         * 
         */
        public List<TourActivitySupplierType> getSupplierOperator() {
            if (supplierOperator == null) {
                supplierOperator = new ArrayList<TourActivitySupplierType>();
            }
            return this.supplierOperator;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityReqType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AccessibilityRequirement
            extends TourActivityAccessibilityReqType
        {

            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class BasicInfo
            extends TourActivityIDType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionCode",
            "participationInfo"
        })
        public static class Discount {

            @XmlElement(name = "PromotionCode")
            protected List<String> promotionCode;
            @XmlElement(name = "ParticipationInfo")
            protected OTATourActivityBookRQ.BookingInfo.Discount.ParticipationInfo participationInfo;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Gets the value of the promotionCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotionCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPromotionCode() {
                if (promotionCode == null) {
                    promotionCode = new ArrayList<String>();
                }
                return this.promotionCode;
            }

            /**
             * Obtiene el valor de la propiedad participationInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.Discount.ParticipationInfo }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.Discount.ParticipationInfo getParticipationInfo() {
                return participationInfo;
            }

            /**
             * Define el valor de la propiedad participationInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.Discount.ParticipationInfo }
             *     
             */
            public void setParticipationInfo(OTATourActivityBookRQ.BookingInfo.Discount.ParticipationInfo value) {
                this.participationInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ParticipationInfo {

                @XmlAttribute(name = "ValidFromDate")
                protected String validFromDate;
                @XmlAttribute(name = "ValidThroughDate")
                protected String validThroughDate;
                @XmlAttribute(name = "ProgramNumber")
                protected String programNumber;
                @XmlAttribute(name = "NameOnCard")
                protected String nameOnCard;

                /**
                 * Obtiene el valor de la propiedad validFromDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValidFromDate() {
                    return validFromDate;
                }

                /**
                 * Define el valor de la propiedad validFromDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValidFromDate(String value) {
                    this.validFromDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad validThroughDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValidThroughDate() {
                    return validThroughDate;
                }

                /**
                 * Define el valor de la propiedad validThroughDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValidThroughDate(String value) {
                    this.validThroughDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad programNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramNumber() {
                    return programNumber;
                }

                /**
                 * Define el valor de la propiedad programNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramNumber(String value) {
                    this.programNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad nameOnCard.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNameOnCard() {
                    return nameOnCard;
                }

                /**
                 * Define el valor de la propiedad nameOnCard.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNameOnCard(String value) {
                    this.nameOnCard = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *       &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "deposit"
        })
        public static class Extra {

            @XmlElement(name = "Deposit")
            protected OTATourActivityBookRQ.BookingInfo.Extra.Deposit deposit;
            @XmlAttribute(name = "SupplierCode")
            protected String supplierCode;
            @XmlAttribute(name = "OtherCode")
            protected String otherCode;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "AdditionalInfo")
            protected String additionalInfo;
            @XmlAttribute(name = "Quantity")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger quantity;
            @XmlAttribute(name = "ReserveInd")
            protected Boolean reserveInd;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Obtiene el valor de la propiedad deposit.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.Extra.Deposit }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.Extra.Deposit getDeposit() {
                return deposit;
            }

            /**
             * Define el valor de la propiedad deposit.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.Extra.Deposit }
             *     
             */
            public void setDeposit(OTATourActivityBookRQ.BookingInfo.Extra.Deposit value) {
                this.deposit = value;
            }

            /**
             * Obtiene el valor de la propiedad supplierCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSupplierCode() {
                return supplierCode;
            }

            /**
             * Define el valor de la propiedad supplierCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSupplierCode(String value) {
                this.supplierCode = value;
            }

            /**
             * Obtiene el valor de la propiedad otherCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherCode() {
                return otherCode;
            }

            /**
             * Define el valor de la propiedad otherCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherCode(String value) {
                this.otherCode = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad additionalInfo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdditionalInfo() {
                return additionalInfo;
            }

            /**
             * Define el valor de la propiedad additionalInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdditionalInfo(String value) {
                this.additionalInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setQuantity(BigInteger value) {
                this.quantity = value;
            }

            /**
             * Obtiene el valor de la propiedad reserveInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReserveInd() {
                return reserveInd;
            }

            /**
             * Define el valor de la propiedad reserveInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReserveInd(Boolean value) {
                this.reserveInd = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Deposit
                extends PaymentDetailType
            {


            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *       &amp;lt;attribute name="PlanID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "providerName",
            "coverageLimit"
        })
        public static class Insurance {

            @XmlElement(name = "ProviderName")
            protected CompanyNameType providerName;
            @XmlElement(name = "CoverageLimit")
            protected CoverageLimitType coverageLimit;
            @XmlAttribute(name = "PlanID")
            protected String planID;
            @XmlAttribute(name = "PlanName")
            protected String planName;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Obtiene el valor de la propiedad providerName.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getProviderName() {
                return providerName;
            }

            /**
             * Define el valor de la propiedad providerName.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setProviderName(CompanyNameType value) {
                this.providerName = value;
            }

            /**
             * Obtiene el valor de la propiedad coverageLimit.
             * 
             * @return
             *     possible object is
             *     {@link CoverageLimitType }
             *     
             */
            public CoverageLimitType getCoverageLimit() {
                return coverageLimit;
            }

            /**
             * Define el valor de la propiedad coverageLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link CoverageLimitType }
             *     
             */
            public void setCoverageLimit(CoverageLimitType value) {
                this.coverageLimit = value;
            }

            /**
             * Obtiene el valor de la propiedad planID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlanID() {
                return planID;
            }

            /**
             * Define el valor de la propiedad planID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlanID(String value) {
                this.planID = value;
            }

            /**
             * Obtiene el valor de la propiedad planName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlanName() {
                return planName;
            }

            /**
             * Define el valor de la propiedad planName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlanName(String value) {
                this.planName = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Category" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Group" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *                 &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                 &amp;lt;attribute name="Gender"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Male"/&amp;gt;
         *                       &amp;lt;enumeration value="Female"/&amp;gt;
         *                       &amp;lt;enumeration value="Unknown"/&amp;gt;
         *                       &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
         *                       &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
         *                 &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "category",
            "group",
            "individual",
            "lodgingInfo"
        })
        public static class ParticipantInfo {

            @XmlElement(name = "Category")
            protected OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category category;
            @XmlElement(name = "Group")
            protected OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group group;
            @XmlElement(name = "Individual")
            protected OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Individual individual;
            @XmlElement(name = "LodgingInfo")
            protected OTATourActivityBookRQ.BookingInfo.ParticipantInfo.LodgingInfo lodgingInfo;
            @XmlAttribute(name = "FreeChildrenQty")
            protected Integer freeChildrenQty;

            /**
             * Obtiene el valor de la propiedad category.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category getCategory() {
                return category;
            }

            /**
             * Define el valor de la propiedad category.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category }
             *     
             */
            public void setCategory(OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category value) {
                this.category = value;
            }

            /**
             * Obtiene el valor de la propiedad group.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group getGroup() {
                return group;
            }

            /**
             * Define el valor de la propiedad group.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group }
             *     
             */
            public void setGroup(OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group value) {
                this.group = value;
            }

            /**
             * Obtiene el valor de la propiedad individual.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Individual }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Individual getIndividual() {
                return individual;
            }

            /**
             * Define el valor de la propiedad individual.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Individual }
             *     
             */
            public void setIndividual(OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Individual value) {
                this.individual = value;
            }

            /**
             * Obtiene el valor de la propiedad lodgingInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.LodgingInfo }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.ParticipantInfo.LodgingInfo getLodgingInfo() {
                return lodgingInfo;
            }

            /**
             * Define el valor de la propiedad lodgingInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.LodgingInfo }
             *     
             */
            public void setLodgingInfo(OTATourActivityBookRQ.BookingInfo.ParticipantInfo.LodgingInfo value) {
                this.lodgingInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad freeChildrenQty.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getFreeChildrenQty() {
                return freeChildrenQty;
            }

            /**
             * Define el valor de la propiedad freeChildrenQty.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setFreeChildrenQty(Integer value) {
                this.freeChildrenQty = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "qualifierInfo",
                "contact",
                "tpaExtensions"
            })
            public static class Category {

                @XmlElement(name = "QualifierInfo")
                protected OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo qualifierInfo;
                @XmlElement(name = "Contact")
                protected ContactPersonType contact;
                @XmlElement(name = "TPA_Extensions")
                protected TPAExtensionsType tpaExtensions;
                @XmlAttribute(name = "Age")
                protected Integer age;
                @XmlAttribute(name = "Quantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger quantity;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;

                /**
                 * Obtiene el valor de la propiedad qualifierInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo }
                 *     
                 */
                public OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo getQualifierInfo() {
                    return qualifierInfo;
                }

                /**
                 * Define el valor de la propiedad qualifierInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo }
                 *     
                 */
                public void setQualifierInfo(OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Category.QualifierInfo value) {
                    this.qualifierInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad contact.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ContactPersonType }
                 *     
                 */
                public ContactPersonType getContact() {
                    return contact;
                }

                /**
                 * Define el valor de la propiedad contact.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ContactPersonType }
                 *     
                 */
                public void setContact(ContactPersonType value) {
                    this.contact = value;
                }

                /**
                 * Obtiene el valor de la propiedad tpaExtensions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public TPAExtensionsType getTPAExtensions() {
                    return tpaExtensions;
                }

                /**
                 * Define el valor de la propiedad tpaExtensions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public void setTPAExtensions(TPAExtensionsType value) {
                    this.tpaExtensions = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setAge(Integer value) {
                    this.age = value;
                }

                /**
                 * Obtiene el valor de la propiedad quantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getQuantity() {
                    return quantity;
                }

                /**
                 * Define el valor de la propiedad quantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setQuantity(BigInteger value) {
                    this.quantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class QualifierInfo
                    extends AgeQualifierType
                {


                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "contact",
                "participantList"
            })
            public static class Group {

                @XmlElement(name = "Contact")
                protected ContactPersonType contact;
                @XmlElement(name = "ParticipantList")
                protected List<OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group.ParticipantList> participantList;
                @XmlAttribute(name = "GroupCode")
                protected String groupCode;
                @XmlAttribute(name = "GroupName")
                protected String groupName;
                @XmlAttribute(name = "GroupID")
                protected String groupID;
                @XmlAttribute(name = "MinGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger minGroupSize;
                @XmlAttribute(name = "MaxGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maxGroupSize;
                @XmlAttribute(name = "KnownGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger knownGroupSize;

                /**
                 * Obtiene el valor de la propiedad contact.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ContactPersonType }
                 *     
                 */
                public ContactPersonType getContact() {
                    return contact;
                }

                /**
                 * Define el valor de la propiedad contact.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ContactPersonType }
                 *     
                 */
                public void setContact(ContactPersonType value) {
                    this.contact = value;
                }

                /**
                 * Gets the value of the participantList property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantList property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getParticipantList().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group.ParticipantList }
                 * 
                 * 
                 */
                public List<OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group.ParticipantList> getParticipantList() {
                    if (participantList == null) {
                        participantList = new ArrayList<OTATourActivityBookRQ.BookingInfo.ParticipantInfo.Group.ParticipantList>();
                    }
                    return this.participantList;
                }

                /**
                 * Obtiene el valor de la propiedad groupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupCode() {
                    return groupCode;
                }

                /**
                 * Define el valor de la propiedad groupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupCode(String value) {
                    this.groupCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupName() {
                    return groupName;
                }

                /**
                 * Define el valor de la propiedad groupName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupName(String value) {
                    this.groupName = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }

                /**
                 * Obtiene el valor de la propiedad minGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMinGroupSize() {
                    return minGroupSize;
                }

                /**
                 * Define el valor de la propiedad minGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMinGroupSize(BigInteger value) {
                    this.minGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad maxGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaxGroupSize() {
                    return maxGroupSize;
                }

                /**
                 * Define el valor de la propiedad maxGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaxGroupSize(BigInteger value) {
                    this.maxGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad knownGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getKnownGroupSize() {
                    return knownGroupSize;
                }

                /**
                 * Define el valor de la propiedad knownGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setKnownGroupSize(BigInteger value) {
                    this.knownGroupSize = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ParticipantList {

                    @XmlAttribute(name = "ParticipantID")
                    protected String participantID;
                    @XmlAttribute(name = "ParticipantCategoryID")
                    protected String participantCategoryID;

                    /**
                     * Obtiene el valor de la propiedad participantID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantID() {
                        return participantID;
                    }

                    /**
                     * Define el valor de la propiedad participantID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantID(String value) {
                        this.participantID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participantCategoryID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantCategoryID() {
                        return participantCategoryID;
                    }

                    /**
                     * Define el valor de la propiedad participantCategoryID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantCategoryID(String value) {
                        this.participantCategoryID = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *       &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *       &amp;lt;attribute name="Gender"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Male"/&amp;gt;
             *             &amp;lt;enumeration value="Female"/&amp;gt;
             *             &amp;lt;enumeration value="Unknown"/&amp;gt;
             *             &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
             *             &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
             *       &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "contact"
            })
            public static class Individual
                extends PersonNameType
            {

                @XmlElement(name = "Contact")
                protected ContactPersonType contact;
                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "Age")
                protected Integer age;
                @XmlAttribute(name = "BirthDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar birthDate;
                @XmlAttribute(name = "Gender")
                protected String gender;
                @XmlAttribute(name = "Nationality")
                protected String nationality;
                @XmlAttribute(name = "LeadCustomerInd")
                protected Boolean leadCustomerInd;
                @XmlAttribute(name = "InfantInd")
                protected Boolean infantInd;

                /**
                 * Obtiene el valor de la propiedad contact.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ContactPersonType }
                 *     
                 */
                public ContactPersonType getContact() {
                    return contact;
                }

                /**
                 * Define el valor de la propiedad contact.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ContactPersonType }
                 *     
                 */
                public void setContact(ContactPersonType value) {
                    this.contact = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setAge(Integer value) {
                    this.age = value;
                }

                /**
                 * Obtiene el valor de la propiedad birthDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getBirthDate() {
                    return birthDate;
                }

                /**
                 * Define el valor de la propiedad birthDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setBirthDate(XMLGregorianCalendar value) {
                    this.birthDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad gender.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGender() {
                    return gender;
                }

                /**
                 * Define el valor de la propiedad gender.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGender(String value) {
                    this.gender = value;
                }

                /**
                 * Obtiene el valor de la propiedad nationality.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNationality() {
                    return nationality;
                }

                /**
                 * Define el valor de la propiedad nationality.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNationality(String value) {
                    this.nationality = value;
                }

                /**
                 * Obtiene el valor de la propiedad leadCustomerInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isLeadCustomerInd() {
                    return leadCustomerInd;
                }

                /**
                 * Define el valor de la propiedad leadCustomerInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setLeadCustomerInd(Boolean value) {
                    this.leadCustomerInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad infantInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isInfantInd() {
                    return infantInd;
                }

                /**
                 * Define el valor de la propiedad infantInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setInfantInd(Boolean value) {
                    this.infantInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class LodgingInfo
                extends TourActivityLodgingType
            {

                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;
                @XmlAttribute(name = "GroupID")
                protected String groupID;

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "detail"
        })
        public static class PaymentInfo
            extends TourActivityChargeType
        {

            @XmlElement(name = "Detail")
            protected PaymentFormType detail;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Obtiene el valor de la propiedad detail.
             * 
             * @return
             *     possible object is
             *     {@link PaymentFormType }
             *     
             */
            public PaymentFormType getDetail() {
                return detail;
            }

            /**
             * Define el valor de la propiedad detail.
             * 
             * @param value
             *     allowed object is
             *     {@link PaymentFormType }
             *     
             */
            public void setDetail(PaymentFormType value) {
                this.detail = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityTransRequestType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ParticipantList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "participantList"
        })
        public static class PickupDropoff
            extends TourActivityTransRequestType
        {

            @XmlElement(name = "ParticipantList")
            protected List<OTATourActivityBookRQ.BookingInfo.PickupDropoff.ParticipantList> participantList;

            /**
             * Gets the value of the participantList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getParticipantList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityBookRQ.BookingInfo.PickupDropoff.ParticipantList }
             * 
             * 
             */
            public List<OTATourActivityBookRQ.BookingInfo.PickupDropoff.ParticipantList> getParticipantList() {
                if (participantList == null) {
                    participantList = new ArrayList<OTATourActivityBookRQ.BookingInfo.PickupDropoff.ParticipantList>();
                }
                return this.participantList;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ParticipantList {

                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;
                @XmlAttribute(name = "GroupID")
                protected String groupID;

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "summary",
            "participantCategory",
            "group"
        })
        public static class Pricing {

            @XmlElement(name = "Summary")
            protected OTATourActivityBookRQ.BookingInfo.Pricing.Summary summary;
            @XmlElement(name = "ParticipantCategory")
            protected List<OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory> participantCategory;
            @XmlElement(name = "Group")
            protected List<OTATourActivityBookRQ.BookingInfo.Pricing.Group> group;

            /**
             * Obtiene el valor de la propiedad summary.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityBookRQ.BookingInfo.Pricing.Summary }
             *     
             */
            public OTATourActivityBookRQ.BookingInfo.Pricing.Summary getSummary() {
                return summary;
            }

            /**
             * Define el valor de la propiedad summary.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityBookRQ.BookingInfo.Pricing.Summary }
             *     
             */
            public void setSummary(OTATourActivityBookRQ.BookingInfo.Pricing.Summary value) {
                this.summary = value;
            }

            /**
             * Gets the value of the participantCategory property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantCategory property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getParticipantCategory().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory }
             * 
             * 
             */
            public List<OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory> getParticipantCategory() {
                if (participantCategory == null) {
                    participantCategory = new ArrayList<OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory>();
                }
                return this.participantCategory;
            }

            /**
             * Gets the value of the group property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the group property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getGroup().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityBookRQ.BookingInfo.Pricing.Group }
             * 
             * 
             */
            public List<OTATourActivityBookRQ.BookingInfo.Pricing.Group> getGroup() {
                if (group == null) {
                    group = new ArrayList<OTATourActivityBookRQ.BookingInfo.Pricing.Group>();
                }
                return this.group;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "price"
            })
            public static class Group {

                @XmlElement(name = "Price")
                protected TourActivityChargeType price;
                @XmlAttribute(name = "GroupCode")
                protected String groupCode;
                @XmlAttribute(name = "GroupName")
                protected String groupName;
                @XmlAttribute(name = "MinGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger minGroupSize;
                @XmlAttribute(name = "MaxGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maxGroupSize;
                @XmlAttribute(name = "KnownGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger knownGroupSize;

                /**
                 * Obtiene el valor de la propiedad price.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getPrice() {
                    return price;
                }

                /**
                 * Define el valor de la propiedad price.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setPrice(TourActivityChargeType value) {
                    this.price = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupCode() {
                    return groupCode;
                }

                /**
                 * Define el valor de la propiedad groupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupCode(String value) {
                    this.groupCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupName() {
                    return groupName;
                }

                /**
                 * Define el valor de la propiedad groupName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupName(String value) {
                    this.groupName = value;
                }

                /**
                 * Obtiene el valor de la propiedad minGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMinGroupSize() {
                    return minGroupSize;
                }

                /**
                 * Define el valor de la propiedad minGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMinGroupSize(BigInteger value) {
                    this.minGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad maxGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaxGroupSize() {
                    return maxGroupSize;
                }

                /**
                 * Define el valor de la propiedad maxGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaxGroupSize(BigInteger value) {
                    this.maxGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad knownGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getKnownGroupSize() {
                    return knownGroupSize;
                }

                /**
                 * Define el valor de la propiedad knownGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setKnownGroupSize(BigInteger value) {
                    this.knownGroupSize = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "qualifierInfo",
                "price",
                "tpaExtensions"
            })
            public static class ParticipantCategory {

                @XmlElement(name = "QualifierInfo")
                protected OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo qualifierInfo;
                @XmlElement(name = "Price")
                protected TourActivityChargeType price;
                @XmlElement(name = "TPA_Extensions")
                protected TPAExtensionsType tpaExtensions;
                @XmlAttribute(name = "Age")
                protected Integer age;

                /**
                 * Obtiene el valor de la propiedad qualifierInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo }
                 *     
                 */
                public OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo getQualifierInfo() {
                    return qualifierInfo;
                }

                /**
                 * Define el valor de la propiedad qualifierInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo }
                 *     
                 */
                public void setQualifierInfo(OTATourActivityBookRQ.BookingInfo.Pricing.ParticipantCategory.QualifierInfo value) {
                    this.qualifierInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad price.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getPrice() {
                    return price;
                }

                /**
                 * Define el valor de la propiedad price.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setPrice(TourActivityChargeType value) {
                    this.price = value;
                }

                /**
                 * Obtiene el valor de la propiedad tpaExtensions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public TPAExtensionsType getTPAExtensions() {
                    return tpaExtensions;
                }

                /**
                 * Define el valor de la propiedad tpaExtensions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public void setTPAExtensions(TPAExtensionsType value) {
                    this.tpaExtensions = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setAge(Integer value) {
                    this.age = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class QualifierInfo
                    extends AgeQualifierType
                {


                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pricingType"
            })
            public static class Summary
                extends TourActivityChargeType
            {

                @XmlElement(name = "PricingType")
                protected OTATourActivityBookRQ.BookingInfo.Pricing.Summary.PricingType pricingType;

                /**
                 * Obtiene el valor de la propiedad pricingType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityBookRQ.BookingInfo.Pricing.Summary.PricingType }
                 *     
                 */
                public OTATourActivityBookRQ.BookingInfo.Pricing.Summary.PricingType getPricingType() {
                    return pricingType;
                }

                /**
                 * Define el valor de la propiedad pricingType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityBookRQ.BookingInfo.Pricing.Summary.PricingType }
                 *     
                 */
                public void setPricingType(OTATourActivityBookRQ.BookingInfo.Pricing.Summary.PricingType value) {
                    this.pricingType = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                 *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class PricingType {

                    @XmlValue
                    protected TourActivityPricingTypeEnum value;
                    @XmlAttribute(name = "Extension")
                    protected String extension;

                    /**
                     * Tour and activity pricing options.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public TourActivityPricingTypeEnum getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public void setValue(TourActivityPricingTypeEnum value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad extension.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExtension() {
                        return extension;
                    }

                    /**
                     * Define el valor de la propiedad extension.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExtension(String value) {
                        this.extension = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Schedule {

            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }

    }

}
