
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Use: This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" enumeration in combination with the Code Extension fields to exchange a value that is not in the list and is known to your trading partners.
 * 
 * &lt;p&gt;Clase Java para List_ChargeUnit complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="List_ChargeUnit"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit_Base"&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "List_ChargeUnit", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.CodeListFeeType.Qualifiers.ChargeUnit.Unit.class,
    org.opentravel.ota._2003._05.CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Unit.class
})
public class ListChargeUnit {

    @XmlValue
    protected ListChargeUnitBase value;

    /**
     * Source: Charge Type (CHG) OpenTravel codelist.
     * 
     * @return
     *     possible object is
     *     {@link ListChargeUnitBase }
     *     
     */
    public ListChargeUnitBase getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link ListChargeUnitBase }
     *     
     */
    public void setValue(ListChargeUnitBase value) {
        this.value = value;
    }

}
