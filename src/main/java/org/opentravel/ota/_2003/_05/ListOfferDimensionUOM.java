
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferDimensionUOM.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferDimensionUOM"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Centimeter"/&amp;gt;
 *     &amp;lt;enumeration value="Foot/Feet"/&amp;gt;
 *     &amp;lt;enumeration value="Inch"/&amp;gt;
 *     &amp;lt;enumeration value="Metre/Meter"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferDimensionUOM")
@XmlEnum
public enum ListOfferDimensionUOM {

    @XmlEnumValue("Centimeter")
    CENTIMETER("Centimeter"),
    @XmlEnumValue("Foot/Feet")
    FOOT_FEET("Foot/Feet"),
    @XmlEnumValue("Inch")
    INCH("Inch"),
    @XmlEnumValue("Metre/Meter")
    METRE_METER("Metre/Meter"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferDimensionUOM(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferDimensionUOM fromValue(String v) {
        for (ListOfferDimensionUOM c: ListOfferDimensionUOM.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
