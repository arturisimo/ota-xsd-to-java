
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="FlightInfoDetails" maxOccurs="999" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="FlightLegInfo" maxOccurs="10"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="DepartureAirport" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
 *                                       &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ArrivalAirport" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
 *                                       &amp;lt;attribute name="Diversion" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="BaggageClaim" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                       &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
 *                               &amp;lt;element name="OperatingAirline" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Comment" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="DepartureDateTime" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                       &amp;lt;attribute name="NextAdvisoryTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ArrivalDateTime" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="OperationTimes" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="OperationTime" maxOccurs="99"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OperationTimeGroup"/&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FlifoLegAttributes"/&amp;gt;
 *                             &amp;lt;attribute name="FlightStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                             &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirDetailsRSAttributes"/&amp;gt;
 *                   &amp;lt;attribute name="MessageStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                   &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "flightInfoDetails",
    "errors"
})
@XmlRootElement(name = "OTA_AirFlifoRS")
public class OTAAirFlifoRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "FlightInfoDetails")
    protected List<OTAAirFlifoRS.FlightInfoDetails> flightInfoDetails;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the flightInfoDetails property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightInfoDetails property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFlightInfoDetails().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirFlifoRS.FlightInfoDetails }
     * 
     * 
     */
    public List<OTAAirFlifoRS.FlightInfoDetails> getFlightInfoDetails() {
        if (flightInfoDetails == null) {
            flightInfoDetails = new ArrayList<OTAAirFlifoRS.FlightInfoDetails>();
        }
        return this.flightInfoDetails;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FlightLegInfo" maxOccurs="10"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="DepartureAirport" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
     *                           &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ArrivalAirport" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Diversion" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="BaggageClaim" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
     *                   &amp;lt;element name="OperatingAirline" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Comment" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="DepartureDateTime" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="NextAdvisoryTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ArrivalDateTime" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="OperationTimes" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="OperationTime" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OperationTimeGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FlifoLegAttributes"/&amp;gt;
     *                 &amp;lt;attribute name="FlightStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirDetailsRSAttributes"/&amp;gt;
     *       &amp;lt;attribute name="MessageStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightLegInfo",
        "comment",
        "warnings",
        "tpaExtensions"
    })
    public static class FlightInfoDetails {

        @XmlElement(name = "FlightLegInfo", required = true)
        protected List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo> flightLegInfo;
        @XmlElement(name = "Comment")
        protected List<FreeTextType> comment;
        @XmlElement(name = "Warnings")
        protected WarningsType warnings;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;
        @XmlAttribute(name = "MessageStatus")
        protected String messageStatus;
        @XmlAttribute(name = "FlightNumber")
        protected String flightNumber;
        @XmlAttribute(name = "TotalFlightTime")
        protected Duration totalFlightTime;
        @XmlAttribute(name = "TotalGroundTime")
        protected Duration totalGroundTime;
        @XmlAttribute(name = "TotalTripTime")
        protected Duration totalTripTime;
        @XmlAttribute(name = "TotalMiles")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger totalMiles;

        /**
         * Gets the value of the flightLegInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightLegInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFlightLegInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo }
         * 
         * 
         */
        public List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo> getFlightLegInfo() {
            if (flightLegInfo == null) {
                flightLegInfo = new ArrayList<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo>();
            }
            return this.flightLegInfo;
        }

        /**
         * Gets the value of the comment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getComment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FreeTextType }
         * 
         * 
         */
        public List<FreeTextType> getComment() {
            if (comment == null) {
                comment = new ArrayList<FreeTextType>();
            }
            return this.comment;
        }

        /**
         * Obtiene el valor de la propiedad warnings.
         * 
         * @return
         *     possible object is
         *     {@link WarningsType }
         *     
         */
        public WarningsType getWarnings() {
            return warnings;
        }

        /**
         * Define el valor de la propiedad warnings.
         * 
         * @param value
         *     allowed object is
         *     {@link WarningsType }
         *     
         */
        public void setWarnings(WarningsType value) {
            this.warnings = value;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

        /**
         * Obtiene el valor de la propiedad messageStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessageStatus() {
            return messageStatus;
        }

        /**
         * Define el valor de la propiedad messageStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessageStatus(String value) {
            this.messageStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad flightNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightNumber() {
            return flightNumber;
        }

        /**
         * Define el valor de la propiedad flightNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightNumber(String value) {
            this.flightNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad totalFlightTime.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getTotalFlightTime() {
            return totalFlightTime;
        }

        /**
         * Define el valor de la propiedad totalFlightTime.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setTotalFlightTime(Duration value) {
            this.totalFlightTime = value;
        }

        /**
         * Obtiene el valor de la propiedad totalGroundTime.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getTotalGroundTime() {
            return totalGroundTime;
        }

        /**
         * Define el valor de la propiedad totalGroundTime.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setTotalGroundTime(Duration value) {
            this.totalGroundTime = value;
        }

        /**
         * Obtiene el valor de la propiedad totalTripTime.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getTotalTripTime() {
            return totalTripTime;
        }

        /**
         * Define el valor de la propiedad totalTripTime.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setTotalTripTime(Duration value) {
            this.totalTripTime = value;
        }

        /**
         * Obtiene el valor de la propiedad totalMiles.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalMiles() {
            return totalMiles;
        }

        /**
         * Define el valor de la propiedad totalMiles.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalMiles(BigInteger value) {
            this.totalMiles = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="DepartureAirport" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
         *                 &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ArrivalAirport" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Diversion" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="BaggageClaim" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
         *         &amp;lt;element name="OperatingAirline" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}EquipmentType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Comment" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="DepartureDateTime" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="NextAdvisoryTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ArrivalDateTime" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="OperationTimes" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="OperationTime" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OperationTimeGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FlifoLegAttributes"/&amp;gt;
         *       &amp;lt;attribute name="FlightStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "departureAirport",
            "arrivalAirport",
            "marketingAirline",
            "operatingAirline",
            "equipment",
            "comment",
            "departureDateTime",
            "arrivalDateTime",
            "operationTimes"
        })
        public static class FlightLegInfo {

            @XmlElement(name = "DepartureAirport")
            protected OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport departureAirport;
            @XmlElement(name = "ArrivalAirport")
            protected List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalAirport> arrivalAirport;
            @XmlElement(name = "MarketingAirline", required = true)
            protected CompanyNameType marketingAirline;
            @XmlElement(name = "OperatingAirline")
            protected OperatingAirlineType operatingAirline;
            @XmlElement(name = "Equipment")
            protected EquipmentType equipment;
            @XmlElement(name = "Comment")
            protected List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.Comment> comment;
            @XmlElement(name = "DepartureDateTime")
            protected OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime departureDateTime;
            @XmlElement(name = "ArrivalDateTime")
            protected OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime arrivalDateTime;
            @XmlElement(name = "OperationTimes")
            protected OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes operationTimes;
            @XmlAttribute(name = "FlightStatus")
            protected String flightStatus;
            @XmlAttribute(name = "OnTimeRate")
            protected BigDecimal onTimeRate;
            @XmlAttribute(name = "FlightNumber")
            protected String flightNumber;
            @XmlAttribute(name = "JourneyDuration")
            protected Duration journeyDuration;
            @XmlAttribute(name = "GroundDuration")
            protected Duration groundDuration;
            @XmlAttribute(name = "AccumulatedDuration")
            protected Duration accumulatedDuration;
            @XmlAttribute(name = "LegDistance")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger legDistance;

            /**
             * Obtiene el valor de la propiedad departureAirport.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport }
             *     
             */
            public OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport getDepartureAirport() {
                return departureAirport;
            }

            /**
             * Define el valor de la propiedad departureAirport.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport }
             *     
             */
            public void setDepartureAirport(OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport value) {
                this.departureAirport = value;
            }

            /**
             * Gets the value of the arrivalAirport property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the arrivalAirport property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getArrivalAirport().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalAirport }
             * 
             * 
             */
            public List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalAirport> getArrivalAirport() {
                if (arrivalAirport == null) {
                    arrivalAirport = new ArrayList<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalAirport>();
                }
                return this.arrivalAirport;
            }

            /**
             * Obtiene el valor de la propiedad marketingAirline.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getMarketingAirline() {
                return marketingAirline;
            }

            /**
             * Define el valor de la propiedad marketingAirline.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setMarketingAirline(CompanyNameType value) {
                this.marketingAirline = value;
            }

            /**
             * Obtiene el valor de la propiedad operatingAirline.
             * 
             * @return
             *     possible object is
             *     {@link OperatingAirlineType }
             *     
             */
            public OperatingAirlineType getOperatingAirline() {
                return operatingAirline;
            }

            /**
             * Define el valor de la propiedad operatingAirline.
             * 
             * @param value
             *     allowed object is
             *     {@link OperatingAirlineType }
             *     
             */
            public void setOperatingAirline(OperatingAirlineType value) {
                this.operatingAirline = value;
            }

            /**
             * Obtiene el valor de la propiedad equipment.
             * 
             * @return
             *     possible object is
             *     {@link EquipmentType }
             *     
             */
            public EquipmentType getEquipment() {
                return equipment;
            }

            /**
             * Define el valor de la propiedad equipment.
             * 
             * @param value
             *     allowed object is
             *     {@link EquipmentType }
             *     
             */
            public void setEquipment(EquipmentType value) {
                this.equipment = value;
            }

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.Comment }
             * 
             * 
             */
            public List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.Comment> getComment() {
                if (comment == null) {
                    comment = new ArrayList<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.Comment>();
                }
                return this.comment;
            }

            /**
             * Obtiene el valor de la propiedad departureDateTime.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime }
             *     
             */
            public OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime getDepartureDateTime() {
                return departureDateTime;
            }

            /**
             * Define el valor de la propiedad departureDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime }
             *     
             */
            public void setDepartureDateTime(OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime value) {
                this.departureDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalDateTime.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime }
             *     
             */
            public OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime getArrivalDateTime() {
                return arrivalDateTime;
            }

            /**
             * Define el valor de la propiedad arrivalDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime }
             *     
             */
            public void setArrivalDateTime(OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime value) {
                this.arrivalDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad operationTimes.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes }
             *     
             */
            public OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes getOperationTimes() {
                return operationTimes;
            }

            /**
             * Define el valor de la propiedad operationTimes.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes }
             *     
             */
            public void setOperationTimes(OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes value) {
                this.operationTimes = value;
            }

            /**
             * Obtiene el valor de la propiedad flightStatus.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlightStatus() {
                return flightStatus;
            }

            /**
             * Define el valor de la propiedad flightStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlightStatus(String value) {
                this.flightStatus = value;
            }

            /**
             * Obtiene el valor de la propiedad onTimeRate.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getOnTimeRate() {
                return onTimeRate;
            }

            /**
             * Define el valor de la propiedad onTimeRate.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setOnTimeRate(BigDecimal value) {
                this.onTimeRate = value;
            }

            /**
             * Obtiene el valor de la propiedad flightNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlightNumber() {
                return flightNumber;
            }

            /**
             * Define el valor de la propiedad flightNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlightNumber(String value) {
                this.flightNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad journeyDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getJourneyDuration() {
                return journeyDuration;
            }

            /**
             * Define el valor de la propiedad journeyDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setJourneyDuration(Duration value) {
                this.journeyDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad groundDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getGroundDuration() {
                return groundDuration;
            }

            /**
             * Define el valor de la propiedad groundDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setGroundDuration(Duration value) {
                this.groundDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad accumulatedDuration.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getAccumulatedDuration() {
                return accumulatedDuration;
            }

            /**
             * Define el valor de la propiedad accumulatedDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setAccumulatedDuration(Duration value) {
                this.accumulatedDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad legDistance.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLegDistance() {
                return legDistance;
            }

            /**
             * Define el valor de la propiedad legDistance.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLegDistance(BigInteger value) {
                this.legDistance = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
             *       &amp;lt;attribute name="Diversion" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="BaggageClaim" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class ArrivalAirport {

                @XmlElement(name = "Comment")
                protected List<FreeTextType> comment;
                @XmlAttribute(name = "Diversion", required = true)
                protected boolean diversion;
                @XmlAttribute(name = "BaggageClaim")
                protected String baggageClaim;
                @XmlAttribute(name = "FlagStopInd")
                protected Boolean flagStopInd;
                @XmlAttribute(name = "LocationCode")
                protected String locationCode;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "Terminal")
                protected String terminal;
                @XmlAttribute(name = "Gate")
                protected String gate;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link FreeTextType }
                 * 
                 * 
                 */
                public List<FreeTextType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<FreeTextType>();
                    }
                    return this.comment;
                }

                /**
                 * Obtiene el valor de la propiedad diversion.
                 * 
                 */
                public boolean isDiversion() {
                    return diversion;
                }

                /**
                 * Define el valor de la propiedad diversion.
                 * 
                 */
                public void setDiversion(boolean value) {
                    this.diversion = value;
                }

                /**
                 * Obtiene el valor de la propiedad baggageClaim.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBaggageClaim() {
                    return baggageClaim;
                }

                /**
                 * Define el valor de la propiedad baggageClaim.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBaggageClaim(String value) {
                    this.baggageClaim = value;
                }

                /**
                 * Obtiene el valor de la propiedad flagStopInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isFlagStopInd() {
                    return flagStopInd;
                }

                /**
                 * Define el valor de la propiedad flagStopInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setFlagStopInd(Boolean value) {
                    this.flagStopInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad locationCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationCode() {
                    return locationCode;
                }

                /**
                 * Define el valor de la propiedad locationCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationCode(String value) {
                    this.locationCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad terminal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTerminal() {
                    return terminal;
                }

                /**
                 * Define el valor de la propiedad terminal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTerminal(String value) {
                    this.terminal = value;
                }

                /**
                 * Obtiene el valor de la propiedad gate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGate() {
                    return gate;
                }

                /**
                 * Define el valor de la propiedad gate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGate(String value) {
                    this.gate = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ArrivalDateTime {

                @XmlAttribute(name = "Scheduled")
                protected String scheduled;
                @XmlAttribute(name = "Estimated")
                protected String estimated;
                @XmlAttribute(name = "Actual")
                protected String actual;
                @XmlAttribute(name = "ReasonCode")
                protected String reasonCode;

                /**
                 * Obtiene el valor de la propiedad scheduled.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getScheduled() {
                    return scheduled;
                }

                /**
                 * Define el valor de la propiedad scheduled.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setScheduled(String value) {
                    this.scheduled = value;
                }

                /**
                 * Obtiene el valor de la propiedad estimated.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEstimated() {
                    return estimated;
                }

                /**
                 * Define el valor de la propiedad estimated.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEstimated(String value) {
                    this.estimated = value;
                }

                /**
                 * Obtiene el valor de la propiedad actual.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getActual() {
                    return actual;
                }

                /**
                 * Define el valor de la propiedad actual.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setActual(String value) {
                    this.actual = value;
                }

                /**
                 * Obtiene el valor de la propiedad reasonCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReasonCode() {
                    return reasonCode;
                }

                /**
                 * Define el valor de la propiedad reasonCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReasonCode(String value) {
                    this.reasonCode = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Comment
                extends FreeTextType
            {


            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
             *       &amp;lt;attribute name="FlagStopInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class DepartureAirport {

                @XmlElement(name = "Comment")
                protected List<FreeTextType> comment;
                @XmlAttribute(name = "FlagStopInd")
                protected Boolean flagStopInd;
                @XmlAttribute(name = "LocationCode")
                protected String locationCode;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "Terminal")
                protected String terminal;
                @XmlAttribute(name = "Gate")
                protected String gate;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link FreeTextType }
                 * 
                 * 
                 */
                public List<FreeTextType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<FreeTextType>();
                    }
                    return this.comment;
                }

                /**
                 * Obtiene el valor de la propiedad flagStopInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isFlagStopInd() {
                    return flagStopInd;
                }

                /**
                 * Define el valor de la propiedad flagStopInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setFlagStopInd(Boolean value) {
                    this.flagStopInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad locationCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationCode() {
                    return locationCode;
                }

                /**
                 * Define el valor de la propiedad locationCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationCode(String value) {
                    this.locationCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad terminal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTerminal() {
                    return terminal;
                }

                /**
                 * Define el valor de la propiedad terminal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTerminal(String value) {
                    this.terminal = value;
                }

                /**
                 * Obtiene el valor de la propiedad gate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGate() {
                    return gate;
                }

                /**
                 * Define el valor de la propiedad gate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGate(String value) {
                    this.gate = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Scheduled" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="Estimated" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="Actual" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ReasonCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="NextAdvisoryTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DepartureDateTime {

                @XmlAttribute(name = "Scheduled")
                protected String scheduled;
                @XmlAttribute(name = "Estimated")
                protected String estimated;
                @XmlAttribute(name = "Actual")
                protected String actual;
                @XmlAttribute(name = "ReasonCode")
                protected String reasonCode;
                @XmlAttribute(name = "NextAdvisoryTime")
                protected String nextAdvisoryTime;

                /**
                 * Obtiene el valor de la propiedad scheduled.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getScheduled() {
                    return scheduled;
                }

                /**
                 * Define el valor de la propiedad scheduled.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setScheduled(String value) {
                    this.scheduled = value;
                }

                /**
                 * Obtiene el valor de la propiedad estimated.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEstimated() {
                    return estimated;
                }

                /**
                 * Define el valor de la propiedad estimated.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEstimated(String value) {
                    this.estimated = value;
                }

                /**
                 * Obtiene el valor de la propiedad actual.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getActual() {
                    return actual;
                }

                /**
                 * Define el valor de la propiedad actual.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setActual(String value) {
                    this.actual = value;
                }

                /**
                 * Obtiene el valor de la propiedad reasonCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReasonCode() {
                    return reasonCode;
                }

                /**
                 * Define el valor de la propiedad reasonCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReasonCode(String value) {
                    this.reasonCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad nextAdvisoryTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNextAdvisoryTime() {
                    return nextAdvisoryTime;
                }

                /**
                 * Define el valor de la propiedad nextAdvisoryTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNextAdvisoryTime(String value) {
                    this.nextAdvisoryTime = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="OperationTime" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OperationTimeGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "operationTime"
            })
            public static class OperationTimes {

                @XmlElement(name = "OperationTime", required = true)
                protected List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes.OperationTime> operationTime;

                /**
                 * Gets the value of the operationTime property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the operationTime property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOperationTime().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes.OperationTime }
                 * 
                 * 
                 */
                public List<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes.OperationTime> getOperationTime() {
                    if (operationTime == null) {
                        operationTime = new ArrayList<OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.OperationTimes.OperationTime>();
                    }
                    return this.operationTime;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OperationTimeGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class OperationTime {

                    @XmlAttribute(name = "Time", required = true)
                    protected String time;
                    @XmlAttribute(name = "OperationType", required = true)
                    protected String operationType;
                    @XmlAttribute(name = "TimeType", required = true)
                    protected String timeType;
                    @XmlAttribute(name = "ReasonCode")
                    protected String reasonCode;

                    /**
                     * Obtiene el valor de la propiedad time.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTime() {
                        return time;
                    }

                    /**
                     * Define el valor de la propiedad time.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTime(String value) {
                        this.time = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad operationType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOperationType() {
                        return operationType;
                    }

                    /**
                     * Define el valor de la propiedad operationType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOperationType(String value) {
                        this.operationType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad timeType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTimeType() {
                        return timeType;
                    }

                    /**
                     * Define el valor de la propiedad timeType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTimeType(String value) {
                        this.timeType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad reasonCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getReasonCode() {
                        return reasonCode;
                    }

                    /**
                     * Define el valor de la propiedad reasonCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setReasonCode(String value) {
                        this.reasonCode = value;
                    }

                }

            }

        }

    }

}
