
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * The category of accommodation on a train.
 * 
 * &lt;p&gt;Clase Java para AccommodationCategoryBaseType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccommodationCategoryBaseType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;choice minOccurs="0"&amp;gt;
 *           &amp;lt;element name="Seat" type="{http://www.opentravel.org/OTA/2003/05}SeatAccommodationType"/&amp;gt;
 *           &amp;lt;element name="Berth" type="{http://www.opentravel.org/OTA/2003/05}BerthAccommodationType"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *         &amp;lt;element name="Class" type="{http://www.opentravel.org/OTA/2003/05}AccommodationClass" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Compartment" type="{http://www.opentravel.org/OTA/2003/05}CompartmentType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccommodationCategoryBaseType", propOrder = {
    "seat",
    "berth",
    "clazz",
    "compartment"
})
public class AccommodationCategoryBaseType {

    @XmlElement(name = "Seat")
    @XmlSchemaType(name = "string")
    protected SeatAccommodationType seat;
    @XmlElement(name = "Berth")
    @XmlSchemaType(name = "string")
    protected BerthAccommodationType berth;
    @XmlElement(name = "Class")
    protected AccommodationClass clazz;
    @XmlElement(name = "Compartment")
    protected CompartmentType compartment;

    /**
     * Obtiene el valor de la propiedad seat.
     * 
     * @return
     *     possible object is
     *     {@link SeatAccommodationType }
     *     
     */
    public SeatAccommodationType getSeat() {
        return seat;
    }

    /**
     * Define el valor de la propiedad seat.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatAccommodationType }
     *     
     */
    public void setSeat(SeatAccommodationType value) {
        this.seat = value;
    }

    /**
     * Obtiene el valor de la propiedad berth.
     * 
     * @return
     *     possible object is
     *     {@link BerthAccommodationType }
     *     
     */
    public BerthAccommodationType getBerth() {
        return berth;
    }

    /**
     * Define el valor de la propiedad berth.
     * 
     * @param value
     *     allowed object is
     *     {@link BerthAccommodationType }
     *     
     */
    public void setBerth(BerthAccommodationType value) {
        this.berth = value;
    }

    /**
     * Obtiene el valor de la propiedad clazz.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationClass }
     *     
     */
    public AccommodationClass getClazz() {
        return clazz;
    }

    /**
     * Define el valor de la propiedad clazz.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationClass }
     *     
     */
    public void setClazz(AccommodationClass value) {
        this.clazz = value;
    }

    /**
     * Obtiene el valor de la propiedad compartment.
     * 
     * @return
     *     possible object is
     *     {@link CompartmentType }
     *     
     */
    public CompartmentType getCompartment() {
        return compartment;
    }

    /**
     * Define el valor de la propiedad compartment.
     * 
     * @param value
     *     allowed object is
     *     {@link CompartmentType }
     *     
     */
    public void setCompartment(CompartmentType value) {
        this.compartment = value;
    }

}
