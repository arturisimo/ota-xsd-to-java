
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPassengerType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPassengerType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="CorporateEmployee"/&amp;gt;
 *     &amp;lt;enumeration value="Disabled"/&amp;gt;
 *     &amp;lt;enumeration value="FrequentGuest"/&amp;gt;
 *     &amp;lt;enumeration value="FrequentTraveler"/&amp;gt;
 *     &amp;lt;enumeration value="Government"/&amp;gt;
 *     &amp;lt;enumeration value="Group"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyProgramMember"/&amp;gt;
 *     &amp;lt;enumeration value="MeetingAttendee"/&amp;gt;
 *     &amp;lt;enumeration value="Military"/&amp;gt;
 *     &amp;lt;enumeration value="Retired"/&amp;gt;
 *     &amp;lt;enumeration value="VIP"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPassengerType")
@XmlEnum
public enum ListOfferPassengerType {

    @XmlEnumValue("CorporateEmployee")
    CORPORATE_EMPLOYEE("CorporateEmployee"),
    @XmlEnumValue("Disabled")
    DISABLED("Disabled"),
    @XmlEnumValue("FrequentGuest")
    FREQUENT_GUEST("FrequentGuest"),
    @XmlEnumValue("FrequentTraveler")
    FREQUENT_TRAVELER("FrequentTraveler"),
    @XmlEnumValue("Government")
    GOVERNMENT("Government"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("LoyaltyProgramMember")
    LOYALTY_PROGRAM_MEMBER("LoyaltyProgramMember"),
    @XmlEnumValue("MeetingAttendee")
    MEETING_ATTENDEE("MeetingAttendee"),
    @XmlEnumValue("Military")
    MILITARY("Military"),
    @XmlEnumValue("Retired")
    RETIRED("Retired"),
    VIP("VIP"),
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferPassengerType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPassengerType fromValue(String v) {
        for (ListOfferPassengerType c: ListOfferPassengerType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
