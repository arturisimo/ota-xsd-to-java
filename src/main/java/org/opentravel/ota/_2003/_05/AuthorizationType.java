
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * ToDo - this types should be moved to OTA_CommonTypes. It's required in a common types file, since the both AuthRQ/RS use it.
 * 
 * &lt;p&gt;Clase Java para AuthorizationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AuthorizationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;element name="CheckAuthorization" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="BankAcct" type="{http://www.opentravel.org/OTA/2003/05}BankAcctType"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="CreditCardAuthorization" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="CreditCard" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardType"/&amp;gt;
 *                     &amp;lt;element name="ID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                   &amp;lt;attribute name="SourceType"&amp;gt;
 *                     &amp;lt;simpleType&amp;gt;
 *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                         &amp;lt;enumeration value="NormalTransaction"/&amp;gt;
 *                         &amp;lt;enumeration value="MailOrPhoneOrder"/&amp;gt;
 *                         &amp;lt;enumeration value="UnattendedTerminal"/&amp;gt;
 *                         &amp;lt;enumeration value="MerchantIsSuspicious"/&amp;gt;
 *                         &amp;lt;enumeration value="eCommerceSecuredTransaction"/&amp;gt;
 *                         &amp;lt;enumeration value="eCommerceUnsecuredTransaction"/&amp;gt;
 *                         &amp;lt;enumeration value="InFlightAirPhone"/&amp;gt;
 *                         &amp;lt;enumeration value="CID_NotLegible"/&amp;gt;
 *                         &amp;lt;enumeration value="CID_NotOnCard"/&amp;gt;
 *                       &amp;lt;/restriction&amp;gt;
 *                     &amp;lt;/simpleType&amp;gt;
 *                   &amp;lt;/attribute&amp;gt;
 *                   &amp;lt;attribute name="ExtendedPaymentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="ExtendedPaymentQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                   &amp;lt;attribute name="ExtendedPaymentFrequency" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
 *                   &amp;lt;attribute name="AuthorizationCode"&amp;gt;
 *                     &amp;lt;simpleType&amp;gt;
 *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                         &amp;lt;pattern value="[A-Za-z0-9]{1,12}"/&amp;gt;
 *                       &amp;lt;/restriction&amp;gt;
 *                     &amp;lt;/simpleType&amp;gt;
 *                   &amp;lt;/attribute&amp;gt;
 *                   &amp;lt;attribute name="ReversalIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="CardPresentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="E_CommerceCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                   &amp;lt;attribute name="AuthTransactionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                   &amp;lt;attribute name="AuthVerificationValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="AccountAuthorization" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="AccountInfo" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attribute name="AccountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;attribute name="CompanyName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;attribute name="AccountID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;attribute name="Password" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                   &amp;lt;attribute name="NonISO_CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *         &amp;lt;element name="DriversLicenseAuthorization" type="{http://www.opentravel.org/OTA/2003/05}DocumentType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BookingReferenceID" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                 &amp;lt;attribute name="IgnoreReservationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="PrincipalCompanyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="RefNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizationType", propOrder = {
    "checkAuthorization",
    "creditCardAuthorization",
    "accountAuthorization",
    "driversLicenseAuthorization",
    "bookingReferenceID"
})
public class AuthorizationType {

    @XmlElement(name = "CheckAuthorization")
    protected AuthorizationType.CheckAuthorization checkAuthorization;
    @XmlElement(name = "CreditCardAuthorization")
    protected AuthorizationType.CreditCardAuthorization creditCardAuthorization;
    @XmlElement(name = "AccountAuthorization")
    protected AuthorizationType.AccountAuthorization accountAuthorization;
    @XmlElement(name = "DriversLicenseAuthorization")
    protected DocumentType driversLicenseAuthorization;
    @XmlElement(name = "BookingReferenceID")
    protected AuthorizationType.BookingReferenceID bookingReferenceID;
    @XmlAttribute(name = "PrincipalCompanyCode")
    protected String principalCompanyCode;
    @XmlAttribute(name = "RefNumber")
    protected String refNumber;

    /**
     * Obtiene el valor de la propiedad checkAuthorization.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationType.CheckAuthorization }
     *     
     */
    public AuthorizationType.CheckAuthorization getCheckAuthorization() {
        return checkAuthorization;
    }

    /**
     * Define el valor de la propiedad checkAuthorization.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationType.CheckAuthorization }
     *     
     */
    public void setCheckAuthorization(AuthorizationType.CheckAuthorization value) {
        this.checkAuthorization = value;
    }

    /**
     * Obtiene el valor de la propiedad creditCardAuthorization.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationType.CreditCardAuthorization }
     *     
     */
    public AuthorizationType.CreditCardAuthorization getCreditCardAuthorization() {
        return creditCardAuthorization;
    }

    /**
     * Define el valor de la propiedad creditCardAuthorization.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationType.CreditCardAuthorization }
     *     
     */
    public void setCreditCardAuthorization(AuthorizationType.CreditCardAuthorization value) {
        this.creditCardAuthorization = value;
    }

    /**
     * Obtiene el valor de la propiedad accountAuthorization.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationType.AccountAuthorization }
     *     
     */
    public AuthorizationType.AccountAuthorization getAccountAuthorization() {
        return accountAuthorization;
    }

    /**
     * Define el valor de la propiedad accountAuthorization.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationType.AccountAuthorization }
     *     
     */
    public void setAccountAuthorization(AuthorizationType.AccountAuthorization value) {
        this.accountAuthorization = value;
    }

    /**
     * Obtiene el valor de la propiedad driversLicenseAuthorization.
     * 
     * @return
     *     possible object is
     *     {@link DocumentType }
     *     
     */
    public DocumentType getDriversLicenseAuthorization() {
        return driversLicenseAuthorization;
    }

    /**
     * Define el valor de la propiedad driversLicenseAuthorization.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentType }
     *     
     */
    public void setDriversLicenseAuthorization(DocumentType value) {
        this.driversLicenseAuthorization = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingReferenceID.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationType.BookingReferenceID }
     *     
     */
    public AuthorizationType.BookingReferenceID getBookingReferenceID() {
        return bookingReferenceID;
    }

    /**
     * Define el valor de la propiedad bookingReferenceID.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationType.BookingReferenceID }
     *     
     */
    public void setBookingReferenceID(AuthorizationType.BookingReferenceID value) {
        this.bookingReferenceID = value;
    }

    /**
     * Obtiene el valor de la propiedad principalCompanyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrincipalCompanyCode() {
        return principalCompanyCode;
    }

    /**
     * Define el valor de la propiedad principalCompanyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrincipalCompanyCode(String value) {
        this.principalCompanyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad refNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefNumber() {
        return refNumber;
    }

    /**
     * Define el valor de la propiedad refNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefNumber(String value) {
        this.refNumber = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AccountInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="AccountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="CompanyName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="AccountID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="Password" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="NonISO_CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountInfo"
    })
    public static class AccountAuthorization {

        @XmlElement(name = "AccountInfo")
        protected AuthorizationType.AccountAuthorization.AccountInfo accountInfo;
        @XmlAttribute(name = "NonISO_CurrencyCode")
        protected String nonISOCurrencyCode;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad accountInfo.
         * 
         * @return
         *     possible object is
         *     {@link AuthorizationType.AccountAuthorization.AccountInfo }
         *     
         */
        public AuthorizationType.AccountAuthorization.AccountInfo getAccountInfo() {
            return accountInfo;
        }

        /**
         * Define el valor de la propiedad accountInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AuthorizationType.AccountAuthorization.AccountInfo }
         *     
         */
        public void setAccountInfo(AuthorizationType.AccountAuthorization.AccountInfo value) {
            this.accountInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad nonISOCurrencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNonISOCurrencyCode() {
            return nonISOCurrencyCode;
        }

        /**
         * Define el valor de la propiedad nonISOCurrencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNonISOCurrencyCode(String value) {
            this.nonISOCurrencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="AccountName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="CompanyName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="AccountID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="Password" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AccountInfo {

            @XmlAttribute(name = "AccountName")
            protected String accountName;
            @XmlAttribute(name = "CompanyName")
            protected String companyName;
            @XmlAttribute(name = "AccountID")
            protected String accountID;
            @XmlAttribute(name = "Password")
            protected String password;
            @XmlAttribute(name = "Code")
            protected String code;

            /**
             * Obtiene el valor de la propiedad accountName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountName() {
                return accountName;
            }

            /**
             * Define el valor de la propiedad accountName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountName(String value) {
                this.accountName = value;
            }

            /**
             * Obtiene el valor de la propiedad companyName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyName() {
                return companyName;
            }

            /**
             * Define el valor de la propiedad companyName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyName(String value) {
                this.companyName = value;
            }

            /**
             * Obtiene el valor de la propiedad accountID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountID() {
                return accountID;
            }

            /**
             * Define el valor de la propiedad accountID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountID(String value) {
                this.accountID = value;
            }

            /**
             * Obtiene el valor de la propiedad password.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassword() {
                return password;
            }

            /**
             * Define el valor de la propiedad password.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassword(String value) {
                this.password = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *       &amp;lt;attribute name="IgnoreReservationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BookingReferenceID
        extends UniqueIDType
    {

        @XmlAttribute(name = "IgnoreReservationInd")
        protected Boolean ignoreReservationInd;

        /**
         * Obtiene el valor de la propiedad ignoreReservationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIgnoreReservationInd() {
            return ignoreReservationInd;
        }

        /**
         * Define el valor de la propiedad ignoreReservationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIgnoreReservationInd(Boolean value) {
            this.ignoreReservationInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BankAcct" type="{http://www.opentravel.org/OTA/2003/05}BankAcctType"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bankAcct"
    })
    public static class CheckAuthorization {

        @XmlElement(name = "BankAcct", required = true)
        protected BankAcctType bankAcct;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad bankAcct.
         * 
         * @return
         *     possible object is
         *     {@link BankAcctType }
         *     
         */
        public BankAcctType getBankAcct() {
            return bankAcct;
        }

        /**
         * Define el valor de la propiedad bankAcct.
         * 
         * @param value
         *     allowed object is
         *     {@link BankAcctType }
         *     
         */
        public void setBankAcct(BankAcctType value) {
            this.bankAcct = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CreditCard" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardType"/&amp;gt;
     *         &amp;lt;element name="ID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="SourceType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="NormalTransaction"/&amp;gt;
     *             &amp;lt;enumeration value="MailOrPhoneOrder"/&amp;gt;
     *             &amp;lt;enumeration value="UnattendedTerminal"/&amp;gt;
     *             &amp;lt;enumeration value="MerchantIsSuspicious"/&amp;gt;
     *             &amp;lt;enumeration value="eCommerceSecuredTransaction"/&amp;gt;
     *             &amp;lt;enumeration value="eCommerceUnsecuredTransaction"/&amp;gt;
     *             &amp;lt;enumeration value="InFlightAirPhone"/&amp;gt;
     *             &amp;lt;enumeration value="CID_NotLegible"/&amp;gt;
     *             &amp;lt;enumeration value="CID_NotOnCard"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="ExtendedPaymentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ExtendedPaymentQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *       &amp;lt;attribute name="ExtendedPaymentFrequency" type="{http://www.opentravel.org/OTA/2003/05}TimeUnitType" /&amp;gt;
     *       &amp;lt;attribute name="AuthorizationCode"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[A-Za-z0-9]{1,12}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="ReversalIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="CardPresentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="E_CommerceCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="AuthTransactionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="AuthVerificationValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "creditCard",
        "id"
    })
    public static class CreditCardAuthorization {

        @XmlElement(name = "CreditCard", required = true)
        protected PaymentCardType creditCard;
        @XmlElement(name = "ID")
        protected List<UniqueIDType> id;
        @XmlAttribute(name = "SourceType")
        protected String sourceType;
        @XmlAttribute(name = "ExtendedPaymentInd")
        protected Boolean extendedPaymentInd;
        @XmlAttribute(name = "ExtendedPaymentQuantity")
        protected Integer extendedPaymentQuantity;
        @XmlAttribute(name = "ExtendedPaymentFrequency")
        protected TimeUnitType extendedPaymentFrequency;
        @XmlAttribute(name = "AuthorizationCode")
        protected String authorizationCode;
        @XmlAttribute(name = "ReversalIndicator")
        protected Boolean reversalIndicator;
        @XmlAttribute(name = "CardPresentInd")
        protected Boolean cardPresentInd;
        @XmlAttribute(name = "E_CommerceCode")
        protected String eCommerceCode;
        @XmlAttribute(name = "AuthTransactionID")
        protected String authTransactionID;
        @XmlAttribute(name = "AuthVerificationValue")
        protected String authVerificationValue;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad creditCard.
         * 
         * @return
         *     possible object is
         *     {@link PaymentCardType }
         *     
         */
        public PaymentCardType getCreditCard() {
            return creditCard;
        }

        /**
         * Define el valor de la propiedad creditCard.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentCardType }
         *     
         */
        public void setCreditCard(PaymentCardType value) {
            this.creditCard = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the id property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getID().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link UniqueIDType }
         * 
         * 
         */
        public List<UniqueIDType> getID() {
            if (id == null) {
                id = new ArrayList<UniqueIDType>();
            }
            return this.id;
        }

        /**
         * Obtiene el valor de la propiedad sourceType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSourceType() {
            return sourceType;
        }

        /**
         * Define el valor de la propiedad sourceType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSourceType(String value) {
            this.sourceType = value;
        }

        /**
         * Obtiene el valor de la propiedad extendedPaymentInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExtendedPaymentInd() {
            return extendedPaymentInd;
        }

        /**
         * Define el valor de la propiedad extendedPaymentInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExtendedPaymentInd(Boolean value) {
            this.extendedPaymentInd = value;
        }

        /**
         * Obtiene el valor de la propiedad extendedPaymentQuantity.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getExtendedPaymentQuantity() {
            return extendedPaymentQuantity;
        }

        /**
         * Define el valor de la propiedad extendedPaymentQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setExtendedPaymentQuantity(Integer value) {
            this.extendedPaymentQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad extendedPaymentFrequency.
         * 
         * @return
         *     possible object is
         *     {@link TimeUnitType }
         *     
         */
        public TimeUnitType getExtendedPaymentFrequency() {
            return extendedPaymentFrequency;
        }

        /**
         * Define el valor de la propiedad extendedPaymentFrequency.
         * 
         * @param value
         *     allowed object is
         *     {@link TimeUnitType }
         *     
         */
        public void setExtendedPaymentFrequency(TimeUnitType value) {
            this.extendedPaymentFrequency = value;
        }

        /**
         * Obtiene el valor de la propiedad authorizationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthorizationCode() {
            return authorizationCode;
        }

        /**
         * Define el valor de la propiedad authorizationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthorizationCode(String value) {
            this.authorizationCode = value;
        }

        /**
         * Obtiene el valor de la propiedad reversalIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReversalIndicator() {
            return reversalIndicator;
        }

        /**
         * Define el valor de la propiedad reversalIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReversalIndicator(Boolean value) {
            this.reversalIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad cardPresentInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCardPresentInd() {
            return cardPresentInd;
        }

        /**
         * Define el valor de la propiedad cardPresentInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCardPresentInd(Boolean value) {
            this.cardPresentInd = value;
        }

        /**
         * Obtiene el valor de la propiedad eCommerceCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getECommerceCode() {
            return eCommerceCode;
        }

        /**
         * Define el valor de la propiedad eCommerceCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setECommerceCode(String value) {
            this.eCommerceCode = value;
        }

        /**
         * Obtiene el valor de la propiedad authTransactionID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthTransactionID() {
            return authTransactionID;
        }

        /**
         * Define el valor de la propiedad authTransactionID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthTransactionID(String value) {
            this.authTransactionID = value;
        }

        /**
         * Obtiene el valor de la propiedad authVerificationValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthVerificationValue() {
            return authVerificationValue;
        }

        /**
         * Define el valor de la propiedad authVerificationValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthVerificationValue(String value) {
            this.authVerificationValue = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }

}
