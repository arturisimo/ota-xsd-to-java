
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ProcessingInformation" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="MaxResponses" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                 &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                 &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                 &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="AlternateAvailInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TourActivity"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="BasicInfo" maxOccurs="9"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AccessibilityRequirement" type="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityReqType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="PromotionCode" maxOccurs="9" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;simpleContent&amp;gt;
 *                                   &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/simpleContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Program" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="NegotiatedRate" type="{http://www.opentravel.org/OTA/2003/05}TourActivityNegotiatedPricing" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Schedule" maxOccurs="9" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="StartTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;choice&amp;gt;
 *                     &amp;lt;element name="ParticipantCount" maxOccurs="unbounded"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantCategoryType"&amp;gt;
 *                             &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="GroupInfo"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                             &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                             &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/choice&amp;gt;
 *                   &amp;lt;element name="MultimodalOffer" type="{http://www.opentravel.org/OTA/2003/05}MultiModalOfferType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "processingInformation",
    "tourActivity",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_TourActivityAvailRQ")
public class OTATourActivityAvailRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "ProcessingInformation")
    protected OTATourActivityAvailRQ.ProcessingInformation processingInformation;
    @XmlElement(name = "TourActivity", required = true)
    protected OTATourActivityAvailRQ.TourActivity tourActivity;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInformation.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityAvailRQ.ProcessingInformation }
     *     
     */
    public OTATourActivityAvailRQ.ProcessingInformation getProcessingInformation() {
        return processingInformation;
    }

    /**
     * Define el valor de la propiedad processingInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityAvailRQ.ProcessingInformation }
     *     
     */
    public void setProcessingInformation(OTATourActivityAvailRQ.ProcessingInformation value) {
        this.processingInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad tourActivity.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityAvailRQ.TourActivity }
     *     
     */
    public OTATourActivityAvailRQ.TourActivity getTourActivity() {
        return tourActivity;
    }

    /**
     * Define el valor de la propiedad tourActivity.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityAvailRQ.TourActivity }
     *     
     */
    public void setTourActivity(OTATourActivityAvailRQ.TourActivity value) {
        this.tourActivity = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="MaxResponses" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *       &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="AlternateAvailInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInformation {

        @XmlAttribute(name = "MaxResponses")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxResponses;
        @XmlAttribute(name = "MoreDataEchoToken")
        protected String moreDataEchoToken;
        @XmlAttribute(name = "LanguageCode")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String languageCode;
        @XmlAttribute(name = "DisplayCurrency")
        protected String displayCurrency;
        @XmlAttribute(name = "PricingCurrency")
        protected String pricingCurrency;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "AlternateAvailInd")
        protected Boolean alternateAvailInd;

        /**
         * Obtiene el valor de la propiedad maxResponses.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxResponses() {
            return maxResponses;
        }

        /**
         * Define el valor de la propiedad maxResponses.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxResponses(BigInteger value) {
            this.maxResponses = value;
        }

        /**
         * Obtiene el valor de la propiedad moreDataEchoToken.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoreDataEchoToken() {
            return moreDataEchoToken;
        }

        /**
         * Define el valor de la propiedad moreDataEchoToken.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoreDataEchoToken(String value) {
            this.moreDataEchoToken = value;
        }

        /**
         * Obtiene el valor de la propiedad languageCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageCode() {
            return languageCode;
        }

        /**
         * Define el valor de la propiedad languageCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageCode(String value) {
            this.languageCode = value;
        }

        /**
         * Obtiene el valor de la propiedad displayCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayCurrency() {
            return displayCurrency;
        }

        /**
         * Define el valor de la propiedad displayCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayCurrency(String value) {
            this.displayCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad pricingCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricingCurrency() {
            return pricingCurrency;
        }

        /**
         * Define el valor de la propiedad pricingCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricingCurrency(String value) {
            this.pricingCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad alternateAvailInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAlternateAvailInd() {
            return alternateAvailInd;
        }

        /**
         * Define el valor de la propiedad alternateAvailInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAlternateAvailInd(Boolean value) {
            this.alternateAvailInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BasicInfo" maxOccurs="9"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AccessibilityRequirement" type="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityReqType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PromotionCode" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Program" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="NegotiatedRate" type="{http://www.opentravel.org/OTA/2003/05}TourActivityNegotiatedPricing" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Schedule" maxOccurs="9" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="StartTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;choice&amp;gt;
     *           &amp;lt;element name="ParticipantCount" maxOccurs="unbounded"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;complexContent&amp;gt;
     *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantCategoryType"&amp;gt;
     *                   &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *                 &amp;lt;/extension&amp;gt;
     *               &amp;lt;/complexContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *           &amp;lt;element name="GroupInfo"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;complexContent&amp;gt;
     *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                   &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                   &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                   &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                   &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                   &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                   &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;/restriction&amp;gt;
     *               &amp;lt;/complexContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *         &amp;lt;/choice&amp;gt;
     *         &amp;lt;element name="MultimodalOffer" type="{http://www.opentravel.org/OTA/2003/05}MultiModalOfferType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "basicInfo",
        "accessibilityRequirement",
        "discount",
        "location",
        "negotiatedRate",
        "schedule",
        "participantCount",
        "groupInfo",
        "multimodalOffer"
    })
    public static class TourActivity {

        @XmlElement(name = "BasicInfo", required = true)
        protected List<OTATourActivityAvailRQ.TourActivity.BasicInfo> basicInfo;
        @XmlElement(name = "AccessibilityRequirement")
        protected TourActivityAccessibilityReqType accessibilityRequirement;
        @XmlElement(name = "Discount")
        protected List<OTATourActivityAvailRQ.TourActivity.Discount> discount;
        @XmlElement(name = "Location")
        protected List<TourActivityLocationType> location;
        @XmlElement(name = "NegotiatedRate")
        protected List<TourActivityNegotiatedPricing> negotiatedRate;
        @XmlElement(name = "Schedule")
        protected List<OTATourActivityAvailRQ.TourActivity.Schedule> schedule;
        @XmlElement(name = "ParticipantCount")
        protected List<OTATourActivityAvailRQ.TourActivity.ParticipantCount> participantCount;
        @XmlElement(name = "GroupInfo")
        protected OTATourActivityAvailRQ.TourActivity.GroupInfo groupInfo;
        @XmlElement(name = "MultimodalOffer")
        protected MultiModalOfferType multimodalOffer;

        /**
         * Gets the value of the basicInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the basicInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getBasicInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRQ.TourActivity.BasicInfo }
         * 
         * 
         */
        public List<OTATourActivityAvailRQ.TourActivity.BasicInfo> getBasicInfo() {
            if (basicInfo == null) {
                basicInfo = new ArrayList<OTATourActivityAvailRQ.TourActivity.BasicInfo>();
            }
            return this.basicInfo;
        }

        /**
         * Obtiene el valor de la propiedad accessibilityRequirement.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityAccessibilityReqType }
         *     
         */
        public TourActivityAccessibilityReqType getAccessibilityRequirement() {
            return accessibilityRequirement;
        }

        /**
         * Define el valor de la propiedad accessibilityRequirement.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityAccessibilityReqType }
         *     
         */
        public void setAccessibilityRequirement(TourActivityAccessibilityReqType value) {
            this.accessibilityRequirement = value;
        }

        /**
         * Gets the value of the discount property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discount property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDiscount().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRQ.TourActivity.Discount }
         * 
         * 
         */
        public List<OTATourActivityAvailRQ.TourActivity.Discount> getDiscount() {
            if (discount == null) {
                discount = new ArrayList<OTATourActivityAvailRQ.TourActivity.Discount>();
            }
            return this.discount;
        }

        /**
         * Gets the value of the location property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the location property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLocation().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityLocationType }
         * 
         * 
         */
        public List<TourActivityLocationType> getLocation() {
            if (location == null) {
                location = new ArrayList<TourActivityLocationType>();
            }
            return this.location;
        }

        /**
         * Gets the value of the negotiatedRate property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the negotiatedRate property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getNegotiatedRate().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityNegotiatedPricing }
         * 
         * 
         */
        public List<TourActivityNegotiatedPricing> getNegotiatedRate() {
            if (negotiatedRate == null) {
                negotiatedRate = new ArrayList<TourActivityNegotiatedPricing>();
            }
            return this.negotiatedRate;
        }

        /**
         * Gets the value of the schedule property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the schedule property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSchedule().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRQ.TourActivity.Schedule }
         * 
         * 
         */
        public List<OTATourActivityAvailRQ.TourActivity.Schedule> getSchedule() {
            if (schedule == null) {
                schedule = new ArrayList<OTATourActivityAvailRQ.TourActivity.Schedule>();
            }
            return this.schedule;
        }

        /**
         * Gets the value of the participantCount property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantCount property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getParticipantCount().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivityAvailRQ.TourActivity.ParticipantCount }
         * 
         * 
         */
        public List<OTATourActivityAvailRQ.TourActivity.ParticipantCount> getParticipantCount() {
            if (participantCount == null) {
                participantCount = new ArrayList<OTATourActivityAvailRQ.TourActivity.ParticipantCount>();
            }
            return this.participantCount;
        }

        /**
         * Obtiene el valor de la propiedad groupInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityAvailRQ.TourActivity.GroupInfo }
         *     
         */
        public OTATourActivityAvailRQ.TourActivity.GroupInfo getGroupInfo() {
            return groupInfo;
        }

        /**
         * Define el valor de la propiedad groupInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityAvailRQ.TourActivity.GroupInfo }
         *     
         */
        public void setGroupInfo(OTATourActivityAvailRQ.TourActivity.GroupInfo value) {
            this.groupInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad multimodalOffer.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType }
         *     
         */
        public MultiModalOfferType getMultimodalOffer() {
            return multimodalOffer;
        }

        /**
         * Define el valor de la propiedad multimodalOffer.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType }
         *     
         */
        public void setMultimodalOffer(MultiModalOfferType value) {
            this.multimodalOffer = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class BasicInfo
            extends TourActivityIDType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PromotionCode" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Program" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionCode",
            "program"
        })
        public static class Discount {

            @XmlElement(name = "PromotionCode")
            protected List<OTATourActivityAvailRQ.TourActivity.Discount.PromotionCode> promotionCode;
            @XmlElement(name = "Program")
            protected OTATourActivityAvailRQ.TourActivity.Discount.Program program;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Gets the value of the promotionCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotionCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityAvailRQ.TourActivity.Discount.PromotionCode }
             * 
             * 
             */
            public List<OTATourActivityAvailRQ.TourActivity.Discount.PromotionCode> getPromotionCode() {
                if (promotionCode == null) {
                    promotionCode = new ArrayList<OTATourActivityAvailRQ.TourActivity.Discount.PromotionCode>();
                }
                return this.promotionCode;
            }

            /**
             * Obtiene el valor de la propiedad program.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityAvailRQ.TourActivity.Discount.Program }
             *     
             */
            public OTATourActivityAvailRQ.TourActivity.Discount.Program getProgram() {
                return program;
            }

            /**
             * Define el valor de la propiedad program.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityAvailRQ.TourActivity.Discount.Program }
             *     
             */
            public void setProgram(OTATourActivityAvailRQ.TourActivity.Discount.Program value) {
                this.program = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "provider"
            })
            public static class Program {

                @XmlElement(name = "Provider")
                protected CompanyNameType provider;
                @XmlAttribute(name = "Name")
                protected String name;
                @XmlAttribute(name = "Description")
                protected String description;

                /**
                 * Obtiene el valor de la propiedad provider.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public CompanyNameType getProvider() {
                    return provider;
                }

                /**
                 * Define el valor de la propiedad provider.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public void setProvider(CompanyNameType value) {
                    this.provider = value;
                }

                /**
                 * Obtiene el valor de la propiedad name.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Define el valor de la propiedad name.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class PromotionCode {

                @XmlValue
                protected String value;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class GroupInfo {

            @XmlAttribute(name = "GroupName")
            protected String groupName;
            @XmlAttribute(name = "GroupID")
            protected String groupID;
            @XmlAttribute(name = "MinGroupSize")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger minGroupSize;
            @XmlAttribute(name = "MaxGroupSize")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger maxGroupSize;
            @XmlAttribute(name = "KnownGroupSize")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger knownGroupSize;
            @XmlAttribute(name = "GroupCode")
            protected String groupCode;

            /**
             * Obtiene el valor de la propiedad groupName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupName() {
                return groupName;
            }

            /**
             * Define el valor de la propiedad groupName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupName(String value) {
                this.groupName = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }

            /**
             * Obtiene el valor de la propiedad minGroupSize.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMinGroupSize() {
                return minGroupSize;
            }

            /**
             * Define el valor de la propiedad minGroupSize.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMinGroupSize(BigInteger value) {
                this.minGroupSize = value;
            }

            /**
             * Obtiene el valor de la propiedad maxGroupSize.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxGroupSize() {
                return maxGroupSize;
            }

            /**
             * Define el valor de la propiedad maxGroupSize.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxGroupSize(BigInteger value) {
                this.maxGroupSize = value;
            }

            /**
             * Obtiene el valor de la propiedad knownGroupSize.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getKnownGroupSize() {
                return knownGroupSize;
            }

            /**
             * Define el valor de la propiedad knownGroupSize.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setKnownGroupSize(BigInteger value) {
                this.knownGroupSize = value;
            }

            /**
             * Obtiene el valor de la propiedad groupCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupCode() {
                return groupCode;
            }

            /**
             * Define el valor de la propiedad groupCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupCode(String value) {
                this.groupCode = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantCategoryType"&amp;gt;
         *       &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ParticipantCount
            extends TourActivityParticipantCategoryType
        {

            @XmlAttribute(name = "FreeChildrenQty")
            protected Integer freeChildrenQty;

            /**
             * Obtiene el valor de la propiedad freeChildrenQty.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getFreeChildrenQty() {
                return freeChildrenQty;
            }

            /**
             * Define el valor de la propiedad freeChildrenQty.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setFreeChildrenQty(Integer value) {
                this.freeChildrenQty = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="StartTime" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "startTime"
        })
        public static class Schedule {

            @XmlElement(name = "StartTime")
            protected List<String> startTime;
            @XmlAttribute(name = "StartPeriod")
            protected String startPeriod;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "EndPeriod")
            protected String endPeriod;

            /**
             * Gets the value of the startTime property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the startTime property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getStartTime().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getStartTime() {
                if (startTime == null) {
                    startTime = new ArrayList<String>();
                }
                return this.startTime;
            }

            /**
             * Obtiene el valor de la propiedad startPeriod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStartPeriod() {
                return startPeriod;
            }

            /**
             * Define el valor de la propiedad startPeriod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStartPeriod(String value) {
                this.startPeriod = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad endPeriod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndPeriod() {
                return endPeriod;
            }

            /**
             * Define el valor de la propiedad endPeriod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndPeriod(String value) {
                this.endPeriod = value;
            }

        }

    }

}
