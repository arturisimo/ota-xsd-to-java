
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para VehiclePeriodUnitNameType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="VehiclePeriodUnitNameType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="RentalPeriod"/&amp;gt;
 *     &amp;lt;enumeration value="Year"/&amp;gt;
 *     &amp;lt;enumeration value="Month"/&amp;gt;
 *     &amp;lt;enumeration value="Week"/&amp;gt;
 *     &amp;lt;enumeration value="Day"/&amp;gt;
 *     &amp;lt;enumeration value="Hour"/&amp;gt;
 *     &amp;lt;enumeration value="Weekend"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraMonth"/&amp;gt;
 *     &amp;lt;enumeration value="Bundle"/&amp;gt;
 *     &amp;lt;enumeration value="Package"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraDay"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraHour"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraWeek"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "VehiclePeriodUnitNameType")
@XmlEnum
public enum VehiclePeriodUnitNameType {

    @XmlEnumValue("RentalPeriod")
    RENTAL_PERIOD("RentalPeriod"),
    @XmlEnumValue("Year")
    YEAR("Year"),
    @XmlEnumValue("Month")
    MONTH("Month"),
    @XmlEnumValue("Week")
    WEEK("Week"),
    @XmlEnumValue("Day")
    DAY("Day"),
    @XmlEnumValue("Hour")
    HOUR("Hour"),

    /**
     * The calculation is for each weekend.
     * 
     */
    @XmlEnumValue("Weekend")
    WEEKEND("Weekend"),

    /**
     * The charge is based on an extra month.
     * 
     */
    @XmlEnumValue("ExtraMonth")
    EXTRA_MONTH("ExtraMonth"),

    /**
     * The rate is the same regardless of the number of days the vehicle is rented.
     * 
     */
    @XmlEnumValue("Bundle")
    BUNDLE("Bundle"),

    /**
     * The charge is based on the package.
     * 
     */
    @XmlEnumValue("Package")
    PACKAGE("Package"),

    /**
     * The charge is based on an extra day.
     * 
     */
    @XmlEnumValue("ExtraDay")
    EXTRA_DAY("ExtraDay"),

    /**
     * The charge is based on an extra hour.
     * 
     */
    @XmlEnumValue("ExtraHour")
    EXTRA_HOUR("ExtraHour"),

    /**
     * The charge is based on an extra week.
     * 
     */
    @XmlEnumValue("ExtraWeek")
    EXTRA_WEEK("ExtraWeek");
    private final String value;

    VehiclePeriodUnitNameType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VehiclePeriodUnitNameType fromValue(String v) {
        for (VehiclePeriodUnitNameType c: VehiclePeriodUnitNameType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
