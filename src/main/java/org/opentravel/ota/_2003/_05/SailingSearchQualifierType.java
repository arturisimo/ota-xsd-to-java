
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Search qualifiers that can be used to identify a cruise.
 * 
 * &lt;p&gt;Clase Java para SailingSearchQualifierType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="SailingSearchQualifierType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SearchQualifierType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Port" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PortInfoGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SailingSearchQualifierType", propOrder = {
    "port"
})
public class SailingSearchQualifierType
    extends SearchQualifierType
{

    @XmlElement(name = "Port")
    protected SailingSearchQualifierType.Port port;

    /**
     * Obtiene el valor de la propiedad port.
     * 
     * @return
     *     possible object is
     *     {@link SailingSearchQualifierType.Port }
     *     
     */
    public SailingSearchQualifierType.Port getPort() {
        return port;
    }

    /**
     * Define el valor de la propiedad port.
     * 
     * @param value
     *     allowed object is
     *     {@link SailingSearchQualifierType.Port }
     *     
     */
    public void setPort(SailingSearchQualifierType.Port value) {
        this.port = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PortInfoGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Port {

        @XmlAttribute(name = "PortCode")
        protected String portCode;
        @XmlAttribute(name = "PortName")
        protected String portName;
        @XmlAttribute(name = "PortCountryCode")
        protected String portCountryCode;
        @XmlAttribute(name = "DockIndicator")
        protected Boolean dockIndicator;
        @XmlAttribute(name = "ShorexIndicator")
        protected Boolean shorexIndicator;
        @XmlAttribute(name = "EmbarkIndicator")
        protected Boolean embarkIndicator;
        @XmlAttribute(name = "DisembarkIndicator")
        protected Boolean disembarkIndicator;

        /**
         * Obtiene el valor de la propiedad portCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPortCode() {
            return portCode;
        }

        /**
         * Define el valor de la propiedad portCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPortCode(String value) {
            this.portCode = value;
        }

        /**
         * Obtiene el valor de la propiedad portName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPortName() {
            return portName;
        }

        /**
         * Define el valor de la propiedad portName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPortName(String value) {
            this.portName = value;
        }

        /**
         * Obtiene el valor de la propiedad portCountryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPortCountryCode() {
            return portCountryCode;
        }

        /**
         * Define el valor de la propiedad portCountryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPortCountryCode(String value) {
            this.portCountryCode = value;
        }

        /**
         * Obtiene el valor de la propiedad dockIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDockIndicator() {
            return dockIndicator;
        }

        /**
         * Define el valor de la propiedad dockIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDockIndicator(Boolean value) {
            this.dockIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad shorexIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isShorexIndicator() {
            return shorexIndicator;
        }

        /**
         * Define el valor de la propiedad shorexIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setShorexIndicator(Boolean value) {
            this.shorexIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad embarkIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEmbarkIndicator() {
            return embarkIndicator;
        }

        /**
         * Define el valor de la propiedad embarkIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEmbarkIndicator(Boolean value) {
            this.embarkIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad disembarkIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDisembarkIndicator() {
            return disembarkIndicator;
        }

        /**
         * Define el valor de la propiedad disembarkIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDisembarkIndicator(Boolean value) {
            this.disembarkIndicator = value;
        }

    }

}
