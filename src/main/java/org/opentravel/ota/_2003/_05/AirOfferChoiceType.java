
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A choice between specifying summary, priced or traveler purchased ancillary offers.
 * 
 * &lt;p&gt;Clase Java para AirOfferChoiceType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirOfferChoiceType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;element name="Summary" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirOfferType"&amp;gt;
 *                 &amp;lt;attribute name="FlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ItineraryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="OrigDestRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Priced" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirPricedOfferType"&amp;gt;
 *                 &amp;lt;attribute name="FlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ItineraryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="OrigDestRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Purchased" type="{http://www.opentravel.org/OTA/2003/05}AirPurchasedOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirOfferChoiceType", propOrder = {
    "summary",
    "priced",
    "purchased"
})
public class AirOfferChoiceType {

    @XmlElement(name = "Summary")
    protected List<AirOfferChoiceType.Summary> summary;
    @XmlElement(name = "Priced")
    protected List<AirOfferChoiceType.Priced> priced;
    @XmlElement(name = "Purchased")
    protected List<AirPurchasedOfferType> purchased;

    /**
     * Gets the value of the summary property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the summary property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSummary().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirOfferChoiceType.Summary }
     * 
     * 
     */
    public List<AirOfferChoiceType.Summary> getSummary() {
        if (summary == null) {
            summary = new ArrayList<AirOfferChoiceType.Summary>();
        }
        return this.summary;
    }

    /**
     * Gets the value of the priced property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the priced property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPriced().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirOfferChoiceType.Priced }
     * 
     * 
     */
    public List<AirOfferChoiceType.Priced> getPriced() {
        if (priced == null) {
            priced = new ArrayList<AirOfferChoiceType.Priced>();
        }
        return this.priced;
    }

    /**
     * Gets the value of the purchased property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the purchased property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPurchased().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPurchasedOfferType }
     * 
     * 
     */
    public List<AirPurchasedOfferType> getPurchased() {
        if (purchased == null) {
            purchased = new ArrayList<AirPurchasedOfferType>();
        }
        return this.purchased;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirPricedOfferType"&amp;gt;
     *       &amp;lt;attribute name="FlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ItineraryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="OrigDestRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Priced
        extends AirPricedOfferType
    {

        @XmlAttribute(name = "FlightSegmentRPH")
        protected String flightSegmentRPH;
        @XmlAttribute(name = "ItineraryRPH")
        protected String itineraryRPH;
        @XmlAttribute(name = "OrigDestRPH")
        protected String origDestRPH;
        @XmlAttribute(name = "TravelerRPH")
        protected String travelerRPH;

        /**
         * Obtiene el valor de la propiedad flightSegmentRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightSegmentRPH() {
            return flightSegmentRPH;
        }

        /**
         * Define el valor de la propiedad flightSegmentRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightSegmentRPH(String value) {
            this.flightSegmentRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad itineraryRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItineraryRPH() {
            return itineraryRPH;
        }

        /**
         * Define el valor de la propiedad itineraryRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItineraryRPH(String value) {
            this.itineraryRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad origDestRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigDestRPH() {
            return origDestRPH;
        }

        /**
         * Define el valor de la propiedad origDestRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigDestRPH(String value) {
            this.origDestRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelerRPH() {
            return travelerRPH;
        }

        /**
         * Define el valor de la propiedad travelerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelerRPH(String value) {
            this.travelerRPH = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirOfferType"&amp;gt;
     *       &amp;lt;attribute name="FlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ItineraryRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="OrigDestRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Summary
        extends AirOfferType
    {

        @XmlAttribute(name = "FlightSegmentRPH")
        protected String flightSegmentRPH;
        @XmlAttribute(name = "ItineraryRPH")
        protected String itineraryRPH;
        @XmlAttribute(name = "OrigDestRPH")
        protected String origDestRPH;
        @XmlAttribute(name = "TravelerRPH")
        protected String travelerRPH;

        /**
         * Obtiene el valor de la propiedad flightSegmentRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightSegmentRPH() {
            return flightSegmentRPH;
        }

        /**
         * Define el valor de la propiedad flightSegmentRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightSegmentRPH(String value) {
            this.flightSegmentRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad itineraryRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItineraryRPH() {
            return itineraryRPH;
        }

        /**
         * Define el valor de la propiedad itineraryRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItineraryRPH(String value) {
            this.itineraryRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad origDestRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigDestRPH() {
            return origDestRPH;
        }

        /**
         * Define el valor de la propiedad origDestRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigDestRPH(String value) {
            this.origDestRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelerRPH() {
            return travelerRPH;
        }

        /**
         * Define el valor de la propiedad travelerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelerRPH(String value) {
            this.travelerRPH = value;
        }

    }

}
