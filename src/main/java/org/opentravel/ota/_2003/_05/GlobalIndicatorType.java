
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para GlobalIndicatorType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="GlobalIndicatorType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AP"/&amp;gt;
 *     &amp;lt;enumeration value="AT"/&amp;gt;
 *     &amp;lt;enumeration value="CT"/&amp;gt;
 *     &amp;lt;enumeration value="DO"/&amp;gt;
 *     &amp;lt;enumeration value="EH"/&amp;gt;
 *     &amp;lt;enumeration value="FE"/&amp;gt;
 *     &amp;lt;enumeration value="PA"/&amp;gt;
 *     &amp;lt;enumeration value="PN"/&amp;gt;
 *     &amp;lt;enumeration value="PO"/&amp;gt;
 *     &amp;lt;enumeration value="RU"/&amp;gt;
 *     &amp;lt;enumeration value="RW"/&amp;gt;
 *     &amp;lt;enumeration value="SA"/&amp;gt;
 *     &amp;lt;enumeration value="TS"/&amp;gt;
 *     &amp;lt;enumeration value="WH"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "GlobalIndicatorType")
@XmlEnum
public enum GlobalIndicatorType {


    /**
     * Atlantic/Pacific Round-the-World
     * 
     */
    AP,

    /**
     * Atlantic Ocean
     * 
     */
    AT,

    /**
     * Circle trip
     * 
     */
    CT,

    /**
     * Domestic
     * 
     */
    DO,

    /**
     * Eastern Hemisphere
     * 
     */
    EH,

    /**
     * Within the Far East
     * 
     */
    FE,

    /**
     * Pacific Ocean
     * 
     */
    PA,

    /**
     * TC1-TC3 via Pacific/N. America
     * 
     */
    PN,

    /**
     * Polar Route
     * 
     */
    PO,

    /**
     * Russia Area 3
     * 
     */
    RU,

    /**
     * Round the world
     * 
     */
    RW,

    /**
     * South Atlantic only
     * 
     */
    SA,

    /**
     * Trans Siberia Route
     * 
     */
    TS,

    /**
     * Western Hemisphere
     * 
     */
    WH;

    public String value() {
        return name();
    }

    public static GlobalIndicatorType fromValue(String v) {
        return valueOf(v);
    }

}
