
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies fare option info.
 * 
 * &lt;p&gt;Clase Java para FareCodeOptionType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FareCodeOptionType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FareRemark" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FareGroup"/&amp;gt;
 *       &amp;lt;attribute name="ListOfFareQualifierCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="FareDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareCodeOptionType", propOrder = {
    "fareRemark"
})
public class FareCodeOptionType {

    @XmlElement(name = "FareRemark")
    protected FareCodeOptionType.FareRemark fareRemark;
    @XmlAttribute(name = "ListOfFareQualifierCode")
    protected List<String> listOfFareQualifierCode;
    @XmlAttribute(name = "Status")
    protected String status;
    @XmlAttribute(name = "FareDescription")
    protected String fareDescription;
    @XmlAttribute(name = "FareCode")
    protected String fareCode;
    @XmlAttribute(name = "GroupCode")
    protected String groupCode;

    /**
     * Obtiene el valor de la propiedad fareRemark.
     * 
     * @return
     *     possible object is
     *     {@link FareCodeOptionType.FareRemark }
     *     
     */
    public FareCodeOptionType.FareRemark getFareRemark() {
        return fareRemark;
    }

    /**
     * Define el valor de la propiedad fareRemark.
     * 
     * @param value
     *     allowed object is
     *     {@link FareCodeOptionType.FareRemark }
     *     
     */
    public void setFareRemark(FareCodeOptionType.FareRemark value) {
        this.fareRemark = value;
    }

    /**
     * Gets the value of the listOfFareQualifierCode property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the listOfFareQualifierCode property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getListOfFareQualifierCode().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getListOfFareQualifierCode() {
        if (listOfFareQualifierCode == null) {
            listOfFareQualifierCode = new ArrayList<String>();
        }
        return this.listOfFareQualifierCode;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad fareDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareDescription() {
        return fareDescription;
    }

    /**
     * Define el valor de la propiedad fareDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareDescription(String value) {
        this.fareDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad fareCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareCode() {
        return fareCode;
    }

    /**
     * Define el valor de la propiedad fareCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareCode(String value) {
        this.fareCode = value;
    }

    /**
     * Obtiene el valor de la propiedad groupCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * Define el valor de la propiedad groupCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupCode(String value) {
        this.groupCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FareRemark
        extends FreeTextType
    {


    }

}
