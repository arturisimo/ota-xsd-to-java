
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para AirComponentSearchType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AirComponentSearchType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="LowestFare"/&amp;gt;
 *     &amp;lt;enumeration value="Schedule"/&amp;gt;
 *     &amp;lt;enumeration value="PriceFlights"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AirComponentSearchType")
@XmlEnum
public enum AirComponentSearchType {

    @XmlEnumValue("LowestFare")
    LOWEST_FARE("LowestFare"),
    @XmlEnumValue("Schedule")
    SCHEDULE("Schedule"),
    @XmlEnumValue("PriceFlights")
    PRICE_FLIGHTS("PriceFlights");
    private final String value;

    AirComponentSearchType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AirComponentSearchType fromValue(String v) {
        for (AirComponentSearchType c: AirComponentSearchType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
