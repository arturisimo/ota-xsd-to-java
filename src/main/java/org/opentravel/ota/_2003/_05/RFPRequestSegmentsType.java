
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides request data for an event.
 * 
 * &lt;p&gt;Clase Java para RFP_RequestSegmentsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RFP_RequestSegmentsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RFP_RequestSegment" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ProposalSpecification" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="KeyDecisionFactors" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="KeyDecisionFactor" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                               &amp;lt;attribute name="Ranking" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Presentation" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="CustomQuestions" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="CustomQuestion" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProposalGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Dates"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Date" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
 *                                     &amp;lt;attribute name="DateType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Primary"/&amp;gt;
 *                                           &amp;lt;enumeration value="Alternate"/&amp;gt;
 *                                           &amp;lt;enumeration value="Published"/&amp;gt;
 *                                           &amp;lt;enumeration value="Set-up"/&amp;gt;
 *                                           &amp;lt;enumeration value="Breakdown"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="PromotionRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Sites" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Promotions" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Promotion" maxOccurs="99"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="AvailableDateRange"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
 *                                                                   &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                                 &amp;lt;/extension&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;element name="OfferedDateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
 *                                                           &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                                                         &amp;lt;attribute name="UserAcknowledgementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                                         &amp;lt;attribute name="AdvertisedRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                         &amp;lt;attribute name="AdvertisedHighAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                         &amp;lt;attribute name="AdvertisedLowAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="ApproxSiteInspectionDate" type="{http://www.w3.org/2001/XMLSchema}gYearMonth" /&amp;gt;
 *                                     &amp;lt;attribute name="SiteInspectionAttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="BudgetedRoomRate" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="RequestedRateType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ReservationMethod" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="StayDays" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="StayDayRooms" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="StayDayRoom" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                   &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="ConcessionsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="FunctionSpaceReq" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="LargestRoomSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="MeetingRoomFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="MinOccupancy" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="CeilingHeight" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                     &amp;lt;attribute name="UnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="MinBreakoutRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="EventBudget" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="BudgetType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Total"/&amp;gt;
 *                                           &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
 *                                           &amp;lt;enumeration value="AudioVisual"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="EventDays" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="EventDay" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence minOccurs="0"&amp;gt;
 *                                                                     &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
 *                                                                                 &amp;lt;complexType&amp;gt;
 *                                                                                   &amp;lt;complexContent&amp;gt;
 *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                       &amp;lt;sequence&amp;gt;
 *                                                                                         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
 *                                                                                       &amp;lt;/sequence&amp;gt;
 *                                                                                       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                                     &amp;lt;/restriction&amp;gt;
 *                                                                                   &amp;lt;/complexContent&amp;gt;
 *                                                                                 &amp;lt;/complexType&amp;gt;
 *                                                                               &amp;lt;/element&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                     &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                   &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                   &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                   &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                   &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                   &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                   &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="FirstEventDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Exhibition" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="ExhibitDetail" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Type"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Public"/&amp;gt;
 *                                           &amp;lt;enumeration value="Private"/&amp;gt;
 *                                           &amp;lt;enumeration value="PublicPrivate"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="KitDistributionCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="KitFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="GeneralServiceContractorInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="GrossExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="NetExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="GrossUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="NetUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="ExhibitQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="CompanyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="SecuredAreaIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="MarketSegmentCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="DefiniteIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="Frequency" type="{http://www.opentravel.org/OTA/2003/05}FrequencyType" /&amp;gt;
 *                           &amp;lt;attribute name="ExpectedTotalAttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="FirstTimeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="FutureOpenDatesIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="ExhibitionIncludedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Preferences" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Preference" maxOccurs="5"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;simpleContent&amp;gt;
 *                                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                                                         &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/simpleContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Recreations" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Recreation" maxOccurs="unbounded"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                                                         &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Properties" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Property" maxOccurs="99"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                                                         &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Histories" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="History" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="DaySummaries" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="DaySummary" maxOccurs="unbounded"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                                                         &amp;lt;attribute name="DayNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="PostEventReportExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="RFP_ID" maxOccurs="9" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                           &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RoomBlockIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EventBlockIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProposalGroup"/&amp;gt;
 *       &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RFP_RequestSegmentsType", propOrder = {
    "rfpRequestSegment"
})
public class RFPRequestSegmentsType {

    @XmlElement(name = "RFP_RequestSegment", required = true)
    protected List<RFPRequestSegmentsType.RFPRequestSegment> rfpRequestSegment;
    @XmlAttribute(name = "AttendeeQuantity")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger attendeeQuantity;
    @XmlAttribute(name = "ResponseDue")
    protected String responseDue;
    @XmlAttribute(name = "DecisionDue")
    protected String decisionDue;
    @XmlAttribute(name = "ResponseFormat")
    protected String responseFormat;
    @XmlAttribute(name = "ResponseLanguage")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String responseLanguage;
    @XmlAttribute(name = "PreliminaryCutIndicator")
    protected Boolean preliminaryCutIndicator;
    @XmlAttribute(name = "PreliminaryCutDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar preliminaryCutDate;
    @XmlAttribute(name = "RFP_DistributionDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar rfpDistributionDate;
    @XmlAttribute(name = "RFP_PublishedDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar rfpPublishedDate;
    @XmlAttribute(name = "RFP_CreationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar rfpCreationDate;

    /**
     * Gets the value of the rfpRequestSegment property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rfpRequestSegment property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRFPRequestSegment().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RFPRequestSegmentsType.RFPRequestSegment }
     * 
     * 
     */
    public List<RFPRequestSegmentsType.RFPRequestSegment> getRFPRequestSegment() {
        if (rfpRequestSegment == null) {
            rfpRequestSegment = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment>();
        }
        return this.rfpRequestSegment;
    }

    /**
     * Obtiene el valor de la propiedad attendeeQuantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAttendeeQuantity() {
        return attendeeQuantity;
    }

    /**
     * Define el valor de la propiedad attendeeQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAttendeeQuantity(BigInteger value) {
        this.attendeeQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad responseDue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDue() {
        return responseDue;
    }

    /**
     * Define el valor de la propiedad responseDue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDue(String value) {
        this.responseDue = value;
    }

    /**
     * Obtiene el valor de la propiedad decisionDue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecisionDue() {
        return decisionDue;
    }

    /**
     * Define el valor de la propiedad decisionDue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecisionDue(String value) {
        this.decisionDue = value;
    }

    /**
     * Obtiene el valor de la propiedad responseFormat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseFormat() {
        return responseFormat;
    }

    /**
     * Define el valor de la propiedad responseFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseFormat(String value) {
        this.responseFormat = value;
    }

    /**
     * Obtiene el valor de la propiedad responseLanguage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseLanguage() {
        return responseLanguage;
    }

    /**
     * Define el valor de la propiedad responseLanguage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseLanguage(String value) {
        this.responseLanguage = value;
    }

    /**
     * Obtiene el valor de la propiedad preliminaryCutIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreliminaryCutIndicator() {
        return preliminaryCutIndicator;
    }

    /**
     * Define el valor de la propiedad preliminaryCutIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreliminaryCutIndicator(Boolean value) {
        this.preliminaryCutIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad preliminaryCutDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPreliminaryCutDate() {
        return preliminaryCutDate;
    }

    /**
     * Define el valor de la propiedad preliminaryCutDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPreliminaryCutDate(XMLGregorianCalendar value) {
        this.preliminaryCutDate = value;
    }

    /**
     * Obtiene el valor de la propiedad rfpDistributionDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRFPDistributionDate() {
        return rfpDistributionDate;
    }

    /**
     * Define el valor de la propiedad rfpDistributionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRFPDistributionDate(XMLGregorianCalendar value) {
        this.rfpDistributionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad rfpPublishedDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRFPPublishedDate() {
        return rfpPublishedDate;
    }

    /**
     * Define el valor de la propiedad rfpPublishedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRFPPublishedDate(XMLGregorianCalendar value) {
        this.rfpPublishedDate = value;
    }

    /**
     * Obtiene el valor de la propiedad rfpCreationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRFPCreationDate() {
        return rfpCreationDate;
    }

    /**
     * Define el valor de la propiedad rfpCreationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRFPCreationDate(XMLGregorianCalendar value) {
        this.rfpCreationDate = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ProposalSpecification" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="KeyDecisionFactors" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="KeyDecisionFactor" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                     &amp;lt;attribute name="Ranking" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Presentation" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="CustomQuestions" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="CustomQuestion" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProposalGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Dates"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Date" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
     *                           &amp;lt;attribute name="DateType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Primary"/&amp;gt;
     *                                 &amp;lt;enumeration value="Alternate"/&amp;gt;
     *                                 &amp;lt;enumeration value="Published"/&amp;gt;
     *                                 &amp;lt;enumeration value="Set-up"/&amp;gt;
     *                                 &amp;lt;enumeration value="Breakdown"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="PromotionRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Sites" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Promotions" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Promotion" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="AvailableDateRange"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
     *                                                         &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="OfferedDateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
     *                                                 &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="UserAcknowledgementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                               &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                               &amp;lt;attribute name="AdvertisedRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                               &amp;lt;attribute name="AdvertisedHighAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                               &amp;lt;attribute name="AdvertisedLowAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *                           &amp;lt;attribute name="ApproxSiteInspectionDate" type="{http://www.w3.org/2001/XMLSchema}gYearMonth" /&amp;gt;
     *                           &amp;lt;attribute name="SiteInspectionAttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="RoomBlock" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="BudgetedRoomRate" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
     *                           &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="RequestedRateType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ReservationMethod" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="StayDays" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="StayDayRooms" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="StayDayRoom" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                         &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="ConcessionsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="EventBlock" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="FunctionSpaceReq" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="LargestRoomSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MeetingRoomFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="MinOccupancy" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="CeilingHeight" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                           &amp;lt;attribute name="UnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="MinBreakoutRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="EventBudget" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                           &amp;lt;attribute name="BudgetType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Total"/&amp;gt;
     *                                 &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
     *                                 &amp;lt;enumeration value="AudioVisual"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="EventDays" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="EventDay" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence minOccurs="0"&amp;gt;
     *                                                           &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
     *                                                                       &amp;lt;complexType&amp;gt;
     *                                                                         &amp;lt;complexContent&amp;gt;
     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                             &amp;lt;sequence&amp;gt;
     *                                                                               &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
     *                                                                             &amp;lt;/sequence&amp;gt;
     *                                                                             &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                             &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
     *                                                                             &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                         &amp;lt;/complexContent&amp;gt;
     *                                                                       &amp;lt;/complexType&amp;gt;
     *                                                                     &amp;lt;/element&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                         &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                         &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                         &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                         &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="FirstEventDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Exhibition" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ExhibitDetail" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Type"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Public"/&amp;gt;
     *                                 &amp;lt;enumeration value="Private"/&amp;gt;
     *                                 &amp;lt;enumeration value="PublicPrivate"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="KitDistributionCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="KitFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="GeneralServiceContractorInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="GrossExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="NetExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="GrossUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="NetUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="ExhibitQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="CompanyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="SecuredAreaIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="MarketSegmentCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="DefiniteIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="Frequency" type="{http://www.opentravel.org/OTA/2003/05}FrequencyType" /&amp;gt;
     *                 &amp;lt;attribute name="ExpectedTotalAttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="FirstTimeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="FutureOpenDatesIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ExhibitionIncludedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Preferences" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Preference" maxOccurs="5"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Recreations" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Recreation" maxOccurs="unbounded"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Properties" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Property" maxOccurs="99"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Histories" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="History" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="DaySummaries" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="DaySummary" maxOccurs="unbounded"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="DayNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="PostEventReportExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="RFP_ID" maxOccurs="9" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                 &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RoomBlockIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EventBlockIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "proposalSpecification",
        "profiles",
        "dates",
        "sites",
        "roomBlock",
        "eventBlock",
        "preferences",
        "histories",
        "comments",
        "rfpid",
        "additionalInfos",
        "tpaExtensions"
    })
    public static class RFPRequestSegment {

        @XmlElement(name = "ProposalSpecification")
        protected RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification proposalSpecification;
        @XmlElement(name = "Profiles")
        protected ProfilesType profiles;
        @XmlElement(name = "Dates", required = true)
        protected RFPRequestSegmentsType.RFPRequestSegment.Dates dates;
        @XmlElement(name = "Sites")
        protected RFPRequestSegmentsType.RFPRequestSegment.Sites sites;
        @XmlElement(name = "RoomBlock")
        protected RFPRequestSegmentsType.RFPRequestSegment.RoomBlock roomBlock;
        @XmlElement(name = "EventBlock")
        protected RFPRequestSegmentsType.RFPRequestSegment.EventBlock eventBlock;
        @XmlElement(name = "Preferences")
        protected RFPRequestSegmentsType.RFPRequestSegment.Preferences preferences;
        @XmlElement(name = "Histories")
        protected RFPRequestSegmentsType.RFPRequestSegment.Histories histories;
        @XmlElement(name = "Comments")
        protected List<ParagraphType> comments;
        @XmlElement(name = "RFP_ID")
        protected List<RFPRequestSegmentsType.RFPRequestSegment.RFPID> rfpid;
        @XmlElement(name = "AdditionalInfos")
        protected RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos additionalInfos;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;
        @XmlAttribute(name = "RoomBlockIndicator")
        protected Boolean roomBlockIndicator;
        @XmlAttribute(name = "EventBlockIndicator")
        protected Boolean eventBlockIndicator;

        /**
         * Obtiene el valor de la propiedad proposalSpecification.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification getProposalSpecification() {
            return proposalSpecification;
        }

        /**
         * Define el valor de la propiedad proposalSpecification.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification }
         *     
         */
        public void setProposalSpecification(RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification value) {
            this.proposalSpecification = value;
        }

        /**
         * Obtiene el valor de la propiedad profiles.
         * 
         * @return
         *     possible object is
         *     {@link ProfilesType }
         *     
         */
        public ProfilesType getProfiles() {
            return profiles;
        }

        /**
         * Define el valor de la propiedad profiles.
         * 
         * @param value
         *     allowed object is
         *     {@link ProfilesType }
         *     
         */
        public void setProfiles(ProfilesType value) {
            this.profiles = value;
        }

        /**
         * Obtiene el valor de la propiedad dates.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Dates }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.Dates getDates() {
            return dates;
        }

        /**
         * Define el valor de la propiedad dates.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Dates }
         *     
         */
        public void setDates(RFPRequestSegmentsType.RFPRequestSegment.Dates value) {
            this.dates = value;
        }

        /**
         * Obtiene el valor de la propiedad sites.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.Sites getSites() {
            return sites;
        }

        /**
         * Define el valor de la propiedad sites.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites }
         *     
         */
        public void setSites(RFPRequestSegmentsType.RFPRequestSegment.Sites value) {
            this.sites = value;
        }

        /**
         * Obtiene el valor de la propiedad roomBlock.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.RoomBlock getRoomBlock() {
            return roomBlock;
        }

        /**
         * Define el valor de la propiedad roomBlock.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock }
         *     
         */
        public void setRoomBlock(RFPRequestSegmentsType.RFPRequestSegment.RoomBlock value) {
            this.roomBlock = value;
        }

        /**
         * Obtiene el valor de la propiedad eventBlock.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.EventBlock getEventBlock() {
            return eventBlock;
        }

        /**
         * Define el valor de la propiedad eventBlock.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock }
         *     
         */
        public void setEventBlock(RFPRequestSegmentsType.RFPRequestSegment.EventBlock value) {
            this.eventBlock = value;
        }

        /**
         * Obtiene el valor de la propiedad preferences.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.Preferences getPreferences() {
            return preferences;
        }

        /**
         * Define el valor de la propiedad preferences.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences }
         *     
         */
        public void setPreferences(RFPRequestSegmentsType.RFPRequestSegment.Preferences value) {
            this.preferences = value;
        }

        /**
         * Obtiene el valor de la propiedad histories.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Histories }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.Histories getHistories() {
            return histories;
        }

        /**
         * Define el valor de la propiedad histories.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Histories }
         *     
         */
        public void setHistories(RFPRequestSegmentsType.RFPRequestSegment.Histories value) {
            this.histories = value;
        }

        /**
         * Gets the value of the comments property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comments property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getComments().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getComments() {
            if (comments == null) {
                comments = new ArrayList<ParagraphType>();
            }
            return this.comments;
        }

        /**
         * Gets the value of the rfpid property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rfpid property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRFPID().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RFPRequestSegmentsType.RFPRequestSegment.RFPID }
         * 
         * 
         */
        public List<RFPRequestSegmentsType.RFPRequestSegment.RFPID> getRFPID() {
            if (rfpid == null) {
                rfpid = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.RFPID>();
            }
            return this.rfpid;
        }

        /**
         * Obtiene el valor de la propiedad additionalInfos.
         * 
         * @return
         *     possible object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos }
         *     
         */
        public RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos getAdditionalInfos() {
            return additionalInfos;
        }

        /**
         * Define el valor de la propiedad additionalInfos.
         * 
         * @param value
         *     allowed object is
         *     {@link RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos }
         *     
         */
        public void setAdditionalInfos(RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos value) {
            this.additionalInfos = value;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

        /**
         * Obtiene el valor de la propiedad roomBlockIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRoomBlockIndicator() {
            return roomBlockIndicator;
        }

        /**
         * Define el valor de la propiedad roomBlockIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRoomBlockIndicator(Boolean value) {
            this.roomBlockIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad eventBlockIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEventBlockIndicator() {
            return eventBlockIndicator;
        }

        /**
         * Define el valor de la propiedad eventBlockIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEventBlockIndicator(Boolean value) {
            this.eventBlockIndicator = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "additionalInfo"
        })
        public static class AdditionalInfos {

            @XmlElement(name = "AdditionalInfo", required = true)
            protected List<RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos.AdditionalInfo> additionalInfo;

            /**
             * Gets the value of the additionalInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additionalInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAdditionalInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos.AdditionalInfo }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos.AdditionalInfo> getAdditionalInfo() {
                if (additionalInfo == null) {
                    additionalInfo = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.AdditionalInfos.AdditionalInfo>();
                }
                return this.additionalInfo;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AdditionalInfo {

                @XmlAttribute(name = "AddtionalInfoCode")
                protected String addtionalInfoCode;
                @XmlAttribute(name = "DeliveryMethodCode")
                protected String deliveryMethodCode;

                /**
                 * Obtiene el valor de la propiedad addtionalInfoCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddtionalInfoCode() {
                    return addtionalInfoCode;
                }

                /**
                 * Define el valor de la propiedad addtionalInfoCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddtionalInfoCode(String value) {
                    this.addtionalInfoCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad deliveryMethodCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDeliveryMethodCode() {
                    return deliveryMethodCode;
                }

                /**
                 * Define el valor de la propiedad deliveryMethodCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDeliveryMethodCode(String value) {
                    this.deliveryMethodCode = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Date" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
         *                 &amp;lt;attribute name="DateType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Primary"/&amp;gt;
         *                       &amp;lt;enumeration value="Alternate"/&amp;gt;
         *                       &amp;lt;enumeration value="Published"/&amp;gt;
         *                       &amp;lt;enumeration value="Set-up"/&amp;gt;
         *                       &amp;lt;enumeration value="Breakdown"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="PromotionRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "date",
            "comments"
        })
        public static class Dates {

            @XmlElement(name = "Date", required = true)
            protected List<RFPRequestSegmentsType.RFPRequestSegment.Dates.Date> date;
            @XmlElement(name = "Comments")
            protected ParagraphType comments;

            /**
             * Gets the value of the date property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the date property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDate().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.Dates.Date }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.Dates.Date> getDate() {
                if (date == null) {
                    date = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Dates.Date>();
                }
                return this.date;
            }

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link ParagraphType }
             *     
             */
            public ParagraphType getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link ParagraphType }
             *     
             */
            public void setComments(ParagraphType value) {
                this.comments = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
             *       &amp;lt;attribute name="DateType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Primary"/&amp;gt;
             *             &amp;lt;enumeration value="Alternate"/&amp;gt;
             *             &amp;lt;enumeration value="Published"/&amp;gt;
             *             &amp;lt;enumeration value="Set-up"/&amp;gt;
             *             &amp;lt;enumeration value="Breakdown"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="PromotionRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Date
                extends DateTimeSpanType
            {

                @XmlAttribute(name = "DateType")
                protected String dateType;
                @XmlAttribute(name = "PromotionRPH")
                protected String promotionRPH;

                /**
                 * Obtiene el valor de la propiedad dateType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDateType() {
                    return dateType;
                }

                /**
                 * Define el valor de la propiedad dateType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDateType(String value) {
                    this.dateType = value;
                }

                /**
                 * Obtiene el valor de la propiedad promotionRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPromotionRPH() {
                    return promotionRPH;
                }

                /**
                 * Define el valor de la propiedad promotionRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPromotionRPH(String value) {
                    this.promotionRPH = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="FunctionSpaceReq" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="LargestRoomSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MeetingRoomFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="MinOccupancy" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="CeilingHeight" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                 &amp;lt;attribute name="UnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="MinBreakoutRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="EventBudget" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                 &amp;lt;attribute name="BudgetType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Total"/&amp;gt;
         *                       &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
         *                       &amp;lt;enumeration value="AudioVisual"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="EventDays" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="EventDay" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence minOccurs="0"&amp;gt;
         *                                                 &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
         *                                                             &amp;lt;complexType&amp;gt;
         *                                                               &amp;lt;complexContent&amp;gt;
         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                   &amp;lt;sequence&amp;gt;
         *                                                                     &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
         *                                                                   &amp;lt;/sequence&amp;gt;
         *                                                                   &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                   &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
         *                                                                   &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                                                 &amp;lt;/restriction&amp;gt;
         *                                                               &amp;lt;/complexContent&amp;gt;
         *                                                             &amp;lt;/complexType&amp;gt;
         *                                                           &amp;lt;/element&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                               &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                               &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                               &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                               &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                               &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                               &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="FirstEventDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Exhibition" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ExhibitDetail" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Type"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Public"/&amp;gt;
         *                       &amp;lt;enumeration value="Private"/&amp;gt;
         *                       &amp;lt;enumeration value="PublicPrivate"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="KitDistributionCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="KitFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="GeneralServiceContractorInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="GrossExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="NetExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="GrossUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="NetUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="ExhibitQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="CompanyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="SecuredAreaIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="10" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="MarketSegmentCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="DefiniteIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="Frequency" type="{http://www.opentravel.org/OTA/2003/05}FrequencyType" /&amp;gt;
         *       &amp;lt;attribute name="ExpectedTotalAttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="FirstTimeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FutureOpenDatesIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ExhibitionIncludedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="EventName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "functionSpaceReq",
            "eventBudget",
            "eventDays",
            "exhibition",
            "comments"
        })
        public static class EventBlock {

            @XmlElement(name = "FunctionSpaceReq")
            protected RFPRequestSegmentsType.RFPRequestSegment.EventBlock.FunctionSpaceReq functionSpaceReq;
            @XmlElement(name = "EventBudget")
            protected List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventBudget> eventBudget;
            @XmlElement(name = "EventDays")
            protected RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays eventDays;
            @XmlElement(name = "Exhibition")
            protected List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition> exhibition;
            @XmlElement(name = "Comments")
            protected List<ParagraphType> comments;
            @XmlAttribute(name = "Type")
            protected String type;
            @XmlAttribute(name = "MarketSegmentCode")
            protected String marketSegmentCode;
            @XmlAttribute(name = "DefiniteIndicator")
            protected Boolean definiteIndicator;
            @XmlAttribute(name = "Frequency")
            protected FrequencyType frequency;
            @XmlAttribute(name = "ExpectedTotalAttendanceQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger expectedTotalAttendanceQuantity;
            @XmlAttribute(name = "FirstTimeIndicator")
            protected Boolean firstTimeIndicator;
            @XmlAttribute(name = "FutureOpenDatesIndicator")
            protected Boolean futureOpenDatesIndicator;
            @XmlAttribute(name = "ExhibitionIncludedIndicator")
            protected Boolean exhibitionIncludedIndicator;
            @XmlAttribute(name = "EventName")
            protected String eventName;

            /**
             * Obtiene el valor de la propiedad functionSpaceReq.
             * 
             * @return
             *     possible object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.FunctionSpaceReq }
             *     
             */
            public RFPRequestSegmentsType.RFPRequestSegment.EventBlock.FunctionSpaceReq getFunctionSpaceReq() {
                return functionSpaceReq;
            }

            /**
             * Define el valor de la propiedad functionSpaceReq.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.FunctionSpaceReq }
             *     
             */
            public void setFunctionSpaceReq(RFPRequestSegmentsType.RFPRequestSegment.EventBlock.FunctionSpaceReq value) {
                this.functionSpaceReq = value;
            }

            /**
             * Gets the value of the eventBudget property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventBudget property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getEventBudget().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventBudget }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventBudget> getEventBudget() {
                if (eventBudget == null) {
                    eventBudget = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventBudget>();
                }
                return this.eventBudget;
            }

            /**
             * Obtiene el valor de la propiedad eventDays.
             * 
             * @return
             *     possible object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays }
             *     
             */
            public RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays getEventDays() {
                return eventDays;
            }

            /**
             * Define el valor de la propiedad eventDays.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays }
             *     
             */
            public void setEventDays(RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays value) {
                this.eventDays = value;
            }

            /**
             * Gets the value of the exhibition property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the exhibition property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getExhibition().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition> getExhibition() {
                if (exhibition == null) {
                    exhibition = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition>();
                }
                return this.exhibition;
            }

            /**
             * Gets the value of the comments property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comments property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComments().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComments() {
                if (comments == null) {
                    comments = new ArrayList<ParagraphType>();
                }
                return this.comments;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad marketSegmentCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMarketSegmentCode() {
                return marketSegmentCode;
            }

            /**
             * Define el valor de la propiedad marketSegmentCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMarketSegmentCode(String value) {
                this.marketSegmentCode = value;
            }

            /**
             * Obtiene el valor de la propiedad definiteIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDefiniteIndicator() {
                return definiteIndicator;
            }

            /**
             * Define el valor de la propiedad definiteIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDefiniteIndicator(Boolean value) {
                this.definiteIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad frequency.
             * 
             * @return
             *     possible object is
             *     {@link FrequencyType }
             *     
             */
            public FrequencyType getFrequency() {
                return frequency;
            }

            /**
             * Define el valor de la propiedad frequency.
             * 
             * @param value
             *     allowed object is
             *     {@link FrequencyType }
             *     
             */
            public void setFrequency(FrequencyType value) {
                this.frequency = value;
            }

            /**
             * Obtiene el valor de la propiedad expectedTotalAttendanceQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getExpectedTotalAttendanceQuantity() {
                return expectedTotalAttendanceQuantity;
            }

            /**
             * Define el valor de la propiedad expectedTotalAttendanceQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setExpectedTotalAttendanceQuantity(BigInteger value) {
                this.expectedTotalAttendanceQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad firstTimeIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFirstTimeIndicator() {
                return firstTimeIndicator;
            }

            /**
             * Define el valor de la propiedad firstTimeIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFirstTimeIndicator(Boolean value) {
                this.firstTimeIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad futureOpenDatesIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFutureOpenDatesIndicator() {
                return futureOpenDatesIndicator;
            }

            /**
             * Define el valor de la propiedad futureOpenDatesIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFutureOpenDatesIndicator(Boolean value) {
                this.futureOpenDatesIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad exhibitionIncludedIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExhibitionIncludedIndicator() {
                return exhibitionIncludedIndicator;
            }

            /**
             * Define el valor de la propiedad exhibitionIncludedIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExhibitionIncludedIndicator(Boolean value) {
                this.exhibitionIncludedIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad eventName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEventName() {
                return eventName;
            }

            /**
             * Define el valor de la propiedad eventName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEventName(String value) {
                this.eventName = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *       &amp;lt;attribute name="BudgetType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Total"/&amp;gt;
             *             &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
             *             &amp;lt;enumeration value="AudioVisual"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class EventBudget {

                @XmlAttribute(name = "BudgetType")
                protected String budgetType;
                @XmlAttribute(name = "IncludesTaxInd")
                protected Boolean includesTaxInd;
                @XmlAttribute(name = "IncludesServiceFeesInd")
                protected Boolean includesServiceFeesInd;
                @XmlAttribute(name = "IncludesGratuityInd")
                protected Boolean includesGratuityInd;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;

                /**
                 * Obtiene el valor de la propiedad budgetType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBudgetType() {
                    return budgetType;
                }

                /**
                 * Define el valor de la propiedad budgetType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBudgetType(String value) {
                    this.budgetType = value;
                }

                /**
                 * Obtiene el valor de la propiedad includesTaxInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIncludesTaxInd() {
                    return includesTaxInd;
                }

                /**
                 * Define el valor de la propiedad includesTaxInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIncludesTaxInd(Boolean value) {
                    this.includesTaxInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad includesServiceFeesInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIncludesServiceFeesInd() {
                    return includesServiceFeesInd;
                }

                /**
                 * Define el valor de la propiedad includesServiceFeesInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIncludesServiceFeesInd(Boolean value) {
                    this.includesServiceFeesInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad includesGratuityInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIncludesGratuityInd() {
                    return includesGratuityInd;
                }

                /**
                 * Define el valor de la propiedad includesGratuityInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIncludesGratuityInd(Boolean value) {
                    this.includesGratuityInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="EventDay" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence minOccurs="0"&amp;gt;
             *                                       &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
             *                                                   &amp;lt;complexType&amp;gt;
             *                                                     &amp;lt;complexContent&amp;gt;
             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                         &amp;lt;sequence&amp;gt;
             *                                                           &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
             *                                                         &amp;lt;/sequence&amp;gt;
             *                                                         &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                         &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
             *                                                         &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                                       &amp;lt;/restriction&amp;gt;
             *                                                     &amp;lt;/complexContent&amp;gt;
             *                                                   &amp;lt;/complexType&amp;gt;
             *                                                 &amp;lt;/element&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                     &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                     &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                     &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                     &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                     &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                     &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="FirstEventDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "eventDay"
            })
            public static class EventDays {

                @XmlElement(name = "EventDay", required = true)
                protected List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay> eventDay;
                @XmlAttribute(name = "FirstEventDayOfWeek")
                protected DayOfWeekType firstEventDayOfWeek;

                /**
                 * Gets the value of the eventDay property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventDay property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getEventDay().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay }
                 * 
                 * 
                 */
                public List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay> getEventDay() {
                    if (eventDay == null) {
                        eventDay = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay>();
                    }
                    return this.eventDay;
                }

                /**
                 * Obtiene el valor de la propiedad firstEventDayOfWeek.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public DayOfWeekType getFirstEventDayOfWeek() {
                    return firstEventDayOfWeek;
                }

                /**
                 * Define el valor de la propiedad firstEventDayOfWeek.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public void setFirstEventDayOfWeek(DayOfWeekType value) {
                    this.firstEventDayOfWeek = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence minOccurs="0"&amp;gt;
                 *                             &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
                 *                                         &amp;lt;complexType&amp;gt;
                 *                                           &amp;lt;complexContent&amp;gt;
                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                               &amp;lt;sequence&amp;gt;
                 *                                                 &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                 *                                               &amp;lt;/sequence&amp;gt;
                 *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                               &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                 *                                               &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                                             &amp;lt;/restriction&amp;gt;
                 *                                           &amp;lt;/complexContent&amp;gt;
                 *                                         &amp;lt;/complexType&amp;gt;
                 *                                       &amp;lt;/element&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                           &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                           &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                           &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                           &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                           &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                           &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "eventDayFunctions"
                })
                public static class EventDay {

                    @XmlElement(name = "EventDayFunctions")
                    protected RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions eventDayFunctions;
                    @XmlAttribute(name = "DayNumber", required = true)
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger dayNumber;

                    /**
                     * Obtiene el valor de la propiedad eventDayFunctions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions }
                     *     
                     */
                    public RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions getEventDayFunctions() {
                        return eventDayFunctions;
                    }

                    /**
                     * Define el valor de la propiedad eventDayFunctions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions }
                     *     
                     */
                    public void setEventDayFunctions(RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions value) {
                        this.eventDayFunctions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dayNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getDayNumber() {
                        return dayNumber;
                    }

                    /**
                     * Define el valor de la propiedad dayNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setDayNumber(BigInteger value) {
                        this.dayNumber = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence minOccurs="0"&amp;gt;
                     *                   &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
                     *                               &amp;lt;complexType&amp;gt;
                     *                                 &amp;lt;complexContent&amp;gt;
                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                     &amp;lt;sequence&amp;gt;
                     *                                       &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                     *                                     &amp;lt;/sequence&amp;gt;
                     *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                     &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                     *                                     &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                                   &amp;lt;/restriction&amp;gt;
                     *                                 &amp;lt;/complexContent&amp;gt;
                     *                               &amp;lt;/complexType&amp;gt;
                     *                             &amp;lt;/element&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                 &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                 &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                 &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                 &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                 &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                 &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "eventDayFunction"
                    })
                    public static class EventDayFunctions {

                        @XmlElement(name = "EventDayFunction", required = true)
                        protected List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction> eventDayFunction;

                        /**
                         * Gets the value of the eventDayFunction property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventDayFunction property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getEventDayFunction().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction }
                         * 
                         * 
                         */
                        public List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction> getEventDayFunction() {
                            if (eventDayFunction == null) {
                                eventDayFunction = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction>();
                            }
                            return this.eventDayFunction;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence minOccurs="0"&amp;gt;
                         *         &amp;lt;element name="AudioVisualReqs" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
                         *                     &amp;lt;complexType&amp;gt;
                         *                       &amp;lt;complexContent&amp;gt;
                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                           &amp;lt;sequence&amp;gt;
                         *                             &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                         *                           &amp;lt;/sequence&amp;gt;
                         *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                           &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                         *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *                         &amp;lt;/restriction&amp;gt;
                         *                       &amp;lt;/complexContent&amp;gt;
                         *                     &amp;lt;/complexType&amp;gt;
                         *                   &amp;lt;/element&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                         *       &amp;lt;attribute name="EventName" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *       &amp;lt;attribute name="EventType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *       &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *       &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *       &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *       &amp;lt;attribute name="RoomSetup" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *       &amp;lt;attribute name="TwentyFourHourHold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "audioVisualReqs",
                            "comments"
                        })
                        public static class EventDayFunction {

                            @XmlElement(name = "AudioVisualReqs")
                            protected RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs audioVisualReqs;
                            @XmlElement(name = "Comments")
                            protected ParagraphType comments;
                            @XmlAttribute(name = "EventName", required = true)
                            protected String eventName;
                            @XmlAttribute(name = "EventType")
                            protected String eventType;
                            @XmlAttribute(name = "StartTime", required = true)
                            @XmlSchemaType(name = "time")
                            protected XMLGregorianCalendar startTime;
                            @XmlAttribute(name = "EndTime", required = true)
                            @XmlSchemaType(name = "time")
                            protected XMLGregorianCalendar endTime;
                            @XmlAttribute(name = "AttendeeQuantity")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger attendeeQuantity;
                            @XmlAttribute(name = "RoomSetup")
                            protected String roomSetup;
                            @XmlAttribute(name = "TwentyFourHourHold")
                            protected Boolean twentyFourHourHold;
                            @XmlAttribute(name = "UnitOfMeasureQuantity")
                            protected BigDecimal unitOfMeasureQuantity;
                            @XmlAttribute(name = "UnitOfMeasure")
                            protected String unitOfMeasure;
                            @XmlAttribute(name = "UnitOfMeasureCode")
                            protected String unitOfMeasureCode;

                            /**
                             * Obtiene el valor de la propiedad audioVisualReqs.
                             * 
                             * @return
                             *     possible object is
                             *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs }
                             *     
                             */
                            public RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs getAudioVisualReqs() {
                                return audioVisualReqs;
                            }

                            /**
                             * Define el valor de la propiedad audioVisualReqs.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs }
                             *     
                             */
                            public void setAudioVisualReqs(RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs value) {
                                this.audioVisualReqs = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad comments.
                             * 
                             * @return
                             *     possible object is
                             *     {@link ParagraphType }
                             *     
                             */
                            public ParagraphType getComments() {
                                return comments;
                            }

                            /**
                             * Define el valor de la propiedad comments.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link ParagraphType }
                             *     
                             */
                            public void setComments(ParagraphType value) {
                                this.comments = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad eventName.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getEventName() {
                                return eventName;
                            }

                            /**
                             * Define el valor de la propiedad eventName.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setEventName(String value) {
                                this.eventName = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad eventType.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getEventType() {
                                return eventType;
                            }

                            /**
                             * Define el valor de la propiedad eventType.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setEventType(String value) {
                                this.eventType = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad startTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getStartTime() {
                                return startTime;
                            }

                            /**
                             * Define el valor de la propiedad startTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setStartTime(XMLGregorianCalendar value) {
                                this.startTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad endTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getEndTime() {
                                return endTime;
                            }

                            /**
                             * Define el valor de la propiedad endTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setEndTime(XMLGregorianCalendar value) {
                                this.endTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad attendeeQuantity.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getAttendeeQuantity() {
                                return attendeeQuantity;
                            }

                            /**
                             * Define el valor de la propiedad attendeeQuantity.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setAttendeeQuantity(BigInteger value) {
                                this.attendeeQuantity = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad roomSetup.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getRoomSetup() {
                                return roomSetup;
                            }

                            /**
                             * Define el valor de la propiedad roomSetup.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setRoomSetup(String value) {
                                this.roomSetup = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad twentyFourHourHold.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isTwentyFourHourHold() {
                                return twentyFourHourHold;
                            }

                            /**
                             * Define el valor de la propiedad twentyFourHourHold.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setTwentyFourHourHold(Boolean value) {
                                this.twentyFourHourHold = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad unitOfMeasureQuantity.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getUnitOfMeasureQuantity() {
                                return unitOfMeasureQuantity;
                            }

                            /**
                             * Define el valor de la propiedad unitOfMeasureQuantity.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setUnitOfMeasureQuantity(BigDecimal value) {
                                this.unitOfMeasureQuantity = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad unitOfMeasure.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getUnitOfMeasure() {
                                return unitOfMeasure;
                            }

                            /**
                             * Define el valor de la propiedad unitOfMeasure.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setUnitOfMeasure(String value) {
                                this.unitOfMeasure = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad unitOfMeasureCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getUnitOfMeasureCode() {
                                return unitOfMeasureCode;
                            }

                            /**
                             * Define el valor de la propiedad unitOfMeasureCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setUnitOfMeasureCode(String value) {
                                this.unitOfMeasureCode = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="AudioVisualReq" maxOccurs="unbounded"&amp;gt;
                             *           &amp;lt;complexType&amp;gt;
                             *             &amp;lt;complexContent&amp;gt;
                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                 &amp;lt;sequence&amp;gt;
                             *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                             *                 &amp;lt;/sequence&amp;gt;
                             *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                 &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                             *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                             *               &amp;lt;/restriction&amp;gt;
                             *             &amp;lt;/complexContent&amp;gt;
                             *           &amp;lt;/complexType&amp;gt;
                             *         &amp;lt;/element&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "audioVisualReq"
                            })
                            public static class AudioVisualReqs {

                                @XmlElement(name = "AudioVisualReq", required = true)
                                protected List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs.AudioVisualReq> audioVisualReq;

                                /**
                                 * Gets the value of the audioVisualReq property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the audioVisualReq property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getAudioVisualReq().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs.AudioVisualReq }
                                 * 
                                 * 
                                 */
                                public List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs.AudioVisualReq> getAudioVisualReq() {
                                    if (audioVisualReq == null) {
                                        audioVisualReq = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisualReqs.AudioVisualReq>();
                                    }
                                    return this.audioVisualReq;
                                }


                                /**
                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                 * 
                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                 * 
                                 * &lt;pre&gt;
                                 * &amp;lt;complexType&amp;gt;
                                 *   &amp;lt;complexContent&amp;gt;
                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *       &amp;lt;sequence&amp;gt;
                                 *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                                 *       &amp;lt;/sequence&amp;gt;
                                 *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *       &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                                 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                                 *     &amp;lt;/restriction&amp;gt;
                                 *   &amp;lt;/complexContent&amp;gt;
                                 * &amp;lt;/complexType&amp;gt;
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "comments"
                                })
                                public static class AudioVisualReq {

                                    @XmlElement(name = "Comments", required = true)
                                    protected ParagraphType comments;
                                    @XmlAttribute(name = "Code")
                                    protected String code;
                                    @XmlAttribute(name = "AudioVisualPref")
                                    protected PreferLevelType audioVisualPref;
                                    @XmlAttribute(name = "Quantity")
                                    @XmlSchemaType(name = "nonNegativeInteger")
                                    protected BigInteger quantity;

                                    /**
                                     * Obtiene el valor de la propiedad comments.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link ParagraphType }
                                     *     
                                     */
                                    public ParagraphType getComments() {
                                        return comments;
                                    }

                                    /**
                                     * Define el valor de la propiedad comments.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link ParagraphType }
                                     *     
                                     */
                                    public void setComments(ParagraphType value) {
                                        this.comments = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad code.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getCode() {
                                        return code;
                                    }

                                    /**
                                     * Define el valor de la propiedad code.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setCode(String value) {
                                        this.code = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad audioVisualPref.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link PreferLevelType }
                                     *     
                                     */
                                    public PreferLevelType getAudioVisualPref() {
                                        return audioVisualPref;
                                    }

                                    /**
                                     * Define el valor de la propiedad audioVisualPref.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link PreferLevelType }
                                     *     
                                     */
                                    public void setAudioVisualPref(PreferLevelType value) {
                                        this.audioVisualPref = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad quantity.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public BigInteger getQuantity() {
                                        return quantity;
                                    }

                                    /**
                                     * Define el valor de la propiedad quantity.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public void setQuantity(BigInteger value) {
                                        this.quantity = value;
                                    }

                                }

                            }

                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ExhibitDetail" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Type"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Public"/&amp;gt;
             *             &amp;lt;enumeration value="Private"/&amp;gt;
             *             &amp;lt;enumeration value="PublicPrivate"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="KitDistributionCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="KitFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="GeneralServiceContractorInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="GrossExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="NetExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="GrossUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="NetUnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="ExhibitQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="CompanyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="SecuredAreaIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "exhibitDetail",
                "comments"
            })
            public static class Exhibition {

                @XmlElement(name = "ExhibitDetail")
                protected List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition.ExhibitDetail> exhibitDetail;
                @XmlElement(name = "Comments")
                protected ParagraphType comments;
                @XmlAttribute(name = "Type")
                protected String type;
                @XmlAttribute(name = "KitDistributionCode")
                protected String kitDistributionCode;
                @XmlAttribute(name = "KitFormatCode")
                protected String kitFormatCode;
                @XmlAttribute(name = "GeneralServiceContractorInd")
                protected Boolean generalServiceContractorInd;
                @XmlAttribute(name = "GrossExhibitionSpace")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger grossExhibitionSpace;
                @XmlAttribute(name = "NetExhibitionSpace")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger netExhibitionSpace;
                @XmlAttribute(name = "GrossUnitOfMeasure")
                protected String grossUnitOfMeasure;
                @XmlAttribute(name = "NetUnitOfMeasure")
                protected String netUnitOfMeasure;
                @XmlAttribute(name = "ExhibitQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger exhibitQuantity;
                @XmlAttribute(name = "CompanyQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger companyQuantity;
                @XmlAttribute(name = "SecuredAreaIndicator")
                protected Boolean securedAreaIndicator;

                /**
                 * Gets the value of the exhibitDetail property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the exhibitDetail property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getExhibitDetail().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition.ExhibitDetail }
                 * 
                 * 
                 */
                public List<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition.ExhibitDetail> getExhibitDetail() {
                    if (exhibitDetail == null) {
                        exhibitDetail = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.EventBlock.Exhibition.ExhibitDetail>();
                    }
                    return this.exhibitDetail;
                }

                /**
                 * Obtiene el valor de la propiedad comments.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ParagraphType }
                 *     
                 */
                public ParagraphType getComments() {
                    return comments;
                }

                /**
                 * Define el valor de la propiedad comments.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ParagraphType }
                 *     
                 */
                public void setComments(ParagraphType value) {
                    this.comments = value;
                }

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad kitDistributionCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getKitDistributionCode() {
                    return kitDistributionCode;
                }

                /**
                 * Define el valor de la propiedad kitDistributionCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setKitDistributionCode(String value) {
                    this.kitDistributionCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad kitFormatCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getKitFormatCode() {
                    return kitFormatCode;
                }

                /**
                 * Define el valor de la propiedad kitFormatCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setKitFormatCode(String value) {
                    this.kitFormatCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad generalServiceContractorInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isGeneralServiceContractorInd() {
                    return generalServiceContractorInd;
                }

                /**
                 * Define el valor de la propiedad generalServiceContractorInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setGeneralServiceContractorInd(Boolean value) {
                    this.generalServiceContractorInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad grossExhibitionSpace.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getGrossExhibitionSpace() {
                    return grossExhibitionSpace;
                }

                /**
                 * Define el valor de la propiedad grossExhibitionSpace.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setGrossExhibitionSpace(BigInteger value) {
                    this.grossExhibitionSpace = value;
                }

                /**
                 * Obtiene el valor de la propiedad netExhibitionSpace.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNetExhibitionSpace() {
                    return netExhibitionSpace;
                }

                /**
                 * Define el valor de la propiedad netExhibitionSpace.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNetExhibitionSpace(BigInteger value) {
                    this.netExhibitionSpace = value;
                }

                /**
                 * Obtiene el valor de la propiedad grossUnitOfMeasure.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGrossUnitOfMeasure() {
                    return grossUnitOfMeasure;
                }

                /**
                 * Define el valor de la propiedad grossUnitOfMeasure.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGrossUnitOfMeasure(String value) {
                    this.grossUnitOfMeasure = value;
                }

                /**
                 * Obtiene el valor de la propiedad netUnitOfMeasure.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNetUnitOfMeasure() {
                    return netUnitOfMeasure;
                }

                /**
                 * Define el valor de la propiedad netUnitOfMeasure.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNetUnitOfMeasure(String value) {
                    this.netUnitOfMeasure = value;
                }

                /**
                 * Obtiene el valor de la propiedad exhibitQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getExhibitQuantity() {
                    return exhibitQuantity;
                }

                /**
                 * Define el valor de la propiedad exhibitQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setExhibitQuantity(BigInteger value) {
                    this.exhibitQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad companyQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCompanyQuantity() {
                    return companyQuantity;
                }

                /**
                 * Define el valor de la propiedad companyQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCompanyQuantity(BigInteger value) {
                    this.companyQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad securedAreaIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isSecuredAreaIndicator() {
                    return securedAreaIndicator;
                }

                /**
                 * Define el valor de la propiedad securedAreaIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setSecuredAreaIndicator(Boolean value) {
                    this.securedAreaIndicator = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ExhibitDetail {

                    @XmlAttribute(name = "ExhibitTypeCode")
                    protected String exhibitTypeCode;

                    /**
                     * Obtiene el valor de la propiedad exhibitTypeCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExhibitTypeCode() {
                        return exhibitTypeCode;
                    }

                    /**
                     * Define el valor de la propiedad exhibitTypeCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExhibitTypeCode(String value) {
                        this.exhibitTypeCode = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="LargestRoomSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="MeetingRoomFormatCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="MinOccupancy" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="CeilingHeight" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *       &amp;lt;attribute name="UnitOfMeasure" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="MinBreakoutRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class FunctionSpaceReq {

                @XmlAttribute(name = "LargestRoomSpace")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger largestRoomSpace;
                @XmlAttribute(name = "MeetingRoomFormatCode")
                protected String meetingRoomFormatCode;
                @XmlAttribute(name = "MinOccupancy")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger minOccupancy;
                @XmlAttribute(name = "CeilingHeight")
                protected BigDecimal ceilingHeight;
                @XmlAttribute(name = "UnitOfMeasure")
                protected String unitOfMeasure;
                @XmlAttribute(name = "MinBreakoutRoomQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger minBreakoutRoomQuantity;

                /**
                 * Obtiene el valor de la propiedad largestRoomSpace.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getLargestRoomSpace() {
                    return largestRoomSpace;
                }

                /**
                 * Define el valor de la propiedad largestRoomSpace.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setLargestRoomSpace(BigInteger value) {
                    this.largestRoomSpace = value;
                }

                /**
                 * Obtiene el valor de la propiedad meetingRoomFormatCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMeetingRoomFormatCode() {
                    return meetingRoomFormatCode;
                }

                /**
                 * Define el valor de la propiedad meetingRoomFormatCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMeetingRoomFormatCode(String value) {
                    this.meetingRoomFormatCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad minOccupancy.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMinOccupancy() {
                    return minOccupancy;
                }

                /**
                 * Define el valor de la propiedad minOccupancy.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMinOccupancy(BigInteger value) {
                    this.minOccupancy = value;
                }

                /**
                 * Obtiene el valor de la propiedad ceilingHeight.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getCeilingHeight() {
                    return ceilingHeight;
                }

                /**
                 * Define el valor de la propiedad ceilingHeight.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setCeilingHeight(BigDecimal value) {
                    this.ceilingHeight = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasure.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnitOfMeasure() {
                    return unitOfMeasure;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasure.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnitOfMeasure(String value) {
                    this.unitOfMeasure = value;
                }

                /**
                 * Obtiene el valor de la propiedad minBreakoutRoomQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMinBreakoutRoomQuantity() {
                    return minBreakoutRoomQuantity;
                }

                /**
                 * Define el valor de la propiedad minBreakoutRoomQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMinBreakoutRoomQuantity(BigInteger value) {
                    this.minBreakoutRoomQuantity = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="History" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="DaySummaries" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="DaySummary" maxOccurs="unbounded"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="DayNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="PostEventReportExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "history"
        })
        public static class Histories {

            @XmlElement(name = "History", required = true)
            protected List<RFPRequestSegmentsType.RFPRequestSegment.Histories.History> history;

            /**
             * Gets the value of the history property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the history property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getHistory().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.Histories.History }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.Histories.History> getHistory() {
                if (history == null) {
                    history = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Histories.History>();
                }
                return this.history;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="DaySummaries" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="DaySummary" maxOccurs="unbounded"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
             *                           &amp;lt;attribute name="DayNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="SiteName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="PostEventReportExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "uniqueID",
                "contactInfos",
                "daySummaries",
                "comments"
            })
            public static class History {

                @XmlElement(name = "UniqueID")
                protected UniqueIDType uniqueID;
                @XmlElement(name = "ContactInfos")
                protected RFPRequestSegmentsType.RFPRequestSegment.Histories.History.ContactInfos contactInfos;
                @XmlElement(name = "DaySummaries")
                protected RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries daySummaries;
                @XmlElement(name = "Comments")
                protected ParagraphType comments;
                @XmlAttribute(name = "MeetingName")
                protected String meetingName;
                @XmlAttribute(name = "SiteName")
                protected String siteName;
                @XmlAttribute(name = "PostEventReportExistsInd")
                protected Boolean postEventReportExistsInd;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "AttendeeQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger attendeeQuantity;
                @XmlAttribute(name = "RoomBlockQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger roomBlockQuantity;
                @XmlAttribute(name = "RoomPickupQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger roomPickupQuantity;
                @XmlAttribute(name = "TotalGuestRoomAmount")
                protected BigDecimal totalGuestRoomAmount;
                @XmlAttribute(name = "TotalFoodAndBevAmount")
                protected BigDecimal totalFoodAndBevAmount;
                @XmlAttribute(name = "TotalMeetingRoomRentalAmount")
                protected BigDecimal totalMeetingRoomRentalAmount;
                @XmlAttribute(name = "TotalAudioVisualRentalAmount")
                protected BigDecimal totalAudioVisualRentalAmount;
                @XmlAttribute(name = "TotalMiscAmount")
                protected BigDecimal totalMiscAmount;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad uniqueID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link UniqueIDType }
                 *     
                 */
                public UniqueIDType getUniqueID() {
                    return uniqueID;
                }

                /**
                 * Define el valor de la propiedad uniqueID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link UniqueIDType }
                 *     
                 */
                public void setUniqueID(UniqueIDType value) {
                    this.uniqueID = value;
                }

                /**
                 * Obtiene el valor de la propiedad contactInfos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Histories.History.ContactInfos }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Histories.History.ContactInfos getContactInfos() {
                    return contactInfos;
                }

                /**
                 * Define el valor de la propiedad contactInfos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Histories.History.ContactInfos }
                 *     
                 */
                public void setContactInfos(RFPRequestSegmentsType.RFPRequestSegment.Histories.History.ContactInfos value) {
                    this.contactInfos = value;
                }

                /**
                 * Obtiene el valor de la propiedad daySummaries.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries getDaySummaries() {
                    return daySummaries;
                }

                /**
                 * Define el valor de la propiedad daySummaries.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries }
                 *     
                 */
                public void setDaySummaries(RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries value) {
                    this.daySummaries = value;
                }

                /**
                 * Obtiene el valor de la propiedad comments.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ParagraphType }
                 *     
                 */
                public ParagraphType getComments() {
                    return comments;
                }

                /**
                 * Define el valor de la propiedad comments.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ParagraphType }
                 *     
                 */
                public void setComments(ParagraphType value) {
                    this.comments = value;
                }

                /**
                 * Obtiene el valor de la propiedad meetingName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMeetingName() {
                    return meetingName;
                }

                /**
                 * Define el valor de la propiedad meetingName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMeetingName(String value) {
                    this.meetingName = value;
                }

                /**
                 * Obtiene el valor de la propiedad siteName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSiteName() {
                    return siteName;
                }

                /**
                 * Define el valor de la propiedad siteName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSiteName(String value) {
                    this.siteName = value;
                }

                /**
                 * Obtiene el valor de la propiedad postEventReportExistsInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPostEventReportExistsInd() {
                    return postEventReportExistsInd;
                }

                /**
                 * Define el valor de la propiedad postEventReportExistsInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPostEventReportExistsInd(Boolean value) {
                    this.postEventReportExistsInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad attendeeQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getAttendeeQuantity() {
                    return attendeeQuantity;
                }

                /**
                 * Define el valor de la propiedad attendeeQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setAttendeeQuantity(BigInteger value) {
                    this.attendeeQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad roomBlockQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getRoomBlockQuantity() {
                    return roomBlockQuantity;
                }

                /**
                 * Define el valor de la propiedad roomBlockQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setRoomBlockQuantity(BigInteger value) {
                    this.roomBlockQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad roomPickupQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getRoomPickupQuantity() {
                    return roomPickupQuantity;
                }

                /**
                 * Define el valor de la propiedad roomPickupQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setRoomPickupQuantity(BigInteger value) {
                    this.roomPickupQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad totalGuestRoomAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getTotalGuestRoomAmount() {
                    return totalGuestRoomAmount;
                }

                /**
                 * Define el valor de la propiedad totalGuestRoomAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setTotalGuestRoomAmount(BigDecimal value) {
                    this.totalGuestRoomAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad totalFoodAndBevAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getTotalFoodAndBevAmount() {
                    return totalFoodAndBevAmount;
                }

                /**
                 * Define el valor de la propiedad totalFoodAndBevAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setTotalFoodAndBevAmount(BigDecimal value) {
                    this.totalFoodAndBevAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad totalMeetingRoomRentalAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getTotalMeetingRoomRentalAmount() {
                    return totalMeetingRoomRentalAmount;
                }

                /**
                 * Define el valor de la propiedad totalMeetingRoomRentalAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setTotalMeetingRoomRentalAmount(BigDecimal value) {
                    this.totalMeetingRoomRentalAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad totalAudioVisualRentalAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getTotalAudioVisualRentalAmount() {
                    return totalAudioVisualRentalAmount;
                }

                /**
                 * Define el valor de la propiedad totalAudioVisualRentalAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setTotalAudioVisualRentalAmount(BigDecimal value) {
                    this.totalAudioVisualRentalAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad totalMiscAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getTotalMiscAmount() {
                    return totalMiscAmount;
                }

                /**
                 * Define el valor de la propiedad totalMiscAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setTotalMiscAmount(BigDecimal value) {
                    this.totalMiscAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "contactInfo"
                })
                public static class ContactInfos {

                    @XmlElement(name = "ContactInfo", required = true)
                    protected List<ContactPersonType> contactInfo;

                    /**
                     * Gets the value of the contactInfo property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contactInfo property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getContactInfo().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link ContactPersonType }
                     * 
                     * 
                     */
                    public List<ContactPersonType> getContactInfo() {
                        if (contactInfo == null) {
                            contactInfo = new ArrayList<ContactPersonType>();
                        }
                        return this.contactInfo;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="DaySummary" maxOccurs="unbounded"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="DayNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "daySummary"
                })
                public static class DaySummaries {

                    @XmlElement(name = "DaySummary", required = true)
                    protected List<RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries.DaySummary> daySummary;

                    /**
                     * Gets the value of the daySummary property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the daySummary property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getDaySummary().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries.DaySummary }
                     * 
                     * 
                     */
                    public List<RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries.DaySummary> getDaySummary() {
                        if (daySummary == null) {
                            daySummary = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Histories.History.DaySummaries.DaySummary>();
                        }
                        return this.daySummary;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EventStatisticsGroup"/&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                     *       &amp;lt;attribute name="DayNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class DaySummary {

                        @XmlAttribute(name = "DayNumber")
                        @XmlSchemaType(name = "positiveInteger")
                        protected BigInteger dayNumber;
                        @XmlAttribute(name = "AttendeeQuantity")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger attendeeQuantity;
                        @XmlAttribute(name = "RoomBlockQuantity")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger roomBlockQuantity;
                        @XmlAttribute(name = "RoomPickupQuantity")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger roomPickupQuantity;
                        @XmlAttribute(name = "TotalGuestRoomAmount")
                        protected BigDecimal totalGuestRoomAmount;
                        @XmlAttribute(name = "TotalFoodAndBevAmount")
                        protected BigDecimal totalFoodAndBevAmount;
                        @XmlAttribute(name = "TotalMeetingRoomRentalAmount")
                        protected BigDecimal totalMeetingRoomRentalAmount;
                        @XmlAttribute(name = "TotalAudioVisualRentalAmount")
                        protected BigDecimal totalAudioVisualRentalAmount;
                        @XmlAttribute(name = "TotalMiscAmount")
                        protected BigDecimal totalMiscAmount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;
                        @XmlAttribute(name = "DecimalPlaces")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger decimalPlaces;

                        /**
                         * Obtiene el valor de la propiedad dayNumber.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getDayNumber() {
                            return dayNumber;
                        }

                        /**
                         * Define el valor de la propiedad dayNumber.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setDayNumber(BigInteger value) {
                            this.dayNumber = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad attendeeQuantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getAttendeeQuantity() {
                            return attendeeQuantity;
                        }

                        /**
                         * Define el valor de la propiedad attendeeQuantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setAttendeeQuantity(BigInteger value) {
                            this.attendeeQuantity = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad roomBlockQuantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getRoomBlockQuantity() {
                            return roomBlockQuantity;
                        }

                        /**
                         * Define el valor de la propiedad roomBlockQuantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setRoomBlockQuantity(BigInteger value) {
                            this.roomBlockQuantity = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad roomPickupQuantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getRoomPickupQuantity() {
                            return roomPickupQuantity;
                        }

                        /**
                         * Define el valor de la propiedad roomPickupQuantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setRoomPickupQuantity(BigInteger value) {
                            this.roomPickupQuantity = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad totalGuestRoomAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getTotalGuestRoomAmount() {
                            return totalGuestRoomAmount;
                        }

                        /**
                         * Define el valor de la propiedad totalGuestRoomAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setTotalGuestRoomAmount(BigDecimal value) {
                            this.totalGuestRoomAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad totalFoodAndBevAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getTotalFoodAndBevAmount() {
                            return totalFoodAndBevAmount;
                        }

                        /**
                         * Define el valor de la propiedad totalFoodAndBevAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setTotalFoodAndBevAmount(BigDecimal value) {
                            this.totalFoodAndBevAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad totalMeetingRoomRentalAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getTotalMeetingRoomRentalAmount() {
                            return totalMeetingRoomRentalAmount;
                        }

                        /**
                         * Define el valor de la propiedad totalMeetingRoomRentalAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setTotalMeetingRoomRentalAmount(BigDecimal value) {
                            this.totalMeetingRoomRentalAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad totalAudioVisualRentalAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getTotalAudioVisualRentalAmount() {
                            return totalAudioVisualRentalAmount;
                        }

                        /**
                         * Define el valor de la propiedad totalAudioVisualRentalAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setTotalAudioVisualRentalAmount(BigDecimal value) {
                            this.totalAudioVisualRentalAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad totalMiscAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getTotalMiscAmount() {
                            return totalMiscAmount;
                        }

                        /**
                         * Define el valor de la propiedad totalMiscAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setTotalMiscAmount(BigDecimal value) {
                            this.totalMiscAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad currencyCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Define el valor de la propiedad currencyCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad decimalPlaces.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getDecimalPlaces() {
                            return decimalPlaces;
                        }

                        /**
                         * Define el valor de la propiedad decimalPlaces.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setDecimalPlaces(BigInteger value) {
                            this.decimalPlaces = value;
                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Preference" maxOccurs="5"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Recreations" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Recreation" maxOccurs="unbounded"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Properties" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Property" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "preference"
        })
        public static class Preferences {

            @XmlElement(name = "Preference", required = true)
            protected List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference> preference;

            /**
             * Gets the value of the preference property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the preference property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPreference().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference> getPreference() {
                if (preference == null) {
                    preference = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference>();
                }
                return this.preference;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
             *                           &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Recreations" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Recreation" maxOccurs="unbounded"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
             *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Properties" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Property" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
             *                           &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "locationCategories",
                "recreations",
                "properties",
                "comments"
            })
            public static class Preference {

                @XmlElement(name = "LocationCategories")
                protected List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories> locationCategories;
                @XmlElement(name = "Recreations")
                protected RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations recreations;
                @XmlElement(name = "Properties")
                protected RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties properties;
                @XmlElement(name = "Comments")
                protected ParagraphType comments;

                /**
                 * Gets the value of the locationCategories property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the locationCategories property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getLocationCategories().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories }
                 * 
                 * 
                 */
                public List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories> getLocationCategories() {
                    if (locationCategories == null) {
                        locationCategories = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories>();
                    }
                    return this.locationCategories;
                }

                /**
                 * Obtiene el valor de la propiedad recreations.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations getRecreations() {
                    return recreations;
                }

                /**
                 * Define el valor de la propiedad recreations.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations }
                 *     
                 */
                public void setRecreations(RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations value) {
                    this.recreations = value;
                }

                /**
                 * Obtiene el valor de la propiedad properties.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties getProperties() {
                    return properties;
                }

                /**
                 * Define el valor de la propiedad properties.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties }
                 *     
                 */
                public void setProperties(RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties value) {
                    this.properties = value;
                }

                /**
                 * Obtiene el valor de la propiedad comments.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ParagraphType }
                 *     
                 */
                public ParagraphType getComments() {
                    return comments;
                }

                /**
                 * Define el valor de la propiedad comments.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ParagraphType }
                 *     
                 */
                public void setComments(ParagraphType value) {
                    this.comments = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "location",
                    "category"
                })
                public static class LocationCategories {

                    @XmlElement(name = "Location")
                    protected LocationGeneralType location;
                    @XmlElement(name = "Category")
                    protected List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories.Category> category;

                    /**
                     * Obtiene el valor de la propiedad location.
                     * 
                     * @return
                     *     possible object is
                     *     {@link LocationGeneralType }
                     *     
                     */
                    public LocationGeneralType getLocation() {
                        return location;
                    }

                    /**
                     * Define el valor de la propiedad location.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link LocationGeneralType }
                     *     
                     */
                    public void setLocation(LocationGeneralType value) {
                        this.location = value;
                    }

                    /**
                     * Gets the value of the category property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the category property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getCategory().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories.Category }
                     * 
                     * 
                     */
                    public List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories.Category> getCategory() {
                        if (category == null) {
                            category = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories.Category>();
                        }
                        return this.category;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                     *       &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Category
                        extends FormattedTextTextType
                    {

                        @XmlAttribute(name = "LocationCategoryCode")
                        protected String locationCategoryCode;
                        @XmlAttribute(name = "PreferLevel")
                        protected PreferLevelType preferLevel;

                        /**
                         * Obtiene el valor de la propiedad locationCategoryCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLocationCategoryCode() {
                            return locationCategoryCode;
                        }

                        /**
                         * Define el valor de la propiedad locationCategoryCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLocationCategoryCode(String value) {
                            this.locationCategoryCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad preferLevel.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public PreferLevelType getPreferLevel() {
                            return preferLevel;
                        }

                        /**
                         * Define el valor de la propiedad preferLevel.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public void setPreferLevel(PreferLevelType value) {
                            this.preferLevel = value;
                        }

                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Property" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "property"
                })
                public static class Properties {

                    @XmlElement(name = "Property", required = true)
                    protected List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties.Property> property;

                    /**
                     * Gets the value of the property property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the property property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getProperty().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties.Property }
                     * 
                     * 
                     */
                    public List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties.Property> getProperty() {
                        if (property == null) {
                            property = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Properties.Property>();
                        }
                        return this.property;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                     *       &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Property {

                        @XmlAttribute(name = "PropertyTypeCode")
                        protected String propertyTypeCode;
                        @XmlAttribute(name = "PreferLevel")
                        protected PreferLevelType preferLevel;

                        /**
                         * Obtiene el valor de la propiedad propertyTypeCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPropertyTypeCode() {
                            return propertyTypeCode;
                        }

                        /**
                         * Define el valor de la propiedad propertyTypeCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPropertyTypeCode(String value) {
                            this.propertyTypeCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad preferLevel.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public PreferLevelType getPreferLevel() {
                            return preferLevel;
                        }

                        /**
                         * Define el valor de la propiedad preferLevel.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public void setPreferLevel(PreferLevelType value) {
                            this.preferLevel = value;
                        }

                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Recreation" maxOccurs="unbounded"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "recreation"
                })
                public static class Recreations {

                    @XmlElement(name = "Recreation", required = true)
                    protected List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations.Recreation> recreation;

                    /**
                     * Gets the value of the recreation property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the recreation property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getRecreation().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations.Recreation }
                     * 
                     * 
                     */
                    public List<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations.Recreation> getRecreation() {
                        if (recreation == null) {
                            recreation = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.Recreations.Recreation>();
                        }
                        return this.recreation;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                     *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Recreation {

                        @XmlAttribute(name = "Code")
                        protected String code;
                        @XmlAttribute(name = "PreferLevel")
                        protected PreferLevelType preferLevel;

                        /**
                         * Obtiene el valor de la propiedad code.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCode() {
                            return code;
                        }

                        /**
                         * Define el valor de la propiedad code.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCode(String value) {
                            this.code = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad preferLevel.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public PreferLevelType getPreferLevel() {
                            return preferLevel;
                        }

                        /**
                         * Define el valor de la propiedad preferLevel.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public void setPreferLevel(PreferLevelType value) {
                            this.preferLevel = value;
                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="KeyDecisionFactors" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="KeyDecisionFactor" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                           &amp;lt;attribute name="Ranking" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Presentation" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="CustomQuestions" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="CustomQuestion" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProposalGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "keyDecisionFactors",
            "presentation",
            "comments",
            "customQuestions"
        })
        public static class ProposalSpecification {

            @XmlElement(name = "KeyDecisionFactors")
            protected RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors keyDecisionFactors;
            @XmlElement(name = "Presentation")
            protected RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.Presentation presentation;
            @XmlElement(name = "Comments")
            protected List<ParagraphType> comments;
            @XmlElement(name = "CustomQuestions")
            protected RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.CustomQuestions customQuestions;
            @XmlAttribute(name = "ResponseDue")
            protected String responseDue;
            @XmlAttribute(name = "DecisionDue")
            protected String decisionDue;
            @XmlAttribute(name = "ResponseFormat")
            protected String responseFormat;
            @XmlAttribute(name = "ResponseLanguage")
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlSchemaType(name = "language")
            protected String responseLanguage;
            @XmlAttribute(name = "PreliminaryCutIndicator")
            protected Boolean preliminaryCutIndicator;
            @XmlAttribute(name = "PreliminaryCutDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar preliminaryCutDate;
            @XmlAttribute(name = "RFP_DistributionDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar rfpDistributionDate;
            @XmlAttribute(name = "RFP_PublishedDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar rfpPublishedDate;
            @XmlAttribute(name = "RFP_CreationDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar rfpCreationDate;

            /**
             * Obtiene el valor de la propiedad keyDecisionFactors.
             * 
             * @return
             *     possible object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors }
             *     
             */
            public RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors getKeyDecisionFactors() {
                return keyDecisionFactors;
            }

            /**
             * Define el valor de la propiedad keyDecisionFactors.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors }
             *     
             */
            public void setKeyDecisionFactors(RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors value) {
                this.keyDecisionFactors = value;
            }

            /**
             * Obtiene el valor de la propiedad presentation.
             * 
             * @return
             *     possible object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.Presentation }
             *     
             */
            public RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.Presentation getPresentation() {
                return presentation;
            }

            /**
             * Define el valor de la propiedad presentation.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.Presentation }
             *     
             */
            public void setPresentation(RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.Presentation value) {
                this.presentation = value;
            }

            /**
             * Gets the value of the comments property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comments property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComments().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComments() {
                if (comments == null) {
                    comments = new ArrayList<ParagraphType>();
                }
                return this.comments;
            }

            /**
             * Obtiene el valor de la propiedad customQuestions.
             * 
             * @return
             *     possible object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.CustomQuestions }
             *     
             */
            public RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.CustomQuestions getCustomQuestions() {
                return customQuestions;
            }

            /**
             * Define el valor de la propiedad customQuestions.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.CustomQuestions }
             *     
             */
            public void setCustomQuestions(RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.CustomQuestions value) {
                this.customQuestions = value;
            }

            /**
             * Obtiene el valor de la propiedad responseDue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResponseDue() {
                return responseDue;
            }

            /**
             * Define el valor de la propiedad responseDue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResponseDue(String value) {
                this.responseDue = value;
            }

            /**
             * Obtiene el valor de la propiedad decisionDue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDecisionDue() {
                return decisionDue;
            }

            /**
             * Define el valor de la propiedad decisionDue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDecisionDue(String value) {
                this.decisionDue = value;
            }

            /**
             * Obtiene el valor de la propiedad responseFormat.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResponseFormat() {
                return responseFormat;
            }

            /**
             * Define el valor de la propiedad responseFormat.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResponseFormat(String value) {
                this.responseFormat = value;
            }

            /**
             * Obtiene el valor de la propiedad responseLanguage.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResponseLanguage() {
                return responseLanguage;
            }

            /**
             * Define el valor de la propiedad responseLanguage.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResponseLanguage(String value) {
                this.responseLanguage = value;
            }

            /**
             * Obtiene el valor de la propiedad preliminaryCutIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPreliminaryCutIndicator() {
                return preliminaryCutIndicator;
            }

            /**
             * Define el valor de la propiedad preliminaryCutIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPreliminaryCutIndicator(Boolean value) {
                this.preliminaryCutIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad preliminaryCutDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getPreliminaryCutDate() {
                return preliminaryCutDate;
            }

            /**
             * Define el valor de la propiedad preliminaryCutDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setPreliminaryCutDate(XMLGregorianCalendar value) {
                this.preliminaryCutDate = value;
            }

            /**
             * Obtiene el valor de la propiedad rfpDistributionDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getRFPDistributionDate() {
                return rfpDistributionDate;
            }

            /**
             * Define el valor de la propiedad rfpDistributionDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setRFPDistributionDate(XMLGregorianCalendar value) {
                this.rfpDistributionDate = value;
            }

            /**
             * Obtiene el valor de la propiedad rfpPublishedDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getRFPPublishedDate() {
                return rfpPublishedDate;
            }

            /**
             * Define el valor de la propiedad rfpPublishedDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setRFPPublishedDate(XMLGregorianCalendar value) {
                this.rfpPublishedDate = value;
            }

            /**
             * Obtiene el valor de la propiedad rfpCreationDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getRFPCreationDate() {
                return rfpCreationDate;
            }

            /**
             * Define el valor de la propiedad rfpCreationDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setRFPCreationDate(XMLGregorianCalendar value) {
                this.rfpCreationDate = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="CustomQuestion" type="{http://www.opentravel.org/OTA/2003/05}CustomQuestionType" maxOccurs="unbounded"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "customQuestion"
            })
            public static class CustomQuestions {

                @XmlElement(name = "CustomQuestion", required = true)
                protected List<CustomQuestionType> customQuestion;

                /**
                 * Gets the value of the customQuestion property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the customQuestion property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getCustomQuestion().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link CustomQuestionType }
                 * 
                 * 
                 */
                public List<CustomQuestionType> getCustomQuestion() {
                    if (customQuestion == null) {
                        customQuestion = new ArrayList<CustomQuestionType>();
                    }
                    return this.customQuestion;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="KeyDecisionFactor" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                 &amp;lt;attribute name="Ranking" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "keyDecisionFactor"
            })
            public static class KeyDecisionFactors {

                @XmlElement(name = "KeyDecisionFactor", required = true)
                protected List<RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors.KeyDecisionFactor> keyDecisionFactor;

                /**
                 * Gets the value of the keyDecisionFactor property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the keyDecisionFactor property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getKeyDecisionFactor().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors.KeyDecisionFactor }
                 * 
                 * 
                 */
                public List<RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors.KeyDecisionFactor> getKeyDecisionFactor() {
                    if (keyDecisionFactor == null) {
                        keyDecisionFactor = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.ProposalSpecification.KeyDecisionFactors.KeyDecisionFactor>();
                    }
                    return this.keyDecisionFactor;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *       &amp;lt;attribute name="Ranking" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class KeyDecisionFactor {

                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "Ranking")
                    protected BigInteger ranking;

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad ranking.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getRanking() {
                        return ranking;
                    }

                    /**
                     * Define el valor de la propiedad ranking.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setRanking(BigInteger value) {
                        this.ranking = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "location"
            })
            public static class Presentation {

                @XmlElement(name = "Location")
                protected LocationGeneralType location;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad location.
                 * 
                 * @return
                 *     possible object is
                 *     {@link LocationGeneralType }
                 *     
                 */
                public LocationGeneralType getLocation() {
                    return location;
                }

                /**
                 * Define el valor de la propiedad location.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link LocationGeneralType }
                 *     
                 */
                public void setLocation(LocationGeneralType value) {
                    this.location = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *       &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class RFPID
            extends UniqueIDType
        {

            @XmlAttribute(name = "MeetingName")
            protected String meetingName;

            /**
             * Obtiene el valor de la propiedad meetingName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMeetingName() {
                return meetingName;
            }

            /**
             * Define el valor de la propiedad meetingName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMeetingName(String value) {
                this.meetingName = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="BudgetedRoomRate" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
         *                 &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="RequestedRateType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ReservationMethod" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="StayDays" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="StayDayRooms" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="StayDayRoom" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                               &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comments" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="ConcessionsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "budgetedRoomRate",
            "reservationMethod",
            "stayDays",
            "comments"
        })
        public static class RoomBlock {

            @XmlElement(name = "BudgetedRoomRate")
            protected List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.BudgetedRoomRate> budgetedRoomRate;
            @XmlElement(name = "ReservationMethod")
            protected List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.ReservationMethod> reservationMethod;
            @XmlElement(name = "StayDays")
            protected RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays stayDays;
            @XmlElement(name = "Comments")
            protected ParagraphType comments;
            @XmlAttribute(name = "TotalRoomNightQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger totalRoomNightQuantity;
            @XmlAttribute(name = "PeakRoomNightQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger peakRoomNightQuantity;
            @XmlAttribute(name = "ConcessionsIndicator")
            protected Boolean concessionsIndicator;

            /**
             * Gets the value of the budgetedRoomRate property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the budgetedRoomRate property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBudgetedRoomRate().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.BudgetedRoomRate }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.BudgetedRoomRate> getBudgetedRoomRate() {
                if (budgetedRoomRate == null) {
                    budgetedRoomRate = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.BudgetedRoomRate>();
                }
                return this.budgetedRoomRate;
            }

            /**
             * Gets the value of the reservationMethod property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reservationMethod property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getReservationMethod().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.ReservationMethod }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.ReservationMethod> getReservationMethod() {
                if (reservationMethod == null) {
                    reservationMethod = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.ReservationMethod>();
                }
                return this.reservationMethod;
            }

            /**
             * Obtiene el valor de la propiedad stayDays.
             * 
             * @return
             *     possible object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays }
             *     
             */
            public RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays getStayDays() {
                return stayDays;
            }

            /**
             * Define el valor de la propiedad stayDays.
             * 
             * @param value
             *     allowed object is
             *     {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays }
             *     
             */
            public void setStayDays(RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays value) {
                this.stayDays = value;
            }

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link ParagraphType }
             *     
             */
            public ParagraphType getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link ParagraphType }
             *     
             */
            public void setComments(ParagraphType value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad totalRoomNightQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTotalRoomNightQuantity() {
                return totalRoomNightQuantity;
            }

            /**
             * Define el valor de la propiedad totalRoomNightQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTotalRoomNightQuantity(BigInteger value) {
                this.totalRoomNightQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad peakRoomNightQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPeakRoomNightQuantity() {
                return peakRoomNightQuantity;
            }

            /**
             * Define el valor de la propiedad peakRoomNightQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPeakRoomNightQuantity(BigInteger value) {
                this.peakRoomNightQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad concessionsIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isConcessionsIndicator() {
                return concessionsIndicator;
            }

            /**
             * Define el valor de la propiedad concessionsIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setConcessionsIndicator(Boolean value) {
                this.concessionsIndicator = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BudgetInclusiveGroup"/&amp;gt;
             *       &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="RequestedRateType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class BudgetedRoomRate {

                @XmlAttribute(name = "TotalRoomNightQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger totalRoomNightQuantity;
                @XmlAttribute(name = "RequestedRateType")
                protected String requestedRateType;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;
                @XmlAttribute(name = "IncludesTaxInd")
                protected Boolean includesTaxInd;
                @XmlAttribute(name = "IncludesServiceFeesInd")
                protected Boolean includesServiceFeesInd;
                @XmlAttribute(name = "IncludesGratuityInd")
                protected Boolean includesGratuityInd;

                /**
                 * Obtiene el valor de la propiedad totalRoomNightQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTotalRoomNightQuantity() {
                    return totalRoomNightQuantity;
                }

                /**
                 * Define el valor de la propiedad totalRoomNightQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTotalRoomNightQuantity(BigInteger value) {
                    this.totalRoomNightQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad requestedRateType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestedRateType() {
                    return requestedRateType;
                }

                /**
                 * Define el valor de la propiedad requestedRateType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestedRateType(String value) {
                    this.requestedRateType = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

                /**
                 * Obtiene el valor de la propiedad includesTaxInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIncludesTaxInd() {
                    return includesTaxInd;
                }

                /**
                 * Define el valor de la propiedad includesTaxInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIncludesTaxInd(Boolean value) {
                    this.includesTaxInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad includesServiceFeesInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIncludesServiceFeesInd() {
                    return includesServiceFeesInd;
                }

                /**
                 * Define el valor de la propiedad includesServiceFeesInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIncludesServiceFeesInd(Boolean value) {
                    this.includesServiceFeesInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad includesGratuityInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isIncludesGratuityInd() {
                    return includesGratuityInd;
                }

                /**
                 * Define el valor de la propiedad includesGratuityInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setIncludesGratuityInd(Boolean value) {
                    this.includesGratuityInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ReservationMethod {

                @XmlAttribute(name = "Code")
                protected String code;

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="StayDayRooms" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="StayDayRoom" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                     &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "stayDay"
            })
            public static class StayDays {

                @XmlElement(name = "StayDay", required = true)
                protected List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay> stayDay;
                @XmlAttribute(name = "FirstStayDayOfWeek")
                protected DayOfWeekType firstStayDayOfWeek;

                /**
                 * Gets the value of the stayDay property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stayDay property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getStayDay().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay }
                 * 
                 * 
                 */
                public List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay> getStayDay() {
                    if (stayDay == null) {
                        stayDay = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay>();
                    }
                    return this.stayDay;
                }

                /**
                 * Obtiene el valor de la propiedad firstStayDayOfWeek.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public DayOfWeekType getFirstStayDayOfWeek() {
                    return firstStayDayOfWeek;
                }

                /**
                 * Define el valor de la propiedad firstStayDayOfWeek.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public void setFirstStayDayOfWeek(DayOfWeekType value) {
                    this.firstStayDayOfWeek = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="StayDayRooms" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="StayDayRoom" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                           &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "stayDayRooms"
                })
                public static class StayDay {

                    @XmlElement(name = "StayDayRooms")
                    protected RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms stayDayRooms;
                    @XmlAttribute(name = "DayNumber", required = true)
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger dayNumber;
                    @XmlAttribute(name = "GuestQuantity")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger guestQuantity;
                    @XmlAttribute(name = "TotalNumberOfUnits")
                    protected BigInteger totalNumberOfUnits;

                    /**
                     * Obtiene el valor de la propiedad stayDayRooms.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms }
                     *     
                     */
                    public RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms getStayDayRooms() {
                        return stayDayRooms;
                    }

                    /**
                     * Define el valor de la propiedad stayDayRooms.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms }
                     *     
                     */
                    public void setStayDayRooms(RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms value) {
                        this.stayDayRooms = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dayNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getDayNumber() {
                        return dayNumber;
                    }

                    /**
                     * Define el valor de la propiedad dayNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setDayNumber(BigInteger value) {
                        this.dayNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad guestQuantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getGuestQuantity() {
                        return guestQuantity;
                    }

                    /**
                     * Define el valor de la propiedad guestQuantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setGuestQuantity(BigInteger value) {
                        this.guestQuantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad totalNumberOfUnits.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getTotalNumberOfUnits() {
                        return totalNumberOfUnits;
                    }

                    /**
                     * Define el valor de la propiedad totalNumberOfUnits.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setTotalNumberOfUnits(BigInteger value) {
                        this.totalNumberOfUnits = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="StayDayRoom" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                 &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stayDayRoom"
                    })
                    public static class StayDayRooms {

                        @XmlElement(name = "StayDayRoom", required = true)
                        protected List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms.StayDayRoom> stayDayRoom;

                        /**
                         * Gets the value of the stayDayRoom property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stayDayRoom property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getStayDayRoom().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms.StayDayRoom }
                         * 
                         * 
                         */
                        public List<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms.StayDayRoom> getStayDayRoom() {
                            if (stayDayRoom == null) {
                                stayDayRoom = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.RoomBlock.StayDays.StayDay.StayDayRooms.StayDayRoom>();
                            }
                            return this.stayDayRoom;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *       &amp;lt;attribute name="RoomTypeRequirement" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *       &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "comment"
                        })
                        public static class StayDayRoom {

                            @XmlElement(name = "Comment")
                            protected ParagraphType comment;
                            @XmlAttribute(name = "RoomTypeRequirement")
                            protected String roomTypeRequirement;
                            @XmlAttribute(name = "NumberOfUnits")
                            protected BigInteger numberOfUnits;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "DecimalPlaces")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger decimalPlaces;
                            @XmlAttribute(name = "Amount")
                            protected BigDecimal amount;

                            /**
                             * Obtiene el valor de la propiedad comment.
                             * 
                             * @return
                             *     possible object is
                             *     {@link ParagraphType }
                             *     
                             */
                            public ParagraphType getComment() {
                                return comment;
                            }

                            /**
                             * Define el valor de la propiedad comment.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link ParagraphType }
                             *     
                             */
                            public void setComment(ParagraphType value) {
                                this.comment = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad roomTypeRequirement.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getRoomTypeRequirement() {
                                return roomTypeRequirement;
                            }

                            /**
                             * Define el valor de la propiedad roomTypeRequirement.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setRoomTypeRequirement(String value) {
                                this.roomTypeRequirement = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad numberOfUnits.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getNumberOfUnits() {
                                return numberOfUnits;
                            }

                            /**
                             * Define el valor de la propiedad numberOfUnits.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setNumberOfUnits(BigInteger value) {
                                this.numberOfUnits = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad currencyCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Define el valor de la propiedad currencyCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad decimalPlaces.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDecimalPlaces() {
                                return decimalPlaces;
                            }

                            /**
                             * Define el valor de la propiedad decimalPlaces.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDecimalPlaces(BigInteger value) {
                                this.decimalPlaces = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setAmount(BigDecimal value) {
                                this.amount = value;
                            }

                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Site" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Promotions" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Promotion" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="AvailableDateRange"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
         *                                               &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="OfferedDateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
         *                                       &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="UserAcknowledgementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                     &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                                     &amp;lt;attribute name="AdvertisedRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                     &amp;lt;attribute name="AdvertisedHighAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                     &amp;lt;attribute name="AdvertisedLowAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *                 &amp;lt;attribute name="ApproxSiteInspectionDate" type="{http://www.w3.org/2001/XMLSchema}gYearMonth" /&amp;gt;
         *                 &amp;lt;attribute name="SiteInspectionAttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "site"
        })
        public static class Sites {

            @XmlElement(name = "Site", required = true)
            protected List<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site> site;

            /**
             * Gets the value of the site property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the site property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSite().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site }
             * 
             * 
             */
            public List<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site> getSite() {
                if (site == null) {
                    site = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site>();
                }
                return this.site;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ContactInfos" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Promotions" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Promotion" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="AvailableDateRange"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
             *                                     &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="OfferedDateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
             *                             &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
             *                           &amp;lt;attribute name="UserAcknowledgementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                           &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *                           &amp;lt;attribute name="AdvertisedRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                           &amp;lt;attribute name="AdvertisedHighAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                           &amp;lt;attribute name="AdvertisedLowAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="AdditionalInfos" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
             *       &amp;lt;attribute name="ApproxSiteInspectionDate" type="{http://www.w3.org/2001/XMLSchema}gYearMonth" /&amp;gt;
             *       &amp;lt;attribute name="SiteInspectionAttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "contactInfos",
                "promotions",
                "additionalInfos"
            })
            public static class Site {

                @XmlElement(name = "ContactInfos")
                protected RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.ContactInfos contactInfos;
                @XmlElement(name = "Promotions")
                protected RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions promotions;
                @XmlElement(name = "AdditionalInfos")
                protected RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos additionalInfos;
                @XmlAttribute(name = "ApproxSiteInspectionDate")
                @XmlSchemaType(name = "gYearMonth")
                protected XMLGregorianCalendar approxSiteInspectionDate;
                @XmlAttribute(name = "SiteInspectionAttendeeQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger siteInspectionAttendeeQuantity;
                @XmlAttribute(name = "ChainCode")
                protected String chainCode;
                @XmlAttribute(name = "BrandCode")
                protected String brandCode;
                @XmlAttribute(name = "HotelCode")
                protected String hotelCode;
                @XmlAttribute(name = "HotelCityCode")
                protected String hotelCityCode;
                @XmlAttribute(name = "HotelName")
                protected String hotelName;
                @XmlAttribute(name = "HotelCodeContext")
                protected String hotelCodeContext;
                @XmlAttribute(name = "ChainName")
                protected String chainName;
                @XmlAttribute(name = "BrandName")
                protected String brandName;
                @XmlAttribute(name = "AreaID")
                protected String areaID;
                @XmlAttribute(name = "TTIcode")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger ttIcode;

                /**
                 * Obtiene el valor de la propiedad contactInfos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.ContactInfos }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.ContactInfos getContactInfos() {
                    return contactInfos;
                }

                /**
                 * Define el valor de la propiedad contactInfos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.ContactInfos }
                 *     
                 */
                public void setContactInfos(RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.ContactInfos value) {
                    this.contactInfos = value;
                }

                /**
                 * Obtiene el valor de la propiedad promotions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions getPromotions() {
                    return promotions;
                }

                /**
                 * Define el valor de la propiedad promotions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions }
                 *     
                 */
                public void setPromotions(RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions value) {
                    this.promotions = value;
                }

                /**
                 * Obtiene el valor de la propiedad additionalInfos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos }
                 *     
                 */
                public RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos getAdditionalInfos() {
                    return additionalInfos;
                }

                /**
                 * Define el valor de la propiedad additionalInfos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos }
                 *     
                 */
                public void setAdditionalInfos(RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos value) {
                    this.additionalInfos = value;
                }

                /**
                 * Obtiene el valor de la propiedad approxSiteInspectionDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getApproxSiteInspectionDate() {
                    return approxSiteInspectionDate;
                }

                /**
                 * Define el valor de la propiedad approxSiteInspectionDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setApproxSiteInspectionDate(XMLGregorianCalendar value) {
                    this.approxSiteInspectionDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad siteInspectionAttendeeQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getSiteInspectionAttendeeQuantity() {
                    return siteInspectionAttendeeQuantity;
                }

                /**
                 * Define el valor de la propiedad siteInspectionAttendeeQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setSiteInspectionAttendeeQuantity(BigInteger value) {
                    this.siteInspectionAttendeeQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad chainCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getChainCode() {
                    return chainCode;
                }

                /**
                 * Define el valor de la propiedad chainCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setChainCode(String value) {
                    this.chainCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad brandCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBrandCode() {
                    return brandCode;
                }

                /**
                 * Define el valor de la propiedad brandCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBrandCode(String value) {
                    this.brandCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelCode() {
                    return hotelCode;
                }

                /**
                 * Define el valor de la propiedad hotelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelCode(String value) {
                    this.hotelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelCityCode() {
                    return hotelCityCode;
                }

                /**
                 * Define el valor de la propiedad hotelCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelCityCode(String value) {
                    this.hotelCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelName() {
                    return hotelName;
                }

                /**
                 * Define el valor de la propiedad hotelName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelName(String value) {
                    this.hotelName = value;
                }

                /**
                 * Obtiene el valor de la propiedad hotelCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHotelCodeContext() {
                    return hotelCodeContext;
                }

                /**
                 * Define el valor de la propiedad hotelCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHotelCodeContext(String value) {
                    this.hotelCodeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad chainName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getChainName() {
                    return chainName;
                }

                /**
                 * Define el valor de la propiedad chainName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setChainName(String value) {
                    this.chainName = value;
                }

                /**
                 * Obtiene el valor de la propiedad brandName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBrandName() {
                    return brandName;
                }

                /**
                 * Define el valor de la propiedad brandName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBrandName(String value) {
                    this.brandName = value;
                }

                /**
                 * Obtiene el valor de la propiedad areaID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAreaID() {
                    return areaID;
                }

                /**
                 * Define el valor de la propiedad areaID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAreaID(String value) {
                    this.areaID = value;
                }

                /**
                 * Obtiene el valor de la propiedad ttIcode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTTIcode() {
                    return ttIcode;
                }

                /**
                 * Define el valor de la propiedad ttIcode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTTIcode(BigInteger value) {
                    this.ttIcode = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="AdditionalInfo" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "additionalInfo"
                })
                public static class AdditionalInfos {

                    @XmlElement(name = "AdditionalInfo", required = true)
                    protected List<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos.AdditionalInfo> additionalInfo;

                    /**
                     * Gets the value of the additionalInfo property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additionalInfo property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getAdditionalInfo().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos.AdditionalInfo }
                     * 
                     * 
                     */
                    public List<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos.AdditionalInfo> getAdditionalInfo() {
                        if (additionalInfo == null) {
                            additionalInfo = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.AdditionalInfos.AdditionalInfo>();
                        }
                        return this.additionalInfo;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AdditionalInfoGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class AdditionalInfo {

                        @XmlAttribute(name = "AddtionalInfoCode")
                        protected String addtionalInfoCode;
                        @XmlAttribute(name = "DeliveryMethodCode")
                        protected String deliveryMethodCode;

                        /**
                         * Obtiene el valor de la propiedad addtionalInfoCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getAddtionalInfoCode() {
                            return addtionalInfoCode;
                        }

                        /**
                         * Define el valor de la propiedad addtionalInfoCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setAddtionalInfoCode(String value) {
                            this.addtionalInfoCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad deliveryMethodCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getDeliveryMethodCode() {
                            return deliveryMethodCode;
                        }

                        /**
                         * Define el valor de la propiedad deliveryMethodCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setDeliveryMethodCode(String value) {
                            this.deliveryMethodCode = value;
                        }

                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "contactInfo"
                })
                public static class ContactInfos {

                    @XmlElement(name = "ContactInfo", required = true)
                    protected List<ContactPersonType> contactInfo;

                    /**
                     * Gets the value of the contactInfo property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contactInfo property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getContactInfo().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link ContactPersonType }
                     * 
                     * 
                     */
                    public List<ContactPersonType> getContactInfo() {
                        if (contactInfo == null) {
                            contactInfo = new ArrayList<ContactPersonType>();
                        }
                        return this.contactInfo;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Promotion" maxOccurs="99"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="AvailableDateRange"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
                 *                           &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="OfferedDateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
                 *                   &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="UserAcknowledgementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                 &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *                 &amp;lt;attribute name="AdvertisedRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                 &amp;lt;attribute name="AdvertisedHighAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                 &amp;lt;attribute name="AdvertisedLowAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "promotion"
                })
                public static class Promotions {

                    @XmlElement(name = "Promotion", required = true)
                    protected List<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion> promotion;

                    /**
                     * Gets the value of the promotion property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotion property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getPromotion().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion }
                     * 
                     * 
                     */
                    public List<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion> getPromotion() {
                        if (promotion == null) {
                            promotion = new ArrayList<RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion>();
                        }
                        return this.promotion;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="AvailableDateRange"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
                     *                 &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="OfferedDateRange" type="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"/&amp;gt;
                     *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                     *       &amp;lt;attribute name="UserAcknowledgementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *       &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                     *       &amp;lt;attribute name="AdvertisedRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *       &amp;lt;attribute name="AdvertisedHighAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *       &amp;lt;attribute name="AdvertisedLowAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "availableDateRange",
                        "offeredDateRange",
                        "description"
                    })
                    public static class Promotion {

                        @XmlElement(name = "AvailableDateRange", required = true)
                        protected RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion.AvailableDateRange availableDateRange;
                        @XmlElement(name = "OfferedDateRange", required = true)
                        protected DateTimeSpanType offeredDateRange;
                        @XmlElement(name = "Description", required = true)
                        protected ParagraphType description;
                        @XmlAttribute(name = "UserAcknowledgementIndicator")
                        protected Boolean userAcknowledgementIndicator;
                        @XmlAttribute(name = "PromotionCode")
                        protected String promotionCode;
                        @XmlAttribute(name = "AdvertisedRoomQuantity")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger advertisedRoomQuantity;
                        @XmlAttribute(name = "AdvertisedHighAmount")
                        protected BigDecimal advertisedHighAmount;
                        @XmlAttribute(name = "AdvertisedLowAmount")
                        protected BigDecimal advertisedLowAmount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;
                        @XmlAttribute(name = "DecimalPlaces")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger decimalPlaces;

                        /**
                         * Obtiene el valor de la propiedad availableDateRange.
                         * 
                         * @return
                         *     possible object is
                         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion.AvailableDateRange }
                         *     
                         */
                        public RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion.AvailableDateRange getAvailableDateRange() {
                            return availableDateRange;
                        }

                        /**
                         * Define el valor de la propiedad availableDateRange.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion.AvailableDateRange }
                         *     
                         */
                        public void setAvailableDateRange(RFPRequestSegmentsType.RFPRequestSegment.Sites.Site.Promotions.Promotion.AvailableDateRange value) {
                            this.availableDateRange = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad offeredDateRange.
                         * 
                         * @return
                         *     possible object is
                         *     {@link DateTimeSpanType }
                         *     
                         */
                        public DateTimeSpanType getOfferedDateRange() {
                            return offeredDateRange;
                        }

                        /**
                         * Define el valor de la propiedad offeredDateRange.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link DateTimeSpanType }
                         *     
                         */
                        public void setOfferedDateRange(DateTimeSpanType value) {
                            this.offeredDateRange = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad description.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ParagraphType }
                         *     
                         */
                        public ParagraphType getDescription() {
                            return description;
                        }

                        /**
                         * Define el valor de la propiedad description.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ParagraphType }
                         *     
                         */
                        public void setDescription(ParagraphType value) {
                            this.description = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad userAcknowledgementIndicator.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isUserAcknowledgementIndicator() {
                            return userAcknowledgementIndicator;
                        }

                        /**
                         * Define el valor de la propiedad userAcknowledgementIndicator.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setUserAcknowledgementIndicator(Boolean value) {
                            this.userAcknowledgementIndicator = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad promotionCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPromotionCode() {
                            return promotionCode;
                        }

                        /**
                         * Define el valor de la propiedad promotionCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPromotionCode(String value) {
                            this.promotionCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad advertisedRoomQuantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getAdvertisedRoomQuantity() {
                            return advertisedRoomQuantity;
                        }

                        /**
                         * Define el valor de la propiedad advertisedRoomQuantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setAdvertisedRoomQuantity(BigInteger value) {
                            this.advertisedRoomQuantity = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad advertisedHighAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getAdvertisedHighAmount() {
                            return advertisedHighAmount;
                        }

                        /**
                         * Define el valor de la propiedad advertisedHighAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setAdvertisedHighAmount(BigDecimal value) {
                            this.advertisedHighAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad advertisedLowAmount.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getAdvertisedLowAmount() {
                            return advertisedLowAmount;
                        }

                        /**
                         * Define el valor de la propiedad advertisedLowAmount.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setAdvertisedLowAmount(BigDecimal value) {
                            this.advertisedLowAmount = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad currencyCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Define el valor de la propiedad currencyCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad decimalPlaces.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getDecimalPlaces() {
                            return decimalPlaces;
                        }

                        /**
                         * Define el valor de la propiedad decimalPlaces.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setDecimalPlaces(BigInteger value) {
                            this.decimalPlaces = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
                         *       &amp;lt;attribute name="DateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class AvailableDateRange
                            extends DateTimeSpanType
                        {

                            @XmlAttribute(name = "DateRPH")
                            protected String dateRPH;

                            /**
                             * Obtiene el valor de la propiedad dateRPH.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDateRPH() {
                                return dateRPH;
                            }

                            /**
                             * Define el valor de la propiedad dateRPH.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDateRPH(String value) {
                                this.dateRPH = value;
                            }

                        }

                    }

                }

            }

        }

    }

}
