
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Lodging information with ontology reference.
 * 
 * &lt;p&gt;Clase Java para OntologyLodgingType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OntologyLodgingType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Code" type="{http://www.opentravel.org/OTA/2003/05}OntologyCodeType"/&amp;gt;
 *         &amp;lt;element name="PropertyClass" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferPropertyClassType"&amp;gt;
 *                 &amp;lt;attribute name="OtherType"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OntologyLodgingType", propOrder = {
    "code",
    "propertyClass",
    "ontologyExtension"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.MultiModalOfferType.TripCharacteristics.Lodging.class
})
public class OntologyLodgingType {

    @XmlElement(name = "Code", required = true)
    protected OntologyCodeType code;
    @XmlElement(name = "PropertyClass")
    protected OntologyLodgingType.PropertyClass propertyClass;
    @XmlElement(name = "OntologyExtension")
    protected OntologyExtensionType ontologyExtension;

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link OntologyCodeType }
     *     
     */
    public OntologyCodeType getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyCodeType }
     *     
     */
    public void setCode(OntologyCodeType value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad propertyClass.
     * 
     * @return
     *     possible object is
     *     {@link OntologyLodgingType.PropertyClass }
     *     
     */
    public OntologyLodgingType.PropertyClass getPropertyClass() {
        return propertyClass;
    }

    /**
     * Define el valor de la propiedad propertyClass.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyLodgingType.PropertyClass }
     *     
     */
    public void setPropertyClass(OntologyLodgingType.PropertyClass value) {
        this.propertyClass = value;
    }

    /**
     * Obtiene el valor de la propiedad ontologyExtension.
     * 
     * @return
     *     possible object is
     *     {@link OntologyExtensionType }
     *     
     */
    public OntologyExtensionType getOntologyExtension() {
        return ontologyExtension;
    }

    /**
     * Define el valor de la propiedad ontologyExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyExtensionType }
     *     
     */
    public void setOntologyExtension(OntologyExtensionType value) {
        this.ontologyExtension = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferPropertyClassType"&amp;gt;
     *       &amp;lt;attribute name="OtherType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class PropertyClass {

        @XmlValue
        protected ListOfferPropertyClassType value;
        @XmlAttribute(name = "OtherType")
        protected String otherType;
        @XmlAttribute(name = "OntologyRefID")
        protected String ontologyRefID;

        /**
         * Source: Property Class Type (PCT) OpenTravel codelist.
         * 
         * @return
         *     possible object is
         *     {@link ListOfferPropertyClassType }
         *     
         */
        public ListOfferPropertyClassType getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfferPropertyClassType }
         *     
         */
        public void setValue(ListOfferPropertyClassType value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad otherType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOtherType() {
            return otherType;
        }

        /**
         * Define el valor de la propiedad otherType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOtherType(String value) {
            this.otherType = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyRefID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOntologyRefID() {
            return ontologyRefID;
        }

        /**
         * Define el valor de la propiedad ontologyRefID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOntologyRefID(String value) {
            this.ontologyRefID = value;
        }

    }

}
