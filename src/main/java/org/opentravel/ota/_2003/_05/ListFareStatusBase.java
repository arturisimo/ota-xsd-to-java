
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_FareStatus_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_FareStatus_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Constructed"/&amp;gt;
 *     &amp;lt;enumeration value="Created"/&amp;gt;
 *     &amp;lt;enumeration value="FareByRule"/&amp;gt;
 *     &amp;lt;enumeration value="FareByRulePrivate"/&amp;gt;
 *     &amp;lt;enumeration value="Published"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_FareStatus_Base")
@XmlEnum
public enum ListFareStatusBase {

    @XmlEnumValue("Constructed")
    CONSTRUCTED("Constructed"),
    @XmlEnumValue("Created")
    CREATED("Created"),

    /**
     * Specifies that the fare was built based on rules.
     * 
     */
    @XmlEnumValue("FareByRule")
    FARE_BY_RULE("FareByRule"),

    /**
     * The private fare was built by rules.
     * 
     */
    @XmlEnumValue("FareByRulePrivate")
    FARE_BY_RULE_PRIVATE("FareByRulePrivate"),
    @XmlEnumValue("Published")
    PUBLISHED("Published"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListFareStatusBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListFareStatusBase fromValue(String v) {
        for (ListFareStatusBase c: ListFareStatusBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
