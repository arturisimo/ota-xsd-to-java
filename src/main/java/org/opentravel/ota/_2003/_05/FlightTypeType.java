
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para FlightTypeType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="FlightTypeType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Nonstop"/&amp;gt;
 *     &amp;lt;enumeration value="Direct"/&amp;gt;
 *     &amp;lt;enumeration value="Connection"/&amp;gt;
 *     &amp;lt;enumeration value="SingleConnection"/&amp;gt;
 *     &amp;lt;enumeration value="DoubleConnection"/&amp;gt;
 *     &amp;lt;enumeration value="OneStopOnly"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "FlightTypeType")
@XmlEnum
public enum FlightTypeType {


    /**
     * Indicates the flight does not make any scheduled stops between 2 points.
     * 
     */
    @XmlEnumValue("Nonstop")
    NONSTOP("Nonstop"),

    /**
     * Indicates the flight makes a scheduled stop(s) between 2 points.
     * 
     */
    @XmlEnumValue("Direct")
    DIRECT("Direct"),

    /**
     * Indicates the flight will require a change of aircraft at a connecting point(s).
     * 
     */
    @XmlEnumValue("Connection")
    CONNECTION("Connection"),

    /**
     * A trip with only one connection.
     * 
     */
    @XmlEnumValue("SingleConnection")
    SINGLE_CONNECTION("SingleConnection"),

    /**
     * A trip with only two connections.
     * 
     */
    @XmlEnumValue("DoubleConnection")
    DOUBLE_CONNECTION("DoubleConnection"),

    /**
     * Indicates that the flight makes only one stop.
     * 
     */
    @XmlEnumValue("OneStopOnly")
    ONE_STOP_ONLY("OneStopOnly");
    private final String value;

    FlightTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlightTypeType fromValue(String v) {
        for (FlightTypeType c: FlightTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
