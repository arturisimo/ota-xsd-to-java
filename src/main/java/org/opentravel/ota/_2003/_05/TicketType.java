
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para TicketType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="TicketType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="eTicket"/&amp;gt;
 *     &amp;lt;enumeration value="Paper"/&amp;gt;
 *     &amp;lt;enumeration value="MCO"/&amp;gt;
 *     &amp;lt;enumeration value="EMD-A"/&amp;gt;
 *     &amp;lt;enumeration value="EMD-S"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "TicketType")
@XmlEnum
public enum TicketType {


    /**
     * An electronic ticket
     * 
     */
    @XmlEnumValue("eTicket")
    E_TICKET("eTicket"),

    /**
     * A paper ticket
     * 
     */
    @XmlEnumValue("Paper")
    PAPER("Paper"),

    /**
     * A miscellaneous charge order
     * 
     */
    MCO("MCO"),
    @XmlEnumValue("EMD-A")
    EMD_A("EMD-A"),
    @XmlEnumValue("EMD-S")
    EMD_S("EMD-S");
    private final String value;

    TicketType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TicketType fromValue(String v) {
        for (TicketType c: TicketType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
