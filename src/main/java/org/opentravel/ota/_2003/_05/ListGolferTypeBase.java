
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_GolferType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_GolferType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="ClubMember"/&amp;gt;
 *     &amp;lt;enumeration value="GuestOfMember"/&amp;gt;
 *     &amp;lt;enumeration value="GuestOfResort"/&amp;gt;
 *     &amp;lt;enumeration value="LocalResident"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyProgramMember"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_GolferType_Base")
@XmlEnum
public enum ListGolferTypeBase {


    /**
     * Member of a golf course.
     * 
     */
    @XmlEnumValue("ClubMember")
    CLUB_MEMBER("ClubMember"),

    /**
     * Guest of a golf course member.
     * 
     */
    @XmlEnumValue("GuestOfMember")
    GUEST_OF_MEMBER("GuestOfMember"),

    /**
     * Guest at a resort.
     * 
     */
    @XmlEnumValue("GuestOfResort")
    GUEST_OF_RESORT("GuestOfResort"),

    /**
     * Local area resident.
     * 
     */
    @XmlEnumValue("LocalResident")
    LOCAL_RESIDENT("LocalResident"),

    /**
     * Member of a golf course loyalty program.
     * 
     */
    @XmlEnumValue("LoyaltyProgramMember")
    LOYALTY_PROGRAM_MEMBER("LoyaltyProgramMember"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListGolferTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListGolferTypeBase fromValue(String v) {
        for (ListGolferTypeBase c: ListGolferTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
