
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para Enum_Status.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="Enum_Status"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Cancel_ConfirmedRequested"/&amp;gt;
 *     &amp;lt;enumeration value="Cancel_Listing"/&amp;gt;
 *     &amp;lt;enumeration value="Cancel_OnlyIfRequestedSegmentIsAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="CancellationRecommended"/&amp;gt;
 *     &amp;lt;enumeration value="CodeToReportActionTakenFromPreviousAccess"/&amp;gt;
 *     &amp;lt;enumeration value="IfHolding_ThenCancel"/&amp;gt;
 *     &amp;lt;enumeration value="IfNotHolding_ThenNeed"/&amp;gt;
 *     &amp;lt;enumeration value="IfNotHolding_ThenSold"/&amp;gt;
 *     &amp;lt;enumeration value="List_AddToWaitingList"/&amp;gt;
 *     &amp;lt;enumeration value="List_SpaceAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="Need"/&amp;gt;
 *     &amp;lt;enumeration value="Need_SegmentSpecifiedOrAlternative"/&amp;gt;
 *     &amp;lt;enumeration value="Sold"/&amp;gt;
 *     &amp;lt;enumeration value="Sold_OnFreeSaleBasis"/&amp;gt;
 *     &amp;lt;enumeration value="SpaceRequested"/&amp;gt;
 *     &amp;lt;enumeration value="Cancelled"/&amp;gt;
 *     &amp;lt;enumeration value="Confirming"/&amp;gt;
 *     &amp;lt;enumeration value="Confirming_FromWaitlist"/&amp;gt;
 *     &amp;lt;enumeration value="NoActionTaken"/&amp;gt;
 *     &amp;lt;enumeration value="Unable_HaveWaitlisted"/&amp;gt;
 *     &amp;lt;enumeration value="Unable_FlightDoesNotOperate"/&amp;gt;
 *     &amp;lt;enumeration value="Unable_ToAcceptSale"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlist_Closed"/&amp;gt;
 *     &amp;lt;enumeration value="Have_Listed"/&amp;gt;
 *     &amp;lt;enumeration value="Have_Requested"/&amp;gt;
 *     &amp;lt;enumeration value="HoldsConfirmed"/&amp;gt;
 *     &amp;lt;enumeration value="Reconfirmed"/&amp;gt;
 *     &amp;lt;enumeration value="HaveSold"/&amp;gt;
 *     &amp;lt;enumeration value="SpaceAlreadyRequested"/&amp;gt;
 *     &amp;lt;enumeration value="OK"/&amp;gt;
 *     &amp;lt;enumeration value="HaveCancelled"/&amp;gt;
 *     &amp;lt;enumeration value="PendingConfirmation"/&amp;gt;
 *     &amp;lt;enumeration value="DeferredFromWaitList"/&amp;gt;
 *     &amp;lt;enumeration value="StaffRequest"/&amp;gt;
 *     &amp;lt;enumeration value="Active"/&amp;gt;
 *     &amp;lt;enumeration value="Available"/&amp;gt;
 *     &amp;lt;enumeration value="BookOnBoard"/&amp;gt;
 *     &amp;lt;enumeration value="Closed"/&amp;gt;
 *     &amp;lt;enumeration value="Confirmed"/&amp;gt;
 *     &amp;lt;enumeration value="Declined"/&amp;gt;
 *     &amp;lt;enumeration value="NotApplicable"/&amp;gt;
 *     &amp;lt;enumeration value="Offered"/&amp;gt;
 *     &amp;lt;enumeration value="OnRequest"/&amp;gt;
 *     &amp;lt;enumeration value="Prepaid"/&amp;gt;
 *     &amp;lt;enumeration value="Shared"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlisted"/&amp;gt;
 *     &amp;lt;enumeration value="Guaranteed"/&amp;gt;
 *     &amp;lt;enumeration value="Held"/&amp;gt;
 *     &amp;lt;enumeration value="Booked"/&amp;gt;
 *     &amp;lt;enumeration value="Open"/&amp;gt;
 *     &amp;lt;enumeration value="SegmentConfirmedAfterScheduleChange"/&amp;gt;
 *     &amp;lt;enumeration value="SegmentConfirmed"/&amp;gt;
 *     &amp;lt;enumeration value="Overbook"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlisted_PriorityA"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlisted_PriorityB"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlisted_PriorityC"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlisted_PriorityD"/&amp;gt;
 *     &amp;lt;enumeration value="Jumpseat"/&amp;gt;
 *     &amp;lt;enumeration value="NeedGroupSpace"/&amp;gt;
 *     &amp;lt;enumeration value="PriorityGroup"/&amp;gt;
 *     &amp;lt;enumeration value="Unable_GroupWaitlisted"/&amp;gt;
 *     &amp;lt;enumeration value="Waitlist_Group"/&amp;gt;
 *     &amp;lt;enumeration value="SegmentWasWaitlist"/&amp;gt;
 *     &amp;lt;enumeration value="SegmentWasReconfirmed"/&amp;gt;
 *     &amp;lt;enumeration value="SegmentWasPending"/&amp;gt;
 *     &amp;lt;enumeration value="BookedOutsideSystem"/&amp;gt;
 *     &amp;lt;enumeration value="PendingCancel"/&amp;gt;
 *     &amp;lt;enumeration value="MandatoryNeed"/&amp;gt;
 *     &amp;lt;enumeration value="NeedProtection"/&amp;gt;
 *     &amp;lt;enumeration value="PendingRequest"/&amp;gt;
 *     &amp;lt;enumeration value="PendingCancellation"/&amp;gt;
 *     &amp;lt;enumeration value="StaffListingInSSR"/&amp;gt;
 *     &amp;lt;enumeration value="LinkSold_HistoryOnly"/&amp;gt;
 *     &amp;lt;enumeration value="CodeshareSold_HistoryOnly"/&amp;gt;
 *     &amp;lt;enumeration value="SuperGuaranteedSell_HistoryOnly"/&amp;gt;
 *     &amp;lt;enumeration value="Confirming_NewSchedule"/&amp;gt;
 *     &amp;lt;enumeration value="OnRequest_NewSchedule"/&amp;gt;
 *     &amp;lt;enumeration value="PlacingOnWaitlist_NewSchedule"/&amp;gt;
 *     &amp;lt;enumeration value="StandbyBoarded"/&amp;gt;
 *     &amp;lt;enumeration value="Flown"/&amp;gt;
 *     &amp;lt;enumeration value="NoRecordAvailable_NOREC"/&amp;gt;
 *     &amp;lt;enumeration value="GoShow"/&amp;gt;
 *     &amp;lt;enumeration value="NoShow"/&amp;gt;
 *     &amp;lt;enumeration value="OffloadFirmBooked"/&amp;gt;
 *     &amp;lt;enumeration value="OffloadOfGoShow"/&amp;gt;
 *     &amp;lt;enumeration value="Downgrade_ToEconomyClass"/&amp;gt;
 *     &amp;lt;enumeration value="DownUpgrade_ToBusinessClass"/&amp;gt;
 *     &amp;lt;enumeration value="Upgrade_ToFirstClass"/&amp;gt;
 *     &amp;lt;enumeration value="OffloadOfNoRecordPassenger"/&amp;gt;
 *     &amp;lt;enumeration value="HoldsConfirmed_ConditionalEMD_Required"/&amp;gt;
 *     &amp;lt;enumeration value="HoldsConfirmed_EMD_Required"/&amp;gt;
 *     &amp;lt;enumeration value="HoldsConfirmed_EMD_Issued"/&amp;gt;
 *     &amp;lt;enumeration value="HoldsConfirmed_NewSchedule_EMDAlreadyIssued"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "Enum_Status")
@XmlEnum
public enum EnumStatus {

    @XmlEnumValue("Cancel_ConfirmedRequested")
    CANCEL_CONFIRMED_REQUESTED("Cancel_ConfirmedRequested"),
    @XmlEnumValue("Cancel_Listing")
    CANCEL_LISTING("Cancel_Listing"),
    @XmlEnumValue("Cancel_OnlyIfRequestedSegmentIsAvailable")
    CANCEL_ONLY_IF_REQUESTED_SEGMENT_IS_AVAILABLE("Cancel_OnlyIfRequestedSegmentIsAvailable"),
    @XmlEnumValue("CancellationRecommended")
    CANCELLATION_RECOMMENDED("CancellationRecommended"),
    @XmlEnumValue("CodeToReportActionTakenFromPreviousAccess")
    CODE_TO_REPORT_ACTION_TAKEN_FROM_PREVIOUS_ACCESS("CodeToReportActionTakenFromPreviousAccess"),
    @XmlEnumValue("IfHolding_ThenCancel")
    IF_HOLDING_THEN_CANCEL("IfHolding_ThenCancel"),
    @XmlEnumValue("IfNotHolding_ThenNeed")
    IF_NOT_HOLDING_THEN_NEED("IfNotHolding_ThenNeed"),
    @XmlEnumValue("IfNotHolding_ThenSold")
    IF_NOT_HOLDING_THEN_SOLD("IfNotHolding_ThenSold"),
    @XmlEnumValue("List_AddToWaitingList")
    LIST_ADD_TO_WAITING_LIST("List_AddToWaitingList"),
    @XmlEnumValue("List_SpaceAvailable")
    LIST_SPACE_AVAILABLE("List_SpaceAvailable"),
    @XmlEnumValue("Need")
    NEED("Need"),
    @XmlEnumValue("Need_SegmentSpecifiedOrAlternative")
    NEED_SEGMENT_SPECIFIED_OR_ALTERNATIVE("Need_SegmentSpecifiedOrAlternative"),
    @XmlEnumValue("Sold")
    SOLD("Sold"),
    @XmlEnumValue("Sold_OnFreeSaleBasis")
    SOLD_ON_FREE_SALE_BASIS("Sold_OnFreeSaleBasis"),
    @XmlEnumValue("SpaceRequested")
    SPACE_REQUESTED("SpaceRequested"),
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled"),
    @XmlEnumValue("Confirming")
    CONFIRMING("Confirming"),
    @XmlEnumValue("Confirming_FromWaitlist")
    CONFIRMING_FROM_WAITLIST("Confirming_FromWaitlist"),
    @XmlEnumValue("NoActionTaken")
    NO_ACTION_TAKEN("NoActionTaken"),
    @XmlEnumValue("Unable_HaveWaitlisted")
    UNABLE_HAVE_WAITLISTED("Unable_HaveWaitlisted"),
    @XmlEnumValue("Unable_FlightDoesNotOperate")
    UNABLE_FLIGHT_DOES_NOT_OPERATE("Unable_FlightDoesNotOperate"),
    @XmlEnumValue("Unable_ToAcceptSale")
    UNABLE_TO_ACCEPT_SALE("Unable_ToAcceptSale"),
    @XmlEnumValue("Waitlist_Closed")
    WAITLIST_CLOSED("Waitlist_Closed"),
    @XmlEnumValue("Have_Listed")
    HAVE_LISTED("Have_Listed"),
    @XmlEnumValue("Have_Requested")
    HAVE_REQUESTED("Have_Requested"),
    @XmlEnumValue("HoldsConfirmed")
    HOLDS_CONFIRMED("HoldsConfirmed"),
    @XmlEnumValue("Reconfirmed")
    RECONFIRMED("Reconfirmed"),
    @XmlEnumValue("HaveSold")
    HAVE_SOLD("HaveSold"),
    @XmlEnumValue("SpaceAlreadyRequested")
    SPACE_ALREADY_REQUESTED("SpaceAlreadyRequested"),
    OK("OK"),
    @XmlEnumValue("HaveCancelled")
    HAVE_CANCELLED("HaveCancelled"),
    @XmlEnumValue("PendingConfirmation")
    PENDING_CONFIRMATION("PendingConfirmation"),
    @XmlEnumValue("DeferredFromWaitList")
    DEFERRED_FROM_WAIT_LIST("DeferredFromWaitList"),
    @XmlEnumValue("StaffRequest")
    STAFF_REQUEST("StaffRequest"),
    @XmlEnumValue("Active")
    ACTIVE("Active"),
    @XmlEnumValue("Available")
    AVAILABLE("Available"),
    @XmlEnumValue("BookOnBoard")
    BOOK_ON_BOARD("BookOnBoard"),
    @XmlEnumValue("Closed")
    CLOSED("Closed"),
    @XmlEnumValue("Confirmed")
    CONFIRMED("Confirmed"),
    @XmlEnumValue("Declined")
    DECLINED("Declined"),
    @XmlEnumValue("NotApplicable")
    NOT_APPLICABLE("NotApplicable"),
    @XmlEnumValue("Offered")
    OFFERED("Offered"),
    @XmlEnumValue("OnRequest")
    ON_REQUEST("OnRequest"),
    @XmlEnumValue("Prepaid")
    PREPAID("Prepaid"),
    @XmlEnumValue("Shared")
    SHARED("Shared"),
    @XmlEnumValue("Waitlisted")
    WAITLISTED("Waitlisted"),
    @XmlEnumValue("Guaranteed")
    GUARANTEED("Guaranteed"),
    @XmlEnumValue("Held")
    HELD("Held"),
    @XmlEnumValue("Booked")
    BOOKED("Booked"),
    @XmlEnumValue("Open")
    OPEN("Open"),
    @XmlEnumValue("SegmentConfirmedAfterScheduleChange")
    SEGMENT_CONFIRMED_AFTER_SCHEDULE_CHANGE("SegmentConfirmedAfterScheduleChange"),
    @XmlEnumValue("SegmentConfirmed")
    SEGMENT_CONFIRMED("SegmentConfirmed"),
    @XmlEnumValue("Overbook")
    OVERBOOK("Overbook"),
    @XmlEnumValue("Waitlisted_PriorityA")
    WAITLISTED_PRIORITY_A("Waitlisted_PriorityA"),
    @XmlEnumValue("Waitlisted_PriorityB")
    WAITLISTED_PRIORITY_B("Waitlisted_PriorityB"),
    @XmlEnumValue("Waitlisted_PriorityC")
    WAITLISTED_PRIORITY_C("Waitlisted_PriorityC"),
    @XmlEnumValue("Waitlisted_PriorityD")
    WAITLISTED_PRIORITY_D("Waitlisted_PriorityD"),
    @XmlEnumValue("Jumpseat")
    JUMPSEAT("Jumpseat"),
    @XmlEnumValue("NeedGroupSpace")
    NEED_GROUP_SPACE("NeedGroupSpace"),
    @XmlEnumValue("PriorityGroup")
    PRIORITY_GROUP("PriorityGroup"),
    @XmlEnumValue("Unable_GroupWaitlisted")
    UNABLE_GROUP_WAITLISTED("Unable_GroupWaitlisted"),
    @XmlEnumValue("Waitlist_Group")
    WAITLIST_GROUP("Waitlist_Group"),
    @XmlEnumValue("SegmentWasWaitlist")
    SEGMENT_WAS_WAITLIST("SegmentWasWaitlist"),
    @XmlEnumValue("SegmentWasReconfirmed")
    SEGMENT_WAS_RECONFIRMED("SegmentWasReconfirmed"),
    @XmlEnumValue("SegmentWasPending")
    SEGMENT_WAS_PENDING("SegmentWasPending"),
    @XmlEnumValue("BookedOutsideSystem")
    BOOKED_OUTSIDE_SYSTEM("BookedOutsideSystem"),
    @XmlEnumValue("PendingCancel")
    PENDING_CANCEL("PendingCancel"),
    @XmlEnumValue("MandatoryNeed")
    MANDATORY_NEED("MandatoryNeed"),
    @XmlEnumValue("NeedProtection")
    NEED_PROTECTION("NeedProtection"),
    @XmlEnumValue("PendingRequest")
    PENDING_REQUEST("PendingRequest"),
    @XmlEnumValue("PendingCancellation")
    PENDING_CANCELLATION("PendingCancellation"),
    @XmlEnumValue("StaffListingInSSR")
    STAFF_LISTING_IN_SSR("StaffListingInSSR"),
    @XmlEnumValue("LinkSold_HistoryOnly")
    LINK_SOLD_HISTORY_ONLY("LinkSold_HistoryOnly"),
    @XmlEnumValue("CodeshareSold_HistoryOnly")
    CODESHARE_SOLD_HISTORY_ONLY("CodeshareSold_HistoryOnly"),
    @XmlEnumValue("SuperGuaranteedSell_HistoryOnly")
    SUPER_GUARANTEED_SELL_HISTORY_ONLY("SuperGuaranteedSell_HistoryOnly"),
    @XmlEnumValue("Confirming_NewSchedule")
    CONFIRMING_NEW_SCHEDULE("Confirming_NewSchedule"),
    @XmlEnumValue("OnRequest_NewSchedule")
    ON_REQUEST_NEW_SCHEDULE("OnRequest_NewSchedule"),
    @XmlEnumValue("PlacingOnWaitlist_NewSchedule")
    PLACING_ON_WAITLIST_NEW_SCHEDULE("PlacingOnWaitlist_NewSchedule"),
    @XmlEnumValue("StandbyBoarded")
    STANDBY_BOARDED("StandbyBoarded"),
    @XmlEnumValue("Flown")
    FLOWN("Flown"),
    @XmlEnumValue("NoRecordAvailable_NOREC")
    NO_RECORD_AVAILABLE_NOREC("NoRecordAvailable_NOREC"),
    @XmlEnumValue("GoShow")
    GO_SHOW("GoShow"),
    @XmlEnumValue("NoShow")
    NO_SHOW("NoShow"),
    @XmlEnumValue("OffloadFirmBooked")
    OFFLOAD_FIRM_BOOKED("OffloadFirmBooked"),
    @XmlEnumValue("OffloadOfGoShow")
    OFFLOAD_OF_GO_SHOW("OffloadOfGoShow"),
    @XmlEnumValue("Downgrade_ToEconomyClass")
    DOWNGRADE_TO_ECONOMY_CLASS("Downgrade_ToEconomyClass"),
    @XmlEnumValue("DownUpgrade_ToBusinessClass")
    DOWN_UPGRADE_TO_BUSINESS_CLASS("DownUpgrade_ToBusinessClass"),
    @XmlEnumValue("Upgrade_ToFirstClass")
    UPGRADE_TO_FIRST_CLASS("Upgrade_ToFirstClass"),
    @XmlEnumValue("OffloadOfNoRecordPassenger")
    OFFLOAD_OF_NO_RECORD_PASSENGER("OffloadOfNoRecordPassenger"),
    @XmlEnumValue("HoldsConfirmed_ConditionalEMD_Required")
    HOLDS_CONFIRMED_CONDITIONAL_EMD_REQUIRED("HoldsConfirmed_ConditionalEMD_Required"),
    @XmlEnumValue("HoldsConfirmed_EMD_Required")
    HOLDS_CONFIRMED_EMD_REQUIRED("HoldsConfirmed_EMD_Required"),
    @XmlEnumValue("HoldsConfirmed_EMD_Issued")
    HOLDS_CONFIRMED_EMD_ISSUED("HoldsConfirmed_EMD_Issued"),
    @XmlEnumValue("HoldsConfirmed_NewSchedule_EMDAlreadyIssued")
    HOLDS_CONFIRMED_NEW_SCHEDULE_EMD_ALREADY_ISSUED("HoldsConfirmed_NewSchedule_EMDAlreadyIssued"),

    /**
     * It is strongly recommended that you submit a comment to have any of your extended list values permanently added to the OpenTravel specification to support maximum trading partner interoperability. http://www.opentraveldevelopersnetwork.com/specificationcomments/2/entercomment.html
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    EnumStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumStatus fromValue(String v) {
        for (EnumStatus c: EnumStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
