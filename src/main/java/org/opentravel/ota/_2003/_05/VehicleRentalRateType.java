
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * This describes time, mileage and other charges.
 * 
 * &lt;p&gt;Clase Java para VehicleRentalRateType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleRentalRateType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RateDistance" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleRateDistanceGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="VehicleCharges" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="VehicleCharge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargePurposeType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RateQualifier" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateQualifierType"&amp;gt;
 *                 &amp;lt;attribute name="TourInfoRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="CustLoyaltyRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *                 &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RateRestrictions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="ArriveByFlight" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="MinimumDayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumDayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RestrictedMileageInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumVehiclesAllowed" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                 &amp;lt;attribute name="OvernightInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="OneWayPolicy"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="OneWayAllowed"/&amp;gt;
 *                       &amp;lt;enumeration value="OneWayNotAllowed"/&amp;gt;
 *                       &amp;lt;enumeration value="RestrictedOneWay"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="MinimumAge" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumAge" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                 &amp;lt;attribute name="NoShowFeeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RateGuarantee" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PickupReturnRule" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                 &amp;lt;attribute name="Time" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="RuleType"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="EarliestPickup"/&amp;gt;
 *                       &amp;lt;enumeration value="LatestPickup"/&amp;gt;
 *                       &amp;lt;enumeration value="LatestReturn"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="NoShowFeeInfo" type="{http://www.opentravel.org/OTA/2003/05}NoShowFeeType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleRentalRateType", propOrder = {
    "rateDistance",
    "vehicleCharges",
    "rateQualifier",
    "rateRestrictions",
    "rateGuarantee",
    "pickupReturnRule",
    "noShowFeeInfo"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAVehRateNotifRQ.Rates.Rate.class,
    org.opentravel.ota._2003._05.OTAVehRateRuleRS.RentalRate.class
})
public class VehicleRentalRateType {

    @XmlElement(name = "RateDistance")
    protected List<VehicleRentalRateType.RateDistance> rateDistance;
    @XmlElement(name = "VehicleCharges")
    protected VehicleRentalRateType.VehicleCharges vehicleCharges;
    @XmlElement(name = "RateQualifier")
    protected VehicleRentalRateType.RateQualifier rateQualifier;
    @XmlElement(name = "RateRestrictions")
    protected VehicleRentalRateType.RateRestrictions rateRestrictions;
    @XmlElement(name = "RateGuarantee")
    protected VehicleRentalRateType.RateGuarantee rateGuarantee;
    @XmlElement(name = "PickupReturnRule")
    protected List<VehicleRentalRateType.PickupReturnRule> pickupReturnRule;
    @XmlElement(name = "NoShowFeeInfo")
    protected NoShowFeeType noShowFeeInfo;
    @XmlAttribute(name = "QuoteID")
    protected String quoteID;

    /**
     * Gets the value of the rateDistance property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rateDistance property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRateDistance().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleRentalRateType.RateDistance }
     * 
     * 
     */
    public List<VehicleRentalRateType.RateDistance> getRateDistance() {
        if (rateDistance == null) {
            rateDistance = new ArrayList<VehicleRentalRateType.RateDistance>();
        }
        return this.rateDistance;
    }

    /**
     * Obtiene el valor de la propiedad vehicleCharges.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRentalRateType.VehicleCharges }
     *     
     */
    public VehicleRentalRateType.VehicleCharges getVehicleCharges() {
        return vehicleCharges;
    }

    /**
     * Define el valor de la propiedad vehicleCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRentalRateType.VehicleCharges }
     *     
     */
    public void setVehicleCharges(VehicleRentalRateType.VehicleCharges value) {
        this.vehicleCharges = value;
    }

    /**
     * Obtiene el valor de la propiedad rateQualifier.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRentalRateType.RateQualifier }
     *     
     */
    public VehicleRentalRateType.RateQualifier getRateQualifier() {
        return rateQualifier;
    }

    /**
     * Define el valor de la propiedad rateQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRentalRateType.RateQualifier }
     *     
     */
    public void setRateQualifier(VehicleRentalRateType.RateQualifier value) {
        this.rateQualifier = value;
    }

    /**
     * Obtiene el valor de la propiedad rateRestrictions.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRentalRateType.RateRestrictions }
     *     
     */
    public VehicleRentalRateType.RateRestrictions getRateRestrictions() {
        return rateRestrictions;
    }

    /**
     * Define el valor de la propiedad rateRestrictions.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRentalRateType.RateRestrictions }
     *     
     */
    public void setRateRestrictions(VehicleRentalRateType.RateRestrictions value) {
        this.rateRestrictions = value;
    }

    /**
     * Obtiene el valor de la propiedad rateGuarantee.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRentalRateType.RateGuarantee }
     *     
     */
    public VehicleRentalRateType.RateGuarantee getRateGuarantee() {
        return rateGuarantee;
    }

    /**
     * Define el valor de la propiedad rateGuarantee.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRentalRateType.RateGuarantee }
     *     
     */
    public void setRateGuarantee(VehicleRentalRateType.RateGuarantee value) {
        this.rateGuarantee = value;
    }

    /**
     * Gets the value of the pickupReturnRule property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pickupReturnRule property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPickupReturnRule().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleRentalRateType.PickupReturnRule }
     * 
     * 
     */
    public List<VehicleRentalRateType.PickupReturnRule> getPickupReturnRule() {
        if (pickupReturnRule == null) {
            pickupReturnRule = new ArrayList<VehicleRentalRateType.PickupReturnRule>();
        }
        return this.pickupReturnRule;
    }

    /**
     * Obtiene el valor de la propiedad noShowFeeInfo.
     * 
     * @return
     *     possible object is
     *     {@link NoShowFeeType }
     *     
     */
    public NoShowFeeType getNoShowFeeInfo() {
        return noShowFeeInfo;
    }

    /**
     * Define el valor de la propiedad noShowFeeInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link NoShowFeeType }
     *     
     */
    public void setNoShowFeeInfo(NoShowFeeType value) {
        this.noShowFeeInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad quoteID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteID() {
        return quoteID;
    }

    /**
     * Define el valor de la propiedad quoteID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteID(String value) {
        this.quoteID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *       &amp;lt;attribute name="Time" type="{http://www.opentravel.org/OTA/2003/05}TimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="RuleType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="EarliestPickup"/&amp;gt;
     *             &amp;lt;enumeration value="LatestPickup"/&amp;gt;
     *             &amp;lt;enumeration value="LatestReturn"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PickupReturnRule {

        @XmlAttribute(name = "DayOfWeek")
        protected DayOfWeekType dayOfWeek;
        @XmlAttribute(name = "Time")
        protected String time;
        @XmlAttribute(name = "RuleType")
        protected String ruleType;

        /**
         * Obtiene el valor de la propiedad dayOfWeek.
         * 
         * @return
         *     possible object is
         *     {@link DayOfWeekType }
         *     
         */
        public DayOfWeekType getDayOfWeek() {
            return dayOfWeek;
        }

        /**
         * Define el valor de la propiedad dayOfWeek.
         * 
         * @param value
         *     allowed object is
         *     {@link DayOfWeekType }
         *     
         */
        public void setDayOfWeek(DayOfWeekType value) {
            this.dayOfWeek = value;
        }

        /**
         * Obtiene el valor de la propiedad time.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTime() {
            return time;
        }

        /**
         * Define el valor de la propiedad time.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTime(String value) {
            this.time = value;
        }

        /**
         * Obtiene el valor de la propiedad ruleType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRuleType() {
            return ruleType;
        }

        /**
         * Define el valor de la propiedad ruleType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRuleType(String value) {
            this.ruleType = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleRateDistanceGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RateDistance {

        @XmlAttribute(name = "Unlimited", required = true)
        protected boolean unlimited;
        @XmlAttribute(name = "Quantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger quantity;
        @XmlAttribute(name = "DistUnitName")
        protected DistanceUnitNameType distUnitName;
        @XmlAttribute(name = "VehiclePeriodUnitName")
        protected VehiclePeriodUnitNameType vehiclePeriodUnitName;

        /**
         * Obtiene el valor de la propiedad unlimited.
         * 
         */
        public boolean isUnlimited() {
            return unlimited;
        }

        /**
         * Define el valor de la propiedad unlimited.
         * 
         */
        public void setUnlimited(boolean value) {
            this.unlimited = value;
        }

        /**
         * Obtiene el valor de la propiedad quantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getQuantity() {
            return quantity;
        }

        /**
         * Define el valor de la propiedad quantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setQuantity(BigInteger value) {
            this.quantity = value;
        }

        /**
         * Obtiene el valor de la propiedad distUnitName.
         * 
         * @return
         *     possible object is
         *     {@link DistanceUnitNameType }
         *     
         */
        public DistanceUnitNameType getDistUnitName() {
            return distUnitName;
        }

        /**
         * Define el valor de la propiedad distUnitName.
         * 
         * @param value
         *     allowed object is
         *     {@link DistanceUnitNameType }
         *     
         */
        public void setDistUnitName(DistanceUnitNameType value) {
            this.distUnitName = value;
        }

        /**
         * Obtiene el valor de la propiedad vehiclePeriodUnitName.
         * 
         * @return
         *     possible object is
         *     {@link VehiclePeriodUnitNameType }
         *     
         */
        public VehiclePeriodUnitNameType getVehiclePeriodUnitName() {
            return vehiclePeriodUnitName;
        }

        /**
         * Define el valor de la propiedad vehiclePeriodUnitName.
         * 
         * @param value
         *     allowed object is
         *     {@link VehiclePeriodUnitNameType }
         *     
         */
        public void setVehiclePeriodUnitName(VehiclePeriodUnitNameType value) {
            this.vehiclePeriodUnitName = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "description"
    })
    public static class RateGuarantee {

        @XmlElement(name = "Description")
        protected FormattedTextTextType description;
        @XmlAttribute(name = "AbsoluteDeadline")
        protected String absoluteDeadline;
        @XmlAttribute(name = "OffsetTimeUnit")
        protected TimeUnitType offsetTimeUnit;
        @XmlAttribute(name = "OffsetUnitMultiplier")
        protected Integer offsetUnitMultiplier;
        @XmlAttribute(name = "OffsetDropTime")
        protected String offsetDropTime;

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link FormattedTextTextType }
         *     
         */
        public FormattedTextTextType getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link FormattedTextTextType }
         *     
         */
        public void setDescription(FormattedTextTextType value) {
            this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad absoluteDeadline.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAbsoluteDeadline() {
            return absoluteDeadline;
        }

        /**
         * Define el valor de la propiedad absoluteDeadline.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAbsoluteDeadline(String value) {
            this.absoluteDeadline = value;
        }

        /**
         * Obtiene el valor de la propiedad offsetTimeUnit.
         * 
         * @return
         *     possible object is
         *     {@link TimeUnitType }
         *     
         */
        public TimeUnitType getOffsetTimeUnit() {
            return offsetTimeUnit;
        }

        /**
         * Define el valor de la propiedad offsetTimeUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link TimeUnitType }
         *     
         */
        public void setOffsetTimeUnit(TimeUnitType value) {
            this.offsetTimeUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad offsetUnitMultiplier.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOffsetUnitMultiplier() {
            return offsetUnitMultiplier;
        }

        /**
         * Define el valor de la propiedad offsetUnitMultiplier.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOffsetUnitMultiplier(Integer value) {
            this.offsetUnitMultiplier = value;
        }

        /**
         * Obtiene el valor de la propiedad offsetDropTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOffsetDropTime() {
            return offsetDropTime;
        }

        /**
         * Define el valor de la propiedad offsetDropTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOffsetDropTime(String value) {
            this.offsetDropTime = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateQualifierType"&amp;gt;
     *       &amp;lt;attribute name="TourInfoRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="CustLoyaltyRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *       &amp;lt;attribute name="QuoteID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RateQualifier
        extends RateQualifierType
    {

        @XmlAttribute(name = "TourInfoRPH")
        protected String tourInfoRPH;
        @XmlAttribute(name = "CustLoyaltyRPH")
        protected List<String> custLoyaltyRPH;
        @XmlAttribute(name = "QuoteID")
        protected String quoteID;

        /**
         * Obtiene el valor de la propiedad tourInfoRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTourInfoRPH() {
            return tourInfoRPH;
        }

        /**
         * Define el valor de la propiedad tourInfoRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTourInfoRPH(String value) {
            this.tourInfoRPH = value;
        }

        /**
         * Gets the value of the custLoyaltyRPH property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the custLoyaltyRPH property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCustLoyaltyRPH().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getCustLoyaltyRPH() {
            if (custLoyaltyRPH == null) {
                custLoyaltyRPH = new ArrayList<String>();
            }
            return this.custLoyaltyRPH;
        }

        /**
         * Obtiene el valor de la propiedad quoteID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuoteID() {
            return quoteID;
        }

        /**
         * Define el valor de la propiedad quoteID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuoteID(String value) {
            this.quoteID = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="ArriveByFlight" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="MinimumDayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="MaximumDayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RestrictedMileageInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="MaximumVehiclesAllowed" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *       &amp;lt;attribute name="OvernightInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="OneWayPolicy"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="OneWayAllowed"/&amp;gt;
     *             &amp;lt;enumeration value="OneWayNotAllowed"/&amp;gt;
     *             &amp;lt;enumeration value="RestrictedOneWay"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="MinimumAge" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *       &amp;lt;attribute name="MaximumAge" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *       &amp;lt;attribute name="NoShowFeeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RateRestrictions {

        @XmlAttribute(name = "ArriveByFlight")
        protected Boolean arriveByFlight;
        @XmlAttribute(name = "MinimumDayInd")
        protected Boolean minimumDayInd;
        @XmlAttribute(name = "MaximumDayInd")
        protected Boolean maximumDayInd;
        @XmlAttribute(name = "AdvancedBookingInd")
        protected Boolean advancedBookingInd;
        @XmlAttribute(name = "RestrictedMileageInd")
        protected Boolean restrictedMileageInd;
        @XmlAttribute(name = "CorporateRateInd")
        protected Boolean corporateRateInd;
        @XmlAttribute(name = "GuaranteeReqInd")
        protected Boolean guaranteeReqInd;
        @XmlAttribute(name = "MaximumVehiclesAllowed")
        protected Integer maximumVehiclesAllowed;
        @XmlAttribute(name = "OvernightInd")
        protected Boolean overnightInd;
        @XmlAttribute(name = "OneWayPolicy")
        protected String oneWayPolicy;
        @XmlAttribute(name = "CancellationPenaltyInd")
        protected Boolean cancellationPenaltyInd;
        @XmlAttribute(name = "ModificationPenaltyInd")
        protected Boolean modificationPenaltyInd;
        @XmlAttribute(name = "MinimumAge")
        protected Integer minimumAge;
        @XmlAttribute(name = "MaximumAge")
        protected Integer maximumAge;
        @XmlAttribute(name = "NoShowFeeInd")
        protected Boolean noShowFeeInd;

        /**
         * Obtiene el valor de la propiedad arriveByFlight.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isArriveByFlight() {
            return arriveByFlight;
        }

        /**
         * Define el valor de la propiedad arriveByFlight.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setArriveByFlight(Boolean value) {
            this.arriveByFlight = value;
        }

        /**
         * Obtiene el valor de la propiedad minimumDayInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMinimumDayInd() {
            return minimumDayInd;
        }

        /**
         * Define el valor de la propiedad minimumDayInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMinimumDayInd(Boolean value) {
            this.minimumDayInd = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumDayInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMaximumDayInd() {
            return maximumDayInd;
        }

        /**
         * Define el valor de la propiedad maximumDayInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMaximumDayInd(Boolean value) {
            this.maximumDayInd = value;
        }

        /**
         * Obtiene el valor de la propiedad advancedBookingInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAdvancedBookingInd() {
            return advancedBookingInd;
        }

        /**
         * Define el valor de la propiedad advancedBookingInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAdvancedBookingInd(Boolean value) {
            this.advancedBookingInd = value;
        }

        /**
         * Obtiene el valor de la propiedad restrictedMileageInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRestrictedMileageInd() {
            return restrictedMileageInd;
        }

        /**
         * Define el valor de la propiedad restrictedMileageInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRestrictedMileageInd(Boolean value) {
            this.restrictedMileageInd = value;
        }

        /**
         * Obtiene el valor de la propiedad corporateRateInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCorporateRateInd() {
            return corporateRateInd;
        }

        /**
         * Define el valor de la propiedad corporateRateInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCorporateRateInd(Boolean value) {
            this.corporateRateInd = value;
        }

        /**
         * Obtiene el valor de la propiedad guaranteeReqInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteeReqInd() {
            return guaranteeReqInd;
        }

        /**
         * Define el valor de la propiedad guaranteeReqInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteeReqInd(Boolean value) {
            this.guaranteeReqInd = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumVehiclesAllowed.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaximumVehiclesAllowed() {
            return maximumVehiclesAllowed;
        }

        /**
         * Define el valor de la propiedad maximumVehiclesAllowed.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaximumVehiclesAllowed(Integer value) {
            this.maximumVehiclesAllowed = value;
        }

        /**
         * Obtiene el valor de la propiedad overnightInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOvernightInd() {
            return overnightInd;
        }

        /**
         * Define el valor de la propiedad overnightInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOvernightInd(Boolean value) {
            this.overnightInd = value;
        }

        /**
         * Obtiene el valor de la propiedad oneWayPolicy.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOneWayPolicy() {
            return oneWayPolicy;
        }

        /**
         * Define el valor de la propiedad oneWayPolicy.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOneWayPolicy(String value) {
            this.oneWayPolicy = value;
        }

        /**
         * Obtiene el valor de la propiedad cancellationPenaltyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCancellationPenaltyInd() {
            return cancellationPenaltyInd;
        }

        /**
         * Define el valor de la propiedad cancellationPenaltyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCancellationPenaltyInd(Boolean value) {
            this.cancellationPenaltyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad modificationPenaltyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isModificationPenaltyInd() {
            return modificationPenaltyInd;
        }

        /**
         * Define el valor de la propiedad modificationPenaltyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setModificationPenaltyInd(Boolean value) {
            this.modificationPenaltyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad minimumAge.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMinimumAge() {
            return minimumAge;
        }

        /**
         * Define el valor de la propiedad minimumAge.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMinimumAge(Integer value) {
            this.minimumAge = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumAge.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaximumAge() {
            return maximumAge;
        }

        /**
         * Define el valor de la propiedad maximumAge.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaximumAge(Integer value) {
            this.maximumAge = value;
        }

        /**
         * Obtiene el valor de la propiedad noShowFeeInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNoShowFeeInd() {
            return noShowFeeInd;
        }

        /**
         * Define el valor de la propiedad noShowFeeInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNoShowFeeInd(Boolean value) {
            this.noShowFeeInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VehicleCharge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargePurposeType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleCharge"
    })
    public static class VehicleCharges {

        @XmlElement(name = "VehicleCharge", required = true)
        protected List<VehicleChargePurposeType> vehicleCharge;

        /**
         * Gets the value of the vehicleCharge property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicleCharge property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVehicleCharge().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleChargePurposeType }
         * 
         * 
         */
        public List<VehicleChargePurposeType> getVehicleCharge() {
            if (vehicleCharge == null) {
                vehicleCharge = new ArrayList<VehicleChargePurposeType>();
            }
            return this.vehicleCharge;
        }

    }

}
