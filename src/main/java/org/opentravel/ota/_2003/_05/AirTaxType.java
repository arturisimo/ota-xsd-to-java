
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Defines the data fields available for air tax. The element text of this type may contain a description of the tax.
 * 
 * &lt;p&gt;Clase Java para AirTaxType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirTaxType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;ShortDescriptionType"&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *       &amp;lt;attribute name="TaxCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="TaxCountry" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *       &amp;lt;attribute name="TaxName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *       &amp;lt;attribute name="TaxExemptInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="Operation" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *       &amp;lt;attribute name="TaxTransactionType"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="charge"/&amp;gt;
 *             &amp;lt;enumeration value="exempt"/&amp;gt;
 *             &amp;lt;enumeration value="refund"/&amp;gt;
 *             &amp;lt;enumeration value="reserve"/&amp;gt;
 *             &amp;lt;enumeration value="paid"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirTaxType", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.EMDLiteType.Taxes.Tax.class,
    org.opentravel.ota._2003._05.EMDType.Taxes.Tax.class
})
public class AirTaxType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "TaxCode")
    protected String taxCode;
    @XmlAttribute(name = "TaxCountry")
    protected String taxCountry;
    @XmlAttribute(name = "TaxName")
    protected String taxName;
    @XmlAttribute(name = "TaxExemptInd")
    protected Boolean taxExemptInd;
    @XmlAttribute(name = "Operation")
    protected ActionType operation;
    @XmlAttribute(name = "TaxTransactionType")
    protected String taxTransactionType;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "RefundableInd")
    protected Boolean refundableInd;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "DecimalPlaces")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger decimalPlaces;
    @XmlAttribute(name = "Amount")
    protected BigDecimal amount;

    /**
     * A textual description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad taxCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCode() {
        return taxCode;
    }

    /**
     * Define el valor de la propiedad taxCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCode(String value) {
        this.taxCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxCountry() {
        return taxCountry;
    }

    /**
     * Define el valor de la propiedad taxCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxCountry(String value) {
        this.taxCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad taxName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxName() {
        return taxName;
    }

    /**
     * Define el valor de la propiedad taxName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxName(String value) {
        this.taxName = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxExemptInd() {
        return taxExemptInd;
    }

    /**
     * Define el valor de la propiedad taxExemptInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxExemptInd(Boolean value) {
        this.taxExemptInd = value;
    }

    /**
     * Obtiene el valor de la propiedad operation.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getOperation() {
        return operation;
    }

    /**
     * Define el valor de la propiedad operation.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setOperation(ActionType value) {
        this.operation = value;
    }

    /**
     * Obtiene el valor de la propiedad taxTransactionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxTransactionType() {
        return taxTransactionType;
    }

    /**
     * Define el valor de la propiedad taxTransactionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxTransactionType(String value) {
        this.taxTransactionType = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad refundableInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundableInd() {
        return refundableInd;
    }

    /**
     * Define el valor de la propiedad refundableInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundableInd(Boolean value) {
        this.refundableInd = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad decimalPlaces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * Define el valor de la propiedad decimalPlaces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDecimalPlaces(BigInteger value) {
        this.decimalPlaces = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

}
