
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Defines information about a train segment.
 * 
 * &lt;p&gt;Clase Java para TrainSegmentType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TrainSegmentType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="DepartureStation" type="{http://www.opentravel.org/OTA/2003/05}StationDetailsType"/&amp;gt;
 *         &amp;lt;element name="ArrivalStation" type="{http://www.opentravel.org/OTA/2003/05}StationDetailsType"/&amp;gt;
 *         &amp;lt;element name="MarketingCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
 *         &amp;lt;element name="OperatingCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Equipment" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TrainInfo" type="{http://www.opentravel.org/OTA/2003/05}TrainInfoType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DepartureDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *       &amp;lt;attribute name="ArrivalDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *       &amp;lt;attribute name="StopQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *       &amp;lt;attribute name="CrossBorderInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrainSegmentType", propOrder = {
    "departureStation",
    "arrivalStation",
    "marketingCompany",
    "operatingCompany",
    "equipment",
    "trainInfo"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.class,
    org.opentravel.ota._2003._05.OTARailScheduleRS.OriginDestinationOptions.OriginDestinationOption.TrainSegment.class,
    org.opentravel.ota._2003._05.OTARailShopRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.class,
    BookedTrainSegmentType.class
})
public class TrainSegmentType {

    @XmlElement(name = "DepartureStation", required = true)
    protected StationDetailsType departureStation;
    @XmlElement(name = "ArrivalStation", required = true)
    protected StationDetailsType arrivalStation;
    @XmlElement(name = "MarketingCompany", required = true)
    protected CompanyNameType marketingCompany;
    @XmlElement(name = "OperatingCompany")
    protected CompanyNameType operatingCompany;
    @XmlElement(name = "Equipment")
    protected ClassCodeType equipment;
    @XmlElement(name = "TrainInfo")
    protected TrainInfoType trainInfo;
    @XmlAttribute(name = "DepartureDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar departureDateTime;
    @XmlAttribute(name = "ArrivalDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arrivalDateTime;
    @XmlAttribute(name = "StopQuantity")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger stopQuantity;
    @XmlAttribute(name = "JourneyDuration")
    protected Duration journeyDuration;
    @XmlAttribute(name = "CrossBorderInd")
    protected Boolean crossBorderInd;

    /**
     * Obtiene el valor de la propiedad departureStation.
     * 
     * @return
     *     possible object is
     *     {@link StationDetailsType }
     *     
     */
    public StationDetailsType getDepartureStation() {
        return departureStation;
    }

    /**
     * Define el valor de la propiedad departureStation.
     * 
     * @param value
     *     allowed object is
     *     {@link StationDetailsType }
     *     
     */
    public void setDepartureStation(StationDetailsType value) {
        this.departureStation = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivalStation.
     * 
     * @return
     *     possible object is
     *     {@link StationDetailsType }
     *     
     */
    public StationDetailsType getArrivalStation() {
        return arrivalStation;
    }

    /**
     * Define el valor de la propiedad arrivalStation.
     * 
     * @param value
     *     allowed object is
     *     {@link StationDetailsType }
     *     
     */
    public void setArrivalStation(StationDetailsType value) {
        this.arrivalStation = value;
    }

    /**
     * Obtiene el valor de la propiedad marketingCompany.
     * 
     * @return
     *     possible object is
     *     {@link CompanyNameType }
     *     
     */
    public CompanyNameType getMarketingCompany() {
        return marketingCompany;
    }

    /**
     * Define el valor de la propiedad marketingCompany.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyNameType }
     *     
     */
    public void setMarketingCompany(CompanyNameType value) {
        this.marketingCompany = value;
    }

    /**
     * Obtiene el valor de la propiedad operatingCompany.
     * 
     * @return
     *     possible object is
     *     {@link CompanyNameType }
     *     
     */
    public CompanyNameType getOperatingCompany() {
        return operatingCompany;
    }

    /**
     * Define el valor de la propiedad operatingCompany.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyNameType }
     *     
     */
    public void setOperatingCompany(CompanyNameType value) {
        this.operatingCompany = value;
    }

    /**
     * Obtiene el valor de la propiedad equipment.
     * 
     * @return
     *     possible object is
     *     {@link ClassCodeType }
     *     
     */
    public ClassCodeType getEquipment() {
        return equipment;
    }

    /**
     * Define el valor de la propiedad equipment.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassCodeType }
     *     
     */
    public void setEquipment(ClassCodeType value) {
        this.equipment = value;
    }

    /**
     * Obtiene el valor de la propiedad trainInfo.
     * 
     * @return
     *     possible object is
     *     {@link TrainInfoType }
     *     
     */
    public TrainInfoType getTrainInfo() {
        return trainInfo;
    }

    /**
     * Define el valor de la propiedad trainInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TrainInfoType }
     *     
     */
    public void setTrainInfo(TrainInfoType value) {
        this.trainInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad departureDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureDateTime() {
        return departureDateTime;
    }

    /**
     * Define el valor de la propiedad departureDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureDateTime(XMLGregorianCalendar value) {
        this.departureDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivalDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArrivalDateTime() {
        return arrivalDateTime;
    }

    /**
     * Define el valor de la propiedad arrivalDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArrivalDateTime(XMLGregorianCalendar value) {
        this.arrivalDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad stopQuantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStopQuantity() {
        return stopQuantity;
    }

    /**
     * Define el valor de la propiedad stopQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStopQuantity(BigInteger value) {
        this.stopQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyDuration.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getJourneyDuration() {
        return journeyDuration;
    }

    /**
     * Define el valor de la propiedad journeyDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setJourneyDuration(Duration value) {
        this.journeyDuration = value;
    }

    /**
     * Obtiene el valor de la propiedad crossBorderInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCrossBorderInd() {
        return crossBorderInd;
    }

    /**
     * Define el valor de la propiedad crossBorderInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCrossBorderInd(Boolean value) {
        this.crossBorderInd = value;
    }

}
