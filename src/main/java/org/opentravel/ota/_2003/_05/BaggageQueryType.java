
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Information about baggage associated with a traveler for allowing the offer engine to determine baggage services.
 * 
 * &lt;p&gt;Clase Java para BaggageQueryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="BaggageQueryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AirlineCarrier" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Code"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="OriginCityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="OriginCodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="DestinationCityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="DestinationCodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="ItinerarySegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaggageQueryType", propOrder = {
    "airlineCarrier"
})
public class BaggageQueryType {

    @XmlElement(name = "AirlineCarrier")
    protected BaggageQueryType.AirlineCarrier airlineCarrier;
    @XmlAttribute(name = "Code")
    protected String code;
    @XmlAttribute(name = "OriginCityCode")
    protected String originCityCode;
    @XmlAttribute(name = "OriginCodeContext")
    protected String originCodeContext;
    @XmlAttribute(name = "DestinationCityCode")
    protected String destinationCityCode;
    @XmlAttribute(name = "DestinationCodeContext")
    protected String destinationCodeContext;
    @XmlAttribute(name = "TravelerRPH")
    protected String travelerRPH;
    @XmlAttribute(name = "ItinerarySegmentRPH")
    protected String itinerarySegmentRPH;

    /**
     * Obtiene el valor de la propiedad airlineCarrier.
     * 
     * @return
     *     possible object is
     *     {@link BaggageQueryType.AirlineCarrier }
     *     
     */
    public BaggageQueryType.AirlineCarrier getAirlineCarrier() {
        return airlineCarrier;
    }

    /**
     * Define el valor de la propiedad airlineCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link BaggageQueryType.AirlineCarrier }
     *     
     */
    public void setAirlineCarrier(BaggageQueryType.AirlineCarrier value) {
        this.airlineCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad originCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCityCode() {
        return originCityCode;
    }

    /**
     * Define el valor de la propiedad originCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCityCode(String value) {
        this.originCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad originCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCodeContext() {
        return originCodeContext;
    }

    /**
     * Define el valor de la propiedad originCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCodeContext(String value) {
        this.originCodeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCityCode() {
        return destinationCityCode;
    }

    /**
     * Define el valor de la propiedad destinationCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCityCode(String value) {
        this.destinationCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCodeContext() {
        return destinationCodeContext;
    }

    /**
     * Define el valor de la propiedad destinationCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCodeContext(String value) {
        this.destinationCodeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelerRPH() {
        return travelerRPH;
    }

    /**
     * Define el valor de la propiedad travelerRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelerRPH(String value) {
        this.travelerRPH = value;
    }

    /**
     * Obtiene el valor de la propiedad itinerarySegmentRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItinerarySegmentRPH() {
        return itinerarySegmentRPH;
    }

    /**
     * Define el valor de la propiedad itinerarySegmentRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItinerarySegmentRPH(String value) {
        this.itinerarySegmentRPH = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AirlineCarrier
        extends CompanyNameType
    {


    }

}
