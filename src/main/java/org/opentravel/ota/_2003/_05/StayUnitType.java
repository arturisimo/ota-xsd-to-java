
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para StayUnitType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="StayUnitType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Minutes"/&amp;gt;
 *     &amp;lt;enumeration value="Hours"/&amp;gt;
 *     &amp;lt;enumeration value="Days"/&amp;gt;
 *     &amp;lt;enumeration value="Months"/&amp;gt;
 *     &amp;lt;enumeration value="MON"/&amp;gt;
 *     &amp;lt;enumeration value="TUES"/&amp;gt;
 *     &amp;lt;enumeration value="WED"/&amp;gt;
 *     &amp;lt;enumeration value="THU"/&amp;gt;
 *     &amp;lt;enumeration value="FRI"/&amp;gt;
 *     &amp;lt;enumeration value="SAT"/&amp;gt;
 *     &amp;lt;enumeration value="SUN"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "StayUnitType")
@XmlEnum
public enum StayUnitType {


    /**
     * Stay restriction in minutes.
     * 
     */
    @XmlEnumValue("Minutes")
    MINUTES("Minutes"),

    /**
     * Stay restriction in hours.
     * 
     */
    @XmlEnumValue("Hours")
    HOURS("Hours"),

    /**
     * Stay restriction in days.
     * 
     */
    @XmlEnumValue("Days")
    DAYS("Days"),

    /**
     * Stay restriction in months.
     * 
     */
    @XmlEnumValue("Months")
    MONTHS("Months"),

    /**
     * Monday
     * 
     */
    MON("MON"),

    /**
     * Tuesday
     * 
     */
    TUES("TUES"),

    /**
     * Wednesday
     * 
     */
    WED("WED"),

    /**
     * Thursday
     * 
     */
    THU("THU"),

    /**
     * Friday
     * 
     */
    FRI("FRI"),

    /**
     * Saturday
     * 
     */
    SAT("SAT"),

    /**
     * Sunday
     * 
     */
    SUN("SUN");
    private final String value;

    StayUnitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StayUnitType fromValue(String v) {
        for (StayUnitType c: StayUnitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
