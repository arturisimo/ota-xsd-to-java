
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Specific traveler requested accommodation information, including class and type, and associated accommodation attributes such as seat,  berth and compartment number, position, direction and adjacent seating preferences.
 * 
 * &lt;p&gt;Clase Java para RailAccommDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RailAccommDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;choice minOccurs="0"&amp;gt;
 *           &amp;lt;element name="Seat" type="{http://www.opentravel.org/OTA/2003/05}SeatDetailType"/&amp;gt;
 *           &amp;lt;element name="Berth" type="{http://www.opentravel.org/OTA/2003/05}BerthDetailType"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *         &amp;lt;element name="Class" type="{http://www.opentravel.org/OTA/2003/05}AccommodationClass" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Compartment" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompartmentType"&amp;gt;
 *                 &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                 &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Car" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Deck" type="{http://www.opentravel.org/OTA/2003/05}DeckType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailAccommDetailType", propOrder = {
    "seat",
    "berth",
    "clazz",
    "compartment",
    "car"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.AccommodationServiceType.AccommodationDetail.class,
    org.opentravel.ota._2003._05.RailPrefType.AccommodationPref.class
})
public class RailAccommDetailType {

    @XmlElement(name = "Seat")
    protected SeatDetailType seat;
    @XmlElement(name = "Berth")
    protected BerthDetailType berth;
    @XmlElement(name = "Class")
    protected AccommodationClass clazz;
    @XmlElement(name = "Compartment")
    protected RailAccommDetailType.Compartment compartment;
    @XmlElement(name = "Car")
    protected RailAccommDetailType.Car car;
    @XmlAttribute(name = "Deck")
    protected DeckType deck;

    /**
     * Obtiene el valor de la propiedad seat.
     * 
     * @return
     *     possible object is
     *     {@link SeatDetailType }
     *     
     */
    public SeatDetailType getSeat() {
        return seat;
    }

    /**
     * Define el valor de la propiedad seat.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatDetailType }
     *     
     */
    public void setSeat(SeatDetailType value) {
        this.seat = value;
    }

    /**
     * Obtiene el valor de la propiedad berth.
     * 
     * @return
     *     possible object is
     *     {@link BerthDetailType }
     *     
     */
    public BerthDetailType getBerth() {
        return berth;
    }

    /**
     * Define el valor de la propiedad berth.
     * 
     * @param value
     *     allowed object is
     *     {@link BerthDetailType }
     *     
     */
    public void setBerth(BerthDetailType value) {
        this.berth = value;
    }

    /**
     * Obtiene el valor de la propiedad clazz.
     * 
     * @return
     *     possible object is
     *     {@link AccommodationClass }
     *     
     */
    public AccommodationClass getClazz() {
        return clazz;
    }

    /**
     * Define el valor de la propiedad clazz.
     * 
     * @param value
     *     allowed object is
     *     {@link AccommodationClass }
     *     
     */
    public void setClazz(AccommodationClass value) {
        this.clazz = value;
    }

    /**
     * Obtiene el valor de la propiedad compartment.
     * 
     * @return
     *     possible object is
     *     {@link RailAccommDetailType.Compartment }
     *     
     */
    public RailAccommDetailType.Compartment getCompartment() {
        return compartment;
    }

    /**
     * Define el valor de la propiedad compartment.
     * 
     * @param value
     *     allowed object is
     *     {@link RailAccommDetailType.Compartment }
     *     
     */
    public void setCompartment(RailAccommDetailType.Compartment value) {
        this.compartment = value;
    }

    /**
     * Obtiene el valor de la propiedad car.
     * 
     * @return
     *     possible object is
     *     {@link RailAccommDetailType.Car }
     *     
     */
    public RailAccommDetailType.Car getCar() {
        return car;
    }

    /**
     * Define el valor de la propiedad car.
     * 
     * @param value
     *     allowed object is
     *     {@link RailAccommDetailType.Car }
     *     
     */
    public void setCar(RailAccommDetailType.Car value) {
        this.car = value;
    }

    /**
     * Obtiene el valor de la propiedad deck.
     * 
     * @return
     *     possible object is
     *     {@link DeckType }
     *     
     */
    public DeckType getDeck() {
        return deck;
    }

    /**
     * Define el valor de la propiedad deck.
     * 
     * @param value
     *     allowed object is
     *     {@link DeckType }
     *     
     */
    public void setDeck(DeckType value) {
        this.deck = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Car {

        @XmlAttribute(name = "Number")
        protected String number;

        /**
         * Obtiene el valor de la propiedad number.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumber() {
            return number;
        }

        /**
         * Define el valor de la propiedad number.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumber(String value) {
            this.number = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompartmentType"&amp;gt;
     *       &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *       &amp;lt;attribute name="Position" type="{http://www.opentravel.org/OTA/2003/05}CompartmentPositionType" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Compartment
        extends CompartmentType
    {

        @XmlAttribute(name = "Number")
        protected String number;
        @XmlAttribute(name = "Position")
        protected CompartmentPositionType position;

        /**
         * Obtiene el valor de la propiedad number.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumber() {
            return number;
        }

        /**
         * Define el valor de la propiedad number.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumber(String value) {
            this.number = value;
        }

        /**
         * Obtiene el valor de la propiedad position.
         * 
         * @return
         *     possible object is
         *     {@link CompartmentPositionType }
         *     
         */
        public CompartmentPositionType getPosition() {
            return position;
        }

        /**
         * Define el valor de la propiedad position.
         * 
         * @param value
         *     allowed object is
         *     {@link CompartmentPositionType }
         *     
         */
        public void setPosition(CompartmentPositionType value) {
            this.position = value;
        }

    }

}
