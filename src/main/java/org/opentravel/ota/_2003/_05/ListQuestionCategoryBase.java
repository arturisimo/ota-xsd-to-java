
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_QuestionCategory_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_QuestionCategory_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="OverallRating"/&amp;gt;
 *     &amp;lt;enumeration value="All"/&amp;gt;
 *     &amp;lt;enumeration value="Accessibility"/&amp;gt;
 *     &amp;lt;enumeration value="Ambiance"/&amp;gt;
 *     &amp;lt;enumeration value="Beach"/&amp;gt;
 *     &amp;lt;enumeration value="BedComfort"/&amp;gt;
 *     &amp;lt;enumeration value="Cleanliness"/&amp;gt;
 *     &amp;lt;enumeration value="Cons"/&amp;gt;
 *     &amp;lt;enumeration value="FamilyFriendly"/&amp;gt;
 *     &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
 *     &amp;lt;enumeration value="Location"/&amp;gt;
 *     &amp;lt;enumeration value="Pool"/&amp;gt;
 *     &amp;lt;enumeration value="Pros"/&amp;gt;
 *     &amp;lt;enumeration value="RecreationalFacilties"/&amp;gt;
 *     &amp;lt;enumeration value="Restaurants"/&amp;gt;
 *     &amp;lt;enumeration value="RoomQuality"/&amp;gt;
 *     &amp;lt;enumeration value="Services"/&amp;gt;
 *     &amp;lt;enumeration value="Staff"/&amp;gt;
 *     &amp;lt;enumeration value="Transportation"/&amp;gt;
 *     &amp;lt;enumeration value="TravelerQuestion"/&amp;gt;
 *     &amp;lt;enumeration value="TravelerStory"/&amp;gt;
 *     &amp;lt;enumeration value="Value"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_QuestionCategory_Base")
@XmlEnum
public enum ListQuestionCategoryBase {

    @XmlEnumValue("OverallRating")
    OVERALL_RATING("OverallRating"),
    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Accessibility")
    ACCESSIBILITY("Accessibility"),
    @XmlEnumValue("Ambiance")
    AMBIANCE("Ambiance"),
    @XmlEnumValue("Beach")
    BEACH("Beach"),
    @XmlEnumValue("BedComfort")
    BED_COMFORT("BedComfort"),
    @XmlEnumValue("Cleanliness")
    CLEANLINESS("Cleanliness"),
    @XmlEnumValue("Cons")
    CONS("Cons"),
    @XmlEnumValue("FamilyFriendly")
    FAMILY_FRIENDLY("FamilyFriendly"),
    @XmlEnumValue("FoodAndBeverage")
    FOOD_AND_BEVERAGE("FoodAndBeverage"),
    @XmlEnumValue("Location")
    LOCATION("Location"),
    @XmlEnumValue("Pool")
    POOL("Pool"),
    @XmlEnumValue("Pros")
    PROS("Pros"),
    @XmlEnumValue("RecreationalFacilties")
    RECREATIONAL_FACILTIES("RecreationalFacilties"),
    @XmlEnumValue("Restaurants")
    RESTAURANTS("Restaurants"),
    @XmlEnumValue("RoomQuality")
    ROOM_QUALITY("RoomQuality"),
    @XmlEnumValue("Services")
    SERVICES("Services"),
    @XmlEnumValue("Staff")
    STAFF("Staff"),
    @XmlEnumValue("Transportation")
    TRANSPORTATION("Transportation"),
    @XmlEnumValue("TravelerQuestion")
    TRAVELER_QUESTION("TravelerQuestion"),
    @XmlEnumValue("TravelerStory")
    TRAVELER_STORY("TravelerStory"),
    @XmlEnumValue("Value")
    VALUE("Value"),
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListQuestionCategoryBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListQuestionCategoryBase fromValue(String v) {
        for (ListQuestionCategoryBase c: ListQuestionCategoryBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
