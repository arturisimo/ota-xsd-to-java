
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines train Information.
 * 
 * &lt;p&gt;Clase Java para TrainInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TrainInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Train" type="{http://www.opentravel.org/OTA/2003/05}TrainIdentificationType"/&amp;gt;
 *         &amp;lt;element name="ValidDate" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DelayTime" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *       &amp;lt;attribute name="ScheduleCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrainInfoType", propOrder = {
    "train",
    "validDate"
})
public class TrainInfoType {

    @XmlElement(name = "Train", required = true)
    protected TrainIdentificationType train;
    @XmlElement(name = "ValidDate")
    protected TrainInfoType.ValidDate validDate;
    @XmlAttribute(name = "DelayTime")
    @XmlSchemaType(name = "anySimpleType")
    protected String delayTime;
    @XmlAttribute(name = "ScheduleCode")
    protected String scheduleCode;

    /**
     * Obtiene el valor de la propiedad train.
     * 
     * @return
     *     possible object is
     *     {@link TrainIdentificationType }
     *     
     */
    public TrainIdentificationType getTrain() {
        return train;
    }

    /**
     * Define el valor de la propiedad train.
     * 
     * @param value
     *     allowed object is
     *     {@link TrainIdentificationType }
     *     
     */
    public void setTrain(TrainIdentificationType value) {
        this.train = value;
    }

    /**
     * Obtiene el valor de la propiedad validDate.
     * 
     * @return
     *     possible object is
     *     {@link TrainInfoType.ValidDate }
     *     
     */
    public TrainInfoType.ValidDate getValidDate() {
        return validDate;
    }

    /**
     * Define el valor de la propiedad validDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TrainInfoType.ValidDate }
     *     
     */
    public void setValidDate(TrainInfoType.ValidDate value) {
        this.validDate = value;
    }

    /**
     * Obtiene el valor de la propiedad delayTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelayTime() {
        return delayTime;
    }

    /**
     * Define el valor de la propiedad delayTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelayTime(String value) {
        this.delayTime = value;
    }

    /**
     * Obtiene el valor de la propiedad scheduleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleCode() {
        return scheduleCode;
    }

    /**
     * Define el valor de la propiedad scheduleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleCode(String value) {
        this.scheduleCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ValidDate {

        @XmlAttribute(name = "StartPeriod")
        protected String startPeriod;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "EndPeriod")
        protected String endPeriod;

        /**
         * Obtiene el valor de la propiedad startPeriod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartPeriod() {
            return startPeriod;
        }

        /**
         * Define el valor de la propiedad startPeriod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartPeriod(String value) {
            this.startPeriod = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad endPeriod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndPeriod() {
            return endPeriod;
        }

        /**
         * Define el valor de la propiedad endPeriod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndPeriod(String value) {
            this.endPeriod = value;
        }

    }

}
