
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para SeatDirectionType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="SeatDirectionType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Facing"/&amp;gt;
 *     &amp;lt;enumeration value="Back"/&amp;gt;
 *     &amp;lt;enumeration value="Airline"/&amp;gt;
 *     &amp;lt;enumeration value="Lateral"/&amp;gt;
 *     &amp;lt;enumeration value="Unknown"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "SeatDirectionType")
@XmlEnum
public enum SeatDirectionType {

    @XmlEnumValue("Facing")
    FACING("Facing"),
    @XmlEnumValue("Back")
    BACK("Back"),
    @XmlEnumValue("Airline")
    AIRLINE("Airline"),
    @XmlEnumValue("Lateral")
    LATERAL("Lateral"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    SeatDirectionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeatDirectionType fromValue(String v) {
        for (SeatDirectionType c: SeatDirectionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
