
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_LevelOfService_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_LevelOfService_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Deluxe"/&amp;gt;
 *     &amp;lt;enumeration value="Economy"/&amp;gt;
 *     &amp;lt;enumeration value="Exclusive"/&amp;gt;
 *     &amp;lt;enumeration value="Executive"/&amp;gt;
 *     &amp;lt;enumeration value="Luxury"/&amp;gt;
 *     &amp;lt;enumeration value="Regular"/&amp;gt;
 *     &amp;lt;enumeration value="Shuttle"/&amp;gt;
 *     &amp;lt;enumeration value="VIP"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_LevelOfService_Base")
@XmlEnum
public enum ListLevelOfServiceBase {

    @XmlEnumValue("Deluxe")
    DELUXE("Deluxe"),
    @XmlEnumValue("Economy")
    ECONOMY("Economy"),
    @XmlEnumValue("Exclusive")
    EXCLUSIVE("Exclusive"),
    @XmlEnumValue("Executive")
    EXECUTIVE("Executive"),
    @XmlEnumValue("Luxury")
    LUXURY("Luxury"),
    @XmlEnumValue("Regular")
    REGULAR("Regular"),
    @XmlEnumValue("Shuttle")
    SHUTTLE("Shuttle"),
    VIP("VIP"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListLevelOfServiceBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListLevelOfServiceBase fromValue(String v) {
        for (ListLevelOfServiceBase c: ListLevelOfServiceBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
