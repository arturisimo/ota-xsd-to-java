
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 *  Pricing information for an itinerary.
 * 
 * &lt;p&gt;Clase Java para BookingPriceInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="BookingPriceInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirItineraryPricingInfoType"&amp;gt;
 *       &amp;lt;attribute name="RepriceRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingPriceInfoType")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo.class
})
public class BookingPriceInfoType
    extends AirItineraryPricingInfoType
{

    @XmlAttribute(name = "RepriceRequired")
    protected Boolean repriceRequired;

    /**
     * Obtiene el valor de la propiedad repriceRequired.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRepriceRequired() {
        return repriceRequired;
    }

    /**
     * Define el valor de la propiedad repriceRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRepriceRequired(Boolean value) {
        this.repriceRequired = value;
    }

}
