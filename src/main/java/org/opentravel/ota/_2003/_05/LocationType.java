
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Code and optional string to describe a location point.
 * 
 * &lt;p&gt;Clase Java para LocationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="LocationType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationType", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.OriginLocation.class,
    org.opentravel.ota._2003._05.OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.DestinationLocation.class,
    org.opentravel.ota._2003._05.OTAAirBaggageRS.BaggageList.OriginDestination.OriginLocation.class,
    org.opentravel.ota._2003._05.OTAAirBaggageRS.BaggageList.OriginDestination.DestinationLocation.class,
    org.opentravel.ota._2003._05.OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations.ConnectionLocation.class,
    org.opentravel.ota._2003._05.OTAVehExchangeRQ.ExchangeLocation.class,
    org.opentravel.ota._2003._05.OTAVehLocDetailRQ.Location.class,
    org.opentravel.ota._2003._05.ConnectionType.ConnectionLocation.class,
    org.opentravel.ota._2003._05.OriginDestinationInformationType.OriginLocation.class,
    org.opentravel.ota._2003._05.OriginDestinationInformationType.DestinationLocation.class,
    AirportPrefType.class,
    org.opentravel.ota._2003._05.VehicleAvailCoreType.VendorLocation.class,
    org.opentravel.ota._2003._05.VehicleAvailCoreType.DropOffLocation.class,
    org.opentravel.ota._2003._05.VehicleLocationAdditionalDetailsType.OneWayDropLocations.OneWayDropLocation.class,
    org.opentravel.ota._2003._05.RailConnectionType.ConnectionLocation.class,
    org.opentravel.ota._2003._05.RailOriginDestinationInformationType.OriginLocation.class,
    org.opentravel.ota._2003._05.RailOriginDestinationInformationType.DestinationLocation.class,
    org.opentravel.ota._2003._05.RailOriginDestinationSummaryType.OriginLocation.class,
    org.opentravel.ota._2003._05.RailOriginDestinationSummaryType.DestinationLocation.class,
    StationType.class,
    LocationPrefType.class,
    org.opentravel.ota._2003._05.VerificationType.StartLocation.class,
    org.opentravel.ota._2003._05.VerificationType.EndLocation.class,
    org.opentravel.ota._2003._05.SailingBaseType.DeparturePort.class,
    org.opentravel.ota._2003._05.SailingBaseType.ArrivalPort.class,
    org.opentravel.ota._2003._05.ItemSearchCriterionType.CodeRef.class,
    org.opentravel.ota._2003._05.VehicleRentalCoreType.PickUpLocation.class,
    org.opentravel.ota._2003._05.VehicleRentalCoreType.ReturnLocation.class
})
public class LocationType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "LocationCode")
    protected String locationCode;
    @XmlAttribute(name = "CodeContext")
    protected String codeContext;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad locationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * Define el valor de la propiedad locationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCode(String value) {
        this.locationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad codeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeContext() {
        return codeContext;
    }

    /**
     * Define el valor de la propiedad codeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeContext(String value) {
        this.codeContext = value;
    }

}
