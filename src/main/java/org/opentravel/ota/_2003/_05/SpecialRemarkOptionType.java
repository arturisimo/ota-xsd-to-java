
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para SpecialRemarkOptionType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="SpecialRemarkOptionType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Itinerary"/&amp;gt;
 *     &amp;lt;enumeration value="Invoice"/&amp;gt;
 *     &amp;lt;enumeration value="Endorsement"/&amp;gt;
 *     &amp;lt;enumeration value="Save"/&amp;gt;
 *     &amp;lt;enumeration value="Confidential"/&amp;gt;
 *     &amp;lt;enumeration value="Free"/&amp;gt;
 *     &amp;lt;enumeration value="GRMS"/&amp;gt;
 *     &amp;lt;enumeration value="Split"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "SpecialRemarkOptionType")
@XmlEnum
public enum SpecialRemarkOptionType {


    /**
     * Remarks apply to the itinerary.
     * 
     */
    @XmlEnumValue("Itinerary")
    ITINERARY("Itinerary"),

    /**
     * Remarks apply to the invoice.
     * 
     */
    @XmlEnumValue("Invoice")
    INVOICE("Invoice"),

    /**
     * Remarks apply to the endorsement.
     * 
     */
    @XmlEnumValue("Endorsement")
    ENDORSEMENT("Endorsement"),

    /**
     * Remarks which can be deleted by the author only.
     * 
     */
    @XmlEnumValue("Save")
    SAVE("Save"),

    /**
     * Confidential remarks which are visible only to the author and system providers.
     * 
     */
    @XmlEnumValue("Confidential")
    CONFIDENTIAL("Confidential"),

    /**
     * Free text remarks which can be sent to specific airlines.
     * 
     */
    @XmlEnumValue("Free")
    FREE("Free"),

    /**
     * Remarks from or to a specific group revenue management system (GRMS).
     * 
     */
    GRMS("GRMS"),

    /**
     * Remarks containing information about split transaction (Split off PNR address, time, who, etc.).
     * 
     */
    @XmlEnumValue("Split")
    SPLIT("Split");
    private final String value;

    SpecialRemarkOptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SpecialRemarkOptionType fromValue(String v) {
        for (SpecialRemarkOptionType c: SpecialRemarkOptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
