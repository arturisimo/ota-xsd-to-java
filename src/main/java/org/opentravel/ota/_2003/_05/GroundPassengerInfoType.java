
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Passenger information that may determine the class of vehicle required.
 * 
 * &lt;p&gt;Clase Java para GroundPassengerInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundPassengerInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="MaximumPassengers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="MaximumBaggage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ChildCarSeatInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundPassengerInfoType")
public class GroundPassengerInfoType {

    @XmlAttribute(name = "MaximumPassengers")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maximumPassengers;
    @XmlAttribute(name = "MaximumBaggage")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maximumBaggage;
    @XmlAttribute(name = "ChildCarSeatInd")
    protected Boolean childCarSeatInd;

    /**
     * Obtiene el valor de la propiedad maximumPassengers.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumPassengers() {
        return maximumPassengers;
    }

    /**
     * Define el valor de la propiedad maximumPassengers.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumPassengers(BigInteger value) {
        this.maximumPassengers = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumBaggage.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumBaggage() {
        return maximumBaggage;
    }

    /**
     * Define el valor de la propiedad maximumBaggage.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumBaggage(BigInteger value) {
        this.maximumBaggage = value;
    }

    /**
     * Obtiene el valor de la propiedad childCarSeatInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChildCarSeatInd() {
        return childCarSeatInd;
    }

    /**
     * Define el valor de la propiedad childCarSeatInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChildCarSeatInd(Boolean value) {
        this.childCarSeatInd = value;
    }

}
