
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para RatePlanEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="RatePlanEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Government"/&amp;gt;
 *     &amp;lt;enumeration value="Negotiated"/&amp;gt;
 *     &amp;lt;enumeration value="Preferred"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "RatePlanEnum")
@XmlEnum
public enum RatePlanEnum {


    /**
     * Inventory is available for sale.
     * 
     */
    @XmlEnumValue("Government")
    GOVERNMENT("Government"),

    /**
     * Inventory is not available for sale.
     * 
     */
    @XmlEnumValue("Negotiated")
    NEGOTIATED("Negotiated"),

    /**
     * Inventory is not available for sale to arriving guests.
     * 
     */
    @XmlEnumValue("Preferred")
    PREFERRED("Preferred"),

    /**
     * Inventory may not be available for sale to arriving guests.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    RatePlanEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RatePlanEnum fromValue(String v) {
        for (RatePlanEnum c: RatePlanEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
