
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para TimeUnitType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="TimeUnitType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to16"&amp;gt;
 *     &amp;lt;enumeration value="Year"/&amp;gt;
 *     &amp;lt;enumeration value="Month"/&amp;gt;
 *     &amp;lt;enumeration value="Week"/&amp;gt;
 *     &amp;lt;enumeration value="Day"/&amp;gt;
 *     &amp;lt;enumeration value="Hour"/&amp;gt;
 *     &amp;lt;enumeration value="Second"/&amp;gt;
 *     &amp;lt;enumeration value="FullDuration"/&amp;gt;
 *     &amp;lt;enumeration value="Minute"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "TimeUnitType")
@XmlEnum
public enum TimeUnitType {

    @XmlEnumValue("Year")
    YEAR("Year"),
    @XmlEnumValue("Month")
    MONTH("Month"),
    @XmlEnumValue("Week")
    WEEK("Week"),
    @XmlEnumValue("Day")
    DAY("Day"),
    @XmlEnumValue("Hour")
    HOUR("Hour"),
    @XmlEnumValue("Second")
    SECOND("Second"),
    @XmlEnumValue("FullDuration")
    FULL_DURATION("FullDuration"),
    @XmlEnumValue("Minute")
    MINUTE("Minute");
    private final String value;

    TimeUnitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TimeUnitType fromValue(String v) {
        for (TimeUnitType c: TimeUnitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
