
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferLoyaltySector.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferLoyaltySector"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Leisure"/&amp;gt;
 *     &amp;lt;enumeration value="Lodging"/&amp;gt;
 *     &amp;lt;enumeration value="Transportation"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferLoyaltySector")
@XmlEnum
public enum ListOfferLoyaltySector {


    /**
     * Leisure and activity suppliers, including cruise, day tour, actvity, package tour and golf.
     * 
     */
    @XmlEnumValue("Leisure")
    LEISURE("Leisure"),

    /**
     * Accommodation suppliers, including hotel, hostel and vacation rental.
     * 
     */
    @XmlEnumValue("Lodging")
    LODGING("Lodging"),

    /**
     * Transportation suppliers, including air, car, ground and rail.
     * 
     */
    @XmlEnumValue("Transportation")
    TRANSPORTATION("Transportation"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferLoyaltySector(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferLoyaltySector fromValue(String v) {
        for (ListOfferLoyaltySector c: ListOfferLoyaltySector.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
