
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para CompartmentTypeEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="CompartmentTypeEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NotSignificant"/&amp;gt;
 *     &amp;lt;enumeration value="Family"/&amp;gt;
 *     &amp;lt;enumeration value="Quite"/&amp;gt;
 *     &amp;lt;enumeration value="Conference"/&amp;gt;
 *     &amp;lt;enumeration value="CompartmentWithoutAnimals"/&amp;gt;
 *     &amp;lt;enumeration value="Complete"/&amp;gt;
 *     &amp;lt;enumeration value="Video"/&amp;gt;
 *     &amp;lt;enumeration value="Pram"/&amp;gt;
 *     &amp;lt;enumeration value="WomanAndChild"/&amp;gt;
 *     &amp;lt;enumeration value="EasyAccess"/&amp;gt;
 *     &amp;lt;enumeration value="T2"/&amp;gt;
 *     &amp;lt;enumeration value="T3"/&amp;gt;
 *     &amp;lt;enumeration value="T4"/&amp;gt;
 *     &amp;lt;enumeration value="T6"/&amp;gt;
 *     &amp;lt;enumeration value="C2"/&amp;gt;
 *     &amp;lt;enumeration value="C4"/&amp;gt;
 *     &amp;lt;enumeration value="C5"/&amp;gt;
 *     &amp;lt;enumeration value="C6"/&amp;gt;
 *     &amp;lt;enumeration value="Single"/&amp;gt;
 *     &amp;lt;enumeration value="Double"/&amp;gt;
 *     &amp;lt;enumeration value="SingleSuite"/&amp;gt;
 *     &amp;lt;enumeration value="DoubleSuite"/&amp;gt;
 *     &amp;lt;enumeration value="Special"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "CompartmentTypeEnum")
@XmlEnum
public enum CompartmentTypeEnum {

    @XmlEnumValue("NotSignificant")
    NOT_SIGNIFICANT("NotSignificant"),
    @XmlEnumValue("Family")
    FAMILY("Family"),
    @XmlEnumValue("Quite")
    QUITE("Quite"),
    @XmlEnumValue("Conference")
    CONFERENCE("Conference"),

    /**
     * Compartment without animals
     * 
     */
    @XmlEnumValue("CompartmentWithoutAnimals")
    COMPARTMENT_WITHOUT_ANIMALS("CompartmentWithoutAnimals"),
    @XmlEnumValue("Complete")
    COMPLETE("Complete"),

    /**
     * Video compartment
     * 
     */
    @XmlEnumValue("Video")
    VIDEO("Video"),
    @XmlEnumValue("Pram")
    PRAM("Pram"),

    /**
     * Woman and Child compartment
     * 
     */
    @XmlEnumValue("WomanAndChild")
    WOMAN_AND_CHILD("WomanAndChild"),

    /**
     * Placess with easy access - PRMS
     * 
     */
    @XmlEnumValue("EasyAccess")
    EASY_ACCESS("EasyAccess"),

    /**
     * Two-berth compartment
     * 
     */
    @XmlEnumValue("T2")
    T_2("T2"),

    /**
     * Three-berth compartment
     * 
     */
    @XmlEnumValue("T3")
    T_3("T3"),

    /**
     * Four-berth compartment
     * 
     */
    @XmlEnumValue("T4")
    T_4("T4"),

    /**
     * Six-berth compartment
     * 
     */
    @XmlEnumValue("T6")
    T_6("T6"),

    /**
     * compartment with 2 couchettes
     * 
     */
    @XmlEnumValue("C2")
    C_2("C2"),

    /**
     * compartment with 4 couchettes
     * 
     */
    @XmlEnumValue("C4")
    C_4("C4"),

    /**
     * compartment with 5 couchettes
     * 
     */
    @XmlEnumValue("C5")
    C_5("C5"),

    /**
     * compartment with 6 couchettes
     * 
     */
    @XmlEnumValue("C6")
    C_6("C6"),

    /**
     * Single-berth compartment
     * 
     */
    @XmlEnumValue("Single")
    SINGLE("Single"),

    /**
     * Double-berth compartment
     * 
     */
    @XmlEnumValue("Double")
    DOUBLE("Double"),

    /**
     * Single suite compartment
     * 
     */
    @XmlEnumValue("SingleSuite")
    SINGLE_SUITE("SingleSuite"),

    /**
     * Double suite compartment
     * 
     */
    @XmlEnumValue("DoubleSuite")
    DOUBLE_SUITE("DoubleSuite"),

    /**
     * Special-berth compartment
     * 
     */
    @XmlEnumValue("Special")
    SPECIAL("Special"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support to support additional compartment type. The Value corresponding to "Other_" will be specified in the  "Value" attribute. See CompartmentType.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    CompartmentTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CompartmentTypeEnum fromValue(String v) {
        for (CompartmentTypeEnum c: CompartmentTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
