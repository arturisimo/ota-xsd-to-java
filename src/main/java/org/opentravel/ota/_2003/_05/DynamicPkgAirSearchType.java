
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A collection of air search criteria.
 * 
 * &lt;p&gt;Clase Java para DynamicPkgAirSearchType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DynamicPkgAirSearchType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgSearchType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AirItinerary" type="{http://www.opentravel.org/OTA/2003/05}AirItineraryType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="OriginDestinationInformation" type="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SpecificFlightInfo" type="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TravelPreferences" type="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TravelRefSummary" type="{http://www.opentravel.org/OTA/2003/05}TravelRefSummaryType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="RequestType" type="{http://www.opentravel.org/OTA/2003/05}AirComponentSearchType" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicPkgAirSearchType", propOrder = {
    "airItinerary",
    "originDestinationInformation",
    "specificFlightInfo",
    "travelPreferences",
    "travelRefSummary",
    "tpaExtensions"
})
public class DynamicPkgAirSearchType
    extends DynamicPkgSearchType
{

    @XmlElement(name = "AirItinerary")
    protected AirItineraryType airItinerary;
    @XmlElement(name = "OriginDestinationInformation")
    protected List<OriginDestinationInformationType> originDestinationInformation;
    @XmlElement(name = "SpecificFlightInfo")
    protected SpecificFlightInfoType specificFlightInfo;
    @XmlElement(name = "TravelPreferences")
    protected AirSearchPrefsType travelPreferences;
    @XmlElement(name = "TravelRefSummary")
    protected TravelRefSummaryType travelRefSummary;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "RequestType")
    protected AirComponentSearchType requestType;

    /**
     * Obtiene el valor de la propiedad airItinerary.
     * 
     * @return
     *     possible object is
     *     {@link AirItineraryType }
     *     
     */
    public AirItineraryType getAirItinerary() {
        return airItinerary;
    }

    /**
     * Define el valor de la propiedad airItinerary.
     * 
     * @param value
     *     allowed object is
     *     {@link AirItineraryType }
     *     
     */
    public void setAirItinerary(AirItineraryType value) {
        this.airItinerary = value;
    }

    /**
     * Gets the value of the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOriginDestinationInformation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OriginDestinationInformationType }
     * 
     * 
     */
    public List<OriginDestinationInformationType> getOriginDestinationInformation() {
        if (originDestinationInformation == null) {
            originDestinationInformation = new ArrayList<OriginDestinationInformationType>();
        }
        return this.originDestinationInformation;
    }

    /**
     * Obtiene el valor de la propiedad specificFlightInfo.
     * 
     * @return
     *     possible object is
     *     {@link SpecificFlightInfoType }
     *     
     */
    public SpecificFlightInfoType getSpecificFlightInfo() {
        return specificFlightInfo;
    }

    /**
     * Define el valor de la propiedad specificFlightInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificFlightInfoType }
     *     
     */
    public void setSpecificFlightInfo(SpecificFlightInfoType value) {
        this.specificFlightInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad travelPreferences.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchPrefsType }
     *     
     */
    public AirSearchPrefsType getTravelPreferences() {
        return travelPreferences;
    }

    /**
     * Define el valor de la propiedad travelPreferences.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchPrefsType }
     *     
     */
    public void setTravelPreferences(AirSearchPrefsType value) {
        this.travelPreferences = value;
    }

    /**
     * Obtiene el valor de la propiedad travelRefSummary.
     * 
     * @return
     *     possible object is
     *     {@link TravelRefSummaryType }
     *     
     */
    public TravelRefSummaryType getTravelRefSummary() {
        return travelRefSummary;
    }

    /**
     * Define el valor de la propiedad travelRefSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelRefSummaryType }
     *     
     */
    public void setTravelRefSummary(TravelRefSummaryType value) {
        this.travelRefSummary = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad requestType.
     * 
     * @return
     *     possible object is
     *     {@link AirComponentSearchType }
     *     
     */
    public AirComponentSearchType getRequestType() {
        return requestType;
    }

    /**
     * Define el valor de la propiedad requestType.
     * 
     * @param value
     *     allowed object is
     *     {@link AirComponentSearchType }
     *     
     */
    public void setRequestType(AirComponentSearchType value) {
        this.requestType = value;
    }

}
