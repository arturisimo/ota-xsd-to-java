
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Collection of insurance coverage requirements from the individuals requesting coverage.
 * 
 * &lt;p&gt;Clase Java para RequestedCoveragesType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RequestedCoveragesType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CoverageRequirement" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" maxOccurs="20"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestedCoveragesType", propOrder = {
    "coverageRequirement"
})
public class RequestedCoveragesType {

    @XmlElement(name = "CoverageRequirement", required = true)
    protected List<CoverageLimitType> coverageRequirement;

    /**
     * Gets the value of the coverageRequirement property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coverageRequirement property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCoverageRequirement().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CoverageLimitType }
     * 
     * 
     */
    public List<CoverageLimitType> getCoverageRequirement() {
        if (coverageRequirement == null) {
            coverageRequirement = new ArrayList<CoverageLimitType>();
        }
        return this.coverageRequirement;
    }

}
