
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferDistanceUOM.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferDistanceUOM"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Block"/&amp;gt;
 *     &amp;lt;enumeration value="Mile"/&amp;gt;
 *     &amp;lt;enumeration value="Kilometer"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferDistanceUOM")
@XmlEnum
public enum ListOfferDistanceUOM {

    @XmlEnumValue("Block")
    BLOCK("Block"),
    @XmlEnumValue("Mile")
    MILE("Mile"),
    @XmlEnumValue("Kilometer")
    KILOMETER("Kilometer"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferDistanceUOM(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferDistanceUOM fromValue(String v) {
        for (ListOfferDistanceUOM c: ListOfferDistanceUOM.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
