
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains an array of ancillary items being or previously purchased for this trip. Internal airline and ATPCO encoding for airline delivered ancillary items and third party trip insurance are supported.
 * 
 * &lt;p&gt;Clase Java para AirPurchasedOfferType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirPurchasedOfferType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="PurchasedItem" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AppliesTo" type="{http://www.opentravel.org/OTA/2003/05}ApplyPriceToType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="LandProductInfo" type="{http://www.opentravel.org/OTA/2003/05}AirLandProductType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="InsuranceProduct" type="{http://www.opentravel.org/OTA/2003/05}AirInsuranceOfferType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirPurchasedOfferType", propOrder = {
    "purchasedItem"
})
public class AirPurchasedOfferType {

    @XmlElement(name = "PurchasedItem", required = true)
    protected List<AirPurchasedOfferType.PurchasedItem> purchasedItem;

    /**
     * Gets the value of the purchasedItem property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the purchasedItem property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPurchasedItem().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPurchasedOfferType.PurchasedItem }
     * 
     * 
     */
    public List<AirPurchasedOfferType.PurchasedItem> getPurchasedItem() {
        if (purchasedItem == null) {
            purchasedItem = new ArrayList<AirPurchasedOfferType.PurchasedItem>();
        }
        return this.purchasedItem;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AppliesTo" type="{http://www.opentravel.org/OTA/2003/05}ApplyPriceToType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="LandProductInfo" type="{http://www.opentravel.org/OTA/2003/05}AirLandProductType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="InsuranceProduct" type="{http://www.opentravel.org/OTA/2003/05}AirInsuranceOfferType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "appliesTo",
        "landProductInfo",
        "insuranceProduct"
    })
    public static class PurchasedItem
        extends AncillaryServiceDetailType
    {

        @XmlElement(name = "AppliesTo")
        protected ApplyPriceToType appliesTo;
        @XmlElement(name = "LandProductInfo")
        protected AirLandProductType landProductInfo;
        @XmlElement(name = "InsuranceProduct")
        protected AirInsuranceOfferType insuranceProduct;

        /**
         * Obtiene el valor de la propiedad appliesTo.
         * 
         * @return
         *     possible object is
         *     {@link ApplyPriceToType }
         *     
         */
        public ApplyPriceToType getAppliesTo() {
            return appliesTo;
        }

        /**
         * Define el valor de la propiedad appliesTo.
         * 
         * @param value
         *     allowed object is
         *     {@link ApplyPriceToType }
         *     
         */
        public void setAppliesTo(ApplyPriceToType value) {
            this.appliesTo = value;
        }

        /**
         * Obtiene el valor de la propiedad landProductInfo.
         * 
         * @return
         *     possible object is
         *     {@link AirLandProductType }
         *     
         */
        public AirLandProductType getLandProductInfo() {
            return landProductInfo;
        }

        /**
         * Define el valor de la propiedad landProductInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirLandProductType }
         *     
         */
        public void setLandProductInfo(AirLandProductType value) {
            this.landProductInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad insuranceProduct.
         * 
         * @return
         *     possible object is
         *     {@link AirInsuranceOfferType }
         *     
         */
        public AirInsuranceOfferType getInsuranceProduct() {
            return insuranceProduct;
        }

        /**
         * Define el valor de la propiedad insuranceProduct.
         * 
         * @param value
         *     allowed object is
         *     {@link AirInsuranceOfferType }
         *     
         */
        public void setInsuranceProduct(AirInsuranceOfferType value) {
            this.insuranceProduct = value;
        }

    }

}
