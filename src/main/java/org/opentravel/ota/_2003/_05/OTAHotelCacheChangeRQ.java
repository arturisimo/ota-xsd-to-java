
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CacheSearchCriteria"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CacheSearchCriterion" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="GeographicalInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Position" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}AddressType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="Radius" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RatePlanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="ChainCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="BrandCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="HotelCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="InvBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                           &amp;lt;attribute name="CacheStartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                           &amp;lt;attribute name="CacheEndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="MoreEchoDataToken" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *                 &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="AvailabilityInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RatesInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="HotelContentInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="CacheFromTimestamp" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MaxResponsesGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "cacheSearchCriteria"
})
@XmlRootElement(name = "OTA_HotelCacheChangeRQ")
public class OTAHotelCacheChangeRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "CacheSearchCriteria", required = true)
    protected OTAHotelCacheChangeRQ.CacheSearchCriteria cacheSearchCriteria;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad cacheSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria }
     *     
     */
    public OTAHotelCacheChangeRQ.CacheSearchCriteria getCacheSearchCriteria() {
        return cacheSearchCriteria;
    }

    /**
     * Define el valor de la propiedad cacheSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria }
     *     
     */
    public void setCacheSearchCriteria(OTAHotelCacheChangeRQ.CacheSearchCriteria value) {
        this.cacheSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CacheSearchCriterion" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="GeographicalInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Position" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}AddressType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="Radius" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RatePlanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="ChainCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="BrandCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="HotelCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="InvBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="CacheStartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="CacheEndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="MoreEchoDataToken" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
     *       &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="AvailabilityInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RatesInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="HotelContentInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="CacheFromTimestamp" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cacheSearchCriterion"
    })
    public static class CacheSearchCriteria {

        @XmlElement(name = "CacheSearchCriterion", required = true)
        protected List<OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion> cacheSearchCriterion;
        @XmlAttribute(name = "MoreEchoDataToken")
        @XmlSchemaType(name = "anySimpleType")
        protected String moreEchoDataToken;
        @XmlAttribute(name = "ResponseType")
        protected String responseType;
        @XmlAttribute(name = "AvailabilityInd", required = true)
        protected boolean availabilityInd;
        @XmlAttribute(name = "RatesInd", required = true)
        protected boolean ratesInd;
        @XmlAttribute(name = "HotelContentInd", required = true)
        protected boolean hotelContentInd;
        @XmlAttribute(name = "CacheFromTimestamp", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar cacheFromTimestamp;

        /**
         * Gets the value of the cacheSearchCriterion property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cacheSearchCriterion property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCacheSearchCriterion().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion }
         * 
         * 
         */
        public List<OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion> getCacheSearchCriterion() {
            if (cacheSearchCriterion == null) {
                cacheSearchCriterion = new ArrayList<OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion>();
            }
            return this.cacheSearchCriterion;
        }

        /**
         * Obtiene el valor de la propiedad moreEchoDataToken.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoreEchoDataToken() {
            return moreEchoDataToken;
        }

        /**
         * Define el valor de la propiedad moreEchoDataToken.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoreEchoDataToken(String value) {
            this.moreEchoDataToken = value;
        }

        /**
         * Obtiene el valor de la propiedad responseType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseType() {
            return responseType;
        }

        /**
         * Define el valor de la propiedad responseType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseType(String value) {
            this.responseType = value;
        }

        /**
         * Obtiene el valor de la propiedad availabilityInd.
         * 
         */
        public boolean isAvailabilityInd() {
            return availabilityInd;
        }

        /**
         * Define el valor de la propiedad availabilityInd.
         * 
         */
        public void setAvailabilityInd(boolean value) {
            this.availabilityInd = value;
        }

        /**
         * Obtiene el valor de la propiedad ratesInd.
         * 
         */
        public boolean isRatesInd() {
            return ratesInd;
        }

        /**
         * Define el valor de la propiedad ratesInd.
         * 
         */
        public void setRatesInd(boolean value) {
            this.ratesInd = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelContentInd.
         * 
         */
        public boolean isHotelContentInd() {
            return hotelContentInd;
        }

        /**
         * Define el valor de la propiedad hotelContentInd.
         * 
         */
        public void setHotelContentInd(boolean value) {
            this.hotelContentInd = value;
        }

        /**
         * Obtiene el valor de la propiedad cacheFromTimestamp.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCacheFromTimestamp() {
            return cacheFromTimestamp;
        }

        /**
         * Define el valor de la propiedad cacheFromTimestamp.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCacheFromTimestamp(XMLGregorianCalendar value) {
            this.cacheFromTimestamp = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="GeographicalInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Position" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}AddressType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Radius" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RatePlanGroup"/&amp;gt;
         *       &amp;lt;attribute name="ChainCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="BrandCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="HotelCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="InvBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="CacheStartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="CacheEndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "geographicalInfo"
        })
        public static class CacheSearchCriterion {

            @XmlElement(name = "GeographicalInfo")
            protected OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo geographicalInfo;
            @XmlAttribute(name = "ChainCode")
            protected String chainCode;
            @XmlAttribute(name = "BrandCode")
            protected String brandCode;
            @XmlAttribute(name = "HotelCode")
            protected String hotelCode;
            @XmlAttribute(name = "RoomTypeCode")
            protected String roomTypeCode;
            @XmlAttribute(name = "InvBlockCode")
            protected String invBlockCode;
            @XmlAttribute(name = "CacheStartDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar cacheStartDateTime;
            @XmlAttribute(name = "CacheEndDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar cacheEndDateTime;
            @XmlAttribute(name = "RatePlanType")
            protected String ratePlanType;
            @XmlAttribute(name = "RatePlanCode")
            protected String ratePlanCode;
            @XmlAttribute(name = "RatePlanID")
            protected String ratePlanID;
            @XmlAttribute(name = "RatePlanQualifier")
            protected Boolean ratePlanQualifier;
            @XmlAttribute(name = "PromotionCode")
            protected String promotionCode;
            @XmlAttribute(name = "PromotionVendorCode")
            protected List<String> promotionVendorCode;
            @XmlAttribute(name = "RatePlanCategory")
            protected String ratePlanCategory;

            /**
             * Obtiene el valor de la propiedad geographicalInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo }
             *     
             */
            public OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo getGeographicalInfo() {
                return geographicalInfo;
            }

            /**
             * Define el valor de la propiedad geographicalInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo }
             *     
             */
            public void setGeographicalInfo(OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo value) {
                this.geographicalInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad chainCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainCode() {
                return chainCode;
            }

            /**
             * Define el valor de la propiedad chainCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainCode(String value) {
                this.chainCode = value;
            }

            /**
             * Obtiene el valor de la propiedad brandCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandCode() {
                return brandCode;
            }

            /**
             * Define el valor de la propiedad brandCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandCode(String value) {
                this.brandCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCode() {
                return hotelCode;
            }

            /**
             * Define el valor de la propiedad hotelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCode(String value) {
                this.hotelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad roomTypeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRoomTypeCode() {
                return roomTypeCode;
            }

            /**
             * Define el valor de la propiedad roomTypeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRoomTypeCode(String value) {
                this.roomTypeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad invBlockCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvBlockCode() {
                return invBlockCode;
            }

            /**
             * Define el valor de la propiedad invBlockCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvBlockCode(String value) {
                this.invBlockCode = value;
            }

            /**
             * Obtiene el valor de la propiedad cacheStartDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCacheStartDateTime() {
                return cacheStartDateTime;
            }

            /**
             * Define el valor de la propiedad cacheStartDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCacheStartDateTime(XMLGregorianCalendar value) {
                this.cacheStartDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad cacheEndDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCacheEndDateTime() {
                return cacheEndDateTime;
            }

            /**
             * Define el valor de la propiedad cacheEndDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCacheEndDateTime(XMLGregorianCalendar value) {
                this.cacheEndDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatePlanType() {
                return ratePlanType;
            }

            /**
             * Define el valor de la propiedad ratePlanType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatePlanType(String value) {
                this.ratePlanType = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatePlanCode() {
                return ratePlanCode;
            }

            /**
             * Define el valor de la propiedad ratePlanCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatePlanCode(String value) {
                this.ratePlanCode = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatePlanID() {
                return ratePlanID;
            }

            /**
             * Define el valor de la propiedad ratePlanID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatePlanID(String value) {
                this.ratePlanID = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanQualifier.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRatePlanQualifier() {
                return ratePlanQualifier;
            }

            /**
             * Define el valor de la propiedad ratePlanQualifier.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRatePlanQualifier(Boolean value) {
                this.ratePlanQualifier = value;
            }

            /**
             * Obtiene el valor de la propiedad promotionCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPromotionCode() {
                return promotionCode;
            }

            /**
             * Define el valor de la propiedad promotionCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPromotionCode(String value) {
                this.promotionCode = value;
            }

            /**
             * Gets the value of the promotionVendorCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionVendorCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotionVendorCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPromotionVendorCode() {
                if (promotionVendorCode == null) {
                    promotionVendorCode = new ArrayList<String>();
                }
                return this.promotionVendorCode;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanCategory.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatePlanCategory() {
                return ratePlanCategory;
            }

            /**
             * Define el valor de la propiedad ratePlanCategory.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatePlanCategory(String value) {
                this.ratePlanCategory = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Position" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}AddressType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Radius" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "position",
                "address",
                "radius"
            })
            public static class GeographicalInfo {

                @XmlElement(name = "Position")
                protected OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Position position;
                @XmlElement(name = "Address")
                protected AddressType address;
                @XmlElement(name = "Radius")
                protected OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Radius radius;

                /**
                 * Obtiene el valor de la propiedad position.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Position }
                 *     
                 */
                public OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Position getPosition() {
                    return position;
                }

                /**
                 * Define el valor de la propiedad position.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Position }
                 *     
                 */
                public void setPosition(OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Position value) {
                    this.position = value;
                }

                /**
                 * Obtiene el valor de la propiedad address.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AddressType }
                 *     
                 */
                public AddressType getAddress() {
                    return address;
                }

                /**
                 * Define el valor de la propiedad address.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AddressType }
                 *     
                 */
                public void setAddress(AddressType value) {
                    this.address = value;
                }

                /**
                 * Obtiene el valor de la propiedad radius.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Radius }
                 *     
                 */
                public OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Radius getRadius() {
                    return radius;
                }

                /**
                 * Define el valor de la propiedad radius.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Radius }
                 *     
                 */
                public void setRadius(OTAHotelCacheChangeRQ.CacheSearchCriteria.CacheSearchCriterion.GeographicalInfo.Radius value) {
                    this.radius = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Position {

                    @XmlAttribute(name = "Latitude")
                    protected String latitude;
                    @XmlAttribute(name = "Longitude")
                    protected String longitude;
                    @XmlAttribute(name = "Altitude")
                    protected String altitude;
                    @XmlAttribute(name = "AltitudeUnitOfMeasureCode")
                    protected String altitudeUnitOfMeasureCode;
                    @XmlAttribute(name = "PositionAccuracyCode")
                    protected String positionAccuracyCode;

                    /**
                     * Obtiene el valor de la propiedad latitude.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLatitude() {
                        return latitude;
                    }

                    /**
                     * Define el valor de la propiedad latitude.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLatitude(String value) {
                        this.latitude = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad longitude.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLongitude() {
                        return longitude;
                    }

                    /**
                     * Define el valor de la propiedad longitude.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLongitude(String value) {
                        this.longitude = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad altitude.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAltitude() {
                        return altitude;
                    }

                    /**
                     * Define el valor de la propiedad altitude.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAltitude(String value) {
                        this.altitude = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad altitudeUnitOfMeasureCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAltitudeUnitOfMeasureCode() {
                        return altitudeUnitOfMeasureCode;
                    }

                    /**
                     * Define el valor de la propiedad altitudeUnitOfMeasureCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAltitudeUnitOfMeasureCode(String value) {
                        this.altitudeUnitOfMeasureCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad positionAccuracyCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPositionAccuracyCode() {
                        return positionAccuracyCode;
                    }

                    /**
                     * Define el valor de la propiedad positionAccuracyCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPositionAccuracyCode(String value) {
                        this.positionAccuracyCode = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Radius {

                    @XmlAttribute(name = "Distance")
                    protected String distance;
                    @XmlAttribute(name = "DistanceMeasure")
                    protected String distanceMeasure;
                    @XmlAttribute(name = "Direction")
                    protected String direction;
                    @XmlAttribute(name = "DistanceMax")
                    protected String distanceMax;
                    @XmlAttribute(name = "UnitOfMeasureCode")
                    protected String unitOfMeasureCode;

                    /**
                     * Obtiene el valor de la propiedad distance.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDistance() {
                        return distance;
                    }

                    /**
                     * Define el valor de la propiedad distance.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDistance(String value) {
                        this.distance = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad distanceMeasure.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDistanceMeasure() {
                        return distanceMeasure;
                    }

                    /**
                     * Define el valor de la propiedad distanceMeasure.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDistanceMeasure(String value) {
                        this.distanceMeasure = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad direction.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDirection() {
                        return direction;
                    }

                    /**
                     * Define el valor de la propiedad direction.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDirection(String value) {
                        this.direction = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad distanceMax.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDistanceMax() {
                        return distanceMax;
                    }

                    /**
                     * Define el valor de la propiedad distanceMax.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDistanceMax(String value) {
                        this.distanceMax = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad unitOfMeasureCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUnitOfMeasureCode() {
                        return unitOfMeasureCode;
                    }

                    /**
                     * Define el valor de la propiedad unitOfMeasureCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUnitOfMeasureCode(String value) {
                        this.unitOfMeasureCode = value;
                    }

                }

            }

        }

    }

}
