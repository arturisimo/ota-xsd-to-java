
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Pickup and drop off requirement information for the tour/activity.
 * 
 * &lt;p&gt;Clase Java para TourActivityTransRequestType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityTransRequestType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="DateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *       &amp;lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="OtherInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="PersonQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="DropoffInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityTransRequestType")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRQ.BookingInfo.PickupDropoff.class,
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.PickupDropoff.class,
    org.opentravel.ota._2003._05.OTATourActivityModifyRQ.BookingInfo.PickupDropoff.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.PickupDropoff.class
})
public class TourActivityTransRequestType {

    @XmlAttribute(name = "DateTime")
    protected String dateTime;
    @XmlAttribute(name = "LocationName")
    protected String locationName;
    @XmlAttribute(name = "OtherInfo")
    protected String otherInfo;
    @XmlAttribute(name = "PersonQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger personQty;
    @XmlAttribute(name = "PickupInd")
    protected Boolean pickupInd;
    @XmlAttribute(name = "DropoffInd")
    protected Boolean dropoffInd;

    /**
     * Obtiene el valor de la propiedad dateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Define el valor de la propiedad dateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad locationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Define el valor de la propiedad locationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Obtiene el valor de la propiedad otherInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherInfo() {
        return otherInfo;
    }

    /**
     * Define el valor de la propiedad otherInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherInfo(String value) {
        this.otherInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad personQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPersonQty() {
        return personQty;
    }

    /**
     * Define el valor de la propiedad personQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPersonQty(BigInteger value) {
        this.personQty = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPickupInd() {
        return pickupInd;
    }

    /**
     * Define el valor de la propiedad pickupInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPickupInd(Boolean value) {
        this.pickupInd = value;
    }

    /**
     * Obtiene el valor de la propiedad dropoffInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDropoffInd() {
        return dropoffInd;
    }

    /**
     * Define el valor de la propiedad dropoffInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropoffInd(Boolean value) {
        this.dropoffInd = value;
    }

}
