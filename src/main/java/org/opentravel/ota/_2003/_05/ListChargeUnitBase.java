
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_ChargeUnit_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_ChargeUnit_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AdditionsPerStay"/&amp;gt;
 *     &amp;lt;enumeration value="Complimentary"/&amp;gt;
 *     &amp;lt;enumeration value="Event"/&amp;gt;
 *     &amp;lt;enumeration value="Gallon"/&amp;gt;
 *     &amp;lt;enumeration value="Item"/&amp;gt;
 *     &amp;lt;enumeration value="MaximumCharge"/&amp;gt;
 *     &amp;lt;enumeration value="MinimumCharge"/&amp;gt;
 *     &amp;lt;enumeration value="OneTimeUse"/&amp;gt;
 *     &amp;lt;enumeration value="OneWay"/&amp;gt;
 *     &amp;lt;enumeration value="Order"/&amp;gt;
 *     &amp;lt;enumeration value="OverMinuteCharge"/&amp;gt;
 *     &amp;lt;enumeration value="Person"/&amp;gt;
 *     &amp;lt;enumeration value="Rental"/&amp;gt;
 *     &amp;lt;enumeration value="Reservation/Booking"/&amp;gt;
 *     &amp;lt;enumeration value="Room"/&amp;gt;
 *     &amp;lt;enumeration value="Stay"/&amp;gt;
 *     &amp;lt;enumeration value="Tray"/&amp;gt;
 *     &amp;lt;enumeration value="Unit"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_ChargeUnit_Base")
@XmlEnum
public enum ListChargeUnitBase {

    @XmlEnumValue("AdditionsPerStay")
    ADDITIONS_PER_STAY("AdditionsPerStay"),
    @XmlEnumValue("Complimentary")
    COMPLIMENTARY("Complimentary"),
    @XmlEnumValue("Event")
    EVENT("Event"),
    @XmlEnumValue("Gallon")
    GALLON("Gallon"),
    @XmlEnumValue("Item")
    ITEM("Item"),
    @XmlEnumValue("MaximumCharge")
    MAXIMUM_CHARGE("MaximumCharge"),
    @XmlEnumValue("MinimumCharge")
    MINIMUM_CHARGE("MinimumCharge"),
    @XmlEnumValue("OneTimeUse")
    ONE_TIME_USE("OneTimeUse"),
    @XmlEnumValue("OneWay")
    ONE_WAY("OneWay"),
    @XmlEnumValue("Order")
    ORDER("Order"),
    @XmlEnumValue("OverMinuteCharge")
    OVER_MINUTE_CHARGE("OverMinuteCharge"),
    @XmlEnumValue("Person")
    PERSON("Person"),
    @XmlEnumValue("Rental")
    RENTAL("Rental"),
    @XmlEnumValue("Reservation/Booking")
    RESERVATION_BOOKING("Reservation/Booking"),
    @XmlEnumValue("Room")
    ROOM("Room"),
    @XmlEnumValue("Stay")
    STAY("Stay"),
    @XmlEnumValue("Tray")
    TRAY("Tray"),
    @XmlEnumValue("Unit")
    UNIT("Unit"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListChargeUnitBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListChargeUnitBase fromValue(String v) {
        for (ListChargeUnitBase c: ListChargeUnitBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
