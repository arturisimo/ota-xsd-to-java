
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Participant information, which may be age, birth date and/or an age qualifier and name information.
 * 
 * &lt;p&gt;Clase Java para TourActivityParticipantType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityParticipantType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CategoryInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ParticipantNameInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SpecialNeed" maxOccurs="5" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;simpleContent&amp;gt;
 *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/simpleContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="LodgingInfo" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="Gender"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Male"/&amp;gt;
 *                       &amp;lt;enumeration value="Female"/&amp;gt;
 *                       &amp;lt;enumeration value="Unknown"/&amp;gt;
 *                       &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
 *                       &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                 &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *       &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityParticipantType", propOrder = {
    "categoryInfo",
    "participantNameInfo",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.Extra.Pricing.Detailed.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.Extra.Pricing.Detailed.class
})
public class TourActivityParticipantType {

    @XmlElement(name = "CategoryInfo")
    protected TourActivityParticipantType.CategoryInfo categoryInfo;
    @XmlElement(name = "ParticipantNameInfo")
    protected TourActivityParticipantType.ParticipantNameInfo participantNameInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "Age")
    protected Integer age;
    @XmlAttribute(name = "BirthDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDate;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantity;

    /**
     * Obtiene el valor de la propiedad categoryInfo.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityParticipantType.CategoryInfo }
     *     
     */
    public TourActivityParticipantType.CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    /**
     * Define el valor de la propiedad categoryInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityParticipantType.CategoryInfo }
     *     
     */
    public void setCategoryInfo(TourActivityParticipantType.CategoryInfo value) {
        this.categoryInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad participantNameInfo.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityParticipantType.ParticipantNameInfo }
     *     
     */
    public TourActivityParticipantType.ParticipantNameInfo getParticipantNameInfo() {
        return participantNameInfo;
    }

    /**
     * Define el valor de la propiedad participantNameInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityParticipantType.ParticipantNameInfo }
     *     
     */
    public void setParticipantNameInfo(TourActivityParticipantType.ParticipantNameInfo value) {
        this.participantNameInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad age.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAge() {
        return age;
    }

    /**
     * Define el valor de la propiedad age.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAge(Integer value) {
        this.age = value;
    }

    /**
     * Obtiene el valor de la propiedad birthDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Define el valor de la propiedad birthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CategoryInfo
        extends AgeQualifierType
    {

        @XmlAttribute(name = "ParticipantCategoryID")
        protected String participantCategoryID;

        /**
         * Obtiene el valor de la propiedad participantCategoryID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParticipantCategoryID() {
            return participantCategoryID;
        }

        /**
         * Define el valor de la propiedad participantCategoryID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParticipantCategoryID(String value) {
            this.participantCategoryID = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SpecialNeed" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="LodgingInfo" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="Gender"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Male"/&amp;gt;
     *             &amp;lt;enumeration value="Female"/&amp;gt;
     *             &amp;lt;enumeration value="Unknown"/&amp;gt;
     *             &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
     *             &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *       &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "specialNeed",
        "lodgingInfo"
    })
    public static class ParticipantNameInfo
        extends PersonNameType
    {

        @XmlElement(name = "SpecialNeed")
        protected List<TourActivityParticipantType.ParticipantNameInfo.SpecialNeed> specialNeed;
        @XmlElement(name = "LodgingInfo")
        protected TourActivityLodgingType lodgingInfo;
        @XmlAttribute(name = "ParticipantID")
        protected String participantID;
        @XmlAttribute(name = "Gender")
        protected String gender;
        @XmlAttribute(name = "Nationality")
        protected String nationality;
        @XmlAttribute(name = "LeadCustomerInd")
        protected Boolean leadCustomerInd;

        /**
         * Gets the value of the specialNeed property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specialNeed property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSpecialNeed().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityParticipantType.ParticipantNameInfo.SpecialNeed }
         * 
         * 
         */
        public List<TourActivityParticipantType.ParticipantNameInfo.SpecialNeed> getSpecialNeed() {
            if (specialNeed == null) {
                specialNeed = new ArrayList<TourActivityParticipantType.ParticipantNameInfo.SpecialNeed>();
            }
            return this.specialNeed;
        }

        /**
         * Obtiene el valor de la propiedad lodgingInfo.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityLodgingType }
         *     
         */
        public TourActivityLodgingType getLodgingInfo() {
            return lodgingInfo;
        }

        /**
         * Define el valor de la propiedad lodgingInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityLodgingType }
         *     
         */
        public void setLodgingInfo(TourActivityLodgingType value) {
            this.lodgingInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad participantID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParticipantID() {
            return participantID;
        }

        /**
         * Define el valor de la propiedad participantID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParticipantID(String value) {
            this.participantID = value;
        }

        /**
         * Obtiene el valor de la propiedad gender.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGender() {
            return gender;
        }

        /**
         * Define el valor de la propiedad gender.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGender(String value) {
            this.gender = value;
        }

        /**
         * Obtiene el valor de la propiedad nationality.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNationality() {
            return nationality;
        }

        /**
         * Define el valor de la propiedad nationality.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNationality(String value) {
            this.nationality = value;
        }

        /**
         * Obtiene el valor de la propiedad leadCustomerInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isLeadCustomerInd() {
            return leadCustomerInd;
        }

        /**
         * Define el valor de la propiedad leadCustomerInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setLeadCustomerInd(Boolean value) {
            this.leadCustomerInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class SpecialNeed {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "Code")
            protected String code;

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

        }

    }

}
