
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferTravelSegment.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferTravelSegment"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="LeisureAndActivities"/&amp;gt;
 *     &amp;lt;enumeration value="Lodging"/&amp;gt;
 *     &amp;lt;enumeration value="Transportation"/&amp;gt;
 *     &amp;lt;enumeration value="TravelServices"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferTravelSegment")
@XmlEnum
public enum ListOfferTravelSegment {

    @XmlEnumValue("LeisureAndActivities")
    LEISURE_AND_ACTIVITIES("LeisureAndActivities"),
    @XmlEnumValue("Lodging")
    LODGING("Lodging"),
    @XmlEnumValue("Transportation")
    TRANSPORTATION("Transportation"),
    @XmlEnumValue("TravelServices")
    TRAVEL_SERVICES("TravelServices");
    private final String value;

    ListOfferTravelSegment(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferTravelSegment fromValue(String v) {
        for (ListOfferTravelSegment c: ListOfferTravelSegment.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
