
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides vendor with information about the traveler(s)' requirements for insurance coverage.
 * 
 * &lt;p&gt;Clase Java para InsCoverageDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="InsCoverageDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CoverageRequirements" type="{http://www.opentravel.org/OTA/2003/05}RequestedCoveragesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TotalTripQuantity" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MaximumTripLength" type="{http://www.opentravel.org/OTA/2003/05}TimeDurationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TotalTripCost" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CoveredTrips" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CoveredTrip" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                           &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DeliveryPref" type="{http://www.opentravel.org/OTA/2003/05}DistribPrefType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}EffectiveExpireOptionalDateGroup"/&amp;gt;
 *       &amp;lt;attribute name="Type"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="SingleTrip"/&amp;gt;
 *             &amp;lt;enumeration value="MultiTrip"/&amp;gt;
 *             &amp;lt;enumeration value="Other"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="AutoRenew" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsCoverageDetailType", propOrder = {
    "coverageRequirements",
    "totalTripQuantity",
    "maximumTripLength",
    "totalTripCost",
    "coveredTrips",
    "deliveryPref"
})
public class InsCoverageDetailType {

    @XmlElement(name = "CoverageRequirements")
    protected RequestedCoveragesType coverageRequirements;
    @XmlElement(name = "TotalTripQuantity")
    protected InsCoverageDetailType.TotalTripQuantity totalTripQuantity;
    @XmlElement(name = "MaximumTripLength")
    protected TimeDurationType maximumTripLength;
    @XmlElement(name = "TotalTripCost")
    protected InsCoverageDetailType.TotalTripCost totalTripCost;
    @XmlElement(name = "CoveredTrips")
    protected InsCoverageDetailType.CoveredTrips coveredTrips;
    @XmlElement(name = "DeliveryPref")
    protected List<DistribPrefType> deliveryPref;
    @XmlAttribute(name = "Type")
    protected String type;
    @XmlAttribute(name = "AutoRenew")
    protected Boolean autoRenew;
    @XmlAttribute(name = "EffectiveDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDate;
    @XmlAttribute(name = "ExpireDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expireDate;
    @XmlAttribute(name = "ExpireDateExclusiveInd")
    protected Boolean expireDateExclusiveInd;

    /**
     * Obtiene el valor de la propiedad coverageRequirements.
     * 
     * @return
     *     possible object is
     *     {@link RequestedCoveragesType }
     *     
     */
    public RequestedCoveragesType getCoverageRequirements() {
        return coverageRequirements;
    }

    /**
     * Define el valor de la propiedad coverageRequirements.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestedCoveragesType }
     *     
     */
    public void setCoverageRequirements(RequestedCoveragesType value) {
        this.coverageRequirements = value;
    }

    /**
     * Obtiene el valor de la propiedad totalTripQuantity.
     * 
     * @return
     *     possible object is
     *     {@link InsCoverageDetailType.TotalTripQuantity }
     *     
     */
    public InsCoverageDetailType.TotalTripQuantity getTotalTripQuantity() {
        return totalTripQuantity;
    }

    /**
     * Define el valor de la propiedad totalTripQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link InsCoverageDetailType.TotalTripQuantity }
     *     
     */
    public void setTotalTripQuantity(InsCoverageDetailType.TotalTripQuantity value) {
        this.totalTripQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumTripLength.
     * 
     * @return
     *     possible object is
     *     {@link TimeDurationType }
     *     
     */
    public TimeDurationType getMaximumTripLength() {
        return maximumTripLength;
    }

    /**
     * Define el valor de la propiedad maximumTripLength.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeDurationType }
     *     
     */
    public void setMaximumTripLength(TimeDurationType value) {
        this.maximumTripLength = value;
    }

    /**
     * Obtiene el valor de la propiedad totalTripCost.
     * 
     * @return
     *     possible object is
     *     {@link InsCoverageDetailType.TotalTripCost }
     *     
     */
    public InsCoverageDetailType.TotalTripCost getTotalTripCost() {
        return totalTripCost;
    }

    /**
     * Define el valor de la propiedad totalTripCost.
     * 
     * @param value
     *     allowed object is
     *     {@link InsCoverageDetailType.TotalTripCost }
     *     
     */
    public void setTotalTripCost(InsCoverageDetailType.TotalTripCost value) {
        this.totalTripCost = value;
    }

    /**
     * Obtiene el valor de la propiedad coveredTrips.
     * 
     * @return
     *     possible object is
     *     {@link InsCoverageDetailType.CoveredTrips }
     *     
     */
    public InsCoverageDetailType.CoveredTrips getCoveredTrips() {
        return coveredTrips;
    }

    /**
     * Define el valor de la propiedad coveredTrips.
     * 
     * @param value
     *     allowed object is
     *     {@link InsCoverageDetailType.CoveredTrips }
     *     
     */
    public void setCoveredTrips(InsCoverageDetailType.CoveredTrips value) {
        this.coveredTrips = value;
    }

    /**
     * Gets the value of the deliveryPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the deliveryPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDeliveryPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link DistribPrefType }
     * 
     * 
     */
    public List<DistribPrefType> getDeliveryPref() {
        if (deliveryPref == null) {
            deliveryPref = new ArrayList<DistribPrefType>();
        }
        return this.deliveryPref;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad autoRenew.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoRenew() {
        return autoRenew;
    }

    /**
     * Define el valor de la propiedad autoRenew.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoRenew(Boolean value) {
        this.autoRenew = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define el valor de la propiedad effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expireDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpireDate() {
        return expireDate;
    }

    /**
     * Define el valor de la propiedad expireDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpireDate(XMLGregorianCalendar value) {
        this.expireDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expireDateExclusiveInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpireDateExclusiveInd() {
        return expireDateExclusiveInd;
    }

    /**
     * Define el valor de la propiedad expireDateExclusiveInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpireDateExclusiveInd(Boolean value) {
        this.expireDateExclusiveInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CoveredTrip" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "coveredTrip"
    })
    public static class CoveredTrips {

        @XmlElement(name = "CoveredTrip", required = true)
        protected List<InsCoverageDetailType.CoveredTrips.CoveredTrip> coveredTrip;

        /**
         * Gets the value of the coveredTrip property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coveredTrip property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCoveredTrip().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link InsCoverageDetailType.CoveredTrips.CoveredTrip }
         * 
         * 
         */
        public List<InsCoverageDetailType.CoveredTrips.CoveredTrip> getCoveredTrip() {
            if (coveredTrip == null) {
                coveredTrip = new ArrayList<InsCoverageDetailType.CoveredTrips.CoveredTrip>();
            }
            return this.coveredTrip;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CoveredTrip
            extends TripFeaturesType
        {

            @XmlAttribute(name = "DepositDate")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar depositDate;
            @XmlAttribute(name = "FinalPayDate")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar finalPayDate;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad depositDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDepositDate() {
                return depositDate;
            }

            /**
             * Define el valor de la propiedad depositDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDepositDate(XMLGregorianCalendar value) {
                this.depositDate = value;
            }

            /**
             * Obtiene el valor de la propiedad finalPayDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFinalPayDate() {
                return finalPayDate;
            }

            /**
             * Define el valor de la propiedad finalPayDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFinalPayDate(XMLGregorianCalendar value) {
                this.finalPayDate = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TotalTripCost {

        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TotalTripQuantity {

        @XmlAttribute(name = "Quantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger quantity;

        /**
         * Obtiene el valor de la propiedad quantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getQuantity() {
            return quantity;
        }

        /**
         * Define el valor de la propiedad quantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setQuantity(BigInteger value) {
            this.quantity = value;
        }

    }

}
