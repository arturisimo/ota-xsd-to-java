
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *             &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="ProcessingInformation" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                     &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                     &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                     &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                     &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element name="TourActivityInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="BasicInfo" type="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"/&amp;gt;
 *                       &amp;lt;element name="Schedule"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                         &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" maxOccurs="365" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="Accessibility" type="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityType" minOccurs="0"/&amp;gt;
 *                       &amp;lt;element name="CategoryAndType" type="{http://www.opentravel.org/OTA/2003/05}TourActivityCategoryType" minOccurs="0"/&amp;gt;
 *                       &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}TourActivityDescriptionType" minOccurs="0"/&amp;gt;
 *                       &amp;lt;element name="Discount" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="ProgramDetail" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                         &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                         &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                         &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;simpleContent&amp;gt;
 *                                       &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                         &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                         &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                         &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                                         &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/simpleContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                               &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                               &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                               &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                               &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                               &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="Insurance" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                             &amp;lt;complexType&amp;gt;
 *                                               &amp;lt;simpleContent&amp;gt;
 *                                                 &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                                   &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;/extension&amp;gt;
 *                                               &amp;lt;/simpleContent&amp;gt;
 *                                             &amp;lt;/complexType&amp;gt;
 *                                           &amp;lt;/element&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                 &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="OwnInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                               &amp;lt;attribute name="SupplierInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="LanguageSpoken" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLanguageType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                       &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
 *                       &amp;lt;element name="PickupDropoff" maxOccurs="2" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" minOccurs="0"/&amp;gt;
 *                                 &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                               &amp;lt;attribute name="Directions" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                               &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                               &amp;lt;attribute name="DropoffInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="Policies" type="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType" minOccurs="0"/&amp;gt;
 *                       &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;simpleContent&amp;gt;
 *                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                         &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/simpleContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="MinPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                               &amp;lt;attribute name="MaxPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                               &amp;lt;attribute name="TaxInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *                               &amp;lt;attribute name="GuaranteedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                     &amp;lt;attribute name="AlternateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *           &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "success",
    "warnings",
    "processingInformation",
    "tourActivityInfo",
    "errors"
})
@XmlRootElement(name = "OTA_TourActivitySearchRS")
public class OTATourActivitySearchRS {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "ProcessingInformation")
    protected OTATourActivitySearchRS.ProcessingInformation processingInformation;
    @XmlElement(name = "TourActivityInfo")
    protected List<OTATourActivitySearchRS.TourActivityInfo> tourActivityInfo;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInformation.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivitySearchRS.ProcessingInformation }
     *     
     */
    public OTATourActivitySearchRS.ProcessingInformation getProcessingInformation() {
        return processingInformation;
    }

    /**
     * Define el valor de la propiedad processingInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivitySearchRS.ProcessingInformation }
     *     
     */
    public void setProcessingInformation(OTATourActivitySearchRS.ProcessingInformation value) {
        this.processingInformation = value;
    }

    /**
     * Gets the value of the tourActivityInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the tourActivityInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTourActivityInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTATourActivitySearchRS.TourActivityInfo }
     * 
     * 
     */
    public List<OTATourActivitySearchRS.TourActivityInfo> getTourActivityInfo() {
        if (tourActivityInfo == null) {
            tourActivityInfo = new ArrayList<OTATourActivitySearchRS.TourActivityInfo>();
        }
        return this.tourActivityInfo;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *       &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInformation {

        @XmlAttribute(name = "MoreDataEchoToken")
        protected String moreDataEchoToken;
        @XmlAttribute(name = "LanguageCode")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String languageCode;
        @XmlAttribute(name = "DisplayCurrency")
        protected String displayCurrency;
        @XmlAttribute(name = "PricingCurrency")
        protected String pricingCurrency;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;

        /**
         * Obtiene el valor de la propiedad moreDataEchoToken.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoreDataEchoToken() {
            return moreDataEchoToken;
        }

        /**
         * Define el valor de la propiedad moreDataEchoToken.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoreDataEchoToken(String value) {
            this.moreDataEchoToken = value;
        }

        /**
         * Obtiene el valor de la propiedad languageCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageCode() {
            return languageCode;
        }

        /**
         * Define el valor de la propiedad languageCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageCode(String value) {
            this.languageCode = value;
        }

        /**
         * Obtiene el valor de la propiedad displayCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayCurrency() {
            return displayCurrency;
        }

        /**
         * Define el valor de la propiedad displayCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayCurrency(String value) {
            this.displayCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad pricingCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricingCurrency() {
            return pricingCurrency;
        }

        /**
         * Define el valor de la propiedad pricingCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricingCurrency(String value) {
            this.pricingCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BasicInfo" type="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"/&amp;gt;
     *         &amp;lt;element name="Schedule"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" maxOccurs="365" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Accessibility" type="{http://www.opentravel.org/OTA/2003/05}TourActivityAccessibilityType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CategoryAndType" type="{http://www.opentravel.org/OTA/2003/05}TourActivityCategoryType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}TourActivityDescriptionType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Discount" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ProgramDetail" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Insurance" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                                     &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="OwnInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="SupplierInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="LanguageSpoken" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLanguageType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PickupDropoff" maxOccurs="2" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Directions" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="DropoffInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Policies" type="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="MinPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="MaxPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TaxInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
     *                 &amp;lt;attribute name="GuaranteedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="AlternateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "basicInfo",
        "schedule",
        "accessibility",
        "categoryAndType",
        "description",
        "discount",
        "extra",
        "insurance",
        "languageSpoken",
        "location",
        "pickupDropoff",
        "policies",
        "pricing",
        "supplierOperator"
    })
    public static class TourActivityInfo {

        @XmlElement(name = "BasicInfo", required = true)
        protected TourActivityIDType basicInfo;
        @XmlElement(name = "Schedule", required = true)
        protected OTATourActivitySearchRS.TourActivityInfo.Schedule schedule;
        @XmlElement(name = "Accessibility")
        protected TourActivityAccessibilityType accessibility;
        @XmlElement(name = "CategoryAndType")
        protected TourActivityCategoryType categoryAndType;
        @XmlElement(name = "Description")
        protected TourActivityDescriptionType description;
        @XmlElement(name = "Discount")
        protected OTATourActivitySearchRS.TourActivityInfo.Discount discount;
        @XmlElement(name = "Extra")
        protected List<OTATourActivitySearchRS.TourActivityInfo.Extra> extra;
        @XmlElement(name = "Insurance")
        protected List<OTATourActivitySearchRS.TourActivityInfo.Insurance> insurance;
        @XmlElement(name = "LanguageSpoken")
        protected List<TourActivityLanguageType> languageSpoken;
        @XmlElement(name = "Location")
        protected TourActivityLocationType location;
        @XmlElement(name = "PickupDropoff")
        protected List<OTATourActivitySearchRS.TourActivityInfo.PickupDropoff> pickupDropoff;
        @XmlElement(name = "Policies")
        protected TourActivityPolicyType policies;
        @XmlElement(name = "Pricing")
        protected OTATourActivitySearchRS.TourActivityInfo.Pricing pricing;
        @XmlElement(name = "SupplierOperator")
        protected List<TourActivitySupplierType> supplierOperator;
        @XmlAttribute(name = "AlternateInd")
        protected Boolean alternateInd;

        /**
         * Obtiene el valor de la propiedad basicInfo.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityIDType }
         *     
         */
        public TourActivityIDType getBasicInfo() {
            return basicInfo;
        }

        /**
         * Define el valor de la propiedad basicInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityIDType }
         *     
         */
        public void setBasicInfo(TourActivityIDType value) {
            this.basicInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad schedule.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivitySearchRS.TourActivityInfo.Schedule }
         *     
         */
        public OTATourActivitySearchRS.TourActivityInfo.Schedule getSchedule() {
            return schedule;
        }

        /**
         * Define el valor de la propiedad schedule.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivitySearchRS.TourActivityInfo.Schedule }
         *     
         */
        public void setSchedule(OTATourActivitySearchRS.TourActivityInfo.Schedule value) {
            this.schedule = value;
        }

        /**
         * Obtiene el valor de la propiedad accessibility.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityAccessibilityType }
         *     
         */
        public TourActivityAccessibilityType getAccessibility() {
            return accessibility;
        }

        /**
         * Define el valor de la propiedad accessibility.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityAccessibilityType }
         *     
         */
        public void setAccessibility(TourActivityAccessibilityType value) {
            this.accessibility = value;
        }

        /**
         * Obtiene el valor de la propiedad categoryAndType.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityCategoryType }
         *     
         */
        public TourActivityCategoryType getCategoryAndType() {
            return categoryAndType;
        }

        /**
         * Define el valor de la propiedad categoryAndType.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityCategoryType }
         *     
         */
        public void setCategoryAndType(TourActivityCategoryType value) {
            this.categoryAndType = value;
        }

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityDescriptionType }
         *     
         */
        public TourActivityDescriptionType getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityDescriptionType }
         *     
         */
        public void setDescription(TourActivityDescriptionType value) {
            this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad discount.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivitySearchRS.TourActivityInfo.Discount }
         *     
         */
        public OTATourActivitySearchRS.TourActivityInfo.Discount getDiscount() {
            return discount;
        }

        /**
         * Define el valor de la propiedad discount.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivitySearchRS.TourActivityInfo.Discount }
         *     
         */
        public void setDiscount(OTATourActivitySearchRS.TourActivityInfo.Discount value) {
            this.discount = value;
        }

        /**
         * Gets the value of the extra property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the extra property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getExtra().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivitySearchRS.TourActivityInfo.Extra }
         * 
         * 
         */
        public List<OTATourActivitySearchRS.TourActivityInfo.Extra> getExtra() {
            if (extra == null) {
                extra = new ArrayList<OTATourActivitySearchRS.TourActivityInfo.Extra>();
            }
            return this.extra;
        }

        /**
         * Gets the value of the insurance property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the insurance property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getInsurance().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivitySearchRS.TourActivityInfo.Insurance }
         * 
         * 
         */
        public List<OTATourActivitySearchRS.TourActivityInfo.Insurance> getInsurance() {
            if (insurance == null) {
                insurance = new ArrayList<OTATourActivitySearchRS.TourActivityInfo.Insurance>();
            }
            return this.insurance;
        }

        /**
         * Gets the value of the languageSpoken property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the languageSpoken property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLanguageSpoken().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityLanguageType }
         * 
         * 
         */
        public List<TourActivityLanguageType> getLanguageSpoken() {
            if (languageSpoken == null) {
                languageSpoken = new ArrayList<TourActivityLanguageType>();
            }
            return this.languageSpoken;
        }

        /**
         * Obtiene el valor de la propiedad location.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityLocationType }
         *     
         */
        public TourActivityLocationType getLocation() {
            return location;
        }

        /**
         * Define el valor de la propiedad location.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityLocationType }
         *     
         */
        public void setLocation(TourActivityLocationType value) {
            this.location = value;
        }

        /**
         * Gets the value of the pickupDropoff property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pickupDropoff property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPickupDropoff().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTATourActivitySearchRS.TourActivityInfo.PickupDropoff }
         * 
         * 
         */
        public List<OTATourActivitySearchRS.TourActivityInfo.PickupDropoff> getPickupDropoff() {
            if (pickupDropoff == null) {
                pickupDropoff = new ArrayList<OTATourActivitySearchRS.TourActivityInfo.PickupDropoff>();
            }
            return this.pickupDropoff;
        }

        /**
         * Obtiene el valor de la propiedad policies.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityPolicyType }
         *     
         */
        public TourActivityPolicyType getPolicies() {
            return policies;
        }

        /**
         * Define el valor de la propiedad policies.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityPolicyType }
         *     
         */
        public void setPolicies(TourActivityPolicyType value) {
            this.policies = value;
        }

        /**
         * Obtiene el valor de la propiedad pricing.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivitySearchRS.TourActivityInfo.Pricing }
         *     
         */
        public OTATourActivitySearchRS.TourActivityInfo.Pricing getPricing() {
            return pricing;
        }

        /**
         * Define el valor de la propiedad pricing.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivitySearchRS.TourActivityInfo.Pricing }
         *     
         */
        public void setPricing(OTATourActivitySearchRS.TourActivityInfo.Pricing value) {
            this.pricing = value;
        }

        /**
         * Gets the value of the supplierOperator property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplierOperator property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSupplierOperator().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivitySupplierType }
         * 
         * 
         */
        public List<TourActivitySupplierType> getSupplierOperator() {
            if (supplierOperator == null) {
                supplierOperator = new ArrayList<TourActivitySupplierType>();
            }
            return this.supplierOperator;
        }

        /**
         * Obtiene el valor de la propiedad alternateInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAlternateInd() {
            return alternateInd;
        }

        /**
         * Define el valor de la propiedad alternateInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAlternateInd(Boolean value) {
            this.alternateInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ProgramDetail" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "programDetail",
            "promotion"
        })
        public static class Discount {

            @XmlElement(name = "ProgramDetail")
            protected OTATourActivitySearchRS.TourActivityInfo.Discount.ProgramDetail programDetail;
            @XmlElement(name = "Promotion")
            protected List<OTATourActivitySearchRS.TourActivityInfo.Discount.Promotion> promotion;

            /**
             * Obtiene el valor de la propiedad programDetail.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Discount.ProgramDetail }
             *     
             */
            public OTATourActivitySearchRS.TourActivityInfo.Discount.ProgramDetail getProgramDetail() {
                return programDetail;
            }

            /**
             * Define el valor de la propiedad programDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Discount.ProgramDetail }
             *     
             */
            public void setProgramDetail(OTATourActivitySearchRS.TourActivityInfo.Discount.ProgramDetail value) {
                this.programDetail = value;
            }

            /**
             * Gets the value of the promotion property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotion property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotion().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivitySearchRS.TourActivityInfo.Discount.Promotion }
             * 
             * 
             */
            public List<OTATourActivitySearchRS.TourActivityInfo.Discount.Promotion> getPromotion() {
                if (promotion == null) {
                    promotion = new ArrayList<OTATourActivitySearchRS.TourActivityInfo.Discount.Promotion>();
                }
                return this.promotion;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "providerName"
            })
            public static class ProgramDetail {

                @XmlElement(name = "ProviderName")
                protected CompanyNameType providerName;
                @XmlAttribute(name = "ProgramID")
                protected String programID;
                @XmlAttribute(name = "Name")
                protected String name;
                @XmlAttribute(name = "Description")
                protected String description;

                /**
                 * Obtiene el valor de la propiedad providerName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public CompanyNameType getProviderName() {
                    return providerName;
                }

                /**
                 * Define el valor de la propiedad providerName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public void setProviderName(CompanyNameType value) {
                    this.providerName = value;
                }

                /**
                 * Obtiene el valor de la propiedad programID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramID() {
                    return programID;
                }

                /**
                 * Define el valor de la propiedad programID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramID(String value) {
                    this.programID = value;
                }

                /**
                 * Obtiene el valor de la propiedad name.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Define el valor de la propiedad name.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Promotion {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "StartDate")
                protected String startDate;
                @XmlAttribute(name = "EndDate")
                protected String endDate;
                @XmlAttribute(name = "Disclaimer")
                protected String disclaimer;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad startDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStartDate() {
                    return startDate;
                }

                /**
                 * Define el valor de la propiedad startDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStartDate(String value) {
                    this.startDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad endDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndDate() {
                    return endDate;
                }

                /**
                 * Define el valor de la propiedad endDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndDate(String value) {
                    this.endDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad disclaimer.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDisclaimer() {
                    return disclaimer;
                }

                /**
                 * Define el valor de la propiedad disclaimer.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDisclaimer(String value) {
                    this.disclaimer = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "deposit"
        })
        public static class Extra {

            @XmlElement(name = "Deposit")
            protected OTATourActivitySearchRS.TourActivityInfo.Extra.Deposit deposit;
            @XmlAttribute(name = "SupplierCode")
            protected String supplierCode;
            @XmlAttribute(name = "OtherCode")
            protected String otherCode;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "RequiredInd")
            protected Boolean requiredInd;
            @XmlAttribute(name = "ReserveInd")
            protected Boolean reserveInd;

            /**
             * Obtiene el valor de la propiedad deposit.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Extra.Deposit }
             *     
             */
            public OTATourActivitySearchRS.TourActivityInfo.Extra.Deposit getDeposit() {
                return deposit;
            }

            /**
             * Define el valor de la propiedad deposit.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Extra.Deposit }
             *     
             */
            public void setDeposit(OTATourActivitySearchRS.TourActivityInfo.Extra.Deposit value) {
                this.deposit = value;
            }

            /**
             * Obtiene el valor de la propiedad supplierCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSupplierCode() {
                return supplierCode;
            }

            /**
             * Define el valor de la propiedad supplierCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSupplierCode(String value) {
                this.supplierCode = value;
            }

            /**
             * Obtiene el valor de la propiedad otherCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherCode() {
                return otherCode;
            }

            /**
             * Define el valor de la propiedad otherCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherCode(String value) {
                this.otherCode = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad requiredInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRequiredInd() {
                return requiredInd;
            }

            /**
             * Define el valor de la propiedad requiredInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRequiredInd(Boolean value) {
                this.requiredInd = value;
            }

            /**
             * Obtiene el valor de la propiedad reserveInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReserveInd() {
                return reserveInd;
            }

            /**
             * Define el valor de la propiedad reserveInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReserveInd(Boolean value) {
                this.reserveInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Amount" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount"
            })
            public static class Deposit
                extends AcceptedPaymentsType
            {

                @XmlElement(name = "Amount")
                protected TourActivityChargeType amount;

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setAmount(TourActivityChargeType value) {
                    this.amount = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ProviderName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="OwnInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="SupplierInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pricing",
            "providerName",
            "coverageLimit"
        })
        public static class Insurance {

            @XmlElement(name = "Pricing")
            protected OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing pricing;
            @XmlElement(name = "ProviderName")
            protected CompanyNameType providerName;
            @XmlElement(name = "CoverageLimit")
            protected CoverageLimitType coverageLimit;
            @XmlAttribute(name = "OwnInsuranceInd")
            protected Boolean ownInsuranceInd;
            @XmlAttribute(name = "SupplierInd")
            protected Boolean supplierInd;

            /**
             * Obtiene el valor de la propiedad pricing.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing }
             *     
             */
            public OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing getPricing() {
                return pricing;
            }

            /**
             * Define el valor de la propiedad pricing.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing }
             *     
             */
            public void setPricing(OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing value) {
                this.pricing = value;
            }

            /**
             * Obtiene el valor de la propiedad providerName.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getProviderName() {
                return providerName;
            }

            /**
             * Define el valor de la propiedad providerName.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setProviderName(CompanyNameType value) {
                this.providerName = value;
            }

            /**
             * Obtiene el valor de la propiedad coverageLimit.
             * 
             * @return
             *     possible object is
             *     {@link CoverageLimitType }
             *     
             */
            public CoverageLimitType getCoverageLimit() {
                return coverageLimit;
            }

            /**
             * Define el valor de la propiedad coverageLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link CoverageLimitType }
             *     
             */
            public void setCoverageLimit(CoverageLimitType value) {
                this.coverageLimit = value;
            }

            /**
             * Obtiene el valor de la propiedad ownInsuranceInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isOwnInsuranceInd() {
                return ownInsuranceInd;
            }

            /**
             * Define el valor de la propiedad ownInsuranceInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setOwnInsuranceInd(Boolean value) {
                this.ownInsuranceInd = value;
            }

            /**
             * Obtiene el valor de la propiedad supplierInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSupplierInd() {
                return supplierInd;
            }

            /**
             * Define el valor de la propiedad supplierInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSupplierInd(Boolean value) {
                this.supplierInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pricingType"
            })
            public static class Pricing
                extends TourActivityChargeType
            {

                @XmlElement(name = "PricingType")
                protected OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing.PricingType pricingType;

                /**
                 * Obtiene el valor de la propiedad pricingType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing.PricingType }
                 *     
                 */
                public OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing.PricingType getPricingType() {
                    return pricingType;
                }

                /**
                 * Define el valor de la propiedad pricingType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing.PricingType }
                 *     
                 */
                public void setPricingType(OTATourActivitySearchRS.TourActivityInfo.Insurance.Pricing.PricingType value) {
                    this.pricingType = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                 *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class PricingType {

                    @XmlValue
                    protected TourActivityPricingTypeEnum value;
                    @XmlAttribute(name = "Extension")
                    protected String extension;

                    /**
                     * Tour and activity pricing options.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public TourActivityPricingTypeEnum getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public void setValue(TourActivityPricingTypeEnum value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad extension.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExtension() {
                        return extension;
                    }

                    /**
                     * Define el valor de la propiedad extension.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExtension(String value) {
                        this.extension = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Directions" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="PickupInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="DropoffInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "schedule",
            "price"
        })
        public static class PickupDropoff {

            @XmlElement(name = "Schedule")
            protected OperationScheduleType schedule;
            @XmlElement(name = "Price")
            protected TourActivityChargeType price;
            @XmlAttribute(name = "LocationName")
            protected String locationName;
            @XmlAttribute(name = "Directions")
            protected String directions;
            @XmlAttribute(name = "PickupInd")
            protected Boolean pickupInd;
            @XmlAttribute(name = "DropoffInd")
            protected Boolean dropoffInd;

            /**
             * Obtiene el valor de la propiedad schedule.
             * 
             * @return
             *     possible object is
             *     {@link OperationScheduleType }
             *     
             */
            public OperationScheduleType getSchedule() {
                return schedule;
            }

            /**
             * Define el valor de la propiedad schedule.
             * 
             * @param value
             *     allowed object is
             *     {@link OperationScheduleType }
             *     
             */
            public void setSchedule(OperationScheduleType value) {
                this.schedule = value;
            }

            /**
             * Obtiene el valor de la propiedad price.
             * 
             * @return
             *     possible object is
             *     {@link TourActivityChargeType }
             *     
             */
            public TourActivityChargeType getPrice() {
                return price;
            }

            /**
             * Define el valor de la propiedad price.
             * 
             * @param value
             *     allowed object is
             *     {@link TourActivityChargeType }
             *     
             */
            public void setPrice(TourActivityChargeType value) {
                this.price = value;
            }

            /**
             * Obtiene el valor de la propiedad locationName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationName() {
                return locationName;
            }

            /**
             * Define el valor de la propiedad locationName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationName(String value) {
                this.locationName = value;
            }

            /**
             * Obtiene el valor de la propiedad directions.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDirections() {
                return directions;
            }

            /**
             * Define el valor de la propiedad directions.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDirections(String value) {
                this.directions = value;
            }

            /**
             * Obtiene el valor de la propiedad pickupInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPickupInd() {
                return pickupInd;
            }

            /**
             * Define el valor de la propiedad pickupInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPickupInd(Boolean value) {
                this.pickupInd = value;
            }

            /**
             * Obtiene el valor de la propiedad dropoffInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDropoffInd() {
                return dropoffInd;
            }

            /**
             * Define el valor de la propiedad dropoffInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDropoffInd(Boolean value) {
                this.dropoffInd = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="MinPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="MaxPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TaxInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
         *       &amp;lt;attribute name="GuaranteedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pricingType"
        })
        public static class Pricing {

            @XmlElement(name = "PricingType")
            protected OTATourActivitySearchRS.TourActivityInfo.Pricing.PricingType pricingType;
            @XmlAttribute(name = "MinPrice")
            protected BigDecimal minPrice;
            @XmlAttribute(name = "MaxPrice")
            protected BigDecimal maxPrice;
            @XmlAttribute(name = "TaxInd")
            @XmlSchemaType(name = "anySimpleType")
            protected String taxInd;
            @XmlAttribute(name = "GuaranteedInd")
            protected Boolean guaranteedInd;

            /**
             * Obtiene el valor de la propiedad pricingType.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Pricing.PricingType }
             *     
             */
            public OTATourActivitySearchRS.TourActivityInfo.Pricing.PricingType getPricingType() {
                return pricingType;
            }

            /**
             * Define el valor de la propiedad pricingType.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Pricing.PricingType }
             *     
             */
            public void setPricingType(OTATourActivitySearchRS.TourActivityInfo.Pricing.PricingType value) {
                this.pricingType = value;
            }

            /**
             * Obtiene el valor de la propiedad minPrice.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getMinPrice() {
                return minPrice;
            }

            /**
             * Define el valor de la propiedad minPrice.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setMinPrice(BigDecimal value) {
                this.minPrice = value;
            }

            /**
             * Obtiene el valor de la propiedad maxPrice.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getMaxPrice() {
                return maxPrice;
            }

            /**
             * Define el valor de la propiedad maxPrice.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setMaxPrice(BigDecimal value) {
                this.maxPrice = value;
            }

            /**
             * Obtiene el valor de la propiedad taxInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxInd() {
                return taxInd;
            }

            /**
             * Define el valor de la propiedad taxInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxInd(String value) {
                this.taxInd = value;
            }

            /**
             * Obtiene el valor de la propiedad guaranteedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isGuaranteedInd() {
                return guaranteedInd;
            }

            /**
             * Define el valor de la propiedad guaranteedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setGuaranteedInd(Boolean value) {
                this.guaranteedInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class PricingType {

                @XmlValue
                protected TourActivityPricingTypeEnum value;
                @XmlAttribute(name = "Extension")
                protected String extension;

                /**
                 * Tour and activity pricing options.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityPricingTypeEnum }
                 *     
                 */
                public TourActivityPricingTypeEnum getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityPricingTypeEnum }
                 *     
                 */
                public void setValue(TourActivityPricingTypeEnum value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad extension.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExtension() {
                    return extension;
                }

                /**
                 * Define el valor de la propiedad extension.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExtension(String value) {
                    this.extension = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}OperationScheduleType" maxOccurs="365" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "summary",
            "detail"
        })
        public static class Schedule {

            @XmlElement(name = "Summary")
            protected OTATourActivitySearchRS.TourActivityInfo.Schedule.Summary summary;
            @XmlElement(name = "Detail")
            protected List<OperationScheduleType> detail;

            /**
             * Obtiene el valor de la propiedad summary.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Schedule.Summary }
             *     
             */
            public OTATourActivitySearchRS.TourActivityInfo.Schedule.Summary getSummary() {
                return summary;
            }

            /**
             * Define el valor de la propiedad summary.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivitySearchRS.TourActivityInfo.Schedule.Summary }
             *     
             */
            public void setSummary(OTATourActivitySearchRS.TourActivityInfo.Schedule.Summary value) {
                this.summary = value;
            }

            /**
             * Gets the value of the detail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OperationScheduleType }
             * 
             * 
             */
            public List<OperationScheduleType> getDetail() {
                if (detail == null) {
                    detail = new ArrayList<OperationScheduleType>();
                }
                return this.detail;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Summary {

                @XmlAttribute(name = "Description")
                protected String description;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }

    }

}
