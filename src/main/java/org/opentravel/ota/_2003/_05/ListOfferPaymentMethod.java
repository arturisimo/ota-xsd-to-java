
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPaymentMethod.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPaymentMethod"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="BusinessCheck"/&amp;gt;
 *     &amp;lt;enumeration value="Cash"/&amp;gt;
 *     &amp;lt;enumeration value="Check"/&amp;gt;
 *     &amp;lt;enumeration value="CorporateAccount"/&amp;gt;
 *     &amp;lt;enumeration value="Coupon"/&amp;gt;
 *     &amp;lt;enumeration value="CreditCard"/&amp;gt;
 *     &amp;lt;enumeration value="DebitCard"/&amp;gt;
 *     &amp;lt;enumeration value="DirectBill"/&amp;gt;
 *     &amp;lt;enumeration value="InternetAccount"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyPaymentCard"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyRedemption"/&amp;gt;
 *     &amp;lt;enumeration value="MobilePayment"/&amp;gt;
 *     &amp;lt;enumeration value="Ticket"/&amp;gt;
 *     &amp;lt;enumeration value="Voucher"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPaymentMethod")
@XmlEnum
public enum ListOfferPaymentMethod {

    @XmlEnumValue("BusinessCheck")
    BUSINESS_CHECK("BusinessCheck"),
    @XmlEnumValue("Cash")
    CASH("Cash"),
    @XmlEnumValue("Check")
    CHECK("Check"),
    @XmlEnumValue("CorporateAccount")
    CORPORATE_ACCOUNT("CorporateAccount"),
    @XmlEnumValue("Coupon")
    COUPON("Coupon"),
    @XmlEnumValue("CreditCard")
    CREDIT_CARD("CreditCard"),
    @XmlEnumValue("DebitCard")
    DEBIT_CARD("DebitCard"),
    @XmlEnumValue("DirectBill")
    DIRECT_BILL("DirectBill"),
    @XmlEnumValue("InternetAccount")
    INTERNET_ACCOUNT("InternetAccount"),
    @XmlEnumValue("LoyaltyPaymentCard")
    LOYALTY_PAYMENT_CARD("LoyaltyPaymentCard"),
    @XmlEnumValue("LoyaltyRedemption")
    LOYALTY_REDEMPTION("LoyaltyRedemption"),
    @XmlEnumValue("MobilePayment")
    MOBILE_PAYMENT("MobilePayment"),
    @XmlEnumValue("Ticket")
    TICKET("Ticket"),
    @XmlEnumValue("Voucher")
    VOUCHER("Voucher");
    private final String value;

    ListOfferPaymentMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPaymentMethod fromValue(String v) {
        for (ListOfferPaymentMethod c: ListOfferPaymentMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
