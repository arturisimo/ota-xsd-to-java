
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Seat marketing classification data for a section of seats within a specified aircraft equipment type.
 * 
 * &lt;p&gt;Clase Java para AirSeatMarketingClassType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirSeatMarketingClassType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CabinInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SubType" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="SeatType" type="{http://www.opentravel.org/OTA/2003/05}AirSeatPreferenceType" /&amp;gt;
 *                           &amp;lt;attribute name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="ReqSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                 &amp;lt;attribute name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *                 &amp;lt;attribute name="SeatType" type="{http://www.opentravel.org/OTA/2003/05}AirSeatPreferenceType" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ItinerarySegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="AirEquipType"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;union memberTypes=" {http://www.opentravel.org/OTA/2003/05}StringLength3 {http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *           &amp;lt;/union&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="PurchasedSeatRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirSeatMarketingClassType", propOrder = {
    "cabinInfo",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirGetOfferRQ.RequestCriterion.SeatInfo.DetailedInfo.class,
    org.opentravel.ota._2003._05.AirPricedOfferType.SeatInfo.class
})
public class AirSeatMarketingClassType {

    @XmlElement(name = "CabinInfo")
    protected List<AirSeatMarketingClassType.CabinInfo> cabinInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "AirEquipType")
    protected String airEquipType;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "PurchasedSeatRPH")
    protected String purchasedSeatRPH;

    /**
     * Gets the value of the cabinInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cabinInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCabinInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirSeatMarketingClassType.CabinInfo }
     * 
     * 
     */
    public List<AirSeatMarketingClassType.CabinInfo> getCabinInfo() {
        if (cabinInfo == null) {
            cabinInfo = new ArrayList<AirSeatMarketingClassType.CabinInfo>();
        }
        return this.cabinInfo;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad airEquipType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirEquipType() {
        return airEquipType;
    }

    /**
     * Define el valor de la propiedad airEquipType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirEquipType(String value) {
        this.airEquipType = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad purchasedSeatRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchasedSeatRPH() {
        return purchasedSeatRPH;
    }

    /**
     * Define el valor de la propiedad purchasedSeatRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchasedSeatRPH(String value) {
        this.purchasedSeatRPH = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SubType" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="SeatType" type="{http://www.opentravel.org/OTA/2003/05}AirSeatPreferenceType" /&amp;gt;
     *                 &amp;lt;attribute name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="ReqSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *       &amp;lt;attribute name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *       &amp;lt;attribute name="SeatType" type="{http://www.opentravel.org/OTA/2003/05}AirSeatPreferenceType" /&amp;gt;
     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ItinerarySegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subType"
    })
    public static class CabinInfo {

        @XmlElement(name = "SubType")
        protected List<AirSeatMarketingClassType.CabinInfo.SubType> subType;
        @XmlAttribute(name = "ReqSeatQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger reqSeatQty;
        @XmlAttribute(name = "ResBookDesigCode")
        protected String resBookDesigCode;
        @XmlAttribute(name = "ClassCode")
        protected String classCode;
        @XmlAttribute(name = "SeatType")
        protected String seatType;
        @XmlAttribute(name = "TravelerRPH")
        protected String travelerRPH;
        @XmlAttribute(name = "OrigDestSequenceRPH")
        protected String origDestSequenceRPH;
        @XmlAttribute(name = "ItinerarySegmentRPH")
        protected String itinerarySegmentRPH;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Gets the value of the subType property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the subType property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSubType().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirSeatMarketingClassType.CabinInfo.SubType }
         * 
         * 
         */
        public List<AirSeatMarketingClassType.CabinInfo.SubType> getSubType() {
            if (subType == null) {
                subType = new ArrayList<AirSeatMarketingClassType.CabinInfo.SubType>();
            }
            return this.subType;
        }

        /**
         * Obtiene el valor de la propiedad reqSeatQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getReqSeatQty() {
            return reqSeatQty;
        }

        /**
         * Define el valor de la propiedad reqSeatQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setReqSeatQty(BigInteger value) {
            this.reqSeatQty = value;
        }

        /**
         * Obtiene el valor de la propiedad resBookDesigCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResBookDesigCode() {
            return resBookDesigCode;
        }

        /**
         * Define el valor de la propiedad resBookDesigCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResBookDesigCode(String value) {
            this.resBookDesigCode = value;
        }

        /**
         * Obtiene el valor de la propiedad classCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClassCode() {
            return classCode;
        }

        /**
         * Define el valor de la propiedad classCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClassCode(String value) {
            this.classCode = value;
        }

        /**
         * Obtiene el valor de la propiedad seatType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeatType() {
            return seatType;
        }

        /**
         * Define el valor de la propiedad seatType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeatType(String value) {
            this.seatType = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelerRPH() {
            return travelerRPH;
        }

        /**
         * Define el valor de la propiedad travelerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelerRPH(String value) {
            this.travelerRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad origDestSequenceRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigDestSequenceRPH() {
            return origDestSequenceRPH;
        }

        /**
         * Define el valor de la propiedad origDestSequenceRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigDestSequenceRPH(String value) {
            this.origDestSequenceRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad itinerarySegmentRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItinerarySegmentRPH() {
            return itinerarySegmentRPH;
        }

        /**
         * Define el valor de la propiedad itinerarySegmentRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItinerarySegmentRPH(String value) {
            this.itinerarySegmentRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="SeatType" type="{http://www.opentravel.org/OTA/2003/05}AirSeatPreferenceType" /&amp;gt;
         *       &amp;lt;attribute name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SubType {

            @XmlAttribute(name = "SeatType")
            protected String seatType;
            @XmlAttribute(name = "ClassCode")
            protected String classCode;

            /**
             * Obtiene el valor de la propiedad seatType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSeatType() {
                return seatType;
            }

            /**
             * Define el valor de la propiedad seatType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSeatType(String value) {
                this.seatType = value;
            }

            /**
             * Obtiene el valor de la propiedad classCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClassCode() {
                return classCode;
            }

            /**
             * Define el valor de la propiedad classCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClassCode(String value) {
                this.classCode = value;
            }

        }

    }

}
