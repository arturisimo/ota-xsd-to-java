
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Check-in information to be be used for self-service channels (kiosks, web and mobile). This information will will allow a customer, multiple customers or groups traveling together and employees to check-in for eligible flights.
 * 
 * &lt;p&gt;Clase Java para AirCheckInType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirCheckInType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MessageFunction" maxOccurs="23" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Function" use="required"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="SelectCheckInSegments"/&amp;gt;
 *                       &amp;lt;enumeration value="SelectPassengersToCheckIn"/&amp;gt;
 *                       &amp;lt;enumeration value="AssignSeat"/&amp;gt;
 *                       &amp;lt;enumeration value="ChangeSeat"/&amp;gt;
 *                       &amp;lt;enumeration value="RequestUpgrade"/&amp;gt;
 *                       &amp;lt;enumeration value="EnterFrequentFlyerInfoBySegment"/&amp;gt;
 *                       &amp;lt;enumeration value="ListForStandby"/&amp;gt;
 *                       &amp;lt;enumeration value="RequestForAlternateFlight"/&amp;gt;
 *                       &amp;lt;enumeration value="EnterAllowableSSR_OSI_Information"/&amp;gt;
 *                       &amp;lt;enumeration value="EnterAPIS_Information"/&amp;gt;
 *                       &amp;lt;enumeration value="EnterEmergencyContactInformation"/&amp;gt;
 *                       &amp;lt;enumeration value="EnterBaggageInformation"/&amp;gt;
 *                       &amp;lt;enumeration value="RedisplayEnteredInformationAndStatusOfCheckIn"/&amp;gt;
 *                       &amp;lt;enumeration value="VolunteerForDeniedBoarding"/&amp;gt;
 *                       &amp;lt;enumeration value="IssueBagTag"/&amp;gt;
 *                       &amp;lt;enumeration value="ReprintBagTag"/&amp;gt;
 *                       &amp;lt;enumeration value="IssueBoardingPass"/&amp;gt;
 *                       &amp;lt;enumeration value="ReprintBoardingPass"/&amp;gt;
 *                       &amp;lt;enumeration value="PrintItinerary"/&amp;gt;
 *                       &amp;lt;enumeration value="PrintReceipt"/&amp;gt;
 *                       &amp;lt;enumeration value="PrintStandbyCard"/&amp;gt;
 *                       &amp;lt;enumeration value="ReprintStandbyCard"/&amp;gt;
 *                       &amp;lt;enumeration value="CancelCheckInDuringProcess"/&amp;gt;
 *                       &amp;lt;enumeration value="CheckIn"/&amp;gt;
 *                       &amp;lt;enumeration value="ValidatePassengerSeat"/&amp;gt;
 *                       &amp;lt;enumeration value="Unseat"/&amp;gt;
 *                       &amp;lt;enumeration value="ChangeSeatReaccommodation"/&amp;gt;
 *                       &amp;lt;enumeration value="UpdatePassengerInfo"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DocumentAndPrintInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CompanyLogo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AllianceLogo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                 &amp;lt;attribute name="AirAllianceName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;attribute name="PrintDeviceID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                 &amp;lt;attribute name="PrintDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FlightInfo" maxOccurs="16" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CarrierInfo" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" maxOccurs="2"/&amp;gt;
 *                   &amp;lt;element name="AircraftOwner" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="DepartureInformation"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
 *                           &amp;lt;attribute name="DateOfDeparture" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="JulianDateOfDeparture"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                           &amp;lt;attribute name="ScheduledDepartureTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                           &amp;lt;attribute name="LatestCheckInDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="BoardingDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="OtherCheckInInformation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                           &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ArrivalInformation" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
 *                           &amp;lt;attribute name="DateOfArrival" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                           &amp;lt;attribute name="ScheduledArrivalTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                           &amp;lt;attribute name="DateChangeIdentifier"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="-1"/&amp;gt;
 *                                 &amp;lt;enumeration value="+1"/&amp;gt;
 *                                 &amp;lt;enumeration value="+2"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ServiceDetails" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="MealService" maxOccurs="12" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                           &amp;lt;attribute name="DeniedBoardingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="FlightProductName" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                           &amp;lt;attribute name="InFlightServiceCodes" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TransitPoint" type="{http://www.opentravel.org/OTA/2003/05}LocationType" maxOccurs="16" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="CabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *                 &amp;lt;attribute name="CabinName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PassengerInfo" maxOccurs="99"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PassengerName" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="PassengerWeight" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="OtherServiceInformation" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="ContactInfo" maxOccurs="2" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Relationship" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="OptionToDecline" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PassengerType" maxOccurs="9" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GenderGroup"/&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TicketingInfo" maxOccurs="4" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CouponInfo" maxOccurs="16"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}FareBasisCodeType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="FlightRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *                                     &amp;lt;attribute name="CouponNumber" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
 *                                     &amp;lt;attribute name="DocumentNumberCheckDigit"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="PaperTicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="PricingInfo" type="{http://www.opentravel.org/OTA/2003/05}FareType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="PaymentTotal" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="FormOfPayment" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" maxOccurs="4" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="CityCodeOfIssue" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="ACN_Number" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                           &amp;lt;attribute name="AirlineAccountingCode" use="required"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="FormAndSerialNumber" use="required"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9a-zA-Z]{10}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="AgencyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="CountryCodeOfIssue" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                           &amp;lt;attribute name="IssuingCompanyName"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9a-zA-Z]{1,35}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="EndorsementInfo" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                           &amp;lt;attribute name="DateOfIssue" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="OriginalDateofIssue" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="TicketBookReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="JumpSeatAuthority" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                 &amp;lt;attribute name="SecurityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                 &amp;lt;attribute name="GroupReference" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="AddToStandbyDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="InfantIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EmployeeSeniorityDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="EmployeeLengthOfService" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                 &amp;lt;attribute name="PassengerAssociationID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PassengerFlightInfo" maxOccurs="1600" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="BookingInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="BookingReferenceID" maxOccurs="9" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="StatusCode"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[A-Z0-9a-z]{2}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="StatusName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                           &amp;lt;attribute name="OriginalResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                           &amp;lt;attribute name="UpgradeResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="SeatBoardingInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="BoardingPriority" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="BoardingZone"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9a-zA-Z]{1,9}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="SeatCharacteristics" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="SeatNumber"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9]{1,3}[A-Z]{1}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="NonRevCategory"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="NRPS"/&amp;gt;
 *                                 &amp;lt;enumeration value="NRSA"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Fee" type="{http://www.opentravel.org/OTA/2003/05}FeeType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="SpecialServiceRequest" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="API_Info" maxOccurs="10" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DocumentType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Address" maxOccurs="2" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="API_InfoSource" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                           &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="BirthStateProv" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
 *                           &amp;lt;attribute name="BirthLocation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="InternationalProcessingInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="DocumentVerification" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
 *                           &amp;lt;attribute name="AirportPassengerProcessingCode"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9]{2}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="DocumentVerifiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="FrequentTravelerInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="PassengerRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="FlightRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="BaggageCabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *                 &amp;lt;attribute name="SpecialPurposeCode"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[A-Z]{1,4}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="DeniedBoardingVolunteerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ShortCheckInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NonRevAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="PassengerCouponStatus" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
 *                 &amp;lt;attribute name="DCS_SequenceNumber"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[A-Za-z0-9]{1,5}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="DCS_PassengerRefNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="BoardingPassColor" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="StandbyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BaggageInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CheckedBaggageDetails" maxOccurs="999" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CheckedBagWeight" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="BagTagDetails" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Length" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Type"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="IssuerCode"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="SerialNumber"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[0-9]{6}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="IssuanceMethod"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Agent"/&amp;gt;
 *                                           &amp;lt;enumeration value="Skycap"/&amp;gt;
 *                                           &amp;lt;enumeration value="SelfService"/&amp;gt;
 *                                           &amp;lt;enumeration value="Vendor"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="CarrierCode"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[A-Za-z0-9]{2,3}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="SpecialType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="ConjunctionBagTagInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="BaggagePoolIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="HeavyBagIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="ShortCheckLocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="ShortCheckCodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="BagStandByIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="PriorityHandlingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="HotConnectionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="VoluntarySeparationIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="BagSecurityStatus"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[A-Za-z0-9]{4}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="CheckedBagWeightTotal" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="HandBagWeightTotal" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="PassengerRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="CheckedBagCountTotal" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                 &amp;lt;attribute name="HandBagCountTotal" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                 &amp;lt;attribute name="HeadOfBaggagePoolInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PaymentInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PaymentDetail" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
 *                           &amp;lt;attribute name="PaymentReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PaymentTotal" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferChoiceType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirCheckInType", propOrder = {
    "pos",
    "messageFunction",
    "documentAndPrintInfo",
    "flightInfo",
    "passengerInfo",
    "passengerFlightInfo",
    "baggageInfo",
    "paymentInfo",
    "offer"
})
@XmlSeeAlso({
    OTAAirCheckInRQ.class
})
public class AirCheckInType {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "MessageFunction")
    protected List<AirCheckInType.MessageFunction> messageFunction;
    @XmlElement(name = "DocumentAndPrintInfo")
    protected AirCheckInType.DocumentAndPrintInfo documentAndPrintInfo;
    @XmlElement(name = "FlightInfo")
    protected List<AirCheckInType.FlightInfo> flightInfo;
    @XmlElement(name = "PassengerInfo", required = true)
    protected List<AirCheckInType.PassengerInfo> passengerInfo;
    @XmlElement(name = "PassengerFlightInfo")
    protected List<AirCheckInType.PassengerFlightInfo> passengerFlightInfo;
    @XmlElement(name = "BaggageInfo")
    protected List<AirCheckInType.BaggageInfo> baggageInfo;
    @XmlElement(name = "PaymentInfo")
    protected AirCheckInType.PaymentInfo paymentInfo;
    @XmlElement(name = "Offer")
    protected AirOfferChoiceType offer;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Gets the value of the messageFunction property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the messageFunction property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getMessageFunction().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirCheckInType.MessageFunction }
     * 
     * 
     */
    public List<AirCheckInType.MessageFunction> getMessageFunction() {
        if (messageFunction == null) {
            messageFunction = new ArrayList<AirCheckInType.MessageFunction>();
        }
        return this.messageFunction;
    }

    /**
     * Obtiene el valor de la propiedad documentAndPrintInfo.
     * 
     * @return
     *     possible object is
     *     {@link AirCheckInType.DocumentAndPrintInfo }
     *     
     */
    public AirCheckInType.DocumentAndPrintInfo getDocumentAndPrintInfo() {
        return documentAndPrintInfo;
    }

    /**
     * Define el valor de la propiedad documentAndPrintInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AirCheckInType.DocumentAndPrintInfo }
     *     
     */
    public void setDocumentAndPrintInfo(AirCheckInType.DocumentAndPrintInfo value) {
        this.documentAndPrintInfo = value;
    }

    /**
     * Gets the value of the flightInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFlightInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirCheckInType.FlightInfo }
     * 
     * 
     */
    public List<AirCheckInType.FlightInfo> getFlightInfo() {
        if (flightInfo == null) {
            flightInfo = new ArrayList<AirCheckInType.FlightInfo>();
        }
        return this.flightInfo;
    }

    /**
     * Gets the value of the passengerInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPassengerInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirCheckInType.PassengerInfo }
     * 
     * 
     */
    public List<AirCheckInType.PassengerInfo> getPassengerInfo() {
        if (passengerInfo == null) {
            passengerInfo = new ArrayList<AirCheckInType.PassengerInfo>();
        }
        return this.passengerInfo;
    }

    /**
     * Gets the value of the passengerFlightInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerFlightInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPassengerFlightInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirCheckInType.PassengerFlightInfo }
     * 
     * 
     */
    public List<AirCheckInType.PassengerFlightInfo> getPassengerFlightInfo() {
        if (passengerFlightInfo == null) {
            passengerFlightInfo = new ArrayList<AirCheckInType.PassengerFlightInfo>();
        }
        return this.passengerFlightInfo;
    }

    /**
     * Gets the value of the baggageInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baggageInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBaggageInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirCheckInType.BaggageInfo }
     * 
     * 
     */
    public List<AirCheckInType.BaggageInfo> getBaggageInfo() {
        if (baggageInfo == null) {
            baggageInfo = new ArrayList<AirCheckInType.BaggageInfo>();
        }
        return this.baggageInfo;
    }

    /**
     * Obtiene el valor de la propiedad paymentInfo.
     * 
     * @return
     *     possible object is
     *     {@link AirCheckInType.PaymentInfo }
     *     
     */
    public AirCheckInType.PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * Define el valor de la propiedad paymentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AirCheckInType.PaymentInfo }
     *     
     */
    public void setPaymentInfo(AirCheckInType.PaymentInfo value) {
        this.paymentInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad offer.
     * 
     * @return
     *     possible object is
     *     {@link AirOfferChoiceType }
     *     
     */
    public AirOfferChoiceType getOffer() {
        return offer;
    }

    /**
     * Define el valor de la propiedad offer.
     * 
     * @param value
     *     allowed object is
     *     {@link AirOfferChoiceType }
     *     
     */
    public void setOffer(AirOfferChoiceType value) {
        this.offer = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CheckedBaggageDetails" maxOccurs="999" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CheckedBagWeight" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="BagTagDetails" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Length" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Type"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="IssuerCode"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="SerialNumber"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[0-9]{6}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="IssuanceMethod"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Agent"/&amp;gt;
     *                                 &amp;lt;enumeration value="Skycap"/&amp;gt;
     *                                 &amp;lt;enumeration value="SelfService"/&amp;gt;
     *                                 &amp;lt;enumeration value="Vendor"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="CarrierCode"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[A-Za-z0-9]{2,3}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="SpecialType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="ConjunctionBagTagInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="BaggagePoolIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="HeavyBagIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ShortCheckLocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="ShortCheckCodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="BagStandByIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PriorityHandlingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="HotConnectionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="VoluntarySeparationIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="BagSecurityStatus"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[A-Za-z0-9]{4}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CheckedBagWeightTotal" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="HandBagWeightTotal" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="PassengerRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="CheckedBagCountTotal" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *       &amp;lt;attribute name="HandBagCountTotal" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *       &amp;lt;attribute name="HeadOfBaggagePoolInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "checkedBaggageDetails",
        "checkedBagWeightTotal",
        "handBagWeightTotal"
    })
    public static class BaggageInfo {

        @XmlElement(name = "CheckedBaggageDetails")
        protected List<AirCheckInType.BaggageInfo.CheckedBaggageDetails> checkedBaggageDetails;
        @XmlElement(name = "CheckedBagWeightTotal")
        protected AirCheckInType.BaggageInfo.CheckedBagWeightTotal checkedBagWeightTotal;
        @XmlElement(name = "HandBagWeightTotal")
        protected AirCheckInType.BaggageInfo.HandBagWeightTotal handBagWeightTotal;
        @XmlAttribute(name = "PassengerRPH", required = true)
        protected String passengerRPH;
        @XmlAttribute(name = "CheckedBagCountTotal")
        protected Integer checkedBagCountTotal;
        @XmlAttribute(name = "HandBagCountTotal")
        protected Integer handBagCountTotal;
        @XmlAttribute(name = "HeadOfBaggagePoolInd")
        protected Boolean headOfBaggagePoolInd;

        /**
         * Gets the value of the checkedBaggageDetails property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the checkedBaggageDetails property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCheckedBaggageDetails().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails }
         * 
         * 
         */
        public List<AirCheckInType.BaggageInfo.CheckedBaggageDetails> getCheckedBaggageDetails() {
            if (checkedBaggageDetails == null) {
                checkedBaggageDetails = new ArrayList<AirCheckInType.BaggageInfo.CheckedBaggageDetails>();
            }
            return this.checkedBaggageDetails;
        }

        /**
         * Obtiene el valor de la propiedad checkedBagWeightTotal.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.BaggageInfo.CheckedBagWeightTotal }
         *     
         */
        public AirCheckInType.BaggageInfo.CheckedBagWeightTotal getCheckedBagWeightTotal() {
            return checkedBagWeightTotal;
        }

        /**
         * Define el valor de la propiedad checkedBagWeightTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.BaggageInfo.CheckedBagWeightTotal }
         *     
         */
        public void setCheckedBagWeightTotal(AirCheckInType.BaggageInfo.CheckedBagWeightTotal value) {
            this.checkedBagWeightTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad handBagWeightTotal.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.BaggageInfo.HandBagWeightTotal }
         *     
         */
        public AirCheckInType.BaggageInfo.HandBagWeightTotal getHandBagWeightTotal() {
            return handBagWeightTotal;
        }

        /**
         * Define el valor de la propiedad handBagWeightTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.BaggageInfo.HandBagWeightTotal }
         *     
         */
        public void setHandBagWeightTotal(AirCheckInType.BaggageInfo.HandBagWeightTotal value) {
            this.handBagWeightTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassengerRPH() {
            return passengerRPH;
        }

        /**
         * Define el valor de la propiedad passengerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassengerRPH(String value) {
            this.passengerRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad checkedBagCountTotal.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCheckedBagCountTotal() {
            return checkedBagCountTotal;
        }

        /**
         * Define el valor de la propiedad checkedBagCountTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCheckedBagCountTotal(Integer value) {
            this.checkedBagCountTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad handBagCountTotal.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHandBagCountTotal() {
            return handBagCountTotal;
        }

        /**
         * Define el valor de la propiedad handBagCountTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHandBagCountTotal(Integer value) {
            this.handBagCountTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad headOfBaggagePoolInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isHeadOfBaggagePoolInd() {
            return headOfBaggagePoolInd;
        }

        /**
         * Define el valor de la propiedad headOfBaggagePoolInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setHeadOfBaggagePoolInd(Boolean value) {
            this.headOfBaggagePoolInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CheckedBagWeightTotal {

            @XmlAttribute(name = "UnitOfMeasureQuantity")
            protected BigDecimal unitOfMeasureQuantity;
            @XmlAttribute(name = "UnitOfMeasure")
            protected String unitOfMeasure;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;

            /**
             * Obtiene el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getUnitOfMeasureQuantity() {
                return unitOfMeasureQuantity;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setUnitOfMeasureQuantity(BigDecimal value) {
                this.unitOfMeasureQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasure.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasure() {
                return unitOfMeasure;
            }

            /**
             * Define el valor de la propiedad unitOfMeasure.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasure(String value) {
                this.unitOfMeasure = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CheckedBagWeight" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="BagTagDetails" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Length" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Type"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="IssuerCode"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="SerialNumber"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[0-9]{6}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="IssuanceMethod"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Agent"/&amp;gt;
         *                       &amp;lt;enumeration value="Skycap"/&amp;gt;
         *                       &amp;lt;enumeration value="SelfService"/&amp;gt;
         *                       &amp;lt;enumeration value="Vendor"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="CarrierCode"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[A-Za-z0-9]{2,3}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="SpecialType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="ConjunctionBagTagInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="BaggagePoolIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="HeavyBagIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ShortCheckLocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="ShortCheckCodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="BagStandByIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PriorityHandlingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="HotConnectionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="VoluntarySeparationIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="BagSecurityStatus"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[A-Za-z0-9]{4}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "checkedBagWeight",
            "bagTagDetails"
        })
        public static class CheckedBaggageDetails {

            @XmlElement(name = "CheckedBagWeight")
            protected AirCheckInType.BaggageInfo.CheckedBaggageDetails.CheckedBagWeight checkedBagWeight;
            @XmlElement(name = "BagTagDetails")
            protected AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails bagTagDetails;
            @XmlAttribute(name = "BaggagePoolIndicator")
            protected Boolean baggagePoolIndicator;
            @XmlAttribute(name = "HeavyBagIndicator")
            protected Boolean heavyBagIndicator;
            @XmlAttribute(name = "ShortCheckLocationCode")
            protected String shortCheckLocationCode;
            @XmlAttribute(name = "ShortCheckCodeContext")
            protected String shortCheckCodeContext;
            @XmlAttribute(name = "BagStandByIndicator")
            protected Boolean bagStandByIndicator;
            @XmlAttribute(name = "PriorityHandlingIndicator")
            protected Boolean priorityHandlingIndicator;
            @XmlAttribute(name = "HotConnectionIndicator")
            protected Boolean hotConnectionIndicator;
            @XmlAttribute(name = "VoluntarySeparationIndicator")
            protected Boolean voluntarySeparationIndicator;
            @XmlAttribute(name = "BagSecurityStatus")
            protected String bagSecurityStatus;

            /**
             * Obtiene el valor de la propiedad checkedBagWeight.
             * 
             * @return
             *     possible object is
             *     {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails.CheckedBagWeight }
             *     
             */
            public AirCheckInType.BaggageInfo.CheckedBaggageDetails.CheckedBagWeight getCheckedBagWeight() {
                return checkedBagWeight;
            }

            /**
             * Define el valor de la propiedad checkedBagWeight.
             * 
             * @param value
             *     allowed object is
             *     {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails.CheckedBagWeight }
             *     
             */
            public void setCheckedBagWeight(AirCheckInType.BaggageInfo.CheckedBaggageDetails.CheckedBagWeight value) {
                this.checkedBagWeight = value;
            }

            /**
             * Obtiene el valor de la propiedad bagTagDetails.
             * 
             * @return
             *     possible object is
             *     {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails }
             *     
             */
            public AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails getBagTagDetails() {
                return bagTagDetails;
            }

            /**
             * Define el valor de la propiedad bagTagDetails.
             * 
             * @param value
             *     allowed object is
             *     {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails }
             *     
             */
            public void setBagTagDetails(AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails value) {
                this.bagTagDetails = value;
            }

            /**
             * Obtiene el valor de la propiedad baggagePoolIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isBaggagePoolIndicator() {
                return baggagePoolIndicator;
            }

            /**
             * Define el valor de la propiedad baggagePoolIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setBaggagePoolIndicator(Boolean value) {
                this.baggagePoolIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad heavyBagIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isHeavyBagIndicator() {
                return heavyBagIndicator;
            }

            /**
             * Define el valor de la propiedad heavyBagIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setHeavyBagIndicator(Boolean value) {
                this.heavyBagIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad shortCheckLocationCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortCheckLocationCode() {
                return shortCheckLocationCode;
            }

            /**
             * Define el valor de la propiedad shortCheckLocationCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortCheckLocationCode(String value) {
                this.shortCheckLocationCode = value;
            }

            /**
             * Obtiene el valor de la propiedad shortCheckCodeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortCheckCodeContext() {
                return shortCheckCodeContext;
            }

            /**
             * Define el valor de la propiedad shortCheckCodeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortCheckCodeContext(String value) {
                this.shortCheckCodeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad bagStandByIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isBagStandByIndicator() {
                return bagStandByIndicator;
            }

            /**
             * Define el valor de la propiedad bagStandByIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setBagStandByIndicator(Boolean value) {
                this.bagStandByIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad priorityHandlingIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPriorityHandlingIndicator() {
                return priorityHandlingIndicator;
            }

            /**
             * Define el valor de la propiedad priorityHandlingIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPriorityHandlingIndicator(Boolean value) {
                this.priorityHandlingIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad hotConnectionIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isHotConnectionIndicator() {
                return hotConnectionIndicator;
            }

            /**
             * Define el valor de la propiedad hotConnectionIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setHotConnectionIndicator(Boolean value) {
                this.hotConnectionIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad voluntarySeparationIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isVoluntarySeparationIndicator() {
                return voluntarySeparationIndicator;
            }

            /**
             * Define el valor de la propiedad voluntarySeparationIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setVoluntarySeparationIndicator(Boolean value) {
                this.voluntarySeparationIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad bagSecurityStatus.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBagSecurityStatus() {
                return bagSecurityStatus;
            }

            /**
             * Define el valor de la propiedad bagSecurityStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBagSecurityStatus(String value) {
                this.bagSecurityStatus = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Length" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Type"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="IssuerCode"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="SerialNumber"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[0-9]{6}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="IssuanceMethod"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Agent"/&amp;gt;
             *             &amp;lt;enumeration value="Skycap"/&amp;gt;
             *             &amp;lt;enumeration value="SelfService"/&amp;gt;
             *             &amp;lt;enumeration value="Vendor"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="CarrierCode"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[A-Za-z0-9]{2,3}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="SpecialType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="ConjunctionBagTagInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "length"
            })
            public static class BagTagDetails {

                @XmlElement(name = "Length")
                protected AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails.Length length;
                @XmlAttribute(name = "Type")
                protected String type;
                @XmlAttribute(name = "IssuerCode")
                protected String issuerCode;
                @XmlAttribute(name = "SerialNumber")
                protected String serialNumber;
                @XmlAttribute(name = "IssuanceMethod")
                protected String issuanceMethod;
                @XmlAttribute(name = "CarrierCode")
                protected String carrierCode;
                @XmlAttribute(name = "SpecialType")
                protected String specialType;
                @XmlAttribute(name = "ConjunctionBagTagInd")
                protected Boolean conjunctionBagTagInd;

                /**
                 * Obtiene el valor de la propiedad length.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails.Length }
                 *     
                 */
                public AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails.Length getLength() {
                    return length;
                }

                /**
                 * Define el valor de la propiedad length.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails.Length }
                 *     
                 */
                public void setLength(AirCheckInType.BaggageInfo.CheckedBaggageDetails.BagTagDetails.Length value) {
                    this.length = value;
                }

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad issuerCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIssuerCode() {
                    return issuerCode;
                }

                /**
                 * Define el valor de la propiedad issuerCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIssuerCode(String value) {
                    this.issuerCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad serialNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSerialNumber() {
                    return serialNumber;
                }

                /**
                 * Define el valor de la propiedad serialNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSerialNumber(String value) {
                    this.serialNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad issuanceMethod.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIssuanceMethod() {
                    return issuanceMethod;
                }

                /**
                 * Define el valor de la propiedad issuanceMethod.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIssuanceMethod(String value) {
                    this.issuanceMethod = value;
                }

                /**
                 * Obtiene el valor de la propiedad carrierCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCarrierCode() {
                    return carrierCode;
                }

                /**
                 * Define el valor de la propiedad carrierCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCarrierCode(String value) {
                    this.carrierCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad specialType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSpecialType() {
                    return specialType;
                }

                /**
                 * Define el valor de la propiedad specialType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSpecialType(String value) {
                    this.specialType = value;
                }

                /**
                 * Obtiene el valor de la propiedad conjunctionBagTagInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isConjunctionBagTagInd() {
                    return conjunctionBagTagInd;
                }

                /**
                 * Define el valor de la propiedad conjunctionBagTagInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setConjunctionBagTagInd(Boolean value) {
                    this.conjunctionBagTagInd = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Length {

                    @XmlAttribute(name = "UnitOfMeasureQuantity")
                    protected BigDecimal unitOfMeasureQuantity;
                    @XmlAttribute(name = "UnitOfMeasure")
                    protected String unitOfMeasure;
                    @XmlAttribute(name = "UnitOfMeasureCode")
                    protected String unitOfMeasureCode;

                    /**
                     * Obtiene el valor de la propiedad unitOfMeasureQuantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getUnitOfMeasureQuantity() {
                        return unitOfMeasureQuantity;
                    }

                    /**
                     * Define el valor de la propiedad unitOfMeasureQuantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setUnitOfMeasureQuantity(BigDecimal value) {
                        this.unitOfMeasureQuantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad unitOfMeasure.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUnitOfMeasure() {
                        return unitOfMeasure;
                    }

                    /**
                     * Define el valor de la propiedad unitOfMeasure.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUnitOfMeasure(String value) {
                        this.unitOfMeasure = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad unitOfMeasureCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUnitOfMeasureCode() {
                        return unitOfMeasureCode;
                    }

                    /**
                     * Define el valor de la propiedad unitOfMeasureCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUnitOfMeasureCode(String value) {
                        this.unitOfMeasureCode = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CheckedBagWeight {

                @XmlAttribute(name = "UnitOfMeasureQuantity")
                protected BigDecimal unitOfMeasureQuantity;
                @XmlAttribute(name = "UnitOfMeasure")
                protected String unitOfMeasure;
                @XmlAttribute(name = "UnitOfMeasureCode")
                protected String unitOfMeasureCode;

                /**
                 * Obtiene el valor de la propiedad unitOfMeasureQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getUnitOfMeasureQuantity() {
                    return unitOfMeasureQuantity;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasureQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setUnitOfMeasureQuantity(BigDecimal value) {
                    this.unitOfMeasureQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasure.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnitOfMeasure() {
                    return unitOfMeasure;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasure.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnitOfMeasure(String value) {
                    this.unitOfMeasure = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasureCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnitOfMeasureCode() {
                    return unitOfMeasureCode;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasureCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnitOfMeasureCode(String value) {
                    this.unitOfMeasureCode = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class HandBagWeightTotal {

            @XmlAttribute(name = "UnitOfMeasureQuantity")
            protected BigDecimal unitOfMeasureQuantity;
            @XmlAttribute(name = "UnitOfMeasure")
            protected String unitOfMeasure;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;

            /**
             * Obtiene el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getUnitOfMeasureQuantity() {
                return unitOfMeasureQuantity;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setUnitOfMeasureQuantity(BigDecimal value) {
                this.unitOfMeasureQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasure.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasure() {
                return unitOfMeasure;
            }

            /**
             * Define el valor de la propiedad unitOfMeasure.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasure(String value) {
                this.unitOfMeasure = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CompanyLogo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AllianceLogo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *       &amp;lt;attribute name="AirAllianceName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="PrintDeviceID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="PrintDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyLogo",
        "allianceLogo"
    })
    public static class DocumentAndPrintInfo {

        @XmlElement(name = "CompanyLogo")
        protected AirCheckInType.DocumentAndPrintInfo.CompanyLogo companyLogo;
        @XmlElement(name = "AllianceLogo")
        protected AirCheckInType.DocumentAndPrintInfo.AllianceLogo allianceLogo;
        @XmlAttribute(name = "AirAllianceName")
        protected String airAllianceName;
        @XmlAttribute(name = "PrintDeviceID")
        protected String printDeviceID;
        @XmlAttribute(name = "PrintDateTime")
        protected String printDateTime;
        @XmlAttribute(name = "CompanyShortName")
        protected String companyShortName;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;

        /**
         * Obtiene el valor de la propiedad companyLogo.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.DocumentAndPrintInfo.CompanyLogo }
         *     
         */
        public AirCheckInType.DocumentAndPrintInfo.CompanyLogo getCompanyLogo() {
            return companyLogo;
        }

        /**
         * Define el valor de la propiedad companyLogo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.DocumentAndPrintInfo.CompanyLogo }
         *     
         */
        public void setCompanyLogo(AirCheckInType.DocumentAndPrintInfo.CompanyLogo value) {
            this.companyLogo = value;
        }

        /**
         * Obtiene el valor de la propiedad allianceLogo.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.DocumentAndPrintInfo.AllianceLogo }
         *     
         */
        public AirCheckInType.DocumentAndPrintInfo.AllianceLogo getAllianceLogo() {
            return allianceLogo;
        }

        /**
         * Define el valor de la propiedad allianceLogo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.DocumentAndPrintInfo.AllianceLogo }
         *     
         */
        public void setAllianceLogo(AirCheckInType.DocumentAndPrintInfo.AllianceLogo value) {
            this.allianceLogo = value;
        }

        /**
         * Obtiene el valor de la propiedad airAllianceName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAirAllianceName() {
            return airAllianceName;
        }

        /**
         * Define el valor de la propiedad airAllianceName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAirAllianceName(String value) {
            this.airAllianceName = value;
        }

        /**
         * Obtiene el valor de la propiedad printDeviceID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrintDeviceID() {
            return printDeviceID;
        }

        /**
         * Define el valor de la propiedad printDeviceID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrintDeviceID(String value) {
            this.printDeviceID = value;
        }

        /**
         * Obtiene el valor de la propiedad printDateTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrintDateTime() {
            return printDateTime;
        }

        /**
         * Define el valor de la propiedad printDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrintDateTime(String value) {
            this.printDateTime = value;
        }

        /**
         * Obtiene el valor de la propiedad companyShortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyShortName() {
            return companyShortName;
        }

        /**
         * Define el valor de la propiedad companyShortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyShortName(String value) {
            this.companyShortName = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad countryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * Define el valor de la propiedad countryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AllianceLogo {

            @XmlAttribute(name = "ContentData")
            protected String contentData;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "PictureCategoryCode")
            protected String pictureCategoryCode;
            @XmlAttribute(name = "Version")
            protected String version;
            @XmlAttribute(name = "ContentTitle")
            protected String contentTitle;
            @XmlAttribute(name = "ContentCaption")
            protected String contentCaption;
            @XmlAttribute(name = "CopyrightNotice")
            protected String copyrightNotice;
            @XmlAttribute(name = "FileName")
            protected String fileName;
            @XmlAttribute(name = "FileSize")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger fileSize;
            @XmlAttribute(name = "MultimediaObjectHeight")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger multimediaObjectHeight;
            @XmlAttribute(name = "MultimediaObjectWidth")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger multimediaObjectWidth;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;
            @XmlAttribute(name = "ContentID")
            protected String contentID;
            @XmlAttribute(name = "ContentCode")
            protected String contentCode;
            @XmlAttribute(name = "ContentFormatCode")
            protected String contentFormatCode;
            @XmlAttribute(name = "RecordID")
            protected String recordID;
            @XmlAttribute(name = "Removal")
            protected Boolean removal;
            @XmlAttribute(name = "CodeDetail")
            protected String codeDetail;

            /**
             * Obtiene el valor de la propiedad contentData.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentData() {
                return contentData;
            }

            /**
             * Define el valor de la propiedad contentData.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentData(String value) {
                this.contentData = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad pictureCategoryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPictureCategoryCode() {
                return pictureCategoryCode;
            }

            /**
             * Define el valor de la propiedad pictureCategoryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPictureCategoryCode(String value) {
                this.pictureCategoryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad version.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVersion() {
                return version;
            }

            /**
             * Define el valor de la propiedad version.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVersion(String value) {
                this.version = value;
            }

            /**
             * Obtiene el valor de la propiedad contentTitle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentTitle() {
                return contentTitle;
            }

            /**
             * Define el valor de la propiedad contentTitle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentTitle(String value) {
                this.contentTitle = value;
            }

            /**
             * Obtiene el valor de la propiedad contentCaption.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentCaption() {
                return contentCaption;
            }

            /**
             * Define el valor de la propiedad contentCaption.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentCaption(String value) {
                this.contentCaption = value;
            }

            /**
             * Obtiene el valor de la propiedad copyrightNotice.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCopyrightNotice() {
                return copyrightNotice;
            }

            /**
             * Define el valor de la propiedad copyrightNotice.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCopyrightNotice(String value) {
                this.copyrightNotice = value;
            }

            /**
             * Obtiene el valor de la propiedad fileName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFileName() {
                return fileName;
            }

            /**
             * Define el valor de la propiedad fileName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFileName(String value) {
                this.fileName = value;
            }

            /**
             * Obtiene el valor de la propiedad fileSize.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFileSize() {
                return fileSize;
            }

            /**
             * Define el valor de la propiedad fileSize.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFileSize(BigInteger value) {
                this.fileSize = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaObjectHeight.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMultimediaObjectHeight() {
                return multimediaObjectHeight;
            }

            /**
             * Define el valor de la propiedad multimediaObjectHeight.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMultimediaObjectHeight(BigInteger value) {
                this.multimediaObjectHeight = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaObjectWidth.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMultimediaObjectWidth() {
                return multimediaObjectWidth;
            }

            /**
             * Define el valor de la propiedad multimediaObjectWidth.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMultimediaObjectWidth(BigInteger value) {
                this.multimediaObjectWidth = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

            /**
             * Obtiene el valor de la propiedad contentID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentID() {
                return contentID;
            }

            /**
             * Define el valor de la propiedad contentID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentID(String value) {
                this.contentID = value;
            }

            /**
             * Obtiene el valor de la propiedad contentCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentCode() {
                return contentCode;
            }

            /**
             * Define el valor de la propiedad contentCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentCode(String value) {
                this.contentCode = value;
            }

            /**
             * Obtiene el valor de la propiedad contentFormatCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentFormatCode() {
                return contentFormatCode;
            }

            /**
             * Define el valor de la propiedad contentFormatCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentFormatCode(String value) {
                this.contentFormatCode = value;
            }

            /**
             * Obtiene el valor de la propiedad recordID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRecordID() {
                return recordID;
            }

            /**
             * Define el valor de la propiedad recordID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRecordID(String value) {
                this.recordID = value;
            }

            /**
             * Obtiene el valor de la propiedad removal.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRemoval() {
                return removal;
            }

            /**
             * Define el valor de la propiedad removal.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRemoval(Boolean value) {
                this.removal = value;
            }

            /**
             * Obtiene el valor de la propiedad codeDetail.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeDetail() {
                return codeDetail;
            }

            /**
             * Define el valor de la propiedad codeDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeDetail(String value) {
                this.codeDetail = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CompanyLogo {

            @XmlAttribute(name = "ContentData")
            protected String contentData;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "PictureCategoryCode")
            protected String pictureCategoryCode;
            @XmlAttribute(name = "Version")
            protected String version;
            @XmlAttribute(name = "ContentTitle")
            protected String contentTitle;
            @XmlAttribute(name = "ContentCaption")
            protected String contentCaption;
            @XmlAttribute(name = "CopyrightNotice")
            protected String copyrightNotice;
            @XmlAttribute(name = "FileName")
            protected String fileName;
            @XmlAttribute(name = "FileSize")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger fileSize;
            @XmlAttribute(name = "MultimediaObjectHeight")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger multimediaObjectHeight;
            @XmlAttribute(name = "MultimediaObjectWidth")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger multimediaObjectWidth;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;
            @XmlAttribute(name = "ContentID")
            protected String contentID;
            @XmlAttribute(name = "ContentCode")
            protected String contentCode;
            @XmlAttribute(name = "ContentFormatCode")
            protected String contentFormatCode;
            @XmlAttribute(name = "RecordID")
            protected String recordID;
            @XmlAttribute(name = "Removal")
            protected Boolean removal;
            @XmlAttribute(name = "CodeDetail")
            protected String codeDetail;

            /**
             * Obtiene el valor de la propiedad contentData.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentData() {
                return contentData;
            }

            /**
             * Define el valor de la propiedad contentData.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentData(String value) {
                this.contentData = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad pictureCategoryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPictureCategoryCode() {
                return pictureCategoryCode;
            }

            /**
             * Define el valor de la propiedad pictureCategoryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPictureCategoryCode(String value) {
                this.pictureCategoryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad version.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVersion() {
                return version;
            }

            /**
             * Define el valor de la propiedad version.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVersion(String value) {
                this.version = value;
            }

            /**
             * Obtiene el valor de la propiedad contentTitle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentTitle() {
                return contentTitle;
            }

            /**
             * Define el valor de la propiedad contentTitle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentTitle(String value) {
                this.contentTitle = value;
            }

            /**
             * Obtiene el valor de la propiedad contentCaption.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentCaption() {
                return contentCaption;
            }

            /**
             * Define el valor de la propiedad contentCaption.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentCaption(String value) {
                this.contentCaption = value;
            }

            /**
             * Obtiene el valor de la propiedad copyrightNotice.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCopyrightNotice() {
                return copyrightNotice;
            }

            /**
             * Define el valor de la propiedad copyrightNotice.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCopyrightNotice(String value) {
                this.copyrightNotice = value;
            }

            /**
             * Obtiene el valor de la propiedad fileName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFileName() {
                return fileName;
            }

            /**
             * Define el valor de la propiedad fileName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFileName(String value) {
                this.fileName = value;
            }

            /**
             * Obtiene el valor de la propiedad fileSize.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFileSize() {
                return fileSize;
            }

            /**
             * Define el valor de la propiedad fileSize.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFileSize(BigInteger value) {
                this.fileSize = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaObjectHeight.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMultimediaObjectHeight() {
                return multimediaObjectHeight;
            }

            /**
             * Define el valor de la propiedad multimediaObjectHeight.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMultimediaObjectHeight(BigInteger value) {
                this.multimediaObjectHeight = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaObjectWidth.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMultimediaObjectWidth() {
                return multimediaObjectWidth;
            }

            /**
             * Define el valor de la propiedad multimediaObjectWidth.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMultimediaObjectWidth(BigInteger value) {
                this.multimediaObjectWidth = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

            /**
             * Obtiene el valor de la propiedad contentID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentID() {
                return contentID;
            }

            /**
             * Define el valor de la propiedad contentID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentID(String value) {
                this.contentID = value;
            }

            /**
             * Obtiene el valor de la propiedad contentCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentCode() {
                return contentCode;
            }

            /**
             * Define el valor de la propiedad contentCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentCode(String value) {
                this.contentCode = value;
            }

            /**
             * Obtiene el valor de la propiedad contentFormatCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentFormatCode() {
                return contentFormatCode;
            }

            /**
             * Define el valor de la propiedad contentFormatCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentFormatCode(String value) {
                this.contentFormatCode = value;
            }

            /**
             * Obtiene el valor de la propiedad recordID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRecordID() {
                return recordID;
            }

            /**
             * Define el valor de la propiedad recordID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRecordID(String value) {
                this.recordID = value;
            }

            /**
             * Obtiene el valor de la propiedad removal.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRemoval() {
                return removal;
            }

            /**
             * Define el valor de la propiedad removal.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRemoval(Boolean value) {
                this.removal = value;
            }

            /**
             * Obtiene el valor de la propiedad codeDetail.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeDetail() {
                return codeDetail;
            }

            /**
             * Define el valor de la propiedad codeDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeDetail(String value) {
                this.codeDetail = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CarrierInfo" type="{http://www.opentravel.org/OTA/2003/05}OperatingAirlineType" maxOccurs="2"/&amp;gt;
     *         &amp;lt;element name="AircraftOwner" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="DepartureInformation"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
     *                 &amp;lt;attribute name="DateOfDeparture" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="JulianDateOfDeparture"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                 &amp;lt;attribute name="ScheduledDepartureTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                 &amp;lt;attribute name="LatestCheckInDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="BoardingDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="OtherCheckInInformation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ArrivalInformation" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
     *                 &amp;lt;attribute name="DateOfArrival" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                 &amp;lt;attribute name="ScheduledArrivalTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                 &amp;lt;attribute name="DateChangeIdentifier"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="-1"/&amp;gt;
     *                       &amp;lt;enumeration value="+1"/&amp;gt;
     *                       &amp;lt;enumeration value="+2"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ServiceDetails" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="MealService" maxOccurs="12" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                 &amp;lt;attribute name="DeniedBoardingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="FlightProductName" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                 &amp;lt;attribute name="InFlightServiceCodes" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TransitPoint" type="{http://www.opentravel.org/OTA/2003/05}LocationType" maxOccurs="16" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="CabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *       &amp;lt;attribute name="CabinName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "carrierInfo",
        "aircraftOwner",
        "departureInformation",
        "arrivalInformation",
        "serviceDetails",
        "transitPoint"
    })
    public static class FlightInfo {

        @XmlElement(name = "CarrierInfo", required = true)
        protected List<OperatingAirlineType> carrierInfo;
        @XmlElement(name = "AircraftOwner")
        protected CompanyNameType aircraftOwner;
        @XmlElement(name = "DepartureInformation", required = true)
        protected AirCheckInType.FlightInfo.DepartureInformation departureInformation;
        @XmlElement(name = "ArrivalInformation")
        protected AirCheckInType.FlightInfo.ArrivalInformation arrivalInformation;
        @XmlElement(name = "ServiceDetails")
        protected AirCheckInType.FlightInfo.ServiceDetails serviceDetails;
        @XmlElement(name = "TransitPoint")
        protected List<LocationType> transitPoint;
        @XmlAttribute(name = "RPH", required = true)
        protected String rph;
        @XmlAttribute(name = "CabinType")
        protected String cabinType;
        @XmlAttribute(name = "CabinName")
        protected String cabinName;

        /**
         * Gets the value of the carrierInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the carrierInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCarrierInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OperatingAirlineType }
         * 
         * 
         */
        public List<OperatingAirlineType> getCarrierInfo() {
            if (carrierInfo == null) {
                carrierInfo = new ArrayList<OperatingAirlineType>();
            }
            return this.carrierInfo;
        }

        /**
         * Obtiene el valor de la propiedad aircraftOwner.
         * 
         * @return
         *     possible object is
         *     {@link CompanyNameType }
         *     
         */
        public CompanyNameType getAircraftOwner() {
            return aircraftOwner;
        }

        /**
         * Define el valor de la propiedad aircraftOwner.
         * 
         * @param value
         *     allowed object is
         *     {@link CompanyNameType }
         *     
         */
        public void setAircraftOwner(CompanyNameType value) {
            this.aircraftOwner = value;
        }

        /**
         * Obtiene el valor de la propiedad departureInformation.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.FlightInfo.DepartureInformation }
         *     
         */
        public AirCheckInType.FlightInfo.DepartureInformation getDepartureInformation() {
            return departureInformation;
        }

        /**
         * Define el valor de la propiedad departureInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.FlightInfo.DepartureInformation }
         *     
         */
        public void setDepartureInformation(AirCheckInType.FlightInfo.DepartureInformation value) {
            this.departureInformation = value;
        }

        /**
         * Obtiene el valor de la propiedad arrivalInformation.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.FlightInfo.ArrivalInformation }
         *     
         */
        public AirCheckInType.FlightInfo.ArrivalInformation getArrivalInformation() {
            return arrivalInformation;
        }

        /**
         * Define el valor de la propiedad arrivalInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.FlightInfo.ArrivalInformation }
         *     
         */
        public void setArrivalInformation(AirCheckInType.FlightInfo.ArrivalInformation value) {
            this.arrivalInformation = value;
        }

        /**
         * Obtiene el valor de la propiedad serviceDetails.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.FlightInfo.ServiceDetails }
         *     
         */
        public AirCheckInType.FlightInfo.ServiceDetails getServiceDetails() {
            return serviceDetails;
        }

        /**
         * Define el valor de la propiedad serviceDetails.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.FlightInfo.ServiceDetails }
         *     
         */
        public void setServiceDetails(AirCheckInType.FlightInfo.ServiceDetails value) {
            this.serviceDetails = value;
        }

        /**
         * Gets the value of the transitPoint property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the transitPoint property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTransitPoint().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link LocationType }
         * 
         * 
         */
        public List<LocationType> getTransitPoint() {
            if (transitPoint == null) {
                transitPoint = new ArrayList<LocationType>();
            }
            return this.transitPoint;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad cabinType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCabinType() {
            return cabinType;
        }

        /**
         * Define el valor de la propiedad cabinType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCabinType(String value) {
            this.cabinType = value;
        }

        /**
         * Obtiene el valor de la propiedad cabinName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCabinName() {
            return cabinName;
        }

        /**
         * Define el valor de la propiedad cabinName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCabinName(String value) {
            this.cabinName = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
         *       &amp;lt;attribute name="DateOfArrival" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *       &amp;lt;attribute name="ScheduledArrivalTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *       &amp;lt;attribute name="DateChangeIdentifier"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="-1"/&amp;gt;
         *             &amp;lt;enumeration value="+1"/&amp;gt;
         *             &amp;lt;enumeration value="+2"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ArrivalInformation {

            @XmlAttribute(name = "DateOfArrival")
            protected String dateOfArrival;
            @XmlAttribute(name = "DayOfWeek")
            protected DayOfWeekType dayOfWeek;
            @XmlAttribute(name = "ScheduledArrivalTime")
            @XmlSchemaType(name = "time")
            protected XMLGregorianCalendar scheduledArrivalTime;
            @XmlAttribute(name = "DateChangeIdentifier")
            protected String dateChangeIdentifier;
            @XmlAttribute(name = "LocationName")
            protected String locationName;
            @XmlAttribute(name = "LocationCode")
            protected String locationCode;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "Terminal")
            protected String terminal;
            @XmlAttribute(name = "Gate")
            protected String gate;

            /**
             * Obtiene el valor de la propiedad dateOfArrival.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateOfArrival() {
                return dateOfArrival;
            }

            /**
             * Define el valor de la propiedad dateOfArrival.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateOfArrival(String value) {
                this.dateOfArrival = value;
            }

            /**
             * Obtiene el valor de la propiedad dayOfWeek.
             * 
             * @return
             *     possible object is
             *     {@link DayOfWeekType }
             *     
             */
            public DayOfWeekType getDayOfWeek() {
                return dayOfWeek;
            }

            /**
             * Define el valor de la propiedad dayOfWeek.
             * 
             * @param value
             *     allowed object is
             *     {@link DayOfWeekType }
             *     
             */
            public void setDayOfWeek(DayOfWeekType value) {
                this.dayOfWeek = value;
            }

            /**
             * Obtiene el valor de la propiedad scheduledArrivalTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getScheduledArrivalTime() {
                return scheduledArrivalTime;
            }

            /**
             * Define el valor de la propiedad scheduledArrivalTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setScheduledArrivalTime(XMLGregorianCalendar value) {
                this.scheduledArrivalTime = value;
            }

            /**
             * Obtiene el valor de la propiedad dateChangeIdentifier.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateChangeIdentifier() {
                return dateChangeIdentifier;
            }

            /**
             * Define el valor de la propiedad dateChangeIdentifier.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateChangeIdentifier(String value) {
                this.dateChangeIdentifier = value;
            }

            /**
             * Obtiene el valor de la propiedad locationName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationName() {
                return locationName;
            }

            /**
             * Define el valor de la propiedad locationName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationName(String value) {
                this.locationName = value;
            }

            /**
             * Obtiene el valor de la propiedad locationCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationCode() {
                return locationCode;
            }

            /**
             * Define el valor de la propiedad locationCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationCode(String value) {
                this.locationCode = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad terminal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTerminal() {
                return terminal;
            }

            /**
             * Define el valor de la propiedad terminal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTerminal(String value) {
                this.terminal = value;
            }

            /**
             * Obtiene el valor de la propiedad gate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGate() {
                return gate;
            }

            /**
             * Define el valor de la propiedad gate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGate(String value) {
                this.gate = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirportLocationGroup"/&amp;gt;
         *       &amp;lt;attribute name="DateOfDeparture" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="JulianDateOfDeparture"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *       &amp;lt;attribute name="ScheduledDepartureTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *       &amp;lt;attribute name="LatestCheckInDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="BoardingDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="OtherCheckInInformation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *       &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class DepartureInformation {

            @XmlAttribute(name = "DateOfDeparture", required = true)
            protected String dateOfDeparture;
            @XmlAttribute(name = "JulianDateOfDeparture")
            protected String julianDateOfDeparture;
            @XmlAttribute(name = "DayOfWeek")
            protected DayOfWeekType dayOfWeek;
            @XmlAttribute(name = "ScheduledDepartureTime")
            @XmlSchemaType(name = "time")
            protected XMLGregorianCalendar scheduledDepartureTime;
            @XmlAttribute(name = "LatestCheckInDateTime")
            protected String latestCheckInDateTime;
            @XmlAttribute(name = "BoardingDateTime")
            protected String boardingDateTime;
            @XmlAttribute(name = "OtherCheckInInformation")
            protected String otherCheckInInformation;
            @XmlAttribute(name = "LocationName")
            protected String locationName;
            @XmlAttribute(name = "LocationCode")
            protected String locationCode;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "Terminal")
            protected String terminal;
            @XmlAttribute(name = "Gate")
            protected String gate;

            /**
             * Obtiene el valor de la propiedad dateOfDeparture.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateOfDeparture() {
                return dateOfDeparture;
            }

            /**
             * Define el valor de la propiedad dateOfDeparture.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateOfDeparture(String value) {
                this.dateOfDeparture = value;
            }

            /**
             * Obtiene el valor de la propiedad julianDateOfDeparture.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getJulianDateOfDeparture() {
                return julianDateOfDeparture;
            }

            /**
             * Define el valor de la propiedad julianDateOfDeparture.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setJulianDateOfDeparture(String value) {
                this.julianDateOfDeparture = value;
            }

            /**
             * Obtiene el valor de la propiedad dayOfWeek.
             * 
             * @return
             *     possible object is
             *     {@link DayOfWeekType }
             *     
             */
            public DayOfWeekType getDayOfWeek() {
                return dayOfWeek;
            }

            /**
             * Define el valor de la propiedad dayOfWeek.
             * 
             * @param value
             *     allowed object is
             *     {@link DayOfWeekType }
             *     
             */
            public void setDayOfWeek(DayOfWeekType value) {
                this.dayOfWeek = value;
            }

            /**
             * Obtiene el valor de la propiedad scheduledDepartureTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getScheduledDepartureTime() {
                return scheduledDepartureTime;
            }

            /**
             * Define el valor de la propiedad scheduledDepartureTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setScheduledDepartureTime(XMLGregorianCalendar value) {
                this.scheduledDepartureTime = value;
            }

            /**
             * Obtiene el valor de la propiedad latestCheckInDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLatestCheckInDateTime() {
                return latestCheckInDateTime;
            }

            /**
             * Define el valor de la propiedad latestCheckInDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLatestCheckInDateTime(String value) {
                this.latestCheckInDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad boardingDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBoardingDateTime() {
                return boardingDateTime;
            }

            /**
             * Define el valor de la propiedad boardingDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBoardingDateTime(String value) {
                this.boardingDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad otherCheckInInformation.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherCheckInInformation() {
                return otherCheckInInformation;
            }

            /**
             * Define el valor de la propiedad otherCheckInInformation.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherCheckInInformation(String value) {
                this.otherCheckInInformation = value;
            }

            /**
             * Obtiene el valor de la propiedad locationName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationName() {
                return locationName;
            }

            /**
             * Define el valor de la propiedad locationName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationName(String value) {
                this.locationName = value;
            }

            /**
             * Obtiene el valor de la propiedad locationCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationCode() {
                return locationCode;
            }

            /**
             * Define el valor de la propiedad locationCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationCode(String value) {
                this.locationCode = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad terminal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTerminal() {
                return terminal;
            }

            /**
             * Define el valor de la propiedad terminal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTerminal(String value) {
                this.terminal = value;
            }

            /**
             * Obtiene el valor de la propiedad gate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGate() {
                return gate;
            }

            /**
             * Define el valor de la propiedad gate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGate(String value) {
                this.gate = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="MealService" maxOccurs="12" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *       &amp;lt;attribute name="DeniedBoardingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FlightProductName" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *       &amp;lt;attribute name="InFlightServiceCodes" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mealService"
        })
        public static class ServiceDetails {

            @XmlElement(name = "MealService")
            protected List<AirCheckInType.FlightInfo.ServiceDetails.MealService> mealService;
            @XmlAttribute(name = "DeniedBoardingIndicator")
            protected Boolean deniedBoardingIndicator;
            @XmlAttribute(name = "FlightProductName")
            protected String flightProductName;
            @XmlAttribute(name = "InFlightServiceCodes")
            protected List<String> inFlightServiceCodes;
            @XmlAttribute(name = "UnitOfMeasureQuantity")
            protected BigDecimal unitOfMeasureQuantity;
            @XmlAttribute(name = "UnitOfMeasure")
            protected String unitOfMeasure;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;

            /**
             * Gets the value of the mealService property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the mealService property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMealService().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AirCheckInType.FlightInfo.ServiceDetails.MealService }
             * 
             * 
             */
            public List<AirCheckInType.FlightInfo.ServiceDetails.MealService> getMealService() {
                if (mealService == null) {
                    mealService = new ArrayList<AirCheckInType.FlightInfo.ServiceDetails.MealService>();
                }
                return this.mealService;
            }

            /**
             * Obtiene el valor de la propiedad deniedBoardingIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDeniedBoardingIndicator() {
                return deniedBoardingIndicator;
            }

            /**
             * Define el valor de la propiedad deniedBoardingIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDeniedBoardingIndicator(Boolean value) {
                this.deniedBoardingIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad flightProductName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlightProductName() {
                return flightProductName;
            }

            /**
             * Define el valor de la propiedad flightProductName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlightProductName(String value) {
                this.flightProductName = value;
            }

            /**
             * Gets the value of the inFlightServiceCodes property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the inFlightServiceCodes property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getInFlightServiceCodes().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getInFlightServiceCodes() {
                if (inFlightServiceCodes == null) {
                    inFlightServiceCodes = new ArrayList<String>();
                }
                return this.inFlightServiceCodes;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getUnitOfMeasureQuantity() {
                return unitOfMeasureQuantity;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setUnitOfMeasureQuantity(BigDecimal value) {
                this.unitOfMeasureQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasure.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasure() {
                return unitOfMeasure;
            }

            /**
             * Define el valor de la propiedad unitOfMeasure.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasure(String value) {
                this.unitOfMeasure = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MealService {

                @XmlAttribute(name = "Code", required = true)
                protected String code;

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Function" use="required"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="SelectCheckInSegments"/&amp;gt;
     *             &amp;lt;enumeration value="SelectPassengersToCheckIn"/&amp;gt;
     *             &amp;lt;enumeration value="AssignSeat"/&amp;gt;
     *             &amp;lt;enumeration value="ChangeSeat"/&amp;gt;
     *             &amp;lt;enumeration value="RequestUpgrade"/&amp;gt;
     *             &amp;lt;enumeration value="EnterFrequentFlyerInfoBySegment"/&amp;gt;
     *             &amp;lt;enumeration value="ListForStandby"/&amp;gt;
     *             &amp;lt;enumeration value="RequestForAlternateFlight"/&amp;gt;
     *             &amp;lt;enumeration value="EnterAllowableSSR_OSI_Information"/&amp;gt;
     *             &amp;lt;enumeration value="EnterAPIS_Information"/&amp;gt;
     *             &amp;lt;enumeration value="EnterEmergencyContactInformation"/&amp;gt;
     *             &amp;lt;enumeration value="EnterBaggageInformation"/&amp;gt;
     *             &amp;lt;enumeration value="RedisplayEnteredInformationAndStatusOfCheckIn"/&amp;gt;
     *             &amp;lt;enumeration value="VolunteerForDeniedBoarding"/&amp;gt;
     *             &amp;lt;enumeration value="IssueBagTag"/&amp;gt;
     *             &amp;lt;enumeration value="ReprintBagTag"/&amp;gt;
     *             &amp;lt;enumeration value="IssueBoardingPass"/&amp;gt;
     *             &amp;lt;enumeration value="ReprintBoardingPass"/&amp;gt;
     *             &amp;lt;enumeration value="PrintItinerary"/&amp;gt;
     *             &amp;lt;enumeration value="PrintReceipt"/&amp;gt;
     *             &amp;lt;enumeration value="PrintStandbyCard"/&amp;gt;
     *             &amp;lt;enumeration value="ReprintStandbyCard"/&amp;gt;
     *             &amp;lt;enumeration value="CancelCheckInDuringProcess"/&amp;gt;
     *             &amp;lt;enumeration value="CheckIn"/&amp;gt;
     *             &amp;lt;enumeration value="ValidatePassengerSeat"/&amp;gt;
     *             &amp;lt;enumeration value="Unseat"/&amp;gt;
     *             &amp;lt;enumeration value="ChangeSeatReaccommodation"/&amp;gt;
     *             &amp;lt;enumeration value="UpdatePassengerInfo"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MessageFunction {

        @XmlAttribute(name = "Function", required = true)
        protected String function;

        /**
         * Obtiene el valor de la propiedad function.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFunction() {
            return function;
        }

        /**
         * Define el valor de la propiedad function.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFunction(String value) {
            this.function = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BookingInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="BookingReferenceID" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="StatusCode"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[A-Z0-9a-z]{2}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="StatusName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                 &amp;lt;attribute name="OriginalResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                 &amp;lt;attribute name="UpgradeResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="SeatBoardingInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="BoardingPriority" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="BoardingZone"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9a-zA-Z]{1,9}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="SeatCharacteristics" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="SeatNumber"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9]{1,3}[A-Z]{1}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="NonRevCategory"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="NRPS"/&amp;gt;
     *                       &amp;lt;enumeration value="NRSA"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Fee" type="{http://www.opentravel.org/OTA/2003/05}FeeType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="SpecialServiceRequest" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="API_Info" maxOccurs="10" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DocumentType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Address" maxOccurs="2" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="API_InfoSource" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="BirthStateProv" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
     *                 &amp;lt;attribute name="BirthLocation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="InternationalProcessingInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="DocumentVerification" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
     *                 &amp;lt;attribute name="AirportPassengerProcessingCode"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9]{2}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="DocumentVerifiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="FrequentTravelerInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="PassengerRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="FlightRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="BaggageCabinType" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *       &amp;lt;attribute name="SpecialPurposeCode"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[A-Z]{1,4}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="DeniedBoardingVolunteerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ShortCheckInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NonRevAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PassengerCouponStatus" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
     *       &amp;lt;attribute name="DCS_SequenceNumber"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[A-Za-z0-9]{1,5}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="DCS_PassengerRefNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="BoardingPassColor" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="StandbyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bookingInfo",
        "seatBoardingInfo",
        "fee",
        "specialServiceRequest",
        "apiInfo",
        "internationalProcessingInfo",
        "frequentTravelerInfo"
    })
    public static class PassengerFlightInfo {

        @XmlElement(name = "BookingInfo")
        protected AirCheckInType.PassengerFlightInfo.BookingInfo bookingInfo;
        @XmlElement(name = "SeatBoardingInfo")
        protected AirCheckInType.PassengerFlightInfo.SeatBoardingInfo seatBoardingInfo;
        @XmlElement(name = "Fee")
        protected List<FeeType> fee;
        @XmlElement(name = "SpecialServiceRequest")
        protected List<SpecialServiceRequestType> specialServiceRequest;
        @XmlElement(name = "API_Info")
        protected List<AirCheckInType.PassengerFlightInfo.APIInfo> apiInfo;
        @XmlElement(name = "InternationalProcessingInfo")
        protected AirCheckInType.PassengerFlightInfo.InternationalProcessingInfo internationalProcessingInfo;
        @XmlElement(name = "FrequentTravelerInfo")
        protected AirCheckInType.PassengerFlightInfo.FrequentTravelerInfo frequentTravelerInfo;
        @XmlAttribute(name = "PassengerRPH", required = true)
        protected String passengerRPH;
        @XmlAttribute(name = "FlightRPH", required = true)
        protected String flightRPH;
        @XmlAttribute(name = "BaggageCabinType")
        protected String baggageCabinType;
        @XmlAttribute(name = "SpecialPurposeCode")
        protected String specialPurposeCode;
        @XmlAttribute(name = "DeniedBoardingVolunteerInd")
        protected Boolean deniedBoardingVolunteerInd;
        @XmlAttribute(name = "ShortCheckInd")
        protected Boolean shortCheckInd;
        @XmlAttribute(name = "NonRevAuthorizedInd")
        protected Boolean nonRevAuthorizedInd;
        @XmlAttribute(name = "PassengerCouponStatus")
        protected String passengerCouponStatus;
        @XmlAttribute(name = "DCS_SequenceNumber")
        protected String dcsSequenceNumber;
        @XmlAttribute(name = "DCS_PassengerRefNumber")
        protected String dcsPassengerRefNumber;
        @XmlAttribute(name = "BoardingPassColor")
        protected String boardingPassColor;
        @XmlAttribute(name = "StandbyIndicator")
        protected Boolean standbyIndicator;

        /**
         * Obtiene el valor de la propiedad bookingInfo.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.PassengerFlightInfo.BookingInfo }
         *     
         */
        public AirCheckInType.PassengerFlightInfo.BookingInfo getBookingInfo() {
            return bookingInfo;
        }

        /**
         * Define el valor de la propiedad bookingInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.PassengerFlightInfo.BookingInfo }
         *     
         */
        public void setBookingInfo(AirCheckInType.PassengerFlightInfo.BookingInfo value) {
            this.bookingInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad seatBoardingInfo.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.PassengerFlightInfo.SeatBoardingInfo }
         *     
         */
        public AirCheckInType.PassengerFlightInfo.SeatBoardingInfo getSeatBoardingInfo() {
            return seatBoardingInfo;
        }

        /**
         * Define el valor de la propiedad seatBoardingInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.PassengerFlightInfo.SeatBoardingInfo }
         *     
         */
        public void setSeatBoardingInfo(AirCheckInType.PassengerFlightInfo.SeatBoardingInfo value) {
            this.seatBoardingInfo = value;
        }

        /**
         * Gets the value of the fee property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fee property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFee().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FeeType }
         * 
         * 
         */
        public List<FeeType> getFee() {
            if (fee == null) {
                fee = new ArrayList<FeeType>();
            }
            return this.fee;
        }

        /**
         * Gets the value of the specialServiceRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specialServiceRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSpecialServiceRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link SpecialServiceRequestType }
         * 
         * 
         */
        public List<SpecialServiceRequestType> getSpecialServiceRequest() {
            if (specialServiceRequest == null) {
                specialServiceRequest = new ArrayList<SpecialServiceRequestType>();
            }
            return this.specialServiceRequest;
        }

        /**
         * Gets the value of the apiInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the apiInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAPIInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirCheckInType.PassengerFlightInfo.APIInfo }
         * 
         * 
         */
        public List<AirCheckInType.PassengerFlightInfo.APIInfo> getAPIInfo() {
            if (apiInfo == null) {
                apiInfo = new ArrayList<AirCheckInType.PassengerFlightInfo.APIInfo>();
            }
            return this.apiInfo;
        }

        /**
         * Obtiene el valor de la propiedad internationalProcessingInfo.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.PassengerFlightInfo.InternationalProcessingInfo }
         *     
         */
        public AirCheckInType.PassengerFlightInfo.InternationalProcessingInfo getInternationalProcessingInfo() {
            return internationalProcessingInfo;
        }

        /**
         * Define el valor de la propiedad internationalProcessingInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.PassengerFlightInfo.InternationalProcessingInfo }
         *     
         */
        public void setInternationalProcessingInfo(AirCheckInType.PassengerFlightInfo.InternationalProcessingInfo value) {
            this.internationalProcessingInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad frequentTravelerInfo.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.PassengerFlightInfo.FrequentTravelerInfo }
         *     
         */
        public AirCheckInType.PassengerFlightInfo.FrequentTravelerInfo getFrequentTravelerInfo() {
            return frequentTravelerInfo;
        }

        /**
         * Define el valor de la propiedad frequentTravelerInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.PassengerFlightInfo.FrequentTravelerInfo }
         *     
         */
        public void setFrequentTravelerInfo(AirCheckInType.PassengerFlightInfo.FrequentTravelerInfo value) {
            this.frequentTravelerInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassengerRPH() {
            return passengerRPH;
        }

        /**
         * Define el valor de la propiedad passengerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassengerRPH(String value) {
            this.passengerRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad flightRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightRPH() {
            return flightRPH;
        }

        /**
         * Define el valor de la propiedad flightRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightRPH(String value) {
            this.flightRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad baggageCabinType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBaggageCabinType() {
            return baggageCabinType;
        }

        /**
         * Define el valor de la propiedad baggageCabinType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBaggageCabinType(String value) {
            this.baggageCabinType = value;
        }

        /**
         * Obtiene el valor de la propiedad specialPurposeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSpecialPurposeCode() {
            return specialPurposeCode;
        }

        /**
         * Define el valor de la propiedad specialPurposeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSpecialPurposeCode(String value) {
            this.specialPurposeCode = value;
        }

        /**
         * Obtiene el valor de la propiedad deniedBoardingVolunteerInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDeniedBoardingVolunteerInd() {
            return deniedBoardingVolunteerInd;
        }

        /**
         * Define el valor de la propiedad deniedBoardingVolunteerInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDeniedBoardingVolunteerInd(Boolean value) {
            this.deniedBoardingVolunteerInd = value;
        }

        /**
         * Obtiene el valor de la propiedad shortCheckInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isShortCheckInd() {
            return shortCheckInd;
        }

        /**
         * Define el valor de la propiedad shortCheckInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setShortCheckInd(Boolean value) {
            this.shortCheckInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonRevAuthorizedInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonRevAuthorizedInd() {
            return nonRevAuthorizedInd;
        }

        /**
         * Define el valor de la propiedad nonRevAuthorizedInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonRevAuthorizedInd(Boolean value) {
            this.nonRevAuthorizedInd = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerCouponStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassengerCouponStatus() {
            return passengerCouponStatus;
        }

        /**
         * Define el valor de la propiedad passengerCouponStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassengerCouponStatus(String value) {
            this.passengerCouponStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad dcsSequenceNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDCSSequenceNumber() {
            return dcsSequenceNumber;
        }

        /**
         * Define el valor de la propiedad dcsSequenceNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDCSSequenceNumber(String value) {
            this.dcsSequenceNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad dcsPassengerRefNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDCSPassengerRefNumber() {
            return dcsPassengerRefNumber;
        }

        /**
         * Define el valor de la propiedad dcsPassengerRefNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDCSPassengerRefNumber(String value) {
            this.dcsPassengerRefNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad boardingPassColor.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBoardingPassColor() {
            return boardingPassColor;
        }

        /**
         * Define el valor de la propiedad boardingPassColor.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBoardingPassColor(String value) {
            this.boardingPassColor = value;
        }

        /**
         * Obtiene el valor de la propiedad standbyIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isStandbyIndicator() {
            return standbyIndicator;
        }

        /**
         * Define el valor de la propiedad standbyIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setStandbyIndicator(Boolean value) {
            this.standbyIndicator = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DocumentType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Address" maxOccurs="2" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="API_InfoSource" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="BirthStateProv" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
         *       &amp;lt;attribute name="BirthLocation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "address"
        })
        public static class APIInfo
            extends DocumentType
        {

            @XmlElement(name = "Address")
            protected List<AirCheckInType.PassengerFlightInfo.APIInfo.Address> address;
            @XmlAttribute(name = "API_InfoSource")
            protected String apiInfoSource;
            @XmlAttribute(name = "InfantInd")
            protected Boolean infantInd;
            @XmlAttribute(name = "BirthStateProv")
            protected String birthStateProv;
            @XmlAttribute(name = "BirthLocation")
            protected String birthLocation;

            /**
             * Gets the value of the address property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the address property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAddress().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AirCheckInType.PassengerFlightInfo.APIInfo.Address }
             * 
             * 
             */
            public List<AirCheckInType.PassengerFlightInfo.APIInfo.Address> getAddress() {
                if (address == null) {
                    address = new ArrayList<AirCheckInType.PassengerFlightInfo.APIInfo.Address>();
                }
                return this.address;
            }

            /**
             * Obtiene el valor de la propiedad apiInfoSource.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAPIInfoSource() {
                return apiInfoSource;
            }

            /**
             * Define el valor de la propiedad apiInfoSource.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAPIInfoSource(String value) {
                this.apiInfoSource = value;
            }

            /**
             * Obtiene el valor de la propiedad infantInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isInfantInd() {
                return infantInd;
            }

            /**
             * Define el valor de la propiedad infantInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setInfantInd(Boolean value) {
                this.infantInd = value;
            }

            /**
             * Obtiene el valor de la propiedad birthStateProv.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthStateProv() {
                return birthStateProv;
            }

            /**
             * Define el valor de la propiedad birthStateProv.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthStateProv(String value) {
                this.birthStateProv = value;
            }

            /**
             * Obtiene el valor de la propiedad birthLocation.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthLocation() {
                return birthLocation;
            }

            /**
             * Define el valor de la propiedad birthLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthLocation(String value) {
                this.birthLocation = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Address
                extends AddressType
            {


            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="BookingReferenceID" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="StatusCode"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[A-Z0-9a-z]{2}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="StatusName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *       &amp;lt;attribute name="OriginalResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *       &amp;lt;attribute name="UpgradeResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "bookingReferenceID"
        })
        public static class BookingInfo {

            @XmlElement(name = "BookingReferenceID")
            protected List<AirCheckInType.PassengerFlightInfo.BookingInfo.BookingReferenceID> bookingReferenceID;
            @XmlAttribute(name = "StatusCode")
            protected String statusCode;
            @XmlAttribute(name = "StatusName")
            protected String statusName;
            @XmlAttribute(name = "ResBookDesigCode")
            protected String resBookDesigCode;
            @XmlAttribute(name = "OriginalResBookDesigCode")
            protected String originalResBookDesigCode;
            @XmlAttribute(name = "UpgradeResBookDesigCode")
            protected String upgradeResBookDesigCode;

            /**
             * Gets the value of the bookingReferenceID property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingReferenceID property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBookingReferenceID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AirCheckInType.PassengerFlightInfo.BookingInfo.BookingReferenceID }
             * 
             * 
             */
            public List<AirCheckInType.PassengerFlightInfo.BookingInfo.BookingReferenceID> getBookingReferenceID() {
                if (bookingReferenceID == null) {
                    bookingReferenceID = new ArrayList<AirCheckInType.PassengerFlightInfo.BookingInfo.BookingReferenceID>();
                }
                return this.bookingReferenceID;
            }

            /**
             * Obtiene el valor de la propiedad statusCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusCode() {
                return statusCode;
            }

            /**
             * Define el valor de la propiedad statusCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusCode(String value) {
                this.statusCode = value;
            }

            /**
             * Obtiene el valor de la propiedad statusName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusName() {
                return statusName;
            }

            /**
             * Define el valor de la propiedad statusName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusName(String value) {
                this.statusName = value;
            }

            /**
             * Obtiene el valor de la propiedad resBookDesigCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResBookDesigCode() {
                return resBookDesigCode;
            }

            /**
             * Define el valor de la propiedad resBookDesigCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResBookDesigCode(String value) {
                this.resBookDesigCode = value;
            }

            /**
             * Obtiene el valor de la propiedad originalResBookDesigCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOriginalResBookDesigCode() {
                return originalResBookDesigCode;
            }

            /**
             * Define el valor de la propiedad originalResBookDesigCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOriginalResBookDesigCode(String value) {
                this.originalResBookDesigCode = value;
            }

            /**
             * Obtiene el valor de la propiedad upgradeResBookDesigCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUpgradeResBookDesigCode() {
                return upgradeResBookDesigCode;
            }

            /**
             * Define el valor de la propiedad upgradeResBookDesigCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUpgradeResBookDesigCode(String value) {
                this.upgradeResBookDesigCode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class BookingReferenceID
                extends UniqueIDType
            {


            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class FrequentTravelerInfo {

            @XmlAttribute(name = "ShareSynchInd")
            protected String shareSynchInd;
            @XmlAttribute(name = "ShareMarketInd")
            protected String shareMarketInd;
            @XmlAttribute(name = "ProgramID")
            protected String programID;
            @XmlAttribute(name = "MembershipID")
            protected String membershipID;
            @XmlAttribute(name = "TravelSector")
            protected String travelSector;
            @XmlAttribute(name = "VendorCode")
            protected List<String> vendorCode;
            @XmlAttribute(name = "PrimaryLoyaltyIndicator")
            protected Boolean primaryLoyaltyIndicator;
            @XmlAttribute(name = "AllianceLoyaltyLevelName")
            protected String allianceLoyaltyLevelName;
            @XmlAttribute(name = "CustomerType")
            protected String customerType;
            @XmlAttribute(name = "CustomerValue")
            protected String customerValue;
            @XmlAttribute(name = "Password")
            protected String password;
            @XmlAttribute(name = "LoyalLevel")
            protected String loyalLevel;
            @XmlAttribute(name = "LoyalLevelCode")
            protected Integer loyalLevelCode;
            @XmlAttribute(name = "SingleVendorInd")
            protected String singleVendorInd;
            @XmlAttribute(name = "SignupDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar signupDate;
            @XmlAttribute(name = "EffectiveDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar effectiveDate;
            @XmlAttribute(name = "ExpireDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar expireDate;
            @XmlAttribute(name = "ExpireDateExclusiveIndicator")
            protected Boolean expireDateExclusiveIndicator;
            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Obtiene el valor de la propiedad shareSynchInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareSynchInd() {
                return shareSynchInd;
            }

            /**
             * Define el valor de la propiedad shareSynchInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareSynchInd(String value) {
                this.shareSynchInd = value;
            }

            /**
             * Obtiene el valor de la propiedad shareMarketInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareMarketInd() {
                return shareMarketInd;
            }

            /**
             * Define el valor de la propiedad shareMarketInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareMarketInd(String value) {
                this.shareMarketInd = value;
            }

            /**
             * Obtiene el valor de la propiedad programID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramID() {
                return programID;
            }

            /**
             * Define el valor de la propiedad programID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramID(String value) {
                this.programID = value;
            }

            /**
             * Obtiene el valor de la propiedad membershipID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMembershipID() {
                return membershipID;
            }

            /**
             * Define el valor de la propiedad membershipID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMembershipID(String value) {
                this.membershipID = value;
            }

            /**
             * Obtiene el valor de la propiedad travelSector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelSector() {
                return travelSector;
            }

            /**
             * Define el valor de la propiedad travelSector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelSector(String value) {
                this.travelSector = value;
            }

            /**
             * Gets the value of the vendorCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getVendorCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getVendorCode() {
                if (vendorCode == null) {
                    vendorCode = new ArrayList<String>();
                }
                return this.vendorCode;
            }

            /**
             * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPrimaryLoyaltyIndicator() {
                return primaryLoyaltyIndicator;
            }

            /**
             * Define el valor de la propiedad primaryLoyaltyIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPrimaryLoyaltyIndicator(Boolean value) {
                this.primaryLoyaltyIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAllianceLoyaltyLevelName() {
                return allianceLoyaltyLevelName;
            }

            /**
             * Define el valor de la propiedad allianceLoyaltyLevelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAllianceLoyaltyLevelName(String value) {
                this.allianceLoyaltyLevelName = value;
            }

            /**
             * Obtiene el valor de la propiedad customerType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerType() {
                return customerType;
            }

            /**
             * Define el valor de la propiedad customerType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerType(String value) {
                this.customerType = value;
            }

            /**
             * Obtiene el valor de la propiedad customerValue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerValue() {
                return customerValue;
            }

            /**
             * Define el valor de la propiedad customerValue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerValue(String value) {
                this.customerValue = value;
            }

            /**
             * Obtiene el valor de la propiedad password.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassword() {
                return password;
            }

            /**
             * Define el valor de la propiedad password.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassword(String value) {
                this.password = value;
            }

            /**
             * Obtiene el valor de la propiedad loyalLevel.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLoyalLevel() {
                return loyalLevel;
            }

            /**
             * Define el valor de la propiedad loyalLevel.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLoyalLevel(String value) {
                this.loyalLevel = value;
            }

            /**
             * Obtiene el valor de la propiedad loyalLevelCode.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getLoyalLevelCode() {
                return loyalLevelCode;
            }

            /**
             * Define el valor de la propiedad loyalLevelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setLoyalLevelCode(Integer value) {
                this.loyalLevelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad singleVendorInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSingleVendorInd() {
                return singleVendorInd;
            }

            /**
             * Define el valor de la propiedad singleVendorInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSingleVendorInd(String value) {
                this.singleVendorInd = value;
            }

            /**
             * Obtiene el valor de la propiedad signupDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getSignupDate() {
                return signupDate;
            }

            /**
             * Define el valor de la propiedad signupDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setSignupDate(XMLGregorianCalendar value) {
                this.signupDate = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEffectiveDate() {
                return effectiveDate;
            }

            /**
             * Define el valor de la propiedad effectiveDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEffectiveDate(XMLGregorianCalendar value) {
                this.effectiveDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpireDate() {
                return expireDate;
            }

            /**
             * Define el valor de la propiedad expireDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpireDate(XMLGregorianCalendar value) {
                this.expireDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExpireDateExclusiveIndicator() {
                return expireDateExclusiveIndicator;
            }

            /**
             * Define el valor de la propiedad expireDateExclusiveIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExpireDateExclusiveIndicator(Boolean value) {
                this.expireDateExclusiveIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="DocumentVerification" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
         *       &amp;lt;attribute name="AirportPassengerProcessingCode"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{2}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="DocumentVerifiedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class InternationalProcessingInfo {

            @XmlAttribute(name = "DocumentVerification")
            protected String documentVerification;
            @XmlAttribute(name = "AirportPassengerProcessingCode")
            protected String airportPassengerProcessingCode;
            @XmlAttribute(name = "DocumentVerifiedInd")
            protected Boolean documentVerifiedInd;

            /**
             * Obtiene el valor de la propiedad documentVerification.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocumentVerification() {
                return documentVerification;
            }

            /**
             * Define el valor de la propiedad documentVerification.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocumentVerification(String value) {
                this.documentVerification = value;
            }

            /**
             * Obtiene el valor de la propiedad airportPassengerProcessingCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAirportPassengerProcessingCode() {
                return airportPassengerProcessingCode;
            }

            /**
             * Define el valor de la propiedad airportPassengerProcessingCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAirportPassengerProcessingCode(String value) {
                this.airportPassengerProcessingCode = value;
            }

            /**
             * Obtiene el valor de la propiedad documentVerifiedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDocumentVerifiedInd() {
                return documentVerifiedInd;
            }

            /**
             * Define el valor de la propiedad documentVerifiedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDocumentVerifiedInd(Boolean value) {
                this.documentVerifiedInd = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="BoardingPriority" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="BoardingZone"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9a-zA-Z]{1,9}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="SeatCharacteristics" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="SeatNumber"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{1,3}[A-Z]{1}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="NonRevCategory"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="NRPS"/&amp;gt;
         *             &amp;lt;enumeration value="NRSA"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SeatBoardingInfo {

            @XmlAttribute(name = "BoardingPriority")
            protected String boardingPriority;
            @XmlAttribute(name = "BoardingZone")
            protected String boardingZone;
            @XmlAttribute(name = "SeatCharacteristics")
            protected List<String> seatCharacteristics;
            @XmlAttribute(name = "SeatNumber")
            protected String seatNumber;
            @XmlAttribute(name = "NonRevCategory")
            protected String nonRevCategory;

            /**
             * Obtiene el valor de la propiedad boardingPriority.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBoardingPriority() {
                return boardingPriority;
            }

            /**
             * Define el valor de la propiedad boardingPriority.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBoardingPriority(String value) {
                this.boardingPriority = value;
            }

            /**
             * Obtiene el valor de la propiedad boardingZone.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBoardingZone() {
                return boardingZone;
            }

            /**
             * Define el valor de la propiedad boardingZone.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBoardingZone(String value) {
                this.boardingZone = value;
            }

            /**
             * Gets the value of the seatCharacteristics property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the seatCharacteristics property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSeatCharacteristics().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getSeatCharacteristics() {
                if (seatCharacteristics == null) {
                    seatCharacteristics = new ArrayList<String>();
                }
                return this.seatCharacteristics;
            }

            /**
             * Obtiene el valor de la propiedad seatNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSeatNumber() {
                return seatNumber;
            }

            /**
             * Define el valor de la propiedad seatNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSeatNumber(String value) {
                this.seatNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad nonRevCategory.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNonRevCategory() {
                return nonRevCategory;
            }

            /**
             * Define el valor de la propiedad nonRevCategory.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNonRevCategory(String value) {
                this.nonRevCategory = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PassengerName" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PassengerWeight" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="OtherServiceInformation" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ContactInfo" maxOccurs="2" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Relationship" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="OptionToDecline" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PassengerType" maxOccurs="9" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GenderGroup"/&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TicketingInfo" maxOccurs="4" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CouponInfo" maxOccurs="16"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}FareBasisCodeType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="FlightRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *                           &amp;lt;attribute name="CouponNumber" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
     *                           &amp;lt;attribute name="DocumentNumberCheckDigit"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="PaperTicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="PricingInfo" type="{http://www.opentravel.org/OTA/2003/05}FareType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="PaymentTotal" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="FormOfPayment" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" maxOccurs="4" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CityCodeOfIssue" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="ACN_Number" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *                 &amp;lt;attribute name="AirlineAccountingCode" use="required"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="FormAndSerialNumber" use="required"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9a-zA-Z]{10}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="AgencyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="CountryCodeOfIssue" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *                 &amp;lt;attribute name="IssuingCompanyName"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9a-zA-Z]{1,35}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="EndorsementInfo" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="DateOfIssue" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="OriginalDateofIssue" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="TicketBookReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="PassengerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="JumpSeatAuthority" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="SecurityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="GroupReference" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="AddToStandbyDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="InfantIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EmployeeSeniorityDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="EmployeeLengthOfService" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *       &amp;lt;attribute name="PassengerAssociationID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "passengerName",
        "passengerWeight",
        "otherServiceInformation",
        "contactInfo",
        "passengerType",
        "ticketingInfo"
    })
    public static class PassengerInfo {

        @XmlElement(name = "PassengerName")
        protected PersonNameType passengerName;
        @XmlElement(name = "PassengerWeight")
        protected AirCheckInType.PassengerInfo.PassengerWeight passengerWeight;
        @XmlElement(name = "OtherServiceInformation")
        protected List<OtherServiceInfoType> otherServiceInformation;
        @XmlElement(name = "ContactInfo")
        protected List<AirCheckInType.PassengerInfo.ContactInfo> contactInfo;
        @XmlElement(name = "PassengerType")
        protected List<AirCheckInType.PassengerInfo.PassengerType> passengerType;
        @XmlElement(name = "TicketingInfo")
        protected List<AirCheckInType.PassengerInfo.TicketingInfo> ticketingInfo;
        @XmlAttribute(name = "RPH", required = true)
        protected String rph;
        @XmlAttribute(name = "PassengerRPH")
        protected String passengerRPH;
        @XmlAttribute(name = "JumpSeatAuthority")
        protected String jumpSeatAuthority;
        @XmlAttribute(name = "SecurityCode")
        protected String securityCode;
        @XmlAttribute(name = "GroupReference")
        protected String groupReference;
        @XmlAttribute(name = "AddToStandbyDateTime")
        protected String addToStandbyDateTime;
        @XmlAttribute(name = "InfantIndicator")
        protected Boolean infantIndicator;
        @XmlAttribute(name = "EmployeeSeniorityDate")
        protected String employeeSeniorityDate;
        @XmlAttribute(name = "EmployeeLengthOfService")
        protected Duration employeeLengthOfService;
        @XmlAttribute(name = "PassengerAssociationID")
        protected String passengerAssociationID;

        /**
         * Obtiene el valor de la propiedad passengerName.
         * 
         * @return
         *     possible object is
         *     {@link PersonNameType }
         *     
         */
        public PersonNameType getPassengerName() {
            return passengerName;
        }

        /**
         * Define el valor de la propiedad passengerName.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonNameType }
         *     
         */
        public void setPassengerName(PersonNameType value) {
            this.passengerName = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerWeight.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.PassengerInfo.PassengerWeight }
         *     
         */
        public AirCheckInType.PassengerInfo.PassengerWeight getPassengerWeight() {
            return passengerWeight;
        }

        /**
         * Define el valor de la propiedad passengerWeight.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.PassengerInfo.PassengerWeight }
         *     
         */
        public void setPassengerWeight(AirCheckInType.PassengerInfo.PassengerWeight value) {
            this.passengerWeight = value;
        }

        /**
         * Gets the value of the otherServiceInformation property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the otherServiceInformation property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOtherServiceInformation().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OtherServiceInfoType }
         * 
         * 
         */
        public List<OtherServiceInfoType> getOtherServiceInformation() {
            if (otherServiceInformation == null) {
                otherServiceInformation = new ArrayList<OtherServiceInfoType>();
            }
            return this.otherServiceInformation;
        }

        /**
         * Gets the value of the contactInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contactInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getContactInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirCheckInType.PassengerInfo.ContactInfo }
         * 
         * 
         */
        public List<AirCheckInType.PassengerInfo.ContactInfo> getContactInfo() {
            if (contactInfo == null) {
                contactInfo = new ArrayList<AirCheckInType.PassengerInfo.ContactInfo>();
            }
            return this.contactInfo;
        }

        /**
         * Gets the value of the passengerType property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerType property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPassengerType().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirCheckInType.PassengerInfo.PassengerType }
         * 
         * 
         */
        public List<AirCheckInType.PassengerInfo.PassengerType> getPassengerType() {
            if (passengerType == null) {
                passengerType = new ArrayList<AirCheckInType.PassengerInfo.PassengerType>();
            }
            return this.passengerType;
        }

        /**
         * Gets the value of the ticketingInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ticketingInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTicketingInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirCheckInType.PassengerInfo.TicketingInfo }
         * 
         * 
         */
        public List<AirCheckInType.PassengerInfo.TicketingInfo> getTicketingInfo() {
            if (ticketingInfo == null) {
                ticketingInfo = new ArrayList<AirCheckInType.PassengerInfo.TicketingInfo>();
            }
            return this.ticketingInfo;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassengerRPH() {
            return passengerRPH;
        }

        /**
         * Define el valor de la propiedad passengerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassengerRPH(String value) {
            this.passengerRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad jumpSeatAuthority.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJumpSeatAuthority() {
            return jumpSeatAuthority;
        }

        /**
         * Define el valor de la propiedad jumpSeatAuthority.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJumpSeatAuthority(String value) {
            this.jumpSeatAuthority = value;
        }

        /**
         * Obtiene el valor de la propiedad securityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSecurityCode() {
            return securityCode;
        }

        /**
         * Define el valor de la propiedad securityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSecurityCode(String value) {
            this.securityCode = value;
        }

        /**
         * Obtiene el valor de la propiedad groupReference.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupReference() {
            return groupReference;
        }

        /**
         * Define el valor de la propiedad groupReference.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupReference(String value) {
            this.groupReference = value;
        }

        /**
         * Obtiene el valor de la propiedad addToStandbyDateTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddToStandbyDateTime() {
            return addToStandbyDateTime;
        }

        /**
         * Define el valor de la propiedad addToStandbyDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddToStandbyDateTime(String value) {
            this.addToStandbyDateTime = value;
        }

        /**
         * Obtiene el valor de la propiedad infantIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isInfantIndicator() {
            return infantIndicator;
        }

        /**
         * Define el valor de la propiedad infantIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setInfantIndicator(Boolean value) {
            this.infantIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad employeeSeniorityDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmployeeSeniorityDate() {
            return employeeSeniorityDate;
        }

        /**
         * Define el valor de la propiedad employeeSeniorityDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmployeeSeniorityDate(String value) {
            this.employeeSeniorityDate = value;
        }

        /**
         * Obtiene el valor de la propiedad employeeLengthOfService.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getEmployeeLengthOfService() {
            return employeeLengthOfService;
        }

        /**
         * Define el valor de la propiedad employeeLengthOfService.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setEmployeeLengthOfService(Duration value) {
            this.employeeLengthOfService = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerAssociationID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassengerAssociationID() {
            return passengerAssociationID;
        }

        /**
         * Define el valor de la propiedad passengerAssociationID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassengerAssociationID(String value) {
            this.passengerAssociationID = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Relationship" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="OptionToDecline" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "relationship"
        })
        public static class ContactInfo
            extends ContactPersonType
        {

            @XmlElement(name = "Relationship", required = true)
            protected String relationship;
            @XmlAttribute(name = "OptionToDecline")
            protected Boolean optionToDecline;

            /**
             * Obtiene el valor de la propiedad relationship.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationship() {
                return relationship;
            }

            /**
             * Define el valor de la propiedad relationship.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationship(String value) {
                this.relationship = value;
            }

            /**
             * Obtiene el valor de la propiedad optionToDecline.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isOptionToDecline() {
                return optionToDecline;
            }

            /**
             * Define el valor de la propiedad optionToDecline.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setOptionToDecline(Boolean value) {
                this.optionToDecline = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GenderGroup"/&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PassengerType
            extends PassengerTypeQuantityType
        {

            @XmlAttribute(name = "Gender")
            protected String gender;

            /**
             * Obtiene el valor de la propiedad gender.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGender() {
                return gender;
            }

            /**
             * Define el valor de la propiedad gender.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGender(String value) {
                this.gender = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PassengerWeight {

            @XmlAttribute(name = "UnitOfMeasureQuantity")
            protected BigDecimal unitOfMeasureQuantity;
            @XmlAttribute(name = "UnitOfMeasure")
            protected String unitOfMeasure;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;

            /**
             * Obtiene el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getUnitOfMeasureQuantity() {
                return unitOfMeasureQuantity;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setUnitOfMeasureQuantity(BigDecimal value) {
                this.unitOfMeasureQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasure.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasure() {
                return unitOfMeasure;
            }

            /**
             * Define el valor de la propiedad unitOfMeasure.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasure(String value) {
                this.unitOfMeasure = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CouponInfo" maxOccurs="16"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}FareBasisCodeType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="FlightRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
         *                 &amp;lt;attribute name="CouponNumber" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
         *                 &amp;lt;attribute name="DocumentNumberCheckDigit"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="PaperTicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="PricingInfo" type="{http://www.opentravel.org/OTA/2003/05}FareType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="PaymentTotal" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="FormOfPayment" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" maxOccurs="4" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CityCodeOfIssue" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="ACN_Number" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
         *       &amp;lt;attribute name="AirlineAccountingCode" use="required"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{3}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="FormAndSerialNumber" use="required"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9a-zA-Z]{10}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="AgencyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="CountryCodeOfIssue" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
         *       &amp;lt;attribute name="IssuingCompanyName"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9a-zA-Z]{1,35}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="EndorsementInfo" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="DateOfIssue" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="OriginalDateofIssue" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="TicketBookReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "couponInfo",
            "pricingInfo",
            "paymentTotal",
            "cityCodeOfIssue",
            "acnNumber"
        })
        public static class TicketingInfo {

            @XmlElement(name = "CouponInfo", required = true)
            protected List<AirCheckInType.PassengerInfo.TicketingInfo.CouponInfo> couponInfo;
            @XmlElement(name = "PricingInfo")
            protected FareType pricingInfo;
            @XmlElement(name = "PaymentTotal")
            protected AirCheckInType.PassengerInfo.TicketingInfo.PaymentTotal paymentTotal;
            @XmlElement(name = "CityCodeOfIssue")
            protected LocationType cityCodeOfIssue;
            @XmlElement(name = "ACN_Number")
            protected UniqueIDType acnNumber;
            @XmlAttribute(name = "AirlineAccountingCode", required = true)
            protected String airlineAccountingCode;
            @XmlAttribute(name = "FormAndSerialNumber", required = true)
            protected String formAndSerialNumber;
            @XmlAttribute(name = "AgencyCode")
            protected String agencyCode;
            @XmlAttribute(name = "CountryCodeOfIssue")
            protected String countryCodeOfIssue;
            @XmlAttribute(name = "IssuingCompanyName")
            protected String issuingCompanyName;
            @XmlAttribute(name = "EndorsementInfo")
            protected String endorsementInfo;
            @XmlAttribute(name = "DateOfIssue")
            protected String dateOfIssue;
            @XmlAttribute(name = "OriginalDateofIssue")
            protected String originalDateofIssue;
            @XmlAttribute(name = "IssuingAgentID")
            protected String issuingAgentID;
            @XmlAttribute(name = "TicketBookReference")
            protected String ticketBookReference;
            @XmlAttribute(name = "CompanyShortName")
            protected String companyShortName;
            @XmlAttribute(name = "TravelSector")
            protected String travelSector;
            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "CountryCode")
            protected String countryCode;

            /**
             * Gets the value of the couponInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the couponInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCouponInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AirCheckInType.PassengerInfo.TicketingInfo.CouponInfo }
             * 
             * 
             */
            public List<AirCheckInType.PassengerInfo.TicketingInfo.CouponInfo> getCouponInfo() {
                if (couponInfo == null) {
                    couponInfo = new ArrayList<AirCheckInType.PassengerInfo.TicketingInfo.CouponInfo>();
                }
                return this.couponInfo;
            }

            /**
             * Obtiene el valor de la propiedad pricingInfo.
             * 
             * @return
             *     possible object is
             *     {@link FareType }
             *     
             */
            public FareType getPricingInfo() {
                return pricingInfo;
            }

            /**
             * Define el valor de la propiedad pricingInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link FareType }
             *     
             */
            public void setPricingInfo(FareType value) {
                this.pricingInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentTotal.
             * 
             * @return
             *     possible object is
             *     {@link AirCheckInType.PassengerInfo.TicketingInfo.PaymentTotal }
             *     
             */
            public AirCheckInType.PassengerInfo.TicketingInfo.PaymentTotal getPaymentTotal() {
                return paymentTotal;
            }

            /**
             * Define el valor de la propiedad paymentTotal.
             * 
             * @param value
             *     allowed object is
             *     {@link AirCheckInType.PassengerInfo.TicketingInfo.PaymentTotal }
             *     
             */
            public void setPaymentTotal(AirCheckInType.PassengerInfo.TicketingInfo.PaymentTotal value) {
                this.paymentTotal = value;
            }

            /**
             * Obtiene el valor de la propiedad cityCodeOfIssue.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getCityCodeOfIssue() {
                return cityCodeOfIssue;
            }

            /**
             * Define el valor de la propiedad cityCodeOfIssue.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setCityCodeOfIssue(LocationType value) {
                this.cityCodeOfIssue = value;
            }

            /**
             * Obtiene el valor de la propiedad acnNumber.
             * 
             * @return
             *     possible object is
             *     {@link UniqueIDType }
             *     
             */
            public UniqueIDType getACNNumber() {
                return acnNumber;
            }

            /**
             * Define el valor de la propiedad acnNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link UniqueIDType }
             *     
             */
            public void setACNNumber(UniqueIDType value) {
                this.acnNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad airlineAccountingCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAirlineAccountingCode() {
                return airlineAccountingCode;
            }

            /**
             * Define el valor de la propiedad airlineAccountingCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAirlineAccountingCode(String value) {
                this.airlineAccountingCode = value;
            }

            /**
             * Obtiene el valor de la propiedad formAndSerialNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFormAndSerialNumber() {
                return formAndSerialNumber;
            }

            /**
             * Define el valor de la propiedad formAndSerialNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFormAndSerialNumber(String value) {
                this.formAndSerialNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad agencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAgencyCode() {
                return agencyCode;
            }

            /**
             * Define el valor de la propiedad agencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAgencyCode(String value) {
                this.agencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad countryCodeOfIssue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCodeOfIssue() {
                return countryCodeOfIssue;
            }

            /**
             * Define el valor de la propiedad countryCodeOfIssue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCodeOfIssue(String value) {
                this.countryCodeOfIssue = value;
            }

            /**
             * Obtiene el valor de la propiedad issuingCompanyName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIssuingCompanyName() {
                return issuingCompanyName;
            }

            /**
             * Define el valor de la propiedad issuingCompanyName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIssuingCompanyName(String value) {
                this.issuingCompanyName = value;
            }

            /**
             * Obtiene el valor de la propiedad endorsementInfo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndorsementInfo() {
                return endorsementInfo;
            }

            /**
             * Define el valor de la propiedad endorsementInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndorsementInfo(String value) {
                this.endorsementInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad dateOfIssue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateOfIssue() {
                return dateOfIssue;
            }

            /**
             * Define el valor de la propiedad dateOfIssue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateOfIssue(String value) {
                this.dateOfIssue = value;
            }

            /**
             * Obtiene el valor de la propiedad originalDateofIssue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOriginalDateofIssue() {
                return originalDateofIssue;
            }

            /**
             * Define el valor de la propiedad originalDateofIssue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOriginalDateofIssue(String value) {
                this.originalDateofIssue = value;
            }

            /**
             * Obtiene el valor de la propiedad issuingAgentID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIssuingAgentID() {
                return issuingAgentID;
            }

            /**
             * Define el valor de la propiedad issuingAgentID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIssuingAgentID(String value) {
                this.issuingAgentID = value;
            }

            /**
             * Obtiene el valor de la propiedad ticketBookReference.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTicketBookReference() {
                return ticketBookReference;
            }

            /**
             * Define el valor de la propiedad ticketBookReference.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTicketBookReference(String value) {
                this.ticketBookReference = value;
            }

            /**
             * Obtiene el valor de la propiedad companyShortName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyShortName() {
                return companyShortName;
            }

            /**
             * Define el valor de la propiedad companyShortName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyShortName(String value) {
                this.companyShortName = value;
            }

            /**
             * Obtiene el valor de la propiedad travelSector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelSector() {
                return travelSector;
            }

            /**
             * Define el valor de la propiedad travelSector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelSector(String value) {
                this.travelSector = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad countryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Define el valor de la propiedad countryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}FareBasisCodeType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="FlightRPH" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
             *       &amp;lt;attribute name="CouponNumber" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
             *       &amp;lt;attribute name="DocumentNumberCheckDigit"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[0-9]{1}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="PaperTicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "fareBasisCode"
            })
            public static class CouponInfo {

                @XmlElement(name = "FareBasisCode")
                protected FareBasisCodeType fareBasisCode;
                @XmlAttribute(name = "FlightRPH", required = true)
                protected List<String> flightRPH;
                @XmlAttribute(name = "CouponNumber", required = true)
                protected int couponNumber;
                @XmlAttribute(name = "DocumentNumberCheckDigit")
                protected String documentNumberCheckDigit;
                @XmlAttribute(name = "PaperTicketInd")
                protected Boolean paperTicketInd;
                @XmlAttribute(name = "CouponReference")
                protected String couponReference;

                /**
                 * Obtiene el valor de la propiedad fareBasisCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link FareBasisCodeType }
                 *     
                 */
                public FareBasisCodeType getFareBasisCode() {
                    return fareBasisCode;
                }

                /**
                 * Define el valor de la propiedad fareBasisCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link FareBasisCodeType }
                 *     
                 */
                public void setFareBasisCode(FareBasisCodeType value) {
                    this.fareBasisCode = value;
                }

                /**
                 * Gets the value of the flightRPH property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightRPH property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getFlightRPH().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getFlightRPH() {
                    if (flightRPH == null) {
                        flightRPH = new ArrayList<String>();
                    }
                    return this.flightRPH;
                }

                /**
                 * Obtiene el valor de la propiedad couponNumber.
                 * 
                 */
                public int getCouponNumber() {
                    return couponNumber;
                }

                /**
                 * Define el valor de la propiedad couponNumber.
                 * 
                 */
                public void setCouponNumber(int value) {
                    this.couponNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad documentNumberCheckDigit.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDocumentNumberCheckDigit() {
                    return documentNumberCheckDigit;
                }

                /**
                 * Define el valor de la propiedad documentNumberCheckDigit.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDocumentNumberCheckDigit(String value) {
                    this.documentNumberCheckDigit = value;
                }

                /**
                 * Obtiene el valor de la propiedad paperTicketInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPaperTicketInd() {
                    return paperTicketInd;
                }

                /**
                 * Define el valor de la propiedad paperTicketInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPaperTicketInd(Boolean value) {
                    this.paperTicketInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad couponReference.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCouponReference() {
                    return couponReference;
                }

                /**
                 * Define el valor de la propiedad couponReference.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCouponReference(String value) {
                    this.couponReference = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="FormOfPayment" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" maxOccurs="4" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "formOfPayment"
            })
            public static class PaymentTotal {

                @XmlElement(name = "FormOfPayment")
                protected List<PaymentDetailType> formOfPayment;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;

                /**
                 * Gets the value of the formOfPayment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the formOfPayment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getFormOfPayment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PaymentDetailType }
                 * 
                 * 
                 */
                public List<PaymentDetailType> getFormOfPayment() {
                    if (formOfPayment == null) {
                        formOfPayment = new ArrayList<PaymentDetailType>();
                    }
                    return this.formOfPayment;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PaymentDetail" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
     *                 &amp;lt;attribute name="PaymentReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PaymentTotal" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paymentDetail",
        "paymentTotal"
    })
    public static class PaymentInfo
        extends PaymentDetailType
    {

        @XmlElement(name = "PaymentDetail", required = true)
        protected List<AirCheckInType.PaymentInfo.PaymentDetail> paymentDetail;
        @XmlElement(name = "PaymentTotal")
        protected AirCheckInType.PaymentInfo.PaymentTotal paymentTotal;

        /**
         * Gets the value of the paymentDetail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentDetail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPaymentDetail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirCheckInType.PaymentInfo.PaymentDetail }
         * 
         * 
         */
        public List<AirCheckInType.PaymentInfo.PaymentDetail> getPaymentDetail() {
            if (paymentDetail == null) {
                paymentDetail = new ArrayList<AirCheckInType.PaymentInfo.PaymentDetail>();
            }
            return this.paymentDetail;
        }

        /**
         * Obtiene el valor de la propiedad paymentTotal.
         * 
         * @return
         *     possible object is
         *     {@link AirCheckInType.PaymentInfo.PaymentTotal }
         *     
         */
        public AirCheckInType.PaymentInfo.PaymentTotal getPaymentTotal() {
            return paymentTotal;
        }

        /**
         * Define el valor de la propiedad paymentTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link AirCheckInType.PaymentInfo.PaymentTotal }
         *     
         */
        public void setPaymentTotal(AirCheckInType.PaymentInfo.PaymentTotal value) {
            this.paymentTotal = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
         *       &amp;lt;attribute name="PaymentReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PaymentDetail
            extends PaymentDetailType
        {

            @XmlAttribute(name = "PaymentReason")
            protected String paymentReason;

            /**
             * Obtiene el valor de la propiedad paymentReason.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentReason() {
                return paymentReason;
            }

            /**
             * Define el valor de la propiedad paymentReason.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentReason(String value) {
                this.paymentReason = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PaymentTotal {

            @XmlAttribute(name = "CurrencyCode")
            protected String currencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;
            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;

            /**
             * Obtiene el valor de la propiedad currencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Define el valor de la propiedad currencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

        }

    }

}
