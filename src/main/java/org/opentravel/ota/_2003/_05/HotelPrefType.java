
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * These are the hotel preference elements used on an instance of a profile.
 * 
 * &lt;p&gt;Clase Java para HotelPrefType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="HotelPrefType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="LoyaltyPref" type="{http://www.opentravel.org/OTA/2003/05}LoyaltyPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PaymentFormPref" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HotelChainPref" type="{http://www.opentravel.org/OTA/2003/05}CompanyNamePrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PropertyNamePref" type="{http://www.opentravel.org/OTA/2003/05}PropertyNamePrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PropertyLocationPref" type="{http://www.opentravel.org/OTA/2003/05}PropertyLocationPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PropertyTypePref" type="{http://www.opentravel.org/OTA/2003/05}PropertyTypePrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PropertyClassPref" type="{http://www.opentravel.org/OTA/2003/05}PropertyClassPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PropertyAmenityPref" type="{http://www.opentravel.org/OTA/2003/05}PropertyAmenityPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RoomAmenityPref" type="{http://www.opentravel.org/OTA/2003/05}RoomAmenityPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RoomLocationPref" type="{http://www.opentravel.org/OTA/2003/05}RoomLocationPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BedTypePref" type="{http://www.opentravel.org/OTA/2003/05}BedTypePrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FoodSrvcPref" type="{http://www.opentravel.org/OTA/2003/05}FoodSrvcPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MediaEntertainPref" type="{http://www.opentravel.org/OTA/2003/05}MediaEntertainPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PetInfoPref" type="{http://www.opentravel.org/OTA/2003/05}PetInfoPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MealPref" type="{http://www.opentravel.org/OTA/2003/05}MealPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RecreationSrvcPref" type="{http://www.opentravel.org/OTA/2003/05}RecreationSrvcPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BusinessSrvcPref" type="{http://www.opentravel.org/OTA/2003/05}BusinessSrvcPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PersonalSrvcPref" type="{http://www.opentravel.org/OTA/2003/05}PersonalSrvcPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SecurityFeaturePref" type="{http://www.opentravel.org/OTA/2003/05}SecurityFeaturePrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PhysChallFeaturePref" type="{http://www.opentravel.org/OTA/2003/05}PhysChallFeaturePrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SpecRequestPref" type="{http://www.opentravel.org/OTA/2003/05}SpecRequestPrefType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PrivacyGroup"/&amp;gt;
 *       &amp;lt;attribute name="RatePlanCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *       &amp;lt;attribute name="HotelGuestType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelPrefType", propOrder = {
    "loyaltyPref",
    "paymentFormPref",
    "hotelChainPref",
    "propertyNamePref",
    "propertyLocationPref",
    "propertyTypePref",
    "propertyClassPref",
    "propertyAmenityPref",
    "roomAmenityPref",
    "roomLocationPref",
    "bedTypePref",
    "foodSrvcPref",
    "mediaEntertainPref",
    "petInfoPref",
    "mealPref",
    "recreationSrvcPref",
    "businessSrvcPref",
    "personalSrvcPref",
    "securityFeaturePref",
    "physChallFeaturePref",
    "specRequestPref",
    "tpaExtensions"
})
public class HotelPrefType {

    @XmlElement(name = "LoyaltyPref")
    protected List<LoyaltyPrefType> loyaltyPref;
    @XmlElement(name = "PaymentFormPref")
    protected List<PaymentFormPrefType> paymentFormPref;
    @XmlElement(name = "HotelChainPref")
    protected List<CompanyNamePrefType> hotelChainPref;
    @XmlElement(name = "PropertyNamePref")
    protected List<PropertyNamePrefType> propertyNamePref;
    @XmlElement(name = "PropertyLocationPref")
    protected List<PropertyLocationPrefType> propertyLocationPref;
    @XmlElement(name = "PropertyTypePref")
    protected List<PropertyTypePrefType> propertyTypePref;
    @XmlElement(name = "PropertyClassPref")
    protected List<PropertyClassPrefType> propertyClassPref;
    @XmlElement(name = "PropertyAmenityPref")
    protected List<PropertyAmenityPrefType> propertyAmenityPref;
    @XmlElement(name = "RoomAmenityPref")
    protected List<RoomAmenityPrefType> roomAmenityPref;
    @XmlElement(name = "RoomLocationPref")
    protected List<RoomLocationPrefType> roomLocationPref;
    @XmlElement(name = "BedTypePref")
    protected List<BedTypePrefType> bedTypePref;
    @XmlElement(name = "FoodSrvcPref")
    protected List<FoodSrvcPrefType> foodSrvcPref;
    @XmlElement(name = "MediaEntertainPref")
    protected List<MediaEntertainPrefType> mediaEntertainPref;
    @XmlElement(name = "PetInfoPref")
    protected List<PetInfoPrefType> petInfoPref;
    @XmlElement(name = "MealPref")
    protected List<MealPrefType> mealPref;
    @XmlElement(name = "RecreationSrvcPref")
    protected List<RecreationSrvcPrefType> recreationSrvcPref;
    @XmlElement(name = "BusinessSrvcPref")
    protected List<BusinessSrvcPrefType> businessSrvcPref;
    @XmlElement(name = "PersonalSrvcPref")
    protected List<PersonalSrvcPrefType> personalSrvcPref;
    @XmlElement(name = "SecurityFeaturePref")
    protected List<SecurityFeaturePrefType> securityFeaturePref;
    @XmlElement(name = "PhysChallFeaturePref")
    protected List<PhysChallFeaturePrefType> physChallFeaturePref;
    @XmlElement(name = "SpecRequestPref")
    protected List<SpecRequestPrefType> specRequestPref;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "RatePlanCode")
    protected String ratePlanCode;
    @XmlAttribute(name = "HotelGuestType")
    protected String hotelGuestType;
    @XmlAttribute(name = "PreferLevel")
    protected PreferLevelType preferLevel;
    @XmlAttribute(name = "SmokingAllowed")
    protected Boolean smokingAllowed;
    @XmlAttribute(name = "ShareSynchInd")
    protected String shareSynchInd;
    @XmlAttribute(name = "ShareMarketInd")
    protected String shareMarketInd;

    /**
     * Gets the value of the loyaltyPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyaltyPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getLoyaltyPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link LoyaltyPrefType }
     * 
     * 
     */
    public List<LoyaltyPrefType> getLoyaltyPref() {
        if (loyaltyPref == null) {
            loyaltyPref = new ArrayList<LoyaltyPrefType>();
        }
        return this.loyaltyPref;
    }

    /**
     * Gets the value of the paymentFormPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentFormPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPaymentFormPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentFormPrefType }
     * 
     * 
     */
    public List<PaymentFormPrefType> getPaymentFormPref() {
        if (paymentFormPref == null) {
            paymentFormPref = new ArrayList<PaymentFormPrefType>();
        }
        return this.paymentFormPref;
    }

    /**
     * Gets the value of the hotelChainPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the hotelChainPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getHotelChainPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CompanyNamePrefType }
     * 
     * 
     */
    public List<CompanyNamePrefType> getHotelChainPref() {
        if (hotelChainPref == null) {
            hotelChainPref = new ArrayList<CompanyNamePrefType>();
        }
        return this.hotelChainPref;
    }

    /**
     * Gets the value of the propertyNamePref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyNamePref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPropertyNamePref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyNamePrefType }
     * 
     * 
     */
    public List<PropertyNamePrefType> getPropertyNamePref() {
        if (propertyNamePref == null) {
            propertyNamePref = new ArrayList<PropertyNamePrefType>();
        }
        return this.propertyNamePref;
    }

    /**
     * Gets the value of the propertyLocationPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyLocationPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPropertyLocationPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyLocationPrefType }
     * 
     * 
     */
    public List<PropertyLocationPrefType> getPropertyLocationPref() {
        if (propertyLocationPref == null) {
            propertyLocationPref = new ArrayList<PropertyLocationPrefType>();
        }
        return this.propertyLocationPref;
    }

    /**
     * Gets the value of the propertyTypePref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyTypePref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPropertyTypePref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyTypePrefType }
     * 
     * 
     */
    public List<PropertyTypePrefType> getPropertyTypePref() {
        if (propertyTypePref == null) {
            propertyTypePref = new ArrayList<PropertyTypePrefType>();
        }
        return this.propertyTypePref;
    }

    /**
     * Gets the value of the propertyClassPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyClassPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPropertyClassPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyClassPrefType }
     * 
     * 
     */
    public List<PropertyClassPrefType> getPropertyClassPref() {
        if (propertyClassPref == null) {
            propertyClassPref = new ArrayList<PropertyClassPrefType>();
        }
        return this.propertyClassPref;
    }

    /**
     * Gets the value of the propertyAmenityPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyAmenityPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPropertyAmenityPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyAmenityPrefType }
     * 
     * 
     */
    public List<PropertyAmenityPrefType> getPropertyAmenityPref() {
        if (propertyAmenityPref == null) {
            propertyAmenityPref = new ArrayList<PropertyAmenityPrefType>();
        }
        return this.propertyAmenityPref;
    }

    /**
     * Gets the value of the roomAmenityPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomAmenityPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRoomAmenityPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RoomAmenityPrefType }
     * 
     * 
     */
    public List<RoomAmenityPrefType> getRoomAmenityPref() {
        if (roomAmenityPref == null) {
            roomAmenityPref = new ArrayList<RoomAmenityPrefType>();
        }
        return this.roomAmenityPref;
    }

    /**
     * Gets the value of the roomLocationPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomLocationPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRoomLocationPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RoomLocationPrefType }
     * 
     * 
     */
    public List<RoomLocationPrefType> getRoomLocationPref() {
        if (roomLocationPref == null) {
            roomLocationPref = new ArrayList<RoomLocationPrefType>();
        }
        return this.roomLocationPref;
    }

    /**
     * Gets the value of the bedTypePref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bedTypePref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBedTypePref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link BedTypePrefType }
     * 
     * 
     */
    public List<BedTypePrefType> getBedTypePref() {
        if (bedTypePref == null) {
            bedTypePref = new ArrayList<BedTypePrefType>();
        }
        return this.bedTypePref;
    }

    /**
     * Gets the value of the foodSrvcPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the foodSrvcPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFoodSrvcPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FoodSrvcPrefType }
     * 
     * 
     */
    public List<FoodSrvcPrefType> getFoodSrvcPref() {
        if (foodSrvcPref == null) {
            foodSrvcPref = new ArrayList<FoodSrvcPrefType>();
        }
        return this.foodSrvcPref;
    }

    /**
     * Gets the value of the mediaEntertainPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the mediaEntertainPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getMediaEntertainPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link MediaEntertainPrefType }
     * 
     * 
     */
    public List<MediaEntertainPrefType> getMediaEntertainPref() {
        if (mediaEntertainPref == null) {
            mediaEntertainPref = new ArrayList<MediaEntertainPrefType>();
        }
        return this.mediaEntertainPref;
    }

    /**
     * Gets the value of the petInfoPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the petInfoPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPetInfoPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PetInfoPrefType }
     * 
     * 
     */
    public List<PetInfoPrefType> getPetInfoPref() {
        if (petInfoPref == null) {
            petInfoPref = new ArrayList<PetInfoPrefType>();
        }
        return this.petInfoPref;
    }

    /**
     * Gets the value of the mealPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the mealPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getMealPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link MealPrefType }
     * 
     * 
     */
    public List<MealPrefType> getMealPref() {
        if (mealPref == null) {
            mealPref = new ArrayList<MealPrefType>();
        }
        return this.mealPref;
    }

    /**
     * Gets the value of the recreationSrvcPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the recreationSrvcPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRecreationSrvcPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RecreationSrvcPrefType }
     * 
     * 
     */
    public List<RecreationSrvcPrefType> getRecreationSrvcPref() {
        if (recreationSrvcPref == null) {
            recreationSrvcPref = new ArrayList<RecreationSrvcPrefType>();
        }
        return this.recreationSrvcPref;
    }

    /**
     * Gets the value of the businessSrvcPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the businessSrvcPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBusinessSrvcPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessSrvcPrefType }
     * 
     * 
     */
    public List<BusinessSrvcPrefType> getBusinessSrvcPref() {
        if (businessSrvcPref == null) {
            businessSrvcPref = new ArrayList<BusinessSrvcPrefType>();
        }
        return this.businessSrvcPref;
    }

    /**
     * Gets the value of the personalSrvcPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the personalSrvcPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPersonalSrvcPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PersonalSrvcPrefType }
     * 
     * 
     */
    public List<PersonalSrvcPrefType> getPersonalSrvcPref() {
        if (personalSrvcPref == null) {
            personalSrvcPref = new ArrayList<PersonalSrvcPrefType>();
        }
        return this.personalSrvcPref;
    }

    /**
     * Gets the value of the securityFeaturePref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the securityFeaturePref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSecurityFeaturePref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityFeaturePrefType }
     * 
     * 
     */
    public List<SecurityFeaturePrefType> getSecurityFeaturePref() {
        if (securityFeaturePref == null) {
            securityFeaturePref = new ArrayList<SecurityFeaturePrefType>();
        }
        return this.securityFeaturePref;
    }

    /**
     * Gets the value of the physChallFeaturePref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the physChallFeaturePref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPhysChallFeaturePref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PhysChallFeaturePrefType }
     * 
     * 
     */
    public List<PhysChallFeaturePrefType> getPhysChallFeaturePref() {
        if (physChallFeaturePref == null) {
            physChallFeaturePref = new ArrayList<PhysChallFeaturePrefType>();
        }
        return this.physChallFeaturePref;
    }

    /**
     * Gets the value of the specRequestPref property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specRequestPref property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSpecRequestPref().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SpecRequestPrefType }
     * 
     * 
     */
    public List<SpecRequestPrefType> getSpecRequestPref() {
        if (specRequestPref == null) {
            specRequestPref = new ArrayList<SpecRequestPrefType>();
        }
        return this.specRequestPref;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad ratePlanCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatePlanCode() {
        return ratePlanCode;
    }

    /**
     * Define el valor de la propiedad ratePlanCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatePlanCode(String value) {
        this.ratePlanCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelGuestType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelGuestType() {
        return hotelGuestType;
    }

    /**
     * Define el valor de la propiedad hotelGuestType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelGuestType(String value) {
        this.hotelGuestType = value;
    }

    /**
     * Obtiene el valor de la propiedad preferLevel.
     * 
     * @return
     *     possible object is
     *     {@link PreferLevelType }
     *     
     */
    public PreferLevelType getPreferLevel() {
        return preferLevel;
    }

    /**
     * Define el valor de la propiedad preferLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferLevelType }
     *     
     */
    public void setPreferLevel(PreferLevelType value) {
        this.preferLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad smokingAllowed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSmokingAllowed() {
        return smokingAllowed;
    }

    /**
     * Define el valor de la propiedad smokingAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmokingAllowed(Boolean value) {
        this.smokingAllowed = value;
    }

    /**
     * Obtiene el valor de la propiedad shareSynchInd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareSynchInd() {
        return shareSynchInd;
    }

    /**
     * Define el valor de la propiedad shareSynchInd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareSynchInd(String value) {
        this.shareSynchInd = value;
    }

    /**
     * Obtiene el valor de la propiedad shareMarketInd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareMarketInd() {
        return shareMarketInd;
    }

    /**
     * Define el valor de la propiedad shareMarketInd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareMarketInd(String value) {
        this.shareMarketInd = value;
    }

}
