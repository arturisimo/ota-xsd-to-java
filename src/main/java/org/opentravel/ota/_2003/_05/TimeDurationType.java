
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.Duration;


/**
 * Used to specify a time period, which may additionally include a minimum and/or maximum duration.
 * 
 * &lt;p&gt;Clase Java para TimeDurationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TimeDurationType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;duration"&amp;gt;
 *       &amp;lt;attribute name="Minimum" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *       &amp;lt;attribute name="Maximum" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeDurationType", propOrder = {
    "value"
})
public class TimeDurationType {

    @XmlValue
    protected Duration value;
    @XmlAttribute(name = "Minimum")
    protected Duration minimum;
    @XmlAttribute(name = "Maximum")
    protected Duration maximum;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setValue(Duration value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad minimum.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMinimum() {
        return minimum;
    }

    /**
     * Define el valor de la propiedad minimum.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMinimum(Duration value) {
        this.minimum = value;
    }

    /**
     * Obtiene el valor de la propiedad maximum.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMaximum() {
        return maximum;
    }

    /**
     * Define el valor de la propiedad maximum.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMaximum(Duration value) {
        this.maximum = value;
    }

}
