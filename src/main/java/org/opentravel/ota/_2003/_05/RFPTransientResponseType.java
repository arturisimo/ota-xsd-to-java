
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides the detailed RFP data and/or the  the general information to let the requestor know when and the detailed information will be returned.
 * 
 * &lt;p&gt;Clase Java para RFP_TransientResponseType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RFP_TransientResponseType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RFP_ResponseDetail" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HotelDescription" type="{http://www.opentravel.org/OTA/2003/05}HotelDescriptiveContentType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AccountSpecificInformation" type="{http://www.opentravel.org/OTA/2003/05}AccountSpecificInformationType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RFP_TransientResponseType", propOrder = {
    "rfpResponseDetail",
    "hotelDescription",
    "accountSpecificInformation"
})
public class RFPTransientResponseType {

    @XmlElement(name = "RFP_ResponseDetail")
    protected RFPResponseDetailType rfpResponseDetail;
    @XmlElement(name = "HotelDescription")
    protected HotelDescriptiveContentType hotelDescription;
    @XmlElement(name = "AccountSpecificInformation")
    protected AccountSpecificInformationType accountSpecificInformation;
    @XmlAttribute(name = "ChainCode")
    protected String chainCode;
    @XmlAttribute(name = "BrandCode")
    protected String brandCode;
    @XmlAttribute(name = "HotelCode")
    protected String hotelCode;
    @XmlAttribute(name = "HotelCityCode")
    protected String hotelCityCode;
    @XmlAttribute(name = "HotelName")
    protected String hotelName;
    @XmlAttribute(name = "HotelCodeContext")
    protected String hotelCodeContext;
    @XmlAttribute(name = "ChainName")
    protected String chainName;
    @XmlAttribute(name = "BrandName")
    protected String brandName;
    @XmlAttribute(name = "AreaID")
    protected String areaID;
    @XmlAttribute(name = "TTIcode")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger ttIcode;

    /**
     * Obtiene el valor de la propiedad rfpResponseDetail.
     * 
     * @return
     *     possible object is
     *     {@link RFPResponseDetailType }
     *     
     */
    public RFPResponseDetailType getRFPResponseDetail() {
        return rfpResponseDetail;
    }

    /**
     * Define el valor de la propiedad rfpResponseDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link RFPResponseDetailType }
     *     
     */
    public void setRFPResponseDetail(RFPResponseDetailType value) {
        this.rfpResponseDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelDescription.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveContentType }
     *     
     */
    public HotelDescriptiveContentType getHotelDescription() {
        return hotelDescription;
    }

    /**
     * Define el valor de la propiedad hotelDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveContentType }
     *     
     */
    public void setHotelDescription(HotelDescriptiveContentType value) {
        this.hotelDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad accountSpecificInformation.
     * 
     * @return
     *     possible object is
     *     {@link AccountSpecificInformationType }
     *     
     */
    public AccountSpecificInformationType getAccountSpecificInformation() {
        return accountSpecificInformation;
    }

    /**
     * Define el valor de la propiedad accountSpecificInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountSpecificInformationType }
     *     
     */
    public void setAccountSpecificInformation(AccountSpecificInformationType value) {
        this.accountSpecificInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad chainCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainCode() {
        return chainCode;
    }

    /**
     * Define el valor de la propiedad chainCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainCode(String value) {
        this.chainCode = value;
    }

    /**
     * Obtiene el valor de la propiedad brandCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * Define el valor de la propiedad brandCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCode(String value) {
        this.brandCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Define el valor de la propiedad hotelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCityCode() {
        return hotelCityCode;
    }

    /**
     * Define el valor de la propiedad hotelCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCityCode(String value) {
        this.hotelCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * Define el valor de la propiedad hotelName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelName(String value) {
        this.hotelName = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCodeContext() {
        return hotelCodeContext;
    }

    /**
     * Define el valor de la propiedad hotelCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCodeContext(String value) {
        this.hotelCodeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad chainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainName() {
        return chainName;
    }

    /**
     * Define el valor de la propiedad chainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainName(String value) {
        this.chainName = value;
    }

    /**
     * Obtiene el valor de la propiedad brandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Define el valor de la propiedad brandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Obtiene el valor de la propiedad areaID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaID() {
        return areaID;
    }

    /**
     * Define el valor de la propiedad areaID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaID(String value) {
        this.areaID = value;
    }

    /**
     * Obtiene el valor de la propiedad ttIcode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTTIcode() {
        return ttIcode;
    }

    /**
     * Define el valor de la propiedad ttIcode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTTIcode(BigInteger value) {
        this.ttIcode = value;
    }

}
