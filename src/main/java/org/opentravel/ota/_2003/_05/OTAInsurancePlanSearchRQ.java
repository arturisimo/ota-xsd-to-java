
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type"/&amp;gt;
 *         &amp;lt;element name="CoveragePreferences" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CoveragePreference" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SearchTripInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="TravelSector" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="TotalTripCost" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="MaximumTripLength" type="{http://www.opentravel.org/OTA/2003/05}TimeDurationType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="CoveredTrips" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CoveredTrip" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                     &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SearchTravInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SearchTravelers" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="SearchTraveler" type="{http://www.opentravel.org/OTA/2003/05}SearchTravelerType" maxOccurs="unbounded"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="CitizenCountryName" type="{http://www.opentravel.org/OTA/2003/05}CountryNameType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="ResidenceCountryName" type="{http://www.opentravel.org/OTA/2003/05}CountryNameType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Affiliations" type="{http://www.opentravel.org/OTA/2003/05}BoundedAffiliationsType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="MinTravelers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaxTravelers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DetailResponseGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attribute name="SearchResponseFilter" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "coveragePreferences",
    "searchTripInfo",
    "searchTravInfo"
})
@XmlRootElement(name = "OTA_InsurancePlanSearchRQ")
public class OTAInsurancePlanSearchRQ {

    @XmlElement(name = "POS", required = true)
    protected POSType pos;
    @XmlElement(name = "CoveragePreferences")
    protected OTAInsurancePlanSearchRQ.CoveragePreferences coveragePreferences;
    @XmlElement(name = "SearchTripInfo")
    protected OTAInsurancePlanSearchRQ.SearchTripInfo searchTripInfo;
    @XmlElement(name = "SearchTravInfo")
    protected OTAInsurancePlanSearchRQ.SearchTravInfo searchTravInfo;
    @XmlAttribute(name = "SearchResponseFilter")
    protected String searchResponseFilter;
    @XmlAttribute(name = "DetailResponse", required = true)
    protected boolean detailResponse;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad coveragePreferences.
     * 
     * @return
     *     possible object is
     *     {@link OTAInsurancePlanSearchRQ.CoveragePreferences }
     *     
     */
    public OTAInsurancePlanSearchRQ.CoveragePreferences getCoveragePreferences() {
        return coveragePreferences;
    }

    /**
     * Define el valor de la propiedad coveragePreferences.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAInsurancePlanSearchRQ.CoveragePreferences }
     *     
     */
    public void setCoveragePreferences(OTAInsurancePlanSearchRQ.CoveragePreferences value) {
        this.coveragePreferences = value;
    }

    /**
     * Obtiene el valor de la propiedad searchTripInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAInsurancePlanSearchRQ.SearchTripInfo }
     *     
     */
    public OTAInsurancePlanSearchRQ.SearchTripInfo getSearchTripInfo() {
        return searchTripInfo;
    }

    /**
     * Define el valor de la propiedad searchTripInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAInsurancePlanSearchRQ.SearchTripInfo }
     *     
     */
    public void setSearchTripInfo(OTAInsurancePlanSearchRQ.SearchTripInfo value) {
        this.searchTripInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad searchTravInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAInsurancePlanSearchRQ.SearchTravInfo }
     *     
     */
    public OTAInsurancePlanSearchRQ.SearchTravInfo getSearchTravInfo() {
        return searchTravInfo;
    }

    /**
     * Define el valor de la propiedad searchTravInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAInsurancePlanSearchRQ.SearchTravInfo }
     *     
     */
    public void setSearchTravInfo(OTAInsurancePlanSearchRQ.SearchTravInfo value) {
        this.searchTravInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad searchResponseFilter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchResponseFilter() {
        return searchResponseFilter;
    }

    /**
     * Define el valor de la propiedad searchResponseFilter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchResponseFilter(String value) {
        this.searchResponseFilter = value;
    }

    /**
     * Obtiene el valor de la propiedad detailResponse.
     * 
     */
    public boolean isDetailResponse() {
        return detailResponse;
    }

    /**
     * Define el valor de la propiedad detailResponse.
     * 
     */
    public void setDetailResponse(boolean value) {
        this.detailResponse = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CoveragePreference" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "coveragePreference"
    })
    public static class CoveragePreferences {

        @XmlElement(name = "CoveragePreference", required = true)
        protected List<OTAInsurancePlanSearchRQ.CoveragePreferences.CoveragePreference> coveragePreference;

        /**
         * Gets the value of the coveragePreference property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coveragePreference property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCoveragePreference().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAInsurancePlanSearchRQ.CoveragePreferences.CoveragePreference }
         * 
         * 
         */
        public List<OTAInsurancePlanSearchRQ.CoveragePreferences.CoveragePreference> getCoveragePreference() {
            if (coveragePreference == null) {
                coveragePreference = new ArrayList<OTAInsurancePlanSearchRQ.CoveragePreferences.CoveragePreference>();
            }
            return this.coveragePreference;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CoveragePreference
            extends CoverageLimitType
        {

            @XmlAttribute(name = "PreferLevel")
            protected PreferLevelType preferLevel;

            /**
             * Obtiene el valor de la propiedad preferLevel.
             * 
             * @return
             *     possible object is
             *     {@link PreferLevelType }
             *     
             */
            public PreferLevelType getPreferLevel() {
                return preferLevel;
            }

            /**
             * Define el valor de la propiedad preferLevel.
             * 
             * @param value
             *     allowed object is
             *     {@link PreferLevelType }
             *     
             */
            public void setPreferLevel(PreferLevelType value) {
                this.preferLevel = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SearchTravelers" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="SearchTraveler" type="{http://www.opentravel.org/OTA/2003/05}SearchTravelerType" maxOccurs="unbounded"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CitizenCountryName" type="{http://www.opentravel.org/OTA/2003/05}CountryNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ResidenceCountryName" type="{http://www.opentravel.org/OTA/2003/05}CountryNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Affiliations" type="{http://www.opentravel.org/OTA/2003/05}BoundedAffiliationsType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="MinTravelers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaxTravelers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "searchTravelers",
        "citizenCountryName",
        "residenceCountryName",
        "affiliations"
    })
    public static class SearchTravInfo {

        @XmlElement(name = "SearchTravelers")
        protected OTAInsurancePlanSearchRQ.SearchTravInfo.SearchTravelers searchTravelers;
        @XmlElement(name = "CitizenCountryName")
        protected CountryNameType citizenCountryName;
        @XmlElement(name = "ResidenceCountryName")
        protected CountryNameType residenceCountryName;
        @XmlElement(name = "Affiliations")
        protected BoundedAffiliationsType affiliations;
        @XmlAttribute(name = "MinTravelers")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger minTravelers;
        @XmlAttribute(name = "MaxTravelers")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxTravelers;

        /**
         * Obtiene el valor de la propiedad searchTravelers.
         * 
         * @return
         *     possible object is
         *     {@link OTAInsurancePlanSearchRQ.SearchTravInfo.SearchTravelers }
         *     
         */
        public OTAInsurancePlanSearchRQ.SearchTravInfo.SearchTravelers getSearchTravelers() {
            return searchTravelers;
        }

        /**
         * Define el valor de la propiedad searchTravelers.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAInsurancePlanSearchRQ.SearchTravInfo.SearchTravelers }
         *     
         */
        public void setSearchTravelers(OTAInsurancePlanSearchRQ.SearchTravInfo.SearchTravelers value) {
            this.searchTravelers = value;
        }

        /**
         * Obtiene el valor de la propiedad citizenCountryName.
         * 
         * @return
         *     possible object is
         *     {@link CountryNameType }
         *     
         */
        public CountryNameType getCitizenCountryName() {
            return citizenCountryName;
        }

        /**
         * Define el valor de la propiedad citizenCountryName.
         * 
         * @param value
         *     allowed object is
         *     {@link CountryNameType }
         *     
         */
        public void setCitizenCountryName(CountryNameType value) {
            this.citizenCountryName = value;
        }

        /**
         * Obtiene el valor de la propiedad residenceCountryName.
         * 
         * @return
         *     possible object is
         *     {@link CountryNameType }
         *     
         */
        public CountryNameType getResidenceCountryName() {
            return residenceCountryName;
        }

        /**
         * Define el valor de la propiedad residenceCountryName.
         * 
         * @param value
         *     allowed object is
         *     {@link CountryNameType }
         *     
         */
        public void setResidenceCountryName(CountryNameType value) {
            this.residenceCountryName = value;
        }

        /**
         * Obtiene el valor de la propiedad affiliations.
         * 
         * @return
         *     possible object is
         *     {@link BoundedAffiliationsType }
         *     
         */
        public BoundedAffiliationsType getAffiliations() {
            return affiliations;
        }

        /**
         * Define el valor de la propiedad affiliations.
         * 
         * @param value
         *     allowed object is
         *     {@link BoundedAffiliationsType }
         *     
         */
        public void setAffiliations(BoundedAffiliationsType value) {
            this.affiliations = value;
        }

        /**
         * Obtiene el valor de la propiedad minTravelers.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMinTravelers() {
            return minTravelers;
        }

        /**
         * Define el valor de la propiedad minTravelers.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMinTravelers(BigInteger value) {
            this.minTravelers = value;
        }

        /**
         * Obtiene el valor de la propiedad maxTravelers.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxTravelers() {
            return maxTravelers;
        }

        /**
         * Define el valor de la propiedad maxTravelers.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxTravelers(BigInteger value) {
            this.maxTravelers = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="SearchTraveler" type="{http://www.opentravel.org/OTA/2003/05}SearchTravelerType" maxOccurs="unbounded"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "searchTraveler"
        })
        public static class SearchTravelers {

            @XmlElement(name = "SearchTraveler", required = true)
            protected List<SearchTravelerType> searchTraveler;

            /**
             * Gets the value of the searchTraveler property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the searchTraveler property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSearchTraveler().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link SearchTravelerType }
             * 
             * 
             */
            public List<SearchTravelerType> getSearchTraveler() {
                if (searchTraveler == null) {
                    searchTraveler = new ArrayList<SearchTravelerType>();
                }
                return this.searchTraveler;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="TravelSector" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="TotalTripCost" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="MaximumTripLength" type="{http://www.opentravel.org/OTA/2003/05}TimeDurationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CoveredTrips" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CoveredTrip" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                           &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "travelSector",
        "totalTripCost",
        "maximumTripLength",
        "coveredTrips"
    })
    public static class SearchTripInfo {

        @XmlElement(name = "TravelSector")
        protected List<String> travelSector;
        @XmlElement(name = "TotalTripCost")
        protected OTAInsurancePlanSearchRQ.SearchTripInfo.TotalTripCost totalTripCost;
        @XmlElement(name = "MaximumTripLength")
        protected TimeDurationType maximumTripLength;
        @XmlElement(name = "CoveredTrips")
        protected OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips coveredTrips;

        /**
         * Gets the value of the travelSector property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelSector property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTravelSector().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getTravelSector() {
            if (travelSector == null) {
                travelSector = new ArrayList<String>();
            }
            return this.travelSector;
        }

        /**
         * Obtiene el valor de la propiedad totalTripCost.
         * 
         * @return
         *     possible object is
         *     {@link OTAInsurancePlanSearchRQ.SearchTripInfo.TotalTripCost }
         *     
         */
        public OTAInsurancePlanSearchRQ.SearchTripInfo.TotalTripCost getTotalTripCost() {
            return totalTripCost;
        }

        /**
         * Define el valor de la propiedad totalTripCost.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAInsurancePlanSearchRQ.SearchTripInfo.TotalTripCost }
         *     
         */
        public void setTotalTripCost(OTAInsurancePlanSearchRQ.SearchTripInfo.TotalTripCost value) {
            this.totalTripCost = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumTripLength.
         * 
         * @return
         *     possible object is
         *     {@link TimeDurationType }
         *     
         */
        public TimeDurationType getMaximumTripLength() {
            return maximumTripLength;
        }

        /**
         * Define el valor de la propiedad maximumTripLength.
         * 
         * @param value
         *     allowed object is
         *     {@link TimeDurationType }
         *     
         */
        public void setMaximumTripLength(TimeDurationType value) {
            this.maximumTripLength = value;
        }

        /**
         * Obtiene el valor de la propiedad coveredTrips.
         * 
         * @return
         *     possible object is
         *     {@link OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips }
         *     
         */
        public OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips getCoveredTrips() {
            return coveredTrips;
        }

        /**
         * Define el valor de la propiedad coveredTrips.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips }
         *     
         */
        public void setCoveredTrips(OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips value) {
            this.coveredTrips = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CoveredTrip" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *                 &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "coveredTrip"
        })
        public static class CoveredTrips {

            @XmlElement(name = "CoveredTrip", required = true)
            protected List<OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips.CoveredTrip> coveredTrip;

            /**
             * Gets the value of the coveredTrip property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coveredTrip property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCoveredTrip().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips.CoveredTrip }
             * 
             * 
             */
            public List<OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips.CoveredTrip> getCoveredTrip() {
                if (coveredTrip == null) {
                    coveredTrip = new ArrayList<OTAInsurancePlanSearchRQ.SearchTripInfo.CoveredTrips.CoveredTrip>();
                }
                return this.coveredTrip;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TripFeaturesType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="DepositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *       &amp;lt;attribute name="FinalPayDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CoveredTrip
                extends TripFeaturesType
            {

                @XmlAttribute(name = "DepositDate")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar depositDate;
                @XmlAttribute(name = "FinalPayDate")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar finalPayDate;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad depositDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDepositDate() {
                    return depositDate;
                }

                /**
                 * Define el valor de la propiedad depositDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDepositDate(XMLGregorianCalendar value) {
                    this.depositDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad finalPayDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFinalPayDate() {
                    return finalPayDate;
                }

                /**
                 * Define el valor de la propiedad finalPayDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFinalPayDate(XMLGregorianCalendar value) {
                    this.finalPayDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class TotalTripCost {

            @XmlAttribute(name = "CurrencyCode")
            protected String currencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;
            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;

            /**
             * Obtiene el valor de la propiedad currencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Define el valor de la propiedad currencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

        }

    }

}
