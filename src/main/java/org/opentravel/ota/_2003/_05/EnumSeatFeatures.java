
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para Enum_SeatFeatures.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="Enum_SeatFeatures"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AdjacentSeat"/&amp;gt;
 *     &amp;lt;enumeration value="AdjacentSeat_Aisle"/&amp;gt;
 *     &amp;lt;enumeration value="AdjacentSeat_Together"/&amp;gt;
 *     &amp;lt;enumeration value="Aisle"/&amp;gt;
 *     &amp;lt;enumeration value="BlockedSeat"/&amp;gt;
 *     &amp;lt;enumeration value="BlockedSeat_Permanent"/&amp;gt;
 *     &amp;lt;enumeration value="Center"/&amp;gt;
 *     &amp;lt;enumeration value="ExitRow"/&amp;gt;
 *     &amp;lt;enumeration value="GroupSeating"/&amp;gt;
 *     &amp;lt;enumeration value="InfantOnLap"/&amp;gt;
 *     &amp;lt;enumeration value="NearCloset"/&amp;gt;
 *     &amp;lt;enumeration value="NearLavatory"/&amp;gt;
 *     &amp;lt;enumeration value="NearSpace"/&amp;gt;
 *     &amp;lt;enumeration value="NearStairs"/&amp;gt;
 *     &amp;lt;enumeration value="Nonsmoking"/&amp;gt;
 *     &amp;lt;enumeration value="Overwing"/&amp;gt;
 *     &amp;lt;enumeration value="Pet_Carriage"/&amp;gt;
 *     &amp;lt;enumeration value="Pet_Medium"/&amp;gt;
 *     &amp;lt;enumeration value="Pet_Small"/&amp;gt;
 *     &amp;lt;enumeration value="PortableOxygen"/&amp;gt;
 *     &amp;lt;enumeration value="PremiumSeat"/&amp;gt;
 *     &amp;lt;enumeration value="PremiumSeat_Reassign"/&amp;gt;
 *     &amp;lt;enumeration value="ProximitySeating_Together"/&amp;gt;
 *     &amp;lt;enumeration value="PremiumSeat_Upgrade"/&amp;gt;
 *     &amp;lt;enumeration value="Smoking"/&amp;gt;
 *     &amp;lt;enumeration value="Specific"/&amp;gt;
 *     &amp;lt;enumeration value="UnaccompaniedTravel_Minor"/&amp;gt;
 *     &amp;lt;enumeration value="UnaccompaniedTravel_Senior"/&amp;gt;
 *     &amp;lt;enumeration value="UnaccompaniedTravel_PassengerAssistance"/&amp;gt;
 *     &amp;lt;enumeration value="Window"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "Enum_SeatFeatures")
@XmlEnum
public enum EnumSeatFeatures {

    @XmlEnumValue("AdjacentSeat")
    ADJACENT_SEAT("AdjacentSeat"),
    @XmlEnumValue("AdjacentSeat_Aisle")
    ADJACENT_SEAT_AISLE("AdjacentSeat_Aisle"),
    @XmlEnumValue("AdjacentSeat_Together")
    ADJACENT_SEAT_TOGETHER("AdjacentSeat_Together"),
    @XmlEnumValue("Aisle")
    AISLE("Aisle"),
    @XmlEnumValue("BlockedSeat")
    BLOCKED_SEAT("BlockedSeat"),
    @XmlEnumValue("BlockedSeat_Permanent")
    BLOCKED_SEAT_PERMANENT("BlockedSeat_Permanent"),
    @XmlEnumValue("Center")
    CENTER("Center"),
    @XmlEnumValue("ExitRow")
    EXIT_ROW("ExitRow"),
    @XmlEnumValue("GroupSeating")
    GROUP_SEATING("GroupSeating"),
    @XmlEnumValue("InfantOnLap")
    INFANT_ON_LAP("InfantOnLap"),
    @XmlEnumValue("NearCloset")
    NEAR_CLOSET("NearCloset"),
    @XmlEnumValue("NearLavatory")
    NEAR_LAVATORY("NearLavatory"),
    @XmlEnumValue("NearSpace")
    NEAR_SPACE("NearSpace"),
    @XmlEnumValue("NearStairs")
    NEAR_STAIRS("NearStairs"),
    @XmlEnumValue("Nonsmoking")
    NONSMOKING("Nonsmoking"),
    @XmlEnumValue("Overwing")
    OVERWING("Overwing"),
    @XmlEnumValue("Pet_Carriage")
    PET_CARRIAGE("Pet_Carriage"),
    @XmlEnumValue("Pet_Medium")
    PET_MEDIUM("Pet_Medium"),
    @XmlEnumValue("Pet_Small")
    PET_SMALL("Pet_Small"),
    @XmlEnumValue("PortableOxygen")
    PORTABLE_OXYGEN("PortableOxygen"),
    @XmlEnumValue("PremiumSeat")
    PREMIUM_SEAT("PremiumSeat"),
    @XmlEnumValue("PremiumSeat_Reassign")
    PREMIUM_SEAT_REASSIGN("PremiumSeat_Reassign"),
    @XmlEnumValue("ProximitySeating_Together")
    PROXIMITY_SEATING_TOGETHER("ProximitySeating_Together"),
    @XmlEnumValue("PremiumSeat_Upgrade")
    PREMIUM_SEAT_UPGRADE("PremiumSeat_Upgrade"),
    @XmlEnumValue("Smoking")
    SMOKING("Smoking"),
    @XmlEnumValue("Specific")
    SPECIFIC("Specific"),
    @XmlEnumValue("UnaccompaniedTravel_Minor")
    UNACCOMPANIED_TRAVEL_MINOR("UnaccompaniedTravel_Minor"),
    @XmlEnumValue("UnaccompaniedTravel_Senior")
    UNACCOMPANIED_TRAVEL_SENIOR("UnaccompaniedTravel_Senior"),
    @XmlEnumValue("UnaccompaniedTravel_PassengerAssistance")
    UNACCOMPANIED_TRAVEL_PASSENGER_ASSISTANCE("UnaccompaniedTravel_PassengerAssistance"),
    @XmlEnumValue("Window")
    WINDOW("Window"),

    /**
     * It is strongly recommended that you submit a comment to have any of your extended list values permanently added to the OpenTravel specification to support maximum trading partner interoperability. http://www.opentraveldevelopersnetwork.com/specificationcomments/2/entercomment.html
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    EnumSeatFeatures(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumSeatFeatures fromValue(String v) {
        for (EnumSeatFeatures c: EnumSeatFeatures.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
