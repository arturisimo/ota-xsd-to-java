
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="Reservation"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="CancelConfirmation" type="{http://www.opentravel.org/OTA/2003/05}CancelInfoRSType"/&amp;gt;
 *                     &amp;lt;element name="ReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="ReservationInfo" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="BasicInfo" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ParticipantInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Category" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;simpleContent&amp;gt;
 *                                                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/simpleContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Group" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                           &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                                 &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                                 &amp;lt;attribute name="Gender"&amp;gt;
 *                                                   &amp;lt;simpleType&amp;gt;
 *                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                       &amp;lt;enumeration value="Male"/&amp;gt;
 *                                                       &amp;lt;enumeration value="Female"/&amp;gt;
 *                                                       &amp;lt;enumeration value="Unknown"/&amp;gt;
 *                                                       &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
 *                                                       &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
 *                                                     &amp;lt;/restriction&amp;gt;
 *                                                   &amp;lt;/simpleType&amp;gt;
 *                                                 &amp;lt;/attribute&amp;gt;
 *                                                 &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                                                 &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="SpecialNeed" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;simpleContent&amp;gt;
 *                                               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/simpleContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="PaymentInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Details" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
 *                                                 &amp;lt;attribute name="ApprovalCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Description" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="ShortDescription" minOccurs="0"&amp;gt;
 *                                           &amp;lt;simpleType&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                               &amp;lt;maxLength value="500"/&amp;gt;
 *                                               &amp;lt;minLength value="1"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/simpleType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="GuideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="GuideOverview" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPromotionType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Policy" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;simpleContent&amp;gt;
 *                                                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/simpleContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;simpleContent&amp;gt;
 *                                                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                                         &amp;lt;/extension&amp;gt;
 *                                                       &amp;lt;/simpleContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "reservation",
    "errors"
})
@XmlRootElement(name = "OTA_TourActivityCancelRS")
public class OTATourActivityCancelRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "Reservation")
    protected OTATourActivityCancelRS.Reservation reservation;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad reservation.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityCancelRS.Reservation }
     *     
     */
    public OTATourActivityCancelRS.Reservation getReservation() {
        return reservation;
    }

    /**
     * Define el valor de la propiedad reservation.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityCancelRS.Reservation }
     *     
     */
    public void setReservation(OTATourActivityCancelRS.Reservation value) {
        this.reservation = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CancelConfirmation" type="{http://www.opentravel.org/OTA/2003/05}CancelInfoRSType"/&amp;gt;
     *         &amp;lt;element name="ReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ReservationInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="BasicInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ParticipantInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Category" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                                     &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Group" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                               &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                     &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                     &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                                     &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                                     &amp;lt;attribute name="Gender"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;enumeration value="Male"/&amp;gt;
     *                                           &amp;lt;enumeration value="Female"/&amp;gt;
     *                                           &amp;lt;enumeration value="Unknown"/&amp;gt;
     *                                           &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
     *                                           &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                     &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *                                     &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="SpecialNeed" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="PaymentInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Details" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
     *                                     &amp;lt;attribute name="ApprovalCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Description" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ShortDescription" minOccurs="0"&amp;gt;
     *                               &amp;lt;simpleType&amp;gt;
     *                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                   &amp;lt;maxLength value="500"/&amp;gt;
     *                                   &amp;lt;minLength value="1"/&amp;gt;
     *                                 &amp;lt;/restriction&amp;gt;
     *                               &amp;lt;/simpleType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="GuideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="GuideOverview" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPromotionType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Policy" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                                               &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                     &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                     &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cancelConfirmation",
        "referenceID",
        "reservationInfo"
    })
    public static class Reservation {

        @XmlElement(name = "CancelConfirmation", required = true)
        protected CancelInfoRSType cancelConfirmation;
        @XmlElement(name = "ReferenceID")
        protected UniqueIDType referenceID;
        @XmlElement(name = "ReservationInfo")
        protected OTATourActivityCancelRS.Reservation.ReservationInfo reservationInfo;

        /**
         * Obtiene el valor de la propiedad cancelConfirmation.
         * 
         * @return
         *     possible object is
         *     {@link CancelInfoRSType }
         *     
         */
        public CancelInfoRSType getCancelConfirmation() {
            return cancelConfirmation;
        }

        /**
         * Define el valor de la propiedad cancelConfirmation.
         * 
         * @param value
         *     allowed object is
         *     {@link CancelInfoRSType }
         *     
         */
        public void setCancelConfirmation(CancelInfoRSType value) {
            this.cancelConfirmation = value;
        }

        /**
         * Obtiene el valor de la propiedad referenceID.
         * 
         * @return
         *     possible object is
         *     {@link UniqueIDType }
         *     
         */
        public UniqueIDType getReferenceID() {
            return referenceID;
        }

        /**
         * Define el valor de la propiedad referenceID.
         * 
         * @param value
         *     allowed object is
         *     {@link UniqueIDType }
         *     
         */
        public void setReferenceID(UniqueIDType value) {
            this.referenceID = value;
        }

        /**
         * Obtiene el valor de la propiedad reservationInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo }
         *     
         */
        public OTATourActivityCancelRS.Reservation.ReservationInfo getReservationInfo() {
            return reservationInfo;
        }

        /**
         * Define el valor de la propiedad reservationInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo }
         *     
         */
        public void setReservationInfo(OTATourActivityCancelRS.Reservation.ReservationInfo value) {
            this.reservationInfo = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="BasicInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ParticipantInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Category" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Group" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                     &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                           &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *                           &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                           &amp;lt;attribute name="Gender"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;enumeration value="Male"/&amp;gt;
         *                                 &amp;lt;enumeration value="Female"/&amp;gt;
         *                                 &amp;lt;enumeration value="Unknown"/&amp;gt;
         *                                 &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
         *                                 &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                           &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
         *                           &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="SpecialNeed" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="PaymentInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Details" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
         *                           &amp;lt;attribute name="ApprovalCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Description" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ShortDescription" minOccurs="0"&amp;gt;
         *                     &amp;lt;simpleType&amp;gt;
         *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                         &amp;lt;maxLength value="500"/&amp;gt;
         *                         &amp;lt;minLength value="1"/&amp;gt;
         *                       &amp;lt;/restriction&amp;gt;
         *                     &amp;lt;/simpleType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="GuideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="GuideOverview" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPromotionType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Policy" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                                     &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                           &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "basicInfo",
            "participantInfo",
            "paymentInfo",
            "description",
            "discount",
            "location",
            "policy",
            "pricing",
            "supplierOperator",
            "tpaExtensions"
        })
        public static class ReservationInfo {

            @XmlElement(name = "BasicInfo")
            protected OTATourActivityCancelRS.Reservation.ReservationInfo.BasicInfo basicInfo;
            @XmlElement(name = "ParticipantInfo")
            protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo> participantInfo;
            @XmlElement(name = "PaymentInfo")
            protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo> paymentInfo;
            @XmlElement(name = "Description")
            protected OTATourActivityCancelRS.Reservation.ReservationInfo.Description description;
            @XmlElement(name = "Discount")
            protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.Discount> discount;
            @XmlElement(name = "Location")
            protected TourActivityLocationType location;
            @XmlElement(name = "Policy")
            protected OTATourActivityCancelRS.Reservation.ReservationInfo.Policy policy;
            @XmlElement(name = "Pricing")
            protected OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing pricing;
            @XmlElement(name = "SupplierOperator")
            protected List<TourActivitySupplierType> supplierOperator;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;

            /**
             * Obtiene el valor de la propiedad basicInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.BasicInfo }
             *     
             */
            public OTATourActivityCancelRS.Reservation.ReservationInfo.BasicInfo getBasicInfo() {
                return basicInfo;
            }

            /**
             * Define el valor de la propiedad basicInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.BasicInfo }
             *     
             */
            public void setBasicInfo(OTATourActivityCancelRS.Reservation.ReservationInfo.BasicInfo value) {
                this.basicInfo = value;
            }

            /**
             * Gets the value of the participantInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getParticipantInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo }
             * 
             * 
             */
            public List<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo> getParticipantInfo() {
                if (participantInfo == null) {
                    participantInfo = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo>();
                }
                return this.participantInfo;
            }

            /**
             * Gets the value of the paymentInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPaymentInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo }
             * 
             * 
             */
            public List<OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo> getPaymentInfo() {
                if (paymentInfo == null) {
                    paymentInfo = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo>();
                }
                return this.paymentInfo;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Description }
             *     
             */
            public OTATourActivityCancelRS.Reservation.ReservationInfo.Description getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Description }
             *     
             */
            public void setDescription(OTATourActivityCancelRS.Reservation.ReservationInfo.Description value) {
                this.description = value;
            }

            /**
             * Gets the value of the discount property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discount property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDiscount().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Discount }
             * 
             * 
             */
            public List<OTATourActivityCancelRS.Reservation.ReservationInfo.Discount> getDiscount() {
                if (discount == null) {
                    discount = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.Discount>();
                }
                return this.discount;
            }

            /**
             * Obtiene el valor de la propiedad location.
             * 
             * @return
             *     possible object is
             *     {@link TourActivityLocationType }
             *     
             */
            public TourActivityLocationType getLocation() {
                return location;
            }

            /**
             * Define el valor de la propiedad location.
             * 
             * @param value
             *     allowed object is
             *     {@link TourActivityLocationType }
             *     
             */
            public void setLocation(TourActivityLocationType value) {
                this.location = value;
            }

            /**
             * Obtiene el valor de la propiedad policy.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Policy }
             *     
             */
            public OTATourActivityCancelRS.Reservation.ReservationInfo.Policy getPolicy() {
                return policy;
            }

            /**
             * Define el valor de la propiedad policy.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Policy }
             *     
             */
            public void setPolicy(OTATourActivityCancelRS.Reservation.ReservationInfo.Policy value) {
                this.policy = value;
            }

            /**
             * Obtiene el valor de la propiedad pricing.
             * 
             * @return
             *     possible object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing }
             *     
             */
            public OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing getPricing() {
                return pricing;
            }

            /**
             * Define el valor de la propiedad pricing.
             * 
             * @param value
             *     allowed object is
             *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing }
             *     
             */
            public void setPricing(OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing value) {
                this.pricing = value;
            }

            /**
             * Gets the value of the supplierOperator property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplierOperator property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSupplierOperator().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link TourActivitySupplierType }
             * 
             * 
             */
            public List<TourActivitySupplierType> getSupplierOperator() {
                if (supplierOperator == null) {
                    supplierOperator = new ArrayList<TourActivitySupplierType>();
                }
                return this.supplierOperator;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class BasicInfo
                extends TourActivityIDType
            {


            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ShortDescription" minOccurs="0"&amp;gt;
             *           &amp;lt;simpleType&amp;gt;
             *             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *               &amp;lt;maxLength value="500"/&amp;gt;
             *               &amp;lt;minLength value="1"/&amp;gt;
             *             &amp;lt;/restriction&amp;gt;
             *           &amp;lt;/simpleType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="GuideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="GuideOverview" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "shortDescription",
                "url"
            })
            public static class Description {

                @XmlElement(name = "ShortDescription")
                protected String shortDescription;
                @XmlElement(name = "URL")
                protected List<URLType> url;
                @XmlAttribute(name = "GuideInd")
                protected Boolean guideInd;
                @XmlAttribute(name = "GuideOverview")
                protected String guideOverview;
                @XmlAttribute(name = "FreeChildrenQty")
                protected Integer freeChildrenQty;

                /**
                 * Obtiene el valor de la propiedad shortDescription.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShortDescription() {
                    return shortDescription;
                }

                /**
                 * Define el valor de la propiedad shortDescription.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShortDescription(String value) {
                    this.shortDescription = value;
                }

                /**
                 * Gets the value of the url property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the url property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getURL().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link URLType }
                 * 
                 * 
                 */
                public List<URLType> getURL() {
                    if (url == null) {
                        url = new ArrayList<URLType>();
                    }
                    return this.url;
                }

                /**
                 * Obtiene el valor de la propiedad guideInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isGuideInd() {
                    return guideInd;
                }

                /**
                 * Define el valor de la propiedad guideInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setGuideInd(Boolean value) {
                    this.guideInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad guideOverview.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGuideOverview() {
                    return guideOverview;
                }

                /**
                 * Define el valor de la propiedad guideOverview.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGuideOverview(String value) {
                    this.guideOverview = value;
                }

                /**
                 * Obtiene el valor de la propiedad freeChildrenQty.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getFreeChildrenQty() {
                    return freeChildrenQty;
                }

                /**
                 * Define el valor de la propiedad freeChildrenQty.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setFreeChildrenQty(Integer value) {
                    this.freeChildrenQty = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPromotionType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Discount
                extends TourActivityPromotionType
            {

                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;
                @XmlAttribute(name = "GroupID")
                protected String groupID;

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Category" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Group" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                           &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *                 &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *                 &amp;lt;attribute name="Gender"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;enumeration value="Male"/&amp;gt;
             *                       &amp;lt;enumeration value="Female"/&amp;gt;
             *                       &amp;lt;enumeration value="Unknown"/&amp;gt;
             *                       &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
             *                       &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *                 &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
             *                 &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="LodgingInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="SpecialNeed" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="FreeChildrenQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "category",
                "group",
                "individual",
                "lodgingInfo",
                "specialNeed"
            })
            public static class ParticipantInfo {

                @XmlElement(name = "Category")
                protected OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category category;
                @XmlElement(name = "Group")
                protected OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group group;
                @XmlElement(name = "Individual")
                protected OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Individual individual;
                @XmlElement(name = "LodgingInfo")
                protected OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.LodgingInfo lodgingInfo;
                @XmlElement(name = "SpecialNeed")
                protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.SpecialNeed> specialNeed;
                @XmlAttribute(name = "FreeChildrenQty")
                protected Integer freeChildrenQty;

                /**
                 * Obtiene el valor de la propiedad category.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category }
                 *     
                 */
                public OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category getCategory() {
                    return category;
                }

                /**
                 * Define el valor de la propiedad category.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category }
                 *     
                 */
                public void setCategory(OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category value) {
                    this.category = value;
                }

                /**
                 * Obtiene el valor de la propiedad group.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group }
                 *     
                 */
                public OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group getGroup() {
                    return group;
                }

                /**
                 * Define el valor de la propiedad group.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group }
                 *     
                 */
                public void setGroup(OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group value) {
                    this.group = value;
                }

                /**
                 * Obtiene el valor de la propiedad individual.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Individual }
                 *     
                 */
                public OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Individual getIndividual() {
                    return individual;
                }

                /**
                 * Define el valor de la propiedad individual.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Individual }
                 *     
                 */
                public void setIndividual(OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Individual value) {
                    this.individual = value;
                }

                /**
                 * Obtiene el valor de la propiedad lodgingInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.LodgingInfo }
                 *     
                 */
                public OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.LodgingInfo getLodgingInfo() {
                    return lodgingInfo;
                }

                /**
                 * Define el valor de la propiedad lodgingInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.LodgingInfo }
                 *     
                 */
                public void setLodgingInfo(OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.LodgingInfo value) {
                    this.lodgingInfo = value;
                }

                /**
                 * Gets the value of the specialNeed property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specialNeed property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getSpecialNeed().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.SpecialNeed }
                 * 
                 * 
                 */
                public List<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.SpecialNeed> getSpecialNeed() {
                    if (specialNeed == null) {
                        specialNeed = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.SpecialNeed>();
                    }
                    return this.specialNeed;
                }

                /**
                 * Obtiene el valor de la propiedad freeChildrenQty.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getFreeChildrenQty() {
                    return freeChildrenQty;
                }

                /**
                 * Define el valor de la propiedad freeChildrenQty.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setFreeChildrenQty(Integer value) {
                    this.freeChildrenQty = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
                 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "qualifierInfo",
                    "contact",
                    "tpaExtensions"
                })
                public static class Category {

                    @XmlElement(name = "QualifierInfo")
                    protected OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category.QualifierInfo qualifierInfo;
                    @XmlElement(name = "Contact")
                    protected ContactPersonType contact;
                    @XmlElement(name = "TPA_Extensions")
                    protected TPAExtensionsType tpaExtensions;
                    @XmlAttribute(name = "Age")
                    protected Integer age;
                    @XmlAttribute(name = "Quantity")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger quantity;
                    @XmlAttribute(name = "ParticipantCategoryID")
                    protected String participantCategoryID;

                    /**
                     * Obtiene el valor de la propiedad qualifierInfo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category.QualifierInfo }
                     *     
                     */
                    public OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category.QualifierInfo getQualifierInfo() {
                        return qualifierInfo;
                    }

                    /**
                     * Define el valor de la propiedad qualifierInfo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category.QualifierInfo }
                     *     
                     */
                    public void setQualifierInfo(OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Category.QualifierInfo value) {
                        this.qualifierInfo = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad contact.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ContactPersonType }
                     *     
                     */
                    public ContactPersonType getContact() {
                        return contact;
                    }

                    /**
                     * Define el valor de la propiedad contact.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ContactPersonType }
                     *     
                     */
                    public void setContact(ContactPersonType value) {
                        this.contact = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad tpaExtensions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public TPAExtensionsType getTPAExtensions() {
                        return tpaExtensions;
                    }

                    /**
                     * Define el valor de la propiedad tpaExtensions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public void setTPAExtensions(TPAExtensionsType value) {
                        this.tpaExtensions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad age.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getAge() {
                        return age;
                    }

                    /**
                     * Define el valor de la propiedad age.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setAge(Integer value) {
                        this.age = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad quantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getQuantity() {
                        return quantity;
                    }

                    /**
                     * Define el valor de la propiedad quantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setQuantity(BigInteger value) {
                        this.quantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participantCategoryID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantCategoryID() {
                        return participantCategoryID;
                    }

                    /**
                     * Define el valor de la propiedad participantCategoryID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantCategoryID(String value) {
                        this.participantCategoryID = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class QualifierInfo
                        extends AgeQualifierType
                    {


                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="ParticipantList" maxOccurs="99" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *                 &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "contact",
                    "participantList"
                })
                public static class Group {

                    @XmlElement(name = "Contact")
                    protected ContactPersonType contact;
                    @XmlElement(name = "ParticipantList")
                    protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group.ParticipantList> participantList;
                    @XmlAttribute(name = "GroupCode")
                    protected String groupCode;
                    @XmlAttribute(name = "GroupName")
                    protected String groupName;
                    @XmlAttribute(name = "GroupID")
                    protected String groupID;
                    @XmlAttribute(name = "MinGroupSize")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger minGroupSize;
                    @XmlAttribute(name = "MaxGroupSize")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger maxGroupSize;
                    @XmlAttribute(name = "KnownGroupSize")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger knownGroupSize;

                    /**
                     * Obtiene el valor de la propiedad contact.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ContactPersonType }
                     *     
                     */
                    public ContactPersonType getContact() {
                        return contact;
                    }

                    /**
                     * Define el valor de la propiedad contact.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ContactPersonType }
                     *     
                     */
                    public void setContact(ContactPersonType value) {
                        this.contact = value;
                    }

                    /**
                     * Gets the value of the participantList property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantList property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getParticipantList().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group.ParticipantList }
                     * 
                     * 
                     */
                    public List<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group.ParticipantList> getParticipantList() {
                        if (participantList == null) {
                            participantList = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.ParticipantInfo.Group.ParticipantList>();
                        }
                        return this.participantList;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupCode() {
                        return groupCode;
                    }

                    /**
                     * Define el valor de la propiedad groupCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupCode(String value) {
                        this.groupCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupName() {
                        return groupName;
                    }

                    /**
                     * Define el valor de la propiedad groupName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupName(String value) {
                        this.groupName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupID() {
                        return groupID;
                    }

                    /**
                     * Define el valor de la propiedad groupID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupID(String value) {
                        this.groupID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad minGroupSize.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMinGroupSize() {
                        return minGroupSize;
                    }

                    /**
                     * Define el valor de la propiedad minGroupSize.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMinGroupSize(BigInteger value) {
                        this.minGroupSize = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad maxGroupSize.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMaxGroupSize() {
                        return maxGroupSize;
                    }

                    /**
                     * Define el valor de la propiedad maxGroupSize.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMaxGroupSize(BigInteger value) {
                        this.maxGroupSize = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad knownGroupSize.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getKnownGroupSize() {
                        return knownGroupSize;
                    }

                    /**
                     * Define el valor de la propiedad knownGroupSize.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setKnownGroupSize(BigInteger value) {
                        this.knownGroupSize = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *       &amp;lt;attribute name="ParticipantCategoryID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class ParticipantList {

                        @XmlAttribute(name = "ParticipantID")
                        protected String participantID;
                        @XmlAttribute(name = "ParticipantCategoryID")
                        protected String participantCategoryID;

                        /**
                         * Obtiene el valor de la propiedad participantID.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getParticipantID() {
                            return participantID;
                        }

                        /**
                         * Define el valor de la propiedad participantID.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setParticipantID(String value) {
                            this.participantID = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad participantCategoryID.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getParticipantCategoryID() {
                            return participantCategoryID;
                        }

                        /**
                         * Define el valor de la propiedad participantCategoryID.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setParticipantCategoryID(String value) {
                            this.participantCategoryID = value;
                        }

                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
                 *       &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                 *       &amp;lt;attribute name="Gender"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;enumeration value="Male"/&amp;gt;
                 *             &amp;lt;enumeration value="Female"/&amp;gt;
                 *             &amp;lt;enumeration value="Unknown"/&amp;gt;
                 *             &amp;lt;enumeration value="Male_NoShare"/&amp;gt;
                 *             &amp;lt;enumeration value="Female_NoShare"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *       &amp;lt;attribute name="Nationality" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
                 *       &amp;lt;attribute name="LeadCustomerInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="InfantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "contact"
                })
                public static class Individual
                    extends PersonNameType
                {

                    @XmlElement(name = "Contact")
                    protected ContactPersonType contact;
                    @XmlAttribute(name = "ParticipantID")
                    protected String participantID;
                    @XmlAttribute(name = "Age")
                    protected Integer age;
                    @XmlAttribute(name = "BirthDate")
                    @XmlSchemaType(name = "date")
                    protected XMLGregorianCalendar birthDate;
                    @XmlAttribute(name = "Gender")
                    protected String gender;
                    @XmlAttribute(name = "Nationality")
                    protected String nationality;
                    @XmlAttribute(name = "LeadCustomerInd")
                    protected Boolean leadCustomerInd;
                    @XmlAttribute(name = "InfantInd")
                    protected Boolean infantInd;

                    /**
                     * Obtiene el valor de la propiedad contact.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ContactPersonType }
                     *     
                     */
                    public ContactPersonType getContact() {
                        return contact;
                    }

                    /**
                     * Define el valor de la propiedad contact.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ContactPersonType }
                     *     
                     */
                    public void setContact(ContactPersonType value) {
                        this.contact = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participantID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantID() {
                        return participantID;
                    }

                    /**
                     * Define el valor de la propiedad participantID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantID(String value) {
                        this.participantID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad age.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getAge() {
                        return age;
                    }

                    /**
                     * Define el valor de la propiedad age.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setAge(Integer value) {
                        this.age = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad birthDate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getBirthDate() {
                        return birthDate;
                    }

                    /**
                     * Define el valor de la propiedad birthDate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setBirthDate(XMLGregorianCalendar value) {
                        this.birthDate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad gender.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGender() {
                        return gender;
                    }

                    /**
                     * Define el valor de la propiedad gender.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGender(String value) {
                        this.gender = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad nationality.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNationality() {
                        return nationality;
                    }

                    /**
                     * Define el valor de la propiedad nationality.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNationality(String value) {
                        this.nationality = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad leadCustomerInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isLeadCustomerInd() {
                        return leadCustomerInd;
                    }

                    /**
                     * Define el valor de la propiedad leadCustomerInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setLeadCustomerInd(Boolean value) {
                        this.leadCustomerInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad infantInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isInfantInd() {
                        return infantInd;
                    }

                    /**
                     * Define el valor de la propiedad infantInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setInfantInd(Boolean value) {
                        this.infantInd = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityLodgingType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class LodgingInfo
                    extends TourActivityLodgingType
                {

                    @XmlAttribute(name = "ParticipantID")
                    protected String participantID;
                    @XmlAttribute(name = "ParticipantCategoryID")
                    protected String participantCategoryID;
                    @XmlAttribute(name = "GroupID")
                    protected String groupID;

                    /**
                     * Obtiene el valor de la propiedad participantID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantID() {
                        return participantID;
                    }

                    /**
                     * Define el valor de la propiedad participantID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantID(String value) {
                        this.participantID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participantCategoryID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantCategoryID() {
                        return participantCategoryID;
                    }

                    /**
                     * Define el valor de la propiedad participantCategoryID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantCategoryID(String value) {
                        this.participantCategoryID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupID() {
                        return groupID;
                    }

                    /**
                     * Define el valor de la propiedad groupID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupID(String value) {
                        this.groupID = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
                 *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class SpecialNeed {

                    @XmlValue
                    protected String value;
                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "ParticipantID")
                    protected String participantID;
                    @XmlAttribute(name = "ParticipantCategoryID")
                    protected String participantCategoryID;
                    @XmlAttribute(name = "GroupID")
                    protected String groupID;

                    /**
                     * Obtiene el valor de la propiedad value.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participantID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantID() {
                        return participantID;
                    }

                    /**
                     * Define el valor de la propiedad participantID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantID(String value) {
                        this.participantID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad participantCategoryID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getParticipantCategoryID() {
                        return participantCategoryID;
                    }

                    /**
                     * Define el valor de la propiedad participantCategoryID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setParticipantCategoryID(String value) {
                        this.participantCategoryID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupID() {
                        return groupID;
                    }

                    /**
                     * Define el valor de la propiedad groupID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupID(String value) {
                        this.groupID = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Details" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
             *                 &amp;lt;attribute name="ApprovalCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "details"
            })
            public static class PaymentInfo
                extends TourActivityChargeType
            {

                @XmlElement(name = "Details")
                protected OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo.Details details;
                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;
                @XmlAttribute(name = "GroupID")
                protected String groupID;

                /**
                 * Obtiene el valor de la propiedad details.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo.Details }
                 *     
                 */
                public OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo.Details getDetails() {
                    return details;
                }

                /**
                 * Define el valor de la propiedad details.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo.Details }
                 *     
                 */
                public void setDetails(OTATourActivityCancelRS.Reservation.ReservationInfo.PaymentInfo.Details value) {
                    this.details = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
                 *       &amp;lt;attribute name="ApprovalCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Details
                    extends PaymentFormType
                {

                    @XmlAttribute(name = "ApprovalCode")
                    protected String approvalCode;

                    /**
                     * Obtiene el valor de la propiedad approvalCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getApprovalCode() {
                        return approvalCode;
                    }

                    /**
                     * Define el valor de la propiedad approvalCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setApprovalCode(String value) {
                        this.approvalCode = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityPolicyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Policy
                extends TourActivityPolicyType
            {

                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;
                @XmlAttribute(name = "GroupID")
                protected String groupID;

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "summary",
                "participantCategory",
                "group"
            })
            public static class Pricing {

                @XmlElement(name = "Summary")
                protected OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary summary;
                @XmlElement(name = "ParticipantCategory")
                protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory> participantCategory;
                @XmlElement(name = "Group")
                protected List<OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Group> group;

                /**
                 * Obtiene el valor de la propiedad summary.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary }
                 *     
                 */
                public OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary getSummary() {
                    return summary;
                }

                /**
                 * Define el valor de la propiedad summary.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary }
                 *     
                 */
                public void setSummary(OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary value) {
                    this.summary = value;
                }

                /**
                 * Gets the value of the participantCategory property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantCategory property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getParticipantCategory().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory }
                 * 
                 * 
                 */
                public List<OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory> getParticipantCategory() {
                    if (participantCategory == null) {
                        participantCategory = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory>();
                    }
                    return this.participantCategory;
                }

                /**
                 * Gets the value of the group property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the group property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getGroup().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Group }
                 * 
                 * 
                 */
                public List<OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Group> getGroup() {
                    if (group == null) {
                        group = new ArrayList<OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Group>();
                    }
                    return this.group;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "price"
                })
                public static class Group {

                    @XmlElement(name = "Price")
                    protected TourActivityChargeType price;
                    @XmlAttribute(name = "GroupCode")
                    protected String groupCode;
                    @XmlAttribute(name = "GroupName")
                    protected String groupName;
                    @XmlAttribute(name = "MinGroupSize")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger minGroupSize;
                    @XmlAttribute(name = "MaxGroupSize")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger maxGroupSize;
                    @XmlAttribute(name = "KnownGroupSize")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger knownGroupSize;

                    /**
                     * Obtiene el valor de la propiedad price.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityChargeType }
                     *     
                     */
                    public TourActivityChargeType getPrice() {
                        return price;
                    }

                    /**
                     * Define el valor de la propiedad price.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityChargeType }
                     *     
                     */
                    public void setPrice(TourActivityChargeType value) {
                        this.price = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupCode() {
                        return groupCode;
                    }

                    /**
                     * Define el valor de la propiedad groupCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupCode(String value) {
                        this.groupCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad groupName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGroupName() {
                        return groupName;
                    }

                    /**
                     * Define el valor de la propiedad groupName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGroupName(String value) {
                        this.groupName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad minGroupSize.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMinGroupSize() {
                        return minGroupSize;
                    }

                    /**
                     * Define el valor de la propiedad minGroupSize.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMinGroupSize(BigInteger value) {
                        this.minGroupSize = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad maxGroupSize.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMaxGroupSize() {
                        return maxGroupSize;
                    }

                    /**
                     * Define el valor de la propiedad maxGroupSize.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMaxGroupSize(BigInteger value) {
                        this.maxGroupSize = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad knownGroupSize.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getKnownGroupSize() {
                        return knownGroupSize;
                    }

                    /**
                     * Define el valor de la propiedad knownGroupSize.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setKnownGroupSize(BigInteger value) {
                        this.knownGroupSize = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "qualifierInfo",
                    "price",
                    "tpaExtensions"
                })
                public static class ParticipantCategory {

                    @XmlElement(name = "QualifierInfo")
                    protected OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory.QualifierInfo qualifierInfo;
                    @XmlElement(name = "Price")
                    protected TourActivityChargeType price;
                    @XmlElement(name = "TPA_Extensions")
                    protected TPAExtensionsType tpaExtensions;
                    @XmlAttribute(name = "Age")
                    protected Integer age;

                    /**
                     * Obtiene el valor de la propiedad qualifierInfo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory.QualifierInfo }
                     *     
                     */
                    public OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory.QualifierInfo getQualifierInfo() {
                        return qualifierInfo;
                    }

                    /**
                     * Define el valor de la propiedad qualifierInfo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory.QualifierInfo }
                     *     
                     */
                    public void setQualifierInfo(OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.ParticipantCategory.QualifierInfo value) {
                        this.qualifierInfo = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad price.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityChargeType }
                     *     
                     */
                    public TourActivityChargeType getPrice() {
                        return price;
                    }

                    /**
                     * Define el valor de la propiedad price.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityChargeType }
                     *     
                     */
                    public void setPrice(TourActivityChargeType value) {
                        this.price = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad tpaExtensions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public TPAExtensionsType getTPAExtensions() {
                        return tpaExtensions;
                    }

                    /**
                     * Define el valor de la propiedad tpaExtensions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TPAExtensionsType }
                     *     
                     */
                    public void setTPAExtensions(TPAExtensionsType value) {
                        this.tpaExtensions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad age.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getAge() {
                        return age;
                    }

                    /**
                     * Define el valor de la propiedad age.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setAge(Integer value) {
                        this.age = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class QualifierInfo
                        extends AgeQualifierType
                    {


                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                 *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "pricingType"
                })
                public static class Summary
                    extends TourActivityChargeType
                {

                    @XmlElement(name = "PricingType")
                    protected OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary.PricingType pricingType;

                    /**
                     * Obtiene el valor de la propiedad pricingType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary.PricingType }
                     *     
                     */
                    public OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary.PricingType getPricingType() {
                        return pricingType;
                    }

                    /**
                     * Define el valor de la propiedad pricingType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary.PricingType }
                     *     
                     */
                    public void setPricingType(OTATourActivityCancelRS.Reservation.ReservationInfo.Pricing.Summary.PricingType value) {
                        this.pricingType = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                     *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    public static class PricingType {

                        @XmlValue
                        protected TourActivityPricingTypeEnum value;
                        @XmlAttribute(name = "Extension")
                        protected String extension;

                        /**
                         * Tour and activity pricing options.
                         * 
                         * @return
                         *     possible object is
                         *     {@link TourActivityPricingTypeEnum }
                         *     
                         */
                        public TourActivityPricingTypeEnum getValue() {
                            return value;
                        }

                        /**
                         * Define el valor de la propiedad value.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link TourActivityPricingTypeEnum }
                         *     
                         */
                        public void setValue(TourActivityPricingTypeEnum value) {
                            this.value = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad extension.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getExtension() {
                            return extension;
                        }

                        /**
                         * Define el valor de la propiedad extension.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setExtension(String value) {
                            this.extension = value;
                        }

                    }

                }

            }

        }

    }

}
