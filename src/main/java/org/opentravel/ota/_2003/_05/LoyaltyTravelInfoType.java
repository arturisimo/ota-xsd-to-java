
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * A choice between the various types of travel information supported by the loyalty program schema.
 * 
 * &lt;p&gt;Clase Java para LoyaltyTravelInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="LoyaltyTravelInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;element name="HotelStayInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="RoomStayInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}RoomTypeType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="RatePlan" type="{http://www.opentravel.org/OTA/2003/05}RatePlanType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AirFlightInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="FlightSegment" type="{http://www.opentravel.org/OTA/2003/05}BookFlightSegmentType" maxOccurs="9"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CarRentalInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CarSegment"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Vendor" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="VehRentalCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TourActivityInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Schedule"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                           &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="NegotiatedRate" type="{http://www.opentravel.org/OTA/2003/05}TourActivityNegotiatedPricing" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="PickupDropoff" maxOccurs="2" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityTransRequestType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ParticipantList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
 *                                               &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                     &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="GroundTransportationInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Locations" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationsType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="ServiceInfo" type="{http://www.opentravel.org/OTA/2003/05}GroundServiceDetailType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RailInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="OriginDestination" maxOccurs="9"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="TrainSegment" maxOccurs="9"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentSummaryType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="ClassCodes" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="TravelerRequest" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="DiscountType" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AccommodationService"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="MarketingCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="SeatPreference" type="{http://www.opentravel.org/OTA/2003/05}SeatType" /&amp;gt;
 *                                     &amp;lt;attribute name="SeatDirection" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="RateQualifier" type="{http://www.opentravel.org/OTA/2003/05}RailRateQualifyingType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Fulfillment" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoyaltyTravelInfoType", propOrder = {
    "hotelStayInfo",
    "airFlightInfo",
    "carRentalInfo",
    "tourActivityInfo",
    "groundTransportationInfo",
    "railInfo"
})
public class LoyaltyTravelInfoType {

    @XmlElement(name = "HotelStayInfo")
    protected LoyaltyTravelInfoType.HotelStayInfo hotelStayInfo;
    @XmlElement(name = "AirFlightInfo")
    protected LoyaltyTravelInfoType.AirFlightInfo airFlightInfo;
    @XmlElement(name = "CarRentalInfo")
    protected LoyaltyTravelInfoType.CarRentalInfo carRentalInfo;
    @XmlElement(name = "TourActivityInfo")
    protected LoyaltyTravelInfoType.TourActivityInfo tourActivityInfo;
    @XmlElement(name = "GroundTransportationInfo")
    protected LoyaltyTravelInfoType.GroundTransportationInfo groundTransportationInfo;
    @XmlElement(name = "RailInfo")
    protected LoyaltyTravelInfoType.RailInfo railInfo;

    /**
     * Obtiene el valor de la propiedad hotelStayInfo.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyTravelInfoType.HotelStayInfo }
     *     
     */
    public LoyaltyTravelInfoType.HotelStayInfo getHotelStayInfo() {
        return hotelStayInfo;
    }

    /**
     * Define el valor de la propiedad hotelStayInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyTravelInfoType.HotelStayInfo }
     *     
     */
    public void setHotelStayInfo(LoyaltyTravelInfoType.HotelStayInfo value) {
        this.hotelStayInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad airFlightInfo.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyTravelInfoType.AirFlightInfo }
     *     
     */
    public LoyaltyTravelInfoType.AirFlightInfo getAirFlightInfo() {
        return airFlightInfo;
    }

    /**
     * Define el valor de la propiedad airFlightInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyTravelInfoType.AirFlightInfo }
     *     
     */
    public void setAirFlightInfo(LoyaltyTravelInfoType.AirFlightInfo value) {
        this.airFlightInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad carRentalInfo.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyTravelInfoType.CarRentalInfo }
     *     
     */
    public LoyaltyTravelInfoType.CarRentalInfo getCarRentalInfo() {
        return carRentalInfo;
    }

    /**
     * Define el valor de la propiedad carRentalInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyTravelInfoType.CarRentalInfo }
     *     
     */
    public void setCarRentalInfo(LoyaltyTravelInfoType.CarRentalInfo value) {
        this.carRentalInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad tourActivityInfo.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyTravelInfoType.TourActivityInfo }
     *     
     */
    public LoyaltyTravelInfoType.TourActivityInfo getTourActivityInfo() {
        return tourActivityInfo;
    }

    /**
     * Define el valor de la propiedad tourActivityInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyTravelInfoType.TourActivityInfo }
     *     
     */
    public void setTourActivityInfo(LoyaltyTravelInfoType.TourActivityInfo value) {
        this.tourActivityInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad groundTransportationInfo.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyTravelInfoType.GroundTransportationInfo }
     *     
     */
    public LoyaltyTravelInfoType.GroundTransportationInfo getGroundTransportationInfo() {
        return groundTransportationInfo;
    }

    /**
     * Define el valor de la propiedad groundTransportationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyTravelInfoType.GroundTransportationInfo }
     *     
     */
    public void setGroundTransportationInfo(LoyaltyTravelInfoType.GroundTransportationInfo value) {
        this.groundTransportationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad railInfo.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyTravelInfoType.RailInfo }
     *     
     */
    public LoyaltyTravelInfoType.RailInfo getRailInfo() {
        return railInfo;
    }

    /**
     * Define el valor de la propiedad railInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyTravelInfoType.RailInfo }
     *     
     */
    public void setRailInfo(LoyaltyTravelInfoType.RailInfo value) {
        this.railInfo = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FlightSegment" type="{http://www.opentravel.org/OTA/2003/05}BookFlightSegmentType" maxOccurs="9"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightSegment"
    })
    public static class AirFlightInfo {

        @XmlElement(name = "FlightSegment", required = true)
        protected List<BookFlightSegmentType> flightSegment;

        /**
         * Gets the value of the flightSegment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightSegment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFlightSegment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link BookFlightSegmentType }
         * 
         * 
         */
        public List<BookFlightSegmentType> getFlightSegment() {
            if (flightSegment == null) {
                flightSegment = new ArrayList<BookFlightSegmentType>();
            }
            return this.flightSegment;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CarSegment"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Vendor" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="VehRentalCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "carSegment"
    })
    public static class CarRentalInfo {

        @XmlElement(name = "CarSegment", required = true)
        protected LoyaltyTravelInfoType.CarRentalInfo.CarSegment carSegment;

        /**
         * Obtiene el valor de la propiedad carSegment.
         * 
         * @return
         *     possible object is
         *     {@link LoyaltyTravelInfoType.CarRentalInfo.CarSegment }
         *     
         */
        public LoyaltyTravelInfoType.CarRentalInfo.CarSegment getCarSegment() {
            return carSegment;
        }

        /**
         * Define el valor de la propiedad carSegment.
         * 
         * @param value
         *     allowed object is
         *     {@link LoyaltyTravelInfoType.CarRentalInfo.CarSegment }
         *     
         */
        public void setCarSegment(LoyaltyTravelInfoType.CarRentalInfo.CarSegment value) {
            this.carSegment = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Vendor" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="VehRentalCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleRentalCoreType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Vehicle" type="{http://www.opentravel.org/OTA/2003/05}VehicleCoreType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "vendor",
            "vehRentalCore",
            "vehicle"
        })
        public static class CarSegment {

            @XmlElement(name = "Vendor")
            protected CompanyNameType vendor;
            @XmlElement(name = "VehRentalCore")
            protected VehicleRentalCoreType vehRentalCore;
            @XmlElement(name = "Vehicle")
            protected VehicleCoreType vehicle;

            /**
             * Obtiene el valor de la propiedad vendor.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getVendor() {
                return vendor;
            }

            /**
             * Define el valor de la propiedad vendor.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setVendor(CompanyNameType value) {
                this.vendor = value;
            }

            /**
             * Obtiene el valor de la propiedad vehRentalCore.
             * 
             * @return
             *     possible object is
             *     {@link VehicleRentalCoreType }
             *     
             */
            public VehicleRentalCoreType getVehRentalCore() {
                return vehRentalCore;
            }

            /**
             * Define el valor de la propiedad vehRentalCore.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleRentalCoreType }
             *     
             */
            public void setVehRentalCore(VehicleRentalCoreType value) {
                this.vehRentalCore = value;
            }

            /**
             * Obtiene el valor de la propiedad vehicle.
             * 
             * @return
             *     possible object is
             *     {@link VehicleCoreType }
             *     
             */
            public VehicleCoreType getVehicle() {
                return vehicle;
            }

            /**
             * Define el valor de la propiedad vehicle.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleCoreType }
             *     
             */
            public void setVehicle(VehicleCoreType value) {
                this.vehicle = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Locations" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationsType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ServiceInfo" type="{http://www.opentravel.org/OTA/2003/05}GroundServiceDetailType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locations",
        "serviceInfo"
    })
    public static class GroundTransportationInfo {

        @XmlElement(name = "Locations")
        protected GroundLocationsType locations;
        @XmlElement(name = "ServiceInfo")
        protected GroundServiceDetailType serviceInfo;

        /**
         * Obtiene el valor de la propiedad locations.
         * 
         * @return
         *     possible object is
         *     {@link GroundLocationsType }
         *     
         */
        public GroundLocationsType getLocations() {
            return locations;
        }

        /**
         * Define el valor de la propiedad locations.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundLocationsType }
         *     
         */
        public void setLocations(GroundLocationsType value) {
            this.locations = value;
        }

        /**
         * Obtiene el valor de la propiedad serviceInfo.
         * 
         * @return
         *     possible object is
         *     {@link GroundServiceDetailType }
         *     
         */
        public GroundServiceDetailType getServiceInfo() {
            return serviceInfo;
        }

        /**
         * Define el valor de la propiedad serviceInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link GroundServiceDetailType }
         *     
         */
        public void setServiceInfo(GroundServiceDetailType value) {
            this.serviceInfo = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="RoomStayInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}RoomTypeType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RatePlan" type="{http://www.opentravel.org/OTA/2003/05}RatePlanType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reservationID",
        "roomStayInfo"
    })
    public static class HotelStayInfo {

        @XmlElement(name = "ReservationID")
        protected UniqueIDType reservationID;
        @XmlElement(name = "RoomStayInfo")
        protected LoyaltyTravelInfoType.HotelStayInfo.RoomStayInfo roomStayInfo;
        @XmlAttribute(name = "ChainCode")
        protected String chainCode;
        @XmlAttribute(name = "BrandCode")
        protected String brandCode;
        @XmlAttribute(name = "HotelCode")
        protected String hotelCode;
        @XmlAttribute(name = "HotelCityCode")
        protected String hotelCityCode;
        @XmlAttribute(name = "HotelName")
        protected String hotelName;
        @XmlAttribute(name = "HotelCodeContext")
        protected String hotelCodeContext;
        @XmlAttribute(name = "ChainName")
        protected String chainName;
        @XmlAttribute(name = "BrandName")
        protected String brandName;
        @XmlAttribute(name = "AreaID")
        protected String areaID;
        @XmlAttribute(name = "TTIcode")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger ttIcode;

        /**
         * Obtiene el valor de la propiedad reservationID.
         * 
         * @return
         *     possible object is
         *     {@link UniqueIDType }
         *     
         */
        public UniqueIDType getReservationID() {
            return reservationID;
        }

        /**
         * Define el valor de la propiedad reservationID.
         * 
         * @param value
         *     allowed object is
         *     {@link UniqueIDType }
         *     
         */
        public void setReservationID(UniqueIDType value) {
            this.reservationID = value;
        }

        /**
         * Obtiene el valor de la propiedad roomStayInfo.
         * 
         * @return
         *     possible object is
         *     {@link LoyaltyTravelInfoType.HotelStayInfo.RoomStayInfo }
         *     
         */
        public LoyaltyTravelInfoType.HotelStayInfo.RoomStayInfo getRoomStayInfo() {
            return roomStayInfo;
        }

        /**
         * Define el valor de la propiedad roomStayInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link LoyaltyTravelInfoType.HotelStayInfo.RoomStayInfo }
         *     
         */
        public void setRoomStayInfo(LoyaltyTravelInfoType.HotelStayInfo.RoomStayInfo value) {
            this.roomStayInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad chainCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChainCode() {
            return chainCode;
        }

        /**
         * Define el valor de la propiedad chainCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChainCode(String value) {
            this.chainCode = value;
        }

        /**
         * Obtiene el valor de la propiedad brandCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandCode() {
            return brandCode;
        }

        /**
         * Define el valor de la propiedad brandCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandCode(String value) {
            this.brandCode = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCode() {
            return hotelCode;
        }

        /**
         * Define el valor de la propiedad hotelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCode(String value) {
            this.hotelCode = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelCityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCityCode() {
            return hotelCityCode;
        }

        /**
         * Define el valor de la propiedad hotelCityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCityCode(String value) {
            this.hotelCityCode = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelName() {
            return hotelName;
        }

        /**
         * Define el valor de la propiedad hotelName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelName(String value) {
            this.hotelName = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelCodeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCodeContext() {
            return hotelCodeContext;
        }

        /**
         * Define el valor de la propiedad hotelCodeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCodeContext(String value) {
            this.hotelCodeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad chainName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChainName() {
            return chainName;
        }

        /**
         * Define el valor de la propiedad chainName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChainName(String value) {
            this.chainName = value;
        }

        /**
         * Obtiene el valor de la propiedad brandName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandName() {
            return brandName;
        }

        /**
         * Define el valor de la propiedad brandName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandName(String value) {
            this.brandName = value;
        }

        /**
         * Obtiene el valor de la propiedad areaID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAreaID() {
            return areaID;
        }

        /**
         * Define el valor de la propiedad areaID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAreaID(String value) {
            this.areaID = value;
        }

        /**
         * Obtiene el valor de la propiedad ttIcode.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTTIcode() {
            return ttIcode;
        }

        /**
         * Define el valor de la propiedad ttIcode.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTTIcode(BigInteger value) {
            this.ttIcode = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}RoomTypeType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RatePlan" type="{http://www.opentravel.org/OTA/2003/05}RatePlanType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomType",
            "ratePlan"
        })
        public static class RoomStayInfo {

            @XmlElement(name = "RoomType")
            protected RoomTypeType roomType;
            @XmlElement(name = "RatePlan")
            protected RatePlanType ratePlan;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad roomType.
             * 
             * @return
             *     possible object is
             *     {@link RoomTypeType }
             *     
             */
            public RoomTypeType getRoomType() {
                return roomType;
            }

            /**
             * Define el valor de la propiedad roomType.
             * 
             * @param value
             *     allowed object is
             *     {@link RoomTypeType }
             *     
             */
            public void setRoomType(RoomTypeType value) {
                this.roomType = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlan.
             * 
             * @return
             *     possible object is
             *     {@link RatePlanType }
             *     
             */
            public RatePlanType getRatePlan() {
                return ratePlan;
            }

            /**
             * Define el valor de la propiedad ratePlan.
             * 
             * @param value
             *     allowed object is
             *     {@link RatePlanType }
             *     
             */
            public void setRatePlan(RatePlanType value) {
                this.ratePlan = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginDestination" maxOccurs="9"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="TrainSegment" maxOccurs="9"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentSummaryType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ClassCodes" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="TravelerRequest" maxOccurs="99" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="DiscountType" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AccommodationService"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="MarketingCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="SeatPreference" type="{http://www.opentravel.org/OTA/2003/05}SeatType" /&amp;gt;
     *                           &amp;lt;attribute name="SeatDirection" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="RateQualifier" type="{http://www.opentravel.org/OTA/2003/05}RailRateQualifyingType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Fulfillment" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestination",
        "rateQualifier",
        "fulfillment"
    })
    public static class RailInfo {

        @XmlElement(name = "OriginDestination", required = true)
        protected List<LoyaltyTravelInfoType.RailInfo.OriginDestination> originDestination;
        @XmlElement(name = "RateQualifier")
        protected RailRateQualifyingType rateQualifier;
        @XmlElement(name = "Fulfillment")
        protected CompanyNameType fulfillment;

        /**
         * Gets the value of the originDestination property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestination property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestination().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyTravelInfoType.RailInfo.OriginDestination }
         * 
         * 
         */
        public List<LoyaltyTravelInfoType.RailInfo.OriginDestination> getOriginDestination() {
            if (originDestination == null) {
                originDestination = new ArrayList<LoyaltyTravelInfoType.RailInfo.OriginDestination>();
            }
            return this.originDestination;
        }

        /**
         * Obtiene el valor de la propiedad rateQualifier.
         * 
         * @return
         *     possible object is
         *     {@link RailRateQualifyingType }
         *     
         */
        public RailRateQualifyingType getRateQualifier() {
            return rateQualifier;
        }

        /**
         * Define el valor de la propiedad rateQualifier.
         * 
         * @param value
         *     allowed object is
         *     {@link RailRateQualifyingType }
         *     
         */
        public void setRateQualifier(RailRateQualifyingType value) {
            this.rateQualifier = value;
        }

        /**
         * Obtiene el valor de la propiedad fulfillment.
         * 
         * @return
         *     possible object is
         *     {@link CompanyNameType }
         *     
         */
        public CompanyNameType getFulfillment() {
            return fulfillment;
        }

        /**
         * Define el valor de la propiedad fulfillment.
         * 
         * @param value
         *     allowed object is
         *     {@link CompanyNameType }
         *     
         */
        public void setFulfillment(CompanyNameType value) {
            this.fulfillment = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="TrainSegment" maxOccurs="9"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentSummaryType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ClassCodes" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" maxOccurs="99" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="TravelerRequest" maxOccurs="99" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="DiscountType" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AccommodationService"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="MarketingCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="SeatPreference" type="{http://www.opentravel.org/OTA/2003/05}SeatType" /&amp;gt;
         *                 &amp;lt;attribute name="SeatDirection" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "originLocation",
            "destinationLocation",
            "trainSegment"
        })
        public static class OriginDestination {

            @XmlElement(name = "OriginLocation")
            protected LocationType originLocation;
            @XmlElement(name = "DestinationLocation")
            protected LocationType destinationLocation;
            @XmlElement(name = "TrainSegment", required = true)
            protected List<LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment> trainSegment;

            /**
             * Obtiene el valor de la propiedad originLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getOriginLocation() {
                return originLocation;
            }

            /**
             * Define el valor de la propiedad originLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setOriginLocation(LocationType value) {
                this.originLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad destinationLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDestinationLocation() {
                return destinationLocation;
            }

            /**
             * Define el valor de la propiedad destinationLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDestinationLocation(LocationType value) {
                this.destinationLocation = value;
            }

            /**
             * Gets the value of the trainSegment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the trainSegment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTrainSegment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment }
             * 
             * 
             */
            public List<LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment> getTrainSegment() {
                if (trainSegment == null) {
                    trainSegment = new ArrayList<LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment>();
                }
                return this.trainSegment;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TrainSegmentSummaryType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ClassCodes" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" maxOccurs="99" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="TravelerRequest" maxOccurs="99" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="DiscountType" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AccommodationService"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="MarketingCompany" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="SeatPreference" type="{http://www.opentravel.org/OTA/2003/05}SeatType" /&amp;gt;
             *       &amp;lt;attribute name="SeatDirection" type="{http://www.opentravel.org/OTA/2003/05}SeatDirectionType" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "classCodes",
                "travelerRequest",
                "marketingCompany"
            })
            public static class TrainSegment
                extends TrainSegmentSummaryType
            {

                @XmlElement(name = "ClassCodes")
                protected List<ClassCodeType> classCodes;
                @XmlElement(name = "TravelerRequest")
                protected List<LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest> travelerRequest;
                @XmlElement(name = "MarketingCompany")
                protected CompanyNameType marketingCompany;
                @XmlAttribute(name = "SeatPreference")
                protected SeatType seatPreference;
                @XmlAttribute(name = "SeatDirection")
                protected SeatDirectionType seatDirection;

                /**
                 * Gets the value of the classCodes property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the classCodes property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getClassCodes().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ClassCodeType }
                 * 
                 * 
                 */
                public List<ClassCodeType> getClassCodes() {
                    if (classCodes == null) {
                        classCodes = new ArrayList<ClassCodeType>();
                    }
                    return this.classCodes;
                }

                /**
                 * Gets the value of the travelerRequest property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelerRequest property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getTravelerRequest().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest }
                 * 
                 * 
                 */
                public List<LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest> getTravelerRequest() {
                    if (travelerRequest == null) {
                        travelerRequest = new ArrayList<LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest>();
                    }
                    return this.travelerRequest;
                }

                /**
                 * Obtiene el valor de la propiedad marketingCompany.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public CompanyNameType getMarketingCompany() {
                    return marketingCompany;
                }

                /**
                 * Define el valor de la propiedad marketingCompany.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CompanyNameType }
                 *     
                 */
                public void setMarketingCompany(CompanyNameType value) {
                    this.marketingCompany = value;
                }

                /**
                 * Obtiene el valor de la propiedad seatPreference.
                 * 
                 * @return
                 *     possible object is
                 *     {@link SeatType }
                 *     
                 */
                public SeatType getSeatPreference() {
                    return seatPreference;
                }

                /**
                 * Define el valor de la propiedad seatPreference.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SeatType }
                 *     
                 */
                public void setSeatPreference(SeatType value) {
                    this.seatPreference = value;
                }

                /**
                 * Obtiene el valor de la propiedad seatDirection.
                 * 
                 * @return
                 *     possible object is
                 *     {@link SeatDirectionType }
                 *     
                 */
                public SeatDirectionType getSeatDirection() {
                    return seatDirection;
                }

                /**
                 * Define el valor de la propiedad seatDirection.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SeatDirectionType }
                 *     
                 */
                public void setSeatDirection(SeatDirectionType value) {
                    this.seatDirection = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="DiscountType" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}AccommodationService"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "discountType",
                    "accommodationService"
                })
                public static class TravelerRequest {

                    @XmlElement(name = "DiscountType")
                    protected LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest.DiscountType discountType;
                    @XmlElement(name = "AccommodationService", required = true)
                    protected AccommodationServiceType accommodationService;
                    @XmlAttribute(name = "TravelerRPH")
                    protected String travelerRPH;

                    /**
                     * Obtiene el valor de la propiedad discountType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest.DiscountType }
                     *     
                     */
                    public LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest.DiscountType getDiscountType() {
                        return discountType;
                    }

                    /**
                     * Define el valor de la propiedad discountType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest.DiscountType }
                     *     
                     */
                    public void setDiscountType(LoyaltyTravelInfoType.RailInfo.OriginDestination.TrainSegment.TravelerRequest.DiscountType value) {
                        this.discountType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad accommodationService.
                     * 
                     * @return
                     *     possible object is
                     *     {@link AccommodationServiceType }
                     *     
                     */
                    public AccommodationServiceType getAccommodationService() {
                        return accommodationService;
                    }

                    /**
                     * Define el valor de la propiedad accommodationService.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link AccommodationServiceType }
                     *     
                     */
                    public void setAccommodationService(AccommodationServiceType value) {
                        this.accommodationService = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad travelerRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTravelerRPH() {
                        return travelerRPH;
                    }

                    /**
                     * Define el valor de la propiedad travelerRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTravelerRPH(String value) {
                        this.travelerRPH = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class DiscountType {

                        @XmlAttribute(name = "Code", required = true)
                        protected String code;
                        @XmlAttribute(name = "CodeContext")
                        protected String codeContext;
                        @XmlAttribute(name = "Quantity")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger quantity;

                        /**
                         * Obtiene el valor de la propiedad code.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCode() {
                            return code;
                        }

                        /**
                         * Define el valor de la propiedad code.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCode(String value) {
                            this.code = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad codeContext.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCodeContext() {
                            return codeContext;
                        }

                        /**
                         * Define el valor de la propiedad codeContext.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCodeContext(String value) {
                            this.codeContext = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad quantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getQuantity() {
                            return quantity;
                        }

                        /**
                         * Define el valor de la propiedad quantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setQuantity(BigInteger value) {
                            this.quantity = value;
                        }

                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Schedule"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Discount" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Extra" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                 &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}TourActivityLocationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="NegotiatedRate" type="{http://www.opentravel.org/OTA/2003/05}TourActivityNegotiatedPricing" maxOccurs="9" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PickupDropoff" maxOccurs="2" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityTransRequestType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ParticipantList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
     *                                     &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="SupplierOperator" type="{http://www.opentravel.org/OTA/2003/05}TourActivitySupplierType" maxOccurs="2" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "schedule",
        "discount",
        "extra",
        "location",
        "negotiatedRate",
        "pickupDropoff",
        "pricing",
        "supplierOperator"
    })
    public static class TourActivityInfo {

        @XmlElement(name = "Schedule", required = true)
        protected LoyaltyTravelInfoType.TourActivityInfo.Schedule schedule;
        @XmlElement(name = "Discount")
        protected List<LoyaltyTravelInfoType.TourActivityInfo.Discount> discount;
        @XmlElement(name = "Extra")
        protected List<LoyaltyTravelInfoType.TourActivityInfo.Extra> extra;
        @XmlElement(name = "Location")
        protected TourActivityLocationType location;
        @XmlElement(name = "NegotiatedRate")
        protected List<TourActivityNegotiatedPricing> negotiatedRate;
        @XmlElement(name = "PickupDropoff")
        protected List<LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff> pickupDropoff;
        @XmlElement(name = "Pricing")
        protected LoyaltyTravelInfoType.TourActivityInfo.Pricing pricing;
        @XmlElement(name = "SupplierOperator")
        protected List<TourActivitySupplierType> supplierOperator;

        /**
         * Obtiene el valor de la propiedad schedule.
         * 
         * @return
         *     possible object is
         *     {@link LoyaltyTravelInfoType.TourActivityInfo.Schedule }
         *     
         */
        public LoyaltyTravelInfoType.TourActivityInfo.Schedule getSchedule() {
            return schedule;
        }

        /**
         * Define el valor de la propiedad schedule.
         * 
         * @param value
         *     allowed object is
         *     {@link LoyaltyTravelInfoType.TourActivityInfo.Schedule }
         *     
         */
        public void setSchedule(LoyaltyTravelInfoType.TourActivityInfo.Schedule value) {
            this.schedule = value;
        }

        /**
         * Gets the value of the discount property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discount property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDiscount().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyTravelInfoType.TourActivityInfo.Discount }
         * 
         * 
         */
        public List<LoyaltyTravelInfoType.TourActivityInfo.Discount> getDiscount() {
            if (discount == null) {
                discount = new ArrayList<LoyaltyTravelInfoType.TourActivityInfo.Discount>();
            }
            return this.discount;
        }

        /**
         * Gets the value of the extra property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the extra property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getExtra().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyTravelInfoType.TourActivityInfo.Extra }
         * 
         * 
         */
        public List<LoyaltyTravelInfoType.TourActivityInfo.Extra> getExtra() {
            if (extra == null) {
                extra = new ArrayList<LoyaltyTravelInfoType.TourActivityInfo.Extra>();
            }
            return this.extra;
        }

        /**
         * Obtiene el valor de la propiedad location.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityLocationType }
         *     
         */
        public TourActivityLocationType getLocation() {
            return location;
        }

        /**
         * Define el valor de la propiedad location.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityLocationType }
         *     
         */
        public void setLocation(TourActivityLocationType value) {
            this.location = value;
        }

        /**
         * Gets the value of the negotiatedRate property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the negotiatedRate property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getNegotiatedRate().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivityNegotiatedPricing }
         * 
         * 
         */
        public List<TourActivityNegotiatedPricing> getNegotiatedRate() {
            if (negotiatedRate == null) {
                negotiatedRate = new ArrayList<TourActivityNegotiatedPricing>();
            }
            return this.negotiatedRate;
        }

        /**
         * Gets the value of the pickupDropoff property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pickupDropoff property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPickupDropoff().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff }
         * 
         * 
         */
        public List<LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff> getPickupDropoff() {
            if (pickupDropoff == null) {
                pickupDropoff = new ArrayList<LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff>();
            }
            return this.pickupDropoff;
        }

        /**
         * Obtiene el valor de la propiedad pricing.
         * 
         * @return
         *     possible object is
         *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing }
         *     
         */
        public LoyaltyTravelInfoType.TourActivityInfo.Pricing getPricing() {
            return pricing;
        }

        /**
         * Define el valor de la propiedad pricing.
         * 
         * @param value
         *     allowed object is
         *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing }
         *     
         */
        public void setPricing(LoyaltyTravelInfoType.TourActivityInfo.Pricing value) {
            this.pricing = value;
        }

        /**
         * Gets the value of the supplierOperator property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplierOperator property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSupplierOperator().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TourActivitySupplierType }
         * 
         * 
         */
        public List<TourActivitySupplierType> getSupplierOperator() {
            if (supplierOperator == null) {
                supplierOperator = new ArrayList<TourActivitySupplierType>();
            }
            return this.supplierOperator;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionCode",
            "participationInfo"
        })
        public static class Discount {

            @XmlElement(name = "PromotionCode")
            protected List<String> promotionCode;
            @XmlElement(name = "ParticipationInfo")
            protected LoyaltyTravelInfoType.TourActivityInfo.Discount.ParticipationInfo participationInfo;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Gets the value of the promotionCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotionCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPromotionCode() {
                if (promotionCode == null) {
                    promotionCode = new ArrayList<String>();
                }
                return this.promotionCode;
            }

            /**
             * Obtiene el valor de la propiedad participationInfo.
             * 
             * @return
             *     possible object is
             *     {@link LoyaltyTravelInfoType.TourActivityInfo.Discount.ParticipationInfo }
             *     
             */
            public LoyaltyTravelInfoType.TourActivityInfo.Discount.ParticipationInfo getParticipationInfo() {
                return participationInfo;
            }

            /**
             * Define el valor de la propiedad participationInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link LoyaltyTravelInfoType.TourActivityInfo.Discount.ParticipationInfo }
             *     
             */
            public void setParticipationInfo(LoyaltyTravelInfoType.TourActivityInfo.Discount.ParticipationInfo value) {
                this.participationInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ParticipationInfo {

                @XmlAttribute(name = "ValidFromDate")
                protected String validFromDate;
                @XmlAttribute(name = "ValidThroughDate")
                protected String validThroughDate;
                @XmlAttribute(name = "ProgramNumber")
                protected String programNumber;
                @XmlAttribute(name = "NameOnCard")
                protected String nameOnCard;

                /**
                 * Obtiene el valor de la propiedad validFromDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValidFromDate() {
                    return validFromDate;
                }

                /**
                 * Define el valor de la propiedad validFromDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValidFromDate(String value) {
                    this.validFromDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad validThroughDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValidThroughDate() {
                    return validThroughDate;
                }

                /**
                 * Define el valor de la propiedad validThroughDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValidThroughDate(String value) {
                    this.validThroughDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad programNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramNumber() {
                    return programNumber;
                }

                /**
                 * Define el valor de la propiedad programNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramNumber(String value) {
                    this.programNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad nameOnCard.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNameOnCard() {
                    return nameOnCard;
                }

                /**
                 * Define el valor de la propiedad nameOnCard.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNameOnCard(String value) {
                    this.nameOnCard = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Deposit" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *       &amp;lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="OtherCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="ReserveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "deposit"
        })
        public static class Extra {

            @XmlElement(name = "Deposit")
            protected LoyaltyTravelInfoType.TourActivityInfo.Extra.Deposit deposit;
            @XmlAttribute(name = "SupplierCode")
            protected String supplierCode;
            @XmlAttribute(name = "OtherCode")
            protected String otherCode;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "AdditionalInfo")
            protected String additionalInfo;
            @XmlAttribute(name = "Quantity")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger quantity;
            @XmlAttribute(name = "ReserveInd")
            protected Boolean reserveInd;
            @XmlAttribute(name = "ParticipantID")
            protected String participantID;
            @XmlAttribute(name = "ParticipantCategoryID")
            protected String participantCategoryID;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Obtiene el valor de la propiedad deposit.
             * 
             * @return
             *     possible object is
             *     {@link LoyaltyTravelInfoType.TourActivityInfo.Extra.Deposit }
             *     
             */
            public LoyaltyTravelInfoType.TourActivityInfo.Extra.Deposit getDeposit() {
                return deposit;
            }

            /**
             * Define el valor de la propiedad deposit.
             * 
             * @param value
             *     allowed object is
             *     {@link LoyaltyTravelInfoType.TourActivityInfo.Extra.Deposit }
             *     
             */
            public void setDeposit(LoyaltyTravelInfoType.TourActivityInfo.Extra.Deposit value) {
                this.deposit = value;
            }

            /**
             * Obtiene el valor de la propiedad supplierCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSupplierCode() {
                return supplierCode;
            }

            /**
             * Define el valor de la propiedad supplierCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSupplierCode(String value) {
                this.supplierCode = value;
            }

            /**
             * Obtiene el valor de la propiedad otherCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherCode() {
                return otherCode;
            }

            /**
             * Define el valor de la propiedad otherCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherCode(String value) {
                this.otherCode = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad additionalInfo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdditionalInfo() {
                return additionalInfo;
            }

            /**
             * Define el valor de la propiedad additionalInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdditionalInfo(String value) {
                this.additionalInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setQuantity(BigInteger value) {
                this.quantity = value;
            }

            /**
             * Obtiene el valor de la propiedad reserveInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReserveInd() {
                return reserveInd;
            }

            /**
             * Define el valor de la propiedad reserveInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReserveInd(Boolean value) {
                this.reserveInd = value;
            }

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

            /**
             * Obtiene el valor de la propiedad participantCategoryID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantCategoryID() {
                return participantCategoryID;
            }

            /**
             * Define el valor de la propiedad participantCategoryID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantCategoryID(String value) {
                this.participantCategoryID = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Deposit
                extends PaymentDetailType
            {


            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityTransRequestType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ParticipantList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "participantList"
        })
        public static class PickupDropoff
            extends TourActivityTransRequestType
        {

            @XmlElement(name = "ParticipantList")
            protected List<LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff.ParticipantList> participantList;

            /**
             * Gets the value of the participantList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getParticipantList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff.ParticipantList }
             * 
             * 
             */
            public List<LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff.ParticipantList> getParticipantList() {
                if (participantList == null) {
                    participantList = new ArrayList<LoyaltyTravelInfoType.TourActivityInfo.PickupDropoff.ParticipantList>();
                }
                return this.participantList;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TourActivityParticipantGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ParticipantList {

                @XmlAttribute(name = "ParticipantID")
                protected String participantID;
                @XmlAttribute(name = "ParticipantCategoryID")
                protected String participantCategoryID;
                @XmlAttribute(name = "GroupID")
                protected String groupID;

                /**
                 * Obtiene el valor de la propiedad participantID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantID() {
                    return participantID;
                }

                /**
                 * Define el valor de la propiedad participantID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantID(String value) {
                    this.participantID = value;
                }

                /**
                 * Obtiene el valor de la propiedad participantCategoryID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipantCategoryID() {
                    return participantCategoryID;
                }

                /**
                 * Define el valor de la propiedad participantCategoryID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipantCategoryID(String value) {
                    this.participantCategoryID = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupID() {
                    return groupID;
                }

                /**
                 * Define el valor de la propiedad groupID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupID(String value) {
                    this.groupID = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Summary" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
         *                           &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ParticipantCategory" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "summary",
            "participantCategory",
            "group"
        })
        public static class Pricing {

            @XmlElement(name = "Summary")
            protected LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary summary;
            @XmlElement(name = "ParticipantCategory")
            protected List<LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory> participantCategory;
            @XmlElement(name = "Group")
            protected List<LoyaltyTravelInfoType.TourActivityInfo.Pricing.Group> group;

            /**
             * Obtiene el valor de la propiedad summary.
             * 
             * @return
             *     possible object is
             *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary }
             *     
             */
            public LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary getSummary() {
                return summary;
            }

            /**
             * Define el valor de la propiedad summary.
             * 
             * @param value
             *     allowed object is
             *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary }
             *     
             */
            public void setSummary(LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary value) {
                this.summary = value;
            }

            /**
             * Gets the value of the participantCategory property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantCategory property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getParticipantCategory().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory }
             * 
             * 
             */
            public List<LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory> getParticipantCategory() {
                if (participantCategory == null) {
                    participantCategory = new ArrayList<LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory>();
                }
                return this.participantCategory;
            }

            /**
             * Gets the value of the group property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the group property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getGroup().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.Group }
             * 
             * 
             */
            public List<LoyaltyTravelInfoType.TourActivityInfo.Pricing.Group> getGroup() {
                if (group == null) {
                    group = new ArrayList<LoyaltyTravelInfoType.TourActivityInfo.Pricing.Group>();
                }
                return this.group;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="MinGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="MaxGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="KnownGroupSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "price"
            })
            public static class Group {

                @XmlElement(name = "Price")
                protected TourActivityChargeType price;
                @XmlAttribute(name = "GroupCode")
                protected String groupCode;
                @XmlAttribute(name = "GroupName")
                protected String groupName;
                @XmlAttribute(name = "MinGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger minGroupSize;
                @XmlAttribute(name = "MaxGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maxGroupSize;
                @XmlAttribute(name = "KnownGroupSize")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger knownGroupSize;

                /**
                 * Obtiene el valor de la propiedad price.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getPrice() {
                    return price;
                }

                /**
                 * Define el valor de la propiedad price.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setPrice(TourActivityChargeType value) {
                    this.price = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupCode() {
                    return groupCode;
                }

                /**
                 * Define el valor de la propiedad groupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupCode(String value) {
                    this.groupCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupName() {
                    return groupName;
                }

                /**
                 * Define el valor de la propiedad groupName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupName(String value) {
                    this.groupName = value;
                }

                /**
                 * Obtiene el valor de la propiedad minGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMinGroupSize() {
                    return minGroupSize;
                }

                /**
                 * Define el valor de la propiedad minGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMinGroupSize(BigInteger value) {
                    this.minGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad maxGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaxGroupSize() {
                    return maxGroupSize;
                }

                /**
                 * Define el valor de la propiedad maxGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaxGroupSize(BigInteger value) {
                    this.maxGroupSize = value;
                }

                /**
                 * Obtiene el valor de la propiedad knownGroupSize.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getKnownGroupSize() {
                    return knownGroupSize;
                }

                /**
                 * Define el valor de la propiedad knownGroupSize.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setKnownGroupSize(BigInteger value) {
                    this.knownGroupSize = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="QualifierInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Price" type="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "qualifierInfo",
                "price",
                "tpaExtensions"
            })
            public static class ParticipantCategory {

                @XmlElement(name = "QualifierInfo")
                protected LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo qualifierInfo;
                @XmlElement(name = "Price")
                protected TourActivityChargeType price;
                @XmlElement(name = "TPA_Extensions")
                protected TPAExtensionsType tpaExtensions;
                @XmlAttribute(name = "Age")
                protected Integer age;

                /**
                 * Obtiene el valor de la propiedad qualifierInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo }
                 *     
                 */
                public LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo getQualifierInfo() {
                    return qualifierInfo;
                }

                /**
                 * Define el valor de la propiedad qualifierInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo }
                 *     
                 */
                public void setQualifierInfo(LoyaltyTravelInfoType.TourActivityInfo.Pricing.ParticipantCategory.QualifierInfo value) {
                    this.qualifierInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad price.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public TourActivityChargeType getPrice() {
                    return price;
                }

                /**
                 * Define el valor de la propiedad price.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TourActivityChargeType }
                 *     
                 */
                public void setPrice(TourActivityChargeType value) {
                    this.price = value;
                }

                /**
                 * Obtiene el valor de la propiedad tpaExtensions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public TPAExtensionsType getTPAExtensions() {
                    return tpaExtensions;
                }

                /**
                 * Define el valor de la propiedad tpaExtensions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TPAExtensionsType }
                 *     
                 */
                public void setTPAExtensions(TPAExtensionsType value) {
                    this.tpaExtensions = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setAge(Integer value) {
                    this.age = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AgeQualifierType"&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class QualifierInfo
                    extends AgeQualifierType
                {


                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityChargeType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="PricingType" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
             *                 &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pricingType"
            })
            public static class Summary
                extends TourActivityChargeType
            {

                @XmlElement(name = "PricingType")
                protected LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary.PricingType pricingType;

                /**
                 * Obtiene el valor de la propiedad pricingType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary.PricingType }
                 *     
                 */
                public LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary.PricingType getPricingType() {
                    return pricingType;
                }

                /**
                 * Define el valor de la propiedad pricingType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary.PricingType }
                 *     
                 */
                public void setPricingType(LoyaltyTravelInfoType.TourActivityInfo.Pricing.Summary.PricingType value) {
                    this.pricingType = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityPricingTypeEnum"&amp;gt;
                 *       &amp;lt;attribute name="Extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class PricingType {

                    @XmlValue
                    protected TourActivityPricingTypeEnum value;
                    @XmlAttribute(name = "Extension")
                    protected String extension;

                    /**
                     * Tour and activity pricing options.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public TourActivityPricingTypeEnum getValue() {
                        return value;
                    }

                    /**
                     * Define el valor de la propiedad value.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TourActivityPricingTypeEnum }
                     *     
                     */
                    public void setValue(TourActivityPricingTypeEnum value) {
                        this.value = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad extension.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExtension() {
                        return extension;
                    }

                    /**
                     * Define el valor de la propiedad extension.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExtension(String value) {
                        this.extension = value;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Schedule {

            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }

    }

}
