
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Contains certain traveler-specific information required for quoting or booking an insurance plan.
 * 
 * &lt;p&gt;Clase Java para IndCoverageReqsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="IndCoverageReqsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="IndTripCost" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FlightAccidentAmount" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CoveredLuggage" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="LuggageItem" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="LuggageDescription" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType"/&amp;gt;
 *                             &amp;lt;element name="ItemDeclaredValue"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="LuggagePremium" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="LuggageType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PreexistingConditions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PreexistingCondition" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;simpleContent&amp;gt;
 *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
 *                           &amp;lt;attribute name="DiagnosisDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                           &amp;lt;attribute name="LastTreatmentDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/simpleContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndCoverageReqsType", propOrder = {
    "indTripCost",
    "flightAccidentAmount",
    "coveredLuggage",
    "preexistingConditions"
})
public class IndCoverageReqsType {

    @XmlElement(name = "IndTripCost")
    protected IndCoverageReqsType.IndTripCost indTripCost;
    @XmlElement(name = "FlightAccidentAmount")
    protected IndCoverageReqsType.FlightAccidentAmount flightAccidentAmount;
    @XmlElement(name = "CoveredLuggage")
    protected IndCoverageReqsType.CoveredLuggage coveredLuggage;
    @XmlElement(name = "PreexistingConditions")
    protected IndCoverageReqsType.PreexistingConditions preexistingConditions;

    /**
     * Obtiene el valor de la propiedad indTripCost.
     * 
     * @return
     *     possible object is
     *     {@link IndCoverageReqsType.IndTripCost }
     *     
     */
    public IndCoverageReqsType.IndTripCost getIndTripCost() {
        return indTripCost;
    }

    /**
     * Define el valor de la propiedad indTripCost.
     * 
     * @param value
     *     allowed object is
     *     {@link IndCoverageReqsType.IndTripCost }
     *     
     */
    public void setIndTripCost(IndCoverageReqsType.IndTripCost value) {
        this.indTripCost = value;
    }

    /**
     * Obtiene el valor de la propiedad flightAccidentAmount.
     * 
     * @return
     *     possible object is
     *     {@link IndCoverageReqsType.FlightAccidentAmount }
     *     
     */
    public IndCoverageReqsType.FlightAccidentAmount getFlightAccidentAmount() {
        return flightAccidentAmount;
    }

    /**
     * Define el valor de la propiedad flightAccidentAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link IndCoverageReqsType.FlightAccidentAmount }
     *     
     */
    public void setFlightAccidentAmount(IndCoverageReqsType.FlightAccidentAmount value) {
        this.flightAccidentAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad coveredLuggage.
     * 
     * @return
     *     possible object is
     *     {@link IndCoverageReqsType.CoveredLuggage }
     *     
     */
    public IndCoverageReqsType.CoveredLuggage getCoveredLuggage() {
        return coveredLuggage;
    }

    /**
     * Define el valor de la propiedad coveredLuggage.
     * 
     * @param value
     *     allowed object is
     *     {@link IndCoverageReqsType.CoveredLuggage }
     *     
     */
    public void setCoveredLuggage(IndCoverageReqsType.CoveredLuggage value) {
        this.coveredLuggage = value;
    }

    /**
     * Obtiene el valor de la propiedad preexistingConditions.
     * 
     * @return
     *     possible object is
     *     {@link IndCoverageReqsType.PreexistingConditions }
     *     
     */
    public IndCoverageReqsType.PreexistingConditions getPreexistingConditions() {
        return preexistingConditions;
    }

    /**
     * Define el valor de la propiedad preexistingConditions.
     * 
     * @param value
     *     allowed object is
     *     {@link IndCoverageReqsType.PreexistingConditions }
     *     
     */
    public void setPreexistingConditions(IndCoverageReqsType.PreexistingConditions value) {
        this.preexistingConditions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="LuggageItem" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="LuggageDescription" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType"/&amp;gt;
     *                   &amp;lt;element name="ItemDeclaredValue"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="LuggagePremium" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="LuggageType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "luggageItem"
    })
    public static class CoveredLuggage {

        @XmlElement(name = "LuggageItem", required = true)
        protected List<IndCoverageReqsType.CoveredLuggage.LuggageItem> luggageItem;

        /**
         * Gets the value of the luggageItem property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the luggageItem property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLuggageItem().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link IndCoverageReqsType.CoveredLuggage.LuggageItem }
         * 
         * 
         */
        public List<IndCoverageReqsType.CoveredLuggage.LuggageItem> getLuggageItem() {
            if (luggageItem == null) {
                luggageItem = new ArrayList<IndCoverageReqsType.CoveredLuggage.LuggageItem>();
            }
            return this.luggageItem;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="LuggageDescription" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType"/&amp;gt;
         *         &amp;lt;element name="ItemDeclaredValue"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="LuggagePremium" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="LuggageType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "luggageDescription",
            "itemDeclaredValue",
            "luggagePremium"
        })
        public static class LuggageItem {

            @XmlElement(name = "LuggageDescription", required = true)
            protected FreeTextType luggageDescription;
            @XmlElement(name = "ItemDeclaredValue", required = true)
            protected IndCoverageReqsType.CoveredLuggage.LuggageItem.ItemDeclaredValue itemDeclaredValue;
            @XmlElement(name = "LuggagePremium")
            protected IndCoverageReqsType.CoveredLuggage.LuggageItem.LuggagePremium luggagePremium;
            @XmlAttribute(name = "LuggageType")
            protected String luggageType;

            /**
             * Obtiene el valor de la propiedad luggageDescription.
             * 
             * @return
             *     possible object is
             *     {@link FreeTextType }
             *     
             */
            public FreeTextType getLuggageDescription() {
                return luggageDescription;
            }

            /**
             * Define el valor de la propiedad luggageDescription.
             * 
             * @param value
             *     allowed object is
             *     {@link FreeTextType }
             *     
             */
            public void setLuggageDescription(FreeTextType value) {
                this.luggageDescription = value;
            }

            /**
             * Obtiene el valor de la propiedad itemDeclaredValue.
             * 
             * @return
             *     possible object is
             *     {@link IndCoverageReqsType.CoveredLuggage.LuggageItem.ItemDeclaredValue }
             *     
             */
            public IndCoverageReqsType.CoveredLuggage.LuggageItem.ItemDeclaredValue getItemDeclaredValue() {
                return itemDeclaredValue;
            }

            /**
             * Define el valor de la propiedad itemDeclaredValue.
             * 
             * @param value
             *     allowed object is
             *     {@link IndCoverageReqsType.CoveredLuggage.LuggageItem.ItemDeclaredValue }
             *     
             */
            public void setItemDeclaredValue(IndCoverageReqsType.CoveredLuggage.LuggageItem.ItemDeclaredValue value) {
                this.itemDeclaredValue = value;
            }

            /**
             * Obtiene el valor de la propiedad luggagePremium.
             * 
             * @return
             *     possible object is
             *     {@link IndCoverageReqsType.CoveredLuggage.LuggageItem.LuggagePremium }
             *     
             */
            public IndCoverageReqsType.CoveredLuggage.LuggageItem.LuggagePremium getLuggagePremium() {
                return luggagePremium;
            }

            /**
             * Define el valor de la propiedad luggagePremium.
             * 
             * @param value
             *     allowed object is
             *     {@link IndCoverageReqsType.CoveredLuggage.LuggageItem.LuggagePremium }
             *     
             */
            public void setLuggagePremium(IndCoverageReqsType.CoveredLuggage.LuggageItem.LuggagePremium value) {
                this.luggagePremium = value;
            }

            /**
             * Obtiene el valor de la propiedad luggageType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLuggageType() {
                return luggageType;
            }

            /**
             * Define el valor de la propiedad luggageType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLuggageType(String value) {
                this.luggageType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ItemDeclaredValue {

                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class LuggagePremium {

                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FlightAccidentAmount {

        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class IndTripCost {

        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PreexistingCondition" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
     *                 &amp;lt;attribute name="DiagnosisDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                 &amp;lt;attribute name="LastTreatmentDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "preexistingCondition"
    })
    public static class PreexistingConditions {

        @XmlElement(name = "PreexistingCondition", required = true)
        protected List<IndCoverageReqsType.PreexistingConditions.PreexistingCondition> preexistingCondition;

        /**
         * Gets the value of the preexistingCondition property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the preexistingCondition property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPreexistingCondition().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link IndCoverageReqsType.PreexistingConditions.PreexistingCondition }
         * 
         * 
         */
        public List<IndCoverageReqsType.PreexistingConditions.PreexistingCondition> getPreexistingCondition() {
            if (preexistingCondition == null) {
                preexistingCondition = new ArrayList<IndCoverageReqsType.PreexistingConditions.PreexistingCondition>();
            }
            return this.preexistingCondition;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
         *       &amp;lt;attribute name="DiagnosisDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *       &amp;lt;attribute name="LastTreatmentDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PreexistingCondition
            extends FreeTextType
        {

            @XmlAttribute(name = "DiagnosisDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar diagnosisDate;
            @XmlAttribute(name = "LastTreatmentDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar lastTreatmentDate;

            /**
             * Obtiene el valor de la propiedad diagnosisDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDiagnosisDate() {
                return diagnosisDate;
            }

            /**
             * Define el valor de la propiedad diagnosisDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDiagnosisDate(XMLGregorianCalendar value) {
                this.diagnosisDate = value;
            }

            /**
             * Obtiene el valor de la propiedad lastTreatmentDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLastTreatmentDate() {
                return lastTreatmentDate;
            }

            /**
             * Define el valor de la propiedad lastTreatmentDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLastTreatmentDate(XMLGregorianCalendar value) {
                this.lastTreatmentDate = value;
            }

        }

    }

}
