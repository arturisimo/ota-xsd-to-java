
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_SSR_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_SSR_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="BassinetSeatingRequested"/&amp;gt;
 *     &amp;lt;enumeration value="BlindPassenger"/&amp;gt;
 *     &amp;lt;enumeration value="BookingContainsAnInfant"/&amp;gt;
 *     &amp;lt;enumeration value="BulkheadSeatingRequested"/&amp;gt;
 *     &amp;lt;enumeration value="ChildAged2to11Years"/&amp;gt;
 *     &amp;lt;enumeration value="DeafPassenger"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraSeatRequired"/&amp;gt;
 *     &amp;lt;enumeration value="ImpairedMobilityPassenger"/&amp;gt;
 *     &amp;lt;enumeration value="InfantAccompanyingAdultPassenger"/&amp;gt;
 *     &amp;lt;enumeration value="LanguageAssistance"/&amp;gt;
 *     &amp;lt;enumeration value="MedicalCondition"/&amp;gt;
 *     &amp;lt;enumeration value="MeetAndAssist"/&amp;gt;
 *     &amp;lt;enumeration value="OxygenRequired"/&amp;gt;
 *     &amp;lt;enumeration value="PetInCabin"/&amp;gt;
 *     &amp;lt;enumeration value="SpecialDiet_MealRestrictions"/&amp;gt;
 *     &amp;lt;enumeration value="ServiceAnimal"/&amp;gt;
 *     &amp;lt;enumeration value="SpecialLuggage"/&amp;gt;
 *     &amp;lt;enumeration value="UnaccompaniedMinor"/&amp;gt;
 *     &amp;lt;enumeration value="WheelchairRequired"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_SSR_Base")
@XmlEnum
public enum ListSSRBase {

    @XmlEnumValue("BassinetSeatingRequested")
    BASSINET_SEATING_REQUESTED("BassinetSeatingRequested"),
    @XmlEnumValue("BlindPassenger")
    BLIND_PASSENGER("BlindPassenger"),
    @XmlEnumValue("BookingContainsAnInfant")
    BOOKING_CONTAINS_AN_INFANT("BookingContainsAnInfant"),
    @XmlEnumValue("BulkheadSeatingRequested")
    BULKHEAD_SEATING_REQUESTED("BulkheadSeatingRequested"),
    @XmlEnumValue("ChildAged2to11Years")
    CHILD_AGED_2_TO_11_YEARS("ChildAged2to11Years"),
    @XmlEnumValue("DeafPassenger")
    DEAF_PASSENGER("DeafPassenger"),
    @XmlEnumValue("ExtraSeatRequired")
    EXTRA_SEAT_REQUIRED("ExtraSeatRequired"),
    @XmlEnumValue("ImpairedMobilityPassenger")
    IMPAIRED_MOBILITY_PASSENGER("ImpairedMobilityPassenger"),
    @XmlEnumValue("InfantAccompanyingAdultPassenger")
    INFANT_ACCOMPANYING_ADULT_PASSENGER("InfantAccompanyingAdultPassenger"),
    @XmlEnumValue("LanguageAssistance")
    LANGUAGE_ASSISTANCE("LanguageAssistance"),
    @XmlEnumValue("MedicalCondition")
    MEDICAL_CONDITION("MedicalCondition"),
    @XmlEnumValue("MeetAndAssist")
    MEET_AND_ASSIST("MeetAndAssist"),
    @XmlEnumValue("OxygenRequired")
    OXYGEN_REQUIRED("OxygenRequired"),
    @XmlEnumValue("PetInCabin")
    PET_IN_CABIN("PetInCabin"),
    @XmlEnumValue("SpecialDiet_MealRestrictions")
    SPECIAL_DIET_MEAL_RESTRICTIONS("SpecialDiet_MealRestrictions"),
    @XmlEnumValue("ServiceAnimal")
    SERVICE_ANIMAL("ServiceAnimal"),
    @XmlEnumValue("SpecialLuggage")
    SPECIAL_LUGGAGE("SpecialLuggage"),
    @XmlEnumValue("UnaccompaniedMinor")
    UNACCOMPANIED_MINOR("UnaccompaniedMinor"),
    @XmlEnumValue("WheelchairRequired")
    WHEELCHAIR_REQUIRED("WheelchairRequired"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListSSRBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListSSRBase fromValue(String v) {
        for (ListSSRBase c: ListSSRBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
