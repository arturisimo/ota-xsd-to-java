
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * This describes data that is specific to a single account.
 * 
 * &lt;p&gt;Clase Java para AccountSpecificInformationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccountSpecificInformationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AccountSpecificRateInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                 &amp;lt;attribute name="RoomNightTargetIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RoomNightTargetQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="RoomNightProductionQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="SubsidiaryEmpRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="PersonalUseRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ContractorsRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RetireeRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RoomTypes" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomType" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomTypeType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="RoomRateInfos" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="RoomRateInfo" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Rates" type="{http://www.opentravel.org/OTA/2003/05}RateType"/&amp;gt;
 *                                                 &amp;lt;element name="RoomRateDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BlackoutDates" type="{http://www.opentravel.org/OTA/2003/05}BlackoutDateType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ContactInfos" type="{http://www.opentravel.org/OTA/2003/05}ContactInfosType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="ContractAgreementIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountSpecificInformationType", propOrder = {
    "accountSpecificRateInfo",
    "roomTypes",
    "blackoutDates",
    "contactInfos",
    "comments"
})
public class AccountSpecificInformationType {

    @XmlElement(name = "AccountSpecificRateInfo")
    protected AccountSpecificInformationType.AccountSpecificRateInfo accountSpecificRateInfo;
    @XmlElement(name = "RoomTypes")
    protected AccountSpecificInformationType.RoomTypes roomTypes;
    @XmlElement(name = "BlackoutDates")
    protected BlackoutDateType blackoutDates;
    @XmlElement(name = "ContactInfos")
    protected ContactInfosType contactInfos;
    @XmlElement(name = "Comments")
    protected AccountSpecificInformationType.Comments comments;
    @XmlAttribute(name = "ContractAgreementIndicator")
    protected Boolean contractAgreementIndicator;

    /**
     * Obtiene el valor de la propiedad accountSpecificRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link AccountSpecificInformationType.AccountSpecificRateInfo }
     *     
     */
    public AccountSpecificInformationType.AccountSpecificRateInfo getAccountSpecificRateInfo() {
        return accountSpecificRateInfo;
    }

    /**
     * Define el valor de la propiedad accountSpecificRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountSpecificInformationType.AccountSpecificRateInfo }
     *     
     */
    public void setAccountSpecificRateInfo(AccountSpecificInformationType.AccountSpecificRateInfo value) {
        this.accountSpecificRateInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad roomTypes.
     * 
     * @return
     *     possible object is
     *     {@link AccountSpecificInformationType.RoomTypes }
     *     
     */
    public AccountSpecificInformationType.RoomTypes getRoomTypes() {
        return roomTypes;
    }

    /**
     * Define el valor de la propiedad roomTypes.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountSpecificInformationType.RoomTypes }
     *     
     */
    public void setRoomTypes(AccountSpecificInformationType.RoomTypes value) {
        this.roomTypes = value;
    }

    /**
     * Obtiene el valor de la propiedad blackoutDates.
     * 
     * @return
     *     possible object is
     *     {@link BlackoutDateType }
     *     
     */
    public BlackoutDateType getBlackoutDates() {
        return blackoutDates;
    }

    /**
     * Define el valor de la propiedad blackoutDates.
     * 
     * @param value
     *     allowed object is
     *     {@link BlackoutDateType }
     *     
     */
    public void setBlackoutDates(BlackoutDateType value) {
        this.blackoutDates = value;
    }

    /**
     * Obtiene el valor de la propiedad contactInfos.
     * 
     * @return
     *     possible object is
     *     {@link ContactInfosType }
     *     
     */
    public ContactInfosType getContactInfos() {
        return contactInfos;
    }

    /**
     * Define el valor de la propiedad contactInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInfosType }
     *     
     */
    public void setContactInfos(ContactInfosType value) {
        this.contactInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad comments.
     * 
     * @return
     *     possible object is
     *     {@link AccountSpecificInformationType.Comments }
     *     
     */
    public AccountSpecificInformationType.Comments getComments() {
        return comments;
    }

    /**
     * Define el valor de la propiedad comments.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountSpecificInformationType.Comments }
     *     
     */
    public void setComments(AccountSpecificInformationType.Comments value) {
        this.comments = value;
    }

    /**
     * Obtiene el valor de la propiedad contractAgreementIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContractAgreementIndicator() {
        return contractAgreementIndicator;
    }

    /**
     * Define el valor de la propiedad contractAgreementIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContractAgreementIndicator(Boolean value) {
        this.contractAgreementIndicator = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *       &amp;lt;attribute name="RoomNightTargetIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RoomNightTargetQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="RoomNightProductionQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="SubsidiaryEmpRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PersonalUseRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ContractorsRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RetireeRateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AccountSpecificRateInfo {

        @XmlAttribute(name = "RoomNightTargetIndicator")
        protected Boolean roomNightTargetIndicator;
        @XmlAttribute(name = "RoomNightTargetQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger roomNightTargetQuantity;
        @XmlAttribute(name = "RoomNightProductionQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger roomNightProductionQuantity;
        @XmlAttribute(name = "SubsidiaryEmpRateIndicator")
        protected Boolean subsidiaryEmpRateIndicator;
        @XmlAttribute(name = "PersonalUseRateIndicator")
        protected Boolean personalUseRateIndicator;
        @XmlAttribute(name = "ContractorsRateIndicator")
        protected Boolean contractorsRateIndicator;
        @XmlAttribute(name = "RetireeRateIndicator")
        protected Boolean retireeRateIndicator;
        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;

        /**
         * Obtiene el valor de la propiedad roomNightTargetIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRoomNightTargetIndicator() {
            return roomNightTargetIndicator;
        }

        /**
         * Define el valor de la propiedad roomNightTargetIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRoomNightTargetIndicator(Boolean value) {
            this.roomNightTargetIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad roomNightTargetQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRoomNightTargetQuantity() {
            return roomNightTargetQuantity;
        }

        /**
         * Define el valor de la propiedad roomNightTargetQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRoomNightTargetQuantity(BigInteger value) {
            this.roomNightTargetQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad roomNightProductionQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRoomNightProductionQuantity() {
            return roomNightProductionQuantity;
        }

        /**
         * Define el valor de la propiedad roomNightProductionQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRoomNightProductionQuantity(BigInteger value) {
            this.roomNightProductionQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad subsidiaryEmpRateIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSubsidiaryEmpRateIndicator() {
            return subsidiaryEmpRateIndicator;
        }

        /**
         * Define el valor de la propiedad subsidiaryEmpRateIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSubsidiaryEmpRateIndicator(Boolean value) {
            this.subsidiaryEmpRateIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad personalUseRateIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPersonalUseRateIndicator() {
            return personalUseRateIndicator;
        }

        /**
         * Define el valor de la propiedad personalUseRateIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPersonalUseRateIndicator(Boolean value) {
            this.personalUseRateIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad contractorsRateIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isContractorsRateIndicator() {
            return contractorsRateIndicator;
        }

        /**
         * Define el valor de la propiedad contractorsRateIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setContractorsRateIndicator(Boolean value) {
            this.contractorsRateIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad retireeRateIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRetireeRateIndicator() {
            return retireeRateIndicator;
        }

        /**
         * Define el valor de la propiedad retireeRateIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRetireeRateIndicator(Boolean value) {
            this.retireeRateIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "comment"
    })
    public static class Comments {

        @XmlElement(name = "Comment", required = true)
        protected List<ParagraphType> comment;

        /**
         * Gets the value of the comment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getComment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getComment() {
            if (comment == null) {
                comment = new ArrayList<ParagraphType>();
            }
            return this.comment;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomType" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomTypeType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RoomRateInfos" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="RoomRateInfo" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Rates" type="{http://www.opentravel.org/OTA/2003/05}RateType"/&amp;gt;
     *                                       &amp;lt;element name="RoomRateDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomType"
    })
    public static class RoomTypes {

        @XmlElement(name = "RoomType", required = true)
        protected List<AccountSpecificInformationType.RoomTypes.RoomType> roomType;

        /**
         * Gets the value of the roomType property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomType property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomType().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AccountSpecificInformationType.RoomTypes.RoomType }
         * 
         * 
         */
        public List<AccountSpecificInformationType.RoomTypes.RoomType> getRoomType() {
            if (roomType == null) {
                roomType = new ArrayList<AccountSpecificInformationType.RoomTypes.RoomType>();
            }
            return this.roomType;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomTypeType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RoomRateInfos" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="RoomRateInfo" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Rates" type="{http://www.opentravel.org/OTA/2003/05}RateType"/&amp;gt;
         *                             &amp;lt;element name="RoomRateDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomRateInfos"
        })
        public static class RoomType
            extends RoomTypeType
        {

            @XmlElement(name = "RoomRateInfos")
            protected AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos roomRateInfos;

            /**
             * Obtiene el valor de la propiedad roomRateInfos.
             * 
             * @return
             *     possible object is
             *     {@link AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos }
             *     
             */
            public AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos getRoomRateInfos() {
                return roomRateInfos;
            }

            /**
             * Define el valor de la propiedad roomRateInfos.
             * 
             * @param value
             *     allowed object is
             *     {@link AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos }
             *     
             */
            public void setRoomRateInfos(AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos value) {
                this.roomRateInfos = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="RoomRateInfo" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Rates" type="{http://www.opentravel.org/OTA/2003/05}RateType"/&amp;gt;
             *                   &amp;lt;element name="RoomRateDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "roomRateInfo"
            })
            public static class RoomRateInfos {

                @XmlElement(name = "RoomRateInfo", required = true)
                protected List<AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos.RoomRateInfo> roomRateInfo;

                /**
                 * Gets the value of the roomRateInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomRateInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getRoomRateInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos.RoomRateInfo }
                 * 
                 * 
                 */
                public List<AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos.RoomRateInfo> getRoomRateInfo() {
                    if (roomRateInfo == null) {
                        roomRateInfo = new ArrayList<AccountSpecificInformationType.RoomTypes.RoomType.RoomRateInfos.RoomRateInfo>();
                    }
                    return this.roomRateInfo;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Rates" type="{http://www.opentravel.org/OTA/2003/05}RateType"/&amp;gt;
                 *         &amp;lt;element name="RoomRateDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "rates",
                    "roomRateDescription",
                    "commission"
                })
                public static class RoomRateInfo {

                    @XmlElement(name = "Rates", required = true)
                    protected RateType rates;
                    @XmlElement(name = "RoomRateDescription")
                    protected FormattedTextTextType roomRateDescription;
                    @XmlElement(name = "Commission")
                    protected List<CommissionType> commission;

                    /**
                     * Obtiene el valor de la propiedad rates.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RateType }
                     *     
                     */
                    public RateType getRates() {
                        return rates;
                    }

                    /**
                     * Define el valor de la propiedad rates.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RateType }
                     *     
                     */
                    public void setRates(RateType value) {
                        this.rates = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad roomRateDescription.
                     * 
                     * @return
                     *     possible object is
                     *     {@link FormattedTextTextType }
                     *     
                     */
                    public FormattedTextTextType getRoomRateDescription() {
                        return roomRateDescription;
                    }

                    /**
                     * Define el valor de la propiedad roomRateDescription.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link FormattedTextTextType }
                     *     
                     */
                    public void setRoomRateDescription(FormattedTextTextType value) {
                        this.roomRateDescription = value;
                    }

                    /**
                     * Gets the value of the commission property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the commission property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getCommission().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link CommissionType }
                     * 
                     * 
                     */
                    public List<CommissionType> getCommission() {
                        if (commission == null) {
                            commission = new ArrayList<CommissionType>();
                        }
                        return this.commission;
                    }

                }

            }

        }

    }

}
