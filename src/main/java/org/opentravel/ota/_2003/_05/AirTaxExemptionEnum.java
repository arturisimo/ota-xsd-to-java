
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para AirTaxExemptionEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AirTaxExemptionEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="PFC_PassengerFacilitationCharge"/&amp;gt;
 *     &amp;lt;enumeration value="ZP_DomesticSegment"/&amp;gt;
 *     &amp;lt;enumeration value="AY_SecurityFee"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AirTaxExemptionEnum")
@XmlEnum
public enum AirTaxExemptionEnum {


    /**
     * Free Baggage Allowance
     * 
     */
    @XmlEnumValue("PFC_PassengerFacilitationCharge")
    PFC_PASSENGER_FACILITATION_CHARGE("PFC_PassengerFacilitationCharge"),

    /**
     * Baggage Charges
     * 
     */
    @XmlEnumValue("ZP_DomesticSegment")
    ZP_DOMESTIC_SEGMENT("ZP_DomesticSegment"),

    /**
     * Flight-Related.
     * 
     */
    @XmlEnumValue("AY_SecurityFee")
    AY_SECURITY_FEE("AY_SecurityFee"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support to support additional compartment type. The Value corresponding to "Other_" will be specified in the  "Value" attribute. See CompartmentType.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    AirTaxExemptionEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AirTaxExemptionEnum fromValue(String v) {
        for (AirTaxExemptionEnum c: AirTaxExemptionEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
