
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A collection of hotel search criteria.
 * 
 * &lt;p&gt;Clase Java para DynamicPkgHotelSearchType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DynamicPkgHotelSearchType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgSearchType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HotelSearchCriteria" type="{http://www.opentravel.org/OTA/2003/05}HotelSearchCriteriaType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HotelReservationIDs" type="{http://www.opentravel.org/OTA/2003/05}HotelReservationIDsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="RequestType" type="{http://www.opentravel.org/OTA/2003/05}HotelComponentSearchType" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicPkgHotelSearchType", propOrder = {
    "hotelSearchCriteria",
    "hotelReservationIDs",
    "tpaExtensions"
})
public class DynamicPkgHotelSearchType
    extends DynamicPkgSearchType
{

    @XmlElement(name = "HotelSearchCriteria")
    protected HotelSearchCriteriaType hotelSearchCriteria;
    @XmlElement(name = "HotelReservationIDs")
    protected HotelReservationIDsType hotelReservationIDs;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "RequestType")
    protected HotelComponentSearchType requestType;

    /**
     * Obtiene el valor de la propiedad hotelSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelSearchCriteriaType }
     *     
     */
    public HotelSearchCriteriaType getHotelSearchCriteria() {
        return hotelSearchCriteria;
    }

    /**
     * Define el valor de la propiedad hotelSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelSearchCriteriaType }
     *     
     */
    public void setHotelSearchCriteria(HotelSearchCriteriaType value) {
        this.hotelSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelReservationIDs.
     * 
     * @return
     *     possible object is
     *     {@link HotelReservationIDsType }
     *     
     */
    public HotelReservationIDsType getHotelReservationIDs() {
        return hotelReservationIDs;
    }

    /**
     * Define el valor de la propiedad hotelReservationIDs.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelReservationIDsType }
     *     
     */
    public void setHotelReservationIDs(HotelReservationIDsType value) {
        this.hotelReservationIDs = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad requestType.
     * 
     * @return
     *     possible object is
     *     {@link HotelComponentSearchType }
     *     
     */
    public HotelComponentSearchType getRequestType() {
        return requestType;
    }

    /**
     * Define el valor de la propiedad requestType.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelComponentSearchType }
     *     
     */
    public void setRequestType(HotelComponentSearchType value) {
        this.requestType = value;
    }

}
