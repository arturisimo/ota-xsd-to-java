
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPetType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPetType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Cat"/&amp;gt;
 *     &amp;lt;enumeration value="Dog"/&amp;gt;
 *     &amp;lt;enumeration value="ServiceAnimal"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPetType")
@XmlEnum
public enum ListOfferPetType {

    @XmlEnumValue("Cat")
    CAT("Cat"),
    @XmlEnumValue("Dog")
    DOG("Dog"),
    @XmlEnumValue("ServiceAnimal")
    SERVICE_ANIMAL("ServiceAnimal"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferPetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPetType fromValue(String v) {
        for (ListOfferPetType c: ListOfferPetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
