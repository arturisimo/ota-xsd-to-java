
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferMeasurementSystem.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferMeasurementSystem"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Imperial"/&amp;gt;
 *     &amp;lt;enumeration value="Metric"/&amp;gt;
 *     &amp;lt;enumeration value="US_Customary"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferMeasurementSystem")
@XmlEnum
public enum ListOfferMeasurementSystem {

    @XmlEnumValue("Imperial")
    IMPERIAL("Imperial"),
    @XmlEnumValue("Metric")
    METRIC("Metric"),
    @XmlEnumValue("US_Customary")
    US_CUSTOMARY("US_Customary");
    private final String value;

    ListOfferMeasurementSystem(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferMeasurementSystem fromValue(String v) {
        for (ListOfferMeasurementSystem c: ListOfferMeasurementSystem.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
