
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * The HotelDescriptiveInfo element contains the descriptive information about a hotel property.
 * 
 * &lt;p&gt;Clase Java para HotelDescriptiveInfoRequestType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="HotelDescriptiveInfoRequestType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HotelInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="HotelStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                 &amp;lt;attribute name="HotelStatusCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FacilityInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendMeetingRooms" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendGuestRooms" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendRestaurants" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Policies" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendPolicies" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AreaInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendRefPoints" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendAttractions" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendRecreations" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AffiliationInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendDistribSystems" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendBrands" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendLoyalPrograms" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SendAwards" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ContactInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MultimediaObjects" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ContentInfos" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ContentInfo" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelDescriptiveInfoRequestType", propOrder = {
    "hotelInfo",
    "facilityInfo",
    "policies",
    "areaInfo",
    "affiliationInfo",
    "contactInfo",
    "multimediaObjects",
    "contentInfos",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAHotelDescriptiveInfoRQ.HotelDescriptiveInfos.HotelDescriptiveInfo.class
})
public class HotelDescriptiveInfoRequestType {

    @XmlElement(name = "HotelInfo")
    protected HotelDescriptiveInfoRequestType.HotelInfo hotelInfo;
    @XmlElement(name = "FacilityInfo")
    protected HotelDescriptiveInfoRequestType.FacilityInfo facilityInfo;
    @XmlElement(name = "Policies")
    protected HotelDescriptiveInfoRequestType.Policies policies;
    @XmlElement(name = "AreaInfo")
    protected HotelDescriptiveInfoRequestType.AreaInfo areaInfo;
    @XmlElement(name = "AffiliationInfo")
    protected HotelDescriptiveInfoRequestType.AffiliationInfo affiliationInfo;
    @XmlElement(name = "ContactInfo")
    protected HotelDescriptiveInfoRequestType.ContactInfo contactInfo;
    @XmlElement(name = "MultimediaObjects")
    protected HotelDescriptiveInfoRequestType.MultimediaObjects multimediaObjects;
    @XmlElement(name = "ContentInfos")
    protected HotelDescriptiveInfoRequestType.ContentInfos contentInfos;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "ChainCode")
    protected String chainCode;
    @XmlAttribute(name = "BrandCode")
    protected String brandCode;
    @XmlAttribute(name = "HotelCode")
    protected String hotelCode;
    @XmlAttribute(name = "HotelCityCode")
    protected String hotelCityCode;
    @XmlAttribute(name = "HotelName")
    protected String hotelName;
    @XmlAttribute(name = "HotelCodeContext")
    protected String hotelCodeContext;
    @XmlAttribute(name = "ChainName")
    protected String chainName;
    @XmlAttribute(name = "BrandName")
    protected String brandName;
    @XmlAttribute(name = "AreaID")
    protected String areaID;
    @XmlAttribute(name = "TTIcode")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger ttIcode;

    /**
     * Obtiene el valor de la propiedad hotelInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.HotelInfo }
     *     
     */
    public HotelDescriptiveInfoRequestType.HotelInfo getHotelInfo() {
        return hotelInfo;
    }

    /**
     * Define el valor de la propiedad hotelInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.HotelInfo }
     *     
     */
    public void setHotelInfo(HotelDescriptiveInfoRequestType.HotelInfo value) {
        this.hotelInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad facilityInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.FacilityInfo }
     *     
     */
    public HotelDescriptiveInfoRequestType.FacilityInfo getFacilityInfo() {
        return facilityInfo;
    }

    /**
     * Define el valor de la propiedad facilityInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.FacilityInfo }
     *     
     */
    public void setFacilityInfo(HotelDescriptiveInfoRequestType.FacilityInfo value) {
        this.facilityInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad policies.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.Policies }
     *     
     */
    public HotelDescriptiveInfoRequestType.Policies getPolicies() {
        return policies;
    }

    /**
     * Define el valor de la propiedad policies.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.Policies }
     *     
     */
    public void setPolicies(HotelDescriptiveInfoRequestType.Policies value) {
        this.policies = value;
    }

    /**
     * Obtiene el valor de la propiedad areaInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.AreaInfo }
     *     
     */
    public HotelDescriptiveInfoRequestType.AreaInfo getAreaInfo() {
        return areaInfo;
    }

    /**
     * Define el valor de la propiedad areaInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.AreaInfo }
     *     
     */
    public void setAreaInfo(HotelDescriptiveInfoRequestType.AreaInfo value) {
        this.areaInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad affiliationInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.AffiliationInfo }
     *     
     */
    public HotelDescriptiveInfoRequestType.AffiliationInfo getAffiliationInfo() {
        return affiliationInfo;
    }

    /**
     * Define el valor de la propiedad affiliationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.AffiliationInfo }
     *     
     */
    public void setAffiliationInfo(HotelDescriptiveInfoRequestType.AffiliationInfo value) {
        this.affiliationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad contactInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.ContactInfo }
     *     
     */
    public HotelDescriptiveInfoRequestType.ContactInfo getContactInfo() {
        return contactInfo;
    }

    /**
     * Define el valor de la propiedad contactInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.ContactInfo }
     *     
     */
    public void setContactInfo(HotelDescriptiveInfoRequestType.ContactInfo value) {
        this.contactInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad multimediaObjects.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.MultimediaObjects }
     *     
     */
    public HotelDescriptiveInfoRequestType.MultimediaObjects getMultimediaObjects() {
        return multimediaObjects;
    }

    /**
     * Define el valor de la propiedad multimediaObjects.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.MultimediaObjects }
     *     
     */
    public void setMultimediaObjects(HotelDescriptiveInfoRequestType.MultimediaObjects value) {
        this.multimediaObjects = value;
    }

    /**
     * Obtiene el valor de la propiedad contentInfos.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveInfoRequestType.ContentInfos }
     *     
     */
    public HotelDescriptiveInfoRequestType.ContentInfos getContentInfos() {
        return contentInfos;
    }

    /**
     * Define el valor de la propiedad contentInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveInfoRequestType.ContentInfos }
     *     
     */
    public void setContentInfos(HotelDescriptiveInfoRequestType.ContentInfos value) {
        this.contentInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad chainCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainCode() {
        return chainCode;
    }

    /**
     * Define el valor de la propiedad chainCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainCode(String value) {
        this.chainCode = value;
    }

    /**
     * Obtiene el valor de la propiedad brandCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * Define el valor de la propiedad brandCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCode(String value) {
        this.brandCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Define el valor de la propiedad hotelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCityCode() {
        return hotelCityCode;
    }

    /**
     * Define el valor de la propiedad hotelCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCityCode(String value) {
        this.hotelCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * Define el valor de la propiedad hotelName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelName(String value) {
        this.hotelName = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCodeContext() {
        return hotelCodeContext;
    }

    /**
     * Define el valor de la propiedad hotelCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCodeContext(String value) {
        this.hotelCodeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad chainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainName() {
        return chainName;
    }

    /**
     * Define el valor de la propiedad chainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainName(String value) {
        this.chainName = value;
    }

    /**
     * Obtiene el valor de la propiedad brandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Define el valor de la propiedad brandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Obtiene el valor de la propiedad areaID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaID() {
        return areaID;
    }

    /**
     * Define el valor de la propiedad areaID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaID(String value) {
        this.areaID = value;
    }

    /**
     * Obtiene el valor de la propiedad ttIcode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTTIcode() {
        return ttIcode;
    }

    /**
     * Define el valor de la propiedad ttIcode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTTIcode(BigInteger value) {
        this.ttIcode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendDistribSystems" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendBrands" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendLoyalPrograms" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendAwards" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AffiliationInfo {

        @XmlAttribute(name = "SendDistribSystems")
        protected Boolean sendDistribSystems;
        @XmlAttribute(name = "SendBrands")
        protected Boolean sendBrands;
        @XmlAttribute(name = "SendLoyalPrograms")
        protected Boolean sendLoyalPrograms;
        @XmlAttribute(name = "SendAwards")
        protected Boolean sendAwards;

        /**
         * Obtiene el valor de la propiedad sendDistribSystems.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendDistribSystems() {
            return sendDistribSystems;
        }

        /**
         * Define el valor de la propiedad sendDistribSystems.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendDistribSystems(Boolean value) {
            this.sendDistribSystems = value;
        }

        /**
         * Obtiene el valor de la propiedad sendBrands.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendBrands() {
            return sendBrands;
        }

        /**
         * Define el valor de la propiedad sendBrands.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendBrands(Boolean value) {
            this.sendBrands = value;
        }

        /**
         * Obtiene el valor de la propiedad sendLoyalPrograms.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendLoyalPrograms() {
            return sendLoyalPrograms;
        }

        /**
         * Define el valor de la propiedad sendLoyalPrograms.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendLoyalPrograms(Boolean value) {
            this.sendLoyalPrograms = value;
        }

        /**
         * Obtiene el valor de la propiedad sendAwards.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendAwards() {
            return sendAwards;
        }

        /**
         * Define el valor de la propiedad sendAwards.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendAwards(Boolean value) {
            this.sendAwards = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendRefPoints" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendAttractions" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendRecreations" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AreaInfo {

        @XmlAttribute(name = "SendRefPoints")
        protected Boolean sendRefPoints;
        @XmlAttribute(name = "SendAttractions")
        protected Boolean sendAttractions;
        @XmlAttribute(name = "SendRecreations")
        protected Boolean sendRecreations;

        /**
         * Obtiene el valor de la propiedad sendRefPoints.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendRefPoints() {
            return sendRefPoints;
        }

        /**
         * Define el valor de la propiedad sendRefPoints.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendRefPoints(Boolean value) {
            this.sendRefPoints = value;
        }

        /**
         * Obtiene el valor de la propiedad sendAttractions.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendAttractions() {
            return sendAttractions;
        }

        /**
         * Define el valor de la propiedad sendAttractions.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendAttractions(Boolean value) {
            this.sendAttractions = value;
        }

        /**
         * Obtiene el valor de la propiedad sendRecreations.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendRecreations() {
            return sendRecreations;
        }

        /**
         * Define el valor de la propiedad sendRecreations.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendRecreations(Boolean value) {
            this.sendRecreations = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ContactInfo {

        @XmlAttribute(name = "SendData")
        protected Boolean sendData;

        /**
         * Obtiene el valor de la propiedad sendData.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendData() {
            return sendData;
        }

        /**
         * Define el valor de la propiedad sendData.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendData(Boolean value) {
            this.sendData = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ContentInfo" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contentInfo"
    })
    public static class ContentInfos {

        @XmlElement(name = "ContentInfo", required = true)
        protected List<HotelDescriptiveInfoRequestType.ContentInfos.ContentInfo> contentInfo;

        /**
         * Gets the value of the contentInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contentInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getContentInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelDescriptiveInfoRequestType.ContentInfos.ContentInfo }
         * 
         * 
         */
        public List<HotelDescriptiveInfoRequestType.ContentInfos.ContentInfo> getContentInfo() {
            if (contentInfo == null) {
                contentInfo = new ArrayList<HotelDescriptiveInfoRequestType.ContentInfos.ContentInfo>();
            }
            return this.contentInfo;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ContentInfo {

            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "Name")
            protected String name;

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendMeetingRooms" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendGuestRooms" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SendRestaurants" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FacilityInfo {

        @XmlAttribute(name = "SendMeetingRooms")
        protected Boolean sendMeetingRooms;
        @XmlAttribute(name = "SendGuestRooms")
        protected Boolean sendGuestRooms;
        @XmlAttribute(name = "SendRestaurants")
        protected Boolean sendRestaurants;

        /**
         * Obtiene el valor de la propiedad sendMeetingRooms.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendMeetingRooms() {
            return sendMeetingRooms;
        }

        /**
         * Define el valor de la propiedad sendMeetingRooms.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendMeetingRooms(Boolean value) {
            this.sendMeetingRooms = value;
        }

        /**
         * Obtiene el valor de la propiedad sendGuestRooms.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendGuestRooms() {
            return sendGuestRooms;
        }

        /**
         * Define el valor de la propiedad sendGuestRooms.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendGuestRooms(Boolean value) {
            this.sendGuestRooms = value;
        }

        /**
         * Obtiene el valor de la propiedad sendRestaurants.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendRestaurants() {
            return sendRestaurants;
        }

        /**
         * Define el valor de la propiedad sendRestaurants.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendRestaurants(Boolean value) {
            this.sendRestaurants = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="HotelStatus" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *       &amp;lt;attribute name="HotelStatusCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class HotelInfo {

        @XmlAttribute(name = "SendData")
        protected Boolean sendData;
        @XmlAttribute(name = "HotelStatus")
        protected String hotelStatus;
        @XmlAttribute(name = "HotelStatusCode")
        protected String hotelStatusCode;

        /**
         * Obtiene el valor de la propiedad sendData.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendData() {
            return sendData;
        }

        /**
         * Define el valor de la propiedad sendData.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendData(Boolean value) {
            this.sendData = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelStatus() {
            return hotelStatus;
        }

        /**
         * Define el valor de la propiedad hotelStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelStatus(String value) {
            this.hotelStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelStatusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelStatusCode() {
            return hotelStatusCode;
        }

        /**
         * Define el valor de la propiedad hotelStatusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelStatusCode(String value) {
            this.hotelStatusCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendData" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MultimediaObjects {

        @XmlAttribute(name = "SendData")
        protected Boolean sendData;

        /**
         * Obtiene el valor de la propiedad sendData.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendData() {
            return sendData;
        }

        /**
         * Define el valor de la propiedad sendData.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendData(Boolean value) {
            this.sendData = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SendPolicies" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Policies {

        @XmlAttribute(name = "SendPolicies")
        protected Boolean sendPolicies;

        /**
         * Obtiene el valor de la propiedad sendPolicies.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSendPolicies() {
            return sendPolicies;
        }

        /**
         * Define el valor de la propiedad sendPolicies.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSendPolicies(Boolean value) {
            this.sendPolicies = value;
        }

    }

}
