
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="ProcessingInfo" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;attribute name="OfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="PayWithMilesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="ResponseType"&amp;gt;
 *                     &amp;lt;simpleType&amp;gt;
 *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                         &amp;lt;enumeration value="Baggage Charge"/&amp;gt;
 *                         &amp;lt;enumeration value="Baggage Allowance"/&amp;gt;
 *                         &amp;lt;enumeration value="Baggage Charge and Allowance"/&amp;gt;
 *                         &amp;lt;enumeration value="Baggage List"/&amp;gt;
 *                       &amp;lt;/restriction&amp;gt;
 *                     &amp;lt;/simpleType&amp;gt;
 *                   &amp;lt;/attribute&amp;gt;
 *                   &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                   &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                   &amp;lt;attribute name="USDOT_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="AllowanceAndCharge" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="OriginDestination" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="OriginLocation"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                                       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="DestinationLocation"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                                       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}ConnectionType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="BaggageDetail" maxOccurs="unbounded" minOccurs="0" form="qualified"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Family" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="FilingPricing" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="AddCurrencyConversion" type="{http://www.w3.org/2001/XMLSchema}anyType"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                 &amp;lt;attribute name="DisplayLabel"&amp;gt;
 *                                                   &amp;lt;simpleType&amp;gt;
 *                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                     &amp;lt;/restriction&amp;gt;
 *                                                   &amp;lt;/simpleType&amp;gt;
 *                                                 &amp;lt;/attribute&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="UpgradeMethod"&amp;gt;
 *                                                   &amp;lt;simpleType&amp;gt;
 *                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                       &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
 *                                                       &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
 *                                                     &amp;lt;/restriction&amp;gt;
 *                                                   &amp;lt;/simpleType&amp;gt;
 *                                                 &amp;lt;/attribute&amp;gt;
 *                                                 &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="BookingMethod"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
 *                                             &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
 *                                             &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *                                       &amp;lt;attribute name="OptionType" use="required"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="Discount"/&amp;gt;
 *                                             &amp;lt;enumeration value="Included"/&amp;gt;
 *                                             &amp;lt;enumeration value="Not Applicable"/&amp;gt;
 *                                             &amp;lt;enumeration value="Surcharge"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="CalculationInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType"&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="MarketingAirline" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
 *                                       &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Service" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Family" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
 *                                                                     &amp;lt;sequence&amp;gt;
 *                                                                       &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                                                     &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;/extension&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                             &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                                                     &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
 *                                                                     &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                                                     &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                                                     &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                             &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
 *                                                             &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;element name="ApplyTo" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                           &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                                                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="ApplyTo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                 &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                 &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                 &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
 *                                             &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
 *                                             &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
 *                                             &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
 *                                             &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="Method"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
 *                                             &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
 *                                             &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
 *                                             &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
 *                                             &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
 *                                             &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                       &amp;lt;attribute name="Concept"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="Piece"/&amp;gt;
 *                                             &amp;lt;enumeration value="Weight"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Offer" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirOfferType"&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                             &amp;lt;attribute name="ArrivalDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                             &amp;lt;attribute name="DepartureDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                             &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="RedemptionPoints" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AlternateCurrencyType"&amp;gt;
 *                                       &amp;lt;attribute name="CabinClass" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                                 &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
 *                                                 &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                                 &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                                 &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                             &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                             &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                             &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                             &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="OfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="BaggageList" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="OriginDestination" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="OriginLocation"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                                       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="DestinationLocation"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                                       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}ConnectionType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="MarketingAirline" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;simpleContent&amp;gt;
 *                                     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
 *                                       &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/simpleContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="BaggageDetail" maxOccurs="unbounded" minOccurs="0" form="qualified"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="ServiceFamily" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="ProductGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;sequence&amp;gt;
 *                                                                       &amp;lt;element name="BaggageDetail"&amp;gt;
 *                                                                         &amp;lt;complexType&amp;gt;
 *                                                                           &amp;lt;complexContent&amp;gt;
 *                                                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
 *                                                                               &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                                               &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                                                               &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
 *                                                                                 &amp;lt;simpleType&amp;gt;
 *                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                     &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
 *                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                 &amp;lt;/simpleType&amp;gt;
 *                                                                               &amp;lt;/attribute&amp;gt;
 *                                                                               &amp;lt;attribute name="Method"&amp;gt;
 *                                                                                 &amp;lt;simpleType&amp;gt;
 *                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                     &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
 *                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                 &amp;lt;/simpleType&amp;gt;
 *                                                                               &amp;lt;/attribute&amp;gt;
 *                                                                               &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                                                                               &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                                                               &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                                             &amp;lt;/extension&amp;gt;
 *                                                                           &amp;lt;/complexContent&amp;gt;
 *                                                                         &amp;lt;/complexType&amp;gt;
 *                                                                       &amp;lt;/element&amp;gt;
 *                                                                       &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
 *                                                                         &amp;lt;complexType&amp;gt;
 *                                                                           &amp;lt;complexContent&amp;gt;
 *                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                               &amp;lt;sequence&amp;gt;
 *                                                                                 &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                                 &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                                 &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
 *                                                                                   &amp;lt;complexType&amp;gt;
 *                                                                                     &amp;lt;complexContent&amp;gt;
 *                                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                         &amp;lt;sequence&amp;gt;
 *                                                                                           &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                                         &amp;lt;/sequence&amp;gt;
 *                                                                                         &amp;lt;attribute name="UpgradeMethod"&amp;gt;
 *                                                                                           &amp;lt;simpleType&amp;gt;
 *                                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                               &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
 *                                                                                               &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
 *                                                                                             &amp;lt;/restriction&amp;gt;
 *                                                                                           &amp;lt;/simpleType&amp;gt;
 *                                                                                         &amp;lt;/attribute&amp;gt;
 *                                                                                         &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                                                                                       &amp;lt;/restriction&amp;gt;
 *                                                                                     &amp;lt;/complexContent&amp;gt;
 *                                                                                   &amp;lt;/complexType&amp;gt;
 *                                                                                 &amp;lt;/element&amp;gt;
 *                                                                                 &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                               &amp;lt;/sequence&amp;gt;
 *                                                                               &amp;lt;attribute name="BookingMethod"&amp;gt;
 *                                                                                 &amp;lt;simpleType&amp;gt;
 *                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                     &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
 *                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                 &amp;lt;/simpleType&amp;gt;
 *                                                                               &amp;lt;/attribute&amp;gt;
 *                                                                             &amp;lt;/restriction&amp;gt;
 *                                                                           &amp;lt;/complexContent&amp;gt;
 *                                                                         &amp;lt;/complexType&amp;gt;
 *                                                                       &amp;lt;/element&amp;gt;
 *                                                                       &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
 *                                                                       &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                       &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
 *                                                                         &amp;lt;complexType&amp;gt;
 *                                                                           &amp;lt;complexContent&amp;gt;
 *                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                               &amp;lt;sequence&amp;gt;
 *                                                                                 &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                                 &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                                 &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                                 &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
 *                                                                                 &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                               &amp;lt;/sequence&amp;gt;
 *                                                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
 *                                                                               &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                               &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                               &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                               &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                                                                               &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                               &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                               &amp;lt;attribute name="DisplayLabel"&amp;gt;
 *                                                                                 &amp;lt;simpleType&amp;gt;
 *                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                 &amp;lt;/simpleType&amp;gt;
 *                                                                               &amp;lt;/attribute&amp;gt;
 *                                                                             &amp;lt;/restriction&amp;gt;
 *                                                                           &amp;lt;/complexContent&amp;gt;
 *                                                                         &amp;lt;/complexType&amp;gt;
 *                                                                       &amp;lt;/element&amp;gt;
 *                                                                       &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
 *                                                                         &amp;lt;complexType&amp;gt;
 *                                                                           &amp;lt;simpleContent&amp;gt;
 *                                                                             &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                                                                               &amp;lt;attribute name="Concept"&amp;gt;
 *                                                                                 &amp;lt;simpleType&amp;gt;
 *                                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                                     &amp;lt;enumeration value="Piece"/&amp;gt;
 *                                                                                     &amp;lt;enumeration value="Weight"/&amp;gt;
 *                                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                                 &amp;lt;/simpleType&amp;gt;
 *                                                                               &amp;lt;/attribute&amp;gt;
 *                                                                             &amp;lt;/extension&amp;gt;
 *                                                                           &amp;lt;/simpleContent&amp;gt;
 *                                                                         &amp;lt;/complexType&amp;gt;
 *                                                                       &amp;lt;/element&amp;gt;
 *                                                                       &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
 *                                                                         &amp;lt;complexType&amp;gt;
 *                                                                           &amp;lt;complexContent&amp;gt;
 *                                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                               &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                               &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                               &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                             &amp;lt;/restriction&amp;gt;
 *                                                                           &amp;lt;/complexContent&amp;gt;
 *                                                                         &amp;lt;/complexType&amp;gt;
 *                                                                       &amp;lt;/element&amp;gt;
 *                                                                     &amp;lt;/sequence&amp;gt;
 *                                                                     &amp;lt;attribute name="CodeSource"&amp;gt;
 *                                                                       &amp;lt;simpleType&amp;gt;
 *                                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                           &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *                                                                           &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *                                                                         &amp;lt;/restriction&amp;gt;
 *                                                                       &amp;lt;/simpleType&amp;gt;
 *                                                                     &amp;lt;/attribute&amp;gt;
 *                                                                     &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                     &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                     &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                     &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                     &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                     &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                     &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                             &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;sequence&amp;gt;
 *                                                                       &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
 *                                                                       &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                                                     &amp;lt;/sequence&amp;gt;
 *                                                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
 *                                                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                                                                     &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                     &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                     &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                     &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                             &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                     &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                     &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                           &amp;lt;attribute name="CodeSource"&amp;gt;
 *                                                             &amp;lt;simpleType&amp;gt;
 *                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                 &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *                                                                 &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *                                                               &amp;lt;/restriction&amp;gt;
 *                                                             &amp;lt;/simpleType&amp;gt;
 *                                                           &amp;lt;/attribute&amp;gt;
 *                                                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                           &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="CodeSource"&amp;gt;
 *                                                   &amp;lt;simpleType&amp;gt;
 *                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
 *                                                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
 *                                                     &amp;lt;/restriction&amp;gt;
 *                                                   &amp;lt;/simpleType&amp;gt;
 *                                                 &amp;lt;/attribute&amp;gt;
 *                                                 &amp;lt;attribute name="ServiceCode" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceFamilyEnum" /&amp;gt;
 *                                                 &amp;lt;attribute name="ExtServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                       &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
 *                                             &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
 *                                             &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
 *                                             &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
 *                                             &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="Method"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
 *                                             &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
 *                                             &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
 *                                             &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
 *                                             &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
 *                                             &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                             &amp;lt;attribute name="ArrivalDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                             &amp;lt;attribute name="DepartureDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                             &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "processingInfo",
    "allowanceAndCharge",
    "baggageList",
    "errors"
})
@XmlRootElement(name = "OTA_AirBaggageRS")
public class OTAAirBaggageRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "ProcessingInfo")
    protected OTAAirBaggageRS.ProcessingInfo processingInfo;
    @XmlElement(name = "AllowanceAndCharge")
    protected OTAAirBaggageRS.AllowanceAndCharge allowanceAndCharge;
    @XmlElement(name = "BaggageList")
    protected List<OTAAirBaggageRS.BaggageList> baggageList;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirBaggageRS.ProcessingInfo }
     *     
     */
    public OTAAirBaggageRS.ProcessingInfo getProcessingInfo() {
        return processingInfo;
    }

    /**
     * Define el valor de la propiedad processingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirBaggageRS.ProcessingInfo }
     *     
     */
    public void setProcessingInfo(OTAAirBaggageRS.ProcessingInfo value) {
        this.processingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad allowanceAndCharge.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirBaggageRS.AllowanceAndCharge }
     *     
     */
    public OTAAirBaggageRS.AllowanceAndCharge getAllowanceAndCharge() {
        return allowanceAndCharge;
    }

    /**
     * Define el valor de la propiedad allowanceAndCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirBaggageRS.AllowanceAndCharge }
     *     
     */
    public void setAllowanceAndCharge(OTAAirBaggageRS.AllowanceAndCharge value) {
        this.allowanceAndCharge = value;
    }

    /**
     * Gets the value of the baggageList property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baggageList property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBaggageList().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirBaggageRS.BaggageList }
     * 
     * 
     */
    public List<OTAAirBaggageRS.BaggageList> getBaggageList() {
        if (baggageList == null) {
            baggageList = new ArrayList<OTAAirBaggageRS.BaggageList>();
        }
        return this.baggageList;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginDestination" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OriginLocation"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *                           &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="DestinationLocation"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *                           &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}ConnectionType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="BaggageDetail" maxOccurs="unbounded" minOccurs="0" form="qualified"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Family" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="FilingPricing" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="AddCurrencyConversion" type="{http://www.w3.org/2001/XMLSchema}anyType"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="DisplayLabel"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="UpgradeMethod"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
     *                                           &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                     &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="BookingMethod"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
     *                                 &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
     *                                 &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *                           &amp;lt;attribute name="OptionType" use="required"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Discount"/&amp;gt;
     *                                 &amp;lt;enumeration value="Included"/&amp;gt;
     *                                 &amp;lt;enumeration value="Not Applicable"/&amp;gt;
     *                                 &amp;lt;enumeration value="Surcharge"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CalculationInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType"&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="MarketingAirline" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
     *                           &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Service" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Family" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                                         &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
     *                                                         &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                                                         &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                                                         &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
     *                                                 &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
     *                                       &amp;lt;element name="ApplyTo" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                               &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                     &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                     &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                     &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="ApplyTo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                     &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                     &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
     *                                 &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
     *                                 &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
     *                                 &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
     *                                 &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="Method"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
     *                                 &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
     *                                 &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
     *                                 &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
     *                                 &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
     *                                 &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                           &amp;lt;attribute name="Concept"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Piece"/&amp;gt;
     *                                 &amp;lt;enumeration value="Weight"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Offer" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirOfferType"&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="ArrivalDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="DepartureDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RedemptionPoints" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AlternateCurrencyType"&amp;gt;
     *                           &amp;lt;attribute name="CabinClass" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                     &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                                     &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="OfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestination",
        "totalPrice"
    })
    public static class AllowanceAndCharge {

        @XmlElement(name = "OriginDestination")
        protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination> originDestination;
        @XmlElement(name = "TotalPrice")
        protected OTAAirBaggageRS.AllowanceAndCharge.TotalPrice totalPrice;
        @XmlAttribute(name = "OfferInd")
        protected Boolean offerInd;

        /**
         * Gets the value of the originDestination property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestination property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestination().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination }
         * 
         * 
         */
        public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination> getOriginDestination() {
            if (originDestination == null) {
                originDestination = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination>();
            }
            return this.originDestination;
        }

        /**
         * Obtiene el valor de la propiedad totalPrice.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice }
         *     
         */
        public OTAAirBaggageRS.AllowanceAndCharge.TotalPrice getTotalPrice() {
            return totalPrice;
        }

        /**
         * Define el valor de la propiedad totalPrice.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice }
         *     
         */
        public void setTotalPrice(OTAAirBaggageRS.AllowanceAndCharge.TotalPrice value) {
            this.totalPrice = value;
        }

        /**
         * Obtiene el valor de la propiedad offerInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOfferInd() {
            return offerInd;
        }

        /**
         * Define el valor de la propiedad offerInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOfferInd(Boolean value) {
            this.offerInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OriginLocation"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
         *                 &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="DestinationLocation"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
         *                 &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}ConnectionType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="BaggageDetail" maxOccurs="unbounded" minOccurs="0" form="qualified"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Family" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="FilingPricing" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="AddCurrencyConversion" type="{http://www.w3.org/2001/XMLSchema}anyType"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                           &amp;lt;attribute name="DisplayLabel"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="UpgradeMethod"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
         *                                 &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                           &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="BookingMethod"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
         *                       &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
         *                       &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
         *                 &amp;lt;attribute name="OptionType" use="required"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Discount"/&amp;gt;
         *                       &amp;lt;enumeration value="Included"/&amp;gt;
         *                       &amp;lt;enumeration value="Not Applicable"/&amp;gt;
         *                       &amp;lt;enumeration value="Surcharge"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CalculationInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType"&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="MarketingAirline" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
         *                 &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Service" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Family" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                                               &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
         *                                               &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                                               &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                                               &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
         *                                       &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
         *                             &amp;lt;element name="ApplyTo" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                     &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
         *                           &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                           &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                           &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                           &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="ApplyTo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                           &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                           &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
         *                       &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
         *                       &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
         *                       &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
         *                       &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="Method"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
         *                       &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
         *                       &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
         *                       &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
         *                       &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
         *                       &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *                 &amp;lt;attribute name="Concept"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Piece"/&amp;gt;
         *                       &amp;lt;enumeration value="Weight"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Offer" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirOfferType"&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="ArrivalDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="DepartureDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "originLocation",
            "destinationLocation",
            "connectionLocations",
            "baggageDetail",
            "bookingInstruction",
            "custLoyalty",
            "calculationInfo",
            "marketingAirline",
            "service",
            "ticketBox",
            "offer"
        })
        public static class OriginDestination {

            @XmlElement(name = "OriginLocation", required = true)
            protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.OriginLocation originLocation;
            @XmlElement(name = "DestinationLocation", required = true)
            protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.DestinationLocation destinationLocation;
            @XmlElement(name = "ConnectionLocations")
            protected ConnectionType connectionLocations;
            @XmlElement(name = "BaggageDetail")
            protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail> baggageDetail;
            @XmlElement(name = "BookingInstruction")
            protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction bookingInstruction;
            @XmlElement(name = "CustLoyalty")
            protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CustLoyalty custLoyalty;
            @XmlElement(name = "CalculationInfo")
            protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CalculationInfo> calculationInfo;
            @XmlElement(name = "MarketingAirline")
            protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.MarketingAirline> marketingAirline;
            @XmlElement(name = "Service")
            protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service> service;
            @XmlElement(name = "TicketBox")
            protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.TicketBox ticketBox;
            @XmlElement(name = "Offer")
            protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Offer> offer;
            @XmlAttribute(name = "AllowanceRPH")
            protected String allowanceRPH;
            @XmlAttribute(name = "ArrivalDateTime")
            protected String arrivalDateTime;
            @XmlAttribute(name = "DepartureDateTime")
            protected String departureDateTime;
            @XmlAttribute(name = "OrigDestSequenceRPH")
            protected String origDestSequenceRPH;

            /**
             * Obtiene el valor de la propiedad originLocation.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.OriginLocation }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.OriginLocation getOriginLocation() {
                return originLocation;
            }

            /**
             * Define el valor de la propiedad originLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.OriginLocation }
             *     
             */
            public void setOriginLocation(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.OriginLocation value) {
                this.originLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad destinationLocation.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.DestinationLocation }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.DestinationLocation getDestinationLocation() {
                return destinationLocation;
            }

            /**
             * Define el valor de la propiedad destinationLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.DestinationLocation }
             *     
             */
            public void setDestinationLocation(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.DestinationLocation value) {
                this.destinationLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad connectionLocations.
             * 
             * @return
             *     possible object is
             *     {@link ConnectionType }
             *     
             */
            public ConnectionType getConnectionLocations() {
                return connectionLocations;
            }

            /**
             * Define el valor de la propiedad connectionLocations.
             * 
             * @param value
             *     allowed object is
             *     {@link ConnectionType }
             *     
             */
            public void setConnectionLocations(ConnectionType value) {
                this.connectionLocations = value;
            }

            /**
             * Gets the value of the baggageDetail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baggageDetail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBaggageDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail }
             * 
             * 
             */
            public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail> getBaggageDetail() {
                if (baggageDetail == null) {
                    baggageDetail = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail>();
                }
                return this.baggageDetail;
            }

            /**
             * Obtiene el valor de la propiedad bookingInstruction.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction getBookingInstruction() {
                return bookingInstruction;
            }

            /**
             * Define el valor de la propiedad bookingInstruction.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction }
             *     
             */
            public void setBookingInstruction(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction value) {
                this.bookingInstruction = value;
            }

            /**
             * Obtiene el valor de la propiedad custLoyalty.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CustLoyalty }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CustLoyalty getCustLoyalty() {
                return custLoyalty;
            }

            /**
             * Define el valor de la propiedad custLoyalty.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CustLoyalty }
             *     
             */
            public void setCustLoyalty(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CustLoyalty value) {
                this.custLoyalty = value;
            }

            /**
             * Gets the value of the calculationInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the calculationInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCalculationInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CalculationInfo }
             * 
             * 
             */
            public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CalculationInfo> getCalculationInfo() {
                if (calculationInfo == null) {
                    calculationInfo = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.CalculationInfo>();
                }
                return this.calculationInfo;
            }

            /**
             * Gets the value of the marketingAirline property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingAirline property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMarketingAirline().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.MarketingAirline }
             * 
             * 
             */
            public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.MarketingAirline> getMarketingAirline() {
                if (marketingAirline == null) {
                    marketingAirline = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.MarketingAirline>();
                }
                return this.marketingAirline;
            }

            /**
             * Gets the value of the service property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the service property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getService().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service }
             * 
             * 
             */
            public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service> getService() {
                if (service == null) {
                    service = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service>();
                }
                return this.service;
            }

            /**
             * Obtiene el valor de la propiedad ticketBox.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.TicketBox }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.TicketBox getTicketBox() {
                return ticketBox;
            }

            /**
             * Define el valor de la propiedad ticketBox.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.TicketBox }
             *     
             */
            public void setTicketBox(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.TicketBox value) {
                this.ticketBox = value;
            }

            /**
             * Gets the value of the offer property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the offer property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getOffer().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Offer }
             * 
             * 
             */
            public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Offer> getOffer() {
                if (offer == null) {
                    offer = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Offer>();
                }
                return this.offer;
            }

            /**
             * Obtiene el valor de la propiedad allowanceRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAllowanceRPH() {
                return allowanceRPH;
            }

            /**
             * Define el valor de la propiedad allowanceRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAllowanceRPH(String value) {
                this.allowanceRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getArrivalDateTime() {
                return arrivalDateTime;
            }

            /**
             * Define el valor de la propiedad arrivalDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setArrivalDateTime(String value) {
                this.arrivalDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDepartureDateTime() {
                return departureDateTime;
            }

            /**
             * Define el valor de la propiedad departureDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDepartureDateTime(String value) {
                this.departureDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad origDestSequenceRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOrigDestSequenceRPH() {
                return origDestSequenceRPH;
            }

            /**
             * Define el valor de la propiedad origDestSequenceRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOrigDestSequenceRPH(String value) {
                this.origDestSequenceRPH = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Family" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="FilingPricing" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="AddCurrencyConversion" type="{http://www.w3.org/2001/XMLSchema}anyType"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                 &amp;lt;attribute name="DisplayLabel"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "family",
                "pricing"
            })
            public static class BaggageDetail
                extends BaggageSpecificationType
            {

                @XmlElement(name = "Family")
                protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Family family;
                @XmlElement(name = "Pricing")
                protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Pricing pricing;
                @XmlAttribute(name = "MaximumPieces")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maximumPieces;
                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;
                @XmlAttribute(name = "Quantity")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger quantity;

                /**
                 * Obtiene el valor de la propiedad family.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Family }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Family getFamily() {
                    return family;
                }

                /**
                 * Define el valor de la propiedad family.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Family }
                 *     
                 */
                public void setFamily(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Family value) {
                    this.family = value;
                }

                /**
                 * Obtiene el valor de la propiedad pricing.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Pricing }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Pricing getPricing() {
                    return pricing;
                }

                /**
                 * Define el valor de la propiedad pricing.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Pricing }
                 *     
                 */
                public void setPricing(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BaggageDetail.Pricing value) {
                    this.pricing = value;
                }

                /**
                 * Obtiene el valor de la propiedad maximumPieces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaximumPieces() {
                    return maximumPieces;
                }

                /**
                 * Define el valor de la propiedad maximumPieces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaximumPieces(BigInteger value) {
                    this.maximumPieces = value;
                }

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad quantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getQuantity() {
                    return quantity;
                }

                /**
                 * Define el valor de la propiedad quantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setQuantity(BigInteger value) {
                    this.quantity = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="FilingPricing" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "filingPricing"
                })
                public static class Family
                    extends AncillaryServiceDetailType
                {

                    @XmlElement(name = "FilingPricing")
                    protected Object filingPricing;

                    /**
                     * Obtiene el valor de la propiedad filingPricing.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getFilingPricing() {
                        return filingPricing;
                    }

                    /**
                     * Define el valor de la propiedad filingPricing.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setFilingPricing(Object value) {
                        this.filingPricing = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="AddCurrencyConversion" type="{http://www.w3.org/2001/XMLSchema}anyType"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *       &amp;lt;attribute name="DisplayLabel"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "addCurrencyConversion"
                })
                public static class Pricing {

                    @XmlElement(name = "AddCurrencyConversion", required = true)
                    protected Object addCurrencyConversion;
                    @XmlAttribute(name = "DisplayLabel")
                    protected String displayLabel;
                    @XmlAttribute(name = "CurrencyCode")
                    protected String currencyCode;
                    @XmlAttribute(name = "DecimalPlaces")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger decimalPlaces;
                    @XmlAttribute(name = "Amount")
                    protected BigDecimal amount;

                    /**
                     * Obtiene el valor de la propiedad addCurrencyConversion.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getAddCurrencyConversion() {
                        return addCurrencyConversion;
                    }

                    /**
                     * Define el valor de la propiedad addCurrencyConversion.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setAddCurrencyConversion(Object value) {
                        this.addCurrencyConversion = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad displayLabel.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDisplayLabel() {
                        return displayLabel;
                    }

                    /**
                     * Define el valor de la propiedad displayLabel.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDisplayLabel(String value) {
                        this.displayLabel = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad currencyCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    /**
                     * Define el valor de la propiedad currencyCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad decimalPlaces.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getDecimalPlaces() {
                        return decimalPlaces;
                    }

                    /**
                     * Define el valor de la propiedad decimalPlaces.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setDecimalPlaces(BigInteger value) {
                        this.decimalPlaces = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad amount.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getAmount() {
                        return amount;
                    }

                    /**
                     * Define el valor de la propiedad amount.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setAmount(BigDecimal value) {
                        this.amount = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="UpgradeMethod"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
             *                       &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *                 &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="BookingMethod"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
             *             &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
             *             &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "ssrInfo",
                "osiInfo",
                "upgrade",
                "otherInfo"
            })
            public static class BookingInstruction {

                @XmlElement(name = "SSR_Info")
                protected List<SpecialServiceRequestType> ssrInfo;
                @XmlElement(name = "OSI_Info")
                protected List<OtherServiceInfoType> osiInfo;
                @XmlElement(name = "Upgrade")
                protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction.Upgrade upgrade;
                @XmlElement(name = "OtherInfo")
                protected List<TextDescriptionType> otherInfo;
                @XmlAttribute(name = "BookingMethod")
                protected String bookingMethod;
                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Gets the value of the ssrInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ssrInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getSSRInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link SpecialServiceRequestType }
                 * 
                 * 
                 */
                public List<SpecialServiceRequestType> getSSRInfo() {
                    if (ssrInfo == null) {
                        ssrInfo = new ArrayList<SpecialServiceRequestType>();
                    }
                    return this.ssrInfo;
                }

                /**
                 * Gets the value of the osiInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the osiInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOSIInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OtherServiceInfoType }
                 * 
                 * 
                 */
                public List<OtherServiceInfoType> getOSIInfo() {
                    if (osiInfo == null) {
                        osiInfo = new ArrayList<OtherServiceInfoType>();
                    }
                    return this.osiInfo;
                }

                /**
                 * Obtiene el valor de la propiedad upgrade.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction.Upgrade }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction.Upgrade getUpgrade() {
                    return upgrade;
                }

                /**
                 * Define el valor de la propiedad upgrade.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction.Upgrade }
                 *     
                 */
                public void setUpgrade(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.BookingInstruction.Upgrade value) {
                    this.upgrade = value;
                }

                /**
                 * Gets the value of the otherInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the otherInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOtherInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextDescriptionType }
                 * 
                 * 
                 */
                public List<TextDescriptionType> getOtherInfo() {
                    if (otherInfo == null) {
                        otherInfo = new ArrayList<TextDescriptionType>();
                    }
                    return this.otherInfo;
                }

                /**
                 * Obtiene el valor de la propiedad bookingMethod.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBookingMethod() {
                    return bookingMethod;
                }

                /**
                 * Define el valor de la propiedad bookingMethod.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBookingMethod(String value) {
                    this.bookingMethod = value;
                }

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="UpgradeMethod"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
                 *             &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *       &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "instruction"
                })
                public static class Upgrade {

                    @XmlElement(name = "Instruction")
                    protected List<String> instruction;
                    @XmlAttribute(name = "UpgradeMethod")
                    protected String upgradeMethod;
                    @XmlAttribute(name = "UpgradeDesigCode")
                    protected String upgradeDesigCode;

                    /**
                     * Gets the value of the instruction property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the instruction property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getInstruction().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link String }
                     * 
                     * 
                     */
                    public List<String> getInstruction() {
                        if (instruction == null) {
                            instruction = new ArrayList<String>();
                        }
                        return this.instruction;
                    }

                    /**
                     * Obtiene el valor de la propiedad upgradeMethod.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUpgradeMethod() {
                        return upgradeMethod;
                    }

                    /**
                     * Define el valor de la propiedad upgradeMethod.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUpgradeMethod(String value) {
                        this.upgradeMethod = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad upgradeDesigCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUpgradeDesigCode() {
                        return upgradeDesigCode;
                    }

                    /**
                     * Define el valor de la propiedad upgradeDesigCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUpgradeDesigCode(String value) {
                        this.upgradeDesigCode = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType"&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CalculationInfo
                extends TextDescriptionType
            {

                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
             *       &amp;lt;attribute name="OptionType" use="required"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Discount"/&amp;gt;
             *             &amp;lt;enumeration value="Included"/&amp;gt;
             *             &amp;lt;enumeration value="Not Applicable"/&amp;gt;
             *             &amp;lt;enumeration value="Surcharge"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CustLoyalty {

                @XmlAttribute(name = "OptionType", required = true)
                protected String optionType;
                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;
                @XmlAttribute(name = "ShareSynchInd")
                protected String shareSynchInd;
                @XmlAttribute(name = "ShareMarketInd")
                protected String shareMarketInd;
                @XmlAttribute(name = "ProgramID")
                protected String programID;
                @XmlAttribute(name = "MembershipID")
                protected String membershipID;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "VendorCode")
                protected List<String> vendorCode;
                @XmlAttribute(name = "PrimaryLoyaltyIndicator")
                protected Boolean primaryLoyaltyIndicator;
                @XmlAttribute(name = "AllianceLoyaltyLevelName")
                protected String allianceLoyaltyLevelName;
                @XmlAttribute(name = "CustomerType")
                protected String customerType;
                @XmlAttribute(name = "CustomerValue")
                protected String customerValue;
                @XmlAttribute(name = "Password")
                protected String password;
                @XmlAttribute(name = "LoyalLevel")
                protected String loyalLevel;
                @XmlAttribute(name = "LoyalLevelCode")
                protected Integer loyalLevelCode;
                @XmlAttribute(name = "SingleVendorInd")
                protected String singleVendorInd;
                @XmlAttribute(name = "SignupDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar signupDate;
                @XmlAttribute(name = "EffectiveDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar effectiveDate;
                @XmlAttribute(name = "ExpireDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar expireDate;
                @XmlAttribute(name = "ExpireDateExclusiveIndicator")
                protected Boolean expireDateExclusiveIndicator;
                @XmlAttribute(name = "RPH")
                protected String rph;

                /**
                 * Obtiene el valor de la propiedad optionType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOptionType() {
                    return optionType;
                }

                /**
                 * Define el valor de la propiedad optionType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOptionType(String value) {
                    this.optionType = value;
                }

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareSynchInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareSynchInd() {
                    return shareSynchInd;
                }

                /**
                 * Define el valor de la propiedad shareSynchInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareSynchInd(String value) {
                    this.shareSynchInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareMarketInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareMarketInd() {
                    return shareMarketInd;
                }

                /**
                 * Define el valor de la propiedad shareMarketInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareMarketInd(String value) {
                    this.shareMarketInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad programID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramID() {
                    return programID;
                }

                /**
                 * Define el valor de la propiedad programID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramID(String value) {
                    this.programID = value;
                }

                /**
                 * Obtiene el valor de la propiedad membershipID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMembershipID() {
                    return membershipID;
                }

                /**
                 * Define el valor de la propiedad membershipID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMembershipID(String value) {
                    this.membershipID = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Gets the value of the vendorCode property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getVendorCode().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getVendorCode() {
                    if (vendorCode == null) {
                        vendorCode = new ArrayList<String>();
                    }
                    return this.vendorCode;
                }

                /**
                 * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPrimaryLoyaltyIndicator() {
                    return primaryLoyaltyIndicator;
                }

                /**
                 * Define el valor de la propiedad primaryLoyaltyIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPrimaryLoyaltyIndicator(Boolean value) {
                    this.primaryLoyaltyIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllianceLoyaltyLevelName() {
                    return allianceLoyaltyLevelName;
                }

                /**
                 * Define el valor de la propiedad allianceLoyaltyLevelName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllianceLoyaltyLevelName(String value) {
                    this.allianceLoyaltyLevelName = value;
                }

                /**
                 * Obtiene el valor de la propiedad customerType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerType() {
                    return customerType;
                }

                /**
                 * Define el valor de la propiedad customerType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerType(String value) {
                    this.customerType = value;
                }

                /**
                 * Obtiene el valor de la propiedad customerValue.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerValue() {
                    return customerValue;
                }

                /**
                 * Define el valor de la propiedad customerValue.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerValue(String value) {
                    this.customerValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad password.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPassword() {
                    return password;
                }

                /**
                 * Define el valor de la propiedad password.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPassword(String value) {
                    this.password = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyalLevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLoyalLevel() {
                    return loyalLevel;
                }

                /**
                 * Define el valor de la propiedad loyalLevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLoyalLevel(String value) {
                    this.loyalLevel = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyalLevelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getLoyalLevelCode() {
                    return loyalLevelCode;
                }

                /**
                 * Define el valor de la propiedad loyalLevelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setLoyalLevelCode(Integer value) {
                    this.loyalLevelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad singleVendorInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSingleVendorInd() {
                    return singleVendorInd;
                }

                /**
                 * Define el valor de la propiedad singleVendorInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSingleVendorInd(String value) {
                    this.singleVendorInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad signupDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getSignupDate() {
                    return signupDate;
                }

                /**
                 * Define el valor de la propiedad signupDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setSignupDate(XMLGregorianCalendar value) {
                    this.signupDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad effectiveDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getEffectiveDate() {
                    return effectiveDate;
                }

                /**
                 * Define el valor de la propiedad effectiveDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setEffectiveDate(XMLGregorianCalendar value) {
                    this.effectiveDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad expireDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getExpireDate() {
                    return expireDate;
                }

                /**
                 * Define el valor de la propiedad expireDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setExpireDate(XMLGregorianCalendar value) {
                    this.expireDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isExpireDateExclusiveIndicator() {
                    return expireDateExclusiveIndicator;
                }

                /**
                 * Define el valor de la propiedad expireDateExclusiveIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setExpireDateExclusiveIndicator(Boolean value) {
                    this.expireDateExclusiveIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRPH(String value) {
                    this.rph = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
             *       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DestinationLocation
                extends LocationType
            {

                @XmlAttribute(name = "MultiAirportCityInd")
                protected Boolean multiAirportCityInd;
                @XmlAttribute(name = "AlternateLocationInd")
                protected Boolean alternateLocationInd;

                /**
                 * Obtiene el valor de la propiedad multiAirportCityInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMultiAirportCityInd() {
                    return multiAirportCityInd;
                }

                /**
                 * Define el valor de la propiedad multiAirportCityInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMultiAirportCityInd(Boolean value) {
                    this.multiAirportCityInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad alternateLocationInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAlternateLocationInd() {
                    return alternateLocationInd;
                }

                /**
                 * Define el valor de la propiedad alternateLocationInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAlternateLocationInd(Boolean value) {
                    this.alternateLocationInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
             *       &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MarketingAirline
                extends CompanyNameType
            {

                @XmlAttribute(name = "MostSignificantInd")
                protected Boolean mostSignificantInd;
                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Obtiene el valor de la propiedad mostSignificantInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMostSignificantInd() {
                    return mostSignificantInd;
                }

                /**
                 * Define el valor de la propiedad mostSignificantInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMostSignificantInd(Boolean value) {
                    this.mostSignificantInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirOfferType"&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Offer
                extends AirOfferType
            {

                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
             *       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OriginLocation
                extends LocationType
            {

                @XmlAttribute(name = "MultiAirportCityInd")
                protected Boolean multiAirportCityInd;
                @XmlAttribute(name = "AlternateLocationInd")
                protected Boolean alternateLocationInd;

                /**
                 * Obtiene el valor de la propiedad multiAirportCityInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMultiAirportCityInd() {
                    return multiAirportCityInd;
                }

                /**
                 * Define el valor de la propiedad multiAirportCityInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMultiAirportCityInd(Boolean value) {
                    this.multiAirportCityInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad alternateLocationInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAlternateLocationInd() {
                    return alternateLocationInd;
                }

                /**
                 * Define el valor de la propiedad alternateLocationInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAlternateLocationInd(Boolean value) {
                    this.alternateLocationInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Family" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *                                     &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
             *                                     &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *                                     &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *                                     &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
             *                             &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
             *                   &amp;lt;element name="ApplyTo" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                           &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
             *                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="ApplyTo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                 &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                 &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
             *             &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
             *             &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
             *             &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
             *             &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="Method"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
             *             &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
             *             &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
             *             &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
             *             &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
             *             &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
             *       &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "family",
                "pricing",
                "applyTo",
                "atpcoDiagnostic"
            })
            public static class Service {

                @XmlElement(name = "Family")
                protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Family family;
                @XmlElement(name = "Pricing")
                protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing pricing;
                @XmlElement(name = "ApplyTo")
                protected List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ApplyTo> applyTo;
                @XmlElement(name = "ATPCO_Diagnostic")
                protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ATPCODiagnostic atpcoDiagnostic;
                @XmlAttribute(name = "EMD_TypeValue")
                protected String emdTypeValue;
                @XmlAttribute(name = "Method")
                protected String method;
                @XmlAttribute(name = "ServiceLocation")
                protected String serviceLocation;
                @XmlAttribute(name = "ServiceDate")
                protected String serviceDate;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Obtiene el valor de la propiedad family.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Family }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Family getFamily() {
                    return family;
                }

                /**
                 * Define el valor de la propiedad family.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Family }
                 *     
                 */
                public void setFamily(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Family value) {
                    this.family = value;
                }

                /**
                 * Obtiene el valor de la propiedad pricing.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing getPricing() {
                    return pricing;
                }

                /**
                 * Define el valor de la propiedad pricing.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing }
                 *     
                 */
                public void setPricing(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing value) {
                    this.pricing = value;
                }

                /**
                 * Gets the value of the applyTo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the applyTo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getApplyTo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ApplyTo }
                 * 
                 * 
                 */
                public List<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ApplyTo> getApplyTo() {
                    if (applyTo == null) {
                        applyTo = new ArrayList<OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ApplyTo>();
                    }
                    return this.applyTo;
                }

                /**
                 * Obtiene el valor de la propiedad atpcoDiagnostic.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ATPCODiagnostic }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ATPCODiagnostic getATPCODiagnostic() {
                    return atpcoDiagnostic;
                }

                /**
                 * Define el valor de la propiedad atpcoDiagnostic.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ATPCODiagnostic }
                 *     
                 */
                public void setATPCODiagnostic(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.ATPCODiagnostic value) {
                    this.atpcoDiagnostic = value;
                }

                /**
                 * Obtiene el valor de la propiedad emdTypeValue.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEMDTypeValue() {
                    return emdTypeValue;
                }

                /**
                 * Define el valor de la propiedad emdTypeValue.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEMDTypeValue(String value) {
                    this.emdTypeValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad method.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMethod() {
                    return method;
                }

                /**
                 * Define el valor de la propiedad method.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMethod(String value) {
                    this.method = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceLocation.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceLocation() {
                    return serviceLocation;
                }

                /**
                 * Define el valor de la propiedad serviceLocation.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceLocation(String value) {
                    this.serviceLocation = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceDate() {
                    return serviceDate;
                }

                /**
                 * Define el valor de la propiedad serviceDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceDate(String value) {
                    this.serviceDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *       &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *       &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ATPCODiagnostic {

                    @XmlAttribute(name = "S5")
                    protected BigInteger s5;
                    @XmlAttribute(name = "S7")
                    protected BigInteger s7;
                    @XmlAttribute(name = "S7SequenceNo")
                    protected BigInteger s7SequenceNo;

                    /**
                     * Obtiene el valor de la propiedad s5.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getS5() {
                        return s5;
                    }

                    /**
                     * Define el valor de la propiedad s5.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setS5(BigInteger value) {
                        this.s5 = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad s7.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getS7() {
                        return s7;
                    }

                    /**
                     * Define el valor de la propiedad s7.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setS7(BigInteger value) {
                        this.s7 = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad s7SequenceNo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getS7SequenceNo() {
                        return s7SequenceNo;
                    }

                    /**
                     * Define el valor de la propiedad s7SequenceNo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setS7SequenceNo(BigInteger value) {
                        this.s7SequenceNo = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ApplyTo {

                    @XmlAttribute(name = "AllowanceRPH")
                    protected String allowanceRPH;
                    @XmlAttribute(name = "OrigDestSequenceRPH")
                    protected String origDestSequenceRPH;
                    @XmlAttribute(name = "ServiceRPH")
                    protected String serviceRPH;
                    @XmlAttribute(name = "TravelerRPH")
                    protected String travelerRPH;

                    /**
                     * Obtiene el valor de la propiedad allowanceRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAllowanceRPH() {
                        return allowanceRPH;
                    }

                    /**
                     * Define el valor de la propiedad allowanceRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAllowanceRPH(String value) {
                        this.allowanceRPH = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad origDestSequenceRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOrigDestSequenceRPH() {
                        return origDestSequenceRPH;
                    }

                    /**
                     * Define el valor de la propiedad origDestSequenceRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOrigDestSequenceRPH(String value) {
                        this.origDestSequenceRPH = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad serviceRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getServiceRPH() {
                        return serviceRPH;
                    }

                    /**
                     * Define el valor de la propiedad serviceRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setServiceRPH(String value) {
                        this.serviceRPH = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad travelerRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTravelerRPH() {
                        return travelerRPH;
                    }

                    /**
                     * Define el valor de la propiedad travelerRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTravelerRPH(String value) {
                        this.travelerRPH = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Family
                    extends AncillaryServiceDetailType
                {


                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *                           &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
                 *                           &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                 *                           &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                 *                           &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
                 *                   &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                 *         &amp;lt;element name="ApplyTo" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *                 &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                 *       &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "taxInfo",
                    "appliedRule",
                    "pricingQualifier",
                    "redemptionPoints",
                    "applyTo"
                })
                public static class Pricing {

                    @XmlElement(name = "TaxInfo")
                    protected List<TaxType> taxInfo;
                    @XmlElement(name = "AppliedRule")
                    protected List<AppliedRuleType> appliedRule;
                    @XmlElement(name = "PricingQualifier")
                    protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier pricingQualifier;
                    @XmlElement(name = "RedemptionPoints")
                    protected AirRedemptionMilesType redemptionPoints;
                    @XmlElement(name = "ApplyTo")
                    protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.ApplyTo applyTo;
                    @XmlAttribute(name = "PreTaxAmount")
                    protected BigDecimal preTaxAmount;
                    @XmlAttribute(name = "TaxAmount")
                    protected BigDecimal taxAmount;
                    @XmlAttribute(name = "Amount")
                    protected BigDecimal amount;
                    @XmlAttribute(name = "BaseNUC_Amount")
                    protected BigDecimal baseNUCAmount;
                    @XmlAttribute(name = "FromCurrency")
                    protected String fromCurrency;
                    @XmlAttribute(name = "ToCurrency")
                    protected String toCurrency;
                    @XmlAttribute(name = "Rate")
                    protected BigDecimal rate;
                    @XmlAttribute(name = "Date")
                    @XmlSchemaType(name = "date")
                    protected XMLGregorianCalendar date;
                    @XmlAttribute(name = "CurrencyCode")
                    protected String currencyCode;
                    @XmlAttribute(name = "DecimalPlaces")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger decimalPlaces;

                    /**
                     * Gets the value of the taxInfo property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxInfo property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getTaxInfo().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link TaxType }
                     * 
                     * 
                     */
                    public List<TaxType> getTaxInfo() {
                        if (taxInfo == null) {
                            taxInfo = new ArrayList<TaxType>();
                        }
                        return this.taxInfo;
                    }

                    /**
                     * Gets the value of the appliedRule property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the appliedRule property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getAppliedRule().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link AppliedRuleType }
                     * 
                     * 
                     */
                    public List<AppliedRuleType> getAppliedRule() {
                        if (appliedRule == null) {
                            appliedRule = new ArrayList<AppliedRuleType>();
                        }
                        return this.appliedRule;
                    }

                    /**
                     * Obtiene el valor de la propiedad pricingQualifier.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier }
                     *     
                     */
                    public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier getPricingQualifier() {
                        return pricingQualifier;
                    }

                    /**
                     * Define el valor de la propiedad pricingQualifier.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier }
                     *     
                     */
                    public void setPricingQualifier(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier value) {
                        this.pricingQualifier = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad redemptionPoints.
                     * 
                     * @return
                     *     possible object is
                     *     {@link AirRedemptionMilesType }
                     *     
                     */
                    public AirRedemptionMilesType getRedemptionPoints() {
                        return redemptionPoints;
                    }

                    /**
                     * Define el valor de la propiedad redemptionPoints.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link AirRedemptionMilesType }
                     *     
                     */
                    public void setRedemptionPoints(AirRedemptionMilesType value) {
                        this.redemptionPoints = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad applyTo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.ApplyTo }
                     *     
                     */
                    public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.ApplyTo getApplyTo() {
                        return applyTo;
                    }

                    /**
                     * Define el valor de la propiedad applyTo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.ApplyTo }
                     *     
                     */
                    public void setApplyTo(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.ApplyTo value) {
                        this.applyTo = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad preTaxAmount.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPreTaxAmount() {
                        return preTaxAmount;
                    }

                    /**
                     * Define el valor de la propiedad preTaxAmount.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPreTaxAmount(BigDecimal value) {
                        this.preTaxAmount = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad taxAmount.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getTaxAmount() {
                        return taxAmount;
                    }

                    /**
                     * Define el valor de la propiedad taxAmount.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setTaxAmount(BigDecimal value) {
                        this.taxAmount = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad amount.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getAmount() {
                        return amount;
                    }

                    /**
                     * Define el valor de la propiedad amount.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setAmount(BigDecimal value) {
                        this.amount = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad baseNUCAmount.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getBaseNUCAmount() {
                        return baseNUCAmount;
                    }

                    /**
                     * Define el valor de la propiedad baseNUCAmount.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setBaseNUCAmount(BigDecimal value) {
                        this.baseNUCAmount = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad fromCurrency.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFromCurrency() {
                        return fromCurrency;
                    }

                    /**
                     * Define el valor de la propiedad fromCurrency.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFromCurrency(String value) {
                        this.fromCurrency = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad toCurrency.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getToCurrency() {
                        return toCurrency;
                    }

                    /**
                     * Define el valor de la propiedad toCurrency.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setToCurrency(String value) {
                        this.toCurrency = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad rate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getRate() {
                        return rate;
                    }

                    /**
                     * Define el valor de la propiedad rate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setRate(BigDecimal value) {
                        this.rate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad date.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getDate() {
                        return date;
                    }

                    /**
                     * Define el valor de la propiedad date.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setDate(XMLGregorianCalendar value) {
                        this.date = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad currencyCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    /**
                     * Define el valor de la propiedad currencyCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad decimalPlaces.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getDecimalPlaces() {
                        return decimalPlaces;
                    }

                    /**
                     * Define el valor de la propiedad decimalPlaces.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setDecimalPlaces(BigInteger value) {
                        this.decimalPlaces = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *       &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class ApplyTo {

                        @XmlAttribute(name = "TravelerRPH")
                        protected String travelerRPH;
                        @XmlAttribute(name = "OrigDestSequenceRPH")
                        protected String origDestSequenceRPH;

                        /**
                         * Obtiene el valor de la propiedad travelerRPH.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getTravelerRPH() {
                            return travelerRPH;
                        }

                        /**
                         * Define el valor de la propiedad travelerRPH.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setTravelerRPH(String value) {
                            this.travelerRPH = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad origDestSequenceRPH.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOrigDestSequenceRPH() {
                            return origDestSequenceRPH;
                        }

                        /**
                         * Define el valor de la propiedad origDestSequenceRPH.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOrigDestSequenceRPH(String value) {
                            this.origDestSequenceRPH = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                     *                 &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
                     *                 &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                     *                 &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                     *                 &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
                     *         &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "negotiatedFare",
                        "originalIssueInfo",
                        "other",
                        "privateFare"
                    })
                    public static class PricingQualifier {

                        @XmlElement(name = "NegotiatedFare")
                        protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.NegotiatedFare negotiatedFare;
                        @XmlElement(name = "OriginalIssueInfo")
                        protected OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.OriginalIssueInfo originalIssueInfo;
                        @XmlElement(name = "Other")
                        protected AirPricingQualifierType other;
                        @XmlElement(name = "PrivateFare")
                        protected PrivateFareType privateFare;

                        /**
                         * Obtiene el valor de la propiedad negotiatedFare.
                         * 
                         * @return
                         *     possible object is
                         *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.NegotiatedFare }
                         *     
                         */
                        public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.NegotiatedFare getNegotiatedFare() {
                            return negotiatedFare;
                        }

                        /**
                         * Define el valor de la propiedad negotiatedFare.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.NegotiatedFare }
                         *     
                         */
                        public void setNegotiatedFare(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.NegotiatedFare value) {
                            this.negotiatedFare = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad originalIssueInfo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.OriginalIssueInfo }
                         *     
                         */
                        public OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.OriginalIssueInfo getOriginalIssueInfo() {
                            return originalIssueInfo;
                        }

                        /**
                         * Define el valor de la propiedad originalIssueInfo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.OriginalIssueInfo }
                         *     
                         */
                        public void setOriginalIssueInfo(OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.OriginalIssueInfo value) {
                            this.originalIssueInfo = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad other.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirPricingQualifierType }
                         *     
                         */
                        public AirPricingQualifierType getOther() {
                            return other;
                        }

                        /**
                         * Define el valor de la propiedad other.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirPricingQualifierType }
                         *     
                         */
                        public void setOther(AirPricingQualifierType value) {
                            this.other = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad privateFare.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PrivateFareType }
                         *     
                         */
                        public PrivateFareType getPrivateFare() {
                            return privateFare;
                        }

                        /**
                         * Define el valor de la propiedad privateFare.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PrivateFareType }
                         *     
                         */
                        public void setPrivateFare(PrivateFareType value) {
                            this.privateFare = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "supplier"
                        })
                        public static class NegotiatedFare
                            extends FareInfoType
                        {

                            @XmlElement(name = "Supplier")
                            protected CompanyNameType supplier;

                            /**
                             * Obtiene el valor de la propiedad supplier.
                             * 
                             * @return
                             *     possible object is
                             *     {@link CompanyNameType }
                             *     
                             */
                            public CompanyNameType getSupplier() {
                                return supplier;
                            }

                            /**
                             * Define el valor de la propiedad supplier.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link CompanyNameType }
                             *     
                             */
                            public void setSupplier(CompanyNameType value) {
                                this.supplier = value;
                            }

                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                         *       &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
                         *       &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                         *       &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                         *       &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class OriginalIssueInfo {

                            @XmlAttribute(name = "TicketDocumentNbr")
                            protected String ticketDocumentNbr;
                            @XmlAttribute(name = "IssuingAgentID")
                            protected String issuingAgentID;
                            @XmlAttribute(name = "DateOfIssue")
                            @XmlSchemaType(name = "date")
                            protected XMLGregorianCalendar dateOfIssue;
                            @XmlAttribute(name = "LocationCode")
                            protected String locationCode;
                            @XmlAttribute(name = "IssuingAirlineCode")
                            protected String issuingAirlineCode;

                            /**
                             * Obtiene el valor de la propiedad ticketDocumentNbr.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getTicketDocumentNbr() {
                                return ticketDocumentNbr;
                            }

                            /**
                             * Define el valor de la propiedad ticketDocumentNbr.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setTicketDocumentNbr(String value) {
                                this.ticketDocumentNbr = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad issuingAgentID.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getIssuingAgentID() {
                                return issuingAgentID;
                            }

                            /**
                             * Define el valor de la propiedad issuingAgentID.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setIssuingAgentID(String value) {
                                this.issuingAgentID = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad dateOfIssue.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getDateOfIssue() {
                                return dateOfIssue;
                            }

                            /**
                             * Define el valor de la propiedad dateOfIssue.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setDateOfIssue(XMLGregorianCalendar value) {
                                this.dateOfIssue = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad locationCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getLocationCode() {
                                return locationCode;
                            }

                            /**
                             * Define el valor de la propiedad locationCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setLocationCode(String value) {
                                this.locationCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad issuingAirlineCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getIssuingAirlineCode() {
                                return issuingAirlineCode;
                            }

                            /**
                             * Define el valor de la propiedad issuingAirlineCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setIssuingAirlineCode(String value) {
                                this.issuingAirlineCode = value;
                            }

                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *       &amp;lt;attribute name="Concept"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Piece"/&amp;gt;
             *             &amp;lt;enumeration value="Weight"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class TicketBox {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Concept")
                protected String concept;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad concept.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getConcept() {
                    return concept;
                }

                /**
                 * Define el valor de la propiedad concept.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setConcept(String value) {
                    this.concept = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RedemptionPoints" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AlternateCurrencyType"&amp;gt;
         *                 &amp;lt;attribute name="CabinClass" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="PricingQualifier" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                           &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                           &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
         *       &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "taxInfo",
            "redemptionPoints",
            "appliedRule",
            "pricingQualifier"
        })
        public static class TotalPrice {

            @XmlElement(name = "TaxInfo")
            protected List<TaxType> taxInfo;
            @XmlElement(name = "RedemptionPoints")
            protected OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.RedemptionPoints redemptionPoints;
            @XmlElement(name = "AppliedRule")
            protected List<AppliedRuleType> appliedRule;
            @XmlElement(name = "PricingQualifier")
            protected OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier pricingQualifier;
            @XmlAttribute(name = "PreTaxAmount")
            protected BigDecimal preTaxAmount;
            @XmlAttribute(name = "TaxAmount")
            protected BigDecimal taxAmount;
            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;
            @XmlAttribute(name = "BaseNUC_Amount")
            protected BigDecimal baseNUCAmount;
            @XmlAttribute(name = "FromCurrency")
            protected String fromCurrency;
            @XmlAttribute(name = "ToCurrency")
            protected String toCurrency;
            @XmlAttribute(name = "Rate")
            protected BigDecimal rate;
            @XmlAttribute(name = "Date")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;
            @XmlAttribute(name = "CurrencyCode")
            protected String currencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;

            /**
             * Gets the value of the taxInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTaxInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link TaxType }
             * 
             * 
             */
            public List<TaxType> getTaxInfo() {
                if (taxInfo == null) {
                    taxInfo = new ArrayList<TaxType>();
                }
                return this.taxInfo;
            }

            /**
             * Obtiene el valor de la propiedad redemptionPoints.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.RedemptionPoints }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.RedemptionPoints getRedemptionPoints() {
                return redemptionPoints;
            }

            /**
             * Define el valor de la propiedad redemptionPoints.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.RedemptionPoints }
             *     
             */
            public void setRedemptionPoints(OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.RedemptionPoints value) {
                this.redemptionPoints = value;
            }

            /**
             * Gets the value of the appliedRule property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the appliedRule property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAppliedRule().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AppliedRuleType }
             * 
             * 
             */
            public List<AppliedRuleType> getAppliedRule() {
                if (appliedRule == null) {
                    appliedRule = new ArrayList<AppliedRuleType>();
                }
                return this.appliedRule;
            }

            /**
             * Obtiene el valor de la propiedad pricingQualifier.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier }
             *     
             */
            public OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier getPricingQualifier() {
                return pricingQualifier;
            }

            /**
             * Define el valor de la propiedad pricingQualifier.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier }
             *     
             */
            public void setPricingQualifier(OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier value) {
                this.pricingQualifier = value;
            }

            /**
             * Obtiene el valor de la propiedad preTaxAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPreTaxAmount() {
                return preTaxAmount;
            }

            /**
             * Define el valor de la propiedad preTaxAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPreTaxAmount(BigDecimal value) {
                this.preTaxAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad taxAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTaxAmount() {
                return taxAmount;
            }

            /**
             * Define el valor de la propiedad taxAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTaxAmount(BigDecimal value) {
                this.taxAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

            /**
             * Obtiene el valor de la propiedad baseNUCAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBaseNUCAmount() {
                return baseNUCAmount;
            }

            /**
             * Define el valor de la propiedad baseNUCAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBaseNUCAmount(BigDecimal value) {
                this.baseNUCAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad fromCurrency.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFromCurrency() {
                return fromCurrency;
            }

            /**
             * Define el valor de la propiedad fromCurrency.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFromCurrency(String value) {
                this.fromCurrency = value;
            }

            /**
             * Obtiene el valor de la propiedad toCurrency.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getToCurrency() {
                return toCurrency;
            }

            /**
             * Define el valor de la propiedad toCurrency.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setToCurrency(String value) {
                this.toCurrency = value;
            }

            /**
             * Obtiene el valor de la propiedad rate.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getRate() {
                return rate;
            }

            /**
             * Define el valor de la propiedad rate.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setRate(BigDecimal value) {
                this.rate = value;
            }

            /**
             * Obtiene el valor de la propiedad date.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDate() {
                return date;
            }

            /**
             * Define el valor de la propiedad date.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDate(XMLGregorianCalendar value) {
                this.date = value;
            }

            /**
             * Obtiene el valor de la propiedad currencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Define el valor de la propiedad currencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="NegotiatedFare" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *                 &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *                 &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Other" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="PrivateFare" type="{http://www.opentravel.org/OTA/2003/05}PrivateFareType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "negotiatedFare",
                "originalIssueInfo",
                "other",
                "privateFare"
            })
            public static class PricingQualifier {

                @XmlElement(name = "NegotiatedFare")
                protected OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.NegotiatedFare negotiatedFare;
                @XmlElement(name = "OriginalIssueInfo")
                protected OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.OriginalIssueInfo originalIssueInfo;
                @XmlElement(name = "Other")
                protected AirPricingQualifierType other;
                @XmlElement(name = "PrivateFare")
                protected PrivateFareType privateFare;

                /**
                 * Obtiene el valor de la propiedad negotiatedFare.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.NegotiatedFare }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.NegotiatedFare getNegotiatedFare() {
                    return negotiatedFare;
                }

                /**
                 * Define el valor de la propiedad negotiatedFare.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.NegotiatedFare }
                 *     
                 */
                public void setNegotiatedFare(OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.NegotiatedFare value) {
                    this.negotiatedFare = value;
                }

                /**
                 * Obtiene el valor de la propiedad originalIssueInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.OriginalIssueInfo }
                 *     
                 */
                public OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.OriginalIssueInfo getOriginalIssueInfo() {
                    return originalIssueInfo;
                }

                /**
                 * Define el valor de la propiedad originalIssueInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.OriginalIssueInfo }
                 *     
                 */
                public void setOriginalIssueInfo(OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.OriginalIssueInfo value) {
                    this.originalIssueInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad other.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirPricingQualifierType }
                 *     
                 */
                public AirPricingQualifierType getOther() {
                    return other;
                }

                /**
                 * Define el valor de la propiedad other.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirPricingQualifierType }
                 *     
                 */
                public void setOther(AirPricingQualifierType value) {
                    this.other = value;
                }

                /**
                 * Obtiene el valor de la propiedad privateFare.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PrivateFareType }
                 *     
                 */
                public PrivateFareType getPrivateFare() {
                    return privateFare;
                }

                /**
                 * Define el valor de la propiedad privateFare.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PrivateFareType }
                 *     
                 */
                public void setPrivateFare(PrivateFareType value) {
                    this.privateFare = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareInfoType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Supplier" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "supplier"
                })
                public static class NegotiatedFare
                    extends FareInfoType
                {

                    @XmlElement(name = "Supplier")
                    protected CompanyNameType supplier;

                    /**
                     * Obtiene el valor de la propiedad supplier.
                     * 
                     * @return
                     *     possible object is
                     *     {@link CompanyNameType }
                     *     
                     */
                    public CompanyNameType getSupplier() {
                        return supplier;
                    }

                    /**
                     * Define el valor de la propiedad supplier.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link CompanyNameType }
                     *     
                     */
                    public void setSupplier(CompanyNameType value) {
                        this.supplier = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *       &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                 *       &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="IssuingAirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class OriginalIssueInfo {

                    @XmlAttribute(name = "TicketDocumentNbr")
                    protected String ticketDocumentNbr;
                    @XmlAttribute(name = "IssuingAgentID")
                    protected String issuingAgentID;
                    @XmlAttribute(name = "DateOfIssue")
                    @XmlSchemaType(name = "date")
                    protected XMLGregorianCalendar dateOfIssue;
                    @XmlAttribute(name = "LocationCode")
                    protected String locationCode;
                    @XmlAttribute(name = "IssuingAirlineCode")
                    protected String issuingAirlineCode;

                    /**
                     * Obtiene el valor de la propiedad ticketDocumentNbr.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTicketDocumentNbr() {
                        return ticketDocumentNbr;
                    }

                    /**
                     * Define el valor de la propiedad ticketDocumentNbr.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTicketDocumentNbr(String value) {
                        this.ticketDocumentNbr = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad issuingAgentID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getIssuingAgentID() {
                        return issuingAgentID;
                    }

                    /**
                     * Define el valor de la propiedad issuingAgentID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIssuingAgentID(String value) {
                        this.issuingAgentID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dateOfIssue.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getDateOfIssue() {
                        return dateOfIssue;
                    }

                    /**
                     * Define el valor de la propiedad dateOfIssue.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setDateOfIssue(XMLGregorianCalendar value) {
                        this.dateOfIssue = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad locationCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLocationCode() {
                        return locationCode;
                    }

                    /**
                     * Define el valor de la propiedad locationCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLocationCode(String value) {
                        this.locationCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad issuingAirlineCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getIssuingAirlineCode() {
                        return issuingAirlineCode;
                    }

                    /**
                     * Define el valor de la propiedad issuingAirlineCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIssuingAirlineCode(String value) {
                        this.issuingAirlineCode = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AlternateCurrencyType"&amp;gt;
             *       &amp;lt;attribute name="CabinClass" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class RedemptionPoints
                extends AlternateCurrencyType
            {

                @XmlAttribute(name = "CabinClass")
                protected String cabinClass;

                /**
                 * Obtiene el valor de la propiedad cabinClass.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCabinClass() {
                    return cabinClass;
                }

                /**
                 * Define el valor de la propiedad cabinClass.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCabinClass(String value) {
                    this.cabinClass = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginDestination" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="OriginLocation"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *                           &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="DestinationLocation"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *                           &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}ConnectionType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="MarketingAirline" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
     *                           &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="BaggageDetail" maxOccurs="unbounded" minOccurs="0" form="qualified"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="ServiceFamily" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="ProductGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="BaggageDetail"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
     *                                                                   &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                                                   &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                                                   &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
     *                                                                     &amp;lt;simpleType&amp;gt;
     *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                         &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
     *                                                                       &amp;lt;/restriction&amp;gt;
     *                                                                     &amp;lt;/simpleType&amp;gt;
     *                                                                   &amp;lt;/attribute&amp;gt;
     *                                                                   &amp;lt;attribute name="Method"&amp;gt;
     *                                                                     &amp;lt;simpleType&amp;gt;
     *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                         &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
     *                                                                       &amp;lt;/restriction&amp;gt;
     *                                                                     &amp;lt;/simpleType&amp;gt;
     *                                                                   &amp;lt;/attribute&amp;gt;
     *                                                                   &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                                                                   &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                                                                   &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                                                 &amp;lt;/extension&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
     *                                                                       &amp;lt;complexType&amp;gt;
     *                                                                         &amp;lt;complexContent&amp;gt;
     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                             &amp;lt;sequence&amp;gt;
     *                                                                               &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                             &amp;lt;/sequence&amp;gt;
     *                                                                             &amp;lt;attribute name="UpgradeMethod"&amp;gt;
     *                                                                               &amp;lt;simpleType&amp;gt;
     *                                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                                   &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
     *                                                                                   &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
     *                                                                                 &amp;lt;/restriction&amp;gt;
     *                                                                               &amp;lt;/simpleType&amp;gt;
     *                                                                             &amp;lt;/attribute&amp;gt;
     *                                                                             &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                         &amp;lt;/complexContent&amp;gt;
     *                                                                       &amp;lt;/complexType&amp;gt;
     *                                                                     &amp;lt;/element&amp;gt;
     *                                                                     &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                   &amp;lt;attribute name="BookingMethod"&amp;gt;
     *                                                                     &amp;lt;simpleType&amp;gt;
     *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                         &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
     *                                                                       &amp;lt;/restriction&amp;gt;
     *                                                                     &amp;lt;/simpleType&amp;gt;
     *                                                                   &amp;lt;/attribute&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
     *                                                                     &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
     *                                                                   &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                                   &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                                   &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                                   &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                                                                   &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                                   &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                                   &amp;lt;attribute name="DisplayLabel"&amp;gt;
     *                                                                     &amp;lt;simpleType&amp;gt;
     *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                       &amp;lt;/restriction&amp;gt;
     *                                                                     &amp;lt;/simpleType&amp;gt;
     *                                                                   &amp;lt;/attribute&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;simpleContent&amp;gt;
     *                                                                 &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                                                                   &amp;lt;attribute name="Concept"&amp;gt;
     *                                                                     &amp;lt;simpleType&amp;gt;
     *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                                         &amp;lt;enumeration value="Piece"/&amp;gt;
     *                                                                         &amp;lt;enumeration value="Weight"/&amp;gt;
     *                                                                       &amp;lt;/restriction&amp;gt;
     *                                                                     &amp;lt;/simpleType&amp;gt;
     *                                                                   &amp;lt;/attribute&amp;gt;
     *                                                                 &amp;lt;/extension&amp;gt;
     *                                                               &amp;lt;/simpleContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                                   &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                                   &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attribute name="CodeSource"&amp;gt;
     *                                                           &amp;lt;simpleType&amp;gt;
     *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                               &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
     *                                                               &amp;lt;enumeration value="ATPCO"/&amp;gt;
     *                                                             &amp;lt;/restriction&amp;gt;
     *                                                           &amp;lt;/simpleType&amp;gt;
     *                                                         &amp;lt;/attribute&amp;gt;
     *                                                         &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                         &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                         &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                         &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                                 &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                         &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                         &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                               &amp;lt;attribute name="CodeSource"&amp;gt;
     *                                                 &amp;lt;simpleType&amp;gt;
     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                     &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
     *                                                     &amp;lt;enumeration value="ATPCO"/&amp;gt;
     *                                                   &amp;lt;/restriction&amp;gt;
     *                                                 &amp;lt;/simpleType&amp;gt;
     *                                               &amp;lt;/attribute&amp;gt;
     *                                               &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                               &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="CodeSource"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
     *                                           &amp;lt;enumeration value="ATPCO"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                     &amp;lt;attribute name="ServiceCode" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceFamilyEnum" /&amp;gt;
     *                                     &amp;lt;attribute name="ExtServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                     &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
     *                                 &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
     *                                 &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
     *                                 &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
     *                                 &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="Method"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
     *                                 &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
     *                                 &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
     *                                 &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
     *                                 &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
     *                                 &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="ArrivalDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="DepartureDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestination"
    })
    public static class BaggageList {

        @XmlElement(name = "OriginDestination")
        protected List<OTAAirBaggageRS.BaggageList.OriginDestination> originDestination;

        /**
         * Gets the value of the originDestination property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestination property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestination().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirBaggageRS.BaggageList.OriginDestination }
         * 
         * 
         */
        public List<OTAAirBaggageRS.BaggageList.OriginDestination> getOriginDestination() {
            if (originDestination == null) {
                originDestination = new ArrayList<OTAAirBaggageRS.BaggageList.OriginDestination>();
            }
            return this.originDestination;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="OriginLocation"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
         *                 &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="DestinationLocation"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
         *                 &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}ConnectionType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="MarketingAirline" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
         *                 &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="BaggageDetail" maxOccurs="unbounded" minOccurs="0" form="qualified"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="ServiceFamily" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="ProductGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="BaggageDetail"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
         *                                                         &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                                                         &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                                                         &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
         *                                                           &amp;lt;simpleType&amp;gt;
         *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                               &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
         *                                                               &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
         *                                                               &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
         *                                                               &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
         *                                                               &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
         *                                                             &amp;lt;/restriction&amp;gt;
         *                                                           &amp;lt;/simpleType&amp;gt;
         *                                                         &amp;lt;/attribute&amp;gt;
         *                                                         &amp;lt;attribute name="Method"&amp;gt;
         *                                                           &amp;lt;simpleType&amp;gt;
         *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                               &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
         *                                                               &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
         *                                                               &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
         *                                                               &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
         *                                                               &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
         *                                                               &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
         *                                                             &amp;lt;/restriction&amp;gt;
         *                                                           &amp;lt;/simpleType&amp;gt;
         *                                                         &amp;lt;/attribute&amp;gt;
         *                                                         &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *                                                         &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                                                         &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                                       &amp;lt;/extension&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
         *                                                             &amp;lt;complexType&amp;gt;
         *                                                               &amp;lt;complexContent&amp;gt;
         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                   &amp;lt;sequence&amp;gt;
         *                                                                     &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                                   &amp;lt;/sequence&amp;gt;
         *                                                                   &amp;lt;attribute name="UpgradeMethod"&amp;gt;
         *                                                                     &amp;lt;simpleType&amp;gt;
         *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                                         &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
         *                                                                         &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
         *                                                                       &amp;lt;/restriction&amp;gt;
         *                                                                     &amp;lt;/simpleType&amp;gt;
         *                                                                   &amp;lt;/attribute&amp;gt;
         *                                                                   &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *                                                                 &amp;lt;/restriction&amp;gt;
         *                                                               &amp;lt;/complexContent&amp;gt;
         *                                                             &amp;lt;/complexType&amp;gt;
         *                                                           &amp;lt;/element&amp;gt;
         *                                                           &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                         &amp;lt;attribute name="BookingMethod"&amp;gt;
         *                                                           &amp;lt;simpleType&amp;gt;
         *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                               &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
         *                                                               &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
         *                                                               &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
         *                                                             &amp;lt;/restriction&amp;gt;
         *                                                           &amp;lt;/simpleType&amp;gt;
         *                                                         &amp;lt;/attribute&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
         *                                                           &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
         *                                                         &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                                         &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                                         &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                                         &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *                                                         &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                                         &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                                         &amp;lt;attribute name="DisplayLabel"&amp;gt;
         *                                                           &amp;lt;simpleType&amp;gt;
         *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                             &amp;lt;/restriction&amp;gt;
         *                                                           &amp;lt;/simpleType&amp;gt;
         *                                                         &amp;lt;/attribute&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;simpleContent&amp;gt;
         *                                                       &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *                                                         &amp;lt;attribute name="Concept"&amp;gt;
         *                                                           &amp;lt;simpleType&amp;gt;
         *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                               &amp;lt;enumeration value="Piece"/&amp;gt;
         *                                                               &amp;lt;enumeration value="Weight"/&amp;gt;
         *                                                             &amp;lt;/restriction&amp;gt;
         *                                                           &amp;lt;/simpleType&amp;gt;
         *                                                         &amp;lt;/attribute&amp;gt;
         *                                                       &amp;lt;/extension&amp;gt;
         *                                                     &amp;lt;/simpleContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                                         &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                                         &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attribute name="CodeSource"&amp;gt;
         *                                                 &amp;lt;simpleType&amp;gt;
         *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                     &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
         *                                                     &amp;lt;enumeration value="ATPCO"/&amp;gt;
         *                                                   &amp;lt;/restriction&amp;gt;
         *                                                 &amp;lt;/simpleType&amp;gt;
         *                                               &amp;lt;/attribute&amp;gt;
         *                                               &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                               &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                               &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                               &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                       &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                               &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                               &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                     &amp;lt;attribute name="CodeSource"&amp;gt;
         *                                       &amp;lt;simpleType&amp;gt;
         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                           &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
         *                                           &amp;lt;enumeration value="ATPCO"/&amp;gt;
         *                                         &amp;lt;/restriction&amp;gt;
         *                                       &amp;lt;/simpleType&amp;gt;
         *                                     &amp;lt;/attribute&amp;gt;
         *                                     &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                     &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="CodeSource"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
         *                                 &amp;lt;enumeration value="ATPCO"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                           &amp;lt;attribute name="ServiceCode" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceFamilyEnum" /&amp;gt;
         *                           &amp;lt;attribute name="ExtServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
         *                       &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
         *                       &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
         *                       &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
         *                       &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="Method"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
         *                       &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
         *                       &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
         *                       &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
         *                       &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
         *                       &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="ArrivalDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="DepartureDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="OrigDestSequenceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "originLocation",
            "destinationLocation",
            "connectionLocations",
            "marketingAirline",
            "baggageDetail"
        })
        public static class OriginDestination {

            @XmlElement(name = "OriginLocation", required = true)
            protected OTAAirBaggageRS.BaggageList.OriginDestination.OriginLocation originLocation;
            @XmlElement(name = "DestinationLocation", required = true)
            protected OTAAirBaggageRS.BaggageList.OriginDestination.DestinationLocation destinationLocation;
            @XmlElement(name = "ConnectionLocations")
            protected ConnectionType connectionLocations;
            @XmlElement(name = "MarketingAirline")
            protected List<OTAAirBaggageRS.BaggageList.OriginDestination.MarketingAirline> marketingAirline;
            @XmlElement(name = "BaggageDetail")
            protected List<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail> baggageDetail;
            @XmlAttribute(name = "AllowanceRPH")
            protected String allowanceRPH;
            @XmlAttribute(name = "ArrivalDateTime")
            protected String arrivalDateTime;
            @XmlAttribute(name = "DepartureDateTime")
            protected String departureDateTime;
            @XmlAttribute(name = "OrigDestSequenceRPH")
            protected String origDestSequenceRPH;

            /**
             * Obtiene el valor de la propiedad originLocation.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.OriginLocation }
             *     
             */
            public OTAAirBaggageRS.BaggageList.OriginDestination.OriginLocation getOriginLocation() {
                return originLocation;
            }

            /**
             * Define el valor de la propiedad originLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.OriginLocation }
             *     
             */
            public void setOriginLocation(OTAAirBaggageRS.BaggageList.OriginDestination.OriginLocation value) {
                this.originLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad destinationLocation.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.DestinationLocation }
             *     
             */
            public OTAAirBaggageRS.BaggageList.OriginDestination.DestinationLocation getDestinationLocation() {
                return destinationLocation;
            }

            /**
             * Define el valor de la propiedad destinationLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.DestinationLocation }
             *     
             */
            public void setDestinationLocation(OTAAirBaggageRS.BaggageList.OriginDestination.DestinationLocation value) {
                this.destinationLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad connectionLocations.
             * 
             * @return
             *     possible object is
             *     {@link ConnectionType }
             *     
             */
            public ConnectionType getConnectionLocations() {
                return connectionLocations;
            }

            /**
             * Define el valor de la propiedad connectionLocations.
             * 
             * @param value
             *     allowed object is
             *     {@link ConnectionType }
             *     
             */
            public void setConnectionLocations(ConnectionType value) {
                this.connectionLocations = value;
            }

            /**
             * Gets the value of the marketingAirline property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingAirline property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMarketingAirline().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.BaggageList.OriginDestination.MarketingAirline }
             * 
             * 
             */
            public List<OTAAirBaggageRS.BaggageList.OriginDestination.MarketingAirline> getMarketingAirline() {
                if (marketingAirline == null) {
                    marketingAirline = new ArrayList<OTAAirBaggageRS.BaggageList.OriginDestination.MarketingAirline>();
                }
                return this.marketingAirline;
            }

            /**
             * Gets the value of the baggageDetail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baggageDetail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBaggageDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail }
             * 
             * 
             */
            public List<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail> getBaggageDetail() {
                if (baggageDetail == null) {
                    baggageDetail = new ArrayList<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail>();
                }
                return this.baggageDetail;
            }

            /**
             * Obtiene el valor de la propiedad allowanceRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAllowanceRPH() {
                return allowanceRPH;
            }

            /**
             * Define el valor de la propiedad allowanceRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAllowanceRPH(String value) {
                this.allowanceRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getArrivalDateTime() {
                return arrivalDateTime;
            }

            /**
             * Define el valor de la propiedad arrivalDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setArrivalDateTime(String value) {
                this.arrivalDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDepartureDateTime() {
                return departureDateTime;
            }

            /**
             * Define el valor de la propiedad departureDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDepartureDateTime(String value) {
                this.departureDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad origDestSequenceRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOrigDestSequenceRPH() {
                return origDestSequenceRPH;
            }

            /**
             * Define el valor de la propiedad origDestSequenceRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOrigDestSequenceRPH(String value) {
                this.origDestSequenceRPH = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="ServiceFamily" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="ProductGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="BaggageDetail"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
             *                                               &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                                               &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                                               &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
             *                                                 &amp;lt;simpleType&amp;gt;
             *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                     &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
             *                                                     &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
             *                                                     &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
             *                                                     &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
             *                                                     &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
             *                                                   &amp;lt;/restriction&amp;gt;
             *                                                 &amp;lt;/simpleType&amp;gt;
             *                                               &amp;lt;/attribute&amp;gt;
             *                                               &amp;lt;attribute name="Method"&amp;gt;
             *                                                 &amp;lt;simpleType&amp;gt;
             *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                     &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
             *                                                     &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
             *                                                     &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
             *                                                     &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
             *                                                     &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
             *                                                     &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
             *                                                   &amp;lt;/restriction&amp;gt;
             *                                                 &amp;lt;/simpleType&amp;gt;
             *                                               &amp;lt;/attribute&amp;gt;
             *                                               &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
             *                                               &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *                                               &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                                             &amp;lt;/extension&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
             *                                                   &amp;lt;complexType&amp;gt;
             *                                                     &amp;lt;complexContent&amp;gt;
             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                         &amp;lt;sequence&amp;gt;
             *                                                           &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                                         &amp;lt;/sequence&amp;gt;
             *                                                         &amp;lt;attribute name="UpgradeMethod"&amp;gt;
             *                                                           &amp;lt;simpleType&amp;gt;
             *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                               &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
             *                                                               &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
             *                                                             &amp;lt;/restriction&amp;gt;
             *                                                           &amp;lt;/simpleType&amp;gt;
             *                                                         &amp;lt;/attribute&amp;gt;
             *                                                         &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
             *                                                       &amp;lt;/restriction&amp;gt;
             *                                                     &amp;lt;/complexContent&amp;gt;
             *                                                   &amp;lt;/complexType&amp;gt;
             *                                                 &amp;lt;/element&amp;gt;
             *                                                 &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                               &amp;lt;attribute name="BookingMethod"&amp;gt;
             *                                                 &amp;lt;simpleType&amp;gt;
             *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                     &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
             *                                                     &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
             *                                                     &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
             *                                                   &amp;lt;/restriction&amp;gt;
             *                                                 &amp;lt;/simpleType&amp;gt;
             *                                               &amp;lt;/attribute&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
             *                                                 &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
             *                                               &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                               &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                               &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                               &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
             *                                               &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                               &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                               &amp;lt;attribute name="DisplayLabel"&amp;gt;
             *                                                 &amp;lt;simpleType&amp;gt;
             *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                   &amp;lt;/restriction&amp;gt;
             *                                                 &amp;lt;/simpleType&amp;gt;
             *                                               &amp;lt;/attribute&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;simpleContent&amp;gt;
             *                                             &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
             *                                               &amp;lt;attribute name="Concept"&amp;gt;
             *                                                 &amp;lt;simpleType&amp;gt;
             *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                                     &amp;lt;enumeration value="Piece"/&amp;gt;
             *                                                     &amp;lt;enumeration value="Weight"/&amp;gt;
             *                                                   &amp;lt;/restriction&amp;gt;
             *                                                 &amp;lt;/simpleType&amp;gt;
             *                                               &amp;lt;/attribute&amp;gt;
             *                                             &amp;lt;/extension&amp;gt;
             *                                           &amp;lt;/simpleContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                               &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                               &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attribute name="CodeSource"&amp;gt;
             *                                       &amp;lt;simpleType&amp;gt;
             *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                           &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
             *                                           &amp;lt;enumeration value="ATPCO"/&amp;gt;
             *                                         &amp;lt;/restriction&amp;gt;
             *                                       &amp;lt;/simpleType&amp;gt;
             *                                     &amp;lt;/attribute&amp;gt;
             *                                     &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                     &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                     &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                     &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                             &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                     &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                     &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                           &amp;lt;attribute name="CodeSource"&amp;gt;
             *                             &amp;lt;simpleType&amp;gt;
             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                 &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
             *                                 &amp;lt;enumeration value="ATPCO"/&amp;gt;
             *                               &amp;lt;/restriction&amp;gt;
             *                             &amp;lt;/simpleType&amp;gt;
             *                           &amp;lt;/attribute&amp;gt;
             *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                           &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="CodeSource"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
             *                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *                 &amp;lt;attribute name="ServiceCode" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceFamilyEnum" /&amp;gt;
             *                 &amp;lt;attribute name="ExtServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
             *             &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
             *             &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
             *             &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
             *             &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="Method"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
             *             &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
             *             &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
             *             &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
             *             &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
             *             &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
             *       &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "serviceFamily"
            })
            public static class BaggageDetail
                extends BaggageSpecificationType
            {

                @XmlElement(name = "ServiceFamily")
                protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily serviceFamily;
                @XmlAttribute(name = "MaximumPieces")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maximumPieces;
                @XmlAttribute(name = "Quantity")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger quantity;
                @XmlAttribute(name = "EMD_TypeValue")
                protected String emdTypeValue;
                @XmlAttribute(name = "Method")
                protected String method;
                @XmlAttribute(name = "ServiceLocation")
                protected String serviceLocation;
                @XmlAttribute(name = "ServiceDate")
                protected String serviceDate;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Obtiene el valor de la propiedad serviceFamily.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily }
                 *     
                 */
                public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily getServiceFamily() {
                    return serviceFamily;
                }

                /**
                 * Define el valor de la propiedad serviceFamily.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily }
                 *     
                 */
                public void setServiceFamily(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily value) {
                    this.serviceFamily = value;
                }

                /**
                 * Obtiene el valor de la propiedad maximumPieces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaximumPieces() {
                    return maximumPieces;
                }

                /**
                 * Define el valor de la propiedad maximumPieces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaximumPieces(BigInteger value) {
                    this.maximumPieces = value;
                }

                /**
                 * Obtiene el valor de la propiedad quantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getQuantity() {
                    return quantity;
                }

                /**
                 * Define el valor de la propiedad quantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setQuantity(BigInteger value) {
                    this.quantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad emdTypeValue.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEMDTypeValue() {
                    return emdTypeValue;
                }

                /**
                 * Define el valor de la propiedad emdTypeValue.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEMDTypeValue(String value) {
                    this.emdTypeValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad method.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMethod() {
                    return method;
                }

                /**
                 * Define el valor de la propiedad method.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMethod(String value) {
                    this.method = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceLocation.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceLocation() {
                    return serviceLocation;
                }

                /**
                 * Define el valor de la propiedad serviceLocation.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceLocation(String value) {
                    this.serviceLocation = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceDate() {
                    return serviceDate;
                }

                /**
                 * Define el valor de la propiedad serviceDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceDate(String value) {
                    this.serviceDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="ProductGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="BaggageDetail"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
                 *                                     &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *                                     &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *                                     &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
                 *                                       &amp;lt;simpleType&amp;gt;
                 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                           &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
                 *                                           &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
                 *                                           &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
                 *                                           &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
                 *                                           &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
                 *                                         &amp;lt;/restriction&amp;gt;
                 *                                       &amp;lt;/simpleType&amp;gt;
                 *                                     &amp;lt;/attribute&amp;gt;
                 *                                     &amp;lt;attribute name="Method"&amp;gt;
                 *                                       &amp;lt;simpleType&amp;gt;
                 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                           &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
                 *                                           &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
                 *                                           &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
                 *                                           &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
                 *                                           &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
                 *                                           &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
                 *                                         &amp;lt;/restriction&amp;gt;
                 *                                       &amp;lt;/simpleType&amp;gt;
                 *                                     &amp;lt;/attribute&amp;gt;
                 *                                     &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                 *                                     &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
                 *                                     &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *                                   &amp;lt;/extension&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
                 *                                         &amp;lt;complexType&amp;gt;
                 *                                           &amp;lt;complexContent&amp;gt;
                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                               &amp;lt;sequence&amp;gt;
                 *                                                 &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                               &amp;lt;/sequence&amp;gt;
                 *                                               &amp;lt;attribute name="UpgradeMethod"&amp;gt;
                 *                                                 &amp;lt;simpleType&amp;gt;
                 *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                                     &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
                 *                                                     &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
                 *                                                   &amp;lt;/restriction&amp;gt;
                 *                                                 &amp;lt;/simpleType&amp;gt;
                 *                                               &amp;lt;/attribute&amp;gt;
                 *                                               &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                 *                                             &amp;lt;/restriction&amp;gt;
                 *                                           &amp;lt;/complexContent&amp;gt;
                 *                                         &amp;lt;/complexType&amp;gt;
                 *                                       &amp;lt;/element&amp;gt;
                 *                                       &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                     &amp;lt;attribute name="BookingMethod"&amp;gt;
                 *                                       &amp;lt;simpleType&amp;gt;
                 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                           &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
                 *                                           &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
                 *                                           &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
                 *                                         &amp;lt;/restriction&amp;gt;
                 *                                       &amp;lt;/simpleType&amp;gt;
                 *                                     &amp;lt;/attribute&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                 *                                       &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                 *                                     &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                                     &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                                     &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                                     &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                 *                                     &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                                     &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                                     &amp;lt;attribute name="DisplayLabel"&amp;gt;
                 *                                       &amp;lt;simpleType&amp;gt;
                 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                         &amp;lt;/restriction&amp;gt;
                 *                                       &amp;lt;/simpleType&amp;gt;
                 *                                     &amp;lt;/attribute&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;simpleContent&amp;gt;
                 *                                   &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
                 *                                     &amp;lt;attribute name="Concept"&amp;gt;
                 *                                       &amp;lt;simpleType&amp;gt;
                 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                           &amp;lt;enumeration value="Piece"/&amp;gt;
                 *                                           &amp;lt;enumeration value="Weight"/&amp;gt;
                 *                                         &amp;lt;/restriction&amp;gt;
                 *                                       &amp;lt;/simpleType&amp;gt;
                 *                                     &amp;lt;/attribute&amp;gt;
                 *                                   &amp;lt;/extension&amp;gt;
                 *                                 &amp;lt;/simpleContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                                     &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                                     &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attribute name="CodeSource"&amp;gt;
                 *                             &amp;lt;simpleType&amp;gt;
                 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                 &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
                 *                                 &amp;lt;enumeration value="ATPCO"/&amp;gt;
                 *                               &amp;lt;/restriction&amp;gt;
                 *                             &amp;lt;/simpleType&amp;gt;
                 *                           &amp;lt;/attribute&amp;gt;
                 *                           &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                           &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                           &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                           &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                   &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                           &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                           &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *                 &amp;lt;attribute name="CodeSource"&amp;gt;
                 *                   &amp;lt;simpleType&amp;gt;
                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
                 *                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
                 *                     &amp;lt;/restriction&amp;gt;
                 *                   &amp;lt;/simpleType&amp;gt;
                 *                 &amp;lt;/attribute&amp;gt;
                 *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                 &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="CodeSource"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
                 *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *       &amp;lt;attribute name="ServiceCode" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceFamilyEnum" /&amp;gt;
                 *       &amp;lt;attribute name="ExtServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "productGroup"
                })
                public static class ServiceFamily {

                    @XmlElement(name = "ProductGroup")
                    protected List<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup> productGroup;
                    @XmlAttribute(name = "CodeSource")
                    protected String codeSource;
                    @XmlAttribute(name = "ServiceCode")
                    protected AncillaryServiceFamilyEnum serviceCode;
                    @XmlAttribute(name = "ExtServiceCode")
                    protected String extServiceCode;
                    @XmlAttribute(name = "Description")
                    protected String description;

                    /**
                     * Gets the value of the productGroup property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the productGroup property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getProductGroup().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup }
                     * 
                     * 
                     */
                    public List<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup> getProductGroup() {
                        if (productGroup == null) {
                            productGroup = new ArrayList<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup>();
                        }
                        return this.productGroup;
                    }

                    /**
                     * Obtiene el valor de la propiedad codeSource.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCodeSource() {
                        return codeSource;
                    }

                    /**
                     * Define el valor de la propiedad codeSource.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCodeSource(String value) {
                        this.codeSource = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad serviceCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link AncillaryServiceFamilyEnum }
                     *     
                     */
                    public AncillaryServiceFamilyEnum getServiceCode() {
                        return serviceCode;
                    }

                    /**
                     * Define el valor de la propiedad serviceCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link AncillaryServiceFamilyEnum }
                     *     
                     */
                    public void setServiceCode(AncillaryServiceFamilyEnum value) {
                        this.serviceCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad extServiceCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExtServiceCode() {
                        return extServiceCode;
                    }

                    /**
                     * Define el valor de la propiedad extServiceCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExtServiceCode(String value) {
                        this.extServiceCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad description.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDescription() {
                        return description;
                    }

                    /**
                     * Define el valor de la propiedad description.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDescription(String value) {
                        this.description = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="SubGroup" maxOccurs="unbounded" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="BaggageDetail"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
                     *                           &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                     *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                     *                           &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
                     *                             &amp;lt;simpleType&amp;gt;
                     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                 &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
                     *                                 &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
                     *                                 &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
                     *                                 &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
                     *                                 &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
                     *                               &amp;lt;/restriction&amp;gt;
                     *                             &amp;lt;/simpleType&amp;gt;
                     *                           &amp;lt;/attribute&amp;gt;
                     *                           &amp;lt;attribute name="Method"&amp;gt;
                     *                             &amp;lt;simpleType&amp;gt;
                     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                 &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
                     *                                 &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
                     *                                 &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
                     *                                 &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
                     *                                 &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
                     *                                 &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
                     *                               &amp;lt;/restriction&amp;gt;
                     *                             &amp;lt;/simpleType&amp;gt;
                     *                           &amp;lt;/attribute&amp;gt;
                     *                           &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                     *                           &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
                     *                           &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *                         &amp;lt;/extension&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
                     *                               &amp;lt;complexType&amp;gt;
                     *                                 &amp;lt;complexContent&amp;gt;
                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                     &amp;lt;sequence&amp;gt;
                     *                                       &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                                     &amp;lt;/sequence&amp;gt;
                     *                                     &amp;lt;attribute name="UpgradeMethod"&amp;gt;
                     *                                       &amp;lt;simpleType&amp;gt;
                     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                           &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
                     *                                           &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
                     *                                         &amp;lt;/restriction&amp;gt;
                     *                                       &amp;lt;/simpleType&amp;gt;
                     *                                     &amp;lt;/attribute&amp;gt;
                     *                                     &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                     *                                   &amp;lt;/restriction&amp;gt;
                     *                                 &amp;lt;/complexContent&amp;gt;
                     *                               &amp;lt;/complexType&amp;gt;
                     *                             &amp;lt;/element&amp;gt;
                     *                             &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                           &amp;lt;attribute name="BookingMethod"&amp;gt;
                     *                             &amp;lt;simpleType&amp;gt;
                     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                 &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
                     *                                 &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
                     *                                 &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
                     *                               &amp;lt;/restriction&amp;gt;
                     *                             &amp;lt;/simpleType&amp;gt;
                     *                           &amp;lt;/attribute&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                     *                             &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                     *                           &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                           &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                           &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                           &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                     *                           &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                           &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                           &amp;lt;attribute name="DisplayLabel"&amp;gt;
                     *                             &amp;lt;simpleType&amp;gt;
                     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                               &amp;lt;/restriction&amp;gt;
                     *                             &amp;lt;/simpleType&amp;gt;
                     *                           &amp;lt;/attribute&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;simpleContent&amp;gt;
                     *                         &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
                     *                           &amp;lt;attribute name="Concept"&amp;gt;
                     *                             &amp;lt;simpleType&amp;gt;
                     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                                 &amp;lt;enumeration value="Piece"/&amp;gt;
                     *                                 &amp;lt;enumeration value="Weight"/&amp;gt;
                     *                               &amp;lt;/restriction&amp;gt;
                     *                             &amp;lt;/simpleType&amp;gt;
                     *                           &amp;lt;/attribute&amp;gt;
                     *                         &amp;lt;/extension&amp;gt;
                     *                       &amp;lt;/simpleContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *                           &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *                           &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attribute name="CodeSource"&amp;gt;
                     *                   &amp;lt;simpleType&amp;gt;
                     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                       &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
                     *                       &amp;lt;enumeration value="ATPCO"/&amp;gt;
                     *                     &amp;lt;/restriction&amp;gt;
                     *                   &amp;lt;/simpleType&amp;gt;
                     *                 &amp;lt;/attribute&amp;gt;
                     *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="TotalPrice" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *         &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *                 &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *                 &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *       &amp;lt;attribute name="CodeSource"&amp;gt;
                     *         &amp;lt;simpleType&amp;gt;
                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
                     *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
                     *           &amp;lt;/restriction&amp;gt;
                     *         &amp;lt;/simpleType&amp;gt;
                     *       &amp;lt;/attribute&amp;gt;
                     *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *       &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "subGroup",
                        "totalPrice",
                        "atpcoDiagnostic"
                    })
                    public static class ProductGroup {

                        @XmlElement(name = "SubGroup")
                        protected List<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup> subGroup;
                        @XmlElement(name = "TotalPrice")
                        protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.TotalPrice totalPrice;
                        @XmlElement(name = "ATPCO_Diagnostic")
                        protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.ATPCODiagnostic atpcoDiagnostic;
                        @XmlAttribute(name = "CodeSource")
                        protected String codeSource;
                        @XmlAttribute(name = "Code")
                        protected String code;
                        @XmlAttribute(name = "Description")
                        protected String description;
                        @XmlAttribute(name = "BrandedFareName")
                        protected String brandedFareName;
                        @XmlAttribute(name = "Name")
                        protected String name;
                        @XmlAttribute(name = "ShortDescription")
                        protected String shortDescription;
                        @XmlAttribute(name = "LongDescription")
                        protected String longDescription;

                        /**
                         * Gets the value of the subGroup property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the subGroup property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getSubGroup().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup }
                         * 
                         * 
                         */
                        public List<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup> getSubGroup() {
                            if (subGroup == null) {
                                subGroup = new ArrayList<OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup>();
                            }
                            return this.subGroup;
                        }

                        /**
                         * Obtiene el valor de la propiedad totalPrice.
                         * 
                         * @return
                         *     possible object is
                         *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.TotalPrice }
                         *     
                         */
                        public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.TotalPrice getTotalPrice() {
                            return totalPrice;
                        }

                        /**
                         * Define el valor de la propiedad totalPrice.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.TotalPrice }
                         *     
                         */
                        public void setTotalPrice(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.TotalPrice value) {
                            this.totalPrice = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad atpcoDiagnostic.
                         * 
                         * @return
                         *     possible object is
                         *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.ATPCODiagnostic }
                         *     
                         */
                        public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.ATPCODiagnostic getATPCODiagnostic() {
                            return atpcoDiagnostic;
                        }

                        /**
                         * Define el valor de la propiedad atpcoDiagnostic.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.ATPCODiagnostic }
                         *     
                         */
                        public void setATPCODiagnostic(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.ATPCODiagnostic value) {
                            this.atpcoDiagnostic = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad codeSource.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCodeSource() {
                            return codeSource;
                        }

                        /**
                         * Define el valor de la propiedad codeSource.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCodeSource(String value) {
                            this.codeSource = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad code.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCode() {
                            return code;
                        }

                        /**
                         * Define el valor de la propiedad code.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCode(String value) {
                            this.code = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad description.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getDescription() {
                            return description;
                        }

                        /**
                         * Define el valor de la propiedad description.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setDescription(String value) {
                            this.description = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad brandedFareName.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getBrandedFareName() {
                            return brandedFareName;
                        }

                        /**
                         * Define el valor de la propiedad brandedFareName.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setBrandedFareName(String value) {
                            this.brandedFareName = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad name.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Define el valor de la propiedad name.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad shortDescription.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getShortDescription() {
                            return shortDescription;
                        }

                        /**
                         * Define el valor de la propiedad shortDescription.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setShortDescription(String value) {
                            this.shortDescription = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad longDescription.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLongDescription() {
                            return longDescription;
                        }

                        /**
                         * Define el valor de la propiedad longDescription.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLongDescription(String value) {
                            this.longDescription = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *       &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *       &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class ATPCODiagnostic {

                            @XmlAttribute(name = "S5")
                            protected BigInteger s5;
                            @XmlAttribute(name = "S7")
                            protected BigInteger s7;
                            @XmlAttribute(name = "S7SequenceNo")
                            protected BigInteger s7SequenceNo;

                            /**
                             * Obtiene el valor de la propiedad s5.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getS5() {
                                return s5;
                            }

                            /**
                             * Define el valor de la propiedad s5.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setS5(BigInteger value) {
                                this.s5 = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad s7.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getS7() {
                                return s7;
                            }

                            /**
                             * Define el valor de la propiedad s7.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setS7(BigInteger value) {
                                this.s7 = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad s7SequenceNo.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getS7SequenceNo() {
                                return s7SequenceNo;
                            }

                            /**
                             * Define el valor de la propiedad s7SequenceNo.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setS7SequenceNo(BigInteger value) {
                                this.s7SequenceNo = value;
                            }

                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="BaggageDetail"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
                         *                 &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                         *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                         *                 &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
                         *                   &amp;lt;simpleType&amp;gt;
                         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                       &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
                         *                       &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
                         *                       &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
                         *                       &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
                         *                       &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
                         *                     &amp;lt;/restriction&amp;gt;
                         *                   &amp;lt;/simpleType&amp;gt;
                         *                 &amp;lt;/attribute&amp;gt;
                         *                 &amp;lt;attribute name="Method"&amp;gt;
                         *                   &amp;lt;simpleType&amp;gt;
                         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                       &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
                         *                       &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
                         *                       &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
                         *                       &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
                         *                       &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
                         *                       &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
                         *                     &amp;lt;/restriction&amp;gt;
                         *                   &amp;lt;/simpleType&amp;gt;
                         *                 &amp;lt;/attribute&amp;gt;
                         *                 &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                         *                 &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
                         *                 &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                         *               &amp;lt;/extension&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
                         *                     &amp;lt;complexType&amp;gt;
                         *                       &amp;lt;complexContent&amp;gt;
                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                           &amp;lt;sequence&amp;gt;
                         *                             &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                           &amp;lt;/sequence&amp;gt;
                         *                           &amp;lt;attribute name="UpgradeMethod"&amp;gt;
                         *                             &amp;lt;simpleType&amp;gt;
                         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                                 &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
                         *                                 &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
                         *                               &amp;lt;/restriction&amp;gt;
                         *                             &amp;lt;/simpleType&amp;gt;
                         *                           &amp;lt;/attribute&amp;gt;
                         *                           &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                         *                         &amp;lt;/restriction&amp;gt;
                         *                       &amp;lt;/complexContent&amp;gt;
                         *                     &amp;lt;/complexType&amp;gt;
                         *                   &amp;lt;/element&amp;gt;
                         *                   &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *                 &amp;lt;attribute name="BookingMethod"&amp;gt;
                         *                   &amp;lt;simpleType&amp;gt;
                         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                       &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
                         *                       &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
                         *                       &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
                         *                     &amp;lt;/restriction&amp;gt;
                         *                   &amp;lt;/simpleType&amp;gt;
                         *                 &amp;lt;/attribute&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="Pricing" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                         *                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                         *                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *                 &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                         *                 &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *                 &amp;lt;attribute name="DisplayLabel"&amp;gt;
                         *                   &amp;lt;simpleType&amp;gt;
                         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                     &amp;lt;/restriction&amp;gt;
                         *                   &amp;lt;/simpleType&amp;gt;
                         *                 &amp;lt;/attribute&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="TicketBox" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;simpleContent&amp;gt;
                         *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
                         *                 &amp;lt;attribute name="Concept"&amp;gt;
                         *                   &amp;lt;simpleType&amp;gt;
                         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *                       &amp;lt;enumeration value="Piece"/&amp;gt;
                         *                       &amp;lt;enumeration value="Weight"/&amp;gt;
                         *                     &amp;lt;/restriction&amp;gt;
                         *                   &amp;lt;/simpleType&amp;gt;
                         *                 &amp;lt;/attribute&amp;gt;
                         *               &amp;lt;/extension&amp;gt;
                         *             &amp;lt;/simpleContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="ATPCO_Diagnostic" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *                 &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *                 &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attribute name="CodeSource"&amp;gt;
                         *         &amp;lt;simpleType&amp;gt;
                         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *             &amp;lt;enumeration value="AirlineInternal"/&amp;gt;
                         *             &amp;lt;enumeration value="ATPCO"/&amp;gt;
                         *           &amp;lt;/restriction&amp;gt;
                         *         &amp;lt;/simpleType&amp;gt;
                         *       &amp;lt;/attribute&amp;gt;
                         *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="AdditionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="BrandedFareName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="LongDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "baggageDetail",
                            "bookingInstruction",
                            "description",
                            "offer",
                            "pricing",
                            "ticketBox",
                            "atpcoDiagnostic"
                        })
                        public static class SubGroup {

                            @XmlElement(name = "BaggageDetail", required = true)
                            protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BaggageDetail baggageDetail;
                            @XmlElement(name = "BookingInstruction")
                            protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction bookingInstruction;
                            @XmlElement(name = "Description")
                            protected FreeTextType description;
                            @XmlElement(name = "Offer")
                            protected List<AirOfferType> offer;
                            @XmlElement(name = "Pricing")
                            protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.Pricing pricing;
                            @XmlElement(name = "TicketBox")
                            protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.TicketBox ticketBox;
                            @XmlElement(name = "ATPCO_Diagnostic")
                            protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.ATPCODiagnostic atpcoDiagnostic;
                            @XmlAttribute(name = "CodeSource")
                            protected String codeSource;
                            @XmlAttribute(name = "Code")
                            protected String code;
                            @XmlAttribute(name = "AdditionalCode")
                            protected String additionalCode;
                            @XmlAttribute(name = "Description")
                            protected String descriptionGroupCode;
                            @XmlAttribute(name = "BrandedFareName")
                            protected String brandedFareName;
                            @XmlAttribute(name = "Name")
                            protected String name;
                            @XmlAttribute(name = "ShortDescription")
                            protected String shortDescription;
                            @XmlAttribute(name = "LongDescription")
                            protected String longDescription;

                            /**
                             * Obtiene el valor de la propiedad baggageDetail.
                             * 
                             * @return
                             *     possible object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BaggageDetail }
                             *     
                             */
                            public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BaggageDetail getBaggageDetail() {
                                return baggageDetail;
                            }

                            /**
                             * Define el valor de la propiedad baggageDetail.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BaggageDetail }
                             *     
                             */
                            public void setBaggageDetail(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BaggageDetail value) {
                                this.baggageDetail = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad bookingInstruction.
                             * 
                             * @return
                             *     possible object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction }
                             *     
                             */
                            public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction getBookingInstruction() {
                                return bookingInstruction;
                            }

                            /**
                             * Define el valor de la propiedad bookingInstruction.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction }
                             *     
                             */
                            public void setBookingInstruction(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction value) {
                                this.bookingInstruction = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad description.
                             * 
                             * @return
                             *     possible object is
                             *     {@link FreeTextType }
                             *     
                             */
                            public FreeTextType getDescription() {
                                return description;
                            }

                            /**
                             * Define el valor de la propiedad description.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link FreeTextType }
                             *     
                             */
                            public void setDescription(FreeTextType value) {
                                this.description = value;
                            }

                            /**
                             * Gets the value of the offer property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the offer property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getOffer().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link AirOfferType }
                             * 
                             * 
                             */
                            public List<AirOfferType> getOffer() {
                                if (offer == null) {
                                    offer = new ArrayList<AirOfferType>();
                                }
                                return this.offer;
                            }

                            /**
                             * Obtiene el valor de la propiedad pricing.
                             * 
                             * @return
                             *     possible object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.Pricing }
                             *     
                             */
                            public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.Pricing getPricing() {
                                return pricing;
                            }

                            /**
                             * Define el valor de la propiedad pricing.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.Pricing }
                             *     
                             */
                            public void setPricing(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.Pricing value) {
                                this.pricing = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad ticketBox.
                             * 
                             * @return
                             *     possible object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.TicketBox }
                             *     
                             */
                            public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.TicketBox getTicketBox() {
                                return ticketBox;
                            }

                            /**
                             * Define el valor de la propiedad ticketBox.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.TicketBox }
                             *     
                             */
                            public void setTicketBox(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.TicketBox value) {
                                this.ticketBox = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad atpcoDiagnostic.
                             * 
                             * @return
                             *     possible object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.ATPCODiagnostic }
                             *     
                             */
                            public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.ATPCODiagnostic getATPCODiagnostic() {
                                return atpcoDiagnostic;
                            }

                            /**
                             * Define el valor de la propiedad atpcoDiagnostic.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.ATPCODiagnostic }
                             *     
                             */
                            public void setATPCODiagnostic(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.ATPCODiagnostic value) {
                                this.atpcoDiagnostic = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad codeSource.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCodeSource() {
                                return codeSource;
                            }

                            /**
                             * Define el valor de la propiedad codeSource.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCodeSource(String value) {
                                this.codeSource = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad code.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode() {
                                return code;
                            }

                            /**
                             * Define el valor de la propiedad code.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode(String value) {
                                this.code = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad additionalCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAdditionalCode() {
                                return additionalCode;
                            }

                            /**
                             * Define el valor de la propiedad additionalCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAdditionalCode(String value) {
                                this.additionalCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad descriptionGroupCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDescriptionGroupCode() {
                                return descriptionGroupCode;
                            }

                            /**
                             * Define el valor de la propiedad descriptionGroupCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDescriptionGroupCode(String value) {
                                this.descriptionGroupCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad brandedFareName.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getBrandedFareName() {
                                return brandedFareName;
                            }

                            /**
                             * Define el valor de la propiedad brandedFareName.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setBrandedFareName(String value) {
                                this.brandedFareName = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad name.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getName() {
                                return name;
                            }

                            /**
                             * Define el valor de la propiedad name.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setName(String value) {
                                this.name = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad shortDescription.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getShortDescription() {
                                return shortDescription;
                            }

                            /**
                             * Define el valor de la propiedad shortDescription.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setShortDescription(String value) {
                                this.shortDescription = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad longDescription.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getLongDescription() {
                                return longDescription;
                            }

                            /**
                             * Define el valor de la propiedad longDescription.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setLongDescription(String value) {
                                this.longDescription = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;attribute name="S5" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                             *       &amp;lt;attribute name="S7" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                             *       &amp;lt;attribute name="S7SequenceNo" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class ATPCODiagnostic {

                                @XmlAttribute(name = "S5")
                                protected BigInteger s5;
                                @XmlAttribute(name = "S7")
                                protected BigInteger s7;
                                @XmlAttribute(name = "S7SequenceNo")
                                protected BigInteger s7SequenceNo;

                                /**
                                 * Obtiene el valor de la propiedad s5.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public BigInteger getS5() {
                                    return s5;
                                }

                                /**
                                 * Define el valor de la propiedad s5.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public void setS5(BigInteger value) {
                                    this.s5 = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad s7.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public BigInteger getS7() {
                                    return s7;
                                }

                                /**
                                 * Define el valor de la propiedad s7.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public void setS7(BigInteger value) {
                                    this.s7 = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad s7SequenceNo.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public BigInteger getS7SequenceNo() {
                                    return s7SequenceNo;
                                }

                                /**
                                 * Define el valor de la propiedad s7SequenceNo.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public void setS7SequenceNo(BigInteger value) {
                                    this.s7SequenceNo = value;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}BaggageSpecificationType"&amp;gt;
                             *       &amp;lt;attribute name="MaximumPieces" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                             *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                             *       &amp;lt;attribute name="EMD_TypeValue"&amp;gt;
                             *         &amp;lt;simpleType&amp;gt;
                             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *             &amp;lt;enumeration value="1 - Standalone"/&amp;gt;
                             *             &amp;lt;enumeration value="2 - Associated to a Flight"/&amp;gt;
                             *             &amp;lt;enumeration value="3 - Standalone No Flight Ticket Associated"/&amp;gt;
                             *             &amp;lt;enumeration value="4 - Carrier Directed No EMD"/&amp;gt;
                             *             &amp;lt;enumeration value="5 - Eticket"/&amp;gt;
                             *           &amp;lt;/restriction&amp;gt;
                             *         &amp;lt;/simpleType&amp;gt;
                             *       &amp;lt;/attribute&amp;gt;
                             *       &amp;lt;attribute name="Method"&amp;gt;
                             *         &amp;lt;simpleType&amp;gt;
                             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *             &amp;lt;enumeration value="D - Direct Settlement"/&amp;gt;
                             *             &amp;lt;enumeration value="EA - EMD Associated"/&amp;gt;
                             *             &amp;lt;enumeration value="ES - EMD Standalone"/&amp;gt;
                             *             &amp;lt;enumeration value="F - Fare Integrated"/&amp;gt;
                             *             &amp;lt;enumeration value="MA - Flight Associated Settled via MCO"/&amp;gt;
                             *             &amp;lt;enumeration value="MA - Standalone Settled via MCO"/&amp;gt;
                             *           &amp;lt;/restriction&amp;gt;
                             *         &amp;lt;/simpleType&amp;gt;
                             *       &amp;lt;/attribute&amp;gt;
                             *       &amp;lt;attribute name="ServiceLocation" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                             *       &amp;lt;attribute name="ServiceDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
                             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                             *     &amp;lt;/extension&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class BaggageDetail
                                extends BaggageSpecificationType
                            {

                                @XmlAttribute(name = "MaximumPieces")
                                @XmlSchemaType(name = "positiveInteger")
                                protected BigInteger maximumPieces;
                                @XmlAttribute(name = "Quantity")
                                @XmlSchemaType(name = "positiveInteger")
                                protected BigInteger quantity;
                                @XmlAttribute(name = "EMD_TypeValue")
                                protected String emdTypeValue;
                                @XmlAttribute(name = "Method")
                                protected String method;
                                @XmlAttribute(name = "ServiceLocation")
                                protected String serviceLocation;
                                @XmlAttribute(name = "ServiceDate")
                                protected String serviceDate;
                                @XmlAttribute(name = "ServiceRPH")
                                protected String serviceRPH;

                                /**
                                 * Obtiene el valor de la propiedad maximumPieces.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public BigInteger getMaximumPieces() {
                                    return maximumPieces;
                                }

                                /**
                                 * Define el valor de la propiedad maximumPieces.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public void setMaximumPieces(BigInteger value) {
                                    this.maximumPieces = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad quantity.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public BigInteger getQuantity() {
                                    return quantity;
                                }

                                /**
                                 * Define el valor de la propiedad quantity.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public void setQuantity(BigInteger value) {
                                    this.quantity = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad emdTypeValue.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getEMDTypeValue() {
                                    return emdTypeValue;
                                }

                                /**
                                 * Define el valor de la propiedad emdTypeValue.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setEMDTypeValue(String value) {
                                    this.emdTypeValue = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad method.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getMethod() {
                                    return method;
                                }

                                /**
                                 * Define el valor de la propiedad method.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setMethod(String value) {
                                    this.method = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad serviceLocation.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getServiceLocation() {
                                    return serviceLocation;
                                }

                                /**
                                 * Define el valor de la propiedad serviceLocation.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setServiceLocation(String value) {
                                    this.serviceLocation = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad serviceDate.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getServiceDate() {
                                    return serviceDate;
                                }

                                /**
                                 * Define el valor de la propiedad serviceDate.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setServiceDate(String value) {
                                    this.serviceDate = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad serviceRPH.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getServiceRPH() {
                                    return serviceRPH;
                                }

                                /**
                                 * Define el valor de la propiedad serviceRPH.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setServiceRPH(String value) {
                                    this.serviceRPH = value;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
                             *           &amp;lt;complexType&amp;gt;
                             *             &amp;lt;complexContent&amp;gt;
                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                 &amp;lt;sequence&amp;gt;
                             *                   &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *                 &amp;lt;/sequence&amp;gt;
                             *                 &amp;lt;attribute name="UpgradeMethod"&amp;gt;
                             *                   &amp;lt;simpleType&amp;gt;
                             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *                       &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
                             *                       &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
                             *                     &amp;lt;/restriction&amp;gt;
                             *                   &amp;lt;/simpleType&amp;gt;
                             *                 &amp;lt;/attribute&amp;gt;
                             *                 &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                             *               &amp;lt;/restriction&amp;gt;
                             *             &amp;lt;/complexContent&amp;gt;
                             *           &amp;lt;/complexType&amp;gt;
                             *         &amp;lt;/element&amp;gt;
                             *         &amp;lt;element name="OtherInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *       &amp;lt;attribute name="BookingMethod"&amp;gt;
                             *         &amp;lt;simpleType&amp;gt;
                             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *             &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
                             *             &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
                             *             &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
                             *           &amp;lt;/restriction&amp;gt;
                             *         &amp;lt;/simpleType&amp;gt;
                             *       &amp;lt;/attribute&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "ssrInfo",
                                "osiInfo",
                                "upgrade",
                                "otherInfo"
                            })
                            public static class BookingInstruction {

                                @XmlElement(name = "SSR_Info")
                                protected List<SpecialServiceRequestType> ssrInfo;
                                @XmlElement(name = "OSI_Info")
                                protected List<OtherServiceInfoType> osiInfo;
                                @XmlElement(name = "Upgrade")
                                protected OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction.Upgrade upgrade;
                                @XmlElement(name = "OtherInfo")
                                protected List<TextDescriptionType> otherInfo;
                                @XmlAttribute(name = "BookingMethod")
                                protected String bookingMethod;

                                /**
                                 * Gets the value of the ssrInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ssrInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getSSRInfo().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link SpecialServiceRequestType }
                                 * 
                                 * 
                                 */
                                public List<SpecialServiceRequestType> getSSRInfo() {
                                    if (ssrInfo == null) {
                                        ssrInfo = new ArrayList<SpecialServiceRequestType>();
                                    }
                                    return this.ssrInfo;
                                }

                                /**
                                 * Gets the value of the osiInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the osiInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getOSIInfo().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link OtherServiceInfoType }
                                 * 
                                 * 
                                 */
                                public List<OtherServiceInfoType> getOSIInfo() {
                                    if (osiInfo == null) {
                                        osiInfo = new ArrayList<OtherServiceInfoType>();
                                    }
                                    return this.osiInfo;
                                }

                                /**
                                 * Obtiene el valor de la propiedad upgrade.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction.Upgrade }
                                 *     
                                 */
                                public OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction.Upgrade getUpgrade() {
                                    return upgrade;
                                }

                                /**
                                 * Define el valor de la propiedad upgrade.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction.Upgrade }
                                 *     
                                 */
                                public void setUpgrade(OTAAirBaggageRS.BaggageList.OriginDestination.BaggageDetail.ServiceFamily.ProductGroup.SubGroup.BookingInstruction.Upgrade value) {
                                    this.upgrade = value;
                                }

                                /**
                                 * Gets the value of the otherInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the otherInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getOtherInfo().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link TextDescriptionType }
                                 * 
                                 * 
                                 */
                                public List<TextDescriptionType> getOtherInfo() {
                                    if (otherInfo == null) {
                                        otherInfo = new ArrayList<TextDescriptionType>();
                                    }
                                    return this.otherInfo;
                                }

                                /**
                                 * Obtiene el valor de la propiedad bookingMethod.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getBookingMethod() {
                                    return bookingMethod;
                                }

                                /**
                                 * Define el valor de la propiedad bookingMethod.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setBookingMethod(String value) {
                                    this.bookingMethod = value;
                                }


                                /**
                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                 * 
                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                 * 
                                 * &lt;pre&gt;
                                 * &amp;lt;complexType&amp;gt;
                                 *   &amp;lt;complexContent&amp;gt;
                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *       &amp;lt;sequence&amp;gt;
                                 *         &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                                 *       &amp;lt;/sequence&amp;gt;
                                 *       &amp;lt;attribute name="UpgradeMethod"&amp;gt;
                                 *         &amp;lt;simpleType&amp;gt;
                                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                                 *             &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
                                 *             &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
                                 *           &amp;lt;/restriction&amp;gt;
                                 *         &amp;lt;/simpleType&amp;gt;
                                 *       &amp;lt;/attribute&amp;gt;
                                 *       &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                                 *     &amp;lt;/restriction&amp;gt;
                                 *   &amp;lt;/complexContent&amp;gt;
                                 * &amp;lt;/complexType&amp;gt;
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "instruction"
                                })
                                public static class Upgrade {

                                    @XmlElement(name = "Instruction")
                                    protected List<String> instruction;
                                    @XmlAttribute(name = "UpgradeMethod")
                                    protected String upgradeMethod;
                                    @XmlAttribute(name = "UpgradeDesigCode")
                                    protected String upgradeDesigCode;

                                    /**
                                     * Gets the value of the instruction property.
                                     * 
                                     * &lt;p&gt;
                                     * This accessor method returns a reference to the live list,
                                     * not a snapshot. Therefore any modification you make to the
                                     * returned list will be present inside the JAXB object.
                                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the instruction property.
                                     * 
                                     * &lt;p&gt;
                                     * For example, to add a new item, do as follows:
                                     * &lt;pre&gt;
                                     *    getInstruction().add(newItem);
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     * &lt;p&gt;
                                     * Objects of the following type(s) are allowed in the list
                                     * {@link String }
                                     * 
                                     * 
                                     */
                                    public List<String> getInstruction() {
                                        if (instruction == null) {
                                            instruction = new ArrayList<String>();
                                        }
                                        return this.instruction;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad upgradeMethod.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getUpgradeMethod() {
                                        return upgradeMethod;
                                    }

                                    /**
                                     * Define el valor de la propiedad upgradeMethod.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setUpgradeMethod(String value) {
                                        this.upgradeMethod = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad upgradeDesigCode.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getUpgradeDesigCode() {
                                        return upgradeDesigCode;
                                    }

                                    /**
                                     * Define el valor de la propiedad upgradeDesigCode.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setUpgradeDesigCode(String value) {
                                        this.upgradeDesigCode = value;
                                    }

                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="CalculationInfo" type="{http://www.opentravel.org/OTA/2003/05}TextDescriptionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                             *         &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                             *       &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                             *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                             *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                             *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
                             *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                             *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                             *       &amp;lt;attribute name="DisplayLabel"&amp;gt;
                             *         &amp;lt;simpleType&amp;gt;
                             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *           &amp;lt;/restriction&amp;gt;
                             *         &amp;lt;/simpleType&amp;gt;
                             *       &amp;lt;/attribute&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "appliedRule",
                                "calculationInfo",
                                "pricingQualifier",
                                "redemptionPoints",
                                "taxInfo"
                            })
                            public static class Pricing {

                                @XmlElement(name = "AppliedRule")
                                protected List<AppliedRuleType> appliedRule;
                                @XmlElement(name = "CalculationInfo")
                                protected List<TextDescriptionType> calculationInfo;
                                @XmlElement(name = "PricingQualifier")
                                protected List<AirPricingQualifierType> pricingQualifier;
                                @XmlElement(name = "RedemptionPoints")
                                protected AirRedemptionMilesType redemptionPoints;
                                @XmlElement(name = "TaxInfo")
                                protected List<TaxType> taxInfo;
                                @XmlAttribute(name = "PreTaxAmount")
                                protected BigDecimal preTaxAmount;
                                @XmlAttribute(name = "TaxAmount")
                                protected BigDecimal taxAmount;
                                @XmlAttribute(name = "Amount")
                                protected BigDecimal amount;
                                @XmlAttribute(name = "PricingCurrency")
                                protected String pricingCurrency;
                                @XmlAttribute(name = "DecimalPlaces")
                                @XmlSchemaType(name = "nonNegativeInteger")
                                protected BigInteger decimalPlaces;
                                @XmlAttribute(name = "BaseNUC_Amount")
                                protected BigDecimal baseNUCAmount;
                                @XmlAttribute(name = "DisplayLabel")
                                protected String displayLabel;
                                @XmlAttribute(name = "FromCurrency")
                                protected String fromCurrency;
                                @XmlAttribute(name = "ToCurrency")
                                protected String toCurrency;
                                @XmlAttribute(name = "Rate")
                                protected BigDecimal rate;
                                @XmlAttribute(name = "Date")
                                @XmlSchemaType(name = "date")
                                protected XMLGregorianCalendar date;

                                /**
                                 * Gets the value of the appliedRule property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the appliedRule property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getAppliedRule().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link AppliedRuleType }
                                 * 
                                 * 
                                 */
                                public List<AppliedRuleType> getAppliedRule() {
                                    if (appliedRule == null) {
                                        appliedRule = new ArrayList<AppliedRuleType>();
                                    }
                                    return this.appliedRule;
                                }

                                /**
                                 * Gets the value of the calculationInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the calculationInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getCalculationInfo().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link TextDescriptionType }
                                 * 
                                 * 
                                 */
                                public List<TextDescriptionType> getCalculationInfo() {
                                    if (calculationInfo == null) {
                                        calculationInfo = new ArrayList<TextDescriptionType>();
                                    }
                                    return this.calculationInfo;
                                }

                                /**
                                 * Gets the value of the pricingQualifier property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pricingQualifier property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getPricingQualifier().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link AirPricingQualifierType }
                                 * 
                                 * 
                                 */
                                public List<AirPricingQualifierType> getPricingQualifier() {
                                    if (pricingQualifier == null) {
                                        pricingQualifier = new ArrayList<AirPricingQualifierType>();
                                    }
                                    return this.pricingQualifier;
                                }

                                /**
                                 * Obtiene el valor de la propiedad redemptionPoints.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link AirRedemptionMilesType }
                                 *     
                                 */
                                public AirRedemptionMilesType getRedemptionPoints() {
                                    return redemptionPoints;
                                }

                                /**
                                 * Define el valor de la propiedad redemptionPoints.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link AirRedemptionMilesType }
                                 *     
                                 */
                                public void setRedemptionPoints(AirRedemptionMilesType value) {
                                    this.redemptionPoints = value;
                                }

                                /**
                                 * Gets the value of the taxInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxInfo property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getTaxInfo().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link TaxType }
                                 * 
                                 * 
                                 */
                                public List<TaxType> getTaxInfo() {
                                    if (taxInfo == null) {
                                        taxInfo = new ArrayList<TaxType>();
                                    }
                                    return this.taxInfo;
                                }

                                /**
                                 * Obtiene el valor de la propiedad preTaxAmount.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public BigDecimal getPreTaxAmount() {
                                    return preTaxAmount;
                                }

                                /**
                                 * Define el valor de la propiedad preTaxAmount.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public void setPreTaxAmount(BigDecimal value) {
                                    this.preTaxAmount = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad taxAmount.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public BigDecimal getTaxAmount() {
                                    return taxAmount;
                                }

                                /**
                                 * Define el valor de la propiedad taxAmount.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public void setTaxAmount(BigDecimal value) {
                                    this.taxAmount = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad amount.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public BigDecimal getAmount() {
                                    return amount;
                                }

                                /**
                                 * Define el valor de la propiedad amount.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public void setAmount(BigDecimal value) {
                                    this.amount = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad pricingCurrency.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getPricingCurrency() {
                                    return pricingCurrency;
                                }

                                /**
                                 * Define el valor de la propiedad pricingCurrency.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setPricingCurrency(String value) {
                                    this.pricingCurrency = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad decimalPlaces.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public BigInteger getDecimalPlaces() {
                                    return decimalPlaces;
                                }

                                /**
                                 * Define el valor de la propiedad decimalPlaces.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigInteger }
                                 *     
                                 */
                                public void setDecimalPlaces(BigInteger value) {
                                    this.decimalPlaces = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad baseNUCAmount.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public BigDecimal getBaseNUCAmount() {
                                    return baseNUCAmount;
                                }

                                /**
                                 * Define el valor de la propiedad baseNUCAmount.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public void setBaseNUCAmount(BigDecimal value) {
                                    this.baseNUCAmount = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad displayLabel.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getDisplayLabel() {
                                    return displayLabel;
                                }

                                /**
                                 * Define el valor de la propiedad displayLabel.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setDisplayLabel(String value) {
                                    this.displayLabel = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad fromCurrency.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getFromCurrency() {
                                    return fromCurrency;
                                }

                                /**
                                 * Define el valor de la propiedad fromCurrency.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setFromCurrency(String value) {
                                    this.fromCurrency = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad toCurrency.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getToCurrency() {
                                    return toCurrency;
                                }

                                /**
                                 * Define el valor de la propiedad toCurrency.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setToCurrency(String value) {
                                    this.toCurrency = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad rate.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public BigDecimal getRate() {
                                    return rate;
                                }

                                /**
                                 * Define el valor de la propiedad rate.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link BigDecimal }
                                 *     
                                 */
                                public void setRate(BigDecimal value) {
                                    this.rate = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad date.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link XMLGregorianCalendar }
                                 *     
                                 */
                                public XMLGregorianCalendar getDate() {
                                    return date;
                                }

                                /**
                                 * Define el valor de la propiedad date.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link XMLGregorianCalendar }
                                 *     
                                 */
                                public void setDate(XMLGregorianCalendar value) {
                                    this.date = value;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;simpleContent&amp;gt;
                             *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
                             *       &amp;lt;attribute name="Concept"&amp;gt;
                             *         &amp;lt;simpleType&amp;gt;
                             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                             *             &amp;lt;enumeration value="Piece"/&amp;gt;
                             *             &amp;lt;enumeration value="Weight"/&amp;gt;
                             *           &amp;lt;/restriction&amp;gt;
                             *         &amp;lt;/simpleType&amp;gt;
                             *       &amp;lt;/attribute&amp;gt;
                             *     &amp;lt;/extension&amp;gt;
                             *   &amp;lt;/simpleContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "value"
                            })
                            public static class TicketBox {

                                @XmlValue
                                protected String value;
                                @XmlAttribute(name = "Concept")
                                protected String concept;

                                /**
                                 * Obtiene el valor de la propiedad value.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getValue() {
                                    return value;
                                }

                                /**
                                 * Define el valor de la propiedad value.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setValue(String value) {
                                    this.value = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad concept.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getConcept() {
                                    return concept;
                                }

                                /**
                                 * Define el valor de la propiedad concept.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setConcept(String value) {
                                    this.concept = value;
                                }

                            }

                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
                         *       &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "redemptionPoints",
                            "taxInfo"
                        })
                        public static class TotalPrice {

                            @XmlElement(name = "RedemptionPoints")
                            protected AirRedemptionMilesType redemptionPoints;
                            @XmlElement(name = "TaxInfo")
                            protected List<TaxType> taxInfo;
                            @XmlAttribute(name = "PreTaxAmount")
                            protected BigDecimal preTaxAmount;
                            @XmlAttribute(name = "TaxAmount")
                            protected BigDecimal taxAmount;
                            @XmlAttribute(name = "Amount")
                            protected BigDecimal amount;
                            @XmlAttribute(name = "BaseNUC_Amount")
                            protected BigDecimal baseNUCAmount;
                            @XmlAttribute(name = "FromCurrency")
                            protected String fromCurrency;
                            @XmlAttribute(name = "ToCurrency")
                            protected String toCurrency;
                            @XmlAttribute(name = "Rate")
                            protected BigDecimal rate;
                            @XmlAttribute(name = "Date")
                            @XmlSchemaType(name = "date")
                            protected XMLGregorianCalendar date;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "DecimalPlaces")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger decimalPlaces;

                            /**
                             * Obtiene el valor de la propiedad redemptionPoints.
                             * 
                             * @return
                             *     possible object is
                             *     {@link AirRedemptionMilesType }
                             *     
                             */
                            public AirRedemptionMilesType getRedemptionPoints() {
                                return redemptionPoints;
                            }

                            /**
                             * Define el valor de la propiedad redemptionPoints.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link AirRedemptionMilesType }
                             *     
                             */
                            public void setRedemptionPoints(AirRedemptionMilesType value) {
                                this.redemptionPoints = value;
                            }

                            /**
                             * Gets the value of the taxInfo property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxInfo property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getTaxInfo().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link TaxType }
                             * 
                             * 
                             */
                            public List<TaxType> getTaxInfo() {
                                if (taxInfo == null) {
                                    taxInfo = new ArrayList<TaxType>();
                                }
                                return this.taxInfo;
                            }

                            /**
                             * Obtiene el valor de la propiedad preTaxAmount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getPreTaxAmount() {
                                return preTaxAmount;
                            }

                            /**
                             * Define el valor de la propiedad preTaxAmount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setPreTaxAmount(BigDecimal value) {
                                this.preTaxAmount = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad taxAmount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getTaxAmount() {
                                return taxAmount;
                            }

                            /**
                             * Define el valor de la propiedad taxAmount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setTaxAmount(BigDecimal value) {
                                this.taxAmount = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setAmount(BigDecimal value) {
                                this.amount = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad baseNUCAmount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getBaseNUCAmount() {
                                return baseNUCAmount;
                            }

                            /**
                             * Define el valor de la propiedad baseNUCAmount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setBaseNUCAmount(BigDecimal value) {
                                this.baseNUCAmount = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad fromCurrency.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getFromCurrency() {
                                return fromCurrency;
                            }

                            /**
                             * Define el valor de la propiedad fromCurrency.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setFromCurrency(String value) {
                                this.fromCurrency = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad toCurrency.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getToCurrency() {
                                return toCurrency;
                            }

                            /**
                             * Define el valor de la propiedad toCurrency.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setToCurrency(String value) {
                                this.toCurrency = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad rate.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getRate() {
                                return rate;
                            }

                            /**
                             * Define el valor de la propiedad rate.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setRate(BigDecimal value) {
                                this.rate = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad date.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getDate() {
                                return date;
                            }

                            /**
                             * Define el valor de la propiedad date.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setDate(XMLGregorianCalendar value) {
                                this.date = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad currencyCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Define el valor de la propiedad currencyCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad decimalPlaces.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDecimalPlaces() {
                                return decimalPlaces;
                            }

                            /**
                             * Define el valor de la propiedad decimalPlaces.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDecimalPlaces(BigInteger value) {
                                this.decimalPlaces = value;
                            }

                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
             *       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DestinationLocation
                extends LocationType
            {

                @XmlAttribute(name = "MultiAirportCityInd")
                protected Boolean multiAirportCityInd;
                @XmlAttribute(name = "AlternateLocationInd")
                protected Boolean alternateLocationInd;

                /**
                 * Obtiene el valor de la propiedad multiAirportCityInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMultiAirportCityInd() {
                    return multiAirportCityInd;
                }

                /**
                 * Define el valor de la propiedad multiAirportCityInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMultiAirportCityInd(Boolean value) {
                    this.multiAirportCityInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad alternateLocationInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAlternateLocationInd() {
                    return alternateLocationInd;
                }

                /**
                 * Define el valor de la propiedad alternateLocationInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAlternateLocationInd(Boolean value) {
                    this.alternateLocationInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
             *       &amp;lt;attribute name="MostSignificantInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AllowanceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *       &amp;lt;attribute name="ServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MarketingAirline
                extends CompanyNameType
            {

                @XmlAttribute(name = "MostSignificantInd")
                protected Boolean mostSignificantInd;
                @XmlAttribute(name = "AllowanceRPH")
                protected String allowanceRPH;
                @XmlAttribute(name = "ServiceRPH")
                protected String serviceRPH;

                /**
                 * Obtiene el valor de la propiedad mostSignificantInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMostSignificantInd() {
                    return mostSignificantInd;
                }

                /**
                 * Define el valor de la propiedad mostSignificantInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMostSignificantInd(Boolean value) {
                    this.mostSignificantInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad allowanceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllowanceRPH() {
                    return allowanceRPH;
                }

                /**
                 * Define el valor de la propiedad allowanceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllowanceRPH(String value) {
                    this.allowanceRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad serviceRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRPH() {
                    return serviceRPH;
                }

                /**
                 * Define el valor de la propiedad serviceRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRPH(String value) {
                    this.serviceRPH = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
             *       &amp;lt;attribute name="MultiAirportCityInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OriginLocation
                extends LocationType
            {

                @XmlAttribute(name = "MultiAirportCityInd")
                protected Boolean multiAirportCityInd;
                @XmlAttribute(name = "AlternateLocationInd")
                protected Boolean alternateLocationInd;

                /**
                 * Obtiene el valor de la propiedad multiAirportCityInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isMultiAirportCityInd() {
                    return multiAirportCityInd;
                }

                /**
                 * Define el valor de la propiedad multiAirportCityInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setMultiAirportCityInd(Boolean value) {
                    this.multiAirportCityInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad alternateLocationInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAlternateLocationInd() {
                    return alternateLocationInd;
                }

                /**
                 * Define el valor de la propiedad alternateLocationInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAlternateLocationInd(Boolean value) {
                    this.alternateLocationInd = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="OfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PayWithMilesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ResponseType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Baggage Charge"/&amp;gt;
     *             &amp;lt;enumeration value="Baggage Allowance"/&amp;gt;
     *             &amp;lt;enumeration value="Baggage Charge and Allowance"/&amp;gt;
     *             &amp;lt;enumeration value="Baggage List"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="USDOT_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInfo {

        @XmlAttribute(name = "OfferInd")
        protected Boolean offerInd;
        @XmlAttribute(name = "PayWithMilesInd")
        protected Boolean payWithMilesInd;
        @XmlAttribute(name = "ResponseType")
        protected String responseType;
        @XmlAttribute(name = "PricingCurrency")
        protected String pricingCurrency;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "USDOT_Ind")
        protected Boolean usdotInd;

        /**
         * Obtiene el valor de la propiedad offerInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOfferInd() {
            return offerInd;
        }

        /**
         * Define el valor de la propiedad offerInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOfferInd(Boolean value) {
            this.offerInd = value;
        }

        /**
         * Obtiene el valor de la propiedad payWithMilesInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPayWithMilesInd() {
            return payWithMilesInd;
        }

        /**
         * Define el valor de la propiedad payWithMilesInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPayWithMilesInd(Boolean value) {
            this.payWithMilesInd = value;
        }

        /**
         * Obtiene el valor de la propiedad responseType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseType() {
            return responseType;
        }

        /**
         * Define el valor de la propiedad responseType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseType(String value) {
            this.responseType = value;
        }

        /**
         * Obtiene el valor de la propiedad pricingCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricingCurrency() {
            return pricingCurrency;
        }

        /**
         * Define el valor de la propiedad pricingCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricingCurrency(String value) {
            this.pricingCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad usdotInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isUSDOTInd() {
            return usdotInd;
        }

        /**
         * Define el valor de la propiedad usdotInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setUSDOTInd(Boolean value) {
            this.usdotInd = value;
        }

    }

}
