
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Includes the statistic data reported and the codes for which it has been aggregated, if applicable. The applicable date range for the data is defined in its attributes.
 * 
 * &lt;p&gt;Clase Java para StatisticApplicationSetType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="StatisticApplicationSetType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="StatisticCodes" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="StatisticCode" maxOccurs="5"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}StatisticCodeGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RevenueCategorySummaries" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RevenueCategorySummary" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RevenueCategorySummaryGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CountCategorySummaries" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CountCategorySummary" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CountCategorySummaryGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ReportSummaries" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ReportSummary" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatisticApplicationSetType", propOrder = {
    "statisticCodes",
    "revenueCategorySummaries",
    "countCategorySummaries",
    "reportSummaries"
})
public class StatisticApplicationSetType {

    @XmlElement(name = "StatisticCodes")
    protected StatisticApplicationSetType.StatisticCodes statisticCodes;
    @XmlElement(name = "RevenueCategorySummaries")
    protected StatisticApplicationSetType.RevenueCategorySummaries revenueCategorySummaries;
    @XmlElement(name = "CountCategorySummaries")
    protected StatisticApplicationSetType.CountCategorySummaries countCategorySummaries;
    @XmlElement(name = "ReportSummaries")
    protected StatisticApplicationSetType.ReportSummaries reportSummaries;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;

    /**
     * Obtiene el valor de la propiedad statisticCodes.
     * 
     * @return
     *     possible object is
     *     {@link StatisticApplicationSetType.StatisticCodes }
     *     
     */
    public StatisticApplicationSetType.StatisticCodes getStatisticCodes() {
        return statisticCodes;
    }

    /**
     * Define el valor de la propiedad statisticCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link StatisticApplicationSetType.StatisticCodes }
     *     
     */
    public void setStatisticCodes(StatisticApplicationSetType.StatisticCodes value) {
        this.statisticCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad revenueCategorySummaries.
     * 
     * @return
     *     possible object is
     *     {@link StatisticApplicationSetType.RevenueCategorySummaries }
     *     
     */
    public StatisticApplicationSetType.RevenueCategorySummaries getRevenueCategorySummaries() {
        return revenueCategorySummaries;
    }

    /**
     * Define el valor de la propiedad revenueCategorySummaries.
     * 
     * @param value
     *     allowed object is
     *     {@link StatisticApplicationSetType.RevenueCategorySummaries }
     *     
     */
    public void setRevenueCategorySummaries(StatisticApplicationSetType.RevenueCategorySummaries value) {
        this.revenueCategorySummaries = value;
    }

    /**
     * Obtiene el valor de la propiedad countCategorySummaries.
     * 
     * @return
     *     possible object is
     *     {@link StatisticApplicationSetType.CountCategorySummaries }
     *     
     */
    public StatisticApplicationSetType.CountCategorySummaries getCountCategorySummaries() {
        return countCategorySummaries;
    }

    /**
     * Define el valor de la propiedad countCategorySummaries.
     * 
     * @param value
     *     allowed object is
     *     {@link StatisticApplicationSetType.CountCategorySummaries }
     *     
     */
    public void setCountCategorySummaries(StatisticApplicationSetType.CountCategorySummaries value) {
        this.countCategorySummaries = value;
    }

    /**
     * Obtiene el valor de la propiedad reportSummaries.
     * 
     * @return
     *     possible object is
     *     {@link StatisticApplicationSetType.ReportSummaries }
     *     
     */
    public StatisticApplicationSetType.ReportSummaries getReportSummaries() {
        return reportSummaries;
    }

    /**
     * Define el valor de la propiedad reportSummaries.
     * 
     * @param value
     *     allowed object is
     *     {@link StatisticApplicationSetType.ReportSummaries }
     *     
     */
    public void setReportSummaries(StatisticApplicationSetType.ReportSummaries value) {
        this.reportSummaries = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }


    /**
     * A collection of CountCategorySummaryType elements.
     * 
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CountCategorySummary" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CountCategorySummaryGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "countCategorySummary"
    })
    public static class CountCategorySummaries {

        @XmlElement(name = "CountCategorySummary", required = true)
        protected List<StatisticApplicationSetType.CountCategorySummaries.CountCategorySummary> countCategorySummary;

        /**
         * Gets the value of the countCategorySummary property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the countCategorySummary property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCountCategorySummary().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link StatisticApplicationSetType.CountCategorySummaries.CountCategorySummary }
         * 
         * 
         */
        public List<StatisticApplicationSetType.CountCategorySummaries.CountCategorySummary> getCountCategorySummary() {
            if (countCategorySummary == null) {
                countCategorySummary = new ArrayList<StatisticApplicationSetType.CountCategorySummaries.CountCategorySummary>();
            }
            return this.countCategorySummary;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CountCategorySummaryGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CountCategorySummary {

            @XmlAttribute(name = "SummaryCount")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger summaryCount;
            @XmlAttribute(name = "CountCategoryCode")
            protected String countCategoryCode;

            /**
             * Obtiene el valor de la propiedad summaryCount.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSummaryCount() {
                return summaryCount;
            }

            /**
             * Define el valor de la propiedad summaryCount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSummaryCount(BigInteger value) {
                this.summaryCount = value;
            }

            /**
             * Obtiene el valor de la propiedad countCategoryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountCategoryCode() {
                return countCategoryCode;
            }

            /**
             * Define el valor de la propiedad countCategoryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountCategoryCode(String value) {
                this.countCategoryCode = value;
            }

        }

    }


    /**
     * Container for ReportSummary elements of type ParagraphType.
     * 
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ReportSummary" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reportSummary"
    })
    public static class ReportSummaries {

        @XmlElement(name = "ReportSummary", required = true)
        protected List<ParagraphType> reportSummary;

        /**
         * Gets the value of the reportSummary property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reportSummary property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getReportSummary().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getReportSummary() {
            if (reportSummary == null) {
                reportSummary = new ArrayList<ParagraphType>();
            }
            return this.reportSummary;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RevenueCategorySummary" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RevenueCategorySummaryGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "revenueCategorySummary"
    })
    public static class RevenueCategorySummaries {

        @XmlElement(name = "RevenueCategorySummary", required = true)
        protected List<StatisticApplicationSetType.RevenueCategorySummaries.RevenueCategorySummary> revenueCategorySummary;

        /**
         * Gets the value of the revenueCategorySummary property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the revenueCategorySummary property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRevenueCategorySummary().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link StatisticApplicationSetType.RevenueCategorySummaries.RevenueCategorySummary }
         * 
         * 
         */
        public List<StatisticApplicationSetType.RevenueCategorySummaries.RevenueCategorySummary> getRevenueCategorySummary() {
            if (revenueCategorySummary == null) {
                revenueCategorySummary = new ArrayList<StatisticApplicationSetType.RevenueCategorySummaries.RevenueCategorySummary>();
            }
            return this.revenueCategorySummary;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RevenueCategorySummaryGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class RevenueCategorySummary {

            @XmlAttribute(name = "RevenueCategoryCode")
            protected String revenueCategoryCode;
            @XmlAttribute(name = "CurrencyCode")
            protected String currencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;
            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;

            /**
             * Obtiene el valor de la propiedad revenueCategoryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevenueCategoryCode() {
                return revenueCategoryCode;
            }

            /**
             * Define el valor de la propiedad revenueCategoryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevenueCategoryCode(String value) {
                this.revenueCategoryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad currencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Define el valor de la propiedad currencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="StatisticCode" maxOccurs="5"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}StatisticCodeGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "statisticCode"
    })
    public static class StatisticCodes {

        @XmlElement(name = "StatisticCode", required = true)
        protected List<StatisticApplicationSetType.StatisticCodes.StatisticCode> statisticCode;

        /**
         * Gets the value of the statisticCode property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the statisticCode property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getStatisticCode().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link StatisticApplicationSetType.StatisticCodes.StatisticCode }
         * 
         * 
         */
        public List<StatisticApplicationSetType.StatisticCodes.StatisticCode> getStatisticCode() {
            if (statisticCode == null) {
                statisticCode = new ArrayList<StatisticApplicationSetType.StatisticCodes.StatisticCode>();
            }
            return this.statisticCode;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}StatisticCodeGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class StatisticCode {

            @XmlAttribute(name = "StatCode")
            protected String statCode;
            @XmlAttribute(name = "StatCategoryCode")
            protected String statCategoryCode;

            /**
             * Obtiene el valor de la propiedad statCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatCode() {
                return statCode;
            }

            /**
             * Define el valor de la propiedad statCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatCode(String value) {
                this.statCode = value;
            }

            /**
             * Obtiene el valor de la propiedad statCategoryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatCategoryCode() {
                return statCategoryCode;
            }

            /**
             * Define el valor de la propiedad statCategoryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatCategoryCode(String value) {
                this.statCategoryCode = value;
            }

        }

    }

}
