
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Passenger preferences requested and/or required for a ground transportation service, including service type, disability vehicle and other vehicle preferences.
 * 
 * &lt;p&gt;Clase Java para GroundServiceDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundServiceDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundServiceType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ServiceLevel" type="{http://www.opentravel.org/OTA/2003/05}List_LevelOfService" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="VehicleType" type="{http://www.opentravel.org/OTA/2003/05}List_VehCategory" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="VehicleSize" type="{http://www.opentravel.org/OTA/2003/05}List_VehSize" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="VehicleMakeModel" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleMakeModelGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DisabilityVehicleInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundServiceDetailType", propOrder = {
    "location",
    "serviceLevel",
    "vehicleType",
    "vehicleSize",
    "vehicleMakeModel",
    "multimediaDescriptions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAGroundAvailRS.GroundServices.GroundService.Service.class
})
public class GroundServiceDetailType
    extends GroundServiceType
{

    @XmlElement(name = "Location")
    protected GroundLocationsType location;
    @XmlElement(name = "ServiceLevel")
    protected ListLevelOfService serviceLevel;
    @XmlElement(name = "VehicleType")
    protected ListVehCategory vehicleType;
    @XmlElement(name = "VehicleSize")
    protected ListVehSize vehicleSize;
    @XmlElement(name = "VehicleMakeModel")
    protected GroundServiceDetailType.VehicleMakeModel vehicleMakeModel;
    @XmlElement(name = "MultimediaDescriptions")
    protected MultimediaDescriptionsType multimediaDescriptions;
    @XmlAttribute(name = "DisabilityVehicleInd", required = true)
    protected boolean disabilityVehicleInd;

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationsType }
     *     
     */
    public GroundLocationsType getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationsType }
     *     
     */
    public void setLocation(GroundLocationsType value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceLevel.
     * 
     * @return
     *     possible object is
     *     {@link ListLevelOfService }
     *     
     */
    public ListLevelOfService getServiceLevel() {
        return serviceLevel;
    }

    /**
     * Define el valor de la propiedad serviceLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link ListLevelOfService }
     *     
     */
    public void setServiceLevel(ListLevelOfService value) {
        this.serviceLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleType.
     * 
     * @return
     *     possible object is
     *     {@link ListVehCategory }
     *     
     */
    public ListVehCategory getVehicleType() {
        return vehicleType;
    }

    /**
     * Define el valor de la propiedad vehicleType.
     * 
     * @param value
     *     allowed object is
     *     {@link ListVehCategory }
     *     
     */
    public void setVehicleType(ListVehCategory value) {
        this.vehicleType = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleSize.
     * 
     * @return
     *     possible object is
     *     {@link ListVehSize }
     *     
     */
    public ListVehSize getVehicleSize() {
        return vehicleSize;
    }

    /**
     * Define el valor de la propiedad vehicleSize.
     * 
     * @param value
     *     allowed object is
     *     {@link ListVehSize }
     *     
     */
    public void setVehicleSize(ListVehSize value) {
        this.vehicleSize = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleMakeModel.
     * 
     * @return
     *     possible object is
     *     {@link GroundServiceDetailType.VehicleMakeModel }
     *     
     */
    public GroundServiceDetailType.VehicleMakeModel getVehicleMakeModel() {
        return vehicleMakeModel;
    }

    /**
     * Define el valor de la propiedad vehicleMakeModel.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundServiceDetailType.VehicleMakeModel }
     *     
     */
    public void setVehicleMakeModel(GroundServiceDetailType.VehicleMakeModel value) {
        this.vehicleMakeModel = value;
    }

    /**
     * Obtiene el valor de la propiedad multimediaDescriptions.
     * 
     * @return
     *     possible object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public MultimediaDescriptionsType getMultimediaDescriptions() {
        return multimediaDescriptions;
    }

    /**
     * Define el valor de la propiedad multimediaDescriptions.
     * 
     * @param value
     *     allowed object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public void setMultimediaDescriptions(MultimediaDescriptionsType value) {
        this.multimediaDescriptions = value;
    }

    /**
     * Obtiene el valor de la propiedad disabilityVehicleInd.
     * 
     */
    public boolean isDisabilityVehicleInd() {
        return disabilityVehicleInd;
    }

    /**
     * Define el valor de la propiedad disabilityVehicleInd.
     * 
     */
    public void setDisabilityVehicleInd(boolean value) {
        this.disabilityVehicleInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}VehicleMakeModelGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class VehicleMakeModel {

        @XmlAttribute(name = "Name", required = true)
        protected String name;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "ModelYear")
        @XmlSchemaType(name = "gYear")
        protected XMLGregorianCalendar modelYear;

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad modelYear.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getModelYear() {
            return modelYear;
        }

        /**
         * Define el valor de la propiedad modelYear.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setModelYear(XMLGregorianCalendar value) {
            this.modelYear = value;
        }

    }

}
