
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Travel segment (air, vehicle, hotel, rail, cruise, tour, general and package), pricing and special request information.
 * 
 * &lt;p&gt;Clase Java para ReservationItemsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ReservationItemsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Item" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TravelSegmentType"&amp;gt;
 *                 &amp;lt;attribute name="ItinSeqNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="IsPassive" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="IssueBoardingPass" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="BoardingPassIssued" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ItemPricing" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ItinPricingType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AirFareInfo" type="{http://www.opentravel.org/OTA/2003/05}BookingPriceInfoType"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SpecialRequestDetails" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecialReqDetailsType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="ChronoOrdered" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReservationItemsType", propOrder = {
    "item",
    "itemPricing",
    "specialRequestDetails"
})
public class ReservationItemsType {

    @XmlElement(name = "Item", required = true)
    protected List<ReservationItemsType.Item> item;
    @XmlElement(name = "ItemPricing")
    protected List<ReservationItemsType.ItemPricing> itemPricing;
    @XmlElement(name = "SpecialRequestDetails")
    protected ReservationItemsType.SpecialRequestDetails specialRequestDetails;
    @XmlAttribute(name = "ChronoOrdered")
    protected Boolean chronoOrdered;

    /**
     * Gets the value of the item property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the item property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getItem().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ReservationItemsType.Item }
     * 
     * 
     */
    public List<ReservationItemsType.Item> getItem() {
        if (item == null) {
            item = new ArrayList<ReservationItemsType.Item>();
        }
        return this.item;
    }

    /**
     * Gets the value of the itemPricing property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the itemPricing property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getItemPricing().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ReservationItemsType.ItemPricing }
     * 
     * 
     */
    public List<ReservationItemsType.ItemPricing> getItemPricing() {
        if (itemPricing == null) {
            itemPricing = new ArrayList<ReservationItemsType.ItemPricing>();
        }
        return this.itemPricing;
    }

    /**
     * Obtiene el valor de la propiedad specialRequestDetails.
     * 
     * @return
     *     possible object is
     *     {@link ReservationItemsType.SpecialRequestDetails }
     *     
     */
    public ReservationItemsType.SpecialRequestDetails getSpecialRequestDetails() {
        return specialRequestDetails;
    }

    /**
     * Define el valor de la propiedad specialRequestDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link ReservationItemsType.SpecialRequestDetails }
     *     
     */
    public void setSpecialRequestDetails(ReservationItemsType.SpecialRequestDetails value) {
        this.specialRequestDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad chronoOrdered.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChronoOrdered() {
        return chronoOrdered;
    }

    /**
     * Define el valor de la propiedad chronoOrdered.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChronoOrdered(Boolean value) {
        this.chronoOrdered = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TravelSegmentType"&amp;gt;
     *       &amp;lt;attribute name="ItinSeqNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="IsPassive" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="IssueBoardingPass" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="BoardingPassIssued" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Item
        extends TravelSegmentType
    {

        @XmlAttribute(name = "ItinSeqNumber")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger itinSeqNumber;
        @XmlAttribute(name = "IsPassive")
        protected Boolean isPassive;
        @XmlAttribute(name = "IssueBoardingPass")
        protected Boolean issueBoardingPass;
        @XmlAttribute(name = "BoardingPassIssued")
        protected Boolean boardingPassIssued;

        /**
         * Obtiene el valor de la propiedad itinSeqNumber.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getItinSeqNumber() {
            return itinSeqNumber;
        }

        /**
         * Define el valor de la propiedad itinSeqNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setItinSeqNumber(BigInteger value) {
            this.itinSeqNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad isPassive.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsPassive() {
            return isPassive;
        }

        /**
         * Define el valor de la propiedad isPassive.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsPassive(Boolean value) {
            this.isPassive = value;
        }

        /**
         * Obtiene el valor de la propiedad issueBoardingPass.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIssueBoardingPass() {
            return issueBoardingPass;
        }

        /**
         * Define el valor de la propiedad issueBoardingPass.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIssueBoardingPass(Boolean value) {
            this.issueBoardingPass = value;
        }

        /**
         * Obtiene el valor de la propiedad boardingPassIssued.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isBoardingPassIssued() {
            return boardingPassIssued;
        }

        /**
         * Define el valor de la propiedad boardingPassIssued.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setBoardingPassIssued(Boolean value) {
            this.boardingPassIssued = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ItinPricingType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AirFareInfo" type="{http://www.opentravel.org/OTA/2003/05}BookingPriceInfoType"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airFareInfo"
    })
    public static class ItemPricing
        extends ItinPricingType
    {

        @XmlElement(name = "AirFareInfo", required = true)
        protected BookingPriceInfoType airFareInfo;

        /**
         * Obtiene el valor de la propiedad airFareInfo.
         * 
         * @return
         *     possible object is
         *     {@link BookingPriceInfoType }
         *     
         */
        public BookingPriceInfoType getAirFareInfo() {
            return airFareInfo;
        }

        /**
         * Define el valor de la propiedad airFareInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link BookingPriceInfoType }
         *     
         */
        public void setAirFareInfo(BookingPriceInfoType value) {
            this.airFareInfo = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecialReqDetailsType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tpaExtensions"
    })
    public static class SpecialRequestDetails
        extends SpecialReqDetailsType
    {

        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

    }

}
