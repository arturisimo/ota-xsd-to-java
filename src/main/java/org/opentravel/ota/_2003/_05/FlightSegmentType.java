
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * FlightSegmentType extends FlightSegmentBaseType to provide additional functionality.
 * 
 * &lt;p&gt;Clase Java para FlightSegmentType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FlightSegmentType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentBaseType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="MarketingAirline" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SingleVendorIndGroup"/&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *       &amp;lt;attribute name="TourOperatorFlightID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="GovernmentApprovalInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="GovernmentApprovalText" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="FlownMileageQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightSegmentType", propOrder = {
    "marketingAirline"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.class,
    org.opentravel.ota._2003._05.OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.class,
    org.opentravel.ota._2003._05.OTAAirSeatMapRQ.SeatMapRequests.SeatMapRequest.FlightSegmentInfo.class,
    org.opentravel.ota._2003._05.OTAAirSeatMapRS.SeatMapResponses.SeatMapResponse.FlightSegmentInfo.class,
    org.opentravel.ota._2003._05.OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.class,
    BookFlightSegmentType.class
})
public class FlightSegmentType
    extends FlightSegmentBaseType
{

    @XmlElement(name = "MarketingAirline")
    protected FlightSegmentType.MarketingAirline marketingAirline;
    @XmlAttribute(name = "FlightNumber")
    protected String flightNumber;
    @XmlAttribute(name = "TourOperatorFlightID")
    protected String tourOperatorFlightID;
    @XmlAttribute(name = "GovernmentApprovalInd")
    protected Boolean governmentApprovalInd;
    @XmlAttribute(name = "GovernmentApprovalText")
    protected String governmentApprovalText;
    @XmlAttribute(name = "FlownMileageQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger flownMileageQty;
    @XmlAttribute(name = "FareBasisCode")
    protected String fareBasisCode;
    @XmlAttribute(name = "ResBookDesigCode")
    protected String resBookDesigCode;

    /**
     * Obtiene el valor de la propiedad marketingAirline.
     * 
     * @return
     *     possible object is
     *     {@link FlightSegmentType.MarketingAirline }
     *     
     */
    public FlightSegmentType.MarketingAirline getMarketingAirline() {
        return marketingAirline;
    }

    /**
     * Define el valor de la propiedad marketingAirline.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightSegmentType.MarketingAirline }
     *     
     */
    public void setMarketingAirline(FlightSegmentType.MarketingAirline value) {
        this.marketingAirline = value;
    }

    /**
     * Obtiene el valor de la propiedad flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad tourOperatorFlightID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourOperatorFlightID() {
        return tourOperatorFlightID;
    }

    /**
     * Define el valor de la propiedad tourOperatorFlightID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourOperatorFlightID(String value) {
        this.tourOperatorFlightID = value;
    }

    /**
     * Obtiene el valor de la propiedad governmentApprovalInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGovernmentApprovalInd() {
        return governmentApprovalInd;
    }

    /**
     * Define el valor de la propiedad governmentApprovalInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGovernmentApprovalInd(Boolean value) {
        this.governmentApprovalInd = value;
    }

    /**
     * Obtiene el valor de la propiedad governmentApprovalText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovernmentApprovalText() {
        return governmentApprovalText;
    }

    /**
     * Define el valor de la propiedad governmentApprovalText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovernmentApprovalText(String value) {
        this.governmentApprovalText = value;
    }

    /**
     * Obtiene el valor de la propiedad flownMileageQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFlownMileageQty() {
        return flownMileageQty;
    }

    /**
     * Define el valor de la propiedad flownMileageQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFlownMileageQty(BigInteger value) {
        this.flownMileageQty = value;
    }

    /**
     * Obtiene el valor de la propiedad fareBasisCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasisCode() {
        return fareBasisCode;
    }

    /**
     * Define el valor de la propiedad fareBasisCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasisCode(String value) {
        this.fareBasisCode = value;
    }

    /**
     * Obtiene el valor de la propiedad resBookDesigCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResBookDesigCode() {
        return resBookDesigCode;
    }

    /**
     * Define el valor de la propiedad resBookDesigCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResBookDesigCode(String value) {
        this.resBookDesigCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CompanyNameType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SingleVendorIndGroup"/&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MarketingAirline
        extends CompanyNameType
    {

        @XmlAttribute(name = "SingleVendorInd")
        protected String singleVendorInd;

        /**
         * Obtiene el valor de la propiedad singleVendorInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSingleVendorInd() {
            return singleVendorInd;
        }

        /**
         * Define el valor de la propiedad singleVendorInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSingleVendorInd(String value) {
            this.singleVendorInd = value;
        }

    }

}
