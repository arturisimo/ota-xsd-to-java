
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para TourActivityTypeEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="TourActivityTypeEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Adventure"/&amp;gt;
 *     &amp;lt;enumeration value="Agriculture"/&amp;gt;
 *     &amp;lt;enumeration value="Airline_Airplane"/&amp;gt;
 *     &amp;lt;enumeration value="All_Terrain_Vehicle"/&amp;gt;
 *     &amp;lt;enumeration value="Amphibious"/&amp;gt;
 *     &amp;lt;enumeration value="Aquariums"/&amp;gt;
 *     &amp;lt;enumeration value="Archeology"/&amp;gt;
 *     &amp;lt;enumeration value="Architecture"/&amp;gt;
 *     &amp;lt;enumeration value="Art"/&amp;gt;
 *     &amp;lt;enumeration value="Astronomy"/&amp;gt;
 *     &amp;lt;enumeration value="Attractions"/&amp;gt;
 *     &amp;lt;enumeration value="Audio"/&amp;gt;
 *     &amp;lt;enumeration value="Ballooning"/&amp;gt;
 *     &amp;lt;enumeration value="Banana_Boat"/&amp;gt;
 *     &amp;lt;enumeration value="Bareboat"/&amp;gt;
 *     &amp;lt;enumeration value="Barges"/&amp;gt;
 *     &amp;lt;enumeration value="Bicycling"/&amp;gt;
 *     &amp;lt;enumeration value="Bird_watching"/&amp;gt;
 *     &amp;lt;enumeration value="Boating/Boat_Tour"/&amp;gt;
 *     &amp;lt;enumeration value="Boogie_Boarding"/&amp;gt;
 *     &amp;lt;enumeration value="Bumper_Tubing"/&amp;gt;
 *     &amp;lt;enumeration value="Camping"/&amp;gt;
 *     &amp;lt;enumeration value="Canoeing"/&amp;gt;
 *     &amp;lt;enumeration value="Canyoneering"/&amp;gt;
 *     &amp;lt;enumeration value="Cave_Tours"/&amp;gt;
 *     &amp;lt;enumeration value="Climbing"/&amp;gt;
 *     &amp;lt;enumeration value="Clothing_Optional"/&amp;gt;
 *     &amp;lt;enumeration value="Combination_Tours"/&amp;gt;
 *     &amp;lt;enumeration value="Comedy_Club"/&amp;gt;
 *     &amp;lt;enumeration value="Concert"/&amp;gt;
 *     &amp;lt;enumeration value="Cross_Country"/&amp;gt;
 *     &amp;lt;enumeration value="Cruise"/&amp;gt;
 *     &amp;lt;enumeration value="Culinary"/&amp;gt;
 *     &amp;lt;enumeration value="Cultural"/&amp;gt;
 *     &amp;lt;enumeration value="Disabled"/&amp;gt;
 *     &amp;lt;enumeration value="Diving"/&amp;gt;
 *     &amp;lt;enumeration value="Dog_Sledding"/&amp;gt;
 *     &amp;lt;enumeration value="Dolphins"/&amp;gt;
 *     &amp;lt;enumeration value="Dude_Ranch"/&amp;gt;
 *     &amp;lt;enumeration value="Eclipse"/&amp;gt;
 *     &amp;lt;enumeration value="Ecology"/&amp;gt;
 *     &amp;lt;enumeration value="Educational"/&amp;gt;
 *     &amp;lt;enumeration value="Equestrian"/&amp;gt;
 *     &amp;lt;enumeration value="Executive_Board"/&amp;gt;
 *     &amp;lt;enumeration value="Farmstays"/&amp;gt;
 *     &amp;lt;enumeration value="Ferries"/&amp;gt;
 *     &amp;lt;enumeration value="Fishing"/&amp;gt;
 *     &amp;lt;enumeration value="Food"/&amp;gt;
 *     &amp;lt;enumeration value="Flightseeing"/&amp;gt;
 *     &amp;lt;enumeration value="Free"/&amp;gt;
 *     &amp;lt;enumeration value="Freighters"/&amp;gt;
 *     &amp;lt;enumeration value="Garden"/&amp;gt;
 *     &amp;lt;enumeration value="Golf"/&amp;gt;
 *     &amp;lt;enumeration value="Ghost"/&amp;gt;
 *     &amp;lt;enumeration value="GlassBottom_Viewing"/&amp;gt;
 *     &amp;lt;enumeration value="Golf_Golf_Courses"/&amp;gt;
 *     &amp;lt;enumeration value="Gourmet"/&amp;gt;
 *     &amp;lt;enumeration value="Guided_Land"/&amp;gt;
 *     &amp;lt;enumeration value="Guided_Restaurant"/&amp;gt;
 *     &amp;lt;enumeration value="Hang_Gliding"/&amp;gt;
 *     &amp;lt;enumeration value="Helicopter"/&amp;gt;
 *     &amp;lt;enumeration value="High_Performance_Jet_Boat"/&amp;gt;
 *     &amp;lt;enumeration value="Hiking"/&amp;gt;
 *     &amp;lt;enumeration value="Historical"/&amp;gt;
 *     &amp;lt;enumeration value="Holistic"/&amp;gt;
 *     &amp;lt;enumeration value="Homestays"/&amp;gt;
 *     &amp;lt;enumeration value="Honeymoon"/&amp;gt;
 *     &amp;lt;enumeration value="Horseback"/&amp;gt;
 *     &amp;lt;enumeration value="Hula_Dancing"/&amp;gt;
 *     &amp;lt;enumeration value="Inter_Island_Transportation"/&amp;gt;
 *     &amp;lt;enumeration value="Island_Day_Trip"/&amp;gt;
 *     &amp;lt;enumeration value="Jazz"/&amp;gt;
 *     &amp;lt;enumeration value="Jet_Skiing"/&amp;gt;
 *     &amp;lt;enumeration value="Kayaking"/&amp;gt;
 *     &amp;lt;enumeration value="Lava_Tour"/&amp;gt;
 *     &amp;lt;enumeration value="Lei_Greeters"/&amp;gt;
 *     &amp;lt;enumeration value="Luau"/&amp;gt;
 *     &amp;lt;enumeration value="Mancation"/&amp;gt;
 *     &amp;lt;enumeration value="Manta_Ray_Snorkel"/&amp;gt;
 *     &amp;lt;enumeration value="Marathon"/&amp;gt;
 *     &amp;lt;enumeration value="Meetings"/&amp;gt;
 *     &amp;lt;enumeration value="Motorcoach"/&amp;gt;
 *     &amp;lt;enumeration value="Motorcycling"/&amp;gt;
 *     &amp;lt;enumeration value="Mountaineering"/&amp;gt;
 *     &amp;lt;enumeration value="Museums"/&amp;gt;
 *     &amp;lt;enumeration value="Music"/&amp;gt;
 *     &amp;lt;enumeration value="Nature"/&amp;gt;
 *     &amp;lt;enumeration value="Night_Life"/&amp;gt;
 *     &amp;lt;enumeration value="Paragliding"/&amp;gt;
 *     &amp;lt;enumeration value="Parasailing"/&amp;gt;
 *     &amp;lt;enumeration value="Parks"/&amp;gt;
 *     &amp;lt;enumeration value="Photography"/&amp;gt;
 *     &amp;lt;enumeration value="Pilgrimage"/&amp;gt;
 *     &amp;lt;enumeration value="Rafting"/&amp;gt;
 *     &amp;lt;enumeration value="Railroad_or_Trams"/&amp;gt;
 *     &amp;lt;enumeration value="Rainforest"/&amp;gt;
 *     &amp;lt;enumeration value="Religious"/&amp;gt;
 *     &amp;lt;enumeration value="Rental_Equipment"/&amp;gt;
 *     &amp;lt;enumeration value="Research"/&amp;gt;
 *     &amp;lt;enumeration value="Restaurant_Tours"/&amp;gt;
 *     &amp;lt;enumeration value="Restaurants"/&amp;gt;
 *     &amp;lt;enumeration value="Safari"/&amp;gt;
 *     &amp;lt;enumeration value="Sailing"/&amp;gt;
 *     &amp;lt;enumeration value="Scuba/Snorkeling"/&amp;gt;
 *     &amp;lt;enumeration value="Scuba_Lessons"/&amp;gt;
 *     &amp;lt;enumeration value="Segway_Tours"/&amp;gt;
 *     &amp;lt;enumeration value="Shopping"/&amp;gt;
 *     &amp;lt;enumeration value="Show"/&amp;gt;
 *     &amp;lt;enumeration value="Sightseeing"/&amp;gt;
 *     &amp;lt;enumeration value="Singles"/&amp;gt;
 *     &amp;lt;enumeration value="Skate_Park"/&amp;gt;
 *     &amp;lt;enumeration value="Skiing"/&amp;gt;
 *     &amp;lt;enumeration value="Snowboarding"/&amp;gt;
 *     &amp;lt;enumeration value="Snowmobile"/&amp;gt;
 *     &amp;lt;enumeration value="Snuba"/&amp;gt;
 *     &amp;lt;enumeration value="Spa_and_Gym"/&amp;gt;
 *     &amp;lt;enumeration value="Space"/&amp;gt;
 *     &amp;lt;enumeration value="Speed_Boating"/&amp;gt;
 *     &amp;lt;enumeration value="Sphere_Riding"/&amp;gt;
 *     &amp;lt;enumeration value="Spiritual"/&amp;gt;
 *     &amp;lt;enumeration value="Sport_Fishing"/&amp;gt;
 *     &amp;lt;enumeration value="Sporting_Clays"/&amp;gt;
 *     &amp;lt;enumeration value="StandUp_Paddle_Boarding"/&amp;gt;
 *     &amp;lt;enumeration value="Storm_Chasing"/&amp;gt;
 *     &amp;lt;enumeration value="Submarine"/&amp;gt;
 *     &amp;lt;enumeration value="Submersible_Scooter"/&amp;gt;
 *     &amp;lt;enumeration value="Sunrise_Tours"/&amp;gt;
 *     &amp;lt;enumeration value="Sunset_Dinner"/&amp;gt;
 *     &amp;lt;enumeration value="Surfing_Camps"/&amp;gt;
 *     &amp;lt;enumeration value="Theater"/&amp;gt;
 *     &amp;lt;enumeration value="Transportation"/&amp;gt;
 *     &amp;lt;enumeration value="Train"/&amp;gt;
 *     &amp;lt;enumeration value="Trekking"/&amp;gt;
 *     &amp;lt;enumeration value="Turtle_Watching"/&amp;gt;
 *     &amp;lt;enumeration value="Underwater_Reef_Tours"/&amp;gt;
 *     &amp;lt;enumeration value="Vacation_Portraits"/&amp;gt;
 *     &amp;lt;enumeration value="Volcano"/&amp;gt;
 *     &amp;lt;enumeration value="Volunteering"/&amp;gt;
 *     &amp;lt;enumeration value="Wakeboarding"/&amp;gt;
 *     &amp;lt;enumeration value="Walking"/&amp;gt;
 *     &amp;lt;enumeration value="Water_Park"/&amp;gt;
 *     &amp;lt;enumeration value="Water_Skiing"/&amp;gt;
 *     &amp;lt;enumeration value="War"/&amp;gt;
 *     &amp;lt;enumeration value="Weddings"/&amp;gt;
 *     &amp;lt;enumeration value="Whale_Watching"/&amp;gt;
 *     &amp;lt;enumeration value="Windsurfing"/&amp;gt;
 *     &amp;lt;enumeration value="Wine"/&amp;gt;
 *     &amp;lt;enumeration value="Yacht_Charters"/&amp;gt;
 *     &amp;lt;enumeration value="Youth_Activities"/&amp;gt;
 *     &amp;lt;enumeration value="Zip_Line"/&amp;gt;
 *     &amp;lt;enumeration value="Zoo"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "TourActivityTypeEnum")
@XmlEnum
public enum TourActivityTypeEnum {

    @XmlEnumValue("Adventure")
    ADVENTURE("Adventure"),
    @XmlEnumValue("Agriculture")
    AGRICULTURE("Agriculture"),
    @XmlEnumValue("Airline_Airplane")
    AIRLINE_AIRPLANE("Airline_Airplane"),
    @XmlEnumValue("All_Terrain_Vehicle")
    ALL_TERRAIN_VEHICLE("All_Terrain_Vehicle"),
    @XmlEnumValue("Amphibious")
    AMPHIBIOUS("Amphibious"),
    @XmlEnumValue("Aquariums")
    AQUARIUMS("Aquariums"),
    @XmlEnumValue("Archeology")
    ARCHEOLOGY("Archeology"),
    @XmlEnumValue("Architecture")
    ARCHITECTURE("Architecture"),
    @XmlEnumValue("Art")
    ART("Art"),
    @XmlEnumValue("Astronomy")
    ASTRONOMY("Astronomy"),
    @XmlEnumValue("Attractions")
    ATTRACTIONS("Attractions"),
    @XmlEnumValue("Audio")
    AUDIO("Audio"),
    @XmlEnumValue("Ballooning")
    BALLOONING("Ballooning"),
    @XmlEnumValue("Banana_Boat")
    BANANA_BOAT("Banana_Boat"),
    @XmlEnumValue("Bareboat")
    BAREBOAT("Bareboat"),
    @XmlEnumValue("Barges")
    BARGES("Barges"),
    @XmlEnumValue("Bicycling")
    BICYCLING("Bicycling"),
    @XmlEnumValue("Bird_watching")
    BIRD_WATCHING("Bird_watching"),
    @XmlEnumValue("Boating/Boat_Tour")
    BOATING_BOAT_TOUR("Boating/Boat_Tour"),
    @XmlEnumValue("Boogie_Boarding")
    BOOGIE_BOARDING("Boogie_Boarding"),
    @XmlEnumValue("Bumper_Tubing")
    BUMPER_TUBING("Bumper_Tubing"),
    @XmlEnumValue("Camping")
    CAMPING("Camping"),
    @XmlEnumValue("Canoeing")
    CANOEING("Canoeing"),
    @XmlEnumValue("Canyoneering")
    CANYONEERING("Canyoneering"),
    @XmlEnumValue("Cave_Tours")
    CAVE_TOURS("Cave_Tours"),
    @XmlEnumValue("Climbing")
    CLIMBING("Climbing"),
    @XmlEnumValue("Clothing_Optional")
    CLOTHING_OPTIONAL("Clothing_Optional"),
    @XmlEnumValue("Combination_Tours")
    COMBINATION_TOURS("Combination_Tours"),
    @XmlEnumValue("Comedy_Club")
    COMEDY_CLUB("Comedy_Club"),
    @XmlEnumValue("Concert")
    CONCERT("Concert"),
    @XmlEnumValue("Cross_Country")
    CROSS_COUNTRY("Cross_Country"),
    @XmlEnumValue("Cruise")
    CRUISE("Cruise"),
    @XmlEnumValue("Culinary")
    CULINARY("Culinary"),
    @XmlEnumValue("Cultural")
    CULTURAL("Cultural"),
    @XmlEnumValue("Disabled")
    DISABLED("Disabled"),
    @XmlEnumValue("Diving")
    DIVING("Diving"),
    @XmlEnumValue("Dog_Sledding")
    DOG_SLEDDING("Dog_Sledding"),
    @XmlEnumValue("Dolphins")
    DOLPHINS("Dolphins"),
    @XmlEnumValue("Dude_Ranch")
    DUDE_RANCH("Dude_Ranch"),
    @XmlEnumValue("Eclipse")
    ECLIPSE("Eclipse"),
    @XmlEnumValue("Ecology")
    ECOLOGY("Ecology"),
    @XmlEnumValue("Educational")
    EDUCATIONAL("Educational"),
    @XmlEnumValue("Equestrian")
    EQUESTRIAN("Equestrian"),
    @XmlEnumValue("Executive_Board")
    EXECUTIVE_BOARD("Executive_Board"),
    @XmlEnumValue("Farmstays")
    FARMSTAYS("Farmstays"),
    @XmlEnumValue("Ferries")
    FERRIES("Ferries"),
    @XmlEnumValue("Fishing")
    FISHING("Fishing"),
    @XmlEnumValue("Food")
    FOOD("Food"),
    @XmlEnumValue("Flightseeing")
    FLIGHTSEEING("Flightseeing"),
    @XmlEnumValue("Free")
    FREE("Free"),
    @XmlEnumValue("Freighters")
    FREIGHTERS("Freighters"),
    @XmlEnumValue("Garden")
    GARDEN("Garden"),
    @XmlEnumValue("Golf")
    GOLF("Golf"),
    @XmlEnumValue("Ghost")
    GHOST("Ghost"),
    @XmlEnumValue("GlassBottom_Viewing")
    GLASS_BOTTOM_VIEWING("GlassBottom_Viewing"),
    @XmlEnumValue("Golf_Golf_Courses")
    GOLF_GOLF_COURSES("Golf_Golf_Courses"),
    @XmlEnumValue("Gourmet")
    GOURMET("Gourmet"),
    @XmlEnumValue("Guided_Land")
    GUIDED_LAND("Guided_Land"),
    @XmlEnumValue("Guided_Restaurant")
    GUIDED_RESTAURANT("Guided_Restaurant"),
    @XmlEnumValue("Hang_Gliding")
    HANG_GLIDING("Hang_Gliding"),
    @XmlEnumValue("Helicopter")
    HELICOPTER("Helicopter"),
    @XmlEnumValue("High_Performance_Jet_Boat")
    HIGH_PERFORMANCE_JET_BOAT("High_Performance_Jet_Boat"),
    @XmlEnumValue("Hiking")
    HIKING("Hiking"),
    @XmlEnumValue("Historical")
    HISTORICAL("Historical"),
    @XmlEnumValue("Holistic")
    HOLISTIC("Holistic"),
    @XmlEnumValue("Homestays")
    HOMESTAYS("Homestays"),
    @XmlEnumValue("Honeymoon")
    HONEYMOON("Honeymoon"),
    @XmlEnumValue("Horseback")
    HORSEBACK("Horseback"),
    @XmlEnumValue("Hula_Dancing")
    HULA_DANCING("Hula_Dancing"),
    @XmlEnumValue("Inter_Island_Transportation")
    INTER_ISLAND_TRANSPORTATION("Inter_Island_Transportation"),
    @XmlEnumValue("Island_Day_Trip")
    ISLAND_DAY_TRIP("Island_Day_Trip"),
    @XmlEnumValue("Jazz")
    JAZZ("Jazz"),
    @XmlEnumValue("Jet_Skiing")
    JET_SKIING("Jet_Skiing"),
    @XmlEnumValue("Kayaking")
    KAYAKING("Kayaking"),
    @XmlEnumValue("Lava_Tour")
    LAVA_TOUR("Lava_Tour"),
    @XmlEnumValue("Lei_Greeters")
    LEI_GREETERS("Lei_Greeters"),
    @XmlEnumValue("Luau")
    LUAU("Luau"),
    @XmlEnumValue("Mancation")
    MANCATION("Mancation"),
    @XmlEnumValue("Manta_Ray_Snorkel")
    MANTA_RAY_SNORKEL("Manta_Ray_Snorkel"),
    @XmlEnumValue("Marathon")
    MARATHON("Marathon"),
    @XmlEnumValue("Meetings")
    MEETINGS("Meetings"),
    @XmlEnumValue("Motorcoach")
    MOTORCOACH("Motorcoach"),
    @XmlEnumValue("Motorcycling")
    MOTORCYCLING("Motorcycling"),
    @XmlEnumValue("Mountaineering")
    MOUNTAINEERING("Mountaineering"),
    @XmlEnumValue("Museums")
    MUSEUMS("Museums"),
    @XmlEnumValue("Music")
    MUSIC("Music"),
    @XmlEnumValue("Nature")
    NATURE("Nature"),
    @XmlEnumValue("Night_Life")
    NIGHT_LIFE("Night_Life"),
    @XmlEnumValue("Paragliding")
    PARAGLIDING("Paragliding"),
    @XmlEnumValue("Parasailing")
    PARASAILING("Parasailing"),
    @XmlEnumValue("Parks")
    PARKS("Parks"),
    @XmlEnumValue("Photography")
    PHOTOGRAPHY("Photography"),
    @XmlEnumValue("Pilgrimage")
    PILGRIMAGE("Pilgrimage"),
    @XmlEnumValue("Rafting")
    RAFTING("Rafting"),
    @XmlEnumValue("Railroad_or_Trams")
    RAILROAD_OR_TRAMS("Railroad_or_Trams"),
    @XmlEnumValue("Rainforest")
    RAINFOREST("Rainforest"),
    @XmlEnumValue("Religious")
    RELIGIOUS("Religious"),
    @XmlEnumValue("Rental_Equipment")
    RENTAL_EQUIPMENT("Rental_Equipment"),
    @XmlEnumValue("Research")
    RESEARCH("Research"),
    @XmlEnumValue("Restaurant_Tours")
    RESTAURANT_TOURS("Restaurant_Tours"),
    @XmlEnumValue("Restaurants")
    RESTAURANTS("Restaurants"),
    @XmlEnumValue("Safari")
    SAFARI("Safari"),
    @XmlEnumValue("Sailing")
    SAILING("Sailing"),
    @XmlEnumValue("Scuba/Snorkeling")
    SCUBA_SNORKELING("Scuba/Snorkeling"),
    @XmlEnumValue("Scuba_Lessons")
    SCUBA_LESSONS("Scuba_Lessons"),
    @XmlEnumValue("Segway_Tours")
    SEGWAY_TOURS("Segway_Tours"),
    @XmlEnumValue("Shopping")
    SHOPPING("Shopping"),
    @XmlEnumValue("Show")
    SHOW("Show"),
    @XmlEnumValue("Sightseeing")
    SIGHTSEEING("Sightseeing"),
    @XmlEnumValue("Singles")
    SINGLES("Singles"),
    @XmlEnumValue("Skate_Park")
    SKATE_PARK("Skate_Park"),
    @XmlEnumValue("Skiing")
    SKIING("Skiing"),
    @XmlEnumValue("Snowboarding")
    SNOWBOARDING("Snowboarding"),
    @XmlEnumValue("Snowmobile")
    SNOWMOBILE("Snowmobile"),
    @XmlEnumValue("Snuba")
    SNUBA("Snuba"),
    @XmlEnumValue("Spa_and_Gym")
    SPA_AND_GYM("Spa_and_Gym"),
    @XmlEnumValue("Space")
    SPACE("Space"),
    @XmlEnumValue("Speed_Boating")
    SPEED_BOATING("Speed_Boating"),
    @XmlEnumValue("Sphere_Riding")
    SPHERE_RIDING("Sphere_Riding"),
    @XmlEnumValue("Spiritual")
    SPIRITUAL("Spiritual"),
    @XmlEnumValue("Sport_Fishing")
    SPORT_FISHING("Sport_Fishing"),
    @XmlEnumValue("Sporting_Clays")
    SPORTING_CLAYS("Sporting_Clays"),
    @XmlEnumValue("StandUp_Paddle_Boarding")
    STAND_UP_PADDLE_BOARDING("StandUp_Paddle_Boarding"),
    @XmlEnumValue("Storm_Chasing")
    STORM_CHASING("Storm_Chasing"),
    @XmlEnumValue("Submarine")
    SUBMARINE("Submarine"),
    @XmlEnumValue("Submersible_Scooter")
    SUBMERSIBLE_SCOOTER("Submersible_Scooter"),
    @XmlEnumValue("Sunrise_Tours")
    SUNRISE_TOURS("Sunrise_Tours"),
    @XmlEnumValue("Sunset_Dinner")
    SUNSET_DINNER("Sunset_Dinner"),
    @XmlEnumValue("Surfing_Camps")
    SURFING_CAMPS("Surfing_Camps"),
    @XmlEnumValue("Theater")
    THEATER("Theater"),
    @XmlEnumValue("Transportation")
    TRANSPORTATION("Transportation"),
    @XmlEnumValue("Train")
    TRAIN("Train"),
    @XmlEnumValue("Trekking")
    TREKKING("Trekking"),
    @XmlEnumValue("Turtle_Watching")
    TURTLE_WATCHING("Turtle_Watching"),
    @XmlEnumValue("Underwater_Reef_Tours")
    UNDERWATER_REEF_TOURS("Underwater_Reef_Tours"),
    @XmlEnumValue("Vacation_Portraits")
    VACATION_PORTRAITS("Vacation_Portraits"),
    @XmlEnumValue("Volcano")
    VOLCANO("Volcano"),
    @XmlEnumValue("Volunteering")
    VOLUNTEERING("Volunteering"),
    @XmlEnumValue("Wakeboarding")
    WAKEBOARDING("Wakeboarding"),
    @XmlEnumValue("Walking")
    WALKING("Walking"),
    @XmlEnumValue("Water_Park")
    WATER_PARK("Water_Park"),
    @XmlEnumValue("Water_Skiing")
    WATER_SKIING("Water_Skiing"),
    @XmlEnumValue("War")
    WAR("War"),
    @XmlEnumValue("Weddings")
    WEDDINGS("Weddings"),
    @XmlEnumValue("Whale_Watching")
    WHALE_WATCHING("Whale_Watching"),
    @XmlEnumValue("Windsurfing")
    WINDSURFING("Windsurfing"),
    @XmlEnumValue("Wine")
    WINE("Wine"),
    @XmlEnumValue("Yacht_Charters")
    YACHT_CHARTERS("Yacht_Charters"),
    @XmlEnumValue("Youth_Activities")
    YOUTH_ACTIVITIES("Youth_Activities"),
    @XmlEnumValue("Zip_Line")
    ZIP_LINE("Zip_Line"),
    @XmlEnumValue("Zoo")
    ZOO("Zoo"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support to support additional compartment type. The Value corresponding to "Other_" will be specified in the  "Value" attribute. See CompartmentType.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    TourActivityTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TourActivityTypeEnum fromValue(String v) {
        for (TourActivityTypeEnum c: TourActivityTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
