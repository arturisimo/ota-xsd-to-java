
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 *  This is used by OTA_HotelRFP_TransientNotifRQ schema. This accommodates detailed transient RFP information.
 * 
 * &lt;p&gt;Clase Java para RFP_TransientDetailsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RFP_TransientDetailsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RFP_TransientResponses" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RFP_TransientResponse" type="{http://www.opentravel.org/OTA/2003/05}RFP_TransientResponseType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MessageID" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RFP_TransientDetailsType", propOrder = {
    "rfpTransientResponses",
    "messageID"
})
@XmlSeeAlso({
    OTAHotelRFPTransientNotifRQ.class
})
public class RFPTransientDetailsType {

    @XmlElement(name = "RFP_TransientResponses")
    protected RFPTransientDetailsType.RFPTransientResponses rfpTransientResponses;
    @XmlElement(name = "MessageID")
    protected RFPTransientDetailsType.MessageID messageID;

    /**
     * Obtiene el valor de la propiedad rfpTransientResponses.
     * 
     * @return
     *     possible object is
     *     {@link RFPTransientDetailsType.RFPTransientResponses }
     *     
     */
    public RFPTransientDetailsType.RFPTransientResponses getRFPTransientResponses() {
        return rfpTransientResponses;
    }

    /**
     * Define el valor de la propiedad rfpTransientResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link RFPTransientDetailsType.RFPTransientResponses }
     *     
     */
    public void setRFPTransientResponses(RFPTransientDetailsType.RFPTransientResponses value) {
        this.rfpTransientResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad messageID.
     * 
     * @return
     *     possible object is
     *     {@link RFPTransientDetailsType.MessageID }
     *     
     */
    public RFPTransientDetailsType.MessageID getMessageID() {
        return messageID;
    }

    /**
     * Define el valor de la propiedad messageID.
     * 
     * @param value
     *     allowed object is
     *     {@link RFPTransientDetailsType.MessageID }
     *     
     */
    public void setMessageID(RFPTransientDetailsType.MessageID value) {
        this.messageID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MessageID
        extends UniqueIDType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RFP_TransientResponse" type="{http://www.opentravel.org/OTA/2003/05}RFP_TransientResponseType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rfpTransientResponse"
    })
    public static class RFPTransientResponses {

        @XmlElement(name = "RFP_TransientResponse", required = true)
        protected List<RFPTransientResponseType> rfpTransientResponse;

        /**
         * Gets the value of the rfpTransientResponse property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rfpTransientResponse property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRFPTransientResponse().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RFPTransientResponseType }
         * 
         * 
         */
        public List<RFPTransientResponseType> getRFPTransientResponse() {
            if (rfpTransientResponse == null) {
                rfpTransientResponse = new ArrayList<RFPTransientResponseType>();
            }
            return this.rfpTransientResponse;
        }

    }

}
