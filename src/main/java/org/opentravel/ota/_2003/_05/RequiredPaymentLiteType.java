
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Summary version of the RequiredPaymentType, initially created for the Travel Itinerary Message set.
 * 
 * &lt;p&gt;Clase Java para RequiredPaymentLiteType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RequiredPaymentLiteType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AcceptedPayments" type="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AmountPercent" type="{http://www.opentravel.org/OTA/2003/05}AmountPercentType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="RetributionType"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
 *             &amp;lt;enumeration value="ResAutoCancelled"/&amp;gt;
 *             &amp;lt;enumeration value="ResNotGuaranteed"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequiredPaymentLiteType", propOrder = {
    "acceptedPayments",
    "amountPercent"
})
public class RequiredPaymentLiteType {

    @XmlElement(name = "AcceptedPayments")
    protected AcceptedPaymentsType acceptedPayments;
    @XmlElement(name = "AmountPercent")
    protected AmountPercentType amountPercent;
    @XmlAttribute(name = "RetributionType")
    protected String retributionType;

    /**
     * Obtiene el valor de la propiedad acceptedPayments.
     * 
     * @return
     *     possible object is
     *     {@link AcceptedPaymentsType }
     *     
     */
    public AcceptedPaymentsType getAcceptedPayments() {
        return acceptedPayments;
    }

    /**
     * Define el valor de la propiedad acceptedPayments.
     * 
     * @param value
     *     allowed object is
     *     {@link AcceptedPaymentsType }
     *     
     */
    public void setAcceptedPayments(AcceptedPaymentsType value) {
        this.acceptedPayments = value;
    }

    /**
     * Obtiene el valor de la propiedad amountPercent.
     * 
     * @return
     *     possible object is
     *     {@link AmountPercentType }
     *     
     */
    public AmountPercentType getAmountPercent() {
        return amountPercent;
    }

    /**
     * Define el valor de la propiedad amountPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountPercentType }
     *     
     */
    public void setAmountPercent(AmountPercentType value) {
        this.amountPercent = value;
    }

    /**
     * Obtiene el valor de la propiedad retributionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetributionType() {
        return retributionType;
    }

    /**
     * Define el valor de la propiedad retributionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetributionType(String value) {
        this.retributionType = value;
    }

}
