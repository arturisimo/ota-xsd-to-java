
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Information representing sold data supporting the fare construction applicable to each fare component in a transaction.
 * 
 * &lt;p&gt;Clase Java para FareComponentType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FareComponentType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="PriceableUnit" maxOccurs="99"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="FareComponentDetail" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CouponSequence" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="SequenceNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                                     &amp;lt;attribute name="StopoverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ConstructionPrinciple" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
 *                                     &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                     &amp;lt;attribute name="Percent" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="BaseAmount" maxOccurs="3"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="TicketDesignator" maxOccurs="9" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="TicketDesignatorCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="TicketDesignatorQualifier" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                           &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
 *                           &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
 *                           &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="WaiverCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to19" /&amp;gt;
 *                           &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                           &amp;lt;attribute name="RuleCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
 *                           &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="AgreementCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TotalConstructionAmount" maxOccurs="3"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Purpose" use="required" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
 *       &amp;lt;attribute name="PriceQuoteDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *       &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="PricingDesignator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareComponentType", propOrder = {
    "priceableUnit",
    "totalConstructionAmount"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.EMDType.ExchResidualFareComponent.class
})
public class FareComponentType {

    @XmlElement(name = "PriceableUnit", required = true)
    protected List<FareComponentType.PriceableUnit> priceableUnit;
    @XmlElement(name = "TotalConstructionAmount", required = true)
    protected List<FareComponentType.TotalConstructionAmount> totalConstructionAmount;
    @XmlAttribute(name = "PriceQuoteDate")
    protected String priceQuoteDate;
    @XmlAttribute(name = "AccountCode")
    protected String accountCode;
    @XmlAttribute(name = "PricingDesignator")
    protected String pricingDesignator;
    @XmlAttribute(name = "ExchangeRate")
    protected BigDecimal exchangeRate;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantity;

    /**
     * Gets the value of the priceableUnit property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the priceableUnit property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPriceableUnit().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareComponentType.PriceableUnit }
     * 
     * 
     */
    public List<FareComponentType.PriceableUnit> getPriceableUnit() {
        if (priceableUnit == null) {
            priceableUnit = new ArrayList<FareComponentType.PriceableUnit>();
        }
        return this.priceableUnit;
    }

    /**
     * Gets the value of the totalConstructionAmount property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the totalConstructionAmount property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTotalConstructionAmount().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareComponentType.TotalConstructionAmount }
     * 
     * 
     */
    public List<FareComponentType.TotalConstructionAmount> getTotalConstructionAmount() {
        if (totalConstructionAmount == null) {
            totalConstructionAmount = new ArrayList<FareComponentType.TotalConstructionAmount>();
        }
        return this.totalConstructionAmount;
    }

    /**
     * Obtiene el valor de la propiedad priceQuoteDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceQuoteDate() {
        return priceQuoteDate;
    }

    /**
     * Define el valor de la propiedad priceQuoteDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceQuoteDate(String value) {
        this.priceQuoteDate = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingDesignator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingDesignator() {
        return pricingDesignator;
    }

    /**
     * Define el valor de la propiedad pricingDesignator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingDesignator(String value) {
        this.pricingDesignator = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Define el valor de la propiedad exchangeRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FareComponentDetail" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CouponSequence" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
     *                           &amp;lt;attribute name="SequenceNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *                           &amp;lt;attribute name="StopoverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ConstructionPrinciple" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
     *                           &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="Percent" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="BaseAmount" maxOccurs="3"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="TicketDesignator" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="TicketDesignatorCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="TicketDesignatorQualifier" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
     *                 &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
     *                 &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="WaiverCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to19" /&amp;gt;
     *                 &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                 &amp;lt;attribute name="RuleCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
     *                 &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="AgreementCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fareComponentDetail"
    })
    public static class PriceableUnit {

        @XmlElement(name = "FareComponentDetail", required = true)
        protected List<FareComponentType.PriceableUnit.FareComponentDetail> fareComponentDetail;
        @XmlAttribute(name = "Number", required = true)
        protected String number;

        /**
         * Gets the value of the fareComponentDetail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareComponentDetail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFareComponentDetail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FareComponentType.PriceableUnit.FareComponentDetail }
         * 
         * 
         */
        public List<FareComponentType.PriceableUnit.FareComponentDetail> getFareComponentDetail() {
            if (fareComponentDetail == null) {
                fareComponentDetail = new ArrayList<FareComponentType.PriceableUnit.FareComponentDetail>();
            }
            return this.fareComponentDetail;
        }

        /**
         * Obtiene el valor de la propiedad number.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumber() {
            return number;
        }

        /**
         * Define el valor de la propiedad number.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumber(String value) {
            this.number = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CouponSequence" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
         *                 &amp;lt;attribute name="SequenceNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
         *                 &amp;lt;attribute name="StopoverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ConstructionPrinciple" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
         *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="Percent" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="BaseAmount" maxOccurs="3"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="TicketDesignator" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="TicketDesignatorCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="TicketDesignatorQualifier" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
         *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
         *       &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
         *       &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="WaiverCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to19" /&amp;gt;
         *       &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *       &amp;lt;attribute name="RuleCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
         *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="AgreementCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "couponSequence",
            "constructionPrinciple",
            "baseAmount",
            "ticketDesignator"
        })
        public static class FareComponentDetail {

            @XmlElement(name = "CouponSequence", required = true)
            protected List<FareComponentType.PriceableUnit.FareComponentDetail.CouponSequence> couponSequence;
            @XmlElement(name = "ConstructionPrinciple")
            protected List<FareComponentType.PriceableUnit.FareComponentDetail.ConstructionPrinciple> constructionPrinciple;
            @XmlElement(name = "BaseAmount", required = true)
            protected List<FareComponentType.PriceableUnit.FareComponentDetail.BaseAmount> baseAmount;
            @XmlElement(name = "TicketDesignator")
            protected List<FareComponentType.PriceableUnit.FareComponentDetail.TicketDesignator> ticketDesignator;
            @XmlAttribute(name = "Number", required = true)
            protected int number;
            @XmlAttribute(name = "TariffNumber")
            protected String tariffNumber;
            @XmlAttribute(name = "RuleNumber")
            protected String ruleNumber;
            @XmlAttribute(name = "WaiverCode")
            protected String waiverCode;
            @XmlAttribute(name = "PassengerTypeCode")
            protected String passengerTypeCode;
            @XmlAttribute(name = "RuleCode")
            protected String ruleCode;
            @XmlAttribute(name = "FareBasisCode")
            protected String fareBasisCode;
            @XmlAttribute(name = "AgreementCode")
            protected String agreementCode;
            @XmlAttribute(name = "CompanyShortName")
            protected String companyShortName;
            @XmlAttribute(name = "TravelSector")
            protected String travelSector;
            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "CountryCode")
            protected String countryCode;

            /**
             * Gets the value of the couponSequence property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the couponSequence property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCouponSequence().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link FareComponentType.PriceableUnit.FareComponentDetail.CouponSequence }
             * 
             * 
             */
            public List<FareComponentType.PriceableUnit.FareComponentDetail.CouponSequence> getCouponSequence() {
                if (couponSequence == null) {
                    couponSequence = new ArrayList<FareComponentType.PriceableUnit.FareComponentDetail.CouponSequence>();
                }
                return this.couponSequence;
            }

            /**
             * Gets the value of the constructionPrinciple property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the constructionPrinciple property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getConstructionPrinciple().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link FareComponentType.PriceableUnit.FareComponentDetail.ConstructionPrinciple }
             * 
             * 
             */
            public List<FareComponentType.PriceableUnit.FareComponentDetail.ConstructionPrinciple> getConstructionPrinciple() {
                if (constructionPrinciple == null) {
                    constructionPrinciple = new ArrayList<FareComponentType.PriceableUnit.FareComponentDetail.ConstructionPrinciple>();
                }
                return this.constructionPrinciple;
            }

            /**
             * Gets the value of the baseAmount property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baseAmount property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getBaseAmount().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link FareComponentType.PriceableUnit.FareComponentDetail.BaseAmount }
             * 
             * 
             */
            public List<FareComponentType.PriceableUnit.FareComponentDetail.BaseAmount> getBaseAmount() {
                if (baseAmount == null) {
                    baseAmount = new ArrayList<FareComponentType.PriceableUnit.FareComponentDetail.BaseAmount>();
                }
                return this.baseAmount;
            }

            /**
             * Gets the value of the ticketDesignator property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ticketDesignator property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTicketDesignator().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link FareComponentType.PriceableUnit.FareComponentDetail.TicketDesignator }
             * 
             * 
             */
            public List<FareComponentType.PriceableUnit.FareComponentDetail.TicketDesignator> getTicketDesignator() {
                if (ticketDesignator == null) {
                    ticketDesignator = new ArrayList<FareComponentType.PriceableUnit.FareComponentDetail.TicketDesignator>();
                }
                return this.ticketDesignator;
            }

            /**
             * Obtiene el valor de la propiedad number.
             * 
             */
            public int getNumber() {
                return number;
            }

            /**
             * Define el valor de la propiedad number.
             * 
             */
            public void setNumber(int value) {
                this.number = value;
            }

            /**
             * Obtiene el valor de la propiedad tariffNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTariffNumber() {
                return tariffNumber;
            }

            /**
             * Define el valor de la propiedad tariffNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTariffNumber(String value) {
                this.tariffNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad ruleNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleNumber() {
                return ruleNumber;
            }

            /**
             * Define el valor de la propiedad ruleNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleNumber(String value) {
                this.ruleNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad waiverCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWaiverCode() {
                return waiverCode;
            }

            /**
             * Define el valor de la propiedad waiverCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWaiverCode(String value) {
                this.waiverCode = value;
            }

            /**
             * Obtiene el valor de la propiedad passengerTypeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassengerTypeCode() {
                return passengerTypeCode;
            }

            /**
             * Define el valor de la propiedad passengerTypeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassengerTypeCode(String value) {
                this.passengerTypeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad ruleCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleCode() {
                return ruleCode;
            }

            /**
             * Define el valor de la propiedad ruleCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleCode(String value) {
                this.ruleCode = value;
            }

            /**
             * Obtiene el valor de la propiedad fareBasisCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareBasisCode() {
                return fareBasisCode;
            }

            /**
             * Define el valor de la propiedad fareBasisCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareBasisCode(String value) {
                this.fareBasisCode = value;
            }

            /**
             * Obtiene el valor de la propiedad agreementCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAgreementCode() {
                return agreementCode;
            }

            /**
             * Define el valor de la propiedad agreementCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAgreementCode(String value) {
                this.agreementCode = value;
            }

            /**
             * Obtiene el valor de la propiedad companyShortName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyShortName() {
                return companyShortName;
            }

            /**
             * Define el valor de la propiedad companyShortName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyShortName(String value) {
                this.companyShortName = value;
            }

            /**
             * Obtiene el valor de la propiedad travelSector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelSector() {
                return travelSector;
            }

            /**
             * Define el valor de la propiedad travelSector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelSector(String value) {
                this.travelSector = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad countryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Define el valor de la propiedad countryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *       &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class BaseAmount {

                @XmlAttribute(name = "Purpose")
                protected PurposeType purpose;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;

                /**
                 * Obtiene el valor de la propiedad purpose.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PurposeType }
                 *     
                 */
                public PurposeType getPurpose() {
                    return purpose;
                }

                /**
                 * Define el valor de la propiedad purpose.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PurposeType }
                 *     
                 */
                public void setPurpose(PurposeType value) {
                    this.purpose = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
             *       &amp;lt;attribute name="Code" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
             *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="Percent" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ConstructionPrinciple {

                @XmlAttribute(name = "Code", required = true)
                protected String code;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;
                @XmlAttribute(name = "Percent")
                protected BigDecimal percent;
                @XmlAttribute(name = "OriginCityCode")
                protected String originCityCode;
                @XmlAttribute(name = "OriginCodeContext")
                protected String originCodeContext;
                @XmlAttribute(name = "DestinationCityCode")
                protected String destinationCityCode;
                @XmlAttribute(name = "DestinationCodeContext")
                protected String destinationCodeContext;

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

                /**
                 * Obtiene el valor de la propiedad percent.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPercent() {
                    return percent;
                }

                /**
                 * Define el valor de la propiedad percent.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPercent(BigDecimal value) {
                    this.percent = value;
                }

                /**
                 * Obtiene el valor de la propiedad originCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginCityCode() {
                    return originCityCode;
                }

                /**
                 * Define el valor de la propiedad originCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginCityCode(String value) {
                    this.originCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad originCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginCodeContext() {
                    return originCodeContext;
                }

                /**
                 * Define el valor de la propiedad originCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginCodeContext(String value) {
                    this.originCodeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationCityCode() {
                    return destinationCityCode;
                }

                /**
                 * Define el valor de la propiedad destinationCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationCityCode(String value) {
                    this.destinationCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationCodeContext() {
                    return destinationCodeContext;
                }

                /**
                 * Define el valor de la propiedad destinationCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationCodeContext(String value) {
                    this.destinationCodeContext = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
             *       &amp;lt;attribute name="SequenceNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
             *       &amp;lt;attribute name="StopoverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CouponSequence {

                @XmlAttribute(name = "SequenceNbr")
                protected String sequenceNbr;
                @XmlAttribute(name = "CouponItinerarySeqNbr")
                protected Integer couponItinerarySeqNbr;
                @XmlAttribute(name = "StopoverInd")
                protected Boolean stopoverInd;
                @XmlAttribute(name = "ResBookDesigCode")
                protected String resBookDesigCode;
                @XmlAttribute(name = "CompanyShortName")
                protected String companyShortName;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "CountryCode")
                protected String countryCode;
                @XmlAttribute(name = "OriginCityCode")
                protected String originCityCode;
                @XmlAttribute(name = "OriginCodeContext")
                protected String originCodeContext;
                @XmlAttribute(name = "DestinationCityCode")
                protected String destinationCityCode;
                @XmlAttribute(name = "DestinationCodeContext")
                protected String destinationCodeContext;

                /**
                 * Obtiene el valor de la propiedad sequenceNbr.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSequenceNbr() {
                    return sequenceNbr;
                }

                /**
                 * Define el valor de la propiedad sequenceNbr.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSequenceNbr(String value) {
                    this.sequenceNbr = value;
                }

                /**
                 * Obtiene el valor de la propiedad couponItinerarySeqNbr.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getCouponItinerarySeqNbr() {
                    return couponItinerarySeqNbr;
                }

                /**
                 * Define el valor de la propiedad couponItinerarySeqNbr.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setCouponItinerarySeqNbr(Integer value) {
                    this.couponItinerarySeqNbr = value;
                }

                /**
                 * Obtiene el valor de la propiedad stopoverInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isStopoverInd() {
                    return stopoverInd;
                }

                /**
                 * Define el valor de la propiedad stopoverInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setStopoverInd(Boolean value) {
                    this.stopoverInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad resBookDesigCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResBookDesigCode() {
                    return resBookDesigCode;
                }

                /**
                 * Define el valor de la propiedad resBookDesigCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResBookDesigCode(String value) {
                    this.resBookDesigCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad companyShortName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCompanyShortName() {
                    return companyShortName;
                }

                /**
                 * Define el valor de la propiedad companyShortName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCompanyShortName(String value) {
                    this.companyShortName = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad countryCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryCode() {
                    return countryCode;
                }

                /**
                 * Define el valor de la propiedad countryCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryCode(String value) {
                    this.countryCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad originCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginCityCode() {
                    return originCityCode;
                }

                /**
                 * Define el valor de la propiedad originCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginCityCode(String value) {
                    this.originCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad originCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginCodeContext() {
                    return originCodeContext;
                }

                /**
                 * Define el valor de la propiedad originCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginCodeContext(String value) {
                    this.originCodeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationCityCode() {
                    return destinationCityCode;
                }

                /**
                 * Define el valor de la propiedad destinationCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationCityCode(String value) {
                    this.destinationCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationCodeContext() {
                    return destinationCodeContext;
                }

                /**
                 * Define el valor de la propiedad destinationCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationCodeContext(String value) {
                    this.destinationCodeContext = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="TicketDesignatorCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="TicketDesignatorQualifier" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class TicketDesignator {

                @XmlAttribute(name = "TicketDesignatorCode", required = true)
                protected String ticketDesignatorCode;
                @XmlAttribute(name = "TicketDesignatorQualifier")
                protected String ticketDesignatorQualifier;

                /**
                 * Obtiene el valor de la propiedad ticketDesignatorCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicketDesignatorCode() {
                    return ticketDesignatorCode;
                }

                /**
                 * Define el valor de la propiedad ticketDesignatorCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicketDesignatorCode(String value) {
                    this.ticketDesignatorCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad ticketDesignatorQualifier.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicketDesignatorQualifier() {
                    return ticketDesignatorQualifier;
                }

                /**
                 * Define el valor de la propiedad ticketDesignatorQualifier.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicketDesignatorQualifier(String value) {
                    this.ticketDesignatorQualifier = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="Purpose" use="required" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TotalConstructionAmount {

        @XmlAttribute(name = "Purpose", required = true)
        protected PurposeType purpose;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad purpose.
         * 
         * @return
         *     possible object is
         *     {@link PurposeType }
         *     
         */
        public PurposeType getPurpose() {
            return purpose;
        }

        /**
         * Define el valor de la propiedad purpose.
         * 
         * @param value
         *     allowed object is
         *     {@link PurposeType }
         *     
         */
        public void setPurpose(PurposeType value) {
            this.purpose = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }

}
