
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Describes summary row information, including number and characteristics.
 * 
 * &lt;p&gt;Clase Java para RowSummaryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RowSummaryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="MaxNumberOfSeats" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *       &amp;lt;attribute name="RowNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *       &amp;lt;attribute name="AirBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *       &amp;lt;attribute name="RowSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *       &amp;lt;attribute name="ColumnQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RowSummaryType")
public class RowSummaryType {

    @XmlAttribute(name = "MaxNumberOfSeats")
    protected Integer maxNumberOfSeats;
    @XmlAttribute(name = "RowNumber")
    protected BigInteger rowNumber;
    @XmlAttribute(name = "AirBookDesigCode")
    protected String airBookDesigCode;
    @XmlAttribute(name = "RowSequenceNumber")
    protected BigInteger rowSequenceNumber;
    @XmlAttribute(name = "ColumnQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger columnQty;

    /**
     * Obtiene el valor de la propiedad maxNumberOfSeats.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNumberOfSeats() {
        return maxNumberOfSeats;
    }

    /**
     * Define el valor de la propiedad maxNumberOfSeats.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNumberOfSeats(Integer value) {
        this.maxNumberOfSeats = value;
    }

    /**
     * Obtiene el valor de la propiedad rowNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowNumber() {
        return rowNumber;
    }

    /**
     * Define el valor de la propiedad rowNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowNumber(BigInteger value) {
        this.rowNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad airBookDesigCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirBookDesigCode() {
        return airBookDesigCode;
    }

    /**
     * Define el valor de la propiedad airBookDesigCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirBookDesigCode(String value) {
        this.airBookDesigCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rowSequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowSequenceNumber() {
        return rowSequenceNumber;
    }

    /**
     * Define el valor de la propiedad rowSequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowSequenceNumber(BigInteger value) {
        this.rowSequenceNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad columnQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColumnQty() {
        return columnQty;
    }

    /**
     * Define el valor de la propiedad columnQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColumnQty(BigInteger value) {
        this.columnQty = value;
    }

}
