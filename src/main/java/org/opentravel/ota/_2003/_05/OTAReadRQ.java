
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReadRequests" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;choice&amp;gt;
 *                   &amp;lt;element name="ReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
 *                             &amp;lt;element name="Verification" type="{http://www.opentravel.org/OTA/2003/05}VerificationType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="HistoryRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="GlobalReservationReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="TravelerName" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AirReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="DepartureAirport" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Telephone" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="TicketNumber" type="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="QueueInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Queue" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QueueGroup"/&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="FirstItemOnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="RemoveFromQueueInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="FullDataInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                     &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Date" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="SeatNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="IncludeFF_EquivPartnerLev" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="ReturnFF_Number" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="ReturnDownlineSeg" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="InfoToReturn" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="FF_RequestCriteria"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="ReturnLevelAndAbove"/&amp;gt;
 *                                 &amp;lt;enumeration value="ReturnLevelAndBelow"/&amp;gt;
 *                                 &amp;lt;enumeration value="ReturnOnlySpecifiedLevel"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="No_SSR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="CruiseReadRequest" maxOccurs="99" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="SelectedSailing" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedSailingGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="GuestInfo" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="HistoryRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="GolfReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Membership" maxOccurs="99" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_Group"/&amp;gt;
 *                           &amp;lt;attribute name="PlayDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="RoundID" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="HotelReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CityName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Airport" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                     &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                     &amp;lt;attribute name="AirportName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="UserID" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                                     &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Verification" type="{http://www.opentravel.org/OTA/2003/05}VerificationType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="SelectionCriteria" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="DateType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
 *                                           &amp;lt;enumeration value="DepartureDate"/&amp;gt;
 *                                           &amp;lt;enumeration value="CreateDate"/&amp;gt;
 *                                           &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
 *                                           &amp;lt;enumeration value="CancelDeadlineDate"/&amp;gt;
 *                                           &amp;lt;enumeration value="PaymentDeadlineDate"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="SelectionType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="All"/&amp;gt;
 *                                           &amp;lt;enumeration value="PreviouslyDelivered"/&amp;gt;
 *                                           &amp;lt;enumeration value="Undelivered"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="ResStatus" type="{http://www.opentravel.org/OTA/2003/05}HotelResStatusType" /&amp;gt;
 *                                     &amp;lt;attribute name="OriginalDeliveryMethodCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PkgReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="ArrivalLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="DepartureLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
 *                           &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}PackageID_RefType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ProfileReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" maxOccurs="9" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                                     &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Company" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Customer" type="{http://www.opentravel.org/OTA/2003/05}CustomerType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="DateType"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
 *                                 &amp;lt;enumeration value="DepartureDate"/&amp;gt;
 *                                 &amp;lt;enumeration value="CreateDate"/&amp;gt;
 *                                 &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="StatusCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="ProfileTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="RailReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Train" type="{http://www.opentravel.org/OTA/2003/05}TrainQueryType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="BookingDateTime" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="DepartureDateTime" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="VehicleReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleRetrieveResRQCoreType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="VehRetResRQInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleRetrieveResRQAdditionalInfoType"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="LoyaltyAccountReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                     &amp;lt;attribute name="MembershipID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/choice&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReqRespVersion"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attribute name="ReservationType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="ReturnListIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "uniqueID",
    "readRequests"
})
@XmlRootElement(name = "OTA_ReadRQ")
public class OTAReadRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "UniqueID")
    protected UniqueIDType uniqueID;
    @XmlElement(name = "ReadRequests")
    protected OTAReadRQ.ReadRequests readRequests;
    @XmlAttribute(name = "ReservationType")
    protected String reservationType;
    @XmlAttribute(name = "ReturnListIndicator")
    protected Boolean returnListIndicator;
    @XmlAttribute(name = "ReqRespVersion")
    protected String reqRespVersion;
    @XmlAttribute(name = "MoreIndicator")
    protected Boolean moreIndicator;
    @XmlAttribute(name = "MoreDataEchoToken")
    protected String moreDataEchoToken;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad uniqueID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getUniqueID() {
        return uniqueID;
    }

    /**
     * Define el valor de la propiedad uniqueID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setUniqueID(UniqueIDType value) {
        this.uniqueID = value;
    }

    /**
     * Obtiene el valor de la propiedad readRequests.
     * 
     * @return
     *     possible object is
     *     {@link OTAReadRQ.ReadRequests }
     *     
     */
    public OTAReadRQ.ReadRequests getReadRequests() {
        return readRequests;
    }

    /**
     * Define el valor de la propiedad readRequests.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAReadRQ.ReadRequests }
     *     
     */
    public void setReadRequests(OTAReadRQ.ReadRequests value) {
        this.readRequests = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationType() {
        return reservationType;
    }

    /**
     * Define el valor de la propiedad reservationType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationType(String value) {
        this.reservationType = value;
    }

    /**
     * Obtiene el valor de la propiedad returnListIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnListIndicator() {
        return returnListIndicator;
    }

    /**
     * Define el valor de la propiedad returnListIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnListIndicator(Boolean value) {
        this.returnListIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad reqRespVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReqRespVersion() {
        return reqRespVersion;
    }

    /**
     * Define el valor de la propiedad reqRespVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReqRespVersion(String value) {
        this.reqRespVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad moreIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMoreIndicator() {
        return moreIndicator;
    }

    /**
     * Define el valor de la propiedad moreIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMoreIndicator(Boolean value) {
        this.moreIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad moreDataEchoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreDataEchoToken() {
        return moreDataEchoToken;
    }

    /**
     * Define el valor de la propiedad moreDataEchoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreDataEchoToken(String value) {
        this.moreDataEchoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;choice&amp;gt;
     *         &amp;lt;element name="ReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
     *                   &amp;lt;element name="Verification" type="{http://www.opentravel.org/OTA/2003/05}VerificationType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="HistoryRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="GlobalReservationReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="TravelerName" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AirReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="DepartureAirport" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Telephone" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="TicketNumber" type="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="QueueInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Queue" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QueueGroup"/&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="FirstItemOnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="RemoveFromQueueInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="FullDataInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Date" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="SeatNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="IncludeFF_EquivPartnerLev" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ReturnFF_Number" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ReturnDownlineSeg" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="InfoToReturn" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="FF_RequestCriteria"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="ReturnLevelAndAbove"/&amp;gt;
     *                       &amp;lt;enumeration value="ReturnLevelAndBelow"/&amp;gt;
     *                       &amp;lt;enumeration value="ReturnOnlySpecifiedLevel"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="No_SSR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CruiseReadRequest" maxOccurs="99" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="SelectedSailing" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedSailingGroup"/&amp;gt;
     *                           &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="GuestInfo" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="HistoryRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="GolfReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Membership" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_Group"/&amp;gt;
     *                 &amp;lt;attribute name="PlayDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="RoundID" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="HotelReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CityName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Airport" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="AirportName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="UserID" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                           &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Verification" type="{http://www.opentravel.org/OTA/2003/05}VerificationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="SelectionCriteria" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="DateType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
     *                                 &amp;lt;enumeration value="DepartureDate"/&amp;gt;
     *                                 &amp;lt;enumeration value="CreateDate"/&amp;gt;
     *                                 &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
     *                                 &amp;lt;enumeration value="CancelDeadlineDate"/&amp;gt;
     *                                 &amp;lt;enumeration value="PaymentDeadlineDate"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="SelectionType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="All"/&amp;gt;
     *                                 &amp;lt;enumeration value="PreviouslyDelivered"/&amp;gt;
     *                                 &amp;lt;enumeration value="Undelivered"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="ResStatus" type="{http://www.opentravel.org/OTA/2003/05}HotelResStatusType" /&amp;gt;
     *                           &amp;lt;attribute name="OriginalDeliveryMethodCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PkgReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="ArrivalLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="DepartureLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
     *                 &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}PackageID_RefType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ProfileReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" maxOccurs="9" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                           &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Company" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Customer" type="{http://www.opentravel.org/OTA/2003/05}CustomerType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="DateType"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
     *                       &amp;lt;enumeration value="DepartureDate"/&amp;gt;
     *                       &amp;lt;enumeration value="CreateDate"/&amp;gt;
     *                       &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="StatusCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="ProfileTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="RailReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Train" type="{http://www.opentravel.org/OTA/2003/05}TrainQueryType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="BookingDateTime" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="DepartureDateTime" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="VehicleReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleRetrieveResRQCoreType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="VehRetResRQInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleRetrieveResRQAdditionalInfoType"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="LoyaltyAccountReadRequest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="MembershipID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/choice&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "readRequest",
        "globalReservationReadRequest",
        "airReadRequest",
        "cruiseReadRequest",
        "golfReadRequest",
        "hotelReadRequest",
        "pkgReadRequest",
        "profileReadRequest",
        "railReadRequest",
        "vehicleReadRequest",
        "loyaltyAccountReadRequest"
    })
    public static class ReadRequests {

        @XmlElement(name = "ReadRequest")
        protected List<OTAReadRQ.ReadRequests.ReadRequest> readRequest;
        @XmlElement(name = "GlobalReservationReadRequest")
        protected List<OTAReadRQ.ReadRequests.GlobalReservationReadRequest> globalReservationReadRequest;
        @XmlElement(name = "AirReadRequest")
        protected List<OTAReadRQ.ReadRequests.AirReadRequest> airReadRequest;
        @XmlElement(name = "CruiseReadRequest")
        protected List<OTAReadRQ.ReadRequests.CruiseReadRequest> cruiseReadRequest;
        @XmlElement(name = "GolfReadRequest")
        protected List<OTAReadRQ.ReadRequests.GolfReadRequest> golfReadRequest;
        @XmlElement(name = "HotelReadRequest")
        protected List<OTAReadRQ.ReadRequests.HotelReadRequest> hotelReadRequest;
        @XmlElement(name = "PkgReadRequest")
        protected List<OTAReadRQ.ReadRequests.PkgReadRequest> pkgReadRequest;
        @XmlElement(name = "ProfileReadRequest")
        protected List<OTAReadRQ.ReadRequests.ProfileReadRequest> profileReadRequest;
        @XmlElement(name = "RailReadRequest")
        protected List<OTAReadRQ.ReadRequests.RailReadRequest> railReadRequest;
        @XmlElement(name = "VehicleReadRequest")
        protected List<OTAReadRQ.ReadRequests.VehicleReadRequest> vehicleReadRequest;
        @XmlElement(name = "LoyaltyAccountReadRequest")
        protected List<OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest> loyaltyAccountReadRequest;

        /**
         * Gets the value of the readRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the readRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.ReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.ReadRequest> getReadRequest() {
            if (readRequest == null) {
                readRequest = new ArrayList<OTAReadRQ.ReadRequests.ReadRequest>();
            }
            return this.readRequest;
        }

        /**
         * Gets the value of the globalReservationReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the globalReservationReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getGlobalReservationReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.GlobalReservationReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.GlobalReservationReadRequest> getGlobalReservationReadRequest() {
            if (globalReservationReadRequest == null) {
                globalReservationReadRequest = new ArrayList<OTAReadRQ.ReadRequests.GlobalReservationReadRequest>();
            }
            return this.globalReservationReadRequest;
        }

        /**
         * Gets the value of the airReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAirReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.AirReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.AirReadRequest> getAirReadRequest() {
            if (airReadRequest == null) {
                airReadRequest = new ArrayList<OTAReadRQ.ReadRequests.AirReadRequest>();
            }
            return this.airReadRequest;
        }

        /**
         * Gets the value of the cruiseReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cruiseReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCruiseReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.CruiseReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.CruiseReadRequest> getCruiseReadRequest() {
            if (cruiseReadRequest == null) {
                cruiseReadRequest = new ArrayList<OTAReadRQ.ReadRequests.CruiseReadRequest>();
            }
            return this.cruiseReadRequest;
        }

        /**
         * Gets the value of the golfReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the golfReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getGolfReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.GolfReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.GolfReadRequest> getGolfReadRequest() {
            if (golfReadRequest == null) {
                golfReadRequest = new ArrayList<OTAReadRQ.ReadRequests.GolfReadRequest>();
            }
            return this.golfReadRequest;
        }

        /**
         * Gets the value of the hotelReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the hotelReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getHotelReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.HotelReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.HotelReadRequest> getHotelReadRequest() {
            if (hotelReadRequest == null) {
                hotelReadRequest = new ArrayList<OTAReadRQ.ReadRequests.HotelReadRequest>();
            }
            return this.hotelReadRequest;
        }

        /**
         * Gets the value of the pkgReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pkgReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPkgReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.PkgReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.PkgReadRequest> getPkgReadRequest() {
            if (pkgReadRequest == null) {
                pkgReadRequest = new ArrayList<OTAReadRQ.ReadRequests.PkgReadRequest>();
            }
            return this.pkgReadRequest;
        }

        /**
         * Gets the value of the profileReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the profileReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getProfileReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.ProfileReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.ProfileReadRequest> getProfileReadRequest() {
            if (profileReadRequest == null) {
                profileReadRequest = new ArrayList<OTAReadRQ.ReadRequests.ProfileReadRequest>();
            }
            return this.profileReadRequest;
        }

        /**
         * Gets the value of the railReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the railReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRailReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.RailReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.RailReadRequest> getRailReadRequest() {
            if (railReadRequest == null) {
                railReadRequest = new ArrayList<OTAReadRQ.ReadRequests.RailReadRequest>();
            }
            return this.railReadRequest;
        }

        /**
         * Gets the value of the vehicleReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicleReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVehicleReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.VehicleReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.VehicleReadRequest> getVehicleReadRequest() {
            if (vehicleReadRequest == null) {
                vehicleReadRequest = new ArrayList<OTAReadRQ.ReadRequests.VehicleReadRequest>();
            }
            return this.vehicleReadRequest;
        }

        /**
         * Gets the value of the loyaltyAccountReadRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyaltyAccountReadRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLoyaltyAccountReadRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest }
         * 
         * 
         */
        public List<OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest> getLoyaltyAccountReadRequest() {
            if (loyaltyAccountReadRequest == null) {
                loyaltyAccountReadRequest = new ArrayList<OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest>();
            }
            return this.loyaltyAccountReadRequest;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="DepartureAirport" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Telephone" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CreditCardInfo" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="TicketNumber" type="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="QueueInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Queue" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QueueGroup"/&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="FirstItemOnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="RemoveFromQueueInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="FullDataInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Date" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="SeatNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="IncludeFF_EquivPartnerLev" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ReturnFF_Number" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ReturnDownlineSeg" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="InfoToReturn" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="FF_RequestCriteria"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="ReturnLevelAndAbove"/&amp;gt;
         *             &amp;lt;enumeration value="ReturnLevelAndBelow"/&amp;gt;
         *             &amp;lt;enumeration value="ReturnOnlySpecifiedLevel"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="No_SSR_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pos",
            "airline",
            "flightNumber",
            "departureAirport",
            "departureDate",
            "name",
            "telephone",
            "custLoyalty",
            "creditCardInfo",
            "ticketNumber",
            "queueInfo",
            "date",
            "tpaExtensions"
        })
        public static class AirReadRequest {

            @XmlElement(name = "POS")
            protected POSType pos;
            @XmlElement(name = "Airline")
            protected CompanyNameType airline;
            @XmlElement(name = "FlightNumber")
            protected String flightNumber;
            @XmlElement(name = "DepartureAirport")
            protected LocationType departureAirport;
            @XmlElement(name = "DepartureDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar departureDate;
            @XmlElement(name = "Name")
            protected PersonNameType name;
            @XmlElement(name = "Telephone")
            protected OTAReadRQ.ReadRequests.AirReadRequest.Telephone telephone;
            @XmlElement(name = "CustLoyalty")
            protected OTAReadRQ.ReadRequests.AirReadRequest.CustLoyalty custLoyalty;
            @XmlElement(name = "CreditCardInfo")
            protected PaymentCardType creditCardInfo;
            @XmlElement(name = "TicketNumber")
            protected TicketingInfoRSType ticketNumber;
            @XmlElement(name = "QueueInfo")
            protected OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo queueInfo;
            @XmlElement(name = "Date")
            protected OTAReadRQ.ReadRequests.AirReadRequest.Date date;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;
            @XmlAttribute(name = "SeatNumber")
            protected String seatNumber;
            @XmlAttribute(name = "IncludeFF_EquivPartnerLev")
            protected Boolean includeFFEquivPartnerLev;
            @XmlAttribute(name = "ReturnFF_Number")
            protected Boolean returnFFNumber;
            @XmlAttribute(name = "ReturnDownlineSeg")
            protected Boolean returnDownlineSeg;
            @XmlAttribute(name = "InfoToReturn")
            protected String infoToReturn;
            @XmlAttribute(name = "FF_RequestCriteria")
            protected String ffRequestCriteria;
            @XmlAttribute(name = "No_SSR_Ind")
            protected Boolean noSSRInd;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad pos.
             * 
             * @return
             *     possible object is
             *     {@link POSType }
             *     
             */
            public POSType getPOS() {
                return pos;
            }

            /**
             * Define el valor de la propiedad pos.
             * 
             * @param value
             *     allowed object is
             *     {@link POSType }
             *     
             */
            public void setPOS(POSType value) {
                this.pos = value;
            }

            /**
             * Obtiene el valor de la propiedad airline.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getAirline() {
                return airline;
            }

            /**
             * Define el valor de la propiedad airline.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setAirline(CompanyNameType value) {
                this.airline = value;
            }

            /**
             * Obtiene el valor de la propiedad flightNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlightNumber() {
                return flightNumber;
            }

            /**
             * Define el valor de la propiedad flightNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlightNumber(String value) {
                this.flightNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad departureAirport.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDepartureAirport() {
                return departureAirport;
            }

            /**
             * Define el valor de la propiedad departureAirport.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDepartureAirport(LocationType value) {
                this.departureAirport = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDepartureDate() {
                return departureDate;
            }

            /**
             * Define el valor de la propiedad departureDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDepartureDate(XMLGregorianCalendar value) {
                this.departureDate = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link PersonNameType }
             *     
             */
            public PersonNameType getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link PersonNameType }
             *     
             */
            public void setName(PersonNameType value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad telephone.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.Telephone }
             *     
             */
            public OTAReadRQ.ReadRequests.AirReadRequest.Telephone getTelephone() {
                return telephone;
            }

            /**
             * Define el valor de la propiedad telephone.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.Telephone }
             *     
             */
            public void setTelephone(OTAReadRQ.ReadRequests.AirReadRequest.Telephone value) {
                this.telephone = value;
            }

            /**
             * Obtiene el valor de la propiedad custLoyalty.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.CustLoyalty }
             *     
             */
            public OTAReadRQ.ReadRequests.AirReadRequest.CustLoyalty getCustLoyalty() {
                return custLoyalty;
            }

            /**
             * Define el valor de la propiedad custLoyalty.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.CustLoyalty }
             *     
             */
            public void setCustLoyalty(OTAReadRQ.ReadRequests.AirReadRequest.CustLoyalty value) {
                this.custLoyalty = value;
            }

            /**
             * Obtiene el valor de la propiedad creditCardInfo.
             * 
             * @return
             *     possible object is
             *     {@link PaymentCardType }
             *     
             */
            public PaymentCardType getCreditCardInfo() {
                return creditCardInfo;
            }

            /**
             * Define el valor de la propiedad creditCardInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link PaymentCardType }
             *     
             */
            public void setCreditCardInfo(PaymentCardType value) {
                this.creditCardInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad ticketNumber.
             * 
             * @return
             *     possible object is
             *     {@link TicketingInfoRSType }
             *     
             */
            public TicketingInfoRSType getTicketNumber() {
                return ticketNumber;
            }

            /**
             * Define el valor de la propiedad ticketNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link TicketingInfoRSType }
             *     
             */
            public void setTicketNumber(TicketingInfoRSType value) {
                this.ticketNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad queueInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo }
             *     
             */
            public OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo getQueueInfo() {
                return queueInfo;
            }

            /**
             * Define el valor de la propiedad queueInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo }
             *     
             */
            public void setQueueInfo(OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo value) {
                this.queueInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad date.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.Date }
             *     
             */
            public OTAReadRQ.ReadRequests.AirReadRequest.Date getDate() {
                return date;
            }

            /**
             * Define el valor de la propiedad date.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.AirReadRequest.Date }
             *     
             */
            public void setDate(OTAReadRQ.ReadRequests.AirReadRequest.Date value) {
                this.date = value;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }

            /**
             * Obtiene el valor de la propiedad seatNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSeatNumber() {
                return seatNumber;
            }

            /**
             * Define el valor de la propiedad seatNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSeatNumber(String value) {
                this.seatNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad includeFFEquivPartnerLev.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIncludeFFEquivPartnerLev() {
                return includeFFEquivPartnerLev;
            }

            /**
             * Define el valor de la propiedad includeFFEquivPartnerLev.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIncludeFFEquivPartnerLev(Boolean value) {
                this.includeFFEquivPartnerLev = value;
            }

            /**
             * Obtiene el valor de la propiedad returnFFNumber.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReturnFFNumber() {
                return returnFFNumber;
            }

            /**
             * Define el valor de la propiedad returnFFNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReturnFFNumber(Boolean value) {
                this.returnFFNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad returnDownlineSeg.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReturnDownlineSeg() {
                return returnDownlineSeg;
            }

            /**
             * Define el valor de la propiedad returnDownlineSeg.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReturnDownlineSeg(Boolean value) {
                this.returnDownlineSeg = value;
            }

            /**
             * Obtiene el valor de la propiedad infoToReturn.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInfoToReturn() {
                return infoToReturn;
            }

            /**
             * Define el valor de la propiedad infoToReturn.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInfoToReturn(String value) {
                this.infoToReturn = value;
            }

            /**
             * Obtiene el valor de la propiedad ffRequestCriteria.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFFRequestCriteria() {
                return ffRequestCriteria;
            }

            /**
             * Define el valor de la propiedad ffRequestCriteria.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFFRequestCriteria(String value) {
                this.ffRequestCriteria = value;
            }

            /**
             * Obtiene el valor de la propiedad noSSRInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isNoSSRInd() {
                return noSSRInd;
            }

            /**
             * Define el valor de la propiedad noSSRInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setNoSSRInd(Boolean value) {
                this.noSSRInd = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CustLoyalty {

                @XmlAttribute(name = "ShareSynchInd")
                protected String shareSynchInd;
                @XmlAttribute(name = "ShareMarketInd")
                protected String shareMarketInd;
                @XmlAttribute(name = "ProgramID")
                protected String programID;
                @XmlAttribute(name = "MembershipID")
                protected String membershipID;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "VendorCode")
                protected List<String> vendorCode;
                @XmlAttribute(name = "PrimaryLoyaltyIndicator")
                protected Boolean primaryLoyaltyIndicator;
                @XmlAttribute(name = "AllianceLoyaltyLevelName")
                protected String allianceLoyaltyLevelName;
                @XmlAttribute(name = "CustomerType")
                protected String customerType;
                @XmlAttribute(name = "CustomerValue")
                protected String customerValue;
                @XmlAttribute(name = "Password")
                protected String password;
                @XmlAttribute(name = "LoyalLevel")
                protected String loyalLevel;
                @XmlAttribute(name = "LoyalLevelCode")
                protected Integer loyalLevelCode;
                @XmlAttribute(name = "SingleVendorInd")
                protected String singleVendorInd;
                @XmlAttribute(name = "SignupDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar signupDate;
                @XmlAttribute(name = "EffectiveDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar effectiveDate;
                @XmlAttribute(name = "ExpireDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar expireDate;
                @XmlAttribute(name = "ExpireDateExclusiveIndicator")
                protected Boolean expireDateExclusiveIndicator;
                @XmlAttribute(name = "RPH")
                protected String rph;

                /**
                 * Obtiene el valor de la propiedad shareSynchInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareSynchInd() {
                    return shareSynchInd;
                }

                /**
                 * Define el valor de la propiedad shareSynchInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareSynchInd(String value) {
                    this.shareSynchInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareMarketInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareMarketInd() {
                    return shareMarketInd;
                }

                /**
                 * Define el valor de la propiedad shareMarketInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareMarketInd(String value) {
                    this.shareMarketInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad programID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramID() {
                    return programID;
                }

                /**
                 * Define el valor de la propiedad programID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramID(String value) {
                    this.programID = value;
                }

                /**
                 * Obtiene el valor de la propiedad membershipID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMembershipID() {
                    return membershipID;
                }

                /**
                 * Define el valor de la propiedad membershipID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMembershipID(String value) {
                    this.membershipID = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Gets the value of the vendorCode property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getVendorCode().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getVendorCode() {
                    if (vendorCode == null) {
                        vendorCode = new ArrayList<String>();
                    }
                    return this.vendorCode;
                }

                /**
                 * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPrimaryLoyaltyIndicator() {
                    return primaryLoyaltyIndicator;
                }

                /**
                 * Define el valor de la propiedad primaryLoyaltyIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPrimaryLoyaltyIndicator(Boolean value) {
                    this.primaryLoyaltyIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllianceLoyaltyLevelName() {
                    return allianceLoyaltyLevelName;
                }

                /**
                 * Define el valor de la propiedad allianceLoyaltyLevelName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllianceLoyaltyLevelName(String value) {
                    this.allianceLoyaltyLevelName = value;
                }

                /**
                 * Obtiene el valor de la propiedad customerType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerType() {
                    return customerType;
                }

                /**
                 * Define el valor de la propiedad customerType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerType(String value) {
                    this.customerType = value;
                }

                /**
                 * Obtiene el valor de la propiedad customerValue.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerValue() {
                    return customerValue;
                }

                /**
                 * Define el valor de la propiedad customerValue.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerValue(String value) {
                    this.customerValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad password.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPassword() {
                    return password;
                }

                /**
                 * Define el valor de la propiedad password.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPassword(String value) {
                    this.password = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyalLevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLoyalLevel() {
                    return loyalLevel;
                }

                /**
                 * Define el valor de la propiedad loyalLevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLoyalLevel(String value) {
                    this.loyalLevel = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyalLevelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getLoyalLevelCode() {
                    return loyalLevelCode;
                }

                /**
                 * Define el valor de la propiedad loyalLevelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setLoyalLevelCode(Integer value) {
                    this.loyalLevelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad singleVendorInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSingleVendorInd() {
                    return singleVendorInd;
                }

                /**
                 * Define el valor de la propiedad singleVendorInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSingleVendorInd(String value) {
                    this.singleVendorInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad signupDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getSignupDate() {
                    return signupDate;
                }

                /**
                 * Define el valor de la propiedad signupDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setSignupDate(XMLGregorianCalendar value) {
                    this.signupDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad effectiveDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getEffectiveDate() {
                    return effectiveDate;
                }

                /**
                 * Define el valor de la propiedad effectiveDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setEffectiveDate(XMLGregorianCalendar value) {
                    this.effectiveDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad expireDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getExpireDate() {
                    return expireDate;
                }

                /**
                 * Define el valor de la propiedad expireDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setExpireDate(XMLGregorianCalendar value) {
                    this.expireDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isExpireDateExclusiveIndicator() {
                    return expireDateExclusiveIndicator;
                }

                /**
                 * Define el valor de la propiedad expireDateExclusiveIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setExpireDateExclusiveIndicator(Boolean value) {
                    this.expireDateExclusiveIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRPH(String value) {
                    this.rph = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Date {

                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Queue" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QueueGroup"/&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="FirstItemOnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="RemoveFromQueueInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="FullDataInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "queue"
            })
            public static class QueueInfo {

                @XmlElement(name = "Queue", required = true)
                protected List<OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo.Queue> queue;
                @XmlAttribute(name = "FirstItemOnlyInd")
                protected Boolean firstItemOnlyInd;
                @XmlAttribute(name = "RemoveFromQueueInd")
                protected Boolean removeFromQueueInd;
                @XmlAttribute(name = "FullDataInd")
                protected Boolean fullDataInd;
                @XmlAttribute(name = "StartDate")
                protected String startDate;
                @XmlAttribute(name = "EndDate")
                protected String endDate;

                /**
                 * Gets the value of the queue property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the queue property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getQueue().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo.Queue }
                 * 
                 * 
                 */
                public List<OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo.Queue> getQueue() {
                    if (queue == null) {
                        queue = new ArrayList<OTAReadRQ.ReadRequests.AirReadRequest.QueueInfo.Queue>();
                    }
                    return this.queue;
                }

                /**
                 * Obtiene el valor de la propiedad firstItemOnlyInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isFirstItemOnlyInd() {
                    return firstItemOnlyInd;
                }

                /**
                 * Define el valor de la propiedad firstItemOnlyInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setFirstItemOnlyInd(Boolean value) {
                    this.firstItemOnlyInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad removeFromQueueInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isRemoveFromQueueInd() {
                    return removeFromQueueInd;
                }

                /**
                 * Define el valor de la propiedad removeFromQueueInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setRemoveFromQueueInd(Boolean value) {
                    this.removeFromQueueInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad fullDataInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isFullDataInd() {
                    return fullDataInd;
                }

                /**
                 * Define el valor de la propiedad fullDataInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setFullDataInd(Boolean value) {
                    this.fullDataInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad startDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStartDate() {
                    return startDate;
                }

                /**
                 * Define el valor de la propiedad startDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStartDate(String value) {
                    this.startDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad endDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndDate() {
                    return endDate;
                }

                /**
                 * Define el valor de la propiedad endDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndDate(String value) {
                    this.endDate = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QueueGroup"/&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Queue {

                    @XmlAttribute(name = "PseudoCityCode")
                    protected String pseudoCityCode;
                    @XmlAttribute(name = "QueueNumber")
                    protected String queueNumber;
                    @XmlAttribute(name = "QueueCategory")
                    protected String queueCategory;
                    @XmlAttribute(name = "SystemCode")
                    protected String systemCode;
                    @XmlAttribute(name = "QueueID")
                    protected String queueID;

                    /**
                     * Obtiene el valor de la propiedad pseudoCityCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPseudoCityCode() {
                        return pseudoCityCode;
                    }

                    /**
                     * Define el valor de la propiedad pseudoCityCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPseudoCityCode(String value) {
                        this.pseudoCityCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad queueNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getQueueNumber() {
                        return queueNumber;
                    }

                    /**
                     * Define el valor de la propiedad queueNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setQueueNumber(String value) {
                        this.queueNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad queueCategory.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getQueueCategory() {
                        return queueCategory;
                    }

                    /**
                     * Define el valor de la propiedad queueCategory.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setQueueCategory(String value) {
                        this.queueCategory = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad systemCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getSystemCode() {
                        return systemCode;
                    }

                    /**
                     * Define el valor de la propiedad systemCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setSystemCode(String value) {
                        this.systemCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad queueID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getQueueID() {
                        return queueID;
                    }

                    /**
                     * Define el valor de la propiedad queueID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setQueueID(String value) {
                        this.queueID = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TelephoneGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Telephone {

                @XmlAttribute(name = "ShareSynchInd")
                protected String shareSynchInd;
                @XmlAttribute(name = "ShareMarketInd")
                protected String shareMarketInd;
                @XmlAttribute(name = "PhoneLocationType")
                protected String phoneLocationType;
                @XmlAttribute(name = "PhoneTechType")
                protected String phoneTechType;
                @XmlAttribute(name = "PhoneUseType")
                protected String phoneUseType;
                @XmlAttribute(name = "CountryAccessCode")
                protected String countryAccessCode;
                @XmlAttribute(name = "AreaCityCode")
                protected String areaCityCode;
                @XmlAttribute(name = "PhoneNumber", required = true)
                protected String phoneNumber;
                @XmlAttribute(name = "Extension")
                protected String extension;
                @XmlAttribute(name = "PIN")
                protected String pin;
                @XmlAttribute(name = "Remark")
                protected String remark;
                @XmlAttribute(name = "FormattedInd")
                protected Boolean formattedInd;

                /**
                 * Obtiene el valor de la propiedad shareSynchInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareSynchInd() {
                    return shareSynchInd;
                }

                /**
                 * Define el valor de la propiedad shareSynchInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareSynchInd(String value) {
                    this.shareSynchInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareMarketInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareMarketInd() {
                    return shareMarketInd;
                }

                /**
                 * Define el valor de la propiedad shareMarketInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareMarketInd(String value) {
                    this.shareMarketInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneLocationType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneLocationType() {
                    return phoneLocationType;
                }

                /**
                 * Define el valor de la propiedad phoneLocationType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneLocationType(String value) {
                    this.phoneLocationType = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneTechType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneTechType() {
                    return phoneTechType;
                }

                /**
                 * Define el valor de la propiedad phoneTechType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneTechType(String value) {
                    this.phoneTechType = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneUseType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneUseType() {
                    return phoneUseType;
                }

                /**
                 * Define el valor de la propiedad phoneUseType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneUseType(String value) {
                    this.phoneUseType = value;
                }

                /**
                 * Obtiene el valor de la propiedad countryAccessCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryAccessCode() {
                    return countryAccessCode;
                }

                /**
                 * Define el valor de la propiedad countryAccessCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryAccessCode(String value) {
                    this.countryAccessCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad areaCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAreaCityCode() {
                    return areaCityCode;
                }

                /**
                 * Define el valor de la propiedad areaCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAreaCityCode(String value) {
                    this.areaCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad phoneNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNumber() {
                    return phoneNumber;
                }

                /**
                 * Define el valor de la propiedad phoneNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNumber(String value) {
                    this.phoneNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad extension.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExtension() {
                    return extension;
                }

                /**
                 * Define el valor de la propiedad extension.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExtension(String value) {
                    this.extension = value;
                }

                /**
                 * Obtiene el valor de la propiedad pin.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPIN() {
                    return pin;
                }

                /**
                 * Define el valor de la propiedad pin.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPIN(String value) {
                    this.pin = value;
                }

                /**
                 * Obtiene el valor de la propiedad remark.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRemark() {
                    return remark;
                }

                /**
                 * Define el valor de la propiedad remark.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRemark(String value) {
                    this.remark = value;
                }

                /**
                 * Obtiene el valor de la propiedad formattedInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isFormattedInd() {
                    return formattedInd;
                }

                /**
                 * Define el valor de la propiedad formattedInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setFormattedInd(Boolean value) {
                    this.formattedInd = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="SelectedSailing" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedSailingGroup"/&amp;gt;
         *                 &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="GuestInfo" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="HistoryRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "selectedSailing",
            "guestInfo"
        })
        public static class CruiseReadRequest {

            @XmlElement(name = "SelectedSailing")
            protected OTAReadRQ.ReadRequests.CruiseReadRequest.SelectedSailing selectedSailing;
            @XmlElement(name = "GuestInfo")
            protected PersonNameType guestInfo;
            @XmlAttribute(name = "HistoryRequestedInd")
            protected Boolean historyRequestedInd;

            /**
             * Obtiene el valor de la propiedad selectedSailing.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.CruiseReadRequest.SelectedSailing }
             *     
             */
            public OTAReadRQ.ReadRequests.CruiseReadRequest.SelectedSailing getSelectedSailing() {
                return selectedSailing;
            }

            /**
             * Define el valor de la propiedad selectedSailing.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.CruiseReadRequest.SelectedSailing }
             *     
             */
            public void setSelectedSailing(OTAReadRQ.ReadRequests.CruiseReadRequest.SelectedSailing value) {
                this.selectedSailing = value;
            }

            /**
             * Obtiene el valor de la propiedad guestInfo.
             * 
             * @return
             *     possible object is
             *     {@link PersonNameType }
             *     
             */
            public PersonNameType getGuestInfo() {
                return guestInfo;
            }

            /**
             * Define el valor de la propiedad guestInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link PersonNameType }
             *     
             */
            public void setGuestInfo(PersonNameType value) {
                this.guestInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad historyRequestedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isHistoryRequestedInd() {
                return historyRequestedInd;
            }

            /**
             * Define el valor de la propiedad historyRequestedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setHistoryRequestedInd(Boolean value) {
                this.historyRequestedInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedSailingGroup"/&amp;gt;
             *       &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class SelectedSailing {

                @XmlAttribute(name = "GroupCode")
                protected String groupCode;
                @XmlAttribute(name = "VoyageID")
                protected String voyageID;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;
                @XmlAttribute(name = "VendorCode")
                protected String vendorCode;
                @XmlAttribute(name = "VendorName")
                protected String vendorName;
                @XmlAttribute(name = "ShipCode")
                protected String shipCode;
                @XmlAttribute(name = "ShipName")
                protected String shipName;
                @XmlAttribute(name = "VendorCodeContext")
                protected String vendorCodeContext;
                @XmlAttribute(name = "Status")
                protected String status;

                /**
                 * Obtiene el valor de la propiedad groupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupCode() {
                    return groupCode;
                }

                /**
                 * Define el valor de la propiedad groupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupCode(String value) {
                    this.groupCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad voyageID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getVoyageID() {
                    return voyageID;
                }

                /**
                 * Define el valor de la propiedad voyageID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVoyageID(String value) {
                    this.voyageID = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

                /**
                 * Obtiene el valor de la propiedad vendorCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getVendorCode() {
                    return vendorCode;
                }

                /**
                 * Define el valor de la propiedad vendorCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVendorCode(String value) {
                    this.vendorCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad vendorName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getVendorName() {
                    return vendorName;
                }

                /**
                 * Define el valor de la propiedad vendorName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVendorName(String value) {
                    this.vendorName = value;
                }

                /**
                 * Obtiene el valor de la propiedad shipCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShipCode() {
                    return shipCode;
                }

                /**
                 * Define el valor de la propiedad shipCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShipCode(String value) {
                    this.shipCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad shipName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShipName() {
                    return shipName;
                }

                /**
                 * Define el valor de la propiedad shipName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShipName(String value) {
                    this.shipName = value;
                }

                /**
                 * Obtiene el valor de la propiedad vendorCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getVendorCodeContext() {
                    return vendorCodeContext;
                }

                /**
                 * Define el valor de la propiedad vendorCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVendorCodeContext(String value) {
                    this.vendorCodeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad status.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStatus() {
                    return status;
                }

                /**
                 * Define el valor de la propiedad status.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStatus(String value) {
                    this.status = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="TravelerName" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "travelerName"
        })
        public static class GlobalReservationReadRequest {

            @XmlElement(name = "TravelerName", required = true)
            protected PersonNameType travelerName;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad travelerName.
             * 
             * @return
             *     possible object is
             *     {@link PersonNameType }
             *     
             */
            public PersonNameType getTravelerName() {
                return travelerName;
            }

            /**
             * Define el valor de la propiedad travelerName.
             * 
             * @param value
             *     allowed object is
             *     {@link PersonNameType }
             *     
             */
            public void setTravelerName(PersonNameType value) {
                this.travelerName = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Membership" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_Group"/&amp;gt;
         *       &amp;lt;attribute name="PlayDateTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="RoundID" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "membership",
            "name"
        })
        public static class GolfReadRequest {

            @XmlElement(name = "Membership")
            protected List<OTAReadRQ.ReadRequests.GolfReadRequest.Membership> membership;
            @XmlElement(name = "Name")
            protected PersonNameType name;
            @XmlAttribute(name = "PlayDateTime")
            protected String playDateTime;
            @XmlAttribute(name = "PackageID")
            protected String packageID;
            @XmlAttribute(name = "RoundID")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger roundID;
            @XmlAttribute(name = "ID", required = true)
            protected String id;

            /**
             * Gets the value of the membership property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the membership property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMembership().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAReadRQ.ReadRequests.GolfReadRequest.Membership }
             * 
             * 
             */
            public List<OTAReadRQ.ReadRequests.GolfReadRequest.Membership> getMembership() {
                if (membership == null) {
                    membership = new ArrayList<OTAReadRQ.ReadRequests.GolfReadRequest.Membership>();
                }
                return this.membership;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link PersonNameType }
             *     
             */
            public PersonNameType getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link PersonNameType }
             *     
             */
            public void setName(PersonNameType value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad playDateTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlayDateTime() {
                return playDateTime;
            }

            /**
             * Define el valor de la propiedad playDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlayDateTime(String value) {
                this.playDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad packageID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPackageID() {
                return packageID;
            }

            /**
             * Define el valor de la propiedad packageID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPackageID(String value) {
                this.packageID = value;
            }

            /**
             * Obtiene el valor de la propiedad roundID.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getRoundID() {
                return roundID;
            }

            /**
             * Define el valor de la propiedad roundID.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setRoundID(BigInteger value) {
                this.roundID = value;
            }

            /**
             * Obtiene el valor de la propiedad id.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getID() {
                return id;
            }

            /**
             * Define el valor de la propiedad id.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setID(String value) {
                this.id = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Membership {

                @XmlAttribute(name = "ShareSynchInd")
                protected String shareSynchInd;
                @XmlAttribute(name = "ShareMarketInd")
                protected String shareMarketInd;
                @XmlAttribute(name = "ProgramID")
                protected String programID;
                @XmlAttribute(name = "MembershipID")
                protected String membershipID;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "VendorCode")
                protected List<String> vendorCode;
                @XmlAttribute(name = "PrimaryLoyaltyIndicator")
                protected Boolean primaryLoyaltyIndicator;
                @XmlAttribute(name = "AllianceLoyaltyLevelName")
                protected String allianceLoyaltyLevelName;
                @XmlAttribute(name = "CustomerType")
                protected String customerType;
                @XmlAttribute(name = "CustomerValue")
                protected String customerValue;
                @XmlAttribute(name = "Password")
                protected String password;
                @XmlAttribute(name = "LoyalLevel")
                protected String loyalLevel;
                @XmlAttribute(name = "LoyalLevelCode")
                protected Integer loyalLevelCode;
                @XmlAttribute(name = "SingleVendorInd")
                protected String singleVendorInd;
                @XmlAttribute(name = "SignupDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar signupDate;
                @XmlAttribute(name = "EffectiveDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar effectiveDate;
                @XmlAttribute(name = "ExpireDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar expireDate;
                @XmlAttribute(name = "ExpireDateExclusiveIndicator")
                protected Boolean expireDateExclusiveIndicator;
                @XmlAttribute(name = "RPH")
                protected String rph;

                /**
                 * Obtiene el valor de la propiedad shareSynchInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareSynchInd() {
                    return shareSynchInd;
                }

                /**
                 * Define el valor de la propiedad shareSynchInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareSynchInd(String value) {
                    this.shareSynchInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareMarketInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareMarketInd() {
                    return shareMarketInd;
                }

                /**
                 * Define el valor de la propiedad shareMarketInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareMarketInd(String value) {
                    this.shareMarketInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad programID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramID() {
                    return programID;
                }

                /**
                 * Define el valor de la propiedad programID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramID(String value) {
                    this.programID = value;
                }

                /**
                 * Obtiene el valor de la propiedad membershipID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMembershipID() {
                    return membershipID;
                }

                /**
                 * Define el valor de la propiedad membershipID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMembershipID(String value) {
                    this.membershipID = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Gets the value of the vendorCode property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getVendorCode().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getVendorCode() {
                    if (vendorCode == null) {
                        vendorCode = new ArrayList<String>();
                    }
                    return this.vendorCode;
                }

                /**
                 * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPrimaryLoyaltyIndicator() {
                    return primaryLoyaltyIndicator;
                }

                /**
                 * Define el valor de la propiedad primaryLoyaltyIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPrimaryLoyaltyIndicator(Boolean value) {
                    this.primaryLoyaltyIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAllianceLoyaltyLevelName() {
                    return allianceLoyaltyLevelName;
                }

                /**
                 * Define el valor de la propiedad allianceLoyaltyLevelName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAllianceLoyaltyLevelName(String value) {
                    this.allianceLoyaltyLevelName = value;
                }

                /**
                 * Obtiene el valor de la propiedad customerType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerType() {
                    return customerType;
                }

                /**
                 * Define el valor de la propiedad customerType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerType(String value) {
                    this.customerType = value;
                }

                /**
                 * Obtiene el valor de la propiedad customerValue.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerValue() {
                    return customerValue;
                }

                /**
                 * Define el valor de la propiedad customerValue.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerValue(String value) {
                    this.customerValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad password.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPassword() {
                    return password;
                }

                /**
                 * Define el valor de la propiedad password.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPassword(String value) {
                    this.password = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyalLevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLoyalLevel() {
                    return loyalLevel;
                }

                /**
                 * Define el valor de la propiedad loyalLevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLoyalLevel(String value) {
                    this.loyalLevel = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyalLevelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getLoyalLevelCode() {
                    return loyalLevelCode;
                }

                /**
                 * Define el valor de la propiedad loyalLevelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setLoyalLevelCode(Integer value) {
                    this.loyalLevelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad singleVendorInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSingleVendorInd() {
                    return singleVendorInd;
                }

                /**
                 * Define el valor de la propiedad singleVendorInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSingleVendorInd(String value) {
                    this.singleVendorInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad signupDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getSignupDate() {
                    return signupDate;
                }

                /**
                 * Define el valor de la propiedad signupDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setSignupDate(XMLGregorianCalendar value) {
                    this.signupDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad effectiveDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getEffectiveDate() {
                    return effectiveDate;
                }

                /**
                 * Define el valor de la propiedad effectiveDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setEffectiveDate(XMLGregorianCalendar value) {
                    this.effectiveDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad expireDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getExpireDate() {
                    return expireDate;
                }

                /**
                 * Define el valor de la propiedad expireDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setExpireDate(XMLGregorianCalendar value) {
                    this.expireDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isExpireDateExclusiveIndicator() {
                    return expireDateExclusiveIndicator;
                }

                /**
                 * Define el valor de la propiedad expireDateExclusiveIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setExpireDateExclusiveIndicator(Boolean value) {
                    this.expireDateExclusiveIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRPH(String value) {
                    this.rph = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CityName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Airport" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="AirportName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="UserID" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *                 &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Verification" type="{http://www.opentravel.org/OTA/2003/05}VerificationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="SelectionCriteria" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="DateType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
         *                       &amp;lt;enumeration value="DepartureDate"/&amp;gt;
         *                       &amp;lt;enumeration value="CreateDate"/&amp;gt;
         *                       &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
         *                       &amp;lt;enumeration value="CancelDeadlineDate"/&amp;gt;
         *                       &amp;lt;enumeration value="PaymentDeadlineDate"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="SelectionType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="All"/&amp;gt;
         *                       &amp;lt;enumeration value="PreviouslyDelivered"/&amp;gt;
         *                       &amp;lt;enumeration value="Undelivered"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="ResStatus" type="{http://www.opentravel.org/OTA/2003/05}HotelResStatusType" /&amp;gt;
         *                 &amp;lt;attribute name="OriginalDeliveryMethodCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cityName",
            "airport",
            "userID",
            "verification",
            "selectionCriteria",
            "tpaExtensions"
        })
        public static class HotelReadRequest {

            @XmlElement(name = "CityName")
            protected String cityName;
            @XmlElement(name = "Airport")
            protected OTAReadRQ.ReadRequests.HotelReadRequest.Airport airport;
            @XmlElement(name = "UserID")
            protected OTAReadRQ.ReadRequests.HotelReadRequest.UserID userID;
            @XmlElement(name = "Verification")
            protected VerificationType verification;
            @XmlElement(name = "SelectionCriteria")
            protected List<OTAReadRQ.ReadRequests.HotelReadRequest.SelectionCriteria> selectionCriteria;
            @XmlElement(name = "TPA_Extensions")
            protected TPAExtensionsType tpaExtensions;
            @XmlAttribute(name = "ChainCode")
            protected String chainCode;
            @XmlAttribute(name = "BrandCode")
            protected String brandCode;
            @XmlAttribute(name = "HotelCode")
            protected String hotelCode;
            @XmlAttribute(name = "HotelCityCode")
            protected String hotelCityCode;
            @XmlAttribute(name = "HotelName")
            protected String hotelName;
            @XmlAttribute(name = "HotelCodeContext")
            protected String hotelCodeContext;
            @XmlAttribute(name = "ChainName")
            protected String chainName;
            @XmlAttribute(name = "BrandName")
            protected String brandName;
            @XmlAttribute(name = "AreaID")
            protected String areaID;
            @XmlAttribute(name = "TTIcode")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger ttIcode;

            /**
             * Obtiene el valor de la propiedad cityName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityName() {
                return cityName;
            }

            /**
             * Define el valor de la propiedad cityName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityName(String value) {
                this.cityName = value;
            }

            /**
             * Obtiene el valor de la propiedad airport.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.HotelReadRequest.Airport }
             *     
             */
            public OTAReadRQ.ReadRequests.HotelReadRequest.Airport getAirport() {
                return airport;
            }

            /**
             * Define el valor de la propiedad airport.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.HotelReadRequest.Airport }
             *     
             */
            public void setAirport(OTAReadRQ.ReadRequests.HotelReadRequest.Airport value) {
                this.airport = value;
            }

            /**
             * Obtiene el valor de la propiedad userID.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.HotelReadRequest.UserID }
             *     
             */
            public OTAReadRQ.ReadRequests.HotelReadRequest.UserID getUserID() {
                return userID;
            }

            /**
             * Define el valor de la propiedad userID.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.HotelReadRequest.UserID }
             *     
             */
            public void setUserID(OTAReadRQ.ReadRequests.HotelReadRequest.UserID value) {
                this.userID = value;
            }

            /**
             * Obtiene el valor de la propiedad verification.
             * 
             * @return
             *     possible object is
             *     {@link VerificationType }
             *     
             */
            public VerificationType getVerification() {
                return verification;
            }

            /**
             * Define el valor de la propiedad verification.
             * 
             * @param value
             *     allowed object is
             *     {@link VerificationType }
             *     
             */
            public void setVerification(VerificationType value) {
                this.verification = value;
            }

            /**
             * Gets the value of the selectionCriteria property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectionCriteria property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getSelectionCriteria().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAReadRQ.ReadRequests.HotelReadRequest.SelectionCriteria }
             * 
             * 
             */
            public List<OTAReadRQ.ReadRequests.HotelReadRequest.SelectionCriteria> getSelectionCriteria() {
                if (selectionCriteria == null) {
                    selectionCriteria = new ArrayList<OTAReadRQ.ReadRequests.HotelReadRequest.SelectionCriteria>();
                }
                return this.selectionCriteria;
            }

            /**
             * Obtiene el valor de la propiedad tpaExtensions.
             * 
             * @return
             *     possible object is
             *     {@link TPAExtensionsType }
             *     
             */
            public TPAExtensionsType getTPAExtensions() {
                return tpaExtensions;
            }

            /**
             * Define el valor de la propiedad tpaExtensions.
             * 
             * @param value
             *     allowed object is
             *     {@link TPAExtensionsType }
             *     
             */
            public void setTPAExtensions(TPAExtensionsType value) {
                this.tpaExtensions = value;
            }

            /**
             * Obtiene el valor de la propiedad chainCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainCode() {
                return chainCode;
            }

            /**
             * Define el valor de la propiedad chainCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainCode(String value) {
                this.chainCode = value;
            }

            /**
             * Obtiene el valor de la propiedad brandCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandCode() {
                return brandCode;
            }

            /**
             * Define el valor de la propiedad brandCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandCode(String value) {
                this.brandCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCode() {
                return hotelCode;
            }

            /**
             * Define el valor de la propiedad hotelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCode(String value) {
                this.hotelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCityCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCityCode() {
                return hotelCityCode;
            }

            /**
             * Define el valor de la propiedad hotelCityCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCityCode(String value) {
                this.hotelCityCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelName() {
                return hotelName;
            }

            /**
             * Define el valor de la propiedad hotelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelName(String value) {
                this.hotelName = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCodeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCodeContext() {
                return hotelCodeContext;
            }

            /**
             * Define el valor de la propiedad hotelCodeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCodeContext(String value) {
                this.hotelCodeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad chainName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainName() {
                return chainName;
            }

            /**
             * Define el valor de la propiedad chainName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainName(String value) {
                this.chainName = value;
            }

            /**
             * Obtiene el valor de la propiedad brandName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandName() {
                return brandName;
            }

            /**
             * Define el valor de la propiedad brandName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandName(String value) {
                this.brandName = value;
            }

            /**
             * Obtiene el valor de la propiedad areaID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAreaID() {
                return areaID;
            }

            /**
             * Define el valor de la propiedad areaID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAreaID(String value) {
                this.areaID = value;
            }

            /**
             * Obtiene el valor de la propiedad ttIcode.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTTIcode() {
                return ttIcode;
            }

            /**
             * Define el valor de la propiedad ttIcode.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTTIcode(BigInteger value) {
                this.ttIcode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="AirportName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Airport {

                @XmlAttribute(name = "LocationCode")
                protected String locationCode;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "AirportName")
                protected String airportName;

                /**
                 * Obtiene el valor de la propiedad locationCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocationCode() {
                    return locationCode;
                }

                /**
                 * Define el valor de la propiedad locationCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocationCode(String value) {
                    this.locationCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad airportName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAirportName() {
                    return airportName;
                }

                /**
                 * Define el valor de la propiedad airportName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAirportName(String value) {
                    this.airportName = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="DateType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
             *             &amp;lt;enumeration value="DepartureDate"/&amp;gt;
             *             &amp;lt;enumeration value="CreateDate"/&amp;gt;
             *             &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
             *             &amp;lt;enumeration value="CancelDeadlineDate"/&amp;gt;
             *             &amp;lt;enumeration value="PaymentDeadlineDate"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="SelectionType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="All"/&amp;gt;
             *             &amp;lt;enumeration value="PreviouslyDelivered"/&amp;gt;
             *             &amp;lt;enumeration value="Undelivered"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="GroupCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="ResStatus" type="{http://www.opentravel.org/OTA/2003/05}HotelResStatusType" /&amp;gt;
             *       &amp;lt;attribute name="OriginalDeliveryMethodCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class SelectionCriteria {

                @XmlAttribute(name = "DateType")
                protected String dateType;
                @XmlAttribute(name = "SelectionType")
                protected String selectionType;
                @XmlAttribute(name = "GroupCode")
                protected String groupCode;
                @XmlAttribute(name = "ResStatus")
                protected String resStatus;
                @XmlAttribute(name = "OriginalDeliveryMethodCode")
                protected String originalDeliveryMethodCode;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad dateType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDateType() {
                    return dateType;
                }

                /**
                 * Define el valor de la propiedad dateType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDateType(String value) {
                    this.dateType = value;
                }

                /**
                 * Obtiene el valor de la propiedad selectionType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSelectionType() {
                    return selectionType;
                }

                /**
                 * Define el valor de la propiedad selectionType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSelectionType(String value) {
                    this.selectionType = value;
                }

                /**
                 * Obtiene el valor de la propiedad groupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGroupCode() {
                    return groupCode;
                }

                /**
                 * Define el valor de la propiedad groupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGroupCode(String value) {
                    this.groupCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad resStatus.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResStatus() {
                    return resStatus;
                }

                /**
                 * Define el valor de la propiedad resStatus.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResStatus(String value) {
                    this.resStatus = value;
                }

                /**
                 * Obtiene el valor de la propiedad originalDeliveryMethodCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginalDeliveryMethodCode() {
                    return originalDeliveryMethodCode;
                }

                /**
                 * Define el valor de la propiedad originalDeliveryMethodCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginalDeliveryMethodCode(String value) {
                    this.originalDeliveryMethodCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
             *       &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class UserID
                extends UniqueIDType
            {

                @XmlAttribute(name = "PinNumber")
                protected String pinNumber;

                /**
                 * Obtiene el valor de la propiedad pinNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPinNumber() {
                    return pinNumber;
                }

                /**
                 * Define el valor de la propiedad pinNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPinNumber(String value) {
                    this.pinNumber = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="MembershipID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID",
            "custLoyalty"
        })
        public static class LoyaltyAccountReadRequest {

            @XmlElement(name = "UniqueID")
            protected UniqueIDType uniqueID;
            @XmlElement(name = "CustLoyalty")
            protected OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest.CustLoyalty custLoyalty;

            /**
             * Obtiene el valor de la propiedad uniqueID.
             * 
             * @return
             *     possible object is
             *     {@link UniqueIDType }
             *     
             */
            public UniqueIDType getUniqueID() {
                return uniqueID;
            }

            /**
             * Define el valor de la propiedad uniqueID.
             * 
             * @param value
             *     allowed object is
             *     {@link UniqueIDType }
             *     
             */
            public void setUniqueID(UniqueIDType value) {
                this.uniqueID = value;
            }

            /**
             * Obtiene el valor de la propiedad custLoyalty.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest.CustLoyalty }
             *     
             */
            public OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest.CustLoyalty getCustLoyalty() {
                return custLoyalty;
            }

            /**
             * Define el valor de la propiedad custLoyalty.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest.CustLoyalty }
             *     
             */
            public void setCustLoyalty(OTAReadRQ.ReadRequests.LoyaltyAccountReadRequest.CustLoyalty value) {
                this.custLoyalty = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="MembershipID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class CustLoyalty {

                @XmlAttribute(name = "ProgramID")
                protected String programID;
                @XmlAttribute(name = "MembershipID")
                protected String membershipID;

                /**
                 * Obtiene el valor de la propiedad programID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramID() {
                    return programID;
                }

                /**
                 * Define el valor de la propiedad programID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramID(String value) {
                    this.programID = value;
                }

                /**
                 * Obtiene el valor de la propiedad membershipID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMembershipID() {
                    return membershipID;
                }

                /**
                 * Define el valor de la propiedad membershipID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMembershipID(String value) {
                    this.membershipID = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="ArrivalLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="DepartureLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
         *       &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}PackageID_RefType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "arrivalLocation",
            "departureLocation"
        })
        public static class PkgReadRequest {

            @XmlElement(name = "Name")
            protected PersonNameType name;
            @XmlElement(name = "ArrivalLocation")
            protected LocationType arrivalLocation;
            @XmlElement(name = "DepartureLocation")
            protected LocationType departureLocation;
            @XmlAttribute(name = "TravelCode")
            protected String travelCode;
            @XmlAttribute(name = "TourCode")
            protected String tourCode;
            @XmlAttribute(name = "PackageID")
            protected String packageID;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link PersonNameType }
             *     
             */
            public PersonNameType getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link PersonNameType }
             *     
             */
            public void setName(PersonNameType value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getArrivalLocation() {
                return arrivalLocation;
            }

            /**
             * Define el valor de la propiedad arrivalLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setArrivalLocation(LocationType value) {
                this.arrivalLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad departureLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDepartureLocation() {
                return departureLocation;
            }

            /**
             * Define el valor de la propiedad departureLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDepartureLocation(LocationType value) {
                this.departureLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad travelCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelCode() {
                return travelCode;
            }

            /**
             * Define el valor de la propiedad travelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelCode(String value) {
                this.travelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad tourCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTourCode() {
                return tourCode;
            }

            /**
             * Define el valor de la propiedad tourCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTourCode(String value) {
                this.tourCode = value;
            }

            /**
             * Obtiene el valor de la propiedad packageID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPackageID() {
                return packageID;
            }

            /**
             * Define el valor de la propiedad packageID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPackageID(String value) {
                this.packageID = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" maxOccurs="9" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *                 &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Company" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Customer" type="{http://www.opentravel.org/OTA/2003/05}CustomerType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="DateType"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="ArrivalDate"/&amp;gt;
         *             &amp;lt;enumeration value="DepartureDate"/&amp;gt;
         *             &amp;lt;enumeration value="CreateDate"/&amp;gt;
         *             &amp;lt;enumeration value="LastUpdateDate"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="StatusCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="ProfileTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID",
            "company",
            "customer"
        })
        public static class ProfileReadRequest {

            @XmlElement(name = "UniqueID")
            protected List<OTAReadRQ.ReadRequests.ProfileReadRequest.UniqueID> uniqueID;
            @XmlElement(name = "Company")
            protected ContactPersonType company;
            @XmlElement(name = "Customer")
            protected CustomerType customer;
            @XmlAttribute(name = "DateType")
            protected String dateType;
            @XmlAttribute(name = "StatusCode")
            protected String statusCode;
            @XmlAttribute(name = "ProfileTypeCode")
            protected String profileTypeCode;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Gets the value of the uniqueID property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the uniqueID property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getUniqueID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAReadRQ.ReadRequests.ProfileReadRequest.UniqueID }
             * 
             * 
             */
            public List<OTAReadRQ.ReadRequests.ProfileReadRequest.UniqueID> getUniqueID() {
                if (uniqueID == null) {
                    uniqueID = new ArrayList<OTAReadRQ.ReadRequests.ProfileReadRequest.UniqueID>();
                }
                return this.uniqueID;
            }

            /**
             * Obtiene el valor de la propiedad company.
             * 
             * @return
             *     possible object is
             *     {@link ContactPersonType }
             *     
             */
            public ContactPersonType getCompany() {
                return company;
            }

            /**
             * Define el valor de la propiedad company.
             * 
             * @param value
             *     allowed object is
             *     {@link ContactPersonType }
             *     
             */
            public void setCompany(ContactPersonType value) {
                this.company = value;
            }

            /**
             * Obtiene el valor de la propiedad customer.
             * 
             * @return
             *     possible object is
             *     {@link CustomerType }
             *     
             */
            public CustomerType getCustomer() {
                return customer;
            }

            /**
             * Define el valor de la propiedad customer.
             * 
             * @param value
             *     allowed object is
             *     {@link CustomerType }
             *     
             */
            public void setCustomer(CustomerType value) {
                this.customer = value;
            }

            /**
             * Obtiene el valor de la propiedad dateType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateType() {
                return dateType;
            }

            /**
             * Define el valor de la propiedad dateType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateType(String value) {
                this.dateType = value;
            }

            /**
             * Obtiene el valor de la propiedad statusCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusCode() {
                return statusCode;
            }

            /**
             * Define el valor de la propiedad statusCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusCode(String value) {
                this.statusCode = value;
            }

            /**
             * Obtiene el valor de la propiedad profileTypeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProfileTypeCode() {
                return profileTypeCode;
            }

            /**
             * Define el valor de la propiedad profileTypeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProfileTypeCode(String value) {
                this.profileTypeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
             *       &amp;lt;attribute name="PinNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class UniqueID
                extends UniqueIDType
            {

                @XmlAttribute(name = "PinNumber")
                protected String pinNumber;

                /**
                 * Obtiene el valor de la propiedad pinNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPinNumber() {
                    return pinNumber;
                }

                /**
                 * Define el valor de la propiedad pinNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPinNumber(String value) {
                    this.pinNumber = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Train" type="{http://www.opentravel.org/OTA/2003/05}TrainQueryType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Traveler" type="{http://www.opentravel.org/OTA/2003/05}PersonNameType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="BookingDateTime" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="DepartureDateTime" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pos",
            "train",
            "traveler",
            "bookingDateTime",
            "departureDateTime"
        })
        public static class RailReadRequest {

            @XmlElement(name = "POS")
            protected POSType pos;
            @XmlElement(name = "Train")
            protected TrainQueryType train;
            @XmlElement(name = "Traveler")
            protected PersonNameType traveler;
            @XmlElement(name = "BookingDateTime")
            protected OTAReadRQ.ReadRequests.RailReadRequest.BookingDateTime bookingDateTime;
            @XmlElement(name = "DepartureDateTime")
            protected OTAReadRQ.ReadRequests.RailReadRequest.DepartureDateTime departureDateTime;

            /**
             * Obtiene el valor de la propiedad pos.
             * 
             * @return
             *     possible object is
             *     {@link POSType }
             *     
             */
            public POSType getPOS() {
                return pos;
            }

            /**
             * Define el valor de la propiedad pos.
             * 
             * @param value
             *     allowed object is
             *     {@link POSType }
             *     
             */
            public void setPOS(POSType value) {
                this.pos = value;
            }

            /**
             * Obtiene el valor de la propiedad train.
             * 
             * @return
             *     possible object is
             *     {@link TrainQueryType }
             *     
             */
            public TrainQueryType getTrain() {
                return train;
            }

            /**
             * Define el valor de la propiedad train.
             * 
             * @param value
             *     allowed object is
             *     {@link TrainQueryType }
             *     
             */
            public void setTrain(TrainQueryType value) {
                this.train = value;
            }

            /**
             * Obtiene el valor de la propiedad traveler.
             * 
             * @return
             *     possible object is
             *     {@link PersonNameType }
             *     
             */
            public PersonNameType getTraveler() {
                return traveler;
            }

            /**
             * Define el valor de la propiedad traveler.
             * 
             * @param value
             *     allowed object is
             *     {@link PersonNameType }
             *     
             */
            public void setTraveler(PersonNameType value) {
                this.traveler = value;
            }

            /**
             * Obtiene el valor de la propiedad bookingDateTime.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.RailReadRequest.BookingDateTime }
             *     
             */
            public OTAReadRQ.ReadRequests.RailReadRequest.BookingDateTime getBookingDateTime() {
                return bookingDateTime;
            }

            /**
             * Define el valor de la propiedad bookingDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.RailReadRequest.BookingDateTime }
             *     
             */
            public void setBookingDateTime(OTAReadRQ.ReadRequests.RailReadRequest.BookingDateTime value) {
                this.bookingDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDateTime.
             * 
             * @return
             *     possible object is
             *     {@link OTAReadRQ.ReadRequests.RailReadRequest.DepartureDateTime }
             *     
             */
            public OTAReadRQ.ReadRequests.RailReadRequest.DepartureDateTime getDepartureDateTime() {
                return departureDateTime;
            }

            /**
             * Define el valor de la propiedad departureDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAReadRQ.ReadRequests.RailReadRequest.DepartureDateTime }
             *     
             */
            public void setDepartureDateTime(OTAReadRQ.ReadRequests.RailReadRequest.DepartureDateTime value) {
                this.departureDateTime = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class BookingDateTime {

                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DepartureDateTime {

                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
         *         &amp;lt;element name="Verification" type="{http://www.opentravel.org/OTA/2003/05}VerificationType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="HistoryRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID",
            "verification"
        })
        public static class ReadRequest {

            @XmlElement(name = "UniqueID", required = true)
            protected UniqueIDType uniqueID;
            @XmlElement(name = "Verification")
            protected VerificationType verification;
            @XmlAttribute(name = "HistoryRequestedInd")
            protected Boolean historyRequestedInd;

            /**
             * Obtiene el valor de la propiedad uniqueID.
             * 
             * @return
             *     possible object is
             *     {@link UniqueIDType }
             *     
             */
            public UniqueIDType getUniqueID() {
                return uniqueID;
            }

            /**
             * Define el valor de la propiedad uniqueID.
             * 
             * @param value
             *     allowed object is
             *     {@link UniqueIDType }
             *     
             */
            public void setUniqueID(UniqueIDType value) {
                this.uniqueID = value;
            }

            /**
             * Obtiene el valor de la propiedad verification.
             * 
             * @return
             *     possible object is
             *     {@link VerificationType }
             *     
             */
            public VerificationType getVerification() {
                return verification;
            }

            /**
             * Define el valor de la propiedad verification.
             * 
             * @param value
             *     allowed object is
             *     {@link VerificationType }
             *     
             */
            public void setVerification(VerificationType value) {
                this.verification = value;
            }

            /**
             * Obtiene el valor de la propiedad historyRequestedInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isHistoryRequestedInd() {
                return historyRequestedInd;
            }

            /**
             * Define el valor de la propiedad historyRequestedInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setHistoryRequestedInd(Boolean value) {
                this.historyRequestedInd = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleRetrieveResRQCoreType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="VehRetResRQInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleRetrieveResRQAdditionalInfoType"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "vehRetResRQInfo"
        })
        public static class VehicleReadRequest
            extends VehicleRetrieveResRQCoreType
        {

            @XmlElement(name = "VehRetResRQInfo", required = true)
            protected VehicleRetrieveResRQAdditionalInfoType vehRetResRQInfo;

            /**
             * Obtiene el valor de la propiedad vehRetResRQInfo.
             * 
             * @return
             *     possible object is
             *     {@link VehicleRetrieveResRQAdditionalInfoType }
             *     
             */
            public VehicleRetrieveResRQAdditionalInfoType getVehRetResRQInfo() {
                return vehRetResRQInfo;
            }

            /**
             * Define el valor de la propiedad vehRetResRQInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleRetrieveResRQAdditionalInfoType }
             *     
             */
            public void setVehRetResRQInfo(VehicleRetrieveResRQAdditionalInfoType value) {
                this.vehRetResRQInfo = value;
            }

        }

    }

}
