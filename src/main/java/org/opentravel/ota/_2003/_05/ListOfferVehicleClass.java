
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferVehicleClass.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferVehicleClass"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Compact/Economy"/&amp;gt;
 *     &amp;lt;enumeration value="Exotic"/&amp;gt;
 *     &amp;lt;enumeration value="Fullsize"/&amp;gt;
 *     &amp;lt;enumeration value="FuelEfficient"/&amp;gt;
 *     &amp;lt;enumeration value="Intermediate/Midsize"/&amp;gt;
 *     &amp;lt;enumeration value="LargeSUV"/&amp;gt;
 *     &amp;lt;enumeration value="Luxury/Premium"/&amp;gt;
 *     &amp;lt;enumeration value="Minivan"/&amp;gt;
 *     &amp;lt;enumeration value="Standard"/&amp;gt;
 *     &amp;lt;enumeration value="Stretch"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferVehicleClass")
@XmlEnum
public enum ListOfferVehicleClass {

    @XmlEnumValue("Compact/Economy")
    COMPACT_ECONOMY("Compact/Economy"),
    @XmlEnumValue("Exotic")
    EXOTIC("Exotic"),
    @XmlEnumValue("Fullsize")
    FULLSIZE("Fullsize"),
    @XmlEnumValue("FuelEfficient")
    FUEL_EFFICIENT("FuelEfficient"),
    @XmlEnumValue("Intermediate/Midsize")
    INTERMEDIATE_MIDSIZE("Intermediate/Midsize"),
    @XmlEnumValue("LargeSUV")
    LARGE_SUV("LargeSUV"),
    @XmlEnumValue("Luxury/Premium")
    LUXURY_PREMIUM("Luxury/Premium"),
    @XmlEnumValue("Minivan")
    MINIVAN("Minivan"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("Stretch")
    STRETCH("Stretch"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferVehicleClass(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferVehicleClass fromValue(String v) {
        for (ListOfferVehicleClass c: ListOfferVehicleClass.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
