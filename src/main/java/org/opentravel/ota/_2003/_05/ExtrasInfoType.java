
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Describes an optional service which is not included in the standard package but may be booked in addition.
 * 
 * &lt;p&gt;Clase Java para ExtrasInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ExtrasInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ExtrasCoreType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Criteria" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="MinimumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="YearsExperience" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="DateRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DurationRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="StockControlledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumOccupancy" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Periods" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Period" type="{http://www.opentravel.org/OTA/2003/05}PeriodPriceType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AdditionalInfoPrompt" maxOccurs="4" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="AdditionalInfoText" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;attribute name="LineNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ParentExtras" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExtrasSelectionGroup"/&amp;gt;
 *                 &amp;lt;attribute name="ListOfParentRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ExtraLocationInfo" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Location" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="Type"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="PickUp"/&amp;gt;
 *                       &amp;lt;enumeration value="DropOff"/&amp;gt;
 *                       &amp;lt;enumeration value="Both"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExtrasSelectionGroup"/&amp;gt;
 *       &amp;lt;attribute name="ApplyTo"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="SelectedPax"/&amp;gt;
 *             &amp;lt;enumeration value="AllPax"/&amp;gt;
 *             &amp;lt;enumeration value="NoPax"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtrasInfoType", propOrder = {
    "criteria",
    "periods",
    "additionalInfoPrompt",
    "parentExtras",
    "extraLocationInfo"
})
public class ExtrasInfoType
    extends ExtrasCoreType
{

    @XmlElement(name = "Criteria")
    protected ExtrasInfoType.Criteria criteria;
    @XmlElement(name = "Periods")
    protected ExtrasInfoType.Periods periods;
    @XmlElement(name = "AdditionalInfoPrompt")
    protected List<ExtrasInfoType.AdditionalInfoPrompt> additionalInfoPrompt;
    @XmlElement(name = "ParentExtras")
    protected ExtrasInfoType.ParentExtras parentExtras;
    @XmlElement(name = "ExtraLocationInfo")
    protected List<ExtrasInfoType.ExtraLocationInfo> extraLocationInfo;
    @XmlAttribute(name = "ApplyTo")
    protected String applyTo;
    @XmlAttribute(name = "SelectionType")
    protected String selectionType;
    @XmlAttribute(name = "RuleCode")
    protected String ruleCode;

    /**
     * Obtiene el valor de la propiedad criteria.
     * 
     * @return
     *     possible object is
     *     {@link ExtrasInfoType.Criteria }
     *     
     */
    public ExtrasInfoType.Criteria getCriteria() {
        return criteria;
    }

    /**
     * Define el valor de la propiedad criteria.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtrasInfoType.Criteria }
     *     
     */
    public void setCriteria(ExtrasInfoType.Criteria value) {
        this.criteria = value;
    }

    /**
     * Obtiene el valor de la propiedad periods.
     * 
     * @return
     *     possible object is
     *     {@link ExtrasInfoType.Periods }
     *     
     */
    public ExtrasInfoType.Periods getPeriods() {
        return periods;
    }

    /**
     * Define el valor de la propiedad periods.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtrasInfoType.Periods }
     *     
     */
    public void setPeriods(ExtrasInfoType.Periods value) {
        this.periods = value;
    }

    /**
     * Gets the value of the additionalInfoPrompt property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additionalInfoPrompt property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAdditionalInfoPrompt().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ExtrasInfoType.AdditionalInfoPrompt }
     * 
     * 
     */
    public List<ExtrasInfoType.AdditionalInfoPrompt> getAdditionalInfoPrompt() {
        if (additionalInfoPrompt == null) {
            additionalInfoPrompt = new ArrayList<ExtrasInfoType.AdditionalInfoPrompt>();
        }
        return this.additionalInfoPrompt;
    }

    /**
     * Obtiene el valor de la propiedad parentExtras.
     * 
     * @return
     *     possible object is
     *     {@link ExtrasInfoType.ParentExtras }
     *     
     */
    public ExtrasInfoType.ParentExtras getParentExtras() {
        return parentExtras;
    }

    /**
     * Define el valor de la propiedad parentExtras.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtrasInfoType.ParentExtras }
     *     
     */
    public void setParentExtras(ExtrasInfoType.ParentExtras value) {
        this.parentExtras = value;
    }

    /**
     * Gets the value of the extraLocationInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the extraLocationInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getExtraLocationInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ExtrasInfoType.ExtraLocationInfo }
     * 
     * 
     */
    public List<ExtrasInfoType.ExtraLocationInfo> getExtraLocationInfo() {
        if (extraLocationInfo == null) {
            extraLocationInfo = new ArrayList<ExtrasInfoType.ExtraLocationInfo>();
        }
        return this.extraLocationInfo;
    }

    /**
     * Obtiene el valor de la propiedad applyTo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyTo() {
        return applyTo;
    }

    /**
     * Define el valor de la propiedad applyTo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyTo(String value) {
        this.applyTo = value;
    }

    /**
     * Obtiene el valor de la propiedad selectionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelectionType() {
        return selectionType;
    }

    /**
     * Define el valor de la propiedad selectionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelectionType(String value) {
        this.selectionType = value;
    }

    /**
     * Obtiene el valor de la propiedad ruleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleCode() {
        return ruleCode;
    }

    /**
     * Define el valor de la propiedad ruleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleCode(String value) {
        this.ruleCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="AdditionalInfoText" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="LineNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AdditionalInfoPrompt {

        @XmlAttribute(name = "AdditionalInfoText")
        protected String additionalInfoText;
        @XmlAttribute(name = "LineNumber")
        protected Integer lineNumber;

        /**
         * Obtiene el valor de la propiedad additionalInfoText.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdditionalInfoText() {
            return additionalInfoText;
        }

        /**
         * Define el valor de la propiedad additionalInfoText.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdditionalInfoText(String value) {
            this.additionalInfoText = value;
        }

        /**
         * Obtiene el valor de la propiedad lineNumber.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getLineNumber() {
            return lineNumber;
        }

        /**
         * Define el valor de la propiedad lineNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setLineNumber(Integer value) {
            this.lineNumber = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="MinimumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaximumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="YearsExperience" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="DateRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DurationRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="StockControlledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="MaximumOccupancy" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Criteria {

        @XmlAttribute(name = "MinimumAge")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger minimumAge;
        @XmlAttribute(name = "MaximumAge")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maximumAge;
        @XmlAttribute(name = "YearsExperience")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger yearsExperience;
        @XmlAttribute(name = "DateRequiredInd")
        protected Boolean dateRequiredInd;
        @XmlAttribute(name = "DurationRequiredInd")
        protected Boolean durationRequiredInd;
        @XmlAttribute(name = "StockControlledInd")
        protected Boolean stockControlledInd;
        @XmlAttribute(name = "MaximumOccupancy")
        protected Integer maximumOccupancy;

        /**
         * Obtiene el valor de la propiedad minimumAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMinimumAge() {
            return minimumAge;
        }

        /**
         * Define el valor de la propiedad minimumAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMinimumAge(BigInteger value) {
            this.minimumAge = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaximumAge() {
            return maximumAge;
        }

        /**
         * Define el valor de la propiedad maximumAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaximumAge(BigInteger value) {
            this.maximumAge = value;
        }

        /**
         * Obtiene el valor de la propiedad yearsExperience.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getYearsExperience() {
            return yearsExperience;
        }

        /**
         * Define el valor de la propiedad yearsExperience.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setYearsExperience(BigInteger value) {
            this.yearsExperience = value;
        }

        /**
         * Obtiene el valor de la propiedad dateRequiredInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDateRequiredInd() {
            return dateRequiredInd;
        }

        /**
         * Define el valor de la propiedad dateRequiredInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDateRequiredInd(Boolean value) {
            this.dateRequiredInd = value;
        }

        /**
         * Obtiene el valor de la propiedad durationRequiredInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDurationRequiredInd() {
            return durationRequiredInd;
        }

        /**
         * Define el valor de la propiedad durationRequiredInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDurationRequiredInd(Boolean value) {
            this.durationRequiredInd = value;
        }

        /**
         * Obtiene el valor de la propiedad stockControlledInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isStockControlledInd() {
            return stockControlledInd;
        }

        /**
         * Define el valor de la propiedad stockControlledInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setStockControlledInd(Boolean value) {
            this.stockControlledInd = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumOccupancy.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaximumOccupancy() {
            return maximumOccupancy;
        }

        /**
         * Define el valor de la propiedad maximumOccupancy.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaximumOccupancy(Integer value) {
            this.maximumOccupancy = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Location" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="Type"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="PickUp"/&amp;gt;
     *             &amp;lt;enumeration value="DropOff"/&amp;gt;
     *             &amp;lt;enumeration value="Both"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ExtraLocationInfo {

        @XmlAttribute(name = "Location")
        protected String location;
        @XmlAttribute(name = "Type")
        protected String type;

        /**
         * Obtiene el valor de la propiedad location.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocation() {
            return location;
        }

        /**
         * Define el valor de la propiedad location.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocation(String value) {
            this.location = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExtrasSelectionGroup"/&amp;gt;
     *       &amp;lt;attribute name="ListOfParentRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ParentExtras {

        @XmlAttribute(name = "ListOfParentRPH")
        protected List<String> listOfParentRPH;
        @XmlAttribute(name = "SelectionType")
        protected String selectionType;
        @XmlAttribute(name = "RuleCode")
        protected String ruleCode;

        /**
         * Gets the value of the listOfParentRPH property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the listOfParentRPH property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getListOfParentRPH().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getListOfParentRPH() {
            if (listOfParentRPH == null) {
                listOfParentRPH = new ArrayList<String>();
            }
            return this.listOfParentRPH;
        }

        /**
         * Obtiene el valor de la propiedad selectionType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSelectionType() {
            return selectionType;
        }

        /**
         * Define el valor de la propiedad selectionType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSelectionType(String value) {
            this.selectionType = value;
        }

        /**
         * Obtiene el valor de la propiedad ruleCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRuleCode() {
            return ruleCode;
        }

        /**
         * Define el valor de la propiedad ruleCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRuleCode(String value) {
            this.ruleCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Period" type="{http://www.opentravel.org/OTA/2003/05}PeriodPriceType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "period"
    })
    public static class Periods {

        @XmlElement(name = "Period", required = true)
        protected List<PeriodPriceType> period;

        /**
         * Gets the value of the period property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the period property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPeriod().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PeriodPriceType }
         * 
         * 
         */
        public List<PeriodPriceType> getPeriod() {
            if (period == null) {
                period = new ArrayList<PeriodPriceType>();
            }
            return this.period;
        }

    }

}
