
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Summary information about a cabin class.
 * 
 * &lt;p&gt;Clase Java para CabinClassSummaryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CabinClassSummaryType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;CabinType"&amp;gt;
 *       &amp;lt;attribute name="Name" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="CabinCapacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="RowQty" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to3" /&amp;gt;
 *       &amp;lt;attribute name="StartingRow" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to3" /&amp;gt;
 *       &amp;lt;attribute name="EndingRow" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to3" /&amp;gt;
 *       &amp;lt;attribute name="AvailableSeatQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CabinClassSummaryType", propOrder = {
    "value"
})
public class CabinClassSummaryType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Name", required = true)
    protected String name;
    @XmlAttribute(name = "CabinCapacity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger cabinCapacity;
    @XmlAttribute(name = "RowQty")
    protected Integer rowQty;
    @XmlAttribute(name = "StartingRow")
    protected Integer startingRow;
    @XmlAttribute(name = "EndingRow")
    protected Integer endingRow;
    @XmlAttribute(name = "AvailableSeatQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger availableSeatQty;
    @XmlAttribute(name = "RPH")
    protected String rph;

    /**
     * Specifies the cabin type (e.g. first, business, economy).
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad cabinCapacity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCabinCapacity() {
        return cabinCapacity;
    }

    /**
     * Define el valor de la propiedad cabinCapacity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCabinCapacity(BigInteger value) {
        this.cabinCapacity = value;
    }

    /**
     * Obtiene el valor de la propiedad rowQty.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRowQty() {
        return rowQty;
    }

    /**
     * Define el valor de la propiedad rowQty.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRowQty(Integer value) {
        this.rowQty = value;
    }

    /**
     * Obtiene el valor de la propiedad startingRow.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStartingRow() {
        return startingRow;
    }

    /**
     * Define el valor de la propiedad startingRow.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStartingRow(Integer value) {
        this.startingRow = value;
    }

    /**
     * Obtiene el valor de la propiedad endingRow.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEndingRow() {
        return endingRow;
    }

    /**
     * Define el valor de la propiedad endingRow.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEndingRow(Integer value) {
        this.endingRow = value;
    }

    /**
     * Obtiene el valor de la propiedad availableSeatQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAvailableSeatQty() {
        return availableSeatQty;
    }

    /**
     * Define el valor de la propiedad availableSeatQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAvailableSeatQty(BigInteger value) {
        this.availableSeatQty = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

}
