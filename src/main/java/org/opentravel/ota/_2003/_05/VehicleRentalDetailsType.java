
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * This provides specific information regarding the milage and condition of the vehicle being rented.
 * 
 * &lt;p&gt;Clase Java para VehicleRentalDetailsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleRentalDetailsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FuelLevelDetails" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                 &amp;lt;attribute name="FuelLevelValue"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="8"/&amp;gt;
 *                       &amp;lt;enumeration value="7"/&amp;gt;
 *                       &amp;lt;enumeration value="6"/&amp;gt;
 *                       &amp;lt;enumeration value="5"/&amp;gt;
 *                       &amp;lt;enumeration value="4"/&amp;gt;
 *                       &amp;lt;enumeration value="3"/&amp;gt;
 *                       &amp;lt;enumeration value="2"/&amp;gt;
 *                       &amp;lt;enumeration value="1"/&amp;gt;
 *                       &amp;lt;enumeration value="0"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OdometerReading" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ConditionReport" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
 *                 &amp;lt;attribute name="Condition"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Damage"/&amp;gt;
 *                       &amp;lt;enumeration value="OK"/&amp;gt;
 *                       &amp;lt;enumeration value="Unknown"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="ParkingLocation" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleRentalDetailsType", propOrder = {
    "fuelLevelDetails",
    "odometerReading",
    "conditionReport"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAVehExchangeRS.Vehicle.VehRentalDetails.class
})
public class VehicleRentalDetailsType {

    @XmlElement(name = "FuelLevelDetails")
    protected VehicleRentalDetailsType.FuelLevelDetails fuelLevelDetails;
    @XmlElement(name = "OdometerReading")
    protected VehicleRentalDetailsType.OdometerReading odometerReading;
    @XmlElement(name = "ConditionReport")
    protected List<VehicleRentalDetailsType.ConditionReport> conditionReport;
    @XmlAttribute(name = "ParkingLocation")
    protected String parkingLocation;

    /**
     * Obtiene el valor de la propiedad fuelLevelDetails.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRentalDetailsType.FuelLevelDetails }
     *     
     */
    public VehicleRentalDetailsType.FuelLevelDetails getFuelLevelDetails() {
        return fuelLevelDetails;
    }

    /**
     * Define el valor de la propiedad fuelLevelDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRentalDetailsType.FuelLevelDetails }
     *     
     */
    public void setFuelLevelDetails(VehicleRentalDetailsType.FuelLevelDetails value) {
        this.fuelLevelDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad odometerReading.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRentalDetailsType.OdometerReading }
     *     
     */
    public VehicleRentalDetailsType.OdometerReading getOdometerReading() {
        return odometerReading;
    }

    /**
     * Define el valor de la propiedad odometerReading.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRentalDetailsType.OdometerReading }
     *     
     */
    public void setOdometerReading(VehicleRentalDetailsType.OdometerReading value) {
        this.odometerReading = value;
    }

    /**
     * Gets the value of the conditionReport property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the conditionReport property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getConditionReport().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleRentalDetailsType.ConditionReport }
     * 
     * 
     */
    public List<VehicleRentalDetailsType.ConditionReport> getConditionReport() {
        if (conditionReport == null) {
            conditionReport = new ArrayList<VehicleRentalDetailsType.ConditionReport>();
        }
        return this.conditionReport;
    }

    /**
     * Obtiene el valor de la propiedad parkingLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingLocation() {
        return parkingLocation;
    }

    /**
     * Define el valor de la propiedad parkingLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingLocation(String value) {
        this.parkingLocation = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
     *       &amp;lt;attribute name="Condition"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Damage"/&amp;gt;
     *             &amp;lt;enumeration value="OK"/&amp;gt;
     *             &amp;lt;enumeration value="Unknown"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ConditionReport
        extends FormattedTextTextType
    {

        @XmlAttribute(name = "Condition")
        protected String condition;

        /**
         * Obtiene el valor de la propiedad condition.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCondition() {
            return condition;
        }

        /**
         * Define el valor de la propiedad condition.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCondition(String value) {
            this.condition = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *       &amp;lt;attribute name="FuelLevelValue"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="8"/&amp;gt;
     *             &amp;lt;enumeration value="7"/&amp;gt;
     *             &amp;lt;enumeration value="6"/&amp;gt;
     *             &amp;lt;enumeration value="5"/&amp;gt;
     *             &amp;lt;enumeration value="4"/&amp;gt;
     *             &amp;lt;enumeration value="3"/&amp;gt;
     *             &amp;lt;enumeration value="2"/&amp;gt;
     *             &amp;lt;enumeration value="1"/&amp;gt;
     *             &amp;lt;enumeration value="0"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FuelLevelDetails {

        @XmlAttribute(name = "FuelLevelValue")
        protected String fuelLevelValue;
        @XmlAttribute(name = "UnitOfMeasureQuantity")
        protected BigDecimal unitOfMeasureQuantity;
        @XmlAttribute(name = "UnitOfMeasure")
        protected String unitOfMeasure;
        @XmlAttribute(name = "UnitOfMeasureCode")
        protected String unitOfMeasureCode;

        /**
         * Obtiene el valor de la propiedad fuelLevelValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFuelLevelValue() {
            return fuelLevelValue;
        }

        /**
         * Define el valor de la propiedad fuelLevelValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFuelLevelValue(String value) {
            this.fuelLevelValue = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getUnitOfMeasureQuantity() {
            return unitOfMeasureQuantity;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setUnitOfMeasureQuantity(BigDecimal value) {
            this.unitOfMeasureQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasure.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasure() {
            return unitOfMeasure;
        }

        /**
         * Define el valor de la propiedad unitOfMeasure.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasure(String value) {
            this.unitOfMeasure = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasureCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasureCode() {
            return unitOfMeasureCode;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasureCode(String value) {
            this.unitOfMeasureCode = value;
        }

    }


    /**
     * To provide details of odometer measurements.
     * 
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OdometerReading {

        @XmlAttribute(name = "UnitOfMeasureQuantity")
        protected BigDecimal unitOfMeasureQuantity;
        @XmlAttribute(name = "UnitOfMeasure")
        protected String unitOfMeasure;
        @XmlAttribute(name = "UnitOfMeasureCode")
        protected String unitOfMeasureCode;

        /**
         * Obtiene el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getUnitOfMeasureQuantity() {
            return unitOfMeasureQuantity;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setUnitOfMeasureQuantity(BigDecimal value) {
            this.unitOfMeasureQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasure.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasure() {
            return unitOfMeasure;
        }

        /**
         * Define el valor de la propiedad unitOfMeasure.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasure(String value) {
            this.unitOfMeasure = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasureCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasureCode() {
            return unitOfMeasureCode;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasureCode(String value) {
            this.unitOfMeasureCode = value;
        }

    }

}
