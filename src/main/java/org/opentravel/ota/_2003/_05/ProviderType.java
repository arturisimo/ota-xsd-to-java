
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para ProviderType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="ProviderType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NotRequired"/&amp;gt;
 *     &amp;lt;enumeration value="Group"/&amp;gt;
 *     &amp;lt;enumeration value="Venue"/&amp;gt;
 *     &amp;lt;enumeration value="OutsideVendor"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "ProviderType")
@XmlEnum
public enum ProviderType {


    /**
     * Indicates no requirement.
     * 
     */
    @XmlEnumValue("NotRequired")
    NOT_REQUIRED("NotRequired"),

    /**
     * Indicates the group will provide.
     * 
     */
    @XmlEnumValue("Group")
    GROUP("Group"),

    /**
     * Indicates the venue will provide.
     * 
     */
    @XmlEnumValue("Venue")
    VENUE("Venue"),

    /**
     * Indicates an outside vendor will provide.
     * 
     */
    @XmlEnumValue("OutsideVendor")
    OUTSIDE_VENDOR("OutsideVendor");
    private final String value;

    ProviderType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProviderType fromValue(String v) {
        for (ProviderType c: ProviderType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
