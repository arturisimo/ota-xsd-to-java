
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to define the types of transportation offered.
 * 
 * &lt;p&gt;Clase Java para TransportationsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TransportationsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Transportations" type="{http://www.opentravel.org/OTA/2003/05}TransportationType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransportationsType", propOrder = {
    "transportations"
})
@XmlSeeAlso({
    RelativePositionType.class
})
public class TransportationsType {

    @XmlElement(name = "Transportations")
    protected TransportationType transportations;

    /**
     * Obtiene el valor de la propiedad transportations.
     * 
     * @return
     *     possible object is
     *     {@link TransportationType }
     *     
     */
    public TransportationType getTransportations() {
        return transportations;
    }

    /**
     * Define el valor de la propiedad transportations.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportationType }
     *     
     */
    public void setTransportations(TransportationType value) {
        this.transportations = value;
    }

}
