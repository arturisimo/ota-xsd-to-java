
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides actualized event information.
 * 
 * &lt;p&gt;Clase Java para EventReportType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="EventReportType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="EventSites" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="EventSite" type="{http://www.opentravel.org/OTA/2003/05}PostEventSiteReportType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="GeneralEventInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="EventContacts" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="EventContact" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                                     &amp;lt;attribute name="Role" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="EventLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="AttendeeInfo" maxOccurs="3" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Date" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Location" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;simpleContent&amp;gt;
 *                                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                                                         &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/simpleContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="ContractedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="EventDateType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Past"/&amp;gt;
 *                                           &amp;lt;enumeration value="Current"/&amp;gt;
 *                                           &amp;lt;enumeration value="Future"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="HousingInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="SleepingRoomsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="HotelQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="HousingProviderCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="HousingProviderName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="FoodAndBeverageInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="FoodAndBeverageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="LargestAttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="LargestAttendanceFunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="LargestRevenueFunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="ICW_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="FunctionSpaceInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="LargestFunctionSpace" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="AttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                     &amp;lt;attribute name="AudioVisualCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="BreakoutSessions" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="LargestConcurrentQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="LargestDailyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="TypicalSeatQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="TypicalRoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="FunctionSpaceRequirements" maxOccurs="10" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Requirement"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="RegistrationArea"/&amp;gt;
 *                                           &amp;lt;enumeration value="LoungeArea"/&amp;gt;
 *                                           &amp;lt;enumeration value="OfficeSpace"/&amp;gt;
 *                                           &amp;lt;enumeration value="TabletopExhibitSpace"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="FunctionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="PreFunctionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="PreEventSetupIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="MoveInRequirement" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                           &amp;lt;attribute name="MoveOutRequirement" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                           &amp;lt;attribute name="UtilityCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ExhibitionInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="ExhibitionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="Acronym" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                 &amp;lt;attribute name="URL" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&amp;gt;
 *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="Frequency" type="{http://www.opentravel.org/OTA/2003/05}FrequencyType" /&amp;gt;
 *                 &amp;lt;attribute name="SpouseInvitedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ChildrenInvitedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="Scope"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="CityWide"/&amp;gt;
 *                       &amp;lt;enumeration value="SingleVenue"/&amp;gt;
 *                       &amp;lt;enumeration value="MultipleVenue"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="OffsiteVenueIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ShuttleServiceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DestinationServiceProviderCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="AttendeeGuestProgramIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EventReportRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *       &amp;lt;attribute name="TimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventReportType", propOrder = {
    "eventSites",
    "generalEventInfo"
})
public class EventReportType {

    @XmlElement(name = "EventSites")
    protected EventReportType.EventSites eventSites;
    @XmlElement(name = "GeneralEventInfo")
    protected EventReportType.GeneralEventInfo generalEventInfo;
    @XmlAttribute(name = "Version")
    protected BigDecimal version;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "RPH")
    protected String rph;

    /**
     * Obtiene el valor de la propiedad eventSites.
     * 
     * @return
     *     possible object is
     *     {@link EventReportType.EventSites }
     *     
     */
    public EventReportType.EventSites getEventSites() {
        return eventSites;
    }

    /**
     * Define el valor de la propiedad eventSites.
     * 
     * @param value
     *     allowed object is
     *     {@link EventReportType.EventSites }
     *     
     */
    public void setEventSites(EventReportType.EventSites value) {
        this.eventSites = value;
    }

    /**
     * Obtiene el valor de la propiedad generalEventInfo.
     * 
     * @return
     *     possible object is
     *     {@link EventReportType.GeneralEventInfo }
     *     
     */
    public EventReportType.GeneralEventInfo getGeneralEventInfo() {
        return generalEventInfo;
    }

    /**
     * Define el valor de la propiedad generalEventInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link EventReportType.GeneralEventInfo }
     *     
     */
    public void setGeneralEventInfo(EventReportType.GeneralEventInfo value) {
        this.generalEventInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="EventSite" type="{http://www.opentravel.org/OTA/2003/05}PostEventSiteReportType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "eventSite"
    })
    public static class EventSites {

        @XmlElement(name = "EventSite", required = true)
        protected List<PostEventSiteReportType> eventSite;

        /**
         * Gets the value of the eventSite property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventSite property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getEventSite().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PostEventSiteReportType }
         * 
         * 
         */
        public List<PostEventSiteReportType> getEventSite() {
            if (eventSite == null) {
                eventSite = new ArrayList<PostEventSiteReportType>();
            }
            return this.eventSite;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="EventContacts" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="EventContact" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                           &amp;lt;attribute name="Role" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="EventLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="AttendeeInfo" maxOccurs="3" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Dates" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Date" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Location" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="ContractedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="EventDateType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Past"/&amp;gt;
     *                                 &amp;lt;enumeration value="Current"/&amp;gt;
     *                                 &amp;lt;enumeration value="Future"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="HousingInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="SleepingRoomsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="HotelQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="HousingProviderCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="HousingProviderName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="FoodAndBeverageInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="FoodAndBeverageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="LargestAttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="LargestAttendanceFunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="LargestRevenueFunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="ICW_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="FunctionSpaceInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="LargestFunctionSpace" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="AttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="AudioVisualCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="BreakoutSessions" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="LargestConcurrentQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="LargestDailyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="TypicalSeatQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="TypicalRoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="FunctionSpaceRequirements" maxOccurs="10" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Requirement"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="RegistrationArea"/&amp;gt;
     *                                 &amp;lt;enumeration value="LoungeArea"/&amp;gt;
     *                                 &amp;lt;enumeration value="OfficeSpace"/&amp;gt;
     *                                 &amp;lt;enumeration value="TabletopExhibitSpace"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="FunctionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="PreFunctionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PreEventSetupIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="MoveInRequirement" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                 &amp;lt;attribute name="MoveOutRequirement" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                 &amp;lt;attribute name="UtilityCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ExhibitionInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="ExhibitionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="Acronym" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *       &amp;lt;attribute name="URL" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&amp;gt;
     *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="Frequency" type="{http://www.opentravel.org/OTA/2003/05}FrequencyType" /&amp;gt;
     *       &amp;lt;attribute name="SpouseInvitedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ChildrenInvitedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="Scope"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="CityWide"/&amp;gt;
     *             &amp;lt;enumeration value="SingleVenue"/&amp;gt;
     *             &amp;lt;enumeration value="MultipleVenue"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="OffsiteVenueIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ShuttleServiceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DestinationServiceProviderCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="AttendeeGuestProgramIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EventReportRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "eventContacts",
        "eventLocation",
        "attendeeInfo",
        "dates",
        "housingInfo",
        "foodAndBeverageInfo",
        "functionSpaceInfo",
        "exhibitionInfo",
        "comments"
    })
    public static class GeneralEventInfo {

        @XmlElement(name = "EventContacts")
        protected EventReportType.GeneralEventInfo.EventContacts eventContacts;
        @XmlElement(name = "EventLocation")
        protected LocationGeneralType eventLocation;
        @XmlElement(name = "AttendeeInfo")
        protected List<EventReportType.GeneralEventInfo.AttendeeInfo> attendeeInfo;
        @XmlElement(name = "Dates")
        protected EventReportType.GeneralEventInfo.Dates dates;
        @XmlElement(name = "HousingInfo")
        protected EventReportType.GeneralEventInfo.HousingInfo housingInfo;
        @XmlElement(name = "FoodAndBeverageInfo")
        protected EventReportType.GeneralEventInfo.FoodAndBeverageInfo foodAndBeverageInfo;
        @XmlElement(name = "FunctionSpaceInfo")
        protected EventReportType.GeneralEventInfo.FunctionSpaceInfo functionSpaceInfo;
        @XmlElement(name = "ExhibitionInfo")
        protected EventReportType.GeneralEventInfo.ExhibitionInfo exhibitionInfo;
        @XmlElement(name = "Comments")
        protected EventReportType.GeneralEventInfo.Comments comments;
        @XmlAttribute(name = "Name")
        protected String name;
        @XmlAttribute(name = "Acronym")
        protected String acronym;
        @XmlAttribute(name = "URL")
        @XmlSchemaType(name = "anyURI")
        protected String url;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "Frequency")
        protected FrequencyType frequency;
        @XmlAttribute(name = "SpouseInvitedIndicator")
        protected Boolean spouseInvitedIndicator;
        @XmlAttribute(name = "ChildrenInvitedIndicator")
        protected Boolean childrenInvitedIndicator;
        @XmlAttribute(name = "Scope")
        protected String scope;
        @XmlAttribute(name = "OffsiteVenueIndicator")
        protected Boolean offsiteVenueIndicator;
        @XmlAttribute(name = "ShuttleServiceIndicator")
        protected Boolean shuttleServiceIndicator;
        @XmlAttribute(name = "DestinationServiceProviderCode")
        protected List<String> destinationServiceProviderCode;
        @XmlAttribute(name = "AttendeeGuestProgramIndicator")
        protected Boolean attendeeGuestProgramIndicator;
        @XmlAttribute(name = "EventReportRPH")
        protected List<String> eventReportRPH;

        /**
         * Obtiene el valor de la propiedad eventContacts.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.EventContacts }
         *     
         */
        public EventReportType.GeneralEventInfo.EventContacts getEventContacts() {
            return eventContacts;
        }

        /**
         * Define el valor de la propiedad eventContacts.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.EventContacts }
         *     
         */
        public void setEventContacts(EventReportType.GeneralEventInfo.EventContacts value) {
            this.eventContacts = value;
        }

        /**
         * Obtiene el valor de la propiedad eventLocation.
         * 
         * @return
         *     possible object is
         *     {@link LocationGeneralType }
         *     
         */
        public LocationGeneralType getEventLocation() {
            return eventLocation;
        }

        /**
         * Define el valor de la propiedad eventLocation.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationGeneralType }
         *     
         */
        public void setEventLocation(LocationGeneralType value) {
            this.eventLocation = value;
        }

        /**
         * Gets the value of the attendeeInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the attendeeInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAttendeeInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link EventReportType.GeneralEventInfo.AttendeeInfo }
         * 
         * 
         */
        public List<EventReportType.GeneralEventInfo.AttendeeInfo> getAttendeeInfo() {
            if (attendeeInfo == null) {
                attendeeInfo = new ArrayList<EventReportType.GeneralEventInfo.AttendeeInfo>();
            }
            return this.attendeeInfo;
        }

        /**
         * Obtiene el valor de la propiedad dates.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.Dates }
         *     
         */
        public EventReportType.GeneralEventInfo.Dates getDates() {
            return dates;
        }

        /**
         * Define el valor de la propiedad dates.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.Dates }
         *     
         */
        public void setDates(EventReportType.GeneralEventInfo.Dates value) {
            this.dates = value;
        }

        /**
         * Obtiene el valor de la propiedad housingInfo.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.HousingInfo }
         *     
         */
        public EventReportType.GeneralEventInfo.HousingInfo getHousingInfo() {
            return housingInfo;
        }

        /**
         * Define el valor de la propiedad housingInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.HousingInfo }
         *     
         */
        public void setHousingInfo(EventReportType.GeneralEventInfo.HousingInfo value) {
            this.housingInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad foodAndBeverageInfo.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.FoodAndBeverageInfo }
         *     
         */
        public EventReportType.GeneralEventInfo.FoodAndBeverageInfo getFoodAndBeverageInfo() {
            return foodAndBeverageInfo;
        }

        /**
         * Define el valor de la propiedad foodAndBeverageInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.FoodAndBeverageInfo }
         *     
         */
        public void setFoodAndBeverageInfo(EventReportType.GeneralEventInfo.FoodAndBeverageInfo value) {
            this.foodAndBeverageInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad functionSpaceInfo.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo }
         *     
         */
        public EventReportType.GeneralEventInfo.FunctionSpaceInfo getFunctionSpaceInfo() {
            return functionSpaceInfo;
        }

        /**
         * Define el valor de la propiedad functionSpaceInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo }
         *     
         */
        public void setFunctionSpaceInfo(EventReportType.GeneralEventInfo.FunctionSpaceInfo value) {
            this.functionSpaceInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad exhibitionInfo.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.ExhibitionInfo }
         *     
         */
        public EventReportType.GeneralEventInfo.ExhibitionInfo getExhibitionInfo() {
            return exhibitionInfo;
        }

        /**
         * Define el valor de la propiedad exhibitionInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.ExhibitionInfo }
         *     
         */
        public void setExhibitionInfo(EventReportType.GeneralEventInfo.ExhibitionInfo value) {
            this.exhibitionInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad comments.
         * 
         * @return
         *     possible object is
         *     {@link EventReportType.GeneralEventInfo.Comments }
         *     
         */
        public EventReportType.GeneralEventInfo.Comments getComments() {
            return comments;
        }

        /**
         * Define el valor de la propiedad comments.
         * 
         * @param value
         *     allowed object is
         *     {@link EventReportType.GeneralEventInfo.Comments }
         *     
         */
        public void setComments(EventReportType.GeneralEventInfo.Comments value) {
            this.comments = value;
        }

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad acronym.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcronym() {
            return acronym;
        }

        /**
         * Define el valor de la propiedad acronym.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcronym(String value) {
            this.acronym = value;
        }

        /**
         * Obtiene el valor de la propiedad url.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getURL() {
            return url;
        }

        /**
         * Define el valor de la propiedad url.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setURL(String value) {
            this.url = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad frequency.
         * 
         * @return
         *     possible object is
         *     {@link FrequencyType }
         *     
         */
        public FrequencyType getFrequency() {
            return frequency;
        }

        /**
         * Define el valor de la propiedad frequency.
         * 
         * @param value
         *     allowed object is
         *     {@link FrequencyType }
         *     
         */
        public void setFrequency(FrequencyType value) {
            this.frequency = value;
        }

        /**
         * Obtiene el valor de la propiedad spouseInvitedIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSpouseInvitedIndicator() {
            return spouseInvitedIndicator;
        }

        /**
         * Define el valor de la propiedad spouseInvitedIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSpouseInvitedIndicator(Boolean value) {
            this.spouseInvitedIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad childrenInvitedIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isChildrenInvitedIndicator() {
            return childrenInvitedIndicator;
        }

        /**
         * Define el valor de la propiedad childrenInvitedIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setChildrenInvitedIndicator(Boolean value) {
            this.childrenInvitedIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad scope.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getScope() {
            return scope;
        }

        /**
         * Define el valor de la propiedad scope.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setScope(String value) {
            this.scope = value;
        }

        /**
         * Obtiene el valor de la propiedad offsiteVenueIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOffsiteVenueIndicator() {
            return offsiteVenueIndicator;
        }

        /**
         * Define el valor de la propiedad offsiteVenueIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOffsiteVenueIndicator(Boolean value) {
            this.offsiteVenueIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad shuttleServiceIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isShuttleServiceIndicator() {
            return shuttleServiceIndicator;
        }

        /**
         * Define el valor de la propiedad shuttleServiceIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setShuttleServiceIndicator(Boolean value) {
            this.shuttleServiceIndicator = value;
        }

        /**
         * Gets the value of the destinationServiceProviderCode property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the destinationServiceProviderCode property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDestinationServiceProviderCode().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDestinationServiceProviderCode() {
            if (destinationServiceProviderCode == null) {
                destinationServiceProviderCode = new ArrayList<String>();
            }
            return this.destinationServiceProviderCode;
        }

        /**
         * Obtiene el valor de la propiedad attendeeGuestProgramIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAttendeeGuestProgramIndicator() {
            return attendeeGuestProgramIndicator;
        }

        /**
         * Define el valor de la propiedad attendeeGuestProgramIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAttendeeGuestProgramIndicator(Boolean value) {
            this.attendeeGuestProgramIndicator = value;
        }

        /**
         * Gets the value of the eventReportRPH property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventReportRPH property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getEventReportRPH().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getEventReportRPH() {
            if (eventReportRPH == null) {
                eventReportRPH = new ArrayList<String>();
            }
            return this.eventReportRPH;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comments"
        })
        public static class AttendeeInfo {

            @XmlElement(name = "Comments")
            protected EventReportType.GeneralEventInfo.AttendeeInfo.Comments comments;
            @XmlAttribute(name = "TotalQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger totalQuantity;
            @XmlAttribute(name = "DomesticQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger domesticQuantity;
            @XmlAttribute(name = "InternationalQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger internationalQuantity;
            @XmlAttribute(name = "PreRegisteredQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger preRegisteredQuantity;
            @XmlAttribute(name = "OnsiteRegisteredQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger onsiteRegisteredQuantity;
            @XmlAttribute(name = "NoShowQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger noShowQuantity;
            @XmlAttribute(name = "ExhibitorQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger exhibitorQuantity;
            @XmlAttribute(name = "QuantityType")
            protected String quantityType;

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link EventReportType.GeneralEventInfo.AttendeeInfo.Comments }
             *     
             */
            public EventReportType.GeneralEventInfo.AttendeeInfo.Comments getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link EventReportType.GeneralEventInfo.AttendeeInfo.Comments }
             *     
             */
            public void setComments(EventReportType.GeneralEventInfo.AttendeeInfo.Comments value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad totalQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTotalQuantity() {
                return totalQuantity;
            }

            /**
             * Define el valor de la propiedad totalQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTotalQuantity(BigInteger value) {
                this.totalQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad domesticQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDomesticQuantity() {
                return domesticQuantity;
            }

            /**
             * Define el valor de la propiedad domesticQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDomesticQuantity(BigInteger value) {
                this.domesticQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad internationalQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getInternationalQuantity() {
                return internationalQuantity;
            }

            /**
             * Define el valor de la propiedad internationalQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setInternationalQuantity(BigInteger value) {
                this.internationalQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad preRegisteredQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPreRegisteredQuantity() {
                return preRegisteredQuantity;
            }

            /**
             * Define el valor de la propiedad preRegisteredQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPreRegisteredQuantity(BigInteger value) {
                this.preRegisteredQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad onsiteRegisteredQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getOnsiteRegisteredQuantity() {
                return onsiteRegisteredQuantity;
            }

            /**
             * Define el valor de la propiedad onsiteRegisteredQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setOnsiteRegisteredQuantity(BigInteger value) {
                this.onsiteRegisteredQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad noShowQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNoShowQuantity() {
                return noShowQuantity;
            }

            /**
             * Define el valor de la propiedad noShowQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNoShowQuantity(BigInteger value) {
                this.noShowQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad exhibitorQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getExhibitorQuantity() {
                return exhibitorQuantity;
            }

            /**
             * Define el valor de la propiedad exhibitorQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setExhibitorQuantity(BigInteger value) {
                this.exhibitorQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad quantityType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuantityType() {
                return quantityType;
            }

            /**
             * Define el valor de la propiedad quantityType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuantityType(String value) {
                this.quantityType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class Comments {

                @XmlElement(name = "Comment", required = true)
                protected List<ParagraphType> comment;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ParagraphType }
                 * 
                 * 
                 */
                public List<ParagraphType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<ParagraphType>();
                    }
                    return this.comment;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comment"
        })
        public static class Comments {

            @XmlElement(name = "Comment")
            protected List<ParagraphType> comment;

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<ParagraphType>();
                }
                return this.comment;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Date" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Location" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="ContractedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="EventDateType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Past"/&amp;gt;
         *                       &amp;lt;enumeration value="Current"/&amp;gt;
         *                       &amp;lt;enumeration value="Future"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "date"
        })
        public static class Dates {

            @XmlElement(name = "Date", required = true)
            protected List<EventReportType.GeneralEventInfo.Dates.Date> date;

            /**
             * Gets the value of the date property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the date property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDate().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link EventReportType.GeneralEventInfo.Dates.Date }
             * 
             * 
             */
            public List<EventReportType.GeneralEventInfo.Dates.Date> getDate() {
                if (date == null) {
                    date = new ArrayList<EventReportType.GeneralEventInfo.Dates.Date>();
                }
                return this.date;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="LocationCategories" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Location" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
             *                           &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="ContractedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="EventDateType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Past"/&amp;gt;
             *             &amp;lt;enumeration value="Current"/&amp;gt;
             *             &amp;lt;enumeration value="Future"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "locationCategories",
                "comments"
            })
            public static class Date
                extends DateTimeSpanType
            {

                @XmlElement(name = "LocationCategories")
                protected List<EventReportType.GeneralEventInfo.Dates.Date.LocationCategories> locationCategories;
                @XmlElement(name = "Comments")
                protected EventReportType.GeneralEventInfo.Dates.Date.Comments comments;
                @XmlAttribute(name = "ContractedIndicator")
                protected Boolean contractedIndicator;
                @XmlAttribute(name = "EventDateType")
                protected String eventDateType;

                /**
                 * Gets the value of the locationCategories property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the locationCategories property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getLocationCategories().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link EventReportType.GeneralEventInfo.Dates.Date.LocationCategories }
                 * 
                 * 
                 */
                public List<EventReportType.GeneralEventInfo.Dates.Date.LocationCategories> getLocationCategories() {
                    if (locationCategories == null) {
                        locationCategories = new ArrayList<EventReportType.GeneralEventInfo.Dates.Date.LocationCategories>();
                    }
                    return this.locationCategories;
                }

                /**
                 * Obtiene el valor de la propiedad comments.
                 * 
                 * @return
                 *     possible object is
                 *     {@link EventReportType.GeneralEventInfo.Dates.Date.Comments }
                 *     
                 */
                public EventReportType.GeneralEventInfo.Dates.Date.Comments getComments() {
                    return comments;
                }

                /**
                 * Define el valor de la propiedad comments.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EventReportType.GeneralEventInfo.Dates.Date.Comments }
                 *     
                 */
                public void setComments(EventReportType.GeneralEventInfo.Dates.Date.Comments value) {
                    this.comments = value;
                }

                /**
                 * Obtiene el valor de la propiedad contractedIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isContractedIndicator() {
                    return contractedIndicator;
                }

                /**
                 * Define el valor de la propiedad contractedIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setContractedIndicator(Boolean value) {
                    this.contractedIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad eventDateType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEventDateType() {
                    return eventDateType;
                }

                /**
                 * Define el valor de la propiedad eventDateType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEventDateType(String value) {
                    this.eventDateType = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "comment"
                })
                public static class Comments {

                    @XmlElement(name = "Comment", required = true)
                    protected List<ParagraphType> comment;

                    /**
                     * Gets the value of the comment property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getComment().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link ParagraphType }
                     * 
                     * 
                     */
                    public List<ParagraphType> getComment() {
                        if (comment == null) {
                            comment = new ArrayList<ParagraphType>();
                        }
                        return this.comment;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Location" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Category" maxOccurs="99" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "location",
                    "category"
                })
                public static class LocationCategories {

                    @XmlElement(name = "Location")
                    protected EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Location location;
                    @XmlElement(name = "Category")
                    protected List<EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Category> category;

                    /**
                     * Obtiene el valor de la propiedad location.
                     * 
                     * @return
                     *     possible object is
                     *     {@link EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Location }
                     *     
                     */
                    public EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Location getLocation() {
                        return location;
                    }

                    /**
                     * Define el valor de la propiedad location.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Location }
                     *     
                     */
                    public void setLocation(EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Location value) {
                        this.location = value;
                    }

                    /**
                     * Gets the value of the category property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the category property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getCategory().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Category }
                     * 
                     * 
                     */
                    public List<EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Category> getCategory() {
                        if (category == null) {
                            category = new ArrayList<EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Category>();
                        }
                        return this.category;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
                     *       &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Category
                        extends FormattedTextTextType
                    {

                        @XmlAttribute(name = "LocationCategoryCode")
                        protected String locationCategoryCode;
                        @XmlAttribute(name = "PreferLevel")
                        protected PreferLevelType preferLevel;

                        /**
                         * Obtiene el valor de la propiedad locationCategoryCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLocationCategoryCode() {
                            return locationCategoryCode;
                        }

                        /**
                         * Define el valor de la propiedad locationCategoryCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLocationCategoryCode(String value) {
                            this.locationCategoryCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad preferLevel.
                         * 
                         * @return
                         *     possible object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public PreferLevelType getPreferLevel() {
                            return preferLevel;
                        }

                        /**
                         * Define el valor de la propiedad preferLevel.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PreferLevelType }
                         *     
                         */
                        public void setPreferLevel(PreferLevelType value) {
                            this.preferLevel = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}LocationGeneralType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Location
                        extends LocationGeneralType
                    {

                        @XmlAttribute(name = "ChainCode")
                        protected String chainCode;
                        @XmlAttribute(name = "BrandCode")
                        protected String brandCode;
                        @XmlAttribute(name = "HotelCode")
                        protected String hotelCode;
                        @XmlAttribute(name = "HotelCityCode")
                        protected String hotelCityCode;
                        @XmlAttribute(name = "HotelName")
                        protected String hotelName;
                        @XmlAttribute(name = "HotelCodeContext")
                        protected String hotelCodeContext;
                        @XmlAttribute(name = "ChainName")
                        protected String chainName;
                        @XmlAttribute(name = "BrandName")
                        protected String brandName;
                        @XmlAttribute(name = "AreaID")
                        protected String areaID;
                        @XmlAttribute(name = "TTIcode")
                        @XmlSchemaType(name = "positiveInteger")
                        protected BigInteger ttIcode;

                        /**
                         * Obtiene el valor de la propiedad chainCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getChainCode() {
                            return chainCode;
                        }

                        /**
                         * Define el valor de la propiedad chainCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setChainCode(String value) {
                            this.chainCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad brandCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getBrandCode() {
                            return brandCode;
                        }

                        /**
                         * Define el valor de la propiedad brandCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setBrandCode(String value) {
                            this.brandCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad hotelCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getHotelCode() {
                            return hotelCode;
                        }

                        /**
                         * Define el valor de la propiedad hotelCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setHotelCode(String value) {
                            this.hotelCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad hotelCityCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getHotelCityCode() {
                            return hotelCityCode;
                        }

                        /**
                         * Define el valor de la propiedad hotelCityCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setHotelCityCode(String value) {
                            this.hotelCityCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad hotelName.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getHotelName() {
                            return hotelName;
                        }

                        /**
                         * Define el valor de la propiedad hotelName.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setHotelName(String value) {
                            this.hotelName = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad hotelCodeContext.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getHotelCodeContext() {
                            return hotelCodeContext;
                        }

                        /**
                         * Define el valor de la propiedad hotelCodeContext.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setHotelCodeContext(String value) {
                            this.hotelCodeContext = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad chainName.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getChainName() {
                            return chainName;
                        }

                        /**
                         * Define el valor de la propiedad chainName.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setChainName(String value) {
                            this.chainName = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad brandName.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getBrandName() {
                            return brandName;
                        }

                        /**
                         * Define el valor de la propiedad brandName.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setBrandName(String value) {
                            this.brandName = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad areaID.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getAreaID() {
                            return areaID;
                        }

                        /**
                         * Define el valor de la propiedad areaID.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setAreaID(String value) {
                            this.areaID = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad ttIcode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getTTIcode() {
                            return ttIcode;
                        }

                        /**
                         * Define el valor de la propiedad ttIcode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setTTIcode(BigInteger value) {
                            this.ttIcode = value;
                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="EventContact" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *                 &amp;lt;attribute name="Role" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "eventContact"
        })
        public static class EventContacts {

            @XmlElement(name = "EventContact", required = true)
            protected List<EventReportType.GeneralEventInfo.EventContacts.EventContact> eventContact;

            /**
             * Gets the value of the eventContact property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventContact property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getEventContact().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link EventReportType.GeneralEventInfo.EventContacts.EventContact }
             * 
             * 
             */
            public List<EventReportType.GeneralEventInfo.EventContacts.EventContact> getEventContact() {
                if (eventContact == null) {
                    eventContact = new ArrayList<EventReportType.GeneralEventInfo.EventContacts.EventContact>();
                }
                return this.eventContact;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
             *       &amp;lt;attribute name="Role" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class EventContact
                extends ContactPersonType
            {

                @XmlAttribute(name = "Role")
                protected String role;

                /**
                 * Obtiene el valor de la propiedad role.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRole() {
                    return role;
                }

                /**
                 * Define el valor de la propiedad role.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRole(String value) {
                    this.role = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="ExhibitionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ExhibitionInfo {

            @XmlAttribute(name = "ExhibitionSpaceIndicator")
            protected Boolean exhibitionSpaceIndicator;
            @XmlAttribute(name = "PropertyTypeCode")
            protected List<String> propertyTypeCode;

            /**
             * Obtiene el valor de la propiedad exhibitionSpaceIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExhibitionSpaceIndicator() {
                return exhibitionSpaceIndicator;
            }

            /**
             * Define el valor de la propiedad exhibitionSpaceIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExhibitionSpaceIndicator(Boolean value) {
                this.exhibitionSpaceIndicator = value;
            }

            /**
             * Gets the value of the propertyTypeCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyTypeCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPropertyTypeCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPropertyTypeCode() {
                if (propertyTypeCode == null) {
                    propertyTypeCode = new ArrayList<String>();
                }
                return this.propertyTypeCode;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="FoodAndBeverageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="LargestAttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="LargestAttendanceFunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="LargestRevenueFunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="ICW_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comments"
        })
        public static class FoodAndBeverageInfo {

            @XmlElement(name = "Comments")
            protected EventReportType.GeneralEventInfo.FoodAndBeverageInfo.Comments comments;
            @XmlAttribute(name = "FoodAndBeverageIndicator")
            protected Boolean foodAndBeverageIndicator;
            @XmlAttribute(name = "LargestAttendanceQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger largestAttendanceQuantity;
            @XmlAttribute(name = "LargestAttendanceFunctionType")
            protected String largestAttendanceFunctionType;
            @XmlAttribute(name = "LargestRevenueFunctionType")
            protected String largestRevenueFunctionType;
            @XmlAttribute(name = "ICW_Indicator")
            protected Boolean icwIndicator;

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link EventReportType.GeneralEventInfo.FoodAndBeverageInfo.Comments }
             *     
             */
            public EventReportType.GeneralEventInfo.FoodAndBeverageInfo.Comments getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link EventReportType.GeneralEventInfo.FoodAndBeverageInfo.Comments }
             *     
             */
            public void setComments(EventReportType.GeneralEventInfo.FoodAndBeverageInfo.Comments value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad foodAndBeverageIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFoodAndBeverageIndicator() {
                return foodAndBeverageIndicator;
            }

            /**
             * Define el valor de la propiedad foodAndBeverageIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFoodAndBeverageIndicator(Boolean value) {
                this.foodAndBeverageIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad largestAttendanceQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLargestAttendanceQuantity() {
                return largestAttendanceQuantity;
            }

            /**
             * Define el valor de la propiedad largestAttendanceQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLargestAttendanceQuantity(BigInteger value) {
                this.largestAttendanceQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad largestAttendanceFunctionType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLargestAttendanceFunctionType() {
                return largestAttendanceFunctionType;
            }

            /**
             * Define el valor de la propiedad largestAttendanceFunctionType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLargestAttendanceFunctionType(String value) {
                this.largestAttendanceFunctionType = value;
            }

            /**
             * Obtiene el valor de la propiedad largestRevenueFunctionType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLargestRevenueFunctionType() {
                return largestRevenueFunctionType;
            }

            /**
             * Define el valor de la propiedad largestRevenueFunctionType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLargestRevenueFunctionType(String value) {
                this.largestRevenueFunctionType = value;
            }

            /**
             * Obtiene el valor de la propiedad icwIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isICWIndicator() {
                return icwIndicator;
            }

            /**
             * Define el valor de la propiedad icwIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setICWIndicator(Boolean value) {
                this.icwIndicator = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class Comments {

                @XmlElement(name = "Comment", required = true)
                protected List<ParagraphType> comment;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ParagraphType }
                 * 
                 * 
                 */
                public List<ParagraphType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<ParagraphType>();
                    }
                    return this.comment;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="LargestFunctionSpace" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="AttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="AudioVisualCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="BreakoutSessions" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="LargestConcurrentQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="LargestDailyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="TypicalSeatQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="TypicalRoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="FunctionSpaceRequirements" maxOccurs="10" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Requirement"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="RegistrationArea"/&amp;gt;
         *                       &amp;lt;enumeration value="LoungeArea"/&amp;gt;
         *                       &amp;lt;enumeration value="OfficeSpace"/&amp;gt;
         *                       &amp;lt;enumeration value="TabletopExhibitSpace"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="FunctionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="PreFunctionSpaceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PreEventSetupIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="MoveInRequirement" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *       &amp;lt;attribute name="MoveOutRequirement" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *       &amp;lt;attribute name="UtilityCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "largestFunctionSpace",
            "breakoutSessions",
            "functionSpaceRequirements",
            "comments"
        })
        public static class FunctionSpaceInfo {

            @XmlElement(name = "LargestFunctionSpace")
            protected EventReportType.GeneralEventInfo.FunctionSpaceInfo.LargestFunctionSpace largestFunctionSpace;
            @XmlElement(name = "BreakoutSessions")
            protected EventReportType.GeneralEventInfo.FunctionSpaceInfo.BreakoutSessions breakoutSessions;
            @XmlElement(name = "FunctionSpaceRequirements")
            protected List<EventReportType.GeneralEventInfo.FunctionSpaceInfo.FunctionSpaceRequirements> functionSpaceRequirements;
            @XmlElement(name = "Comments")
            protected EventReportType.GeneralEventInfo.FunctionSpaceInfo.Comments comments;
            @XmlAttribute(name = "FunctionSpaceIndicator")
            protected Boolean functionSpaceIndicator;
            @XmlAttribute(name = "PropertyTypeCode")
            protected List<String> propertyTypeCode;
            @XmlAttribute(name = "PreFunctionSpaceIndicator")
            protected Boolean preFunctionSpaceIndicator;
            @XmlAttribute(name = "PreEventSetupIndicator")
            protected Boolean preEventSetupIndicator;
            @XmlAttribute(name = "MoveInRequirement")
            protected Duration moveInRequirement;
            @XmlAttribute(name = "MoveOutRequirement")
            protected Duration moveOutRequirement;
            @XmlAttribute(name = "UtilityCode")
            protected List<String> utilityCode;
            @XmlAttribute(name = "TelecommunicationCode")
            protected List<String> telecommunicationCode;

            /**
             * Obtiene el valor de la propiedad largestFunctionSpace.
             * 
             * @return
             *     possible object is
             *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.LargestFunctionSpace }
             *     
             */
            public EventReportType.GeneralEventInfo.FunctionSpaceInfo.LargestFunctionSpace getLargestFunctionSpace() {
                return largestFunctionSpace;
            }

            /**
             * Define el valor de la propiedad largestFunctionSpace.
             * 
             * @param value
             *     allowed object is
             *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.LargestFunctionSpace }
             *     
             */
            public void setLargestFunctionSpace(EventReportType.GeneralEventInfo.FunctionSpaceInfo.LargestFunctionSpace value) {
                this.largestFunctionSpace = value;
            }

            /**
             * Obtiene el valor de la propiedad breakoutSessions.
             * 
             * @return
             *     possible object is
             *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.BreakoutSessions }
             *     
             */
            public EventReportType.GeneralEventInfo.FunctionSpaceInfo.BreakoutSessions getBreakoutSessions() {
                return breakoutSessions;
            }

            /**
             * Define el valor de la propiedad breakoutSessions.
             * 
             * @param value
             *     allowed object is
             *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.BreakoutSessions }
             *     
             */
            public void setBreakoutSessions(EventReportType.GeneralEventInfo.FunctionSpaceInfo.BreakoutSessions value) {
                this.breakoutSessions = value;
            }

            /**
             * Gets the value of the functionSpaceRequirements property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the functionSpaceRequirements property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getFunctionSpaceRequirements().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.FunctionSpaceRequirements }
             * 
             * 
             */
            public List<EventReportType.GeneralEventInfo.FunctionSpaceInfo.FunctionSpaceRequirements> getFunctionSpaceRequirements() {
                if (functionSpaceRequirements == null) {
                    functionSpaceRequirements = new ArrayList<EventReportType.GeneralEventInfo.FunctionSpaceInfo.FunctionSpaceRequirements>();
                }
                return this.functionSpaceRequirements;
            }

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.Comments }
             *     
             */
            public EventReportType.GeneralEventInfo.FunctionSpaceInfo.Comments getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link EventReportType.GeneralEventInfo.FunctionSpaceInfo.Comments }
             *     
             */
            public void setComments(EventReportType.GeneralEventInfo.FunctionSpaceInfo.Comments value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad functionSpaceIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFunctionSpaceIndicator() {
                return functionSpaceIndicator;
            }

            /**
             * Define el valor de la propiedad functionSpaceIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFunctionSpaceIndicator(Boolean value) {
                this.functionSpaceIndicator = value;
            }

            /**
             * Gets the value of the propertyTypeCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the propertyTypeCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPropertyTypeCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPropertyTypeCode() {
                if (propertyTypeCode == null) {
                    propertyTypeCode = new ArrayList<String>();
                }
                return this.propertyTypeCode;
            }

            /**
             * Obtiene el valor de la propiedad preFunctionSpaceIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPreFunctionSpaceIndicator() {
                return preFunctionSpaceIndicator;
            }

            /**
             * Define el valor de la propiedad preFunctionSpaceIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPreFunctionSpaceIndicator(Boolean value) {
                this.preFunctionSpaceIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad preEventSetupIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPreEventSetupIndicator() {
                return preEventSetupIndicator;
            }

            /**
             * Define el valor de la propiedad preEventSetupIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPreEventSetupIndicator(Boolean value) {
                this.preEventSetupIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad moveInRequirement.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getMoveInRequirement() {
                return moveInRequirement;
            }

            /**
             * Define el valor de la propiedad moveInRequirement.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setMoveInRequirement(Duration value) {
                this.moveInRequirement = value;
            }

            /**
             * Obtiene el valor de la propiedad moveOutRequirement.
             * 
             * @return
             *     possible object is
             *     {@link Duration }
             *     
             */
            public Duration getMoveOutRequirement() {
                return moveOutRequirement;
            }

            /**
             * Define el valor de la propiedad moveOutRequirement.
             * 
             * @param value
             *     allowed object is
             *     {@link Duration }
             *     
             */
            public void setMoveOutRequirement(Duration value) {
                this.moveOutRequirement = value;
            }

            /**
             * Gets the value of the utilityCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the utilityCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getUtilityCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getUtilityCode() {
                if (utilityCode == null) {
                    utilityCode = new ArrayList<String>();
                }
                return this.utilityCode;
            }

            /**
             * Gets the value of the telecommunicationCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the telecommunicationCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTelecommunicationCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getTelecommunicationCode() {
                if (telecommunicationCode == null) {
                    telecommunicationCode = new ArrayList<String>();
                }
                return this.telecommunicationCode;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="LargestConcurrentQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="LargestDailyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="TypicalSeatQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="TypicalRoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class BreakoutSessions {

                @XmlAttribute(name = "LargestConcurrentQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger largestConcurrentQuantity;
                @XmlAttribute(name = "LargestDailyQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger largestDailyQuantity;
                @XmlAttribute(name = "TypicalSeatQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger typicalSeatQuantity;
                @XmlAttribute(name = "TypicalRoomSetupCode")
                protected String typicalRoomSetupCode;

                /**
                 * Obtiene el valor de la propiedad largestConcurrentQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getLargestConcurrentQuantity() {
                    return largestConcurrentQuantity;
                }

                /**
                 * Define el valor de la propiedad largestConcurrentQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setLargestConcurrentQuantity(BigInteger value) {
                    this.largestConcurrentQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad largestDailyQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getLargestDailyQuantity() {
                    return largestDailyQuantity;
                }

                /**
                 * Define el valor de la propiedad largestDailyQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setLargestDailyQuantity(BigInteger value) {
                    this.largestDailyQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad typicalSeatQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTypicalSeatQuantity() {
                    return typicalSeatQuantity;
                }

                /**
                 * Define el valor de la propiedad typicalSeatQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTypicalSeatQuantity(BigInteger value) {
                    this.typicalSeatQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad typicalRoomSetupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTypicalRoomSetupCode() {
                    return typicalRoomSetupCode;
                }

                /**
                 * Define el valor de la propiedad typicalRoomSetupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTypicalRoomSetupCode(String value) {
                    this.typicalRoomSetupCode = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class Comments {

                @XmlElement(name = "Comment", required = true)
                protected List<ParagraphType> comment;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ParagraphType }
                 * 
                 * 
                 */
                public List<ParagraphType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<ParagraphType>();
                    }
                    return this.comment;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
             *       &amp;lt;attribute name="Requirement"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="RegistrationArea"/&amp;gt;
             *             &amp;lt;enumeration value="LoungeArea"/&amp;gt;
             *             &amp;lt;enumeration value="OfficeSpace"/&amp;gt;
             *             &amp;lt;enumeration value="TabletopExhibitSpace"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class FunctionSpaceRequirements {

                @XmlAttribute(name = "Requirement")
                protected String requirement;
                @XmlAttribute(name = "Quantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger quantity;

                /**
                 * Obtiene el valor de la propiedad requirement.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequirement() {
                    return requirement;
                }

                /**
                 * Define el valor de la propiedad requirement.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequirement(String value) {
                    this.requirement = value;
                }

                /**
                 * Obtiene el valor de la propiedad quantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getQuantity() {
                    return quantity;
                }

                /**
                 * Define el valor de la propiedad quantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setQuantity(BigInteger value) {
                    this.quantity = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="AttendanceQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="AudioVisualCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class LargestFunctionSpace {

                @XmlAttribute(name = "AttendanceQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger attendanceQuantity;
                @XmlAttribute(name = "TwentyFourHourHoldInd")
                protected Boolean twentyFourHourHoldInd;
                @XmlAttribute(name = "RoomSetupCode")
                protected String roomSetupCode;
                @XmlAttribute(name = "AudioVisualCode")
                protected List<String> audioVisualCode;

                /**
                 * Obtiene el valor de la propiedad attendanceQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getAttendanceQuantity() {
                    return attendanceQuantity;
                }

                /**
                 * Define el valor de la propiedad attendanceQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setAttendanceQuantity(BigInteger value) {
                    this.attendanceQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad twentyFourHourHoldInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isTwentyFourHourHoldInd() {
                    return twentyFourHourHoldInd;
                }

                /**
                 * Define el valor de la propiedad twentyFourHourHoldInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setTwentyFourHourHoldInd(Boolean value) {
                    this.twentyFourHourHoldInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad roomSetupCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRoomSetupCode() {
                    return roomSetupCode;
                }

                /**
                 * Define el valor de la propiedad roomSetupCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRoomSetupCode(String value) {
                    this.roomSetupCode = value;
                }

                /**
                 * Gets the value of the audioVisualCode property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the audioVisualCode property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getAudioVisualCode().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getAudioVisualCode() {
                    if (audioVisualCode == null) {
                        audioVisualCode = new ArrayList<String>();
                    }
                    return this.audioVisualCode;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="SleepingRoomsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="HotelQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="HousingProviderCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="HousingProviderName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comments"
        })
        public static class HousingInfo {

            @XmlElement(name = "Comments")
            protected EventReportType.GeneralEventInfo.HousingInfo.Comments comments;
            @XmlAttribute(name = "SleepingRoomsIndicator")
            protected Boolean sleepingRoomsIndicator;
            @XmlAttribute(name = "HotelQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger hotelQuantity;
            @XmlAttribute(name = "PeakRoomNightQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger peakRoomNightQuantity;
            @XmlAttribute(name = "HousingProviderCode")
            protected String housingProviderCode;
            @XmlAttribute(name = "HousingProviderName")
            protected String housingProviderName;

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link EventReportType.GeneralEventInfo.HousingInfo.Comments }
             *     
             */
            public EventReportType.GeneralEventInfo.HousingInfo.Comments getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link EventReportType.GeneralEventInfo.HousingInfo.Comments }
             *     
             */
            public void setComments(EventReportType.GeneralEventInfo.HousingInfo.Comments value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad sleepingRoomsIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSleepingRoomsIndicator() {
                return sleepingRoomsIndicator;
            }

            /**
             * Define el valor de la propiedad sleepingRoomsIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSleepingRoomsIndicator(Boolean value) {
                this.sleepingRoomsIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getHotelQuantity() {
                return hotelQuantity;
            }

            /**
             * Define el valor de la propiedad hotelQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setHotelQuantity(BigInteger value) {
                this.hotelQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad peakRoomNightQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPeakRoomNightQuantity() {
                return peakRoomNightQuantity;
            }

            /**
             * Define el valor de la propiedad peakRoomNightQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPeakRoomNightQuantity(BigInteger value) {
                this.peakRoomNightQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad housingProviderCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousingProviderCode() {
                return housingProviderCode;
            }

            /**
             * Define el valor de la propiedad housingProviderCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousingProviderCode(String value) {
                this.housingProviderCode = value;
            }

            /**
             * Obtiene el valor de la propiedad housingProviderName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousingProviderName() {
                return housingProviderName;
            }

            /**
             * Define el valor de la propiedad housingProviderName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousingProviderName(String value) {
                this.housingProviderName = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class Comments {

                @XmlElement(name = "Comment", required = true)
                protected List<ParagraphType> comment;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ParagraphType }
                 * 
                 * 
                 */
                public List<ParagraphType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<ParagraphType>();
                    }
                    return this.comment;
                }

            }

        }

    }

}
