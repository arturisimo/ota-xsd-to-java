
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Defines the ship details.
 * 
 * &lt;p&gt;Clase Java para ShipInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ShipInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Ship" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="StabilizedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RegistrationCountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                 &amp;lt;attribute name="RestaurantQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="ElevatorQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaxCrewQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaxGuestQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="CruisingSpeed" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                 &amp;lt;attribute name="MaxSpeed" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                 &amp;lt;attribute name="InsideCabinQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="OutsideCabinQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="InauguralDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="RefurbishedDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="BuiltDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="NextRefurbishDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ShipLength" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ShipVoltage" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ShipReferenceGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipInfoType", propOrder = {
    "ship",
    "shipLength",
    "shipVoltage"
})
public class ShipInfoType {

    @XmlElement(name = "Ship")
    protected ShipInfoType.Ship ship;
    @XmlElement(name = "ShipLength")
    protected ShipInfoType.ShipLength shipLength;
    @XmlElement(name = "ShipVoltage")
    protected ShipInfoType.ShipVoltage shipVoltage;
    @XmlAttribute(name = "VendorCode")
    protected String vendorCode;
    @XmlAttribute(name = "VendorName")
    protected String vendorName;
    @XmlAttribute(name = "ShipCode")
    protected String shipCode;
    @XmlAttribute(name = "ShipName")
    protected String shipName;
    @XmlAttribute(name = "VendorCodeContext")
    protected String vendorCodeContext;

    /**
     * Obtiene el valor de la propiedad ship.
     * 
     * @return
     *     possible object is
     *     {@link ShipInfoType.Ship }
     *     
     */
    public ShipInfoType.Ship getShip() {
        return ship;
    }

    /**
     * Define el valor de la propiedad ship.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipInfoType.Ship }
     *     
     */
    public void setShip(ShipInfoType.Ship value) {
        this.ship = value;
    }

    /**
     * Obtiene el valor de la propiedad shipLength.
     * 
     * @return
     *     possible object is
     *     {@link ShipInfoType.ShipLength }
     *     
     */
    public ShipInfoType.ShipLength getShipLength() {
        return shipLength;
    }

    /**
     * Define el valor de la propiedad shipLength.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipInfoType.ShipLength }
     *     
     */
    public void setShipLength(ShipInfoType.ShipLength value) {
        this.shipLength = value;
    }

    /**
     * Obtiene el valor de la propiedad shipVoltage.
     * 
     * @return
     *     possible object is
     *     {@link ShipInfoType.ShipVoltage }
     *     
     */
    public ShipInfoType.ShipVoltage getShipVoltage() {
        return shipVoltage;
    }

    /**
     * Define el valor de la propiedad shipVoltage.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipInfoType.ShipVoltage }
     *     
     */
    public void setShipVoltage(ShipInfoType.ShipVoltage value) {
        this.shipVoltage = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * Define el valor de la propiedad vendorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCode(String value) {
        this.vendorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * Define el valor de la propiedad vendorName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorName(String value) {
        this.vendorName = value;
    }

    /**
     * Obtiene el valor de la propiedad shipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipCode() {
        return shipCode;
    }

    /**
     * Define el valor de la propiedad shipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipCode(String value) {
        this.shipCode = value;
    }

    /**
     * Obtiene el valor de la propiedad shipName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Define el valor de la propiedad shipName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCodeContext() {
        return vendorCodeContext;
    }

    /**
     * Define el valor de la propiedad vendorCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCodeContext(String value) {
        this.vendorCodeContext = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="StabilizedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RegistrationCountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *       &amp;lt;attribute name="RestaurantQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="ElevatorQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaxCrewQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaxGuestQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="CruisingSpeed" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *       &amp;lt;attribute name="MaxSpeed" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *       &amp;lt;attribute name="InsideCabinQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="OutsideCabinQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="InauguralDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="RefurbishedDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="BuiltDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="NextRefurbishDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Ship {

        @XmlAttribute(name = "StabilizedIndicator")
        protected Boolean stabilizedIndicator;
        @XmlAttribute(name = "RegistrationCountryCode")
        protected String registrationCountryCode;
        @XmlAttribute(name = "RestaurantQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger restaurantQuantity;
        @XmlAttribute(name = "ElevatorQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger elevatorQuantity;
        @XmlAttribute(name = "MaxCrewQuantity")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxCrewQuantity;
        @XmlAttribute(name = "MaxGuestQuantity")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxGuestQuantity;
        @XmlAttribute(name = "CruisingSpeed")
        protected BigDecimal cruisingSpeed;
        @XmlAttribute(name = "MaxSpeed")
        protected BigDecimal maxSpeed;
        @XmlAttribute(name = "InsideCabinQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger insideCabinQuantity;
        @XmlAttribute(name = "OutsideCabinQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger outsideCabinQuantity;
        @XmlAttribute(name = "InauguralDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar inauguralDate;
        @XmlAttribute(name = "RefurbishedDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar refurbishedDate;
        @XmlAttribute(name = "BuiltDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar builtDate;
        @XmlAttribute(name = "NextRefurbishDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar nextRefurbishDate;

        /**
         * Obtiene el valor de la propiedad stabilizedIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isStabilizedIndicator() {
            return stabilizedIndicator;
        }

        /**
         * Define el valor de la propiedad stabilizedIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setStabilizedIndicator(Boolean value) {
            this.stabilizedIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad registrationCountryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegistrationCountryCode() {
            return registrationCountryCode;
        }

        /**
         * Define el valor de la propiedad registrationCountryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegistrationCountryCode(String value) {
            this.registrationCountryCode = value;
        }

        /**
         * Obtiene el valor de la propiedad restaurantQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRestaurantQuantity() {
            return restaurantQuantity;
        }

        /**
         * Define el valor de la propiedad restaurantQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRestaurantQuantity(BigInteger value) {
            this.restaurantQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad elevatorQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getElevatorQuantity() {
            return elevatorQuantity;
        }

        /**
         * Define el valor de la propiedad elevatorQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setElevatorQuantity(BigInteger value) {
            this.elevatorQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad maxCrewQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxCrewQuantity() {
            return maxCrewQuantity;
        }

        /**
         * Define el valor de la propiedad maxCrewQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxCrewQuantity(BigInteger value) {
            this.maxCrewQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad maxGuestQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxGuestQuantity() {
            return maxGuestQuantity;
        }

        /**
         * Define el valor de la propiedad maxGuestQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxGuestQuantity(BigInteger value) {
            this.maxGuestQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad cruisingSpeed.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCruisingSpeed() {
            return cruisingSpeed;
        }

        /**
         * Define el valor de la propiedad cruisingSpeed.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCruisingSpeed(BigDecimal value) {
            this.cruisingSpeed = value;
        }

        /**
         * Obtiene el valor de la propiedad maxSpeed.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getMaxSpeed() {
            return maxSpeed;
        }

        /**
         * Define el valor de la propiedad maxSpeed.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setMaxSpeed(BigDecimal value) {
            this.maxSpeed = value;
        }

        /**
         * Obtiene el valor de la propiedad insideCabinQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getInsideCabinQuantity() {
            return insideCabinQuantity;
        }

        /**
         * Define el valor de la propiedad insideCabinQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setInsideCabinQuantity(BigInteger value) {
            this.insideCabinQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad outsideCabinQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getOutsideCabinQuantity() {
            return outsideCabinQuantity;
        }

        /**
         * Define el valor de la propiedad outsideCabinQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setOutsideCabinQuantity(BigInteger value) {
            this.outsideCabinQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad inauguralDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getInauguralDate() {
            return inauguralDate;
        }

        /**
         * Define el valor de la propiedad inauguralDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setInauguralDate(XMLGregorianCalendar value) {
            this.inauguralDate = value;
        }

        /**
         * Obtiene el valor de la propiedad refurbishedDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRefurbishedDate() {
            return refurbishedDate;
        }

        /**
         * Define el valor de la propiedad refurbishedDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRefurbishedDate(XMLGregorianCalendar value) {
            this.refurbishedDate = value;
        }

        /**
         * Obtiene el valor de la propiedad builtDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getBuiltDate() {
            return builtDate;
        }

        /**
         * Define el valor de la propiedad builtDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setBuiltDate(XMLGregorianCalendar value) {
            this.builtDate = value;
        }

        /**
         * Obtiene el valor de la propiedad nextRefurbishDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getNextRefurbishDate() {
            return nextRefurbishDate;
        }

        /**
         * Define el valor de la propiedad nextRefurbishDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setNextRefurbishDate(XMLGregorianCalendar value) {
            this.nextRefurbishDate = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ShipLength {

        @XmlAttribute(name = "UnitOfMeasureQuantity")
        protected BigDecimal unitOfMeasureQuantity;
        @XmlAttribute(name = "UnitOfMeasure")
        protected String unitOfMeasure;
        @XmlAttribute(name = "UnitOfMeasureCode")
        protected String unitOfMeasureCode;

        /**
         * Obtiene el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getUnitOfMeasureQuantity() {
            return unitOfMeasureQuantity;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setUnitOfMeasureQuantity(BigDecimal value) {
            this.unitOfMeasureQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasure.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasure() {
            return unitOfMeasure;
        }

        /**
         * Define el valor de la propiedad unitOfMeasure.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasure(String value) {
            this.unitOfMeasure = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasureCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasureCode() {
            return unitOfMeasureCode;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasureCode(String value) {
            this.unitOfMeasureCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ShipVoltage {

        @XmlAttribute(name = "UnitOfMeasureQuantity")
        protected BigDecimal unitOfMeasureQuantity;
        @XmlAttribute(name = "UnitOfMeasure")
        protected String unitOfMeasure;
        @XmlAttribute(name = "UnitOfMeasureCode")
        protected String unitOfMeasureCode;

        /**
         * Obtiene el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getUnitOfMeasureQuantity() {
            return unitOfMeasureQuantity;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setUnitOfMeasureQuantity(BigDecimal value) {
            this.unitOfMeasureQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasure.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasure() {
            return unitOfMeasure;
        }

        /**
         * Define el valor de la propiedad unitOfMeasure.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasure(String value) {
            this.unitOfMeasure = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasureCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasureCode() {
            return unitOfMeasureCode;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasureCode(String value) {
            this.unitOfMeasureCode = value;
        }

    }

}
