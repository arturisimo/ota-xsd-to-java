
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPricingType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPricingType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="PerEvent"/&amp;gt;
 *     &amp;lt;enumeration value="PerGroup"/&amp;gt;
 *     &amp;lt;enumeration value="PerPerson"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPricingType")
@XmlEnum
public enum ListOfferPricingType {


    /**
     * Offer pricing includes one price for all travelers in the party.
     * 
     */
    @XmlEnumValue("PerEvent")
    PER_EVENT("PerEvent"),

    /**
     * Offer pricing is for a group and additional eligibility requirements (such as group size) may apply.
     * 
     */
    @XmlEnumValue("PerGroup")
    PER_GROUP("PerGroup"),

    /**
     * Offer pricing is per person.
     * 
     */
    @XmlEnumValue("PerPerson")
    PER_PERSON("PerPerson"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferPricingType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPricingType fromValue(String v) {
        for (ListOfferPricingType c: ListOfferPricingType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
