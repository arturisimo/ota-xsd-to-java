
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides the available facilities for the enquiror's chosen property and stay.
 * 
 * &lt;p&gt;Clase Java para FacilityChoicesType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FacilityChoicesType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AvailableRooms" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Room" type="{http://www.opentravel.org/OTA/2003/05}PkgRoomInventoryType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AvailableMealPlans" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RoomPrices" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomPrice" type="{http://www.opentravel.org/OTA/2003/05}RoomPriceType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OccupancyGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacilityChoicesType", propOrder = {
    "availableRooms",
    "availableMealPlans",
    "roomPrices"
})
public class FacilityChoicesType {

    @XmlElement(name = "AvailableRooms")
    protected FacilityChoicesType.AvailableRooms availableRooms;
    @XmlElement(name = "AvailableMealPlans")
    protected FacilityChoicesType.AvailableMealPlans availableMealPlans;
    @XmlElement(name = "RoomPrices")
    protected List<FacilityChoicesType.RoomPrices> roomPrices;
    @XmlAttribute(name = "MinOccupancy")
    protected Integer minOccupancy;
    @XmlAttribute(name = "MaxOccupancy")
    protected Integer maxOccupancy;

    /**
     * Obtiene el valor de la propiedad availableRooms.
     * 
     * @return
     *     possible object is
     *     {@link FacilityChoicesType.AvailableRooms }
     *     
     */
    public FacilityChoicesType.AvailableRooms getAvailableRooms() {
        return availableRooms;
    }

    /**
     * Define el valor de la propiedad availableRooms.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityChoicesType.AvailableRooms }
     *     
     */
    public void setAvailableRooms(FacilityChoicesType.AvailableRooms value) {
        this.availableRooms = value;
    }

    /**
     * Obtiene el valor de la propiedad availableMealPlans.
     * 
     * @return
     *     possible object is
     *     {@link FacilityChoicesType.AvailableMealPlans }
     *     
     */
    public FacilityChoicesType.AvailableMealPlans getAvailableMealPlans() {
        return availableMealPlans;
    }

    /**
     * Define el valor de la propiedad availableMealPlans.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityChoicesType.AvailableMealPlans }
     *     
     */
    public void setAvailableMealPlans(FacilityChoicesType.AvailableMealPlans value) {
        this.availableMealPlans = value;
    }

    /**
     * Gets the value of the roomPrices property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomPrices property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRoomPrices().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FacilityChoicesType.RoomPrices }
     * 
     * 
     */
    public List<FacilityChoicesType.RoomPrices> getRoomPrices() {
        if (roomPrices == null) {
            roomPrices = new ArrayList<FacilityChoicesType.RoomPrices>();
        }
        return this.roomPrices;
    }

    /**
     * Obtiene el valor de la propiedad minOccupancy.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinOccupancy() {
        return minOccupancy;
    }

    /**
     * Define el valor de la propiedad minOccupancy.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinOccupancy(Integer value) {
        this.minOccupancy = value;
    }

    /**
     * Obtiene el valor de la propiedad maxOccupancy.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxOccupancy() {
        return maxOccupancy;
    }

    /**
     * Define el valor de la propiedad maxOccupancy.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxOccupancy(Integer value) {
        this.maxOccupancy = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanType" maxOccurs="9"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mealPlan"
    })
    public static class AvailableMealPlans {

        @XmlElement(name = "MealPlan", required = true)
        protected List<MealPlanType> mealPlan;

        /**
         * Gets the value of the mealPlan property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the mealPlan property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getMealPlan().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link MealPlanType }
         * 
         * 
         */
        public List<MealPlanType> getMealPlan() {
            if (mealPlan == null) {
                mealPlan = new ArrayList<MealPlanType>();
            }
            return this.mealPlan;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Room" type="{http://www.opentravel.org/OTA/2003/05}PkgRoomInventoryType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "room"
    })
    public static class AvailableRooms {

        @XmlElement(name = "Room", required = true)
        protected List<PkgRoomInventoryType> room;

        /**
         * Gets the value of the room property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the room property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoom().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PkgRoomInventoryType }
         * 
         * 
         */
        public List<PkgRoomInventoryType> getRoom() {
            if (room == null) {
                room = new ArrayList<PkgRoomInventoryType>();
            }
            return this.room;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomPrice" type="{http://www.opentravel.org/OTA/2003/05}RoomPriceType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="MealPlan" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomPrice"
    })
    public static class RoomPrices {

        @XmlElement(name = "RoomPrice", required = true)
        protected List<RoomPriceType> roomPrice;
        @XmlAttribute(name = "MealPlan")
        protected String mealPlan;

        /**
         * Gets the value of the roomPrice property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomPrice property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomPrice().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link RoomPriceType }
         * 
         * 
         */
        public List<RoomPriceType> getRoomPrice() {
            if (roomPrice == null) {
                roomPrice = new ArrayList<RoomPriceType>();
            }
            return this.roomPrice;
        }

        /**
         * Obtiene el valor de la propiedad mealPlan.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMealPlan() {
            return mealPlan;
        }

        /**
         * Define el valor de la propiedad mealPlan.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMealPlan(String value) {
            this.mealPlan = value;
        }

    }

}
