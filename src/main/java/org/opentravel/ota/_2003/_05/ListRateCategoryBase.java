
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_RateCategory_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_RateCategory_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="All"/&amp;gt;
 *     &amp;lt;enumeration value="Association"/&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="BusinessStandard"/&amp;gt;
 *     &amp;lt;enumeration value="Consortiums"/&amp;gt;
 *     &amp;lt;enumeration value="Conventions"/&amp;gt;
 *     &amp;lt;enumeration value="Corporate"/&amp;gt;
 *     &amp;lt;enumeration value="Credential"/&amp;gt;
 *     &amp;lt;enumeration value="Employee"/&amp;gt;
 *     &amp;lt;enumeration value="FullyInclusive"/&amp;gt;
 *     &amp;lt;enumeration value="Government"/&amp;gt;
 *     &amp;lt;enumeration value="Inclusive"/&amp;gt;
 *     &amp;lt;enumeration value="Industry"/&amp;gt;
 *     &amp;lt;enumeration value="Leisure"/&amp;gt;
 *     &amp;lt;enumeration value="Negotiated"/&amp;gt;
 *     &amp;lt;enumeration value="Promotional"/&amp;gt;
 *     &amp;lt;enumeration value="Standard"/&amp;gt;
 *     &amp;lt;enumeration value="VIP"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_RateCategory_Base")
@XmlEnum
public enum ListRateCategoryBase {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Association")
    ASSOCIATION("Association"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("BusinessStandard")
    BUSINESS_STANDARD("BusinessStandard"),
    @XmlEnumValue("Consortiums")
    CONSORTIUMS("Consortiums"),
    @XmlEnumValue("Conventions")
    CONVENTIONS("Conventions"),
    @XmlEnumValue("Corporate")
    CORPORATE("Corporate"),
    @XmlEnumValue("Credential")
    CREDENTIAL("Credential"),
    @XmlEnumValue("Employee")
    EMPLOYEE("Employee"),
    @XmlEnumValue("FullyInclusive")
    FULLY_INCLUSIVE("FullyInclusive"),
    @XmlEnumValue("Government")
    GOVERNMENT("Government"),
    @XmlEnumValue("Inclusive")
    INCLUSIVE("Inclusive"),
    @XmlEnumValue("Industry")
    INDUSTRY("Industry"),
    @XmlEnumValue("Leisure")
    LEISURE("Leisure"),
    @XmlEnumValue("Negotiated")
    NEGOTIATED("Negotiated"),
    @XmlEnumValue("Promotional")
    PROMOTIONAL("Promotional"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    VIP("VIP"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListRateCategoryBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListRateCategoryBase fromValue(String v) {
        for (ListRateCategoryBase c: ListRateCategoryBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
