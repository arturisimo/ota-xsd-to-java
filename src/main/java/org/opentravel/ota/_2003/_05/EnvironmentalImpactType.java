
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Property environmental impact and green program and initiative information.
 * 
 * &lt;p&gt;Clase Java para EnvironmentalImpactType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="EnvironmentalImpactType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CarbonFootprint" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CarbonUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="NonCarbonMeasureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NonCarbonMeasureDesc" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="MeetingEvent" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="RoomNight" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="WeatherNormalized" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Water" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="WaterUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="ProgramDescription" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="GrayWaterInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="WaterProgramInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="PerRoomNightUsage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="PerRoomDayUsage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="WasteDiversionPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Energy" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Usage" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="PowerType" type="{http://www.opentravel.org/OTA/2003/05}List_PowerType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="PowerTypeUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ProgramDescription" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="EfficientAppliancesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EfficientLightingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="HeatPowerCombInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="HeatPumpInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="PowerSavingPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SolarInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="TempControlInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *                 &amp;lt;attribute name="WindTurbineInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EnergyGenerated" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="EnergyStarRating" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Recycling" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Details" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="RecycledProducts" type="{http://www.opentravel.org/OTA/2003/05}List_RecycledProducts" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="RecyclingLocations" type="{http://www.opentravel.org/OTA/2003/05}List_RecycleFacilityLocation" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ProgramDescription" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="ActiveProgramInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EcologicalDishwareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="General" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Certification" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Environmental" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="CertificationAvailInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ChemicalAllergiesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="EnvironmentalCleanersInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="LinenReuseInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *                 &amp;lt;attribute name="ProcurementPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnvironmentalImpactType", propOrder = {
    "carbonFootprint",
    "water",
    "energy",
    "recycling",
    "general"
})
public class EnvironmentalImpactType {

    @XmlElement(name = "CarbonFootprint")
    protected EnvironmentalImpactType.CarbonFootprint carbonFootprint;
    @XmlElement(name = "Water")
    protected EnvironmentalImpactType.Water water;
    @XmlElement(name = "Energy")
    protected EnvironmentalImpactType.Energy energy;
    @XmlElement(name = "Recycling")
    protected EnvironmentalImpactType.Recycling recycling;
    @XmlElement(name = "General")
    protected EnvironmentalImpactType.General general;

    /**
     * Obtiene el valor de la propiedad carbonFootprint.
     * 
     * @return
     *     possible object is
     *     {@link EnvironmentalImpactType.CarbonFootprint }
     *     
     */
    public EnvironmentalImpactType.CarbonFootprint getCarbonFootprint() {
        return carbonFootprint;
    }

    /**
     * Define el valor de la propiedad carbonFootprint.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvironmentalImpactType.CarbonFootprint }
     *     
     */
    public void setCarbonFootprint(EnvironmentalImpactType.CarbonFootprint value) {
        this.carbonFootprint = value;
    }

    /**
     * Obtiene el valor de la propiedad water.
     * 
     * @return
     *     possible object is
     *     {@link EnvironmentalImpactType.Water }
     *     
     */
    public EnvironmentalImpactType.Water getWater() {
        return water;
    }

    /**
     * Define el valor de la propiedad water.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvironmentalImpactType.Water }
     *     
     */
    public void setWater(EnvironmentalImpactType.Water value) {
        this.water = value;
    }

    /**
     * Obtiene el valor de la propiedad energy.
     * 
     * @return
     *     possible object is
     *     {@link EnvironmentalImpactType.Energy }
     *     
     */
    public EnvironmentalImpactType.Energy getEnergy() {
        return energy;
    }

    /**
     * Define el valor de la propiedad energy.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvironmentalImpactType.Energy }
     *     
     */
    public void setEnergy(EnvironmentalImpactType.Energy value) {
        this.energy = value;
    }

    /**
     * Obtiene el valor de la propiedad recycling.
     * 
     * @return
     *     possible object is
     *     {@link EnvironmentalImpactType.Recycling }
     *     
     */
    public EnvironmentalImpactType.Recycling getRecycling() {
        return recycling;
    }

    /**
     * Define el valor de la propiedad recycling.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvironmentalImpactType.Recycling }
     *     
     */
    public void setRecycling(EnvironmentalImpactType.Recycling value) {
        this.recycling = value;
    }

    /**
     * Obtiene el valor de la propiedad general.
     * 
     * @return
     *     possible object is
     *     {@link EnvironmentalImpactType.General }
     *     
     */
    public EnvironmentalImpactType.General getGeneral() {
        return general;
    }

    /**
     * Define el valor de la propiedad general.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvironmentalImpactType.General }
     *     
     */
    public void setGeneral(EnvironmentalImpactType.General value) {
        this.general = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CarbonUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="NonCarbonMeasureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NonCarbonMeasureDesc" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="MeetingEvent" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="RoomNight" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="WeatherNormalized" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "carbonUOM"
    })
    public static class CarbonFootprint {

        @XmlElement(name = "CarbonUOM")
        protected ListUnitOfMeasure carbonUOM;
        @XmlAttribute(name = "NonCarbonMeasureInd")
        protected Boolean nonCarbonMeasureInd;
        @XmlAttribute(name = "NonCarbonMeasureDesc")
        protected String nonCarbonMeasureDesc;
        @XmlAttribute(name = "MeetingEvent")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger meetingEvent;
        @XmlAttribute(name = "RoomNight")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger roomNight;
        @XmlAttribute(name = "WeatherNormalized")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger weatherNormalized;

        /**
         * Obtiene el valor de la propiedad carbonUOM.
         * 
         * @return
         *     possible object is
         *     {@link ListUnitOfMeasure }
         *     
         */
        public ListUnitOfMeasure getCarbonUOM() {
            return carbonUOM;
        }

        /**
         * Define el valor de la propiedad carbonUOM.
         * 
         * @param value
         *     allowed object is
         *     {@link ListUnitOfMeasure }
         *     
         */
        public void setCarbonUOM(ListUnitOfMeasure value) {
            this.carbonUOM = value;
        }

        /**
         * Obtiene el valor de la propiedad nonCarbonMeasureInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonCarbonMeasureInd() {
            return nonCarbonMeasureInd;
        }

        /**
         * Define el valor de la propiedad nonCarbonMeasureInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonCarbonMeasureInd(Boolean value) {
            this.nonCarbonMeasureInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonCarbonMeasureDesc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNonCarbonMeasureDesc() {
            return nonCarbonMeasureDesc;
        }

        /**
         * Define el valor de la propiedad nonCarbonMeasureDesc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNonCarbonMeasureDesc(String value) {
            this.nonCarbonMeasureDesc = value;
        }

        /**
         * Obtiene el valor de la propiedad meetingEvent.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeetingEvent() {
            return meetingEvent;
        }

        /**
         * Define el valor de la propiedad meetingEvent.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeetingEvent(BigInteger value) {
            this.meetingEvent = value;
        }

        /**
         * Obtiene el valor de la propiedad roomNight.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRoomNight() {
            return roomNight;
        }

        /**
         * Define el valor de la propiedad roomNight.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRoomNight(BigInteger value) {
            this.roomNight = value;
        }

        /**
         * Obtiene el valor de la propiedad weatherNormalized.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getWeatherNormalized() {
            return weatherNormalized;
        }

        /**
         * Define el valor de la propiedad weatherNormalized.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setWeatherNormalized(BigInteger value) {
            this.weatherNormalized = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Usage" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PowerType" type="{http://www.opentravel.org/OTA/2003/05}List_PowerType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="PowerTypeUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ProgramDescription" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="EfficientAppliancesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EfficientLightingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="HeatPowerCombInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="HeatPumpInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PowerSavingPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SolarInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="TempControlInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
     *       &amp;lt;attribute name="WindTurbineInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EnergyGenerated" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="EnergyStarRating" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "usage",
        "programDescription"
    })
    public static class Energy {

        @XmlElement(name = "Usage")
        protected List<EnvironmentalImpactType.Energy.Usage> usage;
        @XmlElement(name = "ProgramDescription")
        protected List<ParagraphType> programDescription;
        @XmlAttribute(name = "EfficientAppliancesInd")
        protected Boolean efficientAppliancesInd;
        @XmlAttribute(name = "EfficientLightingInd")
        protected Boolean efficientLightingInd;
        @XmlAttribute(name = "HeatPowerCombInd")
        protected Boolean heatPowerCombInd;
        @XmlAttribute(name = "HeatPumpInd")
        protected Boolean heatPumpInd;
        @XmlAttribute(name = "PowerSavingPolicyInd")
        protected Boolean powerSavingPolicyInd;
        @XmlAttribute(name = "SolarInd")
        protected Boolean solarInd;
        @XmlAttribute(name = "TempControlInd")
        @XmlSchemaType(name = "anySimpleType")
        protected String tempControlInd;
        @XmlAttribute(name = "WindTurbineInd")
        protected Boolean windTurbineInd;
        @XmlAttribute(name = "EnergyGenerated")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger energyGenerated;
        @XmlAttribute(name = "EnergyStarRating")
        protected String energyStarRating;

        /**
         * Gets the value of the usage property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the usage property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getUsage().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link EnvironmentalImpactType.Energy.Usage }
         * 
         * 
         */
        public List<EnvironmentalImpactType.Energy.Usage> getUsage() {
            if (usage == null) {
                usage = new ArrayList<EnvironmentalImpactType.Energy.Usage>();
            }
            return this.usage;
        }

        /**
         * Gets the value of the programDescription property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the programDescription property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getProgramDescription().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getProgramDescription() {
            if (programDescription == null) {
                programDescription = new ArrayList<ParagraphType>();
            }
            return this.programDescription;
        }

        /**
         * Obtiene el valor de la propiedad efficientAppliancesInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEfficientAppliancesInd() {
            return efficientAppliancesInd;
        }

        /**
         * Define el valor de la propiedad efficientAppliancesInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEfficientAppliancesInd(Boolean value) {
            this.efficientAppliancesInd = value;
        }

        /**
         * Obtiene el valor de la propiedad efficientLightingInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEfficientLightingInd() {
            return efficientLightingInd;
        }

        /**
         * Define el valor de la propiedad efficientLightingInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEfficientLightingInd(Boolean value) {
            this.efficientLightingInd = value;
        }

        /**
         * Obtiene el valor de la propiedad heatPowerCombInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isHeatPowerCombInd() {
            return heatPowerCombInd;
        }

        /**
         * Define el valor de la propiedad heatPowerCombInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setHeatPowerCombInd(Boolean value) {
            this.heatPowerCombInd = value;
        }

        /**
         * Obtiene el valor de la propiedad heatPumpInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isHeatPumpInd() {
            return heatPumpInd;
        }

        /**
         * Define el valor de la propiedad heatPumpInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setHeatPumpInd(Boolean value) {
            this.heatPumpInd = value;
        }

        /**
         * Obtiene el valor de la propiedad powerSavingPolicyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPowerSavingPolicyInd() {
            return powerSavingPolicyInd;
        }

        /**
         * Define el valor de la propiedad powerSavingPolicyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPowerSavingPolicyInd(Boolean value) {
            this.powerSavingPolicyInd = value;
        }

        /**
         * Obtiene el valor de la propiedad solarInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSolarInd() {
            return solarInd;
        }

        /**
         * Define el valor de la propiedad solarInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSolarInd(Boolean value) {
            this.solarInd = value;
        }

        /**
         * Obtiene el valor de la propiedad tempControlInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTempControlInd() {
            return tempControlInd;
        }

        /**
         * Define el valor de la propiedad tempControlInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTempControlInd(String value) {
            this.tempControlInd = value;
        }

        /**
         * Obtiene el valor de la propiedad windTurbineInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isWindTurbineInd() {
            return windTurbineInd;
        }

        /**
         * Define el valor de la propiedad windTurbineInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setWindTurbineInd(Boolean value) {
            this.windTurbineInd = value;
        }

        /**
         * Obtiene el valor de la propiedad energyGenerated.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEnergyGenerated() {
            return energyGenerated;
        }

        /**
         * Define el valor de la propiedad energyGenerated.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEnergyGenerated(BigInteger value) {
            this.energyGenerated = value;
        }

        /**
         * Obtiene el valor de la propiedad energyStarRating.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnergyStarRating() {
            return energyStarRating;
        }

        /**
         * Define el valor de la propiedad energyStarRating.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnergyStarRating(String value) {
            this.energyStarRating = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PowerType" type="{http://www.opentravel.org/OTA/2003/05}List_PowerType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="PowerTypeUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "powerType",
            "powerTypeUOM"
        })
        public static class Usage {

            @XmlElement(name = "PowerType")
            protected ListPowerType powerType;
            @XmlElement(name = "PowerTypeUOM")
            protected ListUnitOfMeasure powerTypeUOM;
            @XmlAttribute(name = "Quantity")
            protected BigDecimal quantity;

            /**
             * Obtiene el valor de la propiedad powerType.
             * 
             * @return
             *     possible object is
             *     {@link ListPowerType }
             *     
             */
            public ListPowerType getPowerType() {
                return powerType;
            }

            /**
             * Define el valor de la propiedad powerType.
             * 
             * @param value
             *     allowed object is
             *     {@link ListPowerType }
             *     
             */
            public void setPowerType(ListPowerType value) {
                this.powerType = value;
            }

            /**
             * Obtiene el valor de la propiedad powerTypeUOM.
             * 
             * @return
             *     possible object is
             *     {@link ListUnitOfMeasure }
             *     
             */
            public ListUnitOfMeasure getPowerTypeUOM() {
                return powerTypeUOM;
            }

            /**
             * Define el valor de la propiedad powerTypeUOM.
             * 
             * @param value
             *     allowed object is
             *     {@link ListUnitOfMeasure }
             *     
             */
            public void setPowerTypeUOM(ListUnitOfMeasure value) {
                this.powerTypeUOM = value;
            }

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setQuantity(BigDecimal value) {
                this.quantity = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Certification" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Environmental" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="CertificationAvailInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ChemicalAllergiesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EnvironmentalCleanersInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="LinenReuseInd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
     *       &amp;lt;attribute name="ProcurementPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "certification",
        "environmental"
    })
    public static class General {

        @XmlElement(name = "Certification")
        protected List<ParagraphType> certification;
        @XmlElement(name = "Environmental")
        protected List<ParagraphType> environmental;
        @XmlAttribute(name = "CertificationAvailInd")
        protected Boolean certificationAvailInd;
        @XmlAttribute(name = "ChemicalAllergiesInd")
        protected Boolean chemicalAllergiesInd;
        @XmlAttribute(name = "EnvironmentalCleanersInd")
        protected Boolean environmentalCleanersInd;
        @XmlAttribute(name = "LinenReuseInd")
        @XmlSchemaType(name = "anySimpleType")
        protected String linenReuseInd;
        @XmlAttribute(name = "ProcurementPolicyInd")
        protected Boolean procurementPolicyInd;

        /**
         * Gets the value of the certification property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the certification property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCertification().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getCertification() {
            if (certification == null) {
                certification = new ArrayList<ParagraphType>();
            }
            return this.certification;
        }

        /**
         * Gets the value of the environmental property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the environmental property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getEnvironmental().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getEnvironmental() {
            if (environmental == null) {
                environmental = new ArrayList<ParagraphType>();
            }
            return this.environmental;
        }

        /**
         * Obtiene el valor de la propiedad certificationAvailInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCertificationAvailInd() {
            return certificationAvailInd;
        }

        /**
         * Define el valor de la propiedad certificationAvailInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCertificationAvailInd(Boolean value) {
            this.certificationAvailInd = value;
        }

        /**
         * Obtiene el valor de la propiedad chemicalAllergiesInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isChemicalAllergiesInd() {
            return chemicalAllergiesInd;
        }

        /**
         * Define el valor de la propiedad chemicalAllergiesInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setChemicalAllergiesInd(Boolean value) {
            this.chemicalAllergiesInd = value;
        }

        /**
         * Obtiene el valor de la propiedad environmentalCleanersInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEnvironmentalCleanersInd() {
            return environmentalCleanersInd;
        }

        /**
         * Define el valor de la propiedad environmentalCleanersInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEnvironmentalCleanersInd(Boolean value) {
            this.environmentalCleanersInd = value;
        }

        /**
         * Obtiene el valor de la propiedad linenReuseInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLinenReuseInd() {
            return linenReuseInd;
        }

        /**
         * Define el valor de la propiedad linenReuseInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLinenReuseInd(String value) {
            this.linenReuseInd = value;
        }

        /**
         * Obtiene el valor de la propiedad procurementPolicyInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isProcurementPolicyInd() {
            return procurementPolicyInd;
        }

        /**
         * Define el valor de la propiedad procurementPolicyInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setProcurementPolicyInd(Boolean value) {
            this.procurementPolicyInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Details" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RecycledProducts" type="{http://www.opentravel.org/OTA/2003/05}List_RecycledProducts" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RecyclingLocations" type="{http://www.opentravel.org/OTA/2003/05}List_RecycleFacilityLocation" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ProgramDescription" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="ActiveProgramInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="EcologicalDishwareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "details",
        "programDescription"
    })
    public static class Recycling {

        @XmlElement(name = "Details")
        protected EnvironmentalImpactType.Recycling.Details details;
        @XmlElement(name = "ProgramDescription")
        protected List<ParagraphType> programDescription;
        @XmlAttribute(name = "ActiveProgramInd")
        protected Boolean activeProgramInd;
        @XmlAttribute(name = "EcologicalDishwareInd")
        protected Boolean ecologicalDishwareInd;

        /**
         * Obtiene el valor de la propiedad details.
         * 
         * @return
         *     possible object is
         *     {@link EnvironmentalImpactType.Recycling.Details }
         *     
         */
        public EnvironmentalImpactType.Recycling.Details getDetails() {
            return details;
        }

        /**
         * Define el valor de la propiedad details.
         * 
         * @param value
         *     allowed object is
         *     {@link EnvironmentalImpactType.Recycling.Details }
         *     
         */
        public void setDetails(EnvironmentalImpactType.Recycling.Details value) {
            this.details = value;
        }

        /**
         * Gets the value of the programDescription property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the programDescription property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getProgramDescription().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getProgramDescription() {
            if (programDescription == null) {
                programDescription = new ArrayList<ParagraphType>();
            }
            return this.programDescription;
        }

        /**
         * Obtiene el valor de la propiedad activeProgramInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isActiveProgramInd() {
            return activeProgramInd;
        }

        /**
         * Define el valor de la propiedad activeProgramInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setActiveProgramInd(Boolean value) {
            this.activeProgramInd = value;
        }

        /**
         * Obtiene el valor de la propiedad ecologicalDishwareInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEcologicalDishwareInd() {
            return ecologicalDishwareInd;
        }

        /**
         * Define el valor de la propiedad ecologicalDishwareInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEcologicalDishwareInd(Boolean value) {
            this.ecologicalDishwareInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RecycledProducts" type="{http://www.opentravel.org/OTA/2003/05}List_RecycledProducts" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RecyclingLocations" type="{http://www.opentravel.org/OTA/2003/05}List_RecycleFacilityLocation" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "recycledProducts",
            "recyclingLocations"
        })
        public static class Details {

            @XmlElement(name = "RecycledProducts")
            protected List<ListRecycledProducts> recycledProducts;
            @XmlElement(name = "RecyclingLocations")
            protected List<ListRecycleFacilityLocation> recyclingLocations;

            /**
             * Gets the value of the recycledProducts property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the recycledProducts property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRecycledProducts().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ListRecycledProducts }
             * 
             * 
             */
            public List<ListRecycledProducts> getRecycledProducts() {
                if (recycledProducts == null) {
                    recycledProducts = new ArrayList<ListRecycledProducts>();
                }
                return this.recycledProducts;
            }

            /**
             * Gets the value of the recyclingLocations property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the recyclingLocations property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRecyclingLocations().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ListRecycleFacilityLocation }
             * 
             * 
             */
            public List<ListRecycleFacilityLocation> getRecyclingLocations() {
                if (recyclingLocations == null) {
                    recyclingLocations = new ArrayList<ListRecycleFacilityLocation>();
                }
                return this.recyclingLocations;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="WaterUOM" type="{http://www.opentravel.org/OTA/2003/05}List_UnitOfMeasure" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ProgramDescription" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="GrayWaterInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="WaterProgramInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PerRoomNightUsage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="PerRoomDayUsage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="WasteDiversionPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "waterUOM",
        "programDescription"
    })
    public static class Water {

        @XmlElement(name = "WaterUOM")
        protected ListUnitOfMeasure waterUOM;
        @XmlElement(name = "ProgramDescription")
        protected List<ParagraphType> programDescription;
        @XmlAttribute(name = "GrayWaterInd")
        protected Boolean grayWaterInd;
        @XmlAttribute(name = "WaterProgramInd")
        protected Boolean waterProgramInd;
        @XmlAttribute(name = "PerRoomNightUsage")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger perRoomNightUsage;
        @XmlAttribute(name = "PerRoomDayUsage")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger perRoomDayUsage;
        @XmlAttribute(name = "WasteDiversionPercentage")
        protected BigDecimal wasteDiversionPercentage;

        /**
         * Obtiene el valor de la propiedad waterUOM.
         * 
         * @return
         *     possible object is
         *     {@link ListUnitOfMeasure }
         *     
         */
        public ListUnitOfMeasure getWaterUOM() {
            return waterUOM;
        }

        /**
         * Define el valor de la propiedad waterUOM.
         * 
         * @param value
         *     allowed object is
         *     {@link ListUnitOfMeasure }
         *     
         */
        public void setWaterUOM(ListUnitOfMeasure value) {
            this.waterUOM = value;
        }

        /**
         * Gets the value of the programDescription property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the programDescription property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getProgramDescription().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getProgramDescription() {
            if (programDescription == null) {
                programDescription = new ArrayList<ParagraphType>();
            }
            return this.programDescription;
        }

        /**
         * Obtiene el valor de la propiedad grayWaterInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGrayWaterInd() {
            return grayWaterInd;
        }

        /**
         * Define el valor de la propiedad grayWaterInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGrayWaterInd(Boolean value) {
            this.grayWaterInd = value;
        }

        /**
         * Obtiene el valor de la propiedad waterProgramInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isWaterProgramInd() {
            return waterProgramInd;
        }

        /**
         * Define el valor de la propiedad waterProgramInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setWaterProgramInd(Boolean value) {
            this.waterProgramInd = value;
        }

        /**
         * Obtiene el valor de la propiedad perRoomNightUsage.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPerRoomNightUsage() {
            return perRoomNightUsage;
        }

        /**
         * Define el valor de la propiedad perRoomNightUsage.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPerRoomNightUsage(BigInteger value) {
            this.perRoomNightUsage = value;
        }

        /**
         * Obtiene el valor de la propiedad perRoomDayUsage.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPerRoomDayUsage() {
            return perRoomDayUsage;
        }

        /**
         * Define el valor de la propiedad perRoomDayUsage.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPerRoomDayUsage(BigInteger value) {
            this.perRoomDayUsage = value;
        }

        /**
         * Obtiene el valor de la propiedad wasteDiversionPercentage.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getWasteDiversionPercentage() {
            return wasteDiversionPercentage;
        }

        /**
         * Define el valor de la propiedad wasteDiversionPercentage.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setWasteDiversionPercentage(BigDecimal value) {
            this.wasteDiversionPercentage = value;
        }

    }

}
