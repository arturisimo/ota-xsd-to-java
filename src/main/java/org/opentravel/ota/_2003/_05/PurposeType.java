
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para PurposeType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="PurposeType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Sell"/&amp;gt;
 *     &amp;lt;enumeration value="Net"/&amp;gt;
 *     &amp;lt;enumeration value="Base"/&amp;gt;
 *     &amp;lt;enumeration value="Refund"/&amp;gt;
 *     &amp;lt;enumeration value="Additional"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "PurposeType")
@XmlEnum
public enum PurposeType {

    @XmlEnumValue("Sell")
    SELL("Sell"),
    @XmlEnumValue("Net")
    NET("Net"),
    @XmlEnumValue("Base")
    BASE("Base"),
    @XmlEnumValue("Refund")
    REFUND("Refund"),
    @XmlEnumValue("Additional")
    ADDITIONAL("Additional");
    private final String value;

    PurposeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PurposeType fromValue(String v) {
        for (PurposeType c: PurposeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
