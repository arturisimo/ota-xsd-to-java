
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identifies the aggregation information to be returned in the response.
 * 
 * &lt;p&gt;Clase Java para AggregateInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AggregateInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AggregateInfo" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Level" use="required"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Overall"/&amp;gt;
 *                       &amp;lt;enumeration value="Category"/&amp;gt;
 *                       &amp;lt;enumeration value="Question"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="AggregationRef" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerTypeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DateAggregation"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Month"/&amp;gt;
 *                       &amp;lt;enumeration value="Quarter"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregateInfoType", propOrder = {
    "aggregateInfo"
})
public class AggregateInfoType {

    @XmlElement(name = "AggregateInfo", required = true)
    protected List<AggregateInfoType.AggregateInfo> aggregateInfo;

    /**
     * Gets the value of the aggregateInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the aggregateInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAggregateInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AggregateInfoType.AggregateInfo }
     * 
     * 
     */
    public List<AggregateInfoType.AggregateInfo> getAggregateInfo() {
        if (aggregateInfo == null) {
            aggregateInfo = new ArrayList<AggregateInfoType.AggregateInfo>();
        }
        return this.aggregateInfo;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Level" use="required"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Overall"/&amp;gt;
     *             &amp;lt;enumeration value="Category"/&amp;gt;
     *             &amp;lt;enumeration value="Question"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="AggregationRef" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="TravelerTypeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DateAggregation"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Month"/&amp;gt;
     *             &amp;lt;enumeration value="Quarter"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AggregateInfo {

        @XmlAttribute(name = "Level", required = true)
        protected String level;
        @XmlAttribute(name = "AggregationRef")
        protected String aggregationRef;
        @XmlAttribute(name = "TravelerTypeInd")
        protected Boolean travelerTypeInd;
        @XmlAttribute(name = "DateAggregation")
        protected String dateAggregation;

        /**
         * Obtiene el valor de la propiedad level.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLevel() {
            return level;
        }

        /**
         * Define el valor de la propiedad level.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLevel(String value) {
            this.level = value;
        }

        /**
         * Obtiene el valor de la propiedad aggregationRef.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAggregationRef() {
            return aggregationRef;
        }

        /**
         * Define el valor de la propiedad aggregationRef.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAggregationRef(String value) {
            this.aggregationRef = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerTypeInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isTravelerTypeInd() {
            return travelerTypeInd;
        }

        /**
         * Define el valor de la propiedad travelerTypeInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setTravelerTypeInd(Boolean value) {
            this.travelerTypeInd = value;
        }

        /**
         * Obtiene el valor de la propiedad dateAggregation.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDateAggregation() {
            return dateAggregation;
        }

        /**
         * Define el valor de la propiedad dateAggregation.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDateAggregation(String value) {
            this.dateAggregation = value;
        }

    }

}
