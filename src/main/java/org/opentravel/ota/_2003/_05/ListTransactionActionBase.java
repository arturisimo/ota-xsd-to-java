
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_TransactionAction_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_TransactionAction_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Book"/&amp;gt;
 *     &amp;lt;enumeration value="Cancel"/&amp;gt;
 *     &amp;lt;enumeration value="Commit"/&amp;gt;
 *     &amp;lt;enumeration value="CommitOverrideEdits"/&amp;gt;
 *     &amp;lt;enumeration value="Hold"/&amp;gt;
 *     &amp;lt;enumeration value="Initiate"/&amp;gt;
 *     &amp;lt;enumeration value="Ignore"/&amp;gt;
 *     &amp;lt;enumeration value="Modify"/&amp;gt;
 *     &amp;lt;enumeration value="Ticket"/&amp;gt;
 *     &amp;lt;enumeration value="Quote"/&amp;gt;
 *     &amp;lt;enumeration value="VerifyPrice"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_TransactionAction_Base")
@XmlEnum
public enum ListTransactionActionBase {

    @XmlEnumValue("Book")
    BOOK("Book"),
    @XmlEnumValue("Cancel")
    CANCEL("Cancel"),
    @XmlEnumValue("Commit")
    COMMIT("Commit"),

    /**
     * Commit the transaction and override the end transaction edits.
     * 
     */
    @XmlEnumValue("CommitOverrideEdits")
    COMMIT_OVERRIDE_EDITS("CommitOverrideEdits"),
    @XmlEnumValue("Hold")
    HOLD("Hold"),
    @XmlEnumValue("Initiate")
    INITIATE("Initiate"),
    @XmlEnumValue("Ignore")
    IGNORE("Ignore"),
    @XmlEnumValue("Modify")
    MODIFY("Modify"),

    /**
     * A ticket for an event, such as a show or theme park.
     * 
     */
    @XmlEnumValue("Ticket")
    TICKET("Ticket"),
    @XmlEnumValue("Quote")
    QUOTE("Quote"),

    /**
     * Perform a price verification.
     * 
     */
    @XmlEnumValue("VerifyPrice")
    VERIFY_PRICE("VerifyPrice"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListTransactionActionBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListTransactionActionBase fromValue(String v) {
        for (ListTransactionActionBase c: ListTransactionActionBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
