
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para FareAccessPrefType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="FareAccessPrefType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="PointToPoint"/&amp;gt;
 *     &amp;lt;enumeration value="Through"/&amp;gt;
 *     &amp;lt;enumeration value="Joint"/&amp;gt;
 *     &amp;lt;enumeration value="Private"/&amp;gt;
 *     &amp;lt;enumeration value="Negotiated"/&amp;gt;
 *     &amp;lt;enumeration value="Net"/&amp;gt;
 *     &amp;lt;enumeration value="Historical"/&amp;gt;
 *     &amp;lt;enumeration value="SecurateAir"/&amp;gt;
 *     &amp;lt;enumeration value="Moneysaver"/&amp;gt;
 *     &amp;lt;enumeration value="MoneysaverRoundtrip"/&amp;gt;
 *     &amp;lt;enumeration value="MoneysaverNoOneWay"/&amp;gt;
 *     &amp;lt;enumeration value="MoneysaverOneWayOnly"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "FareAccessPrefType")
@XmlEnum
public enum FareAccessPrefType {

    @XmlEnumValue("PointToPoint")
    POINT_TO_POINT("PointToPoint"),
    @XmlEnumValue("Through")
    THROUGH("Through"),
    @XmlEnumValue("Joint")
    JOINT("Joint"),
    @XmlEnumValue("Private")
    PRIVATE("Private"),
    @XmlEnumValue("Negotiated")
    NEGOTIATED("Negotiated"),
    @XmlEnumValue("Net")
    NET("Net"),

    /**
     * To request ATPCO historical fare/rule information.
     * 
     */
    @XmlEnumValue("Historical")
    HISTORICAL("Historical"),

    /**
     * To request fares for a specified agreement.
     * 
     */
    @XmlEnumValue("SecurateAir")
    SECURATE_AIR("SecurateAir"),

    /**
     * To request all airline fares for the specified city pair, lowest to highest.
     * 
     */
    @XmlEnumValue("Moneysaver")
    MONEYSAVER("Moneysaver"),

    /**
     * All roundtrip airline fares for the specified city pair including one way fares.
     * 
     */
    @XmlEnumValue("MoneysaverRoundtrip")
    MONEYSAVER_ROUNDTRIP("MoneysaverRoundtrip"),

    /**
     * All airline fares for the specified city pair but no one way fares.
     * 
     */
    @XmlEnumValue("MoneysaverNoOneWay")
    MONEYSAVER_NO_ONE_WAY("MoneysaverNoOneWay"),

    /**
     * Only one-way fares for all airlines for the specified city pair.
     * 
     */
    @XmlEnumValue("MoneysaverOneWayOnly")
    MONEYSAVER_ONE_WAY_ONLY("MoneysaverOneWayOnly");
    private final String value;

    FareAccessPrefType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FareAccessPrefType fromValue(String v) {
        for (FareAccessPrefType c: FareAccessPrefType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
