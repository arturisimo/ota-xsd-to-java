
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Rules for this priced option.
 * 
 * &lt;p&gt;Clase Java para FareInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FareInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FareReference" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;StringLength0to8"&amp;gt;
 *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                 &amp;lt;attribute name="TicketDesignatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RuleInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
 *                 &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
 *                 &amp;lt;attribute name="MoneySaverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FilingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="DepartureAirport" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ArrivalAirport" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Date" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="Type"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="LastRuleChange"/&amp;gt;
 *                       &amp;lt;enumeration value="RuleBecomesInvalid"/&amp;gt;
 *                       &amp;lt;enumeration value="RestrictiveFareEffective"/&amp;gt;
 *                       &amp;lt;enumeration value="RestrictiveFareDiscontinue"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FareInfo" maxOccurs="15" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Date" maxOccurs="5" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="Type"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="EffectiveTravel"/&amp;gt;
 *                                 &amp;lt;enumeration value="DiscontinueTravel"/&amp;gt;
 *                                 &amp;lt;enumeration value="FirstTicketing"/&amp;gt;
 *                                 &amp;lt;enumeration value="LastTicketing"/&amp;gt;
 *                                 &amp;lt;enumeration value="TravelCompletion"/&amp;gt;
 *                                 &amp;lt;enumeration value="Historic"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Fare" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="BaseAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="TotalFare" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="FareDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PTC" maxOccurs="5" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="GlobalIndicatorCode" type="{http://www.opentravel.org/OTA/2003/05}GlobalIndicatorType" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumPermittedMileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                 &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
 *                 &amp;lt;attribute name="FareType" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to3" /&amp;gt;
 *                 &amp;lt;attribute name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}FareStatusType" /&amp;gt;
 *                 &amp;lt;attribute name="Operation" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DiscountPricing" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DiscountPricingGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="City" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Airport" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}NegotiatedFareAttributes"/&amp;gt;
 *       &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *       &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="RoutingNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
 *       &amp;lt;attribute name="NbrOfCities" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareInfoType", propOrder = {
    "departureDate",
    "fareReference",
    "ruleInfo",
    "filingAirline",
    "marketingAirline",
    "departureAirport",
    "arrivalAirport",
    "date",
    "fareInfo",
    "discountPricing",
    "city",
    "airport"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirBaggageRQ.FareInfo.FareDetail.class,
    org.opentravel.ota._2003._05.OTAAirBaggageRQ.PricingInfo.NegotiatedFare.class,
    org.opentravel.ota._2003._05.OTAAirBaggageRS.AllowanceAndCharge.OriginDestination.Service.Pricing.PricingQualifier.NegotiatedFare.class,
    org.opentravel.ota._2003._05.OTAAirBaggageRS.AllowanceAndCharge.TotalPrice.PricingQualifier.NegotiatedFare.class,
    org.opentravel.ota._2003._05.OTAAirRulesRQ.RuleReqInfo.class,
    org.opentravel.ota._2003._05.PTCFareBreakdownType.FareInfo.class,
    org.opentravel.ota._2003._05.FareRuleResponseInfoType.FareRuleInfo.class,
    org.opentravel.ota._2003._05.AirItineraryPricingInfoType.FareInfos.FareInfo.class
})
public class FareInfoType {

    @XmlElement(name = "DepartureDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar departureDate;
    @XmlElement(name = "FareReference")
    protected List<FareInfoType.FareReference> fareReference;
    @XmlElement(name = "RuleInfo")
    protected FareInfoType.RuleInfo ruleInfo;
    @XmlElement(name = "FilingAirline")
    protected CompanyNameType filingAirline;
    @XmlElement(name = "MarketingAirline")
    protected List<CompanyNameType> marketingAirline;
    @XmlElement(name = "DepartureAirport")
    protected LocationType departureAirport;
    @XmlElement(name = "ArrivalAirport")
    protected LocationType arrivalAirport;
    @XmlElement(name = "Date")
    protected List<FareInfoType.Date> date;
    @XmlElement(name = "FareInfo")
    protected List<FareInfoType.FareInfo> fareInfo;
    @XmlElement(name = "DiscountPricing")
    protected FareInfoType.DiscountPricing discountPricing;
    @XmlElement(name = "City")
    protected List<FareInfoType.City> city;
    @XmlElement(name = "Airport")
    protected List<FareInfoType.Airport> airport;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "TariffNumber")
    protected String tariffNumber;
    @XmlAttribute(name = "RuleNumber")
    protected String ruleNumber;
    @XmlAttribute(name = "RoutingNumber")
    protected Integer routingNumber;
    @XmlAttribute(name = "NbrOfCities")
    protected Integer nbrOfCities;
    @XmlAttribute(name = "NegotiatedFareInd")
    protected Boolean negotiatedFareInd;
    @XmlAttribute(name = "NegotiatedFareCode")
    protected String negotiatedFareCode;
    @XmlAttribute(name = "ATPCO_NegCategoryCode")
    protected String atpcoNegCategoryCode;

    /**
     * Obtiene el valor de la propiedad departureDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureDate() {
        return departureDate;
    }

    /**
     * Define el valor de la propiedad departureDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureDate(XMLGregorianCalendar value) {
        this.departureDate = value;
    }

    /**
     * Gets the value of the fareReference property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareReference property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFareReference().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfoType.FareReference }
     * 
     * 
     */
    public List<FareInfoType.FareReference> getFareReference() {
        if (fareReference == null) {
            fareReference = new ArrayList<FareInfoType.FareReference>();
        }
        return this.fareReference;
    }

    /**
     * Obtiene el valor de la propiedad ruleInfo.
     * 
     * @return
     *     possible object is
     *     {@link FareInfoType.RuleInfo }
     *     
     */
    public FareInfoType.RuleInfo getRuleInfo() {
        return ruleInfo;
    }

    /**
     * Define el valor de la propiedad ruleInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link FareInfoType.RuleInfo }
     *     
     */
    public void setRuleInfo(FareInfoType.RuleInfo value) {
        this.ruleInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad filingAirline.
     * 
     * @return
     *     possible object is
     *     {@link CompanyNameType }
     *     
     */
    public CompanyNameType getFilingAirline() {
        return filingAirline;
    }

    /**
     * Define el valor de la propiedad filingAirline.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyNameType }
     *     
     */
    public void setFilingAirline(CompanyNameType value) {
        this.filingAirline = value;
    }

    /**
     * Gets the value of the marketingAirline property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingAirline property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getMarketingAirline().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CompanyNameType }
     * 
     * 
     */
    public List<CompanyNameType> getMarketingAirline() {
        if (marketingAirline == null) {
            marketingAirline = new ArrayList<CompanyNameType>();
        }
        return this.marketingAirline;
    }

    /**
     * Obtiene el valor de la propiedad departureAirport.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Define el valor de la propiedad departureAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setDepartureAirport(LocationType value) {
        this.departureAirport = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivalAirport.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Define el valor de la propiedad arrivalAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setArrivalAirport(LocationType value) {
        this.arrivalAirport = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the date property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDate().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfoType.Date }
     * 
     * 
     */
    public List<FareInfoType.Date> getDate() {
        if (date == null) {
            date = new ArrayList<FareInfoType.Date>();
        }
        return this.date;
    }

    /**
     * Gets the value of the fareInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFareInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfoType.FareInfo }
     * 
     * 
     */
    public List<FareInfoType.FareInfo> getFareInfo() {
        if (fareInfo == null) {
            fareInfo = new ArrayList<FareInfoType.FareInfo>();
        }
        return this.fareInfo;
    }

    /**
     * Obtiene el valor de la propiedad discountPricing.
     * 
     * @return
     *     possible object is
     *     {@link FareInfoType.DiscountPricing }
     *     
     */
    public FareInfoType.DiscountPricing getDiscountPricing() {
        return discountPricing;
    }

    /**
     * Define el valor de la propiedad discountPricing.
     * 
     * @param value
     *     allowed object is
     *     {@link FareInfoType.DiscountPricing }
     *     
     */
    public void setDiscountPricing(FareInfoType.DiscountPricing value) {
        this.discountPricing = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the city property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCity().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfoType.City }
     * 
     * 
     */
    public List<FareInfoType.City> getCity() {
        if (city == null) {
            city = new ArrayList<FareInfoType.City>();
        }
        return this.city;
    }

    /**
     * Gets the value of the airport property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airport property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAirport().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfoType.Airport }
     * 
     * 
     */
    public List<FareInfoType.Airport> getAirport() {
        if (airport == null) {
            airport = new ArrayList<FareInfoType.Airport>();
        }
        return this.airport;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad tariffNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffNumber() {
        return tariffNumber;
    }

    /**
     * Define el valor de la propiedad tariffNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffNumber(String value) {
        this.tariffNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad ruleNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleNumber() {
        return ruleNumber;
    }

    /**
     * Define el valor de la propiedad ruleNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleNumber(String value) {
        this.ruleNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad routingNumber.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRoutingNumber() {
        return routingNumber;
    }

    /**
     * Define el valor de la propiedad routingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRoutingNumber(Integer value) {
        this.routingNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad nbrOfCities.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNbrOfCities() {
        return nbrOfCities;
    }

    /**
     * Define el valor de la propiedad nbrOfCities.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNbrOfCities(Integer value) {
        this.nbrOfCities = value;
    }

    /**
     * Obtiene el valor de la propiedad negotiatedFareInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNegotiatedFareInd() {
        return negotiatedFareInd;
    }

    /**
     * Define el valor de la propiedad negotiatedFareInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNegotiatedFareInd(Boolean value) {
        this.negotiatedFareInd = value;
    }

    /**
     * Obtiene el valor de la propiedad negotiatedFareCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegotiatedFareCode() {
        return negotiatedFareCode;
    }

    /**
     * Define el valor de la propiedad negotiatedFareCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegotiatedFareCode(String value) {
        this.negotiatedFareCode = value;
    }

    /**
     * Obtiene el valor de la propiedad atpcoNegCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getATPCONegCategoryCode() {
        return atpcoNegCategoryCode;
    }

    /**
     * Define el valor de la propiedad atpcoNegCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setATPCONegCategoryCode(String value) {
        this.atpcoNegCategoryCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Airport {

        @XmlAttribute(name = "LocationCode")
        protected String locationCode;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;

        /**
         * Obtiene el valor de la propiedad locationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocationCode() {
            return locationCode;
        }

        /**
         * Define el valor de la propiedad locationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocationCode(String value) {
            this.locationCode = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class City {

        @XmlAttribute(name = "LocationCode")
        protected String locationCode;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;

        /**
         * Obtiene el valor de la propiedad locationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocationCode() {
            return locationCode;
        }

        /**
         * Define el valor de la propiedad locationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocationCode(String value) {
            this.locationCode = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="Type"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="LastRuleChange"/&amp;gt;
     *             &amp;lt;enumeration value="RuleBecomesInvalid"/&amp;gt;
     *             &amp;lt;enumeration value="RestrictiveFareEffective"/&amp;gt;
     *             &amp;lt;enumeration value="RestrictiveFareDiscontinue"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Date {

        @XmlAttribute(name = "Date")
        protected String date;
        @XmlAttribute(name = "Type")
        protected String type;

        /**
         * Obtiene el valor de la propiedad date.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDate() {
            return date;
        }

        /**
         * Define el valor de la propiedad date.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDate(String value) {
            this.date = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DiscountPricingGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DiscountPricing {

        @XmlAttribute(name = "Purpose")
        protected String purpose;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "Usage")
        protected String usage;
        @XmlAttribute(name = "Discount")
        protected String discount;
        @XmlAttribute(name = "TicketDesignatorCode")
        protected String ticketDesignatorCode;
        @XmlAttribute(name = "Text")
        protected String text;

        /**
         * Obtiene el valor de la propiedad purpose.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPurpose() {
            return purpose;
        }

        /**
         * Define el valor de la propiedad purpose.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPurpose(String value) {
            this.purpose = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad usage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUsage() {
            return usage;
        }

        /**
         * Define el valor de la propiedad usage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUsage(String value) {
            this.usage = value;
        }

        /**
         * Obtiene el valor de la propiedad discount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDiscount() {
            return discount;
        }

        /**
         * Define el valor de la propiedad discount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDiscount(String value) {
            this.discount = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketDesignatorCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketDesignatorCode() {
            return ticketDesignatorCode;
        }

        /**
         * Define el valor de la propiedad ticketDesignatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketDesignatorCode(String value) {
            this.ticketDesignatorCode = value;
        }

        /**
         * Obtiene el valor de la propiedad text.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getText() {
            return text;
        }

        /**
         * Define el valor de la propiedad text.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setText(String value) {
            this.text = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Date" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="Type"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="EffectiveTravel"/&amp;gt;
     *                       &amp;lt;enumeration value="DiscontinueTravel"/&amp;gt;
     *                       &amp;lt;enumeration value="FirstTicketing"/&amp;gt;
     *                       &amp;lt;enumeration value="LastTicketing"/&amp;gt;
     *                       &amp;lt;enumeration value="TravelCompletion"/&amp;gt;
     *                       &amp;lt;enumeration value="Historic"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Fare" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="BaseAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TotalFare" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="FareDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PTC" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="GlobalIndicatorCode" type="{http://www.opentravel.org/OTA/2003/05}GlobalIndicatorType" /&amp;gt;
     *       &amp;lt;attribute name="MaximumPermittedMileage" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *       &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
     *       &amp;lt;attribute name="FareType" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to3" /&amp;gt;
     *       &amp;lt;attribute name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}FareStatusType" /&amp;gt;
     *       &amp;lt;attribute name="Operation" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "date",
        "fare",
        "ptc"
    })
    public static class FareInfo {

        @XmlElement(name = "Date")
        protected List<FareInfoType.FareInfo.Date> date;
        @XmlElement(name = "Fare")
        protected FareInfoType.FareInfo.Fare fare;
        @XmlElement(name = "PTC")
        protected List<FareInfoType.FareInfo.PTC> ptc;
        @XmlAttribute(name = "FareBasisCode")
        protected String fareBasisCode;
        @XmlAttribute(name = "GlobalIndicatorCode")
        protected GlobalIndicatorType globalIndicatorCode;
        @XmlAttribute(name = "MaximumPermittedMileage")
        protected BigInteger maximumPermittedMileage;
        @XmlAttribute(name = "TripType")
        protected AirTripType tripType;
        @XmlAttribute(name = "FareType")
        protected String fareType;
        @XmlAttribute(name = "FareStatus")
        protected FareStatusType fareStatus;
        @XmlAttribute(name = "Operation")
        protected ActionType operation;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Gets the value of the date property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the date property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDate().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FareInfoType.FareInfo.Date }
         * 
         * 
         */
        public List<FareInfoType.FareInfo.Date> getDate() {
            if (date == null) {
                date = new ArrayList<FareInfoType.FareInfo.Date>();
            }
            return this.date;
        }

        /**
         * Obtiene el valor de la propiedad fare.
         * 
         * @return
         *     possible object is
         *     {@link FareInfoType.FareInfo.Fare }
         *     
         */
        public FareInfoType.FareInfo.Fare getFare() {
            return fare;
        }

        /**
         * Define el valor de la propiedad fare.
         * 
         * @param value
         *     allowed object is
         *     {@link FareInfoType.FareInfo.Fare }
         *     
         */
        public void setFare(FareInfoType.FareInfo.Fare value) {
            this.fare = value;
        }

        /**
         * Gets the value of the ptc property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ptc property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPTC().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FareInfoType.FareInfo.PTC }
         * 
         * 
         */
        public List<FareInfoType.FareInfo.PTC> getPTC() {
            if (ptc == null) {
                ptc = new ArrayList<FareInfoType.FareInfo.PTC>();
            }
            return this.ptc;
        }

        /**
         * Obtiene el valor de la propiedad fareBasisCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareBasisCode() {
            return fareBasisCode;
        }

        /**
         * Define el valor de la propiedad fareBasisCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareBasisCode(String value) {
            this.fareBasisCode = value;
        }

        /**
         * Obtiene el valor de la propiedad globalIndicatorCode.
         * 
         * @return
         *     possible object is
         *     {@link GlobalIndicatorType }
         *     
         */
        public GlobalIndicatorType getGlobalIndicatorCode() {
            return globalIndicatorCode;
        }

        /**
         * Define el valor de la propiedad globalIndicatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link GlobalIndicatorType }
         *     
         */
        public void setGlobalIndicatorCode(GlobalIndicatorType value) {
            this.globalIndicatorCode = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumPermittedMileage.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaximumPermittedMileage() {
            return maximumPermittedMileage;
        }

        /**
         * Define el valor de la propiedad maximumPermittedMileage.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaximumPermittedMileage(BigInteger value) {
            this.maximumPermittedMileage = value;
        }

        /**
         * Obtiene el valor de la propiedad tripType.
         * 
         * @return
         *     possible object is
         *     {@link AirTripType }
         *     
         */
        public AirTripType getTripType() {
            return tripType;
        }

        /**
         * Define el valor de la propiedad tripType.
         * 
         * @param value
         *     allowed object is
         *     {@link AirTripType }
         *     
         */
        public void setTripType(AirTripType value) {
            this.tripType = value;
        }

        /**
         * Obtiene el valor de la propiedad fareType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareType() {
            return fareType;
        }

        /**
         * Define el valor de la propiedad fareType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareType(String value) {
            this.fareType = value;
        }

        /**
         * Obtiene el valor de la propiedad fareStatus.
         * 
         * @return
         *     possible object is
         *     {@link FareStatusType }
         *     
         */
        public FareStatusType getFareStatus() {
            return fareStatus;
        }

        /**
         * Define el valor de la propiedad fareStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link FareStatusType }
         *     
         */
        public void setFareStatus(FareStatusType value) {
            this.fareStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad operation.
         * 
         * @return
         *     possible object is
         *     {@link ActionType }
         *     
         */
        public ActionType getOperation() {
            return operation;
        }

        /**
         * Define el valor de la propiedad operation.
         * 
         * @param value
         *     allowed object is
         *     {@link ActionType }
         *     
         */
        public void setOperation(ActionType value) {
            this.operation = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="Date" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="Type"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="EffectiveTravel"/&amp;gt;
         *             &amp;lt;enumeration value="DiscontinueTravel"/&amp;gt;
         *             &amp;lt;enumeration value="FirstTicketing"/&amp;gt;
         *             &amp;lt;enumeration value="LastTicketing"/&amp;gt;
         *             &amp;lt;enumeration value="TravelCompletion"/&amp;gt;
         *             &amp;lt;enumeration value="Historic"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Date {

            @XmlAttribute(name = "Date")
            protected String date;
            @XmlAttribute(name = "Type")
            protected String type;

            /**
             * Obtiene el valor de la propiedad date.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDate() {
                return date;
            }

            /**
             * Define el valor de la propiedad date.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDate(String value) {
                this.date = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="BaseAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TotalFare" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="FareDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to64" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Fare {

            @XmlAttribute(name = "BaseAmount")
            protected BigDecimal baseAmount;
            @XmlAttribute(name = "BaseNUC_Amount")
            protected BigDecimal baseNUCAmount;
            @XmlAttribute(name = "TaxAmount")
            protected BigDecimal taxAmount;
            @XmlAttribute(name = "TotalFare")
            protected BigDecimal totalFare;
            @XmlAttribute(name = "FareDescription")
            protected String fareDescription;

            /**
             * Obtiene el valor de la propiedad baseAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBaseAmount() {
                return baseAmount;
            }

            /**
             * Define el valor de la propiedad baseAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBaseAmount(BigDecimal value) {
                this.baseAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad baseNUCAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBaseNUCAmount() {
                return baseNUCAmount;
            }

            /**
             * Define el valor de la propiedad baseNUCAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBaseNUCAmount(BigDecimal value) {
                this.baseNUCAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad taxAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTaxAmount() {
                return taxAmount;
            }

            /**
             * Define el valor de la propiedad taxAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTaxAmount(BigDecimal value) {
                this.taxAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad totalFare.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalFare() {
                return totalFare;
            }

            /**
             * Define el valor de la propiedad totalFare.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalFare(BigDecimal value) {
                this.totalFare = value;
            }

            /**
             * Obtiene el valor de la propiedad fareDescription.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareDescription() {
                return fareDescription;
            }

            /**
             * Define el valor de la propiedad fareDescription.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareDescription(String value) {
                this.fareDescription = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PTC {

            @XmlAttribute(name = "PassengerTypeCode")
            protected String passengerTypeCode;

            /**
             * Obtiene el valor de la propiedad passengerTypeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassengerTypeCode() {
                return passengerTypeCode;
            }

            /**
             * Define el valor de la propiedad passengerTypeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassengerTypeCode(String value) {
                this.passengerTypeCode = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;StringLength0to8"&amp;gt;
     *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *       &amp;lt;attribute name="TicketDesignatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class FareReference {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "ResBookDesigCode")
        protected String resBookDesigCode;
        @XmlAttribute(name = "TicketDesignatorCode")
        protected String ticketDesignatorCode;
        @XmlAttribute(name = "AccountCode")
        protected String accountCode;

        /**
         * Used for Character Strings, length 0 to 8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad resBookDesigCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResBookDesigCode() {
            return resBookDesigCode;
        }

        /**
         * Define el valor de la propiedad resBookDesigCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResBookDesigCode(String value) {
            this.resBookDesigCode = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketDesignatorCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketDesignatorCode() {
            return ticketDesignatorCode;
        }

        /**
         * Define el valor de la propiedad ticketDesignatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketDesignatorCode(String value) {
            this.ticketDesignatorCode = value;
        }

        /**
         * Obtiene el valor de la propiedad accountCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountCode() {
            return accountCode;
        }

        /**
         * Define el valor de la propiedad accountCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountCode(String value) {
            this.accountCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
     *       &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
     *       &amp;lt;attribute name="MoneySaverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RuleInfo
        extends RuleInfoType
    {

        @XmlAttribute(name = "TripType")
        protected AirTripType tripType;
        @XmlAttribute(name = "MoneySaverInd")
        protected Boolean moneySaverInd;

        /**
         * Obtiene el valor de la propiedad tripType.
         * 
         * @return
         *     possible object is
         *     {@link AirTripType }
         *     
         */
        public AirTripType getTripType() {
            return tripType;
        }

        /**
         * Define el valor de la propiedad tripType.
         * 
         * @param value
         *     allowed object is
         *     {@link AirTripType }
         *     
         */
        public void setTripType(AirTripType value) {
            this.tripType = value;
        }

        /**
         * Obtiene el valor de la propiedad moneySaverInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMoneySaverInd() {
            return moneySaverInd;
        }

        /**
         * Define el valor de la propiedad moneySaverInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMoneySaverInd(Boolean value) {
            this.moneySaverInd = value;
        }

    }

}
