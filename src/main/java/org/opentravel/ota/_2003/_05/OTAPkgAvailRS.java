
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="Package" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="CompanyName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="DateRange" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="PriceInfo" maxOccurs="5" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PkgPriceType"&amp;gt;
 *                             &amp;lt;attribute name="OtherPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                             &amp;lt;attribute name="OtherPriceDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="ItineraryItems" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="ItineraryItem" type="{http://www.opentravel.org/OTA/2003/05}ItineraryItemResponseType" maxOccurs="99"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Cautions" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="99"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_OptionalGroup"/&amp;gt;
 *                   &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                   &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
 *                   &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                   &amp;lt;attribute name="ShortDescription" type="{http://www.opentravel.org/OTA/2003/05}ShortDescriptionType" /&amp;gt;
 *                   &amp;lt;attribute name="BoardCode" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
 *                   &amp;lt;attribute name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                   &amp;lt;attribute name="BrandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                   &amp;lt;attribute name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                   &amp;lt;attribute name="ExtrasInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="TravelChoices" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="TravelItem" maxOccurs="99"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;choice&amp;gt;
 *                                 &amp;lt;element name="TravelDetail" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="OutwardTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5"/&amp;gt;
 *                                           &amp;lt;element name="ReturnTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="TravelJourney" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PkgTravelJourneyGroup"/&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/choice&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="SupplementCharges" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="SupplementCharge" type="{http://www.opentravel.org/OTA/2003/05}FeeType" maxOccurs="9"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="Cautions" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="9"/&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="AccomOKFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="AccommodationChoices" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="AvailableProperty" type="{http://www.opentravel.org/OTA/2003/05}AccommodationDetailType" maxOccurs="99"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "_package",
    "travelChoices",
    "accommodationChoices",
    "tpaExtensions",
    "errors"
})
@XmlRootElement(name = "OTA_PkgAvailRS")
public class OTAPkgAvailRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "Package")
    protected OTAPkgAvailRS.Package _package;
    @XmlElement(name = "TravelChoices")
    protected OTAPkgAvailRS.TravelChoices travelChoices;
    @XmlElement(name = "AccommodationChoices")
    protected OTAPkgAvailRS.AccommodationChoices accommodationChoices;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "DecimalPlaces")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger decimalPlaces;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "CompanyShortName")
    protected String companyShortName;
    @XmlAttribute(name = "TravelSector")
    protected String travelSector;
    @XmlAttribute(name = "Code")
    protected String code;
    @XmlAttribute(name = "CodeContext")
    protected String codeContext;
    @XmlAttribute(name = "CountryCode")
    protected String countryCode;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad package.
     * 
     * @return
     *     possible object is
     *     {@link OTAPkgAvailRS.Package }
     *     
     */
    public OTAPkgAvailRS.Package getPackage() {
        return _package;
    }

    /**
     * Define el valor de la propiedad package.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAPkgAvailRS.Package }
     *     
     */
    public void setPackage(OTAPkgAvailRS.Package value) {
        this._package = value;
    }

    /**
     * Obtiene el valor de la propiedad travelChoices.
     * 
     * @return
     *     possible object is
     *     {@link OTAPkgAvailRS.TravelChoices }
     *     
     */
    public OTAPkgAvailRS.TravelChoices getTravelChoices() {
        return travelChoices;
    }

    /**
     * Define el valor de la propiedad travelChoices.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAPkgAvailRS.TravelChoices }
     *     
     */
    public void setTravelChoices(OTAPkgAvailRS.TravelChoices value) {
        this.travelChoices = value;
    }

    /**
     * Obtiene el valor de la propiedad accommodationChoices.
     * 
     * @return
     *     possible object is
     *     {@link OTAPkgAvailRS.AccommodationChoices }
     *     
     */
    public OTAPkgAvailRS.AccommodationChoices getAccommodationChoices() {
        return accommodationChoices;
    }

    /**
     * Define el valor de la propiedad accommodationChoices.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAPkgAvailRS.AccommodationChoices }
     *     
     */
    public void setAccommodationChoices(OTAPkgAvailRS.AccommodationChoices value) {
        this.accommodationChoices = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad decimalPlaces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * Define el valor de la propiedad decimalPlaces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDecimalPlaces(BigInteger value) {
        this.decimalPlaces = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad companyShortName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyShortName() {
        return companyShortName;
    }

    /**
     * Define el valor de la propiedad companyShortName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyShortName(String value) {
        this.companyShortName = value;
    }

    /**
     * Obtiene el valor de la propiedad travelSector.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelSector() {
        return travelSector;
    }

    /**
     * Define el valor de la propiedad travelSector.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelSector(String value) {
        this.travelSector = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad codeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeContext() {
        return codeContext;
    }

    /**
     * Define el valor de la propiedad codeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeContext(String value) {
        this.codeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AvailableProperty" type="{http://www.opentravel.org/OTA/2003/05}AccommodationDetailType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "availableProperty"
    })
    public static class AccommodationChoices {

        @XmlElement(name = "AvailableProperty", required = true)
        protected List<AccommodationDetailType> availableProperty;

        /**
         * Gets the value of the availableProperty property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the availableProperty property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAvailableProperty().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AccommodationDetailType }
         * 
         * 
         */
        public List<AccommodationDetailType> getAvailableProperty() {
            if (availableProperty == null) {
                availableProperty = new ArrayList<AccommodationDetailType>();
            }
            return this.availableProperty;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="URL" type="{http://www.opentravel.org/OTA/2003/05}URL_Type" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CompanyName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="DateRange" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PriceInfo" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PkgPriceType"&amp;gt;
     *                 &amp;lt;attribute name="OtherPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="OtherPriceDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ItineraryItems" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ItineraryItem" type="{http://www.opentravel.org/OTA/2003/05}ItineraryItemResponseType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Cautions" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ID_OptionalGroup"/&amp;gt;
     *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="TravelCode" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelCode" /&amp;gt;
     *       &amp;lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ShortDescription" type="{http://www.opentravel.org/OTA/2003/05}ShortDescriptionType" /&amp;gt;
     *       &amp;lt;attribute name="BoardCode" type="{http://www.opentravel.org/OTA/2003/05}MealPlanCode" /&amp;gt;
     *       &amp;lt;attribute name="PromotionCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="BrandCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ExtrasInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "url",
        "companyName",
        "dateRange",
        "priceInfo",
        "itineraryItems",
        "cautions"
    })
    public static class Package {

        @XmlElement(name = "URL")
        protected URLType url;
        @XmlElement(name = "CompanyName")
        protected CompanyNameType companyName;
        @XmlElement(name = "DateRange")
        protected OTAPkgAvailRS.Package.DateRange dateRange;
        @XmlElement(name = "PriceInfo")
        protected List<OTAPkgAvailRS.Package.PriceInfo> priceInfo;
        @XmlElement(name = "ItineraryItems")
        protected OTAPkgAvailRS.Package.ItineraryItems itineraryItems;
        @XmlElement(name = "Cautions")
        protected OTAPkgAvailRS.Package.Cautions cautions;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "TravelCode")
        protected String travelCode;
        @XmlAttribute(name = "TourCode")
        protected String tourCode;
        @XmlAttribute(name = "ShortDescription")
        protected String shortDescription;
        @XmlAttribute(name = "BoardCode")
        protected String boardCode;
        @XmlAttribute(name = "PromotionCode")
        protected String promotionCode;
        @XmlAttribute(name = "BrandCode")
        protected String brandCode;
        @XmlAttribute(name = "ProductCode")
        protected String productCode;
        @XmlAttribute(name = "ExtrasInd")
        protected Boolean extrasInd;
        @XmlAttribute(name = "ID")
        protected String id;

        /**
         * Obtiene el valor de la propiedad url.
         * 
         * @return
         *     possible object is
         *     {@link URLType }
         *     
         */
        public URLType getURL() {
            return url;
        }

        /**
         * Define el valor de la propiedad url.
         * 
         * @param value
         *     allowed object is
         *     {@link URLType }
         *     
         */
        public void setURL(URLType value) {
            this.url = value;
        }

        /**
         * Obtiene el valor de la propiedad companyName.
         * 
         * @return
         *     possible object is
         *     {@link CompanyNameType }
         *     
         */
        public CompanyNameType getCompanyName() {
            return companyName;
        }

        /**
         * Define el valor de la propiedad companyName.
         * 
         * @param value
         *     allowed object is
         *     {@link CompanyNameType }
         *     
         */
        public void setCompanyName(CompanyNameType value) {
            this.companyName = value;
        }

        /**
         * Obtiene el valor de la propiedad dateRange.
         * 
         * @return
         *     possible object is
         *     {@link OTAPkgAvailRS.Package.DateRange }
         *     
         */
        public OTAPkgAvailRS.Package.DateRange getDateRange() {
            return dateRange;
        }

        /**
         * Define el valor de la propiedad dateRange.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAPkgAvailRS.Package.DateRange }
         *     
         */
        public void setDateRange(OTAPkgAvailRS.Package.DateRange value) {
            this.dateRange = value;
        }

        /**
         * Gets the value of the priceInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the priceInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPriceInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAPkgAvailRS.Package.PriceInfo }
         * 
         * 
         */
        public List<OTAPkgAvailRS.Package.PriceInfo> getPriceInfo() {
            if (priceInfo == null) {
                priceInfo = new ArrayList<OTAPkgAvailRS.Package.PriceInfo>();
            }
            return this.priceInfo;
        }

        /**
         * Obtiene el valor de la propiedad itineraryItems.
         * 
         * @return
         *     possible object is
         *     {@link OTAPkgAvailRS.Package.ItineraryItems }
         *     
         */
        public OTAPkgAvailRS.Package.ItineraryItems getItineraryItems() {
            return itineraryItems;
        }

        /**
         * Define el valor de la propiedad itineraryItems.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAPkgAvailRS.Package.ItineraryItems }
         *     
         */
        public void setItineraryItems(OTAPkgAvailRS.Package.ItineraryItems value) {
            this.itineraryItems = value;
        }

        /**
         * Obtiene el valor de la propiedad cautions.
         * 
         * @return
         *     possible object is
         *     {@link OTAPkgAvailRS.Package.Cautions }
         *     
         */
        public OTAPkgAvailRS.Package.Cautions getCautions() {
            return cautions;
        }

        /**
         * Define el valor de la propiedad cautions.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAPkgAvailRS.Package.Cautions }
         *     
         */
        public void setCautions(OTAPkgAvailRS.Package.Cautions value) {
            this.cautions = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad travelCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelCode() {
            return travelCode;
        }

        /**
         * Define el valor de la propiedad travelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelCode(String value) {
            this.travelCode = value;
        }

        /**
         * Obtiene el valor de la propiedad tourCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTourCode() {
            return tourCode;
        }

        /**
         * Define el valor de la propiedad tourCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTourCode(String value) {
            this.tourCode = value;
        }

        /**
         * Obtiene el valor de la propiedad shortDescription.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShortDescription() {
            return shortDescription;
        }

        /**
         * Define el valor de la propiedad shortDescription.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShortDescription(String value) {
            this.shortDescription = value;
        }

        /**
         * Obtiene el valor de la propiedad boardCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBoardCode() {
            return boardCode;
        }

        /**
         * Define el valor de la propiedad boardCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBoardCode(String value) {
            this.boardCode = value;
        }

        /**
         * Obtiene el valor de la propiedad promotionCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPromotionCode() {
            return promotionCode;
        }

        /**
         * Define el valor de la propiedad promotionCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPromotionCode(String value) {
            this.promotionCode = value;
        }

        /**
         * Obtiene el valor de la propiedad brandCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandCode() {
            return brandCode;
        }

        /**
         * Define el valor de la propiedad brandCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandCode(String value) {
            this.brandCode = value;
        }

        /**
         * Obtiene el valor de la propiedad productCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductCode() {
            return productCode;
        }

        /**
         * Define el valor de la propiedad productCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductCode(String value) {
            this.productCode = value;
        }

        /**
         * Obtiene el valor de la propiedad extrasInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExtrasInd() {
            return extrasInd;
        }

        /**
         * Define el valor de la propiedad extrasInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExtrasInd(Boolean value) {
            this.extrasInd = value;
        }

        /**
         * Obtiene el valor de la propiedad id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getID() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setID(String value) {
            this.id = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "caution"
        })
        public static class Cautions {

            @XmlElement(name = "Caution", required = true)
            protected List<PkgCautionType> caution;

            /**
             * Gets the value of the caution property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the caution property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCaution().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PkgCautionType }
             * 
             * 
             */
            public List<PkgCautionType> getCaution() {
                if (caution == null) {
                    caution = new ArrayList<PkgCautionType>();
                }
                return this.caution;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class DateRange {

            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ItineraryItem" type="{http://www.opentravel.org/OTA/2003/05}ItineraryItemResponseType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "itineraryItem"
        })
        public static class ItineraryItems {

            @XmlElement(name = "ItineraryItem", required = true)
            protected List<ItineraryItemResponseType> itineraryItem;

            /**
             * Gets the value of the itineraryItem property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the itineraryItem property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getItineraryItem().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ItineraryItemResponseType }
             * 
             * 
             */
            public List<ItineraryItemResponseType> getItineraryItem() {
                if (itineraryItem == null) {
                    itineraryItem = new ArrayList<ItineraryItemResponseType>();
                }
                return this.itineraryItem;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PkgPriceType"&amp;gt;
         *       &amp;lt;attribute name="OtherPrice" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="OtherPriceDescription" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PriceInfo
            extends PkgPriceType
        {

            @XmlAttribute(name = "OtherPrice")
            protected BigDecimal otherPrice;
            @XmlAttribute(name = "OtherPriceDescription")
            protected String otherPriceDescription;

            /**
             * Obtiene el valor de la propiedad otherPrice.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getOtherPrice() {
                return otherPrice;
            }

            /**
             * Define el valor de la propiedad otherPrice.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setOtherPrice(BigDecimal value) {
                this.otherPrice = value;
            }

            /**
             * Obtiene el valor de la propiedad otherPriceDescription.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherPriceDescription() {
                return otherPriceDescription;
            }

            /**
             * Define el valor de la propiedad otherPriceDescription.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherPriceDescription(String value) {
                this.otherPriceDescription = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="TravelItem" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;choice&amp;gt;
     *                     &amp;lt;element name="TravelDetail" maxOccurs="9" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                             &amp;lt;sequence&amp;gt;
     *                               &amp;lt;element name="OutwardTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5"/&amp;gt;
     *                               &amp;lt;element name="ReturnTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                             &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;/restriction&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                     &amp;lt;element name="TravelJourney" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PkgTravelJourneyGroup"/&amp;gt;
     *                           &amp;lt;/restriction&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                   &amp;lt;/choice&amp;gt;
     *                   &amp;lt;sequence&amp;gt;
     *                     &amp;lt;element name="SupplementCharges" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                             &amp;lt;sequence&amp;gt;
     *                               &amp;lt;element name="SupplementCharge" type="{http://www.opentravel.org/OTA/2003/05}FeeType" maxOccurs="9"/&amp;gt;
     *                             &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;/restriction&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                     &amp;lt;element name="Cautions" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                             &amp;lt;sequence&amp;gt;
     *                               &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="9"/&amp;gt;
     *                             &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;/restriction&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                   &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="AccomOKFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "travelItem"
    })
    public static class TravelChoices {

        @XmlElement(name = "TravelItem", required = true)
        protected List<OTAPkgAvailRS.TravelChoices.TravelItem> travelItem;

        /**
         * Gets the value of the travelItem property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelItem property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTravelItem().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAPkgAvailRS.TravelChoices.TravelItem }
         * 
         * 
         */
        public List<OTAPkgAvailRS.TravelChoices.TravelItem> getTravelItem() {
            if (travelItem == null) {
                travelItem = new ArrayList<OTAPkgAvailRS.TravelChoices.TravelItem>();
            }
            return this.travelItem;
        }


        /**
         * Details of a travel component e.g. a set of travel legs making up a travel component or a single item defining a round-trip journey.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;choice&amp;gt;
         *           &amp;lt;element name="TravelDetail" maxOccurs="9" minOccurs="0"&amp;gt;
         *             &amp;lt;complexType&amp;gt;
         *               &amp;lt;complexContent&amp;gt;
         *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                   &amp;lt;sequence&amp;gt;
         *                     &amp;lt;element name="OutwardTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5"/&amp;gt;
         *                     &amp;lt;element name="ReturnTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5" minOccurs="0"/&amp;gt;
         *                   &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;/restriction&amp;gt;
         *               &amp;lt;/complexContent&amp;gt;
         *             &amp;lt;/complexType&amp;gt;
         *           &amp;lt;/element&amp;gt;
         *           &amp;lt;element name="TravelJourney" minOccurs="0"&amp;gt;
         *             &amp;lt;complexType&amp;gt;
         *               &amp;lt;complexContent&amp;gt;
         *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PkgTravelJourneyGroup"/&amp;gt;
         *                 &amp;lt;/restriction&amp;gt;
         *               &amp;lt;/complexContent&amp;gt;
         *             &amp;lt;/complexType&amp;gt;
         *           &amp;lt;/element&amp;gt;
         *         &amp;lt;/choice&amp;gt;
         *         &amp;lt;sequence&amp;gt;
         *           &amp;lt;element name="SupplementCharges" minOccurs="0"&amp;gt;
         *             &amp;lt;complexType&amp;gt;
         *               &amp;lt;complexContent&amp;gt;
         *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                   &amp;lt;sequence&amp;gt;
         *                     &amp;lt;element name="SupplementCharge" type="{http://www.opentravel.org/OTA/2003/05}FeeType" maxOccurs="9"/&amp;gt;
         *                   &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;/restriction&amp;gt;
         *               &amp;lt;/complexContent&amp;gt;
         *             &amp;lt;/complexType&amp;gt;
         *           &amp;lt;/element&amp;gt;
         *           &amp;lt;element name="Cautions" minOccurs="0"&amp;gt;
         *             &amp;lt;complexType&amp;gt;
         *               &amp;lt;complexContent&amp;gt;
         *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                   &amp;lt;sequence&amp;gt;
         *                     &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="9"/&amp;gt;
         *                   &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;/restriction&amp;gt;
         *               &amp;lt;/complexContent&amp;gt;
         *             &amp;lt;/complexType&amp;gt;
         *           &amp;lt;/element&amp;gt;
         *         &amp;lt;/sequence&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="AccomOKFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "travelDetail",
            "travelJourney",
            "supplementCharges",
            "cautions"
        })
        public static class TravelItem {

            @XmlElement(name = "TravelDetail")
            protected List<OTAPkgAvailRS.TravelChoices.TravelItem.TravelDetail> travelDetail;
            @XmlElement(name = "TravelJourney")
            protected OTAPkgAvailRS.TravelChoices.TravelItem.TravelJourney travelJourney;
            @XmlElement(name = "SupplementCharges")
            protected OTAPkgAvailRS.TravelChoices.TravelItem.SupplementCharges supplementCharges;
            @XmlElement(name = "Cautions")
            protected OTAPkgAvailRS.TravelChoices.TravelItem.Cautions cautions;
            @XmlAttribute(name = "AccomOKFlag")
            protected Boolean accomOKFlag;

            /**
             * Gets the value of the travelDetail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelDetail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTravelDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAPkgAvailRS.TravelChoices.TravelItem.TravelDetail }
             * 
             * 
             */
            public List<OTAPkgAvailRS.TravelChoices.TravelItem.TravelDetail> getTravelDetail() {
                if (travelDetail == null) {
                    travelDetail = new ArrayList<OTAPkgAvailRS.TravelChoices.TravelItem.TravelDetail>();
                }
                return this.travelDetail;
            }

            /**
             * Obtiene el valor de la propiedad travelJourney.
             * 
             * @return
             *     possible object is
             *     {@link OTAPkgAvailRS.TravelChoices.TravelItem.TravelJourney }
             *     
             */
            public OTAPkgAvailRS.TravelChoices.TravelItem.TravelJourney getTravelJourney() {
                return travelJourney;
            }

            /**
             * Define el valor de la propiedad travelJourney.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAPkgAvailRS.TravelChoices.TravelItem.TravelJourney }
             *     
             */
            public void setTravelJourney(OTAPkgAvailRS.TravelChoices.TravelItem.TravelJourney value) {
                this.travelJourney = value;
            }

            /**
             * Obtiene el valor de la propiedad supplementCharges.
             * 
             * @return
             *     possible object is
             *     {@link OTAPkgAvailRS.TravelChoices.TravelItem.SupplementCharges }
             *     
             */
            public OTAPkgAvailRS.TravelChoices.TravelItem.SupplementCharges getSupplementCharges() {
                return supplementCharges;
            }

            /**
             * Define el valor de la propiedad supplementCharges.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAPkgAvailRS.TravelChoices.TravelItem.SupplementCharges }
             *     
             */
            public void setSupplementCharges(OTAPkgAvailRS.TravelChoices.TravelItem.SupplementCharges value) {
                this.supplementCharges = value;
            }

            /**
             * Obtiene el valor de la propiedad cautions.
             * 
             * @return
             *     possible object is
             *     {@link OTAPkgAvailRS.TravelChoices.TravelItem.Cautions }
             *     
             */
            public OTAPkgAvailRS.TravelChoices.TravelItem.Cautions getCautions() {
                return cautions;
            }

            /**
             * Define el valor de la propiedad cautions.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAPkgAvailRS.TravelChoices.TravelItem.Cautions }
             *     
             */
            public void setCautions(OTAPkgAvailRS.TravelChoices.TravelItem.Cautions value) {
                this.cautions = value;
            }

            /**
             * Obtiene el valor de la propiedad accomOKFlag.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAccomOKFlag() {
                return accomOKFlag;
            }

            /**
             * Define el valor de la propiedad accomOKFlag.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAccomOKFlag(Boolean value) {
                this.accomOKFlag = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Caution" type="{http://www.opentravel.org/OTA/2003/05}PkgCautionType" maxOccurs="9"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "caution"
            })
            public static class Cautions {

                @XmlElement(name = "Caution", required = true)
                protected List<PkgCautionType> caution;

                /**
                 * Gets the value of the caution property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the caution property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getCaution().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PkgCautionType }
                 * 
                 * 
                 */
                public List<PkgCautionType> getCaution() {
                    if (caution == null) {
                        caution = new ArrayList<PkgCautionType>();
                    }
                    return this.caution;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="SupplementCharge" type="{http://www.opentravel.org/OTA/2003/05}FeeType" maxOccurs="9"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "supplementCharge"
            })
            public static class SupplementCharges {

                @XmlElement(name = "SupplementCharge", required = true)
                protected List<FeeType> supplementCharge;

                /**
                 * Gets the value of the supplementCharge property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the supplementCharge property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getSupplementCharge().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link FeeType }
                 * 
                 * 
                 */
                public List<FeeType> getSupplementCharge() {
                    if (supplementCharge == null) {
                        supplementCharge = new ArrayList<FeeType>();
                    }
                    return this.supplementCharge;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="OutwardTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5"/&amp;gt;
             *         &amp;lt;element name="ReturnTravel" type="{http://www.opentravel.org/OTA/2003/05}PkgTravelSegment" maxOccurs="5" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "outwardTravel",
                "returnTravel"
            })
            public static class TravelDetail {

                @XmlElement(name = "OutwardTravel", required = true)
                protected List<PkgTravelSegment> outwardTravel;
                @XmlElement(name = "ReturnTravel")
                protected List<PkgTravelSegment> returnTravel;

                /**
                 * Gets the value of the outwardTravel property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the outwardTravel property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getOutwardTravel().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PkgTravelSegment }
                 * 
                 * 
                 */
                public List<PkgTravelSegment> getOutwardTravel() {
                    if (outwardTravel == null) {
                        outwardTravel = new ArrayList<PkgTravelSegment>();
                    }
                    return this.outwardTravel;
                }

                /**
                 * Gets the value of the returnTravel property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the returnTravel property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getReturnTravel().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PkgTravelSegment }
                 * 
                 * 
                 */
                public List<PkgTravelSegment> getReturnTravel() {
                    if (returnTravel == null) {
                        returnTravel = new ArrayList<PkgTravelSegment>();
                    }
                    return this.returnTravel;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PkgTravelJourneyGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class TravelJourney {

                @XmlAttribute(name = "Type")
                protected String type;
                @XmlAttribute(name = "OriginAirportID", required = true)
                protected String originAirportID;
                @XmlAttribute(name = "DestinationAirportID", required = true)
                protected String destinationAirportID;
                @XmlAttribute(name = "OutwardDateTime", required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar outwardDateTime;
                @XmlAttribute(name = "ReturnDateTime")
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar returnDateTime;
                @XmlAttribute(name = "TravelCode")
                protected String travelCode;
                @XmlAttribute(name = "CompanyShortName")
                protected String companyShortName;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad originAirportID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginAirportID() {
                    return originAirportID;
                }

                /**
                 * Define el valor de la propiedad originAirportID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginAirportID(String value) {
                    this.originAirportID = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationAirportID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationAirportID() {
                    return destinationAirportID;
                }

                /**
                 * Define el valor de la propiedad destinationAirportID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationAirportID(String value) {
                    this.destinationAirportID = value;
                }

                /**
                 * Obtiene el valor de la propiedad outwardDateTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getOutwardDateTime() {
                    return outwardDateTime;
                }

                /**
                 * Define el valor de la propiedad outwardDateTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setOutwardDateTime(XMLGregorianCalendar value) {
                    this.outwardDateTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad returnDateTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getReturnDateTime() {
                    return returnDateTime;
                }

                /**
                 * Define el valor de la propiedad returnDateTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setReturnDateTime(XMLGregorianCalendar value) {
                    this.returnDateTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelCode() {
                    return travelCode;
                }

                /**
                 * Define el valor de la propiedad travelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelCode(String value) {
                    this.travelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad companyShortName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCompanyShortName() {
                    return companyShortName;
                }

                /**
                 * Define el valor de la propiedad companyShortName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCompanyShortName(String value) {
                    this.companyShortName = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

            }

        }

    }

}
