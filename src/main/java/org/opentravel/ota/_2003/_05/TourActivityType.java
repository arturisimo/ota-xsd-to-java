
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Tour or activity type definition that supports open enumerations. Use the Other_  option to specify an activity type that is not in the list and has been agreed upon by trading partners.
 * 
 * &lt;p&gt;Clase Java para TourActivityType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityTypeEnum"&amp;gt;
 *       &amp;lt;attribute name="extension" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityType", propOrder = {
    "value"
})
public class TourActivityType {

    @XmlValue
    protected TourActivityTypeEnum value;
    @XmlAttribute(name = "extension")
    protected String extension;

    /**
     * Tour and activity types enum with an "Other" value to support an open enumeration list as agreed upon between trading partners.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityTypeEnum }
     *     
     */
    public TourActivityTypeEnum getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityTypeEnum }
     *     
     */
    public void setValue(TourActivityTypeEnum value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

}
