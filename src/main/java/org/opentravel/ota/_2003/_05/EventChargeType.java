
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides details for specific event charges.
 * 
 * &lt;p&gt;Clase Java para EventChargeType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="EventChargeType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="EventCharge" maxOccurs="999"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *                 &amp;lt;attribute name="EventCharge" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="PrimaryChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="SecondaryChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="AppliedPercentageEventCharge" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="MinChargeAmount" type="{http://www.opentravel.org/OTA/2003/05}MoneyOrPercentageType" /&amp;gt;
 *                 &amp;lt;attribute name="MaxChargeAmount" type="{http://www.opentravel.org/OTA/2003/05}MoneyOrPercentageType" /&amp;gt;
 *                 &amp;lt;attribute name="AverageChargeAmount" type="{http://www.opentravel.org/OTA/2003/05}MoneyOrPercentageType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventChargeType", propOrder = {
    "eventCharge",
    "comment"
})
public class EventChargeType {

    @XmlElement(name = "EventCharge", required = true)
    protected List<EventChargeType.EventCharge> eventCharge;
    @XmlElement(name = "Comment")
    protected List<ParagraphType> comment;

    /**
     * Gets the value of the eventCharge property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventCharge property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getEventCharge().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EventChargeType.EventCharge }
     * 
     * 
     */
    public List<EventChargeType.EventCharge> getEventCharge() {
        if (eventCharge == null) {
            eventCharge = new ArrayList<EventChargeType.EventCharge>();
        }
        return this.eventCharge;
    }

    /**
     * Gets the value of the comment property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getComment().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ParagraphType }
     * 
     * 
     */
    public List<ParagraphType> getComment() {
        if (comment == null) {
            comment = new ArrayList<ParagraphType>();
        }
        return this.comment;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
     *       &amp;lt;attribute name="EventCharge" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="PrimaryChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="SecondaryChargeType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="AppliedPercentageEventCharge" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="MinChargeAmount" type="{http://www.opentravel.org/OTA/2003/05}MoneyOrPercentageType" /&amp;gt;
     *       &amp;lt;attribute name="MaxChargeAmount" type="{http://www.opentravel.org/OTA/2003/05}MoneyOrPercentageType" /&amp;gt;
     *       &amp;lt;attribute name="AverageChargeAmount" type="{http://www.opentravel.org/OTA/2003/05}MoneyOrPercentageType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxes"
    })
    public static class EventCharge {

        @XmlElement(name = "Taxes", required = true)
        protected TaxesType taxes;
        @XmlAttribute(name = "EventCharge")
        protected String eventCharge;
        @XmlAttribute(name = "PrimaryChargeType")
        protected String primaryChargeType;
        @XmlAttribute(name = "SecondaryChargeType")
        protected String secondaryChargeType;
        @XmlAttribute(name = "AppliedPercentageEventCharge")
        protected String appliedPercentageEventCharge;
        @XmlAttribute(name = "MinChargeAmount")
        protected String minChargeAmount;
        @XmlAttribute(name = "MaxChargeAmount")
        protected String maxChargeAmount;
        @XmlAttribute(name = "AverageChargeAmount")
        protected String averageChargeAmount;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;

        /**
         * Obtiene el valor de la propiedad taxes.
         * 
         * @return
         *     possible object is
         *     {@link TaxesType }
         *     
         */
        public TaxesType getTaxes() {
            return taxes;
        }

        /**
         * Define el valor de la propiedad taxes.
         * 
         * @param value
         *     allowed object is
         *     {@link TaxesType }
         *     
         */
        public void setTaxes(TaxesType value) {
            this.taxes = value;
        }

        /**
         * Obtiene el valor de la propiedad eventCharge.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventCharge() {
            return eventCharge;
        }

        /**
         * Define el valor de la propiedad eventCharge.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventCharge(String value) {
            this.eventCharge = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryChargeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryChargeType() {
            return primaryChargeType;
        }

        /**
         * Define el valor de la propiedad primaryChargeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryChargeType(String value) {
            this.primaryChargeType = value;
        }

        /**
         * Obtiene el valor de la propiedad secondaryChargeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSecondaryChargeType() {
            return secondaryChargeType;
        }

        /**
         * Define el valor de la propiedad secondaryChargeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSecondaryChargeType(String value) {
            this.secondaryChargeType = value;
        }

        /**
         * Obtiene el valor de la propiedad appliedPercentageEventCharge.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAppliedPercentageEventCharge() {
            return appliedPercentageEventCharge;
        }

        /**
         * Define el valor de la propiedad appliedPercentageEventCharge.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAppliedPercentageEventCharge(String value) {
            this.appliedPercentageEventCharge = value;
        }

        /**
         * Obtiene el valor de la propiedad minChargeAmount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMinChargeAmount() {
            return minChargeAmount;
        }

        /**
         * Define el valor de la propiedad minChargeAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMinChargeAmount(String value) {
            this.minChargeAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad maxChargeAmount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaxChargeAmount() {
            return maxChargeAmount;
        }

        /**
         * Define el valor de la propiedad maxChargeAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaxChargeAmount(String value) {
            this.maxChargeAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad averageChargeAmount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAverageChargeAmount() {
            return averageChargeAmount;
        }

        /**
         * Define el valor de la propiedad averageChargeAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAverageChargeAmount(String value) {
            this.averageChargeAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

    }

}
