
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para FareAmountType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="FareAmountType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NOADC"/&amp;gt;
 *     &amp;lt;enumeration value="Bulk"/&amp;gt;
 *     &amp;lt;enumeration value="IT"/&amp;gt;
 *     &amp;lt;enumeration value="Additional_Collection"/&amp;gt;
 *     &amp;lt;enumeration value="Charter_Fare"/&amp;gt;
 *     &amp;lt;enumeration value="No_Fare"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "FareAmountType")
@XmlEnum
public enum FareAmountType {

    NOADC("NOADC"),
    @XmlEnumValue("Bulk")
    BULK("Bulk"),
    IT("IT"),

    /**
     * This amount represents an additional collection.
     * 
     */
    @XmlEnumValue("Additional_Collection")
    ADDITIONAL_COLLECTION("Additional_Collection"),

    /**
     * This amount represents a fare for a charter flight.
     * 
     */
    @XmlEnumValue("Charter_Fare")
    CHARTER_FARE("Charter_Fare"),

    /**
     * No fare applies.
     * 
     */
    @XmlEnumValue("No_Fare")
    NO_FARE("No_Fare");
    private final String value;

    FareAmountType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FareAmountType fromValue(String v) {
        for (FareAmountType c: FareAmountType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
