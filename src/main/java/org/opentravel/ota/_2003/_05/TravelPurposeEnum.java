
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para TravelPurposeEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="TravelPurposeEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NotSignificant"/&amp;gt;
 *     &amp;lt;enumeration value="Business"/&amp;gt;
 *     &amp;lt;enumeration value="Personal"/&amp;gt;
 *     &amp;lt;enumeration value="Group"/&amp;gt;
 *     &amp;lt;enumeration value="Conference"/&amp;gt;
 *     &amp;lt;enumeration value="Consortiums"/&amp;gt;
 *     &amp;lt;enumeration value="HomeVisiting"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "TravelPurposeEnum")
@XmlEnum
public enum TravelPurposeEnum {

    @XmlEnumValue("NotSignificant")
    NOT_SIGNIFICANT("NotSignificant"),
    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("Personal")
    PERSONAL("Personal"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Conference")
    CONFERENCE("Conference"),
    @XmlEnumValue("Consortiums")
    CONSORTIUMS("Consortiums"),
    @XmlEnumValue("HomeVisiting")
    HOME_VISITING("HomeVisiting"),
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    TravelPurposeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TravelPurposeEnum fromValue(String v) {
        for (TravelPurposeEnum c: TravelPurposeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
