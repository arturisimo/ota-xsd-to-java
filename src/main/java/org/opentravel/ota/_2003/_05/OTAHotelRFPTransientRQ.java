
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TransientRFP_RequestSegments"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RequestedInfos" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="RequestedInfo" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="SpecificRequests" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="SpecificRequest" maxOccurs="unbounded"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
 *                                                         &amp;lt;attribute name="MandatoryIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="RequestedInfoCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="SingleFileIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TransientRFP_RequestSegment" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ContactInfos" type="{http://www.opentravel.org/OTA/2003/05}ContactInfosType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="HotelAddress" type="{http://www.opentravel.org/OTA/2003/05}AddressPrefType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="AccountSpecificInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="HotelPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="CityPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="AdditionalHotelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *                           &amp;lt;attribute name="IntermediaryPropertyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comment" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
 *                                     &amp;lt;attribute name="FormatType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="ResponseMethod" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="ResponseFormat" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="ResponseDueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="DecisionDueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="ContractDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                 &amp;lt;attribute name="ContractStart" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MessageID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "transientRFPRequestSegments",
    "messageID"
})
@XmlRootElement(name = "OTA_HotelRFP_TransientRQ")
public class OTAHotelRFPTransientRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "TransientRFP_RequestSegments", required = true)
    protected OTAHotelRFPTransientRQ.TransientRFPRequestSegments transientRFPRequestSegments;
    @XmlElement(name = "MessageID")
    protected UniqueIDType messageID;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad transientRFPRequestSegments.
     * 
     * @return
     *     possible object is
     *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments }
     *     
     */
    public OTAHotelRFPTransientRQ.TransientRFPRequestSegments getTransientRFPRequestSegments() {
        return transientRFPRequestSegments;
    }

    /**
     * Define el valor de la propiedad transientRFPRequestSegments.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments }
     *     
     */
    public void setTransientRFPRequestSegments(OTAHotelRFPTransientRQ.TransientRFPRequestSegments value) {
        this.transientRFPRequestSegments = value;
    }

    /**
     * Obtiene el valor de la propiedad messageID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getMessageID() {
        return messageID;
    }

    /**
     * Define el valor de la propiedad messageID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setMessageID(UniqueIDType value) {
        this.messageID = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RequestedInfos" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RequestedInfo" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="SpecificRequests" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="SpecificRequest" maxOccurs="unbounded"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
     *                                               &amp;lt;attribute name="MandatoryIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="RequestedInfoCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="SingleFileIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TransientRFP_RequestSegment" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ContactInfos" type="{http://www.opentravel.org/OTA/2003/05}ContactInfosType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="HotelAddress" type="{http://www.opentravel.org/OTA/2003/05}AddressPrefType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="AccountSpecificInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="HotelPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="CityPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="AdditionalHotelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *                 &amp;lt;attribute name="IntermediaryPropertyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comment" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
     *                           &amp;lt;attribute name="FormatType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="ResponseMethod" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="ResponseFormat" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="ResponseDueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="DecisionDueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="ContractDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *       &amp;lt;attribute name="ContractStart" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "requestedInfos",
        "transientRFPRequestSegment",
        "comments"
    })
    public static class TransientRFPRequestSegments {

        @XmlElement(name = "RequestedInfos")
        protected OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos requestedInfos;
        @XmlElement(name = "TransientRFP_RequestSegment")
        protected List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment> transientRFPRequestSegment;
        @XmlElement(name = "Comments")
        protected OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments comments;
        @XmlAttribute(name = "ResponseMethod")
        protected String responseMethod;
        @XmlAttribute(name = "ResponseFormat")
        protected String responseFormat;
        @XmlAttribute(name = "ResponseDueDate")
        protected String responseDueDate;
        @XmlAttribute(name = "DecisionDueDate")
        protected String decisionDueDate;
        @XmlAttribute(name = "ContractDuration")
        protected Duration contractDuration;
        @XmlAttribute(name = "ContractStart")
        protected String contractStart;

        /**
         * Obtiene el valor de la propiedad requestedInfos.
         * 
         * @return
         *     possible object is
         *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos }
         *     
         */
        public OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos getRequestedInfos() {
            return requestedInfos;
        }

        /**
         * Define el valor de la propiedad requestedInfos.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos }
         *     
         */
        public void setRequestedInfos(OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos value) {
            this.requestedInfos = value;
        }

        /**
         * Gets the value of the transientRFPRequestSegment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the transientRFPRequestSegment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTransientRFPRequestSegment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment }
         * 
         * 
         */
        public List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment> getTransientRFPRequestSegment() {
            if (transientRFPRequestSegment == null) {
                transientRFPRequestSegment = new ArrayList<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment>();
            }
            return this.transientRFPRequestSegment;
        }

        /**
         * Obtiene el valor de la propiedad comments.
         * 
         * @return
         *     possible object is
         *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments }
         *     
         */
        public OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments getComments() {
            return comments;
        }

        /**
         * Define el valor de la propiedad comments.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments }
         *     
         */
        public void setComments(OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments value) {
            this.comments = value;
        }

        /**
         * Obtiene el valor de la propiedad responseMethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseMethod() {
            return responseMethod;
        }

        /**
         * Define el valor de la propiedad responseMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseMethod(String value) {
            this.responseMethod = value;
        }

        /**
         * Obtiene el valor de la propiedad responseFormat.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseFormat() {
            return responseFormat;
        }

        /**
         * Define el valor de la propiedad responseFormat.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseFormat(String value) {
            this.responseFormat = value;
        }

        /**
         * Obtiene el valor de la propiedad responseDueDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseDueDate() {
            return responseDueDate;
        }

        /**
         * Define el valor de la propiedad responseDueDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseDueDate(String value) {
            this.responseDueDate = value;
        }

        /**
         * Obtiene el valor de la propiedad decisionDueDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDecisionDueDate() {
            return decisionDueDate;
        }

        /**
         * Define el valor de la propiedad decisionDueDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDecisionDueDate(String value) {
            this.decisionDueDate = value;
        }

        /**
         * Obtiene el valor de la propiedad contractDuration.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getContractDuration() {
            return contractDuration;
        }

        /**
         * Define el valor de la propiedad contractDuration.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setContractDuration(Duration value) {
            this.contractDuration = value;
        }

        /**
         * Obtiene el valor de la propiedad contractStart.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractStart() {
            return contractStart;
        }

        /**
         * Define el valor de la propiedad contractStart.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractStart(String value) {
            this.contractStart = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comment" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
         *                 &amp;lt;attribute name="FormatType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comment"
        })
        public static class Comments {

            @XmlElement(name = "Comment", required = true)
            protected List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments.Comment> comment;

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments.Comment }
             * 
             * 
             */
            public List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments.Comment> getComment() {
                if (comment == null) {
                    comment = new ArrayList<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.Comments.Comment>();
                }
                return this.comment;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
             *       &amp;lt;attribute name="FormatType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Comment
                extends ParagraphType
            {

                @XmlAttribute(name = "FormatType")
                protected String formatType;

                /**
                 * Obtiene el valor de la propiedad formatType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFormatType() {
                    return formatType;
                }

                /**
                 * Define el valor de la propiedad formatType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFormatType(String value) {
                    this.formatType = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RequestedInfo" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="SpecificRequests" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="SpecificRequest" maxOccurs="unbounded"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
         *                                     &amp;lt;attribute name="MandatoryIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="RequestedInfoCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="SingleFileIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "requestedInfo"
        })
        public static class RequestedInfos {

            @XmlElement(name = "RequestedInfo", required = true)
            protected List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo> requestedInfo;
            @XmlAttribute(name = "SingleFileIndicator")
            protected Boolean singleFileIndicator;

            /**
             * Gets the value of the requestedInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the requestedInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRequestedInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo }
             * 
             * 
             */
            public List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo> getRequestedInfo() {
                if (requestedInfo == null) {
                    requestedInfo = new ArrayList<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo>();
                }
                return this.requestedInfo;
            }

            /**
             * Obtiene el valor de la propiedad singleFileIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSingleFileIndicator() {
                return singleFileIndicator;
            }

            /**
             * Define el valor de la propiedad singleFileIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSingleFileIndicator(Boolean value) {
                this.singleFileIndicator = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="SpecificRequests" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="SpecificRequest" maxOccurs="unbounded"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
             *                           &amp;lt;attribute name="MandatoryIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="RequestedInfoCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "specificRequests"
            })
            public static class RequestedInfo {

                @XmlElement(name = "SpecificRequests")
                protected OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests specificRequests;
                @XmlAttribute(name = "RequestedInfoCode")
                protected String requestedInfoCode;

                /**
                 * Obtiene el valor de la propiedad specificRequests.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests }
                 *     
                 */
                public OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests getSpecificRequests() {
                    return specificRequests;
                }

                /**
                 * Define el valor de la propiedad specificRequests.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests }
                 *     
                 */
                public void setSpecificRequests(OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests value) {
                    this.specificRequests = value;
                }

                /**
                 * Obtiene el valor de la propiedad requestedInfoCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestedInfoCode() {
                    return requestedInfoCode;
                }

                /**
                 * Define el valor de la propiedad requestedInfoCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestedInfoCode(String value) {
                    this.requestedInfoCode = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="SpecificRequest" maxOccurs="unbounded"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
                 *                 &amp;lt;attribute name="MandatoryIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "specificRequest"
                })
                public static class SpecificRequests {

                    @XmlElement(name = "SpecificRequest", required = true)
                    protected List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests.SpecificRequest> specificRequest;

                    /**
                     * Gets the value of the specificRequest property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the specificRequest property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getSpecificRequest().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests.SpecificRequest }
                     * 
                     * 
                     */
                    public List<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests.SpecificRequest> getSpecificRequest() {
                        if (specificRequest == null) {
                            specificRequest = new ArrayList<OTAHotelRFPTransientRQ.TransientRFPRequestSegments.RequestedInfos.RequestedInfo.SpecificRequests.SpecificRequest>();
                        }
                        return this.specificRequest;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
                     *       &amp;lt;attribute name="MandatoryIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class SpecificRequest
                        extends ParagraphType
                    {

                        @XmlAttribute(name = "MandatoryIndicator")
                        protected Boolean mandatoryIndicator;

                        /**
                         * Obtiene el valor de la propiedad mandatoryIndicator.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isMandatoryIndicator() {
                            return mandatoryIndicator;
                        }

                        /**
                         * Define el valor de la propiedad mandatoryIndicator.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setMandatoryIndicator(Boolean value) {
                            this.mandatoryIndicator = value;
                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ContactInfos" type="{http://www.opentravel.org/OTA/2003/05}ContactInfosType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="HotelAddress" type="{http://www.opentravel.org/OTA/2003/05}AddressPrefType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="AccountSpecificInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="HotelPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="CityPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="AdditionalHotelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
         *       &amp;lt;attribute name="IntermediaryPropertyCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contactInfos",
            "hotelAddress",
            "accountSpecificInfo"
        })
        public static class TransientRFPRequestSegment {

            @XmlElement(name = "ContactInfos")
            protected ContactInfosType contactInfos;
            @XmlElement(name = "HotelAddress")
            protected AddressPrefType hotelAddress;
            @XmlElement(name = "AccountSpecificInfo")
            protected OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment.AccountSpecificInfo accountSpecificInfo;
            @XmlAttribute(name = "IntermediaryPropertyCode")
            protected String intermediaryPropertyCode;
            @XmlAttribute(name = "ChainCode")
            protected String chainCode;
            @XmlAttribute(name = "BrandCode")
            protected String brandCode;
            @XmlAttribute(name = "HotelCode")
            protected String hotelCode;
            @XmlAttribute(name = "HotelCityCode")
            protected String hotelCityCode;
            @XmlAttribute(name = "HotelName")
            protected String hotelName;
            @XmlAttribute(name = "HotelCodeContext")
            protected String hotelCodeContext;
            @XmlAttribute(name = "ChainName")
            protected String chainName;
            @XmlAttribute(name = "BrandName")
            protected String brandName;
            @XmlAttribute(name = "AreaID")
            protected String areaID;
            @XmlAttribute(name = "TTIcode")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger ttIcode;

            /**
             * Obtiene el valor de la propiedad contactInfos.
             * 
             * @return
             *     possible object is
             *     {@link ContactInfosType }
             *     
             */
            public ContactInfosType getContactInfos() {
                return contactInfos;
            }

            /**
             * Define el valor de la propiedad contactInfos.
             * 
             * @param value
             *     allowed object is
             *     {@link ContactInfosType }
             *     
             */
            public void setContactInfos(ContactInfosType value) {
                this.contactInfos = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelAddress.
             * 
             * @return
             *     possible object is
             *     {@link AddressPrefType }
             *     
             */
            public AddressPrefType getHotelAddress() {
                return hotelAddress;
            }

            /**
             * Define el valor de la propiedad hotelAddress.
             * 
             * @param value
             *     allowed object is
             *     {@link AddressPrefType }
             *     
             */
            public void setHotelAddress(AddressPrefType value) {
                this.hotelAddress = value;
            }

            /**
             * Obtiene el valor de la propiedad accountSpecificInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment.AccountSpecificInfo }
             *     
             */
            public OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment.AccountSpecificInfo getAccountSpecificInfo() {
                return accountSpecificInfo;
            }

            /**
             * Define el valor de la propiedad accountSpecificInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment.AccountSpecificInfo }
             *     
             */
            public void setAccountSpecificInfo(OTAHotelRFPTransientRQ.TransientRFPRequestSegments.TransientRFPRequestSegment.AccountSpecificInfo value) {
                this.accountSpecificInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad intermediaryPropertyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIntermediaryPropertyCode() {
                return intermediaryPropertyCode;
            }

            /**
             * Define el valor de la propiedad intermediaryPropertyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIntermediaryPropertyCode(String value) {
                this.intermediaryPropertyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad chainCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainCode() {
                return chainCode;
            }

            /**
             * Define el valor de la propiedad chainCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainCode(String value) {
                this.chainCode = value;
            }

            /**
             * Obtiene el valor de la propiedad brandCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandCode() {
                return brandCode;
            }

            /**
             * Define el valor de la propiedad brandCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandCode(String value) {
                this.brandCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCode() {
                return hotelCode;
            }

            /**
             * Define el valor de la propiedad hotelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCode(String value) {
                this.hotelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCityCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCityCode() {
                return hotelCityCode;
            }

            /**
             * Define el valor de la propiedad hotelCityCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCityCode(String value) {
                this.hotelCityCode = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelName() {
                return hotelName;
            }

            /**
             * Define el valor de la propiedad hotelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelName(String value) {
                this.hotelName = value;
            }

            /**
             * Obtiene el valor de la propiedad hotelCodeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHotelCodeContext() {
                return hotelCodeContext;
            }

            /**
             * Define el valor de la propiedad hotelCodeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHotelCodeContext(String value) {
                this.hotelCodeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad chainName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChainName() {
                return chainName;
            }

            /**
             * Define el valor de la propiedad chainName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChainName(String value) {
                this.chainName = value;
            }

            /**
             * Obtiene el valor de la propiedad brandName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrandName() {
                return brandName;
            }

            /**
             * Define el valor de la propiedad brandName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrandName(String value) {
                this.brandName = value;
            }

            /**
             * Obtiene el valor de la propiedad areaID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAreaID() {
                return areaID;
            }

            /**
             * Define el valor de la propiedad areaID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAreaID(String value) {
                this.areaID = value;
            }

            /**
             * Obtiene el valor de la propiedad ttIcode.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTTIcode() {
                return ttIcode;
            }

            /**
             * Define el valor de la propiedad ttIcode.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTTIcode(BigInteger value) {
                this.ttIcode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="HotelPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="CityPotentialRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="AdditionalHotelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AccountSpecificInfo {

                @XmlAttribute(name = "HotelPotentialRoomQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger hotelPotentialRoomQuantity;
                @XmlAttribute(name = "CityPotentialRoomQuantity")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger cityPotentialRoomQuantity;
                @XmlAttribute(name = "AdditionalHotelIndicator")
                protected Boolean additionalHotelIndicator;

                /**
                 * Obtiene el valor de la propiedad hotelPotentialRoomQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getHotelPotentialRoomQuantity() {
                    return hotelPotentialRoomQuantity;
                }

                /**
                 * Define el valor de la propiedad hotelPotentialRoomQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setHotelPotentialRoomQuantity(BigInteger value) {
                    this.hotelPotentialRoomQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad cityPotentialRoomQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCityPotentialRoomQuantity() {
                    return cityPotentialRoomQuantity;
                }

                /**
                 * Define el valor de la propiedad cityPotentialRoomQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCityPotentialRoomQuantity(BigInteger value) {
                    this.cityPotentialRoomQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad additionalHotelIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAdditionalHotelIndicator() {
                    return additionalHotelIndicator;
                }

                /**
                 * Define el valor de la propiedad additionalHotelIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAdditionalHotelIndicator(Boolean value) {
                    this.additionalHotelIndicator = value;
                }

            }

        }

    }

}
