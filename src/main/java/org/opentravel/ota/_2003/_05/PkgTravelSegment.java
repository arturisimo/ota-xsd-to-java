
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A full definition of a travel segment including supplemental price and facilities.
 * 
 * &lt;p&gt;Clase Java para PkgTravelSegment complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PkgTravelSegment"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AirSegment" type="{http://www.opentravel.org/OTA/2003/05}PkgAirSegmentType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PkgTravelSegment", propOrder = {
    "airSegment"
})
public class PkgTravelSegment {

    @XmlElement(name = "AirSegment", required = true)
    protected PkgAirSegmentType airSegment;

    /**
     * Obtiene el valor de la propiedad airSegment.
     * 
     * @return
     *     possible object is
     *     {@link PkgAirSegmentType }
     *     
     */
    public PkgAirSegmentType getAirSegment() {
        return airSegment;
    }

    /**
     * Define el valor de la propiedad airSegment.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgAirSegmentType }
     *     
     */
    public void setAirSegment(PkgAirSegmentType value) {
        this.airSegment = value;
    }

}
