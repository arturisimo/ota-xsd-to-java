
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The ResGuest object contains the information about a guest associated with a reservation.
 * 
 * &lt;p&gt;Clase Java para DynamicPkgGuestType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DynamicPkgGuestType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResGuestType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="SeatRequests" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SeatRequest" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SeatRequestType"&amp;gt;
 *                           &amp;lt;attribute name="TravelerRefNumberRPHList" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *                           &amp;lt;attribute name="FlightRefNumberRPHList" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *                           &amp;lt;attribute name="PartialSeatingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicPkgGuestType", propOrder = {
    "seatRequests"
})
public class DynamicPkgGuestType
    extends ResGuestType
{

    @XmlElement(name = "SeatRequests")
    protected DynamicPkgGuestType.SeatRequests seatRequests;

    /**
     * Obtiene el valor de la propiedad seatRequests.
     * 
     * @return
     *     possible object is
     *     {@link DynamicPkgGuestType.SeatRequests }
     *     
     */
    public DynamicPkgGuestType.SeatRequests getSeatRequests() {
        return seatRequests;
    }

    /**
     * Define el valor de la propiedad seatRequests.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicPkgGuestType.SeatRequests }
     *     
     */
    public void setSeatRequests(DynamicPkgGuestType.SeatRequests value) {
        this.seatRequests = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SeatRequest" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SeatRequestType"&amp;gt;
     *                 &amp;lt;attribute name="TravelerRefNumberRPHList" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *                 &amp;lt;attribute name="FlightRefNumberRPHList" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *                 &amp;lt;attribute name="PartialSeatingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "seatRequest"
    })
    public static class SeatRequests {

        @XmlElement(name = "SeatRequest", required = true)
        protected List<DynamicPkgGuestType.SeatRequests.SeatRequest> seatRequest;

        /**
         * Gets the value of the seatRequest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the seatRequest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSeatRequest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link DynamicPkgGuestType.SeatRequests.SeatRequest }
         * 
         * 
         */
        public List<DynamicPkgGuestType.SeatRequests.SeatRequest> getSeatRequest() {
            if (seatRequest == null) {
                seatRequest = new ArrayList<DynamicPkgGuestType.SeatRequests.SeatRequest>();
            }
            return this.seatRequest;
        }


        /**
         *  AWG to revisit.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SeatRequestType"&amp;gt;
         *       &amp;lt;attribute name="TravelerRefNumberRPHList" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
         *       &amp;lt;attribute name="FlightRefNumberRPHList" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
         *       &amp;lt;attribute name="PartialSeatingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SeatRequest
            extends SeatRequestType
        {

            @XmlAttribute(name = "TravelerRefNumberRPHList")
            protected List<String> travelerRefNumberRPHList;
            @XmlAttribute(name = "FlightRefNumberRPHList")
            protected List<String> flightRefNumberRPHList;
            @XmlAttribute(name = "PartialSeatingInd")
            protected Boolean partialSeatingInd;

            /**
             * Gets the value of the travelerRefNumberRPHList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelerRefNumberRPHList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTravelerRefNumberRPHList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getTravelerRefNumberRPHList() {
                if (travelerRefNumberRPHList == null) {
                    travelerRefNumberRPHList = new ArrayList<String>();
                }
                return this.travelerRefNumberRPHList;
            }

            /**
             * Gets the value of the flightRefNumberRPHList property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightRefNumberRPHList property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getFlightRefNumberRPHList().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getFlightRefNumberRPHList() {
                if (flightRefNumberRPHList == null) {
                    flightRefNumberRPHList = new ArrayList<String>();
                }
                return this.flightRefNumberRPHList;
            }

            /**
             * Obtiene el valor de la propiedad partialSeatingInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPartialSeatingInd() {
                return partialSeatingInd;
            }

            /**
             * Define el valor de la propiedad partialSeatingInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPartialSeatingInd(Boolean value) {
                this.partialSeatingInd = value;
            }

        }

    }

}
