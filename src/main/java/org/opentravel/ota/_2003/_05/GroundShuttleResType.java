
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Shuttle service reservation information.
 * 
 * &lt;p&gt;Clase Java para GroundShuttleResType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundShuttleResType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ServiceType" type="{http://www.opentravel.org/OTA/2003/05}List_GroundServiceProvided"/&amp;gt;
 *         &amp;lt;element name="ServiceLocation" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
 *                 &amp;lt;attribute name="OriginInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DestinationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="TicketIssueInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundShuttleResType", propOrder = {
    "serviceType",
    "serviceLocation"
})
public class GroundShuttleResType {

    @XmlElement(name = "ServiceType", required = true)
    protected ListGroundServiceProvided serviceType;
    @XmlElement(name = "ServiceLocation")
    protected List<GroundShuttleResType.ServiceLocation> serviceLocation;
    @XmlAttribute(name = "TicketIssueInd")
    protected Boolean ticketIssueInd;

    /**
     * Obtiene el valor de la propiedad serviceType.
     * 
     * @return
     *     possible object is
     *     {@link ListGroundServiceProvided }
     *     
     */
    public ListGroundServiceProvided getServiceType() {
        return serviceType;
    }

    /**
     * Define el valor de la propiedad serviceType.
     * 
     * @param value
     *     allowed object is
     *     {@link ListGroundServiceProvided }
     *     
     */
    public void setServiceType(ListGroundServiceProvided value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the serviceLocation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the serviceLocation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getServiceLocation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GroundShuttleResType.ServiceLocation }
     * 
     * 
     */
    public List<GroundShuttleResType.ServiceLocation> getServiceLocation() {
        if (serviceLocation == null) {
            serviceLocation = new ArrayList<GroundShuttleResType.ServiceLocation>();
        }
        return this.serviceLocation;
    }

    /**
     * Obtiene el valor de la propiedad ticketIssueInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTicketIssueInd() {
        return ticketIssueInd;
    }

    /**
     * Define el valor de la propiedad ticketIssueInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketIssueInd(Boolean value) {
        this.ticketIssueInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
     *       &amp;lt;attribute name="OriginInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DestinationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ServiceLocation
        extends GroundLocationType
    {

        @XmlAttribute(name = "OriginInd")
        protected Boolean originInd;
        @XmlAttribute(name = "DestinationInd")
        protected Boolean destinationInd;

        /**
         * Obtiene el valor de la propiedad originInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOriginInd() {
            return originInd;
        }

        /**
         * Define el valor de la propiedad originInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOriginInd(Boolean value) {
            this.originInd = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDestinationInd() {
            return destinationInd;
        }

        /**
         * Define el valor de la propiedad destinationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDestinationInd(Boolean value) {
            this.destinationInd = value;
        }

    }

}
