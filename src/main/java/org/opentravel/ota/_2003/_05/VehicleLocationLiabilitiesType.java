
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * The Vehicle Location Liabilities Type is used to define information on the financial liabilities assumed by the renter when renting from this facility, along with optional coverage to reduce the financial liabilities. 
 * 
 * &lt;p&gt;Clase Java para VehicleLocationLiabilitiesType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleLocationLiabilitiesType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Coverages" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Coverage" maxOccurs="99" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CoverageInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="CoverageFees" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="CoverageFee" maxOccurs="99"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"/&amp;gt;
 *                                                 &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
 *                                                                   &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
 *                                                                 &amp;lt;/extension&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Deductible" type="{http://www.opentravel.org/OTA/2003/05}DeductibleType" minOccurs="0"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Info" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleLocationLiabilitiesType", propOrder = {
    "coverages",
    "info",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Liabilities.class
})
public class VehicleLocationLiabilitiesType {

    @XmlElement(name = "Coverages")
    protected VehicleLocationLiabilitiesType.Coverages coverages;
    @XmlElement(name = "Info")
    protected FormattedTextType info;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Obtiene el valor de la propiedad coverages.
     * 
     * @return
     *     possible object is
     *     {@link VehicleLocationLiabilitiesType.Coverages }
     *     
     */
    public VehicleLocationLiabilitiesType.Coverages getCoverages() {
        return coverages;
    }

    /**
     * Define el valor de la propiedad coverages.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleLocationLiabilitiesType.Coverages }
     *     
     */
    public void setCoverages(VehicleLocationLiabilitiesType.Coverages value) {
        this.coverages = value;
    }

    /**
     * Obtiene el valor de la propiedad info.
     * 
     * @return
     *     possible object is
     *     {@link FormattedTextType }
     *     
     */
    public FormattedTextType getInfo() {
        return info;
    }

    /**
     * Define el valor de la propiedad info.
     * 
     * @param value
     *     allowed object is
     *     {@link FormattedTextType }
     *     
     */
    public void setInfo(FormattedTextType value) {
        this.info = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Coverage" maxOccurs="99" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CoverageInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="CoverageFees" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="CoverageFee" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"/&amp;gt;
     *                                       &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
     *                                                         &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Deductible" type="{http://www.opentravel.org/OTA/2003/05}DeductibleType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "coverage"
    })
    public static class Coverages {

        @XmlElement(name = "Coverage")
        protected List<VehicleLocationLiabilitiesType.Coverages.Coverage> coverage;

        /**
         * Gets the value of the coverage property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coverage property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCoverage().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleLocationLiabilitiesType.Coverages.Coverage }
         * 
         * 
         */
        public List<VehicleLocationLiabilitiesType.Coverages.Coverage> getCoverage() {
            if (coverage == null) {
                coverage = new ArrayList<VehicleLocationLiabilitiesType.Coverages.Coverage>();
            }
            return this.coverage;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CoverageInfo" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="CoverageFees" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="CoverageFee" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"/&amp;gt;
         *                             &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
         *                                               &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Deductible" type="{http://www.opentravel.org/OTA/2003/05}DeductibleType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "coverageInfo",
            "coverageFees"
        })
        public static class Coverage {

            @XmlElement(name = "CoverageInfo")
            protected FormattedTextType coverageInfo;
            @XmlElement(name = "CoverageFees")
            protected VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees coverageFees;
            @XmlAttribute(name = "Type", required = true)
            protected String type;
            @XmlAttribute(name = "RequiredInd")
            protected Boolean requiredInd;

            /**
             * Obtiene el valor de la propiedad coverageInfo.
             * 
             * @return
             *     possible object is
             *     {@link FormattedTextType }
             *     
             */
            public FormattedTextType getCoverageInfo() {
                return coverageInfo;
            }

            /**
             * Define el valor de la propiedad coverageInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link FormattedTextType }
             *     
             */
            public void setCoverageInfo(FormattedTextType value) {
                this.coverageInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad coverageFees.
             * 
             * @return
             *     possible object is
             *     {@link VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees }
             *     
             */
            public VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees getCoverageFees() {
                return coverageFees;
            }

            /**
             * Define el valor de la propiedad coverageFees.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees }
             *     
             */
            public void setCoverageFees(VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees value) {
                this.coverageFees = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad requiredInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRequiredInd() {
                return requiredInd;
            }

            /**
             * Define el valor de la propiedad requiredInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRequiredInd(Boolean value) {
                this.requiredInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="CoverageFee" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"/&amp;gt;
             *                   &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
             *                                     &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Deductible" type="{http://www.opentravel.org/OTA/2003/05}DeductibleType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "coverageFee"
            })
            public static class CoverageFees {

                @XmlElement(name = "CoverageFee", required = true)
                protected List<VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee> coverageFee;

                /**
                 * Gets the value of the coverageFee property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coverageFee property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getCoverageFee().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee }
                 * 
                 * 
                 */
                public List<VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee> getCoverageFee() {
                    if (coverageFee == null) {
                        coverageFee = new ArrayList<VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee>();
                    }
                    return this.coverageFee;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}VehicleChargeType"/&amp;gt;
                 *         &amp;lt;element name="Vehicles" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
                 *                           &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Deductible" type="{http://www.opentravel.org/OTA/2003/05}DeductibleType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "charge",
                    "vehicles",
                    "deductible"
                })
                public static class CoverageFee {

                    @XmlElement(name = "Charge", required = true)
                    protected VehicleChargeType charge;
                    @XmlElement(name = "Vehicles")
                    protected VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles vehicles;
                    @XmlElement(name = "Deductible")
                    protected DeductibleType deductible;

                    /**
                     * Obtiene el valor de la propiedad charge.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VehicleChargeType }
                     *     
                     */
                    public VehicleChargeType getCharge() {
                        return charge;
                    }

                    /**
                     * Define el valor de la propiedad charge.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VehicleChargeType }
                     *     
                     */
                    public void setCharge(VehicleChargeType value) {
                        this.charge = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad vehicles.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles }
                     *     
                     */
                    public VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles getVehicles() {
                        return vehicles;
                    }

                    /**
                     * Define el valor de la propiedad vehicles.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles }
                     *     
                     */
                    public void setVehicles(VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles value) {
                        this.vehicles = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad deductible.
                     * 
                     * @return
                     *     possible object is
                     *     {@link DeductibleType }
                     *     
                     */
                    public DeductibleType getDeductible() {
                        return deductible;
                    }

                    /**
                     * Define el valor de la propiedad deductible.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link DeductibleType }
                     *     
                     */
                    public void setDeductible(DeductibleType value) {
                        this.deductible = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Vehicle" maxOccurs="99"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
                     *                 &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "vehicle"
                    })
                    public static class Vehicles {

                        @XmlElement(name = "Vehicle", required = true)
                        protected List<VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles.Vehicle> vehicle;

                        /**
                         * Gets the value of the vehicle property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getVehicle().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles.Vehicle }
                         * 
                         * 
                         */
                        public List<VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles.Vehicle> getVehicle() {
                            if (vehicle == null) {
                                vehicle = new ArrayList<VehicleLocationLiabilitiesType.Coverages.Coverage.CoverageFees.CoverageFee.Vehicles.Vehicle>();
                            }
                            return this.vehicle;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
                         *       &amp;lt;attribute name="IncludeExclude" type="{http://www.opentravel.org/OTA/2003/05}IncludeExcludeType" /&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class Vehicle
                            extends VehicleType
                        {

                            @XmlAttribute(name = "IncludeExclude")
                            protected IncludeExcludeType includeExclude;

                            /**
                             * Obtiene el valor de la propiedad includeExclude.
                             * 
                             * @return
                             *     possible object is
                             *     {@link IncludeExcludeType }
                             *     
                             */
                            public IncludeExcludeType getIncludeExclude() {
                                return includeExclude;
                            }

                            /**
                             * Define el valor de la propiedad includeExclude.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link IncludeExcludeType }
                             *     
                             */
                            public void setIncludeExclude(IncludeExcludeType value) {
                                this.includeExclude = value;
                            }

                        }

                    }

                }

            }

        }

    }

}
