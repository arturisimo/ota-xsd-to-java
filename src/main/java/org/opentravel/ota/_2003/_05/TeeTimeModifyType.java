
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Information associated with modifying a booked tee time round reservation.
 * 
 * &lt;p&gt;Clase Java para TeeTimeModifyType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TeeTimeModifyType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
 *         &amp;lt;element name="Amenity" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GolfAmenitySummaryType"&amp;gt;
 *                 &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Charge" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="TaxAmounts" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="TaxAmount" maxOccurs="99"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GolfTaxAmountGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Calculation" maxOccurs="5" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="UnitCharge" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="UnitName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="Quantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                           &amp;lt;attribute name="Percentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                           &amp;lt;attribute name="Applicability"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="FromPickupLocation"/&amp;gt;
 *                                 &amp;lt;enumeration value="FromDropoffLocation"/&amp;gt;
 *                                 &amp;lt;enumeration value="BeforePickup"/&amp;gt;
 *                                 &amp;lt;enumeration value="AfterDropoff"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="MaxQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                           &amp;lt;attribute name="Total" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="TaxInclusive" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="GuaranteedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RateConvertInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Discounts" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateQualifierType"&amp;gt;
 *                 &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Golfer" type="{http://www.opentravel.org/OTA/2003/05}GolferType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PaymentForm" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
 *                 &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="ChargeRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PaymentGuarantee" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *       &amp;lt;attribute name="InternalResID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="RoundID" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="GolferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="PackageID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="StartingTee" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="NumberOfHoles" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="CartQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ReplayRoundQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TeeTimeModifyType", propOrder = {
    "reservationID",
    "amenity",
    "charge",
    "commission",
    "discounts",
    "golfer",
    "paymentForm",
    "paymentGuarantee",
    "tpaExtensions"
})
public class TeeTimeModifyType {

    @XmlElement(name = "ReservationID", required = true)
    protected UniqueIDType reservationID;
    @XmlElement(name = "Amenity")
    protected List<TeeTimeModifyType.Amenity> amenity;
    @XmlElement(name = "Charge")
    protected List<TeeTimeModifyType.Charge> charge;
    @XmlElement(name = "Commission")
    protected List<CommissionType> commission;
    @XmlElement(name = "Discounts")
    protected List<TeeTimeModifyType.Discounts> discounts;
    @XmlElement(name = "Golfer")
    protected List<GolferType> golfer;
    @XmlElement(name = "PaymentForm")
    protected List<TeeTimeModifyType.PaymentForm> paymentForm;
    @XmlElement(name = "PaymentGuarantee")
    protected PaymentFormType paymentGuarantee;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "InternalResID")
    protected String internalResID;
    @XmlAttribute(name = "RoundID")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger roundID;
    @XmlAttribute(name = "GolferQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger golferQty;
    @XmlAttribute(name = "PackageID")
    protected String packageID;
    @XmlAttribute(name = "StartingTee")
    protected String startingTee;
    @XmlAttribute(name = "NumberOfHoles")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger numberOfHoles;
    @XmlAttribute(name = "CartQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger cartQty;
    @XmlAttribute(name = "ReplayRoundQty")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger replayRoundQty;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;

    /**
     * Obtiene el valor de la propiedad reservationID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getReservationID() {
        return reservationID;
    }

    /**
     * Define el valor de la propiedad reservationID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setReservationID(UniqueIDType value) {
        this.reservationID = value;
    }

    /**
     * Gets the value of the amenity property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the amenity property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAmenity().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TeeTimeModifyType.Amenity }
     * 
     * 
     */
    public List<TeeTimeModifyType.Amenity> getAmenity() {
        if (amenity == null) {
            amenity = new ArrayList<TeeTimeModifyType.Amenity>();
        }
        return this.amenity;
    }

    /**
     * Gets the value of the charge property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the charge property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCharge().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TeeTimeModifyType.Charge }
     * 
     * 
     */
    public List<TeeTimeModifyType.Charge> getCharge() {
        if (charge == null) {
            charge = new ArrayList<TeeTimeModifyType.Charge>();
        }
        return this.charge;
    }

    /**
     * Gets the value of the commission property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the commission property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCommission().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CommissionType }
     * 
     * 
     */
    public List<CommissionType> getCommission() {
        if (commission == null) {
            commission = new ArrayList<CommissionType>();
        }
        return this.commission;
    }

    /**
     * Gets the value of the discounts property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the discounts property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDiscounts().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TeeTimeModifyType.Discounts }
     * 
     * 
     */
    public List<TeeTimeModifyType.Discounts> getDiscounts() {
        if (discounts == null) {
            discounts = new ArrayList<TeeTimeModifyType.Discounts>();
        }
        return this.discounts;
    }

    /**
     * Gets the value of the golfer property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the golfer property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getGolfer().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GolferType }
     * 
     * 
     */
    public List<GolferType> getGolfer() {
        if (golfer == null) {
            golfer = new ArrayList<GolferType>();
        }
        return this.golfer;
    }

    /**
     * Gets the value of the paymentForm property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentForm property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPaymentForm().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TeeTimeModifyType.PaymentForm }
     * 
     * 
     */
    public List<TeeTimeModifyType.PaymentForm> getPaymentForm() {
        if (paymentForm == null) {
            paymentForm = new ArrayList<TeeTimeModifyType.PaymentForm>();
        }
        return this.paymentForm;
    }

    /**
     * Obtiene el valor de la propiedad paymentGuarantee.
     * 
     * @return
     *     possible object is
     *     {@link PaymentFormType }
     *     
     */
    public PaymentFormType getPaymentGuarantee() {
        return paymentGuarantee;
    }

    /**
     * Define el valor de la propiedad paymentGuarantee.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentFormType }
     *     
     */
    public void setPaymentGuarantee(PaymentFormType value) {
        this.paymentGuarantee = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad internalResID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalResID() {
        return internalResID;
    }

    /**
     * Define el valor de la propiedad internalResID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalResID(String value) {
        this.internalResID = value;
    }

    /**
     * Obtiene el valor de la propiedad roundID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRoundID() {
        return roundID;
    }

    /**
     * Define el valor de la propiedad roundID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRoundID(BigInteger value) {
        this.roundID = value;
    }

    /**
     * Obtiene el valor de la propiedad golferQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGolferQty() {
        return golferQty;
    }

    /**
     * Define el valor de la propiedad golferQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGolferQty(BigInteger value) {
        this.golferQty = value;
    }

    /**
     * Obtiene el valor de la propiedad packageID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageID() {
        return packageID;
    }

    /**
     * Define el valor de la propiedad packageID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageID(String value) {
        this.packageID = value;
    }

    /**
     * Obtiene el valor de la propiedad startingTee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartingTee() {
        return startingTee;
    }

    /**
     * Define el valor de la propiedad startingTee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartingTee(String value) {
        this.startingTee = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfHoles.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfHoles() {
        return numberOfHoles;
    }

    /**
     * Define el valor de la propiedad numberOfHoles.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfHoles(BigInteger value) {
        this.numberOfHoles = value;
    }

    /**
     * Obtiene el valor de la propiedad cartQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCartQty() {
        return cartQty;
    }

    /**
     * Define el valor de la propiedad cartQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCartQty(BigInteger value) {
        this.cartQty = value;
    }

    /**
     * Obtiene el valor de la propiedad replayRoundQty.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getReplayRoundQty() {
        return replayRoundQty;
    }

    /**
     * Define el valor de la propiedad replayRoundQty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setReplayRoundQty(BigInteger value) {
        this.replayRoundQty = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GolfAmenitySummaryType"&amp;gt;
     *       &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Amenity
        extends GolfAmenitySummaryType
    {

        @XmlAttribute(name = "GolferRPH")
        protected String golferRPH;
        @XmlAttribute(name = "Quantity", required = true)
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger quantity;

        /**
         * Obtiene el valor de la propiedad golferRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGolferRPH() {
            return golferRPH;
        }

        /**
         * Define el valor de la propiedad golferRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGolferRPH(String value) {
            this.golferRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad quantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getQuantity() {
            return quantity;
        }

        /**
         * Define el valor de la propiedad quantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setQuantity(BigInteger value) {
            this.quantity = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="TaxAmounts" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="TaxAmount" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GolfTaxAmountGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Calculation" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="UnitCharge" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="UnitName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="Quantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *                 &amp;lt;attribute name="Percentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                 &amp;lt;attribute name="Applicability"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="FromPickupLocation"/&amp;gt;
     *                       &amp;lt;enumeration value="FromDropoffLocation"/&amp;gt;
     *                       &amp;lt;enumeration value="BeforePickup"/&amp;gt;
     *                       &amp;lt;enumeration value="AfterDropoff"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="MaxQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *                 &amp;lt;attribute name="Total" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="TaxInclusive" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GuaranteedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RateConvertInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxAmounts",
        "description",
        "calculation"
    })
    public static class Charge {

        @XmlElement(name = "TaxAmounts")
        protected TeeTimeModifyType.Charge.TaxAmounts taxAmounts;
        @XmlElement(name = "Description")
        protected List<ParagraphType> description;
        @XmlElement(name = "Calculation")
        protected List<TeeTimeModifyType.Charge.Calculation> calculation;
        @XmlAttribute(name = "TaxInclusive")
        protected Boolean taxInclusive;
        @XmlAttribute(name = "GuaranteedInd")
        protected Boolean guaranteedInd;
        @XmlAttribute(name = "RateConvertInd")
        protected Boolean rateConvertInd;
        @XmlAttribute(name = "GolferRPH")
        protected String golferRPH;
        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad taxAmounts.
         * 
         * @return
         *     possible object is
         *     {@link TeeTimeModifyType.Charge.TaxAmounts }
         *     
         */
        public TeeTimeModifyType.Charge.TaxAmounts getTaxAmounts() {
            return taxAmounts;
        }

        /**
         * Define el valor de la propiedad taxAmounts.
         * 
         * @param value
         *     allowed object is
         *     {@link TeeTimeModifyType.Charge.TaxAmounts }
         *     
         */
        public void setTaxAmounts(TeeTimeModifyType.Charge.TaxAmounts value) {
            this.taxAmounts = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the description property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDescription().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getDescription() {
            if (description == null) {
                description = new ArrayList<ParagraphType>();
            }
            return this.description;
        }

        /**
         * Gets the value of the calculation property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the calculation property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCalculation().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TeeTimeModifyType.Charge.Calculation }
         * 
         * 
         */
        public List<TeeTimeModifyType.Charge.Calculation> getCalculation() {
            if (calculation == null) {
                calculation = new ArrayList<TeeTimeModifyType.Charge.Calculation>();
            }
            return this.calculation;
        }

        /**
         * Obtiene el valor de la propiedad taxInclusive.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isTaxInclusive() {
            return taxInclusive;
        }

        /**
         * Define el valor de la propiedad taxInclusive.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setTaxInclusive(Boolean value) {
            this.taxInclusive = value;
        }

        /**
         * Obtiene el valor de la propiedad guaranteedInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteedInd() {
            return guaranteedInd;
        }

        /**
         * Define el valor de la propiedad guaranteedInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteedInd(Boolean value) {
            this.guaranteedInd = value;
        }

        /**
         * Obtiene el valor de la propiedad rateConvertInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRateConvertInd() {
            return rateConvertInd;
        }

        /**
         * Define el valor de la propiedad rateConvertInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRateConvertInd(Boolean value) {
            this.rateConvertInd = value;
        }

        /**
         * Obtiene el valor de la propiedad golferRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGolferRPH() {
            return golferRPH;
        }

        /**
         * Define el valor de la propiedad golferRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGolferRPH(String value) {
            this.golferRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="UnitCharge" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="UnitName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="Quantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
         *       &amp;lt;attribute name="Percentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *       &amp;lt;attribute name="Applicability"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="FromPickupLocation"/&amp;gt;
         *             &amp;lt;enumeration value="FromDropoffLocation"/&amp;gt;
         *             &amp;lt;enumeration value="BeforePickup"/&amp;gt;
         *             &amp;lt;enumeration value="AfterDropoff"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="MaxQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
         *       &amp;lt;attribute name="Total" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Calculation {

            @XmlAttribute(name = "UnitCharge")
            protected BigDecimal unitCharge;
            @XmlAttribute(name = "UnitName")
            protected String unitName;
            @XmlAttribute(name = "Quantity")
            protected Integer quantity;
            @XmlAttribute(name = "Percentage")
            protected BigDecimal percentage;
            @XmlAttribute(name = "Applicability")
            protected String applicability;
            @XmlAttribute(name = "MaxQuantity")
            protected Integer maxQuantity;
            @XmlAttribute(name = "Total")
            protected BigDecimal total;

            /**
             * Obtiene el valor de la propiedad unitCharge.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getUnitCharge() {
                return unitCharge;
            }

            /**
             * Define el valor de la propiedad unitCharge.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setUnitCharge(BigDecimal value) {
                this.unitCharge = value;
            }

            /**
             * Obtiene el valor de la propiedad unitName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitName() {
                return unitName;
            }

            /**
             * Define el valor de la propiedad unitName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitName(String value) {
                this.unitName = value;
            }

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setQuantity(Integer value) {
                this.quantity = value;
            }

            /**
             * Obtiene el valor de la propiedad percentage.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPercentage() {
                return percentage;
            }

            /**
             * Define el valor de la propiedad percentage.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPercentage(BigDecimal value) {
                this.percentage = value;
            }

            /**
             * Obtiene el valor de la propiedad applicability.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApplicability() {
                return applicability;
            }

            /**
             * Define el valor de la propiedad applicability.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApplicability(String value) {
                this.applicability = value;
            }

            /**
             * Obtiene el valor de la propiedad maxQuantity.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getMaxQuantity() {
                return maxQuantity;
            }

            /**
             * Define el valor de la propiedad maxQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setMaxQuantity(Integer value) {
                this.maxQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad total.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotal() {
                return total;
            }

            /**
             * Define el valor de la propiedad total.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotal(BigDecimal value) {
                this.total = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="TaxAmount" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GolfTaxAmountGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "taxAmount"
        })
        public static class TaxAmounts {

            @XmlElement(name = "TaxAmount", required = true)
            protected List<TeeTimeModifyType.Charge.TaxAmounts.TaxAmount> taxAmount;

            /**
             * Gets the value of the taxAmount property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxAmount property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTaxAmount().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link TeeTimeModifyType.Charge.TaxAmounts.TaxAmount }
             * 
             * 
             */
            public List<TeeTimeModifyType.Charge.TaxAmounts.TaxAmount> getTaxAmount() {
                if (taxAmount == null) {
                    taxAmount = new ArrayList<TeeTimeModifyType.Charge.TaxAmounts.TaxAmount>();
                }
                return this.taxAmount;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GolfTaxAmountGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class TaxAmount {

                @XmlAttribute(name = "Total", required = true)
                protected BigDecimal total;
                @XmlAttribute(name = "CurrencyCode", required = true)
                protected String currencyCode;
                @XmlAttribute(name = "TaxCode")
                protected String taxCode;
                @XmlAttribute(name = "Percentage")
                protected BigDecimal percentage;
                @XmlAttribute(name = "Description")
                protected String description;

                /**
                 * Obtiene el valor de la propiedad total.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getTotal() {
                    return total;
                }

                /**
                 * Define el valor de la propiedad total.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setTotal(BigDecimal value) {
                    this.total = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad taxCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTaxCode() {
                    return taxCode;
                }

                /**
                 * Define el valor de la propiedad taxCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTaxCode(String value) {
                    this.taxCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad percentage.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPercentage() {
                    return percentage;
                }

                /**
                 * Define el valor de la propiedad percentage.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPercentage(BigDecimal value) {
                    this.percentage = value;
                }

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateQualifierType"&amp;gt;
     *       &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Discounts
        extends RateQualifierType
    {

        @XmlAttribute(name = "GolferRPH")
        protected String golferRPH;

        /**
         * Obtiene el valor de la propiedad golferRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGolferRPH() {
            return golferRPH;
        }

        /**
         * Define el valor de la propiedad golferRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGolferRPH(String value) {
            this.golferRPH = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentFormType"&amp;gt;
     *       &amp;lt;attribute name="GolferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="ChargeRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PaymentForm
        extends PaymentFormType
    {

        @XmlAttribute(name = "GolferRPH")
        protected String golferRPH;
        @XmlAttribute(name = "ChargeRPH")
        protected String chargeRPH;

        /**
         * Obtiene el valor de la propiedad golferRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGolferRPH() {
            return golferRPH;
        }

        /**
         * Define el valor de la propiedad golferRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGolferRPH(String value) {
            this.golferRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad chargeRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChargeRPH() {
            return chargeRPH;
        }

        /**
         * Define el valor de la propiedad chargeRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChargeRPH(String value) {
            this.chargeRPH = value;
        }

    }

}
