
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_LoyaltyPrgCurrency_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_LoyaltyPrgCurrency_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Miles"/&amp;gt;
 *     &amp;lt;enumeration value="Points"/&amp;gt;
 *     &amp;lt;enumeration value="Stays"/&amp;gt;
 *     &amp;lt;enumeration value="Voucher"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_LoyaltyPrgCurrency_Base")
@XmlEnum
public enum ListLoyaltyPrgCurrencyBase {

    @XmlEnumValue("Miles")
    MILES("Miles"),
    @XmlEnumValue("Points")
    POINTS("Points"),
    @XmlEnumValue("Stays")
    STAYS("Stays"),
    @XmlEnumValue("Voucher")
    VOUCHER("Voucher"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListLoyaltyPrgCurrencyBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListLoyaltyPrgCurrencyBase fromValue(String v) {
        for (ListLoyaltyPrgCurrencyBase c: ListLoyaltyPrgCurrencyBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
