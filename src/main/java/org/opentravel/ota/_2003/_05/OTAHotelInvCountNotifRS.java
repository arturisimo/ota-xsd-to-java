
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}MessageAcknowledgementType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Inventories" type="{http://www.opentravel.org/OTA/2003/05}InvCountType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inventories"
})
@XmlRootElement(name = "OTA_HotelInvCountNotifRS")
public class OTAHotelInvCountNotifRS
    extends MessageAcknowledgementType
{

    @XmlElement(name = "Inventories")
    protected InvCountType inventories;

    /**
     * Obtiene el valor de la propiedad inventories.
     * 
     * @return
     *     possible object is
     *     {@link InvCountType }
     *     
     */
    public InvCountType getInventories() {
        return inventories;
    }

    /**
     * Define el valor de la propiedad inventories.
     * 
     * @param value
     *     allowed object is
     *     {@link InvCountType }
     *     
     */
    public void setInventories(InvCountType value) {
        this.inventories = value;
    }

}
