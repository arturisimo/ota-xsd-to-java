
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Rules that apply to an extra.
 * 
 * &lt;p&gt;Clase Java para TourActivityExtraRule complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityExtraRule"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ParticipantQualifier" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AgeQualifier" type="{http://www.opentravel.org/OTA/2003/05}AgeQualifierType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BirthDateGroup"/&amp;gt;
 *                 &amp;lt;attribute name="MinimumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaximumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="YearsExperience" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OtherCriteria" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="DepositPaymentTypes" type="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="MaxOccupancy" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                 &amp;lt;attribute name="DateTimeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="StockControlledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DurationReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DepositInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="MandatoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="OptionalInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ApplyTo"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="All_Participants"/&amp;gt;
 *             &amp;lt;enumeration value="Some_Participants"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityExtraRule", propOrder = {
    "participantQualifier",
    "otherCriteria"
})
public class TourActivityExtraRule {

    @XmlElement(name = "ParticipantQualifier")
    protected List<TourActivityExtraRule.ParticipantQualifier> participantQualifier;
    @XmlElement(name = "OtherCriteria")
    protected TourActivityExtraRule.OtherCriteria otherCriteria;
    @XmlAttribute(name = "MandatoryInd")
    protected Boolean mandatoryInd;
    @XmlAttribute(name = "OptionalInd")
    protected Boolean optionalInd;
    @XmlAttribute(name = "ApplyTo")
    protected String applyTo;
    @XmlAttribute(name = "Description")
    protected String description;

    /**
     * Gets the value of the participantQualifier property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantQualifier property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getParticipantQualifier().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TourActivityExtraRule.ParticipantQualifier }
     * 
     * 
     */
    public List<TourActivityExtraRule.ParticipantQualifier> getParticipantQualifier() {
        if (participantQualifier == null) {
            participantQualifier = new ArrayList<TourActivityExtraRule.ParticipantQualifier>();
        }
        return this.participantQualifier;
    }

    /**
     * Obtiene el valor de la propiedad otherCriteria.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityExtraRule.OtherCriteria }
     *     
     */
    public TourActivityExtraRule.OtherCriteria getOtherCriteria() {
        return otherCriteria;
    }

    /**
     * Define el valor de la propiedad otherCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityExtraRule.OtherCriteria }
     *     
     */
    public void setOtherCriteria(TourActivityExtraRule.OtherCriteria value) {
        this.otherCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad mandatoryInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMandatoryInd() {
        return mandatoryInd;
    }

    /**
     * Define el valor de la propiedad mandatoryInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMandatoryInd(Boolean value) {
        this.mandatoryInd = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOptionalInd() {
        return optionalInd;
    }

    /**
     * Define el valor de la propiedad optionalInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOptionalInd(Boolean value) {
        this.optionalInd = value;
    }

    /**
     * Obtiene el valor de la propiedad applyTo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyTo() {
        return applyTo;
    }

    /**
     * Define el valor de la propiedad applyTo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyTo(String value) {
        this.applyTo = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="DepositPaymentTypes" type="{http://www.opentravel.org/OTA/2003/05}AcceptedPaymentsType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="MaxOccupancy" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *       &amp;lt;attribute name="DateTimeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="StockControlledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DurationReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DepositInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "depositPaymentTypes"
    })
    public static class OtherCriteria {

        @XmlElement(name = "DepositPaymentTypes")
        protected AcceptedPaymentsType depositPaymentTypes;
        @XmlAttribute(name = "MaxOccupancy")
        protected Integer maxOccupancy;
        @XmlAttribute(name = "DateTimeReqInd")
        protected Boolean dateTimeReqInd;
        @XmlAttribute(name = "StockControlledInd")
        protected Boolean stockControlledInd;
        @XmlAttribute(name = "DurationReqInd")
        protected Boolean durationReqInd;
        @XmlAttribute(name = "DepositInd")
        protected Boolean depositInd;

        /**
         * Obtiene el valor de la propiedad depositPaymentTypes.
         * 
         * @return
         *     possible object is
         *     {@link AcceptedPaymentsType }
         *     
         */
        public AcceptedPaymentsType getDepositPaymentTypes() {
            return depositPaymentTypes;
        }

        /**
         * Define el valor de la propiedad depositPaymentTypes.
         * 
         * @param value
         *     allowed object is
         *     {@link AcceptedPaymentsType }
         *     
         */
        public void setDepositPaymentTypes(AcceptedPaymentsType value) {
            this.depositPaymentTypes = value;
        }

        /**
         * Obtiene el valor de la propiedad maxOccupancy.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxOccupancy() {
            return maxOccupancy;
        }

        /**
         * Define el valor de la propiedad maxOccupancy.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxOccupancy(Integer value) {
            this.maxOccupancy = value;
        }

        /**
         * Obtiene el valor de la propiedad dateTimeReqInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDateTimeReqInd() {
            return dateTimeReqInd;
        }

        /**
         * Define el valor de la propiedad dateTimeReqInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDateTimeReqInd(Boolean value) {
            this.dateTimeReqInd = value;
        }

        /**
         * Obtiene el valor de la propiedad stockControlledInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isStockControlledInd() {
            return stockControlledInd;
        }

        /**
         * Define el valor de la propiedad stockControlledInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setStockControlledInd(Boolean value) {
            this.stockControlledInd = value;
        }

        /**
         * Obtiene el valor de la propiedad durationReqInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDurationReqInd() {
            return durationReqInd;
        }

        /**
         * Define el valor de la propiedad durationReqInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDurationReqInd(Boolean value) {
            this.durationReqInd = value;
        }

        /**
         * Obtiene el valor de la propiedad depositInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDepositInd() {
            return depositInd;
        }

        /**
         * Define el valor de la propiedad depositInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDepositInd(Boolean value) {
            this.depositInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AgeQualifier" type="{http://www.opentravel.org/OTA/2003/05}AgeQualifierType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BirthDateGroup"/&amp;gt;
     *       &amp;lt;attribute name="MinimumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaximumAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="YearsExperience" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="Age" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ageQualifier"
    })
    public static class ParticipantQualifier {

        @XmlElement(name = "AgeQualifier")
        protected AgeQualifierType ageQualifier;
        @XmlAttribute(name = "MinimumAge")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger minimumAge;
        @XmlAttribute(name = "MaximumAge")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maximumAge;
        @XmlAttribute(name = "YearsExperience")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger yearsExperience;
        @XmlAttribute(name = "Age")
        protected Integer age;
        @XmlAttribute(name = "BirthDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar birthDate;

        /**
         * Obtiene el valor de la propiedad ageQualifier.
         * 
         * @return
         *     possible object is
         *     {@link AgeQualifierType }
         *     
         */
        public AgeQualifierType getAgeQualifier() {
            return ageQualifier;
        }

        /**
         * Define el valor de la propiedad ageQualifier.
         * 
         * @param value
         *     allowed object is
         *     {@link AgeQualifierType }
         *     
         */
        public void setAgeQualifier(AgeQualifierType value) {
            this.ageQualifier = value;
        }

        /**
         * Obtiene el valor de la propiedad minimumAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMinimumAge() {
            return minimumAge;
        }

        /**
         * Define el valor de la propiedad minimumAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMinimumAge(BigInteger value) {
            this.minimumAge = value;
        }

        /**
         * Obtiene el valor de la propiedad maximumAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaximumAge() {
            return maximumAge;
        }

        /**
         * Define el valor de la propiedad maximumAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaximumAge(BigInteger value) {
            this.maximumAge = value;
        }

        /**
         * Obtiene el valor de la propiedad yearsExperience.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getYearsExperience() {
            return yearsExperience;
        }

        /**
         * Define el valor de la propiedad yearsExperience.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setYearsExperience(BigInteger value) {
            this.yearsExperience = value;
        }

        /**
         * Obtiene el valor de la propiedad age.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAge() {
            return age;
        }

        /**
         * Define el valor de la propiedad age.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAge(Integer value) {
            this.age = value;
        }

        /**
         * Obtiene el valor de la propiedad birthDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getBirthDate() {
            return birthDate;
        }

        /**
         * Define el valor de la propiedad birthDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setBirthDate(XMLGregorianCalendar value) {
            this.birthDate = value;
        }

    }

}
