
package org.opentravel.ota._2003._05;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Detail information about a cabin class, including row characteristics, zone and seat details.
 * 
 * &lt;p&gt;Clase Java para CabinClassDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CabinClassDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AvailabilityList" type="{http://www.opentravel.org/OTA/2003/05}CabinClassAvailabilityType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RowInfo" type="{http://www.opentravel.org/OTA/2003/05}RowDetailType" maxOccurs="999" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SeatInfo" type="{http://www.opentravel.org/OTA/2003/05}SeatDetailsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Zone" type="{http://www.opentravel.org/OTA/2003/05}SeatZoneSummaryType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Layout" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="UpperDeckInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ColumnNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ColumnSpan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CabinClassDetailType", propOrder = {
    "content"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.SeatMapDetailsType.CabinClass.class
})
public class CabinClassDetailType {

    @XmlElementRefs({
        @XmlElementRef(name = "AvailabilityList", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "RowInfo", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "SeatInfo", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Zone", namespace = "http://www.opentravel.org/OTA/2003/05", type = JAXBElement.class, required = false)
    })
    @XmlMixed
    protected List<Serializable> content;
    @XmlAttribute(name = "Layout")
    protected String layout;
    @XmlAttribute(name = "UpperDeckInd")
    protected Boolean upperDeckInd;
    @XmlAttribute(name = "ColumnNumber")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger columnNumber;
    @XmlAttribute(name = "ColumnSpan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger columnSpan;

    /**
     * Detail information about a cabin class, including row characteristics, zone and seat details.Gets the value of the content property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the content property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getContent().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link CabinClassAvailabilityType }{@code >}
     * {@link JAXBElement }{@code <}{@link RowDetailType }{@code >}
     * {@link JAXBElement }{@code <}{@link SeatDetailsType }{@code >}
     * {@link JAXBElement }{@code <}{@link SeatZoneSummaryType }{@code >}
     * {@link String }
     * 
     * 
     */
    public List<Serializable> getContent() {
        if (content == null) {
            content = new ArrayList<Serializable>();
        }
        return this.content;
    }

    /**
     * Obtiene el valor de la propiedad layout.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLayout() {
        return layout;
    }

    /**
     * Define el valor de la propiedad layout.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLayout(String value) {
        this.layout = value;
    }

    /**
     * Obtiene el valor de la propiedad upperDeckInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUpperDeckInd() {
        return upperDeckInd;
    }

    /**
     * Define el valor de la propiedad upperDeckInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpperDeckInd(Boolean value) {
        this.upperDeckInd = value;
    }

    /**
     * Obtiene el valor de la propiedad columnNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColumnNumber() {
        return columnNumber;
    }

    /**
     * Define el valor de la propiedad columnNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColumnNumber(BigInteger value) {
        this.columnNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad columnSpan.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColumnSpan() {
        return columnSpan;
    }

    /**
     * Define el valor de la propiedad columnSpan.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColumnSpan(BigInteger value) {
        this.columnSpan = value;
    }

}
