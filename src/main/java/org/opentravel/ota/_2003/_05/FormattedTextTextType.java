
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Provides text and indicates whether it is formatted or not.
 * 
 * &lt;p&gt;Clase Java para FormattedTextTextType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="FormattedTextTextType"&amp;gt;
 *   &amp;lt;simpleContent&amp;gt;
 *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LanguageGroup"/&amp;gt;
 *       &amp;lt;attribute name="Formatted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TextFormat"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="PlainText"/&amp;gt;
 *             &amp;lt;enumeration value="HTML"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/simpleContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormattedTextTextType", propOrder = {
    "value"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.TravelerInfoType.AirTraveler.Comment.class,
    org.opentravel.ota._2003._05.FulfillmentType.PaymentText.class,
    org.opentravel.ota._2003._05.OTAGolfRatePlanRS.Course.Rate.AppliesTo.NegotiatedRate.RateComment.class,
    org.opentravel.ota._2003._05.RFPRequestSegmentsType.RFPRequestSegment.Preferences.Preference.LocationCategories.Category.class,
    org.opentravel.ota._2003._05.VehicleRentalDetailsType.ConditionReport.class,
    org.opentravel.ota._2003._05.ImageDescriptionType.Description.class,
    org.opentravel.ota._2003._05.TextDescriptionType.Description.class,
    org.opentravel.ota._2003._05.VideoDescriptionType.Description.class,
    CoverageDetailsType.class,
    org.opentravel.ota._2003._05.RateQualifierType.RateComments.RateComment.class,
    org.opentravel.ota._2003._05.CustomQuestionType.ProvidedAnswerChoices.class,
    org.opentravel.ota._2003._05.EventReportType.GeneralEventInfo.Dates.Date.LocationCategories.Category.class,
    org.opentravel.ota._2003._05.ParagraphType.ListItem.class,
    PkgCautionType.class
})
public class FormattedTextTextType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Formatted")
    protected Boolean formatted;
    @XmlAttribute(name = "TextFormat")
    protected String textFormat;
    @XmlAttribute(name = "Language")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad formatted.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFormatted() {
        return formatted;
    }

    /**
     * Define el valor de la propiedad formatted.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormatted(Boolean value) {
        this.formatted = value;
    }

    /**
     * Obtiene el valor de la propiedad textFormat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextFormat() {
        return textFormat;
    }

    /**
     * Define el valor de la propiedad textFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextFormat(String value) {
        this.textFormat = value;
    }

    /**
     * Obtiene el valor de la propiedad language.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Define el valor de la propiedad language.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

}
