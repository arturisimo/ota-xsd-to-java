
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The HotelDescriptiveContent element contains the descriptive information about a hotel property.
 * 
 * &lt;p&gt;Clase Java para HotelDescriptiveContentType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="HotelDescriptiveContentType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="DestinationSystemsCode" type="{http://www.opentravel.org/OTA/2003/05}DestinationSystemCodesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HotelInfo" type="{http://www.opentravel.org/OTA/2003/05}HotelInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FacilityInfo" type="{http://www.opentravel.org/OTA/2003/05}FacilityInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Policies" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PoliciesType"&amp;gt;
 *                 &amp;lt;attribute name="GuaranteeRoomTypeViaGDS" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="GuaranteeRoomTypeViaCRC" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="GuaranteeRoomTypeViaProperty" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AreaInfo" type="{http://www.opentravel.org/OTA/2003/05}AreaInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AffiliationInfo" type="{http://www.opentravel.org/OTA/2003/05}AffiliationInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="MultimediaDescriptions" type="{http://www.opentravel.org/OTA/2003/05}MultimediaDescriptionsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ContactInfos" type="{http://www.opentravel.org/OTA/2003/05}ContactInfosType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="GDS_Info" type="{http://www.opentravel.org/OTA/2003/05}GDS_InfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Viewerships" type="{http://www.opentravel.org/OTA/2003/05}ViewershipsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="EffectivePeriods" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="EffectivePeriod" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Promotions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Promotion" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}DescriptionType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="PkgOrPromotion"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="Package"/&amp;gt;
 *                                 &amp;lt;enumeration value="Promotion"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="Title" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                           &amp;lt;attribute name="MinLOS" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="Remarks" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                           &amp;lt;attribute name="SortOrder" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RoomBlocks" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="AvailableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="TotalRoomQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="CancelPeriodDays" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="RateType"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;union&amp;gt;
 *                       &amp;lt;simpleType&amp;gt;
 *                         &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
 *                           &amp;lt;enumeration value="NotApplicable"/&amp;gt;
 *                           &amp;lt;enumeration value="Corporate"/&amp;gt;
 *                           &amp;lt;enumeration value="Preferred"/&amp;gt;
 *                           &amp;lt;enumeration value="Rack"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/simpleType&amp;gt;
 *                     &amp;lt;/union&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="BlackOutDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="EnvironmentalImpact" type="{http://www.opentravel.org/OTA/2003/05}EnvironmentalImpactType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *       &amp;lt;attribute name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *       &amp;lt;attribute name="TimeZone" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *       &amp;lt;attribute name="DistanceUnitOfMeasureCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="AreaUnitOfMeasureCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="WeightUnitOfMeasureCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelDescriptiveContentType", propOrder = {
    "destinationSystemsCode",
    "hotelInfo",
    "facilityInfo",
    "policies",
    "areaInfo",
    "affiliationInfo",
    "multimediaDescriptions",
    "contactInfos",
    "gdsInfo",
    "viewerships",
    "effectivePeriods",
    "promotions",
    "roomBlocks",
    "environmentalImpact",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAHotelDescriptiveContentNotifRQ.HotelDescriptiveContents.HotelDescriptiveContent.class,
    org.opentravel.ota._2003._05.OTAHotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.class,
    org.opentravel.ota._2003._05.OTANotifReportRQ.NotifDetails.HotelNotifReport.HotelDescriptiveContents.HotelDescriptiveContent.class
})
public class HotelDescriptiveContentType {

    @XmlElement(name = "DestinationSystemsCode")
    protected DestinationSystemCodesType destinationSystemsCode;
    @XmlElement(name = "HotelInfo")
    protected HotelInfoType hotelInfo;
    @XmlElement(name = "FacilityInfo")
    protected FacilityInfoType facilityInfo;
    @XmlElement(name = "Policies")
    protected HotelDescriptiveContentType.Policies policies;
    @XmlElement(name = "AreaInfo")
    protected AreaInfoType areaInfo;
    @XmlElement(name = "AffiliationInfo")
    protected AffiliationInfoType affiliationInfo;
    @XmlElement(name = "MultimediaDescriptions")
    protected MultimediaDescriptionsType multimediaDescriptions;
    @XmlElement(name = "ContactInfos")
    protected ContactInfosType contactInfos;
    @XmlElement(name = "GDS_Info")
    protected GDSInfoType gdsInfo;
    @XmlElement(name = "Viewerships")
    protected ViewershipsType viewerships;
    @XmlElement(name = "EffectivePeriods")
    protected HotelDescriptiveContentType.EffectivePeriods effectivePeriods;
    @XmlElement(name = "Promotions")
    protected HotelDescriptiveContentType.Promotions promotions;
    @XmlElement(name = "RoomBlocks")
    protected HotelDescriptiveContentType.RoomBlocks roomBlocks;
    @XmlElement(name = "EnvironmentalImpact")
    protected EnvironmentalImpactType environmentalImpact;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "LanguageCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String languageCode;
    @XmlAttribute(name = "TimeZone")
    protected String timeZone;
    @XmlAttribute(name = "DistanceUnitOfMeasureCode")
    protected String distanceUnitOfMeasureCode;
    @XmlAttribute(name = "AreaUnitOfMeasureCode")
    protected String areaUnitOfMeasureCode;
    @XmlAttribute(name = "WeightUnitOfMeasureCode")
    protected String weightUnitOfMeasureCode;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "DecimalPlaces")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger decimalPlaces;
    @XmlAttribute(name = "UnitOfMeasureQuantity")
    protected BigDecimal unitOfMeasureQuantity;
    @XmlAttribute(name = "UnitOfMeasure")
    protected String unitOfMeasure;
    @XmlAttribute(name = "UnitOfMeasureCode")
    protected String unitOfMeasureCode;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;

    /**
     * Obtiene el valor de la propiedad destinationSystemsCode.
     * 
     * @return
     *     possible object is
     *     {@link DestinationSystemCodesType }
     *     
     */
    public DestinationSystemCodesType getDestinationSystemsCode() {
        return destinationSystemsCode;
    }

    /**
     * Define el valor de la propiedad destinationSystemsCode.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationSystemCodesType }
     *     
     */
    public void setDestinationSystemsCode(DestinationSystemCodesType value) {
        this.destinationSystemsCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelInfoType }
     *     
     */
    public HotelInfoType getHotelInfo() {
        return hotelInfo;
    }

    /**
     * Define el valor de la propiedad hotelInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelInfoType }
     *     
     */
    public void setHotelInfo(HotelInfoType value) {
        this.hotelInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad facilityInfo.
     * 
     * @return
     *     possible object is
     *     {@link FacilityInfoType }
     *     
     */
    public FacilityInfoType getFacilityInfo() {
        return facilityInfo;
    }

    /**
     * Define el valor de la propiedad facilityInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityInfoType }
     *     
     */
    public void setFacilityInfo(FacilityInfoType value) {
        this.facilityInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad policies.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveContentType.Policies }
     *     
     */
    public HotelDescriptiveContentType.Policies getPolicies() {
        return policies;
    }

    /**
     * Define el valor de la propiedad policies.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveContentType.Policies }
     *     
     */
    public void setPolicies(HotelDescriptiveContentType.Policies value) {
        this.policies = value;
    }

    /**
     * Obtiene el valor de la propiedad areaInfo.
     * 
     * @return
     *     possible object is
     *     {@link AreaInfoType }
     *     
     */
    public AreaInfoType getAreaInfo() {
        return areaInfo;
    }

    /**
     * Define el valor de la propiedad areaInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AreaInfoType }
     *     
     */
    public void setAreaInfo(AreaInfoType value) {
        this.areaInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad affiliationInfo.
     * 
     * @return
     *     possible object is
     *     {@link AffiliationInfoType }
     *     
     */
    public AffiliationInfoType getAffiliationInfo() {
        return affiliationInfo;
    }

    /**
     * Define el valor de la propiedad affiliationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AffiliationInfoType }
     *     
     */
    public void setAffiliationInfo(AffiliationInfoType value) {
        this.affiliationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad multimediaDescriptions.
     * 
     * @return
     *     possible object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public MultimediaDescriptionsType getMultimediaDescriptions() {
        return multimediaDescriptions;
    }

    /**
     * Define el valor de la propiedad multimediaDescriptions.
     * 
     * @param value
     *     allowed object is
     *     {@link MultimediaDescriptionsType }
     *     
     */
    public void setMultimediaDescriptions(MultimediaDescriptionsType value) {
        this.multimediaDescriptions = value;
    }

    /**
     * Obtiene el valor de la propiedad contactInfos.
     * 
     * @return
     *     possible object is
     *     {@link ContactInfosType }
     *     
     */
    public ContactInfosType getContactInfos() {
        return contactInfos;
    }

    /**
     * Define el valor de la propiedad contactInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInfosType }
     *     
     */
    public void setContactInfos(ContactInfosType value) {
        this.contactInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad gdsInfo.
     * 
     * @return
     *     possible object is
     *     {@link GDSInfoType }
     *     
     */
    public GDSInfoType getGDSInfo() {
        return gdsInfo;
    }

    /**
     * Define el valor de la propiedad gdsInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSInfoType }
     *     
     */
    public void setGDSInfo(GDSInfoType value) {
        this.gdsInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad viewerships.
     * 
     * @return
     *     possible object is
     *     {@link ViewershipsType }
     *     
     */
    public ViewershipsType getViewerships() {
        return viewerships;
    }

    /**
     * Define el valor de la propiedad viewerships.
     * 
     * @param value
     *     allowed object is
     *     {@link ViewershipsType }
     *     
     */
    public void setViewerships(ViewershipsType value) {
        this.viewerships = value;
    }

    /**
     * Obtiene el valor de la propiedad effectivePeriods.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveContentType.EffectivePeriods }
     *     
     */
    public HotelDescriptiveContentType.EffectivePeriods getEffectivePeriods() {
        return effectivePeriods;
    }

    /**
     * Define el valor de la propiedad effectivePeriods.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveContentType.EffectivePeriods }
     *     
     */
    public void setEffectivePeriods(HotelDescriptiveContentType.EffectivePeriods value) {
        this.effectivePeriods = value;
    }

    /**
     * Obtiene el valor de la propiedad promotions.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveContentType.Promotions }
     *     
     */
    public HotelDescriptiveContentType.Promotions getPromotions() {
        return promotions;
    }

    /**
     * Define el valor de la propiedad promotions.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveContentType.Promotions }
     *     
     */
    public void setPromotions(HotelDescriptiveContentType.Promotions value) {
        this.promotions = value;
    }

    /**
     * Obtiene el valor de la propiedad roomBlocks.
     * 
     * @return
     *     possible object is
     *     {@link HotelDescriptiveContentType.RoomBlocks }
     *     
     */
    public HotelDescriptiveContentType.RoomBlocks getRoomBlocks() {
        return roomBlocks;
    }

    /**
     * Define el valor de la propiedad roomBlocks.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDescriptiveContentType.RoomBlocks }
     *     
     */
    public void setRoomBlocks(HotelDescriptiveContentType.RoomBlocks value) {
        this.roomBlocks = value;
    }

    /**
     * Obtiene el valor de la propiedad environmentalImpact.
     * 
     * @return
     *     possible object is
     *     {@link EnvironmentalImpactType }
     *     
     */
    public EnvironmentalImpactType getEnvironmentalImpact() {
        return environmentalImpact;
    }

    /**
     * Define el valor de la propiedad environmentalImpact.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvironmentalImpactType }
     *     
     */
    public void setEnvironmentalImpact(EnvironmentalImpactType value) {
        this.environmentalImpact = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad languageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * Define el valor de la propiedad languageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageCode(String value) {
        this.languageCode = value;
    }

    /**
     * Obtiene el valor de la propiedad timeZone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Define el valor de la propiedad timeZone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Obtiene el valor de la propiedad distanceUnitOfMeasureCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceUnitOfMeasureCode() {
        return distanceUnitOfMeasureCode;
    }

    /**
     * Define el valor de la propiedad distanceUnitOfMeasureCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceUnitOfMeasureCode(String value) {
        this.distanceUnitOfMeasureCode = value;
    }

    /**
     * Obtiene el valor de la propiedad areaUnitOfMeasureCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaUnitOfMeasureCode() {
        return areaUnitOfMeasureCode;
    }

    /**
     * Define el valor de la propiedad areaUnitOfMeasureCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaUnitOfMeasureCode(String value) {
        this.areaUnitOfMeasureCode = value;
    }

    /**
     * Obtiene el valor de la propiedad weightUnitOfMeasureCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightUnitOfMeasureCode() {
        return weightUnitOfMeasureCode;
    }

    /**
     * Define el valor de la propiedad weightUnitOfMeasureCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightUnitOfMeasureCode(String value) {
        this.weightUnitOfMeasureCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad decimalPlaces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * Define el valor de la propiedad decimalPlaces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDecimalPlaces(BigInteger value) {
        this.decimalPlaces = value;
    }

    /**
     * Obtiene el valor de la propiedad unitOfMeasureQuantity.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnitOfMeasureQuantity() {
        return unitOfMeasureQuantity;
    }

    /**
     * Define el valor de la propiedad unitOfMeasureQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnitOfMeasureQuantity(BigDecimal value) {
        this.unitOfMeasureQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad unitOfMeasure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Define el valor de la propiedad unitOfMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasure(String value) {
        this.unitOfMeasure = value;
    }

    /**
     * Obtiene el valor de la propiedad unitOfMeasureCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasureCode() {
        return unitOfMeasureCode;
    }

    /**
     * Define el valor de la propiedad unitOfMeasureCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasureCode(String value) {
        this.unitOfMeasureCode = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="EffectivePeriod" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "effectivePeriod"
    })
    public static class EffectivePeriods {

        @XmlElement(name = "EffectivePeriod", required = true)
        protected List<HotelDescriptiveContentType.EffectivePeriods.EffectivePeriod> effectivePeriod;

        /**
         * Gets the value of the effectivePeriod property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the effectivePeriod property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getEffectivePeriod().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelDescriptiveContentType.EffectivePeriods.EffectivePeriod }
         * 
         * 
         */
        public List<HotelDescriptiveContentType.EffectivePeriods.EffectivePeriod> getEffectivePeriod() {
            if (effectivePeriod == null) {
                effectivePeriod = new ArrayList<HotelDescriptiveContentType.EffectivePeriods.EffectivePeriod>();
            }
            return this.effectivePeriod;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class EffectivePeriod {

            @XmlAttribute(name = "StartPeriod")
            protected String startPeriod;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "EndPeriod")
            protected String endPeriod;

            /**
             * Obtiene el valor de la propiedad startPeriod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStartPeriod() {
                return startPeriod;
            }

            /**
             * Define el valor de la propiedad startPeriod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStartPeriod(String value) {
                this.startPeriod = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad endPeriod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndPeriod() {
                return endPeriod;
            }

            /**
             * Define el valor de la propiedad endPeriod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndPeriod(String value) {
                this.endPeriod = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PoliciesType"&amp;gt;
     *       &amp;lt;attribute name="GuaranteeRoomTypeViaGDS" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GuaranteeRoomTypeViaCRC" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GuaranteeRoomTypeViaProperty" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Policies
        extends PoliciesType
    {

        @XmlAttribute(name = "GuaranteeRoomTypeViaGDS")
        protected Boolean guaranteeRoomTypeViaGDS;
        @XmlAttribute(name = "GuaranteeRoomTypeViaCRC")
        protected Boolean guaranteeRoomTypeViaCRC;
        @XmlAttribute(name = "GuaranteeRoomTypeViaProperty")
        protected Boolean guaranteeRoomTypeViaProperty;

        /**
         * Obtiene el valor de la propiedad guaranteeRoomTypeViaGDS.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteeRoomTypeViaGDS() {
            return guaranteeRoomTypeViaGDS;
        }

        /**
         * Define el valor de la propiedad guaranteeRoomTypeViaGDS.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteeRoomTypeViaGDS(Boolean value) {
            this.guaranteeRoomTypeViaGDS = value;
        }

        /**
         * Obtiene el valor de la propiedad guaranteeRoomTypeViaCRC.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteeRoomTypeViaCRC() {
            return guaranteeRoomTypeViaCRC;
        }

        /**
         * Define el valor de la propiedad guaranteeRoomTypeViaCRC.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteeRoomTypeViaCRC(Boolean value) {
            this.guaranteeRoomTypeViaCRC = value;
        }

        /**
         * Obtiene el valor de la propiedad guaranteeRoomTypeViaProperty.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteeRoomTypeViaProperty() {
            return guaranteeRoomTypeViaProperty;
        }

        /**
         * Define el valor de la propiedad guaranteeRoomTypeViaProperty.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteeRoomTypeViaProperty(Boolean value) {
            this.guaranteeRoomTypeViaProperty = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Promotion" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}DescriptionType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="PkgOrPromotion"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="Package"/&amp;gt;
     *                       &amp;lt;enumeration value="Promotion"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="Title" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                 &amp;lt;attribute name="MinLOS" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="Remarks" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;attribute name="SortOrder" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "promotion"
    })
    public static class Promotions {

        @XmlElement(name = "Promotion", required = true)
        protected List<HotelDescriptiveContentType.Promotions.Promotion> promotion;

        /**
         * Gets the value of the promotion property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotion property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPromotion().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelDescriptiveContentType.Promotions.Promotion }
         * 
         * 
         */
        public List<HotelDescriptiveContentType.Promotions.Promotion> getPromotion() {
            if (promotion == null) {
                promotion = new ArrayList<HotelDescriptiveContentType.Promotions.Promotion>();
            }
            return this.promotion;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Description" type="{http://www.opentravel.org/OTA/2003/05}DescriptionType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="PkgOrPromotion"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="Package"/&amp;gt;
         *             &amp;lt;enumeration value="Promotion"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="PromotionCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="Title" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="MinLOS" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="Remarks" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *       &amp;lt;attribute name="SortOrder" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "description"
        })
        public static class Promotion {

            @XmlElement(name = "Description")
            protected DescriptionType description;
            @XmlAttribute(name = "PkgOrPromotion")
            protected String pkgOrPromotion;
            @XmlAttribute(name = "PromotionCode")
            protected String promotionCode;
            @XmlAttribute(name = "Type")
            protected String type;
            @XmlAttribute(name = "Title")
            protected String title;
            @XmlAttribute(name = "MinLOS")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger minLOS;
            @XmlAttribute(name = "Remarks")
            protected String remarks;
            @XmlAttribute(name = "SortOrder")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger sortOrder;
            @XmlAttribute(name = "Mon")
            protected Boolean mon;
            @XmlAttribute(name = "Tue")
            protected Boolean tue;
            @XmlAttribute(name = "Weds")
            protected Boolean weds;
            @XmlAttribute(name = "Thur")
            protected Boolean thur;
            @XmlAttribute(name = "Fri")
            protected Boolean fri;
            @XmlAttribute(name = "Sat")
            protected Boolean sat;
            @XmlAttribute(name = "Sun")
            protected Boolean sun;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link DescriptionType }
             *     
             */
            public DescriptionType getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link DescriptionType }
             *     
             */
            public void setDescription(DescriptionType value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad pkgOrPromotion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPkgOrPromotion() {
                return pkgOrPromotion;
            }

            /**
             * Define el valor de la propiedad pkgOrPromotion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPkgOrPromotion(String value) {
                this.pkgOrPromotion = value;
            }

            /**
             * Obtiene el valor de la propiedad promotionCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPromotionCode() {
                return promotionCode;
            }

            /**
             * Define el valor de la propiedad promotionCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPromotionCode(String value) {
                this.promotionCode = value;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad title.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * Define el valor de la propiedad title.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

            /**
             * Obtiene el valor de la propiedad minLOS.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMinLOS() {
                return minLOS;
            }

            /**
             * Define el valor de la propiedad minLOS.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMinLOS(BigInteger value) {
                this.minLOS = value;
            }

            /**
             * Obtiene el valor de la propiedad remarks.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRemarks() {
                return remarks;
            }

            /**
             * Define el valor de la propiedad remarks.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRemarks(String value) {
                this.remarks = value;
            }

            /**
             * Obtiene el valor de la propiedad sortOrder.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSortOrder() {
                return sortOrder;
            }

            /**
             * Define el valor de la propiedad sortOrder.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSortOrder(BigInteger value) {
                this.sortOrder = value;
            }

            /**
             * Obtiene el valor de la propiedad mon.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isMon() {
                return mon;
            }

            /**
             * Define el valor de la propiedad mon.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setMon(Boolean value) {
                this.mon = value;
            }

            /**
             * Obtiene el valor de la propiedad tue.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isTue() {
                return tue;
            }

            /**
             * Define el valor de la propiedad tue.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setTue(Boolean value) {
                this.tue = value;
            }

            /**
             * Obtiene el valor de la propiedad weds.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isWeds() {
                return weds;
            }

            /**
             * Define el valor de la propiedad weds.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setWeds(Boolean value) {
                this.weds = value;
            }

            /**
             * Obtiene el valor de la propiedad thur.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isThur() {
                return thur;
            }

            /**
             * Define el valor de la propiedad thur.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setThur(Boolean value) {
                this.thur = value;
            }

            /**
             * Obtiene el valor de la propiedad fri.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFri() {
                return fri;
            }

            /**
             * Define el valor de la propiedad fri.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFri(Boolean value) {
                this.fri = value;
            }

            /**
             * Obtiene el valor de la propiedad sat.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSat() {
                return sat;
            }

            /**
             * Define el valor de la propiedad sat.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSat(Boolean value) {
                this.sat = value;
            }

            /**
             * Obtiene el valor de la propiedad sun.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSun() {
                return sun;
            }

            /**
             * Define el valor de la propiedad sun.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSun(Boolean value) {
                this.sun = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="AvailableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="TotalRoomQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="CancelPeriodDays" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="RateType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;union&amp;gt;
     *             &amp;lt;simpleType&amp;gt;
     *               &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
     *                 &amp;lt;enumeration value="NotApplicable"/&amp;gt;
     *                 &amp;lt;enumeration value="Corporate"/&amp;gt;
     *                 &amp;lt;enumeration value="Preferred"/&amp;gt;
     *                 &amp;lt;enumeration value="Rack"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/simpleType&amp;gt;
     *           &amp;lt;/union&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="BlackOutDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RoomBlocks {

        @XmlAttribute(name = "AvailableInd")
        protected Boolean availableInd;
        @XmlAttribute(name = "TotalRoomQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger totalRoomQty;
        @XmlAttribute(name = "CancelPeriodDays")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger cancelPeriodDays;
        @XmlAttribute(name = "RateType")
        protected String rateType;
        @XmlAttribute(name = "RoomType")
        protected String roomType;
        @XmlAttribute(name = "BlackOutDate")
        protected String blackOutDate;

        /**
         * Obtiene el valor de la propiedad availableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAvailableInd() {
            return availableInd;
        }

        /**
         * Define el valor de la propiedad availableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAvailableInd(Boolean value) {
            this.availableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad totalRoomQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalRoomQty() {
            return totalRoomQty;
        }

        /**
         * Define el valor de la propiedad totalRoomQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalRoomQty(BigInteger value) {
            this.totalRoomQty = value;
        }

        /**
         * Obtiene el valor de la propiedad cancelPeriodDays.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCancelPeriodDays() {
            return cancelPeriodDays;
        }

        /**
         * Define el valor de la propiedad cancelPeriodDays.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCancelPeriodDays(BigInteger value) {
            this.cancelPeriodDays = value;
        }

        /**
         * Obtiene el valor de la propiedad rateType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRateType() {
            return rateType;
        }

        /**
         * Define el valor de la propiedad rateType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRateType(String value) {
            this.rateType = value;
        }

        /**
         * Obtiene el valor de la propiedad roomType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoomType() {
            return roomType;
        }

        /**
         * Define el valor de la propiedad roomType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoomType(String value) {
            this.roomType = value;
        }

        /**
         * Obtiene el valor de la propiedad blackOutDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBlackOutDate() {
            return blackOutDate;
        }

        /**
         * Define el valor de la propiedad blackOutDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBlackOutDate(String value) {
            this.blackOutDate = value;
        }

    }

}
