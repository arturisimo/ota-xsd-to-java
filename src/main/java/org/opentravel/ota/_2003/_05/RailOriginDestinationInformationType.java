
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines rail journey origin and destination location, and time information for the request. Also includes the ability to specify a connection location for the search.
 * 
 * &lt;p&gt;Clase Java para RailOriginDestinationInformationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RailOriginDestinationInformationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TravelDateTimeType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="OriginLocation"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                 &amp;lt;attribute name="MultiCityStationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DestinationLocation"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                 &amp;lt;attribute name="MultiCityStationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ConnectionLocations" type="{http://www.opentravel.org/OTA/2003/05}RailConnectionType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailOriginDestinationInformationType", propOrder = {
    "originLocation",
    "destinationLocation",
    "connectionLocations"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTARailAvailRS.OriginDestinationInformation.class,
    org.opentravel.ota._2003._05.OTARailShopRS.OriginDestinationInformation.class
})
public class RailOriginDestinationInformationType
    extends TravelDateTimeType
{

    @XmlElement(name = "OriginLocation", required = true)
    protected RailOriginDestinationInformationType.OriginLocation originLocation;
    @XmlElement(name = "DestinationLocation", required = true)
    protected RailOriginDestinationInformationType.DestinationLocation destinationLocation;
    @XmlElement(name = "ConnectionLocations")
    protected RailConnectionType connectionLocations;

    /**
     * Obtiene el valor de la propiedad originLocation.
     * 
     * @return
     *     possible object is
     *     {@link RailOriginDestinationInformationType.OriginLocation }
     *     
     */
    public RailOriginDestinationInformationType.OriginLocation getOriginLocation() {
        return originLocation;
    }

    /**
     * Define el valor de la propiedad originLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link RailOriginDestinationInformationType.OriginLocation }
     *     
     */
    public void setOriginLocation(RailOriginDestinationInformationType.OriginLocation value) {
        this.originLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationLocation.
     * 
     * @return
     *     possible object is
     *     {@link RailOriginDestinationInformationType.DestinationLocation }
     *     
     */
    public RailOriginDestinationInformationType.DestinationLocation getDestinationLocation() {
        return destinationLocation;
    }

    /**
     * Define el valor de la propiedad destinationLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link RailOriginDestinationInformationType.DestinationLocation }
     *     
     */
    public void setDestinationLocation(RailOriginDestinationInformationType.DestinationLocation value) {
        this.destinationLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad connectionLocations.
     * 
     * @return
     *     possible object is
     *     {@link RailConnectionType }
     *     
     */
    public RailConnectionType getConnectionLocations() {
        return connectionLocations;
    }

    /**
     * Define el valor de la propiedad connectionLocations.
     * 
     * @param value
     *     allowed object is
     *     {@link RailConnectionType }
     *     
     */
    public void setConnectionLocations(RailConnectionType value) {
        this.connectionLocations = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *       &amp;lt;attribute name="MultiCityStationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DestinationLocation
        extends LocationType
    {

        @XmlAttribute(name = "MultiCityStationInd")
        protected Boolean multiCityStationInd;
        @XmlAttribute(name = "AlternateLocationInd")
        protected Boolean alternateLocationInd;

        /**
         * Obtiene el valor de la propiedad multiCityStationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMultiCityStationInd() {
            return multiCityStationInd;
        }

        /**
         * Define el valor de la propiedad multiCityStationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMultiCityStationInd(Boolean value) {
            this.multiCityStationInd = value;
        }

        /**
         * Obtiene el valor de la propiedad alternateLocationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAlternateLocationInd() {
            return alternateLocationInd;
        }

        /**
         * Define el valor de la propiedad alternateLocationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAlternateLocationInd(Boolean value) {
            this.alternateLocationInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *       &amp;lt;attribute name="MultiCityStationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="AlternateLocationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OriginLocation
        extends LocationType
    {

        @XmlAttribute(name = "MultiCityStationInd")
        protected Boolean multiCityStationInd;
        @XmlAttribute(name = "AlternateLocationInd")
        protected Boolean alternateLocationInd;

        /**
         * Obtiene el valor de la propiedad multiCityStationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMultiCityStationInd() {
            return multiCityStationInd;
        }

        /**
         * Define el valor de la propiedad multiCityStationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMultiCityStationInd(Boolean value) {
            this.multiCityStationInd = value;
        }

        /**
         * Obtiene el valor de la propiedad alternateLocationInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAlternateLocationInd() {
            return alternateLocationInd;
        }

        /**
         * Define el valor de la propiedad alternateLocationInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAlternateLocationInd(Boolean value) {
            this.alternateLocationInd = value;
        }

    }

}
