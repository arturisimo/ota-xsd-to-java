
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A collection of package option search criteria.
 * 
 * &lt;p&gt;Clase Java para DynamicPkgOptionSearchType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DynamicPkgOptionSearchType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgSearchType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="OptionSearchCriteria" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Criterion" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionSearchCriterionType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="AvailableOnlyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="RequestType" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionComponentSearchType" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicPkgOptionSearchType", propOrder = {
    "optionSearchCriteria",
    "tpaExtensions"
})
public class DynamicPkgOptionSearchType
    extends DynamicPkgSearchType
{

    @XmlElement(name = "OptionSearchCriteria")
    protected DynamicPkgOptionSearchType.OptionSearchCriteria optionSearchCriteria;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "RequestType")
    protected PackageOptionComponentSearchType requestType;

    /**
     * Obtiene el valor de la propiedad optionSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link DynamicPkgOptionSearchType.OptionSearchCriteria }
     *     
     */
    public DynamicPkgOptionSearchType.OptionSearchCriteria getOptionSearchCriteria() {
        return optionSearchCriteria;
    }

    /**
     * Define el valor de la propiedad optionSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicPkgOptionSearchType.OptionSearchCriteria }
     *     
     */
    public void setOptionSearchCriteria(DynamicPkgOptionSearchType.OptionSearchCriteria value) {
        this.optionSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad requestType.
     * 
     * @return
     *     possible object is
     *     {@link PackageOptionComponentSearchType }
     *     
     */
    public PackageOptionComponentSearchType getRequestType() {
        return requestType;
    }

    /**
     * Define el valor de la propiedad requestType.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageOptionComponentSearchType }
     *     
     */
    public void setRequestType(PackageOptionComponentSearchType value) {
        this.requestType = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Criterion" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionSearchCriterionType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="AvailableOnlyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "criterion"
    })
    public static class OptionSearchCriteria {

        @XmlElement(name = "Criterion", required = true)
        protected List<PackageOptionSearchCriterionType> criterion;
        @XmlAttribute(name = "AvailableOnlyIndicator")
        protected Boolean availableOnlyIndicator;

        /**
         * Gets the value of the criterion property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the criterion property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCriterion().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PackageOptionSearchCriterionType }
         * 
         * 
         */
        public List<PackageOptionSearchCriterionType> getCriterion() {
            if (criterion == null) {
                criterion = new ArrayList<PackageOptionSearchCriterionType>();
            }
            return this.criterion;
        }

        /**
         * Obtiene el valor de la propiedad availableOnlyIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAvailableOnlyIndicator() {
            return availableOnlyIndicator;
        }

        /**
         * Define el valor de la propiedad availableOnlyIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAvailableOnlyIndicator(Boolean value) {
            this.availableOnlyIndicator = value;
        }

    }

}
