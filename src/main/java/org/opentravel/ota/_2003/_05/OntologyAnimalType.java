
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Animal type, weight with ontology reference.
 * 
 * &lt;p&gt;Clase Java para OntologyAnimalType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OntologyAnimalType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Type"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;simpleContent&amp;gt;
 *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferPetType"&amp;gt;
 *                           &amp;lt;attribute name="OtherType"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/simpleContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Weight" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyWeightType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ServiceAnimalInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OntologyAnimalType", propOrder = {
    "detail"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.MultiModalOfferType.TripCharacteristics.Animals.class
})
public class OntologyAnimalType {

    @XmlElement(name = "Detail")
    protected List<OntologyAnimalType.Detail> detail;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger quantity;
    @XmlAttribute(name = "ServiceAnimalInd")
    protected Boolean serviceAnimalInd;

    /**
     * Gets the value of the detail property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detail property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDetail().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OntologyAnimalType.Detail }
     * 
     * 
     */
    public List<OntologyAnimalType.Detail> getDetail() {
        if (detail == null) {
            detail = new ArrayList<OntologyAnimalType.Detail>();
        }
        return this.detail;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceAnimalInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isServiceAnimalInd() {
        return serviceAnimalInd;
    }

    /**
     * Define el valor de la propiedad serviceAnimalInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServiceAnimalInd(Boolean value) {
        this.serviceAnimalInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Type"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferPetType"&amp;gt;
     *                 &amp;lt;attribute name="OtherType"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Weight" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyWeightType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "type",
        "weight",
        "ontologyExtension"
    })
    public static class Detail {

        @XmlElement(name = "Type", required = true)
        protected OntologyAnimalType.Detail.Type type;
        @XmlElement(name = "Weight")
        protected OntologyAnimalType.Detail.Weight weight;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link OntologyAnimalType.Detail.Type }
         *     
         */
        public OntologyAnimalType.Detail.Type getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyAnimalType.Detail.Type }
         *     
         */
        public void setType(OntologyAnimalType.Detail.Type value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad weight.
         * 
         * @return
         *     possible object is
         *     {@link OntologyAnimalType.Detail.Weight }
         *     
         */
        public OntologyAnimalType.Detail.Weight getWeight() {
            return weight;
        }

        /**
         * Define el valor de la propiedad weight.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyAnimalType.Detail.Weight }
         *     
         */
        public void setWeight(OntologyAnimalType.Detail.Weight value) {
            this.weight = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferPetType"&amp;gt;
         *       &amp;lt;attribute name="OtherType"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Type {

            @XmlValue
            protected ListOfferPetType value;
            @XmlAttribute(name = "OtherType")
            protected String otherType;
            @XmlAttribute(name = "OntologyRefID")
            protected String ontologyRefID;

            /**
             * Source: Pets Policy Code (PET) OpenTravel codelist.
             * 
             * @return
             *     possible object is
             *     {@link ListOfferPetType }
             *     
             */
            public ListOfferPetType getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link ListOfferPetType }
             *     
             */
            public void setValue(ListOfferPetType value) {
                this.value = value;
            }

            /**
             * Obtiene el valor de la propiedad otherType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherType() {
                return otherType;
            }

            /**
             * Define el valor de la propiedad otherType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherType(String value) {
                this.otherType = value;
            }

            /**
             * Obtiene el valor de la propiedad ontologyRefID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOntologyRefID() {
                return ontologyRefID;
            }

            /**
             * Define el valor de la propiedad ontologyRefID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOntologyRefID(String value) {
                this.ontologyRefID = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyWeightType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Weight
            extends OntologyWeightType
        {


        }

    }

}
