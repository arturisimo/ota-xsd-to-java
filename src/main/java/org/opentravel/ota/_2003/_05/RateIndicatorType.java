
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para RateIndicatorType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="RateIndicatorType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
 *     &amp;lt;enumeration value="ChangeDuringStay"/&amp;gt;
 *     &amp;lt;enumeration value="MultipleNights"/&amp;gt;
 *     &amp;lt;enumeration value="Exclusive"/&amp;gt;
 *     &amp;lt;enumeration value="OnRequest"/&amp;gt;
 *     &amp;lt;enumeration value="LimitedAvailability"/&amp;gt;
 *     &amp;lt;enumeration value="AvailableForSale"/&amp;gt;
 *     &amp;lt;enumeration value="ClosedOut"/&amp;gt;
 *     &amp;lt;enumeration value="OtherAvailable"/&amp;gt;
 *     &amp;lt;enumeration value="UnableToProcess"/&amp;gt;
 *     &amp;lt;enumeration value="NoAvailability"/&amp;gt;
 *     &amp;lt;enumeration value="RoomTypeClosed"/&amp;gt;
 *     &amp;lt;enumeration value="RatePlanClosed"/&amp;gt;
 *     &amp;lt;enumeration value="LOS_Restricted"/&amp;gt;
 *     &amp;lt;enumeration value="Restricted"/&amp;gt;
 *     &amp;lt;enumeration value="DoesNotExist"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "RateIndicatorType")
@XmlEnum
public enum RateIndicatorType {

    @XmlEnumValue("ChangeDuringStay")
    CHANGE_DURING_STAY("ChangeDuringStay"),
    @XmlEnumValue("MultipleNights")
    MULTIPLE_NIGHTS("MultipleNights"),

    /**
     * Availability is limited based on guest qualification criteria e.g. AAA member or Government Employee
     * 
     */
    @XmlEnumValue("Exclusive")
    EXCLUSIVE("Exclusive"),
    @XmlEnumValue("OnRequest")
    ON_REQUEST("OnRequest"),
    @XmlEnumValue("LimitedAvailability")
    LIMITED_AVAILABILITY("LimitedAvailability"),
    @XmlEnumValue("AvailableForSale")
    AVAILABLE_FOR_SALE("AvailableForSale"),
    @XmlEnumValue("ClosedOut")
    CLOSED_OUT("ClosedOut"),
    @XmlEnumValue("OtherAvailable")
    OTHER_AVAILABLE("OtherAvailable"),

    /**
     * Indicates an issue that precluded the ability to provide the information.
     * 
     */
    @XmlEnumValue("UnableToProcess")
    UNABLE_TO_PROCESS("UnableToProcess"),
    @XmlEnumValue("NoAvailability")
    NO_AVAILABILITY("NoAvailability"),
    @XmlEnumValue("RoomTypeClosed")
    ROOM_TYPE_CLOSED("RoomTypeClosed"),
    @XmlEnumValue("RatePlanClosed")
    RATE_PLAN_CLOSED("RatePlanClosed"),
    @XmlEnumValue("LOS_Restricted")
    LOS_RESTRICTED("LOS_Restricted"),

    /**
     * Availability is limited based on distribution channel qualification criteria (e.g., Expedia or Sabre).
     * 
     */
    @XmlEnumValue("Restricted")
    RESTRICTED("Restricted"),

    /**
     * The rate plan does not exist.
     * 
     */
    @XmlEnumValue("DoesNotExist")
    DOES_NOT_EXIST("DoesNotExist");
    private final String value;

    RateIndicatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RateIndicatorType fromValue(String v) {
        for (RateIndicatorType c: RateIndicatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
