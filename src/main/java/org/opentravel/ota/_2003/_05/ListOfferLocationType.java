
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferLocationType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferLocationType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Origin"/&amp;gt;
 *     &amp;lt;enumeration value="Destination"/&amp;gt;
 *     &amp;lt;enumeration value="SingleLocation"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferLocationType")
@XmlEnum
public enum ListOfferLocationType {


    /**
     * Trip mode origin or starting location.
     * 
     */
    @XmlEnumValue("Origin")
    ORIGIN("Origin"),

    /**
     * Trip mode ending or destination location.
     * 
     */
    @XmlEnumValue("Destination")
    DESTINATION("Destination"),

    /**
     * Trip mode start and end locations are the same.
     * 
     */
    @XmlEnumValue("SingleLocation")
    SINGLE_LOCATION("SingleLocation");
    private final String value;

    ListOfferLocationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferLocationType fromValue(String v) {
        for (ListOfferLocationType c: ListOfferLocationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
