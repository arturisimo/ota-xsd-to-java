
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Provides information about donations made during a booking.
 * 
 * &lt;p&gt;Clase Java para DonationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DonationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FrontOfficeInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="ProductName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="ProductVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="OfficeID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="CorporateID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CreditCardInfo"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentCardType"&amp;gt;
 *                 &amp;lt;attribute name="Currency" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                 &amp;lt;attribute name="DonationAmount" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DonorInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Name" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ContactInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
 *                           &amp;lt;attribute name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Language" use="required" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *       &amp;lt;attribute name="GDS_ID"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="1A"/&amp;gt;
 *             &amp;lt;enumeration value="1G"/&amp;gt;
 *             &amp;lt;enumeration value="1P"/&amp;gt;
 *             &amp;lt;enumeration value="1V"/&amp;gt;
 *             &amp;lt;enumeration value="1W"/&amp;gt;
 *             &amp;lt;enumeration value="WE"/&amp;gt;
 *             &amp;lt;enumeration value="WS"/&amp;gt;
 *             &amp;lt;enumeration value="MF"/&amp;gt;
 *             &amp;lt;enumeration value="ZZ"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="AskForReceiptInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="CountryCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *       &amp;lt;attribute name="StateCode" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DonationType", propOrder = {
    "frontOfficeInfo",
    "creditCardInfo",
    "donorInfo",
    "tpaExtensions"
})
public class DonationType {

    @XmlElement(name = "FrontOfficeInfo")
    protected DonationType.FrontOfficeInfo frontOfficeInfo;
    @XmlElement(name = "CreditCardInfo", required = true)
    protected DonationType.CreditCardInfo creditCardInfo;
    @XmlElement(name = "DonorInfo")
    protected DonationType.DonorInfo donorInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "Language", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;
    @XmlAttribute(name = "GDS_ID")
    protected String gdsid;
    @XmlAttribute(name = "AskForReceiptInd", required = true)
    protected boolean askForReceiptInd;
    @XmlAttribute(name = "CountryCode", required = true)
    protected String countryCode;
    @XmlAttribute(name = "StateCode")
    protected String stateCode;

    /**
     * Obtiene el valor de la propiedad frontOfficeInfo.
     * 
     * @return
     *     possible object is
     *     {@link DonationType.FrontOfficeInfo }
     *     
     */
    public DonationType.FrontOfficeInfo getFrontOfficeInfo() {
        return frontOfficeInfo;
    }

    /**
     * Define el valor de la propiedad frontOfficeInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link DonationType.FrontOfficeInfo }
     *     
     */
    public void setFrontOfficeInfo(DonationType.FrontOfficeInfo value) {
        this.frontOfficeInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad creditCardInfo.
     * 
     * @return
     *     possible object is
     *     {@link DonationType.CreditCardInfo }
     *     
     */
    public DonationType.CreditCardInfo getCreditCardInfo() {
        return creditCardInfo;
    }

    /**
     * Define el valor de la propiedad creditCardInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link DonationType.CreditCardInfo }
     *     
     */
    public void setCreditCardInfo(DonationType.CreditCardInfo value) {
        this.creditCardInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad donorInfo.
     * 
     * @return
     *     possible object is
     *     {@link DonationType.DonorInfo }
     *     
     */
    public DonationType.DonorInfo getDonorInfo() {
        return donorInfo;
    }

    /**
     * Define el valor de la propiedad donorInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link DonationType.DonorInfo }
     *     
     */
    public void setDonorInfo(DonationType.DonorInfo value) {
        this.donorInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad language.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Define el valor de la propiedad language.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Obtiene el valor de la propiedad gdsid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGDSID() {
        return gdsid;
    }

    /**
     * Define el valor de la propiedad gdsid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGDSID(String value) {
        this.gdsid = value;
    }

    /**
     * Obtiene el valor de la propiedad askForReceiptInd.
     * 
     */
    public boolean isAskForReceiptInd() {
        return askForReceiptInd;
    }

    /**
     * Define el valor de la propiedad askForReceiptInd.
     * 
     */
    public void setAskForReceiptInd(boolean value) {
        this.askForReceiptInd = value;
    }

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad stateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateCode() {
        return stateCode;
    }

    /**
     * Define el valor de la propiedad stateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateCode(String value) {
        this.stateCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentCardType"&amp;gt;
     *       &amp;lt;attribute name="Currency" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *       &amp;lt;attribute name="DonationAmount" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CreditCardInfo
        extends PaymentCardType
    {

        @XmlAttribute(name = "Currency", required = true)
        protected String currency;
        @XmlAttribute(name = "DonationAmount", required = true)
        protected BigDecimal donationAmount;

        /**
         * Obtiene el valor de la propiedad currency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrency() {
            return currency;
        }

        /**
         * Define el valor de la propiedad currency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrency(String value) {
            this.currency = value;
        }

        /**
         * Obtiene el valor de la propiedad donationAmount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getDonationAmount() {
            return donationAmount;
        }

        /**
         * Define el valor de la propiedad donationAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setDonationAmount(BigDecimal value) {
            this.donationAmount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Name" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ContactInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
     *                 &amp;lt;attribute name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name",
        "contactInfo"
    })
    public static class DonorInfo {

        @XmlElement(name = "Name")
        protected DonationType.DonorInfo.Name name;
        @XmlElement(name = "ContactInfo")
        protected DonationType.DonorInfo.ContactInfo contactInfo;

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link DonationType.DonorInfo.Name }
         *     
         */
        public DonationType.DonorInfo.Name getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link DonationType.DonorInfo.Name }
         *     
         */
        public void setName(DonationType.DonorInfo.Name value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad contactInfo.
         * 
         * @return
         *     possible object is
         *     {@link DonationType.DonorInfo.ContactInfo }
         *     
         */
        public DonationType.DonorInfo.ContactInfo getContactInfo() {
            return contactInfo;
        }

        /**
         * Define el valor de la propiedad contactInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link DonationType.DonorInfo.ContactInfo }
         *     
         */
        public void setContactInfo(DonationType.DonorInfo.ContactInfo value) {
            this.contactInfo = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
         *       &amp;lt;attribute name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ContactInfo
            extends AddressType
        {

            @XmlAttribute(name = "EmailAddress")
            protected String emailAddress;

            /**
             * Obtiene el valor de la propiedad emailAddress.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmailAddress() {
                return emailAddress;
            }

            /**
             * Define el valor de la propiedad emailAddress.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmailAddress(String value) {
                this.emailAddress = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Name
            extends PersonNameType
        {


        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="ProductName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="ProductVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="OfficeID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="CorporateID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FrontOfficeInfo {

        @XmlAttribute(name = "ProductName", required = true)
        protected String productName;
        @XmlAttribute(name = "ProductVersion", required = true)
        protected String productVersion;
        @XmlAttribute(name = "OfficeID", required = true)
        protected String officeID;
        @XmlAttribute(name = "CorporateID", required = true)
        protected String corporateID;

        /**
         * Obtiene el valor de la propiedad productName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductName() {
            return productName;
        }

        /**
         * Define el valor de la propiedad productName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductName(String value) {
            this.productName = value;
        }

        /**
         * Obtiene el valor de la propiedad productVersion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductVersion() {
            return productVersion;
        }

        /**
         * Define el valor de la propiedad productVersion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductVersion(String value) {
            this.productVersion = value;
        }

        /**
         * Obtiene el valor de la propiedad officeID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfficeID() {
            return officeID;
        }

        /**
         * Define el valor de la propiedad officeID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfficeID(String value) {
            this.officeID = value;
        }

        /**
         * Obtiene el valor de la propiedad corporateID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCorporateID() {
            return corporateID;
        }

        /**
         * Define el valor de la propiedad corporateID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCorporateID(String value) {
            this.corporateID = value;
        }

    }

}
