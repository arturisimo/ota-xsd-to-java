
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para AgeQualifyingEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AgeQualifyingEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Additional occupant with adult"/&amp;gt;
 *     &amp;lt;enumeration value="Additional occupant without adult"/&amp;gt;
 *     &amp;lt;enumeration value="Adult"/&amp;gt;
 *     &amp;lt;enumeration value="Child"/&amp;gt;
 *     &amp;lt;enumeration value="Free adult"/&amp;gt;
 *     &amp;lt;enumeration value="Free child"/&amp;gt;
 *     &amp;lt;enumeration value="Infant"/&amp;gt;
 *     &amp;lt;enumeration value="Over 21"/&amp;gt;
 *     &amp;lt;enumeration value="Over 65"/&amp;gt;
 *     &amp;lt;enumeration value="Senior "/&amp;gt;
 *     &amp;lt;enumeration value="Teenager"/&amp;gt;
 *     &amp;lt;enumeration value="Under 10"/&amp;gt;
 *     &amp;lt;enumeration value="Under 12"/&amp;gt;
 *     &amp;lt;enumeration value="Under 17"/&amp;gt;
 *     &amp;lt;enumeration value="Under 2"/&amp;gt;
 *     &amp;lt;enumeration value="Under 21"/&amp;gt;
 *     &amp;lt;enumeration value="Young driver"/&amp;gt;
 *     &amp;lt;enumeration value="Younger driver"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AgeQualifyingEnum")
@XmlEnum
public enum AgeQualifyingEnum {

    @XmlEnumValue("Additional occupant with adult")
    ADDITIONAL_OCCUPANT_WITH_ADULT("Additional occupant with adult"),
    @XmlEnumValue("Additional occupant without adult")
    ADDITIONAL_OCCUPANT_WITHOUT_ADULT("Additional occupant without adult"),
    @XmlEnumValue("Adult")
    ADULT("Adult"),
    @XmlEnumValue("Child")
    CHILD("Child"),
    @XmlEnumValue("Free adult")
    FREE_ADULT("Free adult"),
    @XmlEnumValue("Free child")
    FREE_CHILD("Free child"),
    @XmlEnumValue("Infant")
    INFANT("Infant"),
    @XmlEnumValue("Over 21")
    OVER_21("Over 21"),
    @XmlEnumValue("Over 65")
    OVER_65("Over 65"),
    @XmlEnumValue("Senior ")
    SENIOR("Senior "),
    @XmlEnumValue("Teenager")
    TEENAGER("Teenager"),
    @XmlEnumValue("Under 10")
    UNDER_10("Under 10"),
    @XmlEnumValue("Under 12")
    UNDER_12("Under 12"),
    @XmlEnumValue("Under 17")
    UNDER_17("Under 17"),
    @XmlEnumValue("Under 2")
    UNDER_2("Under 2"),
    @XmlEnumValue("Under 21")
    UNDER_21("Under 21"),
    @XmlEnumValue("Young driver")
    YOUNG_DRIVER("Young driver"),
    @XmlEnumValue("Younger driver")
    YOUNGER_DRIVER("Younger driver"),
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    AgeQualifyingEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AgeQualifyingEnum fromValue(String v) {
        for (AgeQualifyingEnum c: AgeQualifyingEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
