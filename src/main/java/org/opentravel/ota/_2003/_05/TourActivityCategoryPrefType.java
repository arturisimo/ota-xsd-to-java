
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Tour/ activity category and type definitions with preference designators.
 * 
 * &lt;p&gt;Clase Java para TourActivityCategoryPrefType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityCategoryPrefType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Category" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityCategoryEnum"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="Extension"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Cultural"/&amp;gt;
 *                       &amp;lt;enumeration value="EcoAdventure"/&amp;gt;
 *                       &amp;lt;enumeration value="Educational"/&amp;gt;
 *                       &amp;lt;enumeration value="FamilyFun"/&amp;gt;
 *                       &amp;lt;enumeration value="Free"/&amp;gt;
 *                       &amp;lt;enumeration value="FoodBeverage"/&amp;gt;
 *                       &amp;lt;enumeration value="Romantic"/&amp;gt;
 *                       &amp;lt;enumeration value="SportsRecreation"/&amp;gt;
 *                       &amp;lt;enumeration value="WheelchairAccess"/&amp;gt;
 *                       &amp;lt;enumeration value="Other"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Type" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityTypeEnum"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="Extension"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Cultural"/&amp;gt;
 *                       &amp;lt;enumeration value="EcoAdventure"/&amp;gt;
 *                       &amp;lt;enumeration value="Educational"/&amp;gt;
 *                       &amp;lt;enumeration value="FamilyFun"/&amp;gt;
 *                       &amp;lt;enumeration value="Free"/&amp;gt;
 *                       &amp;lt;enumeration value="FoodBeverage"/&amp;gt;
 *                       &amp;lt;enumeration value="Romantic"/&amp;gt;
 *                       &amp;lt;enumeration value="SportsRecreation"/&amp;gt;
 *                       &amp;lt;enumeration value="WheelchairAccess"/&amp;gt;
 *                       &amp;lt;enumeration value="Other"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityCategoryPrefType", propOrder = {
    "category",
    "type",
    "tpaExtensions"
})
public class TourActivityCategoryPrefType {

    @XmlElement(name = "Category")
    protected TourActivityCategoryPrefType.Category category;
    @XmlElement(name = "Type")
    protected List<TourActivityCategoryPrefType.Type> type;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityCategoryPrefType.Category }
     *     
     */
    public TourActivityCategoryPrefType.Category getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityCategoryPrefType.Category }
     *     
     */
    public void setCategory(TourActivityCategoryPrefType.Category value) {
        this.category = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the type property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getType().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TourActivityCategoryPrefType.Type }
     * 
     * 
     */
    public List<TourActivityCategoryPrefType.Type> getType() {
        if (type == null) {
            type = new ArrayList<TourActivityCategoryPrefType.Type>();
        }
        return this.type;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityCategoryEnum"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="Extension"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Cultural"/&amp;gt;
     *             &amp;lt;enumeration value="EcoAdventure"/&amp;gt;
     *             &amp;lt;enumeration value="Educational"/&amp;gt;
     *             &amp;lt;enumeration value="FamilyFun"/&amp;gt;
     *             &amp;lt;enumeration value="Free"/&amp;gt;
     *             &amp;lt;enumeration value="FoodBeverage"/&amp;gt;
     *             &amp;lt;enumeration value="Romantic"/&amp;gt;
     *             &amp;lt;enumeration value="SportsRecreation"/&amp;gt;
     *             &amp;lt;enumeration value="WheelchairAccess"/&amp;gt;
     *             &amp;lt;enumeration value="Other"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Category {

        @XmlValue
        protected TourActivityCategoryEnum value;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "Extension")
        protected String extension;
        @XmlAttribute(name = "PreferLevel")
        protected PreferLevelType preferLevel;

        /**
         * Tour and activity categories enum with an "Other" value to support an open enumeration list as agreed upon between trading partners.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityCategoryEnum }
         *     
         */
        public TourActivityCategoryEnum getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityCategoryEnum }
         *     
         */
        public void setValue(TourActivityCategoryEnum value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad extension.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtension() {
            return extension;
        }

        /**
         * Define el valor de la propiedad extension.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtension(String value) {
            this.extension = value;
        }

        /**
         * Obtiene el valor de la propiedad preferLevel.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreferLevel() {
            return preferLevel;
        }

        /**
         * Define el valor de la propiedad preferLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreferLevel(PreferLevelType value) {
            this.preferLevel = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;TourActivityTypeEnum"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PreferLevelGroup"/&amp;gt;
     *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="Extension"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Cultural"/&amp;gt;
     *             &amp;lt;enumeration value="EcoAdventure"/&amp;gt;
     *             &amp;lt;enumeration value="Educational"/&amp;gt;
     *             &amp;lt;enumeration value="FamilyFun"/&amp;gt;
     *             &amp;lt;enumeration value="Free"/&amp;gt;
     *             &amp;lt;enumeration value="FoodBeverage"/&amp;gt;
     *             &amp;lt;enumeration value="Romantic"/&amp;gt;
     *             &amp;lt;enumeration value="SportsRecreation"/&amp;gt;
     *             &amp;lt;enumeration value="WheelchairAccess"/&amp;gt;
     *             &amp;lt;enumeration value="Other"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Type {

        @XmlValue
        protected TourActivityTypeEnum value;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "Extension")
        protected String extension;
        @XmlAttribute(name = "PreferLevel")
        protected PreferLevelType preferLevel;

        /**
         * Tour and activity types enum with an "Other" value to support an open enumeration list as agreed upon between trading partners.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityTypeEnum }
         *     
         */
        public TourActivityTypeEnum getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityTypeEnum }
         *     
         */
        public void setValue(TourActivityTypeEnum value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad extension.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtension() {
            return extension;
        }

        /**
         * Define el valor de la propiedad extension.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtension(String value) {
            this.extension = value;
        }

        /**
         * Obtiene el valor de la propiedad preferLevel.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreferLevel() {
            return preferLevel;
        }

        /**
         * Define el valor de la propiedad preferLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreferLevel(PreferLevelType value) {
            this.preferLevel = value;
        }

    }

}
