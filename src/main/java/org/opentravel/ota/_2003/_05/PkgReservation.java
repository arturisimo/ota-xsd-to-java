
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Details of a Package Holiday reservation object.
 * 
 * &lt;p&gt;Clase Java para PkgReservation complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PkgReservation"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"/&amp;gt;
 *         &amp;lt;element name="Package" type="{http://www.opentravel.org/OTA/2003/05}PackageResponseType"/&amp;gt;
 *         &amp;lt;element name="ContactDetail" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
 *         &amp;lt;element name="PassengerListItems"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PassengerListItem" type="{http://www.opentravel.org/OTA/2003/05}PkgPassengerListItem" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OwnInsuranceChoices" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="OwnInsuranceChoice" type="{http://www.opentravel.org/OTA/2003/05}OwnInsuranceChoiceType" maxOccurs="9"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TicketingInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type"&amp;gt;
 *                 &amp;lt;attribute name="TOD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="InvoiceDetail" type="{http://www.opentravel.org/OTA/2003/05}PkgInvoiceDetail"/&amp;gt;
 *         &amp;lt;element name="Transactions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Transaction" type="{http://www.opentravel.org/OTA/2003/05}TransactionType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PkgReservation", propOrder = {
    "pos",
    "uniqueID",
    "_package",
    "contactDetail",
    "passengerListItems",
    "ownInsuranceChoices",
    "ticketingInfo",
    "invoiceDetail",
    "transactions",
    "tpaExtensions"
})
public class PkgReservation {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "UniqueID", required = true)
    protected UniqueIDType uniqueID;
    @XmlElement(name = "Package", required = true)
    protected PackageResponseType _package;
    @XmlElement(name = "ContactDetail", required = true)
    protected ContactPersonType contactDetail;
    @XmlElement(name = "PassengerListItems", required = true)
    protected PkgReservation.PassengerListItems passengerListItems;
    @XmlElement(name = "OwnInsuranceChoices")
    protected PkgReservation.OwnInsuranceChoices ownInsuranceChoices;
    @XmlElement(name = "TicketingInfo")
    protected PkgReservation.TicketingInfo ticketingInfo;
    @XmlElement(name = "InvoiceDetail", required = true)
    protected PkgInvoiceDetail invoiceDetail;
    @XmlElement(name = "Transactions")
    protected PkgReservation.Transactions transactions;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad uniqueID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getUniqueID() {
        return uniqueID;
    }

    /**
     * Define el valor de la propiedad uniqueID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setUniqueID(UniqueIDType value) {
        this.uniqueID = value;
    }

    /**
     * Obtiene el valor de la propiedad package.
     * 
     * @return
     *     possible object is
     *     {@link PackageResponseType }
     *     
     */
    public PackageResponseType getPackage() {
        return _package;
    }

    /**
     * Define el valor de la propiedad package.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageResponseType }
     *     
     */
    public void setPackage(PackageResponseType value) {
        this._package = value;
    }

    /**
     * Obtiene el valor de la propiedad contactDetail.
     * 
     * @return
     *     possible object is
     *     {@link ContactPersonType }
     *     
     */
    public ContactPersonType getContactDetail() {
        return contactDetail;
    }

    /**
     * Define el valor de la propiedad contactDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPersonType }
     *     
     */
    public void setContactDetail(ContactPersonType value) {
        this.contactDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad passengerListItems.
     * 
     * @return
     *     possible object is
     *     {@link PkgReservation.PassengerListItems }
     *     
     */
    public PkgReservation.PassengerListItems getPassengerListItems() {
        return passengerListItems;
    }

    /**
     * Define el valor de la propiedad passengerListItems.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgReservation.PassengerListItems }
     *     
     */
    public void setPassengerListItems(PkgReservation.PassengerListItems value) {
        this.passengerListItems = value;
    }

    /**
     * Obtiene el valor de la propiedad ownInsuranceChoices.
     * 
     * @return
     *     possible object is
     *     {@link PkgReservation.OwnInsuranceChoices }
     *     
     */
    public PkgReservation.OwnInsuranceChoices getOwnInsuranceChoices() {
        return ownInsuranceChoices;
    }

    /**
     * Define el valor de la propiedad ownInsuranceChoices.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgReservation.OwnInsuranceChoices }
     *     
     */
    public void setOwnInsuranceChoices(PkgReservation.OwnInsuranceChoices value) {
        this.ownInsuranceChoices = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingInfo.
     * 
     * @return
     *     possible object is
     *     {@link PkgReservation.TicketingInfo }
     *     
     */
    public PkgReservation.TicketingInfo getTicketingInfo() {
        return ticketingInfo;
    }

    /**
     * Define el valor de la propiedad ticketingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgReservation.TicketingInfo }
     *     
     */
    public void setTicketingInfo(PkgReservation.TicketingInfo value) {
        this.ticketingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceDetail.
     * 
     * @return
     *     possible object is
     *     {@link PkgInvoiceDetail }
     *     
     */
    public PkgInvoiceDetail getInvoiceDetail() {
        return invoiceDetail;
    }

    /**
     * Define el valor de la propiedad invoiceDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgInvoiceDetail }
     *     
     */
    public void setInvoiceDetail(PkgInvoiceDetail value) {
        this.invoiceDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad transactions.
     * 
     * @return
     *     possible object is
     *     {@link PkgReservation.Transactions }
     *     
     */
    public PkgReservation.Transactions getTransactions() {
        return transactions;
    }

    /**
     * Define el valor de la propiedad transactions.
     * 
     * @param value
     *     allowed object is
     *     {@link PkgReservation.Transactions }
     *     
     */
    public void setTransactions(PkgReservation.Transactions value) {
        this.transactions = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OwnInsuranceChoice" type="{http://www.opentravel.org/OTA/2003/05}OwnInsuranceChoiceType" maxOccurs="9"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ownInsuranceChoice"
    })
    public static class OwnInsuranceChoices {

        @XmlElement(name = "OwnInsuranceChoice", required = true)
        protected List<OwnInsuranceChoiceType> ownInsuranceChoice;

        /**
         * Gets the value of the ownInsuranceChoice property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ownInsuranceChoice property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOwnInsuranceChoice().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OwnInsuranceChoiceType }
         * 
         * 
         */
        public List<OwnInsuranceChoiceType> getOwnInsuranceChoice() {
            if (ownInsuranceChoice == null) {
                ownInsuranceChoice = new ArrayList<OwnInsuranceChoiceType>();
            }
            return this.ownInsuranceChoice;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PassengerListItem" type="{http://www.opentravel.org/OTA/2003/05}PkgPassengerListItem" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "passengerListItem"
    })
    public static class PassengerListItems {

        @XmlElement(name = "PassengerListItem", required = true)
        protected List<PkgPassengerListItem> passengerListItem;

        /**
         * Gets the value of the passengerListItem property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerListItem property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPassengerListItem().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PkgPassengerListItem }
         * 
         * 
         */
        public List<PkgPassengerListItem> getPassengerListItem() {
            if (passengerListItem == null) {
                passengerListItem = new ArrayList<PkgPassengerListItem>();
            }
            return this.passengerListItem;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type"&amp;gt;
     *       &amp;lt;attribute name="TOD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TicketingInfo
        extends TicketingInfoRSType
    {

        @XmlAttribute(name = "TOD_Ind")
        protected Boolean todInd;

        /**
         * Obtiene el valor de la propiedad todInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isTODInd() {
            return todInd;
        }

        /**
         * Define el valor de la propiedad todInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setTODInd(Boolean value) {
            this.todInd = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Transaction" type="{http://www.opentravel.org/OTA/2003/05}TransactionType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transaction"
    })
    public static class Transactions {

        @XmlElement(name = "Transaction", required = true)
        protected List<TransactionType> transaction;

        /**
         * Gets the value of the transaction property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the transaction property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTransaction().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TransactionType }
         * 
         * 
         */
        public List<TransactionType> getTransaction() {
            if (transaction == null) {
                transaction = new ArrayList<TransactionType>();
            }
            return this.transaction;
        }

    }

}
