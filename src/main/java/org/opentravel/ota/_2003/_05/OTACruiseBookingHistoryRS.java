
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="ReservationID"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReservationStatusGroup"/&amp;gt;
 *                 &amp;lt;/extension&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="SailingInfo" type="{http://www.opentravel.org/OTA/2003/05}SailingCategoryInfoType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="HistoryInfos" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="HistoryInfo" maxOccurs="999"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SourceType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Items" maxOccurs="99"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Item" maxOccurs="999"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
 *                                                 &amp;lt;attribute name="GuestID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                                 &amp;lt;attribute name="ItemName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                 &amp;lt;attribute name="OldValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
 *                                                 &amp;lt;attribute name="NewValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
 *                                                 &amp;lt;attribute name="Classification" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                               &amp;lt;/extension&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="LastModified" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "reservationID",
    "sailingInfo",
    "historyInfos",
    "errors"
})
@XmlRootElement(name = "OTA_CruiseBookingHistoryRS")
public class OTACruiseBookingHistoryRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "ReservationID")
    protected OTACruiseBookingHistoryRS.ReservationID reservationID;
    @XmlElement(name = "SailingInfo")
    protected SailingCategoryInfoType sailingInfo;
    @XmlElement(name = "HistoryInfos")
    protected OTACruiseBookingHistoryRS.HistoryInfos historyInfos;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "MoreIndicator")
    protected Boolean moreIndicator;
    @XmlAttribute(name = "MoreDataEchoToken")
    protected String moreDataEchoToken;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationID.
     * 
     * @return
     *     possible object is
     *     {@link OTACruiseBookingHistoryRS.ReservationID }
     *     
     */
    public OTACruiseBookingHistoryRS.ReservationID getReservationID() {
        return reservationID;
    }

    /**
     * Define el valor de la propiedad reservationID.
     * 
     * @param value
     *     allowed object is
     *     {@link OTACruiseBookingHistoryRS.ReservationID }
     *     
     */
    public void setReservationID(OTACruiseBookingHistoryRS.ReservationID value) {
        this.reservationID = value;
    }

    /**
     * Obtiene el valor de la propiedad sailingInfo.
     * 
     * @return
     *     possible object is
     *     {@link SailingCategoryInfoType }
     *     
     */
    public SailingCategoryInfoType getSailingInfo() {
        return sailingInfo;
    }

    /**
     * Define el valor de la propiedad sailingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SailingCategoryInfoType }
     *     
     */
    public void setSailingInfo(SailingCategoryInfoType value) {
        this.sailingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad historyInfos.
     * 
     * @return
     *     possible object is
     *     {@link OTACruiseBookingHistoryRS.HistoryInfos }
     *     
     */
    public OTACruiseBookingHistoryRS.HistoryInfos getHistoryInfos() {
        return historyInfos;
    }

    /**
     * Define el valor de la propiedad historyInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTACruiseBookingHistoryRS.HistoryInfos }
     *     
     */
    public void setHistoryInfos(OTACruiseBookingHistoryRS.HistoryInfos value) {
        this.historyInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad moreIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMoreIndicator() {
        return moreIndicator;
    }

    /**
     * Define el valor de la propiedad moreIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMoreIndicator(Boolean value) {
        this.moreIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad moreDataEchoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreDataEchoToken() {
        return moreDataEchoToken;
    }

    /**
     * Define el valor de la propiedad moreDataEchoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreDataEchoToken(String value) {
        this.moreDataEchoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="HistoryInfo" maxOccurs="999"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SourceType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Items" maxOccurs="99"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Item" maxOccurs="999"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
     *                                     &amp;lt;attribute name="GuestID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                     &amp;lt;attribute name="ItemName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                     &amp;lt;attribute name="OldValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
     *                                     &amp;lt;attribute name="NewValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
     *                                     &amp;lt;attribute name="Classification" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="LastModified" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "historyInfo"
    })
    public static class HistoryInfos {

        @XmlElement(name = "HistoryInfo", required = true)
        protected List<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo> historyInfo;

        /**
         * Gets the value of the historyInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the historyInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getHistoryInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo }
         * 
         * 
         */
        public List<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo> getHistoryInfo() {
            if (historyInfo == null) {
                historyInfo = new ArrayList<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo>();
            }
            return this.historyInfo;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SourceType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Items" maxOccurs="99"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Item" maxOccurs="999"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
         *                           &amp;lt;attribute name="GuestID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                           &amp;lt;attribute name="ItemName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                           &amp;lt;attribute name="OldValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
         *                           &amp;lt;attribute name="NewValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
         *                           &amp;lt;attribute name="Classification" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="LastModified" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "items"
        })
        public static class HistoryInfo
            extends SourceType
        {

            @XmlElement(name = "Items", required = true)
            protected List<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items> items;
            @XmlAttribute(name = "LastModified", required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar lastModified;

            /**
             * Gets the value of the items property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the items property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getItems().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items }
             * 
             * 
             */
            public List<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items> getItems() {
                if (items == null) {
                    items = new ArrayList<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items>();
                }
                return this.items;
            }

            /**
             * Obtiene el valor de la propiedad lastModified.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLastModified() {
                return lastModified;
            }

            /**
             * Define el valor de la propiedad lastModified.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLastModified(XMLGregorianCalendar value) {
                this.lastModified = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Item" maxOccurs="999"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
             *                 &amp;lt;attribute name="GuestID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *                 &amp;lt;attribute name="ItemName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                 &amp;lt;attribute name="OldValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
             *                 &amp;lt;attribute name="NewValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
             *                 &amp;lt;attribute name="Classification" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OptionalCodeOptionalNameGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "item"
            })
            public static class Items {

                @XmlElement(name = "Item", required = true)
                protected List<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items.Item> item;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "Name")
                protected String name;

                /**
                 * Gets the value of the item property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the item property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getItem().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items.Item }
                 * 
                 * 
                 */
                public List<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items.Item> getItem() {
                    if (item == null) {
                        item = new ArrayList<OTACruiseBookingHistoryRS.HistoryInfos.HistoryInfo.Items.Item>();
                    }
                    return this.item;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad name.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Define el valor de la propiedad name.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
                 *       &amp;lt;attribute name="GuestID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *       &amp;lt;attribute name="ItemName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *       &amp;lt;attribute name="OldValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
                 *       &amp;lt;attribute name="NewValue" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
                 *       &amp;lt;attribute name="Classification" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Item
                    extends ParagraphType
                {

                    @XmlAttribute(name = "GuestID")
                    protected String guestID;
                    @XmlAttribute(name = "ItemName")
                    protected String itemName;
                    @XmlAttribute(name = "OldValue")
                    protected String oldValue;
                    @XmlAttribute(name = "NewValue")
                    protected String newValue;
                    @XmlAttribute(name = "Classification")
                    protected String classification;

                    /**
                     * Obtiene el valor de la propiedad guestID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGuestID() {
                        return guestID;
                    }

                    /**
                     * Define el valor de la propiedad guestID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGuestID(String value) {
                        this.guestID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad itemName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getItemName() {
                        return itemName;
                    }

                    /**
                     * Define el valor de la propiedad itemName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setItemName(String value) {
                        this.itemName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad oldValue.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOldValue() {
                        return oldValue;
                    }

                    /**
                     * Define el valor de la propiedad oldValue.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOldValue(String value) {
                        this.oldValue = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad newValue.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNewValue() {
                        return newValue;
                    }

                    /**
                     * Define el valor de la propiedad newValue.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNewValue(String value) {
                        this.newValue = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad classification.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getClassification() {
                        return classification;
                    }

                    /**
                     * Define el valor de la propiedad classification.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setClassification(String value) {
                        this.classification = value;
                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReservationStatusGroup"/&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ReservationID
        extends UniqueIDType
    {

        @XmlAttribute(name = "StatusCode")
        protected String statusCode;
        @XmlAttribute(name = "LastModifyDateTime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastModifyDateTime;
        @XmlAttribute(name = "BookedDate")
        protected String bookedDate;
        @XmlAttribute(name = "OfferDate")
        protected String offerDate;
        @XmlAttribute(name = "SyncDateTime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar syncDateTime;

        /**
         * Obtiene el valor de la propiedad statusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatusCode() {
            return statusCode;
        }

        /**
         * Define el valor de la propiedad statusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatusCode(String value) {
            this.statusCode = value;
        }

        /**
         * Obtiene el valor de la propiedad lastModifyDateTime.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastModifyDateTime() {
            return lastModifyDateTime;
        }

        /**
         * Define el valor de la propiedad lastModifyDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastModifyDateTime(XMLGregorianCalendar value) {
            this.lastModifyDateTime = value;
        }

        /**
         * Obtiene el valor de la propiedad bookedDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBookedDate() {
            return bookedDate;
        }

        /**
         * Define el valor de la propiedad bookedDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBookedDate(String value) {
            this.bookedDate = value;
        }

        /**
         * Obtiene el valor de la propiedad offerDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfferDate() {
            return offerDate;
        }

        /**
         * Define el valor de la propiedad offerDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfferDate(String value) {
            this.offerDate = value;
        }

        /**
         * Obtiene el valor de la propiedad syncDateTime.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSyncDateTime() {
            return syncDateTime;
        }

        /**
         * Define el valor de la propiedad syncDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSyncDateTime(XMLGregorianCalendar value) {
            this.syncDateTime = value;
        }

    }

}
