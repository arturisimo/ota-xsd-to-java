
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * The StayInfo class contains the guest revenue and stay data to be sent to the central server.
 * 
 * &lt;p&gt;Clase Java para StayInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="StayInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="LoyaltyPointsAccruals" type="{http://www.opentravel.org/OTA/2003/05}LoyaltyPointsAccrualsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RevenueCategories" type="{http://www.opentravel.org/OTA/2003/05}RevenueCategoriesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="HotelReservation" type="{http://www.opentravel.org/OTA/2003/05}HotelReservationType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="RoomStayRPH" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StayInfoType", propOrder = {
    "loyaltyPointsAccruals",
    "revenueCategories",
    "reservationID",
    "hotelReservation"
})
public class StayInfoType {

    @XmlElement(name = "LoyaltyPointsAccruals")
    protected LoyaltyPointsAccrualsType loyaltyPointsAccruals;
    @XmlElement(name = "RevenueCategories")
    protected RevenueCategoriesType revenueCategories;
    @XmlElement(name = "ReservationID")
    protected UniqueIDType reservationID;
    @XmlElement(name = "HotelReservation")
    protected HotelReservationType hotelReservation;
    @XmlAttribute(name = "SequenceNumber")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger sequenceNumber;
    @XmlAttribute(name = "RoomStayRPH")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger roomStayRPH;

    /**
     * Obtiene el valor de la propiedad loyaltyPointsAccruals.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyPointsAccrualsType }
     *     
     */
    public LoyaltyPointsAccrualsType getLoyaltyPointsAccruals() {
        return loyaltyPointsAccruals;
    }

    /**
     * Define el valor de la propiedad loyaltyPointsAccruals.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyPointsAccrualsType }
     *     
     */
    public void setLoyaltyPointsAccruals(LoyaltyPointsAccrualsType value) {
        this.loyaltyPointsAccruals = value;
    }

    /**
     * Obtiene el valor de la propiedad revenueCategories.
     * 
     * @return
     *     possible object is
     *     {@link RevenueCategoriesType }
     *     
     */
    public RevenueCategoriesType getRevenueCategories() {
        return revenueCategories;
    }

    /**
     * Define el valor de la propiedad revenueCategories.
     * 
     * @param value
     *     allowed object is
     *     {@link RevenueCategoriesType }
     *     
     */
    public void setRevenueCategories(RevenueCategoriesType value) {
        this.revenueCategories = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getReservationID() {
        return reservationID;
    }

    /**
     * Define el valor de la propiedad reservationID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setReservationID(UniqueIDType value) {
        this.reservationID = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelReservation.
     * 
     * @return
     *     possible object is
     *     {@link HotelReservationType }
     *     
     */
    public HotelReservationType getHotelReservation() {
        return hotelReservation;
    }

    /**
     * Define el valor de la propiedad hotelReservation.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelReservationType }
     *     
     */
    public void setHotelReservation(HotelReservationType value) {
        this.hotelReservation = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define el valor de la propiedad sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNumber(BigInteger value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad roomStayRPH.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRoomStayRPH() {
        return roomStayRPH;
    }

    /**
     * Define el valor de la propiedad roomStayRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRoomStayRPH(BigInteger value) {
        this.roomStayRPH = value;
    }

}
