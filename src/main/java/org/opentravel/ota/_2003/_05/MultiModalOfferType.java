
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Traveler and trip information used for targeted multi-modal offers.
 * 
 * &lt;p&gt;Clase Java para MultiModalOfferType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="MultiModalOfferType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RequestingParty"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCompanyType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Ontology" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDefinitionType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CompatibleWith" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDefinitionType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RequestedOffer"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="OfferTypes" type="{http://www.opentravel.org/OTA/2003/05}OntologyOfferType"/&amp;gt;
 *                   &amp;lt;element name="TimePeriod"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="EarliestStart"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="CalculationMethod" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Formula" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;simpleContent&amp;gt;
 *                                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferAvailabilityStartFormula"&amp;gt;
 *                                                         &amp;lt;attribute name="OtherType"&amp;gt;
 *                                                           &amp;lt;simpleType&amp;gt;
 *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                               &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                                                             &amp;lt;/restriction&amp;gt;
 *                                                           &amp;lt;/simpleType&amp;gt;
 *                                                         &amp;lt;/attribute&amp;gt;
 *                                                         &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                                                           &amp;lt;simpleType&amp;gt;
 *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                               &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                                                             &amp;lt;/restriction&amp;gt;
 *                                                           &amp;lt;/simpleType&amp;gt;
 *                                                         &amp;lt;/attribute&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/simpleContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Distance" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDistanceType"&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Duration" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;simpleContent&amp;gt;
 *                                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
 *                                                         &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                                         &amp;lt;attribute name="OtherType"&amp;gt;
 *                                                           &amp;lt;simpleType&amp;gt;
 *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                               &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                                                             &amp;lt;/restriction&amp;gt;
 *                                                           &amp;lt;/simpleType&amp;gt;
 *                                                         &amp;lt;/attribute&amp;gt;
 *                                                         &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                                                           &amp;lt;simpleType&amp;gt;
 *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                               &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                                                             &amp;lt;/restriction&amp;gt;
 *                                                           &amp;lt;/simpleType&amp;gt;
 *                                                         &amp;lt;/attribute&amp;gt;
 *                                                       &amp;lt;/extension&amp;gt;
 *                                                     &amp;lt;/simpleContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="OtherType"&amp;gt;
 *                                                 &amp;lt;simpleType&amp;gt;
 *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                     &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                                                   &amp;lt;/restriction&amp;gt;
 *                                                 &amp;lt;/simpleType&amp;gt;
 *                                               &amp;lt;/attribute&amp;gt;
 *                                               &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                                                 &amp;lt;simpleType&amp;gt;
 *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                     &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                                                   &amp;lt;/restriction&amp;gt;
 *                                                 &amp;lt;/simpleType&amp;gt;
 *                                               &amp;lt;/attribute&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="DateTime" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                     &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="MaximumDuration" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;simpleContent&amp;gt;
 *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
 *                                     &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                     &amp;lt;attribute name="OtherType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/simpleContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="GuidelinePricing" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="MaximumPrice" type="{http://www.opentravel.org/OTA/2003/05}OntologyCurrencyType"/&amp;gt;
 *                             &amp;lt;element name="Method" type="{http://www.opentravel.org/OTA/2003/05}OntologyPricingMethodType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TripPurpose" type="{http://www.opentravel.org/OTA/2003/05}OntologyTripPurposeType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="NumberInParty" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TripCharacteristics" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Mode" type="{http://www.opentravel.org/OTA/2003/05}OntologyTripModeType"/&amp;gt;
 *                   &amp;lt;element name="BookingMethod" type="{http://www.opentravel.org/OTA/2003/05}OntologyBookingMethodType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="DateTimeDuration" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyTimeDurationType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Location" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyLocationType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="PriceAndPayment" type="{http://www.opentravel.org/OTA/2003/05}OntologyPaymentType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="ReservationStatus" type="{http://www.opentravel.org/OTA/2003/05}OntologyReservationStatusType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Baggage" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyBaggageType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Animals" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyAnimalType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Activity" type="{http://www.opentravel.org/OTA/2003/05}OntologyActivityType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Lodging" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyLodgingType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Transportation" type="{http://www.opentravel.org/OTA/2003/05}OntologyTransportationType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="TripValue" type="{http://www.opentravel.org/OTA/2003/05}OntologyValueType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TravelerCharacteristics" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="TripPurpose" type="{http://www.opentravel.org/OTA/2003/05}OntologyTripPurposeType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Classification" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyTravelerClassType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="DetailInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Identification" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}OntologyNameType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="Age" type="{http://www.opentravel.org/OTA/2003/05}OntologyAgeBirthDateType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}OntologyAddressType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}OntologyContactType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="LoyaltyProgram" type="{http://www.opentravel.org/OTA/2003/05}OntologyLoyaltyType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="CustomerValue" type="{http://www.opentravel.org/OTA/2003/05}OntologyValueType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="ServiceAnimalInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="DisabledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="FemaleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="MaleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiModalOfferType", propOrder = {
    "requestingParty",
    "ontology",
    "requestedOffer",
    "tripCharacteristics",
    "travelerCharacteristics",
    "ontologyExtension"
})
public class MultiModalOfferType {

    @XmlElement(name = "RequestingParty", required = true)
    protected MultiModalOfferType.RequestingParty requestingParty;
    @XmlElement(name = "Ontology")
    protected MultiModalOfferType.Ontology ontology;
    @XmlElement(name = "RequestedOffer", required = true)
    protected MultiModalOfferType.RequestedOffer requestedOffer;
    @XmlElement(name = "TripCharacteristics")
    protected MultiModalOfferType.TripCharacteristics tripCharacteristics;
    @XmlElement(name = "TravelerCharacteristics")
    protected MultiModalOfferType.TravelerCharacteristics travelerCharacteristics;
    @XmlElement(name = "OntologyExtension")
    protected OntologyExtensionType ontologyExtension;

    /**
     * Obtiene el valor de la propiedad requestingParty.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType.RequestingParty }
     *     
     */
    public MultiModalOfferType.RequestingParty getRequestingParty() {
        return requestingParty;
    }

    /**
     * Define el valor de la propiedad requestingParty.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType.RequestingParty }
     *     
     */
    public void setRequestingParty(MultiModalOfferType.RequestingParty value) {
        this.requestingParty = value;
    }

    /**
     * Obtiene el valor de la propiedad ontology.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType.Ontology }
     *     
     */
    public MultiModalOfferType.Ontology getOntology() {
        return ontology;
    }

    /**
     * Define el valor de la propiedad ontology.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType.Ontology }
     *     
     */
    public void setOntology(MultiModalOfferType.Ontology value) {
        this.ontology = value;
    }

    /**
     * Obtiene el valor de la propiedad requestedOffer.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType.RequestedOffer }
     *     
     */
    public MultiModalOfferType.RequestedOffer getRequestedOffer() {
        return requestedOffer;
    }

    /**
     * Define el valor de la propiedad requestedOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType.RequestedOffer }
     *     
     */
    public void setRequestedOffer(MultiModalOfferType.RequestedOffer value) {
        this.requestedOffer = value;
    }

    /**
     * Obtiene el valor de la propiedad tripCharacteristics.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType.TripCharacteristics }
     *     
     */
    public MultiModalOfferType.TripCharacteristics getTripCharacteristics() {
        return tripCharacteristics;
    }

    /**
     * Define el valor de la propiedad tripCharacteristics.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType.TripCharacteristics }
     *     
     */
    public void setTripCharacteristics(MultiModalOfferType.TripCharacteristics value) {
        this.tripCharacteristics = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerCharacteristics.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType.TravelerCharacteristics }
     *     
     */
    public MultiModalOfferType.TravelerCharacteristics getTravelerCharacteristics() {
        return travelerCharacteristics;
    }

    /**
     * Define el valor de la propiedad travelerCharacteristics.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType.TravelerCharacteristics }
     *     
     */
    public void setTravelerCharacteristics(MultiModalOfferType.TravelerCharacteristics value) {
        this.travelerCharacteristics = value;
    }

    /**
     * Obtiene el valor de la propiedad ontologyExtension.
     * 
     * @return
     *     possible object is
     *     {@link OntologyExtensionType }
     *     
     */
    public OntologyExtensionType getOntologyExtension() {
        return ontologyExtension;
    }

    /**
     * Define el valor de la propiedad ontologyExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyExtensionType }
     *     
     */
    public void setOntologyExtension(OntologyExtensionType value) {
        this.ontologyExtension = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDefinitionType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CompatibleWith" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDefinitionType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "compatibleWith",
        "ontologyExtension"
    })
    public static class Ontology
        extends OntologyDefinitionType
    {

        @XmlElement(name = "CompatibleWith")
        protected List<MultiModalOfferType.Ontology.CompatibleWith> compatibleWith;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;

        /**
         * Gets the value of the compatibleWith property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the compatibleWith property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCompatibleWith().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link MultiModalOfferType.Ontology.CompatibleWith }
         * 
         * 
         */
        public List<MultiModalOfferType.Ontology.CompatibleWith> getCompatibleWith() {
            if (compatibleWith == null) {
                compatibleWith = new ArrayList<MultiModalOfferType.Ontology.CompatibleWith>();
            }
            return this.compatibleWith;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDefinitionType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CompatibleWith
            extends OntologyDefinitionType
        {


        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OfferTypes" type="{http://www.opentravel.org/OTA/2003/05}OntologyOfferType"/&amp;gt;
     *         &amp;lt;element name="TimePeriod"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="EarliestStart"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="CalculationMethod" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Formula" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferAvailabilityStartFormula"&amp;gt;
     *                                               &amp;lt;attribute name="OtherType"&amp;gt;
     *                                                 &amp;lt;simpleType&amp;gt;
     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                     &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                                                   &amp;lt;/restriction&amp;gt;
     *                                                 &amp;lt;/simpleType&amp;gt;
     *                                               &amp;lt;/attribute&amp;gt;
     *                                               &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                                                 &amp;lt;simpleType&amp;gt;
     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                     &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                                                   &amp;lt;/restriction&amp;gt;
     *                                                 &amp;lt;/simpleType&amp;gt;
     *                                               &amp;lt;/attribute&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Distance" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDistanceType"&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Duration" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;simpleContent&amp;gt;
     *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
     *                                               &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                                               &amp;lt;attribute name="OtherType"&amp;gt;
     *                                                 &amp;lt;simpleType&amp;gt;
     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                     &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                                                   &amp;lt;/restriction&amp;gt;
     *                                                 &amp;lt;/simpleType&amp;gt;
     *                                               &amp;lt;/attribute&amp;gt;
     *                                               &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                                                 &amp;lt;simpleType&amp;gt;
     *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                     &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                                                   &amp;lt;/restriction&amp;gt;
     *                                                 &amp;lt;/simpleType&amp;gt;
     *                                               &amp;lt;/attribute&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/simpleContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="OtherType"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                     &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="DateTime" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="MaximumDuration" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
     *                           &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                           &amp;lt;attribute name="OtherType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="GuidelinePricing" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="MaximumPrice" type="{http://www.opentravel.org/OTA/2003/05}OntologyCurrencyType"/&amp;gt;
     *                   &amp;lt;element name="Method" type="{http://www.opentravel.org/OTA/2003/05}OntologyPricingMethodType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TripPurpose" type="{http://www.opentravel.org/OTA/2003/05}OntologyTripPurposeType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="NumberInParty" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offerTypes",
        "timePeriod",
        "guidelinePricing",
        "tripPurpose",
        "ontologyExtension"
    })
    public static class RequestedOffer {

        @XmlElement(name = "OfferTypes", required = true)
        protected OntologyOfferType offerTypes;
        @XmlElement(name = "TimePeriod", required = true)
        protected MultiModalOfferType.RequestedOffer.TimePeriod timePeriod;
        @XmlElement(name = "GuidelinePricing")
        protected MultiModalOfferType.RequestedOffer.GuidelinePricing guidelinePricing;
        @XmlElement(name = "TripPurpose")
        protected OntologyTripPurposeType tripPurpose;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;
        @XmlAttribute(name = "NumberInParty", required = true)
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger numberInParty;

        /**
         * Obtiene el valor de la propiedad offerTypes.
         * 
         * @return
         *     possible object is
         *     {@link OntologyOfferType }
         *     
         */
        public OntologyOfferType getOfferTypes() {
            return offerTypes;
        }

        /**
         * Define el valor de la propiedad offerTypes.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyOfferType }
         *     
         */
        public void setOfferTypes(OntologyOfferType value) {
            this.offerTypes = value;
        }

        /**
         * Obtiene el valor de la propiedad timePeriod.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.RequestedOffer.TimePeriod }
         *     
         */
        public MultiModalOfferType.RequestedOffer.TimePeriod getTimePeriod() {
            return timePeriod;
        }

        /**
         * Define el valor de la propiedad timePeriod.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.RequestedOffer.TimePeriod }
         *     
         */
        public void setTimePeriod(MultiModalOfferType.RequestedOffer.TimePeriod value) {
            this.timePeriod = value;
        }

        /**
         * Obtiene el valor de la propiedad guidelinePricing.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.RequestedOffer.GuidelinePricing }
         *     
         */
        public MultiModalOfferType.RequestedOffer.GuidelinePricing getGuidelinePricing() {
            return guidelinePricing;
        }

        /**
         * Define el valor de la propiedad guidelinePricing.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.RequestedOffer.GuidelinePricing }
         *     
         */
        public void setGuidelinePricing(MultiModalOfferType.RequestedOffer.GuidelinePricing value) {
            this.guidelinePricing = value;
        }

        /**
         * Obtiene el valor de la propiedad tripPurpose.
         * 
         * @return
         *     possible object is
         *     {@link OntologyTripPurposeType }
         *     
         */
        public OntologyTripPurposeType getTripPurpose() {
            return tripPurpose;
        }

        /**
         * Define el valor de la propiedad tripPurpose.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyTripPurposeType }
         *     
         */
        public void setTripPurpose(OntologyTripPurposeType value) {
            this.tripPurpose = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }

        /**
         * Obtiene el valor de la propiedad numberInParty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNumberInParty() {
            return numberInParty;
        }

        /**
         * Define el valor de la propiedad numberInParty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNumberInParty(BigInteger value) {
            this.numberInParty = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="MaximumPrice" type="{http://www.opentravel.org/OTA/2003/05}OntologyCurrencyType"/&amp;gt;
         *         &amp;lt;element name="Method" type="{http://www.opentravel.org/OTA/2003/05}OntologyPricingMethodType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "maximumPrice",
            "method"
        })
        public static class GuidelinePricing {

            @XmlElement(name = "MaximumPrice", required = true)
            protected OntologyCurrencyType maximumPrice;
            @XmlElement(name = "Method")
            protected OntologyPricingMethodType method;

            /**
             * Obtiene el valor de la propiedad maximumPrice.
             * 
             * @return
             *     possible object is
             *     {@link OntologyCurrencyType }
             *     
             */
            public OntologyCurrencyType getMaximumPrice() {
                return maximumPrice;
            }

            /**
             * Define el valor de la propiedad maximumPrice.
             * 
             * @param value
             *     allowed object is
             *     {@link OntologyCurrencyType }
             *     
             */
            public void setMaximumPrice(OntologyCurrencyType value) {
                this.maximumPrice = value;
            }

            /**
             * Obtiene el valor de la propiedad method.
             * 
             * @return
             *     possible object is
             *     {@link OntologyPricingMethodType }
             *     
             */
            public OntologyPricingMethodType getMethod() {
                return method;
            }

            /**
             * Define el valor de la propiedad method.
             * 
             * @param value
             *     allowed object is
             *     {@link OntologyPricingMethodType }
             *     
             */
            public void setMethod(OntologyPricingMethodType value) {
                this.method = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="EarliestStart"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="CalculationMethod" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Formula" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferAvailabilityStartFormula"&amp;gt;
         *                                     &amp;lt;attribute name="OtherType"&amp;gt;
         *                                       &amp;lt;simpleType&amp;gt;
         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                           &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
         *                                         &amp;lt;/restriction&amp;gt;
         *                                       &amp;lt;/simpleType&amp;gt;
         *                                     &amp;lt;/attribute&amp;gt;
         *                                     &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *                                       &amp;lt;simpleType&amp;gt;
         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                           &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *                                         &amp;lt;/restriction&amp;gt;
         *                                       &amp;lt;/simpleType&amp;gt;
         *                                     &amp;lt;/attribute&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Distance" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDistanceType"&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Duration" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;simpleContent&amp;gt;
         *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
         *                                     &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                                     &amp;lt;attribute name="OtherType"&amp;gt;
         *                                       &amp;lt;simpleType&amp;gt;
         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                           &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
         *                                         &amp;lt;/restriction&amp;gt;
         *                                       &amp;lt;/simpleType&amp;gt;
         *                                     &amp;lt;/attribute&amp;gt;
         *                                     &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *                                       &amp;lt;simpleType&amp;gt;
         *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                           &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *                                         &amp;lt;/restriction&amp;gt;
         *                                       &amp;lt;/simpleType&amp;gt;
         *                                     &amp;lt;/attribute&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/simpleContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="OtherType"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="DateTime" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="MaximumDuration" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
         *                 &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                 &amp;lt;attribute name="OtherType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "earliestStart",
            "maximumDuration",
            "ontologyExtension"
        })
        public static class TimePeriod {

            @XmlElement(name = "EarliestStart", required = true)
            protected MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart earliestStart;
            @XmlElement(name = "MaximumDuration")
            protected MultiModalOfferType.RequestedOffer.TimePeriod.MaximumDuration maximumDuration;
            @XmlElement(name = "OntologyExtension")
            protected OntologyExtensionType ontologyExtension;
            @XmlAttribute(name = "OntologyRefID")
            protected String ontologyRefID;

            /**
             * Obtiene el valor de la propiedad earliestStart.
             * 
             * @return
             *     possible object is
             *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart }
             *     
             */
            public MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart getEarliestStart() {
                return earliestStart;
            }

            /**
             * Define el valor de la propiedad earliestStart.
             * 
             * @param value
             *     allowed object is
             *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart }
             *     
             */
            public void setEarliestStart(MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart value) {
                this.earliestStart = value;
            }

            /**
             * Obtiene el valor de la propiedad maximumDuration.
             * 
             * @return
             *     possible object is
             *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.MaximumDuration }
             *     
             */
            public MultiModalOfferType.RequestedOffer.TimePeriod.MaximumDuration getMaximumDuration() {
                return maximumDuration;
            }

            /**
             * Define el valor de la propiedad maximumDuration.
             * 
             * @param value
             *     allowed object is
             *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.MaximumDuration }
             *     
             */
            public void setMaximumDuration(MultiModalOfferType.RequestedOffer.TimePeriod.MaximumDuration value) {
                this.maximumDuration = value;
            }

            /**
             * Obtiene el valor de la propiedad ontologyExtension.
             * 
             * @return
             *     possible object is
             *     {@link OntologyExtensionType }
             *     
             */
            public OntologyExtensionType getOntologyExtension() {
                return ontologyExtension;
            }

            /**
             * Define el valor de la propiedad ontologyExtension.
             * 
             * @param value
             *     allowed object is
             *     {@link OntologyExtensionType }
             *     
             */
            public void setOntologyExtension(OntologyExtensionType value) {
                this.ontologyExtension = value;
            }

            /**
             * Obtiene el valor de la propiedad ontologyRefID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOntologyRefID() {
                return ontologyRefID;
            }

            /**
             * Define el valor de la propiedad ontologyRefID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOntologyRefID(String value) {
                this.ontologyRefID = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="CalculationMethod" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Formula" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferAvailabilityStartFormula"&amp;gt;
             *                           &amp;lt;attribute name="OtherType"&amp;gt;
             *                             &amp;lt;simpleType&amp;gt;
             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                 &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
             *                               &amp;lt;/restriction&amp;gt;
             *                             &amp;lt;/simpleType&amp;gt;
             *                           &amp;lt;/attribute&amp;gt;
             *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
             *                             &amp;lt;simpleType&amp;gt;
             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
             *                               &amp;lt;/restriction&amp;gt;
             *                             &amp;lt;/simpleType&amp;gt;
             *                           &amp;lt;/attribute&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Distance" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDistanceType"&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Duration" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;simpleContent&amp;gt;
             *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
             *                           &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *                           &amp;lt;attribute name="OtherType"&amp;gt;
             *                             &amp;lt;simpleType&amp;gt;
             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                 &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
             *                               &amp;lt;/restriction&amp;gt;
             *                             &amp;lt;/simpleType&amp;gt;
             *                           &amp;lt;/attribute&amp;gt;
             *                           &amp;lt;attribute name="OntologyRefID"&amp;gt;
             *                             &amp;lt;simpleType&amp;gt;
             *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                 &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
             *                               &amp;lt;/restriction&amp;gt;
             *                             &amp;lt;/simpleType&amp;gt;
             *                           &amp;lt;/attribute&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/simpleContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="OtherType"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="DateTime" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "calculationMethod",
                "ontologyExtension"
            })
            public static class EarliestStart {

                @XmlElement(name = "CalculationMethod")
                protected MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod calculationMethod;
                @XmlElement(name = "OntologyExtension")
                protected OntologyExtensionType ontologyExtension;
                @XmlAttribute(name = "DateTime", required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar dateTime;
                @XmlAttribute(name = "OntologyRefID")
                protected String ontologyRefID;

                /**
                 * Obtiene el valor de la propiedad calculationMethod.
                 * 
                 * @return
                 *     possible object is
                 *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod }
                 *     
                 */
                public MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod getCalculationMethod() {
                    return calculationMethod;
                }

                /**
                 * Define el valor de la propiedad calculationMethod.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod }
                 *     
                 */
                public void setCalculationMethod(MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod value) {
                    this.calculationMethod = value;
                }

                /**
                 * Obtiene el valor de la propiedad ontologyExtension.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OntologyExtensionType }
                 *     
                 */
                public OntologyExtensionType getOntologyExtension() {
                    return ontologyExtension;
                }

                /**
                 * Define el valor de la propiedad ontologyExtension.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OntologyExtensionType }
                 *     
                 */
                public void setOntologyExtension(OntologyExtensionType value) {
                    this.ontologyExtension = value;
                }

                /**
                 * Obtiene el valor de la propiedad dateTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDateTime() {
                    return dateTime;
                }

                /**
                 * Define el valor de la propiedad dateTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDateTime(XMLGregorianCalendar value) {
                    this.dateTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad ontologyRefID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOntologyRefID() {
                    return ontologyRefID;
                }

                /**
                 * Define el valor de la propiedad ontologyRefID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOntologyRefID(String value) {
                    this.ontologyRefID = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Formula" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferAvailabilityStartFormula"&amp;gt;
                 *                 &amp;lt;attribute name="OtherType"&amp;gt;
                 *                   &amp;lt;simpleType&amp;gt;
                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
                 *                     &amp;lt;/restriction&amp;gt;
                 *                   &amp;lt;/simpleType&amp;gt;
                 *                 &amp;lt;/attribute&amp;gt;
                 *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
                 *                   &amp;lt;simpleType&amp;gt;
                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
                 *                     &amp;lt;/restriction&amp;gt;
                 *                   &amp;lt;/simpleType&amp;gt;
                 *                 &amp;lt;/attribute&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Distance" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDistanceType"&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Duration" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;simpleContent&amp;gt;
                 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
                 *                 &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                 *                 &amp;lt;attribute name="OtherType"&amp;gt;
                 *                   &amp;lt;simpleType&amp;gt;
                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                       &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
                 *                     &amp;lt;/restriction&amp;gt;
                 *                   &amp;lt;/simpleType&amp;gt;
                 *                 &amp;lt;/attribute&amp;gt;
                 *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
                 *                   &amp;lt;simpleType&amp;gt;
                 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
                 *                     &amp;lt;/restriction&amp;gt;
                 *                   &amp;lt;/simpleType&amp;gt;
                 *                 &amp;lt;/attribute&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/simpleContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="OtherType"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "formula",
                    "distance",
                    "duration",
                    "ontologyExtension"
                })
                public static class CalculationMethod {

                    @XmlElement(name = "Formula")
                    protected MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Formula formula;
                    @XmlElement(name = "Distance")
                    protected MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Distance distance;
                    @XmlElement(name = "Duration")
                    protected MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Duration duration;
                    @XmlElement(name = "OntologyExtension")
                    protected OntologyExtensionType ontologyExtension;
                    @XmlAttribute(name = "OtherType")
                    protected String otherType;
                    @XmlAttribute(name = "OntologyRefID")
                    protected String ontologyRefID;

                    /**
                     * Obtiene el valor de la propiedad formula.
                     * 
                     * @return
                     *     possible object is
                     *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Formula }
                     *     
                     */
                    public MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Formula getFormula() {
                        return formula;
                    }

                    /**
                     * Define el valor de la propiedad formula.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Formula }
                     *     
                     */
                    public void setFormula(MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Formula value) {
                        this.formula = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad distance.
                     * 
                     * @return
                     *     possible object is
                     *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Distance }
                     *     
                     */
                    public MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Distance getDistance() {
                        return distance;
                    }

                    /**
                     * Define el valor de la propiedad distance.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Distance }
                     *     
                     */
                    public void setDistance(MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Distance value) {
                        this.distance = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad duration.
                     * 
                     * @return
                     *     possible object is
                     *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Duration }
                     *     
                     */
                    public MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Duration getDuration() {
                        return duration;
                    }

                    /**
                     * Define el valor de la propiedad duration.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Duration }
                     *     
                     */
                    public void setDuration(MultiModalOfferType.RequestedOffer.TimePeriod.EarliestStart.CalculationMethod.Duration value) {
                        this.duration = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad ontologyExtension.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OntologyExtensionType }
                     *     
                     */
                    public OntologyExtensionType getOntologyExtension() {
                        return ontologyExtension;
                    }

                    /**
                     * Define el valor de la propiedad ontologyExtension.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OntologyExtensionType }
                     *     
                     */
                    public void setOntologyExtension(OntologyExtensionType value) {
                        this.ontologyExtension = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad otherType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOtherType() {
                        return otherType;
                    }

                    /**
                     * Define el valor de la propiedad otherType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOtherType(String value) {
                        this.otherType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad ontologyRefID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOntologyRefID() {
                        return ontologyRefID;
                    }

                    /**
                     * Define el valor de la propiedad ontologyRefID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOntologyRefID(String value) {
                        this.ontologyRefID = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyDistanceType"&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Distance
                        extends OntologyDistanceType
                    {


                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
                     *       &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                     *       &amp;lt;attribute name="OtherType"&amp;gt;
                     *         &amp;lt;simpleType&amp;gt;
                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
                     *           &amp;lt;/restriction&amp;gt;
                     *         &amp;lt;/simpleType&amp;gt;
                     *       &amp;lt;/attribute&amp;gt;
                     *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
                     *         &amp;lt;simpleType&amp;gt;
                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
                     *           &amp;lt;/restriction&amp;gt;
                     *         &amp;lt;/simpleType&amp;gt;
                     *       &amp;lt;/attribute&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    public static class Duration {

                        @XmlValue
                        protected ListOfferDurationUOM value;
                        @XmlAttribute(name = "Value", required = true)
                        protected BigDecimal valueOfferDistanceUOM;
                        @XmlAttribute(name = "OtherType")
                        protected String otherType;
                        @XmlAttribute(name = "OntologyRefID")
                        protected String ontologyRefID;

                        /**
                         * Source: Unit of Measure (UOM) OpenTravel codelist.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ListOfferDurationUOM }
                         *     
                         */
                        public ListOfferDurationUOM getValue() {
                            return value;
                        }

                        /**
                         * Define el valor de la propiedad value.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ListOfferDurationUOM }
                         *     
                         */
                        public void setValue(ListOfferDurationUOM value) {
                            this.value = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad valueOfferDistanceUOM.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getValueOfferDistanceUOM() {
                            return valueOfferDistanceUOM;
                        }

                        /**
                         * Define el valor de la propiedad valueOfferDistanceUOM.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setValueOfferDistanceUOM(BigDecimal value) {
                            this.valueOfferDistanceUOM = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad otherType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOtherType() {
                            return otherType;
                        }

                        /**
                         * Define el valor de la propiedad otherType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOtherType(String value) {
                            this.otherType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad ontologyRefID.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOntologyRefID() {
                            return ontologyRefID;
                        }

                        /**
                         * Define el valor de la propiedad ontologyRefID.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOntologyRefID(String value) {
                            this.ontologyRefID = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;simpleContent&amp;gt;
                     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferAvailabilityStartFormula"&amp;gt;
                     *       &amp;lt;attribute name="OtherType"&amp;gt;
                     *         &amp;lt;simpleType&amp;gt;
                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
                     *           &amp;lt;/restriction&amp;gt;
                     *         &amp;lt;/simpleType&amp;gt;
                     *       &amp;lt;/attribute&amp;gt;
                     *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
                     *         &amp;lt;simpleType&amp;gt;
                     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
                     *           &amp;lt;/restriction&amp;gt;
                     *         &amp;lt;/simpleType&amp;gt;
                     *       &amp;lt;/attribute&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/simpleContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    public static class Formula {

                        @XmlValue
                        protected ListOfferAvailabilityStartFormula value;
                        @XmlAttribute(name = "OtherType")
                        protected String otherType;
                        @XmlAttribute(name = "OntologyRefID")
                        protected String ontologyRefID;

                        /**
                         * Source: OpenTravel
                         * 
                         * @return
                         *     possible object is
                         *     {@link ListOfferAvailabilityStartFormula }
                         *     
                         */
                        public ListOfferAvailabilityStartFormula getValue() {
                            return value;
                        }

                        /**
                         * Define el valor de la propiedad value.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ListOfferAvailabilityStartFormula }
                         *     
                         */
                        public void setValue(ListOfferAvailabilityStartFormula value) {
                            this.value = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad otherType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOtherType() {
                            return otherType;
                        }

                        /**
                         * Define el valor de la propiedad otherType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOtherType(String value) {
                            this.otherType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad ontologyRefID.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOntologyRefID() {
                            return ontologyRefID;
                        }

                        /**
                         * Define el valor de la propiedad ontologyRefID.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOntologyRefID(String value) {
                            this.ontologyRefID = value;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferDurationUOM"&amp;gt;
             *       &amp;lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *       &amp;lt;attribute name="OtherType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class MaximumDuration {

                @XmlValue
                protected ListOfferDurationUOM value;
                @XmlAttribute(name = "Value", required = true)
                protected BigDecimal valueOfferDistanceUOM;
                @XmlAttribute(name = "OtherType")
                protected String otherType;
                @XmlAttribute(name = "OntologyRefID")
                protected String ontologyRefID;

                /**
                 * Source: Unit of Measure (UOM) OpenTravel codelist.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ListOfferDurationUOM }
                 *     
                 */
                public ListOfferDurationUOM getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ListOfferDurationUOM }
                 *     
                 */
                public void setValue(ListOfferDurationUOM value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad valueOfferDistanceUOM.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getValueOfferDistanceUOM() {
                    return valueOfferDistanceUOM;
                }

                /**
                 * Define el valor de la propiedad valueOfferDistanceUOM.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setValueOfferDistanceUOM(BigDecimal value) {
                    this.valueOfferDistanceUOM = value;
                }

                /**
                 * Obtiene el valor de la propiedad otherType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOtherType() {
                    return otherType;
                }

                /**
                 * Define el valor de la propiedad otherType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOtherType(String value) {
                    this.otherType = value;
                }

                /**
                 * Obtiene el valor de la propiedad ontologyRefID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOntologyRefID() {
                    return ontologyRefID;
                }

                /**
                 * Define el valor de la propiedad ontologyRefID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOntologyRefID(String value) {
                    this.ontologyRefID = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCompanyType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RequestingParty
        extends OntologyCompanyType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="TripPurpose" type="{http://www.opentravel.org/OTA/2003/05}OntologyTripPurposeType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Classification" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyTravelerClassType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="DetailInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Identification" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}OntologyNameType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="Age" type="{http://www.opentravel.org/OTA/2003/05}OntologyAgeBirthDateType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}OntologyAddressType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}OntologyContactType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="LoyaltyProgram" type="{http://www.opentravel.org/OTA/2003/05}OntologyLoyaltyType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="CustomerValue" type="{http://www.opentravel.org/OTA/2003/05}OntologyValueType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ServiceAnimalInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="DisabledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="FemaleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="MaleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tripPurpose",
        "classification",
        "detailInfo",
        "ontologyExtension"
    })
    public static class TravelerCharacteristics {

        @XmlElement(name = "TripPurpose")
        protected OntologyTripPurposeType tripPurpose;
        @XmlElement(name = "Classification")
        protected MultiModalOfferType.TravelerCharacteristics.Classification classification;
        @XmlElement(name = "DetailInfo")
        protected List<MultiModalOfferType.TravelerCharacteristics.DetailInfo> detailInfo;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;

        /**
         * Obtiene el valor de la propiedad tripPurpose.
         * 
         * @return
         *     possible object is
         *     {@link OntologyTripPurposeType }
         *     
         */
        public OntologyTripPurposeType getTripPurpose() {
            return tripPurpose;
        }

        /**
         * Define el valor de la propiedad tripPurpose.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyTripPurposeType }
         *     
         */
        public void setTripPurpose(OntologyTripPurposeType value) {
            this.tripPurpose = value;
        }

        /**
         * Obtiene el valor de la propiedad classification.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.TravelerCharacteristics.Classification }
         *     
         */
        public MultiModalOfferType.TravelerCharacteristics.Classification getClassification() {
            return classification;
        }

        /**
         * Define el valor de la propiedad classification.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.TravelerCharacteristics.Classification }
         *     
         */
        public void setClassification(MultiModalOfferType.TravelerCharacteristics.Classification value) {
            this.classification = value;
        }

        /**
         * Gets the value of the detailInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the detailInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDetailInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link MultiModalOfferType.TravelerCharacteristics.DetailInfo }
         * 
         * 
         */
        public List<MultiModalOfferType.TravelerCharacteristics.DetailInfo> getDetailInfo() {
            if (detailInfo == null) {
                detailInfo = new ArrayList<MultiModalOfferType.TravelerCharacteristics.DetailInfo>();
            }
            return this.detailInfo;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyTravelerClassType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Classification
            extends OntologyTravelerClassType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Identification" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}OntologyNameType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Age" type="{http://www.opentravel.org/OTA/2003/05}OntologyAgeBirthDateType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}OntologyAddressType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}OntologyContactType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="LoyaltyProgram" type="{http://www.opentravel.org/OTA/2003/05}OntologyLoyaltyType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="CustomerValue" type="{http://www.opentravel.org/OTA/2003/05}OntologyValueType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ServiceAnimalInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="DisabledInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FemaleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="MaleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identification",
            "customerValue"
        })
        public static class DetailInfo {

            @XmlElement(name = "Identification")
            protected MultiModalOfferType.TravelerCharacteristics.DetailInfo.Identification identification;
            @XmlElement(name = "CustomerValue")
            protected OntologyValueType customerValue;
            @XmlAttribute(name = "ServiceAnimalInd")
            protected Boolean serviceAnimalInd;
            @XmlAttribute(name = "DisabledInd")
            protected Boolean disabledInd;
            @XmlAttribute(name = "FemaleInd")
            protected Boolean femaleInd;
            @XmlAttribute(name = "MaleInd")
            protected Boolean maleInd;

            /**
             * Obtiene el valor de la propiedad identification.
             * 
             * @return
             *     possible object is
             *     {@link MultiModalOfferType.TravelerCharacteristics.DetailInfo.Identification }
             *     
             */
            public MultiModalOfferType.TravelerCharacteristics.DetailInfo.Identification getIdentification() {
                return identification;
            }

            /**
             * Define el valor de la propiedad identification.
             * 
             * @param value
             *     allowed object is
             *     {@link MultiModalOfferType.TravelerCharacteristics.DetailInfo.Identification }
             *     
             */
            public void setIdentification(MultiModalOfferType.TravelerCharacteristics.DetailInfo.Identification value) {
                this.identification = value;
            }

            /**
             * Obtiene el valor de la propiedad customerValue.
             * 
             * @return
             *     possible object is
             *     {@link OntologyValueType }
             *     
             */
            public OntologyValueType getCustomerValue() {
                return customerValue;
            }

            /**
             * Define el valor de la propiedad customerValue.
             * 
             * @param value
             *     allowed object is
             *     {@link OntologyValueType }
             *     
             */
            public void setCustomerValue(OntologyValueType value) {
                this.customerValue = value;
            }

            /**
             * Obtiene el valor de la propiedad serviceAnimalInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isServiceAnimalInd() {
                return serviceAnimalInd;
            }

            /**
             * Define el valor de la propiedad serviceAnimalInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setServiceAnimalInd(Boolean value) {
                this.serviceAnimalInd = value;
            }

            /**
             * Obtiene el valor de la propiedad disabledInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDisabledInd() {
                return disabledInd;
            }

            /**
             * Define el valor de la propiedad disabledInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDisabledInd(Boolean value) {
                this.disabledInd = value;
            }

            /**
             * Obtiene el valor de la propiedad femaleInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFemaleInd() {
                return femaleInd;
            }

            /**
             * Define el valor de la propiedad femaleInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFemaleInd(Boolean value) {
                this.femaleInd = value;
            }

            /**
             * Obtiene el valor de la propiedad maleInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isMaleInd() {
                return maleInd;
            }

            /**
             * Define el valor de la propiedad maleInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setMaleInd(Boolean value) {
                this.maleInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Name" type="{http://www.opentravel.org/OTA/2003/05}OntologyNameType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Age" type="{http://www.opentravel.org/OTA/2003/05}OntologyAgeBirthDateType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Address" type="{http://www.opentravel.org/OTA/2003/05}OntologyAddressType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}OntologyContactType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="LoyaltyProgram" type="{http://www.opentravel.org/OTA/2003/05}OntologyLoyaltyType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
             *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "name",
                "age",
                "address",
                "contact",
                "loyaltyProgram",
                "ontologyExtension"
            })
            public static class Identification {

                @XmlElement(name = "Name")
                protected OntologyNameType name;
                @XmlElement(name = "Age")
                protected OntologyAgeBirthDateType age;
                @XmlElement(name = "Address")
                protected OntologyAddressType address;
                @XmlElement(name = "Contact")
                protected OntologyContactType contact;
                @XmlElement(name = "LoyaltyProgram")
                protected List<OntologyLoyaltyType> loyaltyProgram;
                @XmlElement(name = "OntologyExtension")
                protected OntologyExtensionType ontologyExtension;

                /**
                 * Obtiene el valor de la propiedad name.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OntologyNameType }
                 *     
                 */
                public OntologyNameType getName() {
                    return name;
                }

                /**
                 * Define el valor de la propiedad name.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OntologyNameType }
                 *     
                 */
                public void setName(OntologyNameType value) {
                    this.name = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OntologyAgeBirthDateType }
                 *     
                 */
                public OntologyAgeBirthDateType getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OntologyAgeBirthDateType }
                 *     
                 */
                public void setAge(OntologyAgeBirthDateType value) {
                    this.age = value;
                }

                /**
                 * Obtiene el valor de la propiedad address.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OntologyAddressType }
                 *     
                 */
                public OntologyAddressType getAddress() {
                    return address;
                }

                /**
                 * Define el valor de la propiedad address.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OntologyAddressType }
                 *     
                 */
                public void setAddress(OntologyAddressType value) {
                    this.address = value;
                }

                /**
                 * Obtiene el valor de la propiedad contact.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OntologyContactType }
                 *     
                 */
                public OntologyContactType getContact() {
                    return contact;
                }

                /**
                 * Define el valor de la propiedad contact.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OntologyContactType }
                 *     
                 */
                public void setContact(OntologyContactType value) {
                    this.contact = value;
                }

                /**
                 * Gets the value of the loyaltyProgram property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyaltyProgram property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getLoyaltyProgram().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OntologyLoyaltyType }
                 * 
                 * 
                 */
                public List<OntologyLoyaltyType> getLoyaltyProgram() {
                    if (loyaltyProgram == null) {
                        loyaltyProgram = new ArrayList<OntologyLoyaltyType>();
                    }
                    return this.loyaltyProgram;
                }

                /**
                 * Obtiene el valor de la propiedad ontologyExtension.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OntologyExtensionType }
                 *     
                 */
                public OntologyExtensionType getOntologyExtension() {
                    return ontologyExtension;
                }

                /**
                 * Define el valor de la propiedad ontologyExtension.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OntologyExtensionType }
                 *     
                 */
                public void setOntologyExtension(OntologyExtensionType value) {
                    this.ontologyExtension = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Mode" type="{http://www.opentravel.org/OTA/2003/05}OntologyTripModeType"/&amp;gt;
     *         &amp;lt;element name="BookingMethod" type="{http://www.opentravel.org/OTA/2003/05}OntologyBookingMethodType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="DateTimeDuration" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyTimeDurationType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Location" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyLocationType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PriceAndPayment" type="{http://www.opentravel.org/OTA/2003/05}OntologyPaymentType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ReservationStatus" type="{http://www.opentravel.org/OTA/2003/05}OntologyReservationStatusType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Baggage" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyBaggageType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Animals" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyAnimalType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Activity" type="{http://www.opentravel.org/OTA/2003/05}OntologyActivityType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Lodging" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyLodgingType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Transportation" type="{http://www.opentravel.org/OTA/2003/05}OntologyTransportationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="TripValue" type="{http://www.opentravel.org/OTA/2003/05}OntologyValueType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mode",
        "bookingMethod",
        "dateTimeDuration",
        "location",
        "priceAndPayment",
        "reservationStatus",
        "baggage",
        "animals",
        "activity",
        "lodging",
        "transportation",
        "tripValue",
        "ontologyExtension"
    })
    public static class TripCharacteristics {

        @XmlElement(name = "Mode", required = true)
        protected OntologyTripModeType mode;
        @XmlElement(name = "BookingMethod")
        protected OntologyBookingMethodType bookingMethod;
        @XmlElement(name = "DateTimeDuration")
        protected MultiModalOfferType.TripCharacteristics.DateTimeDuration dateTimeDuration;
        @XmlElement(name = "Location")
        protected List<MultiModalOfferType.TripCharacteristics.Location> location;
        @XmlElement(name = "PriceAndPayment")
        protected OntologyPaymentType priceAndPayment;
        @XmlElement(name = "ReservationStatus")
        protected OntologyReservationStatusType reservationStatus;
        @XmlElement(name = "Baggage")
        protected MultiModalOfferType.TripCharacteristics.Baggage baggage;
        @XmlElement(name = "Animals")
        protected MultiModalOfferType.TripCharacteristics.Animals animals;
        @XmlElement(name = "Activity")
        protected OntologyActivityType activity;
        @XmlElement(name = "Lodging")
        protected MultiModalOfferType.TripCharacteristics.Lodging lodging;
        @XmlElement(name = "Transportation")
        protected OntologyTransportationType transportation;
        @XmlElement(name = "TripValue")
        protected OntologyValueType tripValue;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;

        /**
         * Obtiene el valor de la propiedad mode.
         * 
         * @return
         *     possible object is
         *     {@link OntologyTripModeType }
         *     
         */
        public OntologyTripModeType getMode() {
            return mode;
        }

        /**
         * Define el valor de la propiedad mode.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyTripModeType }
         *     
         */
        public void setMode(OntologyTripModeType value) {
            this.mode = value;
        }

        /**
         * Obtiene el valor de la propiedad bookingMethod.
         * 
         * @return
         *     possible object is
         *     {@link OntologyBookingMethodType }
         *     
         */
        public OntologyBookingMethodType getBookingMethod() {
            return bookingMethod;
        }

        /**
         * Define el valor de la propiedad bookingMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyBookingMethodType }
         *     
         */
        public void setBookingMethod(OntologyBookingMethodType value) {
            this.bookingMethod = value;
        }

        /**
         * Obtiene el valor de la propiedad dateTimeDuration.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.TripCharacteristics.DateTimeDuration }
         *     
         */
        public MultiModalOfferType.TripCharacteristics.DateTimeDuration getDateTimeDuration() {
            return dateTimeDuration;
        }

        /**
         * Define el valor de la propiedad dateTimeDuration.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.TripCharacteristics.DateTimeDuration }
         *     
         */
        public void setDateTimeDuration(MultiModalOfferType.TripCharacteristics.DateTimeDuration value) {
            this.dateTimeDuration = value;
        }

        /**
         * Gets the value of the location property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the location property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLocation().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link MultiModalOfferType.TripCharacteristics.Location }
         * 
         * 
         */
        public List<MultiModalOfferType.TripCharacteristics.Location> getLocation() {
            if (location == null) {
                location = new ArrayList<MultiModalOfferType.TripCharacteristics.Location>();
            }
            return this.location;
        }

        /**
         * Obtiene el valor de la propiedad priceAndPayment.
         * 
         * @return
         *     possible object is
         *     {@link OntologyPaymentType }
         *     
         */
        public OntologyPaymentType getPriceAndPayment() {
            return priceAndPayment;
        }

        /**
         * Define el valor de la propiedad priceAndPayment.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyPaymentType }
         *     
         */
        public void setPriceAndPayment(OntologyPaymentType value) {
            this.priceAndPayment = value;
        }

        /**
         * Obtiene el valor de la propiedad reservationStatus.
         * 
         * @return
         *     possible object is
         *     {@link OntologyReservationStatusType }
         *     
         */
        public OntologyReservationStatusType getReservationStatus() {
            return reservationStatus;
        }

        /**
         * Define el valor de la propiedad reservationStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyReservationStatusType }
         *     
         */
        public void setReservationStatus(OntologyReservationStatusType value) {
            this.reservationStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad baggage.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.TripCharacteristics.Baggage }
         *     
         */
        public MultiModalOfferType.TripCharacteristics.Baggage getBaggage() {
            return baggage;
        }

        /**
         * Define el valor de la propiedad baggage.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.TripCharacteristics.Baggage }
         *     
         */
        public void setBaggage(MultiModalOfferType.TripCharacteristics.Baggage value) {
            this.baggage = value;
        }

        /**
         * Obtiene el valor de la propiedad animals.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.TripCharacteristics.Animals }
         *     
         */
        public MultiModalOfferType.TripCharacteristics.Animals getAnimals() {
            return animals;
        }

        /**
         * Define el valor de la propiedad animals.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.TripCharacteristics.Animals }
         *     
         */
        public void setAnimals(MultiModalOfferType.TripCharacteristics.Animals value) {
            this.animals = value;
        }

        /**
         * Obtiene el valor de la propiedad activity.
         * 
         * @return
         *     possible object is
         *     {@link OntologyActivityType }
         *     
         */
        public OntologyActivityType getActivity() {
            return activity;
        }

        /**
         * Define el valor de la propiedad activity.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyActivityType }
         *     
         */
        public void setActivity(OntologyActivityType value) {
            this.activity = value;
        }

        /**
         * Obtiene el valor de la propiedad lodging.
         * 
         * @return
         *     possible object is
         *     {@link MultiModalOfferType.TripCharacteristics.Lodging }
         *     
         */
        public MultiModalOfferType.TripCharacteristics.Lodging getLodging() {
            return lodging;
        }

        /**
         * Define el valor de la propiedad lodging.
         * 
         * @param value
         *     allowed object is
         *     {@link MultiModalOfferType.TripCharacteristics.Lodging }
         *     
         */
        public void setLodging(MultiModalOfferType.TripCharacteristics.Lodging value) {
            this.lodging = value;
        }

        /**
         * Obtiene el valor de la propiedad transportation.
         * 
         * @return
         *     possible object is
         *     {@link OntologyTransportationType }
         *     
         */
        public OntologyTransportationType getTransportation() {
            return transportation;
        }

        /**
         * Define el valor de la propiedad transportation.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyTransportationType }
         *     
         */
        public void setTransportation(OntologyTransportationType value) {
            this.transportation = value;
        }

        /**
         * Obtiene el valor de la propiedad tripValue.
         * 
         * @return
         *     possible object is
         *     {@link OntologyValueType }
         *     
         */
        public OntologyValueType getTripValue() {
            return tripValue;
        }

        /**
         * Define el valor de la propiedad tripValue.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyValueType }
         *     
         */
        public void setTripValue(OntologyValueType value) {
            this.tripValue = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyAnimalType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Animals
            extends OntologyAnimalType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyBaggageType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Baggage
            extends OntologyBaggageType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyTimeDurationType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class DateTimeDuration
            extends OntologyTimeDurationType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyLocationType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Location
            extends OntologyLocationType
        {


        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyLodgingType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Lodging
            extends OntologyLodgingType
        {


        }

    }

}
