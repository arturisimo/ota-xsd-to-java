
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="FareDisplayInfos" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="FareDisplayInfo" maxOccurs="unbounded"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="TravelDates"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="DepartureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                       &amp;lt;attribute name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                       &amp;lt;attribute name="BlackoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="FareReference" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8"/&amp;gt;
 *                               &amp;lt;element name="RuleInfo"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
 *                                       &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                       &amp;lt;attribute name="Tariff" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="FilingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
 *                               &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="DepartureLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                               &amp;lt;element name="ArrivalLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
 *                               &amp;lt;element name="Restrictions" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Restriction" maxOccurs="99"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="DateRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
 *                                                           &amp;lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                                           &amp;lt;attribute name="EndDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="DaysOfWeekRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
 *                                                           &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
 *                                                           &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
 *                                                           &amp;lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                           &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="TimeRestrictions" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="TimeRestriction" maxOccurs="10"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                     &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                     &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="Flights" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="Flight" maxOccurs="99"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;complexContent&amp;gt;
 *                                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                     &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
 *                                                                     &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *                                                                     &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                   &amp;lt;/restriction&amp;gt;
 *                                                                 &amp;lt;/complexContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="FlightNumberRange" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
 *                                                           &amp;lt;attribute name="RangeStart" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *                                                           &amp;lt;attribute name="RangeEnd" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
 *                                                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="ConnectionLocations" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;sequence&amp;gt;
 *                                                             &amp;lt;element name="ConnectionLocation" maxOccurs="99"&amp;gt;
 *                                                               &amp;lt;complexType&amp;gt;
 *                                                                 &amp;lt;simpleContent&amp;gt;
 *                                                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
 *                                                                     &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                   &amp;lt;/extension&amp;gt;
 *                                                                 &amp;lt;/simpleContent&amp;gt;
 *                                                               &amp;lt;/complexType&amp;gt;
 *                                                             &amp;lt;/element&amp;gt;
 *                                                           &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;element name="SaleDateRestriction" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="StartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                                           &amp;lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="PricingInfo"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="SeasonalInfo" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="SeasonalRange" maxOccurs="2" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                                           &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="Comment"&amp;gt;
 *                                                   &amp;lt;simpleType&amp;gt;
 *                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                       &amp;lt;enumeration value="NoComment"/&amp;gt;
 *                                                       &amp;lt;enumeration value="AllYear"/&amp;gt;
 *                                                       &amp;lt;enumeration value="MoreSeasonsInRule"/&amp;gt;
 *                                                       &amp;lt;enumeration value="PastDate"/&amp;gt;
 *                                                       &amp;lt;enumeration value="UnableToDisplaySeason"/&amp;gt;
 *                                                       &amp;lt;enumeration value="NewFare"/&amp;gt;
 *                                                     &amp;lt;/restriction&amp;gt;
 *                                                   &amp;lt;/simpleType&amp;gt;
 *                                                 &amp;lt;/attribute&amp;gt;
 *                                                 &amp;lt;attribute name="VariationByDatesExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
 *                                       &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                       &amp;lt;attribute name="ConvertedToEuroInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="CodeOnlyFaresInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                       &amp;lt;attribute name="CurrencyOverrideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="NetFare" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="MinAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                       &amp;lt;attribute name="MaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                       &amp;lt;attribute name="DistributorName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                       &amp;lt;attribute name="FareAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                       &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                                       &amp;lt;attribute name="UnlimitedAmountInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="UpdateFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="SellInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="TicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="ViewInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="RedistributeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="FareRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                             &amp;lt;attribute name="NotAvailableForFareQuotation" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="FareApplicationType" type="{http://www.opentravel.org/OTA/2003/05}FareApplicationType" /&amp;gt;
 *                             &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength1to2" /&amp;gt;
 *                             &amp;lt;attribute name="MileageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="LastFareUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                             &amp;lt;attribute name="FareIndication"&amp;gt;
 *                               &amp;lt;simpleType&amp;gt;
 *                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                   &amp;lt;enumeration value="private"/&amp;gt;
 *                                   &amp;lt;enumeration value="public"/&amp;gt;
 *                                   &amp;lt;enumeration value="net"/&amp;gt;
 *                                 &amp;lt;/restriction&amp;gt;
 *                               &amp;lt;/simpleType&amp;gt;
 *                             &amp;lt;/attribute&amp;gt;
 *                             &amp;lt;attribute name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}FareStatusType" /&amp;gt;
 *                             &amp;lt;attribute name="E_TicketRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="RestrictAutoPriceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="PenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="NewFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                             &amp;lt;attribute name="RoutingNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Routing" maxOccurs="99" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Info" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                       &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                   &amp;lt;attribute name="SystemFaresLoadDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                   &amp;lt;attribute name="GMT_FaresLoadDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                   &amp;lt;attribute name="OriginationArea" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                   &amp;lt;attribute name="DestinationArea" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                   &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                   &amp;lt;attribute name="JointFareDisplayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="FareAccess" type="{http://www.opentravel.org/OTA/2003/05}FareAccessPrefType" /&amp;gt;
 *                   &amp;lt;attribute name="MoneySaverDisplayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="OtherFareInformation" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="Airlines" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="99"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="OtherCarrierFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="GroupFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="OtherCurrencyFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="UnsellableFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                   &amp;lt;attribute name="AirportFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="ExchangeRates" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="ExchangeRate" maxOccurs="99"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="AdvisoryInfo" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "fareDisplayInfos",
    "otherFareInformation",
    "exchangeRates",
    "advisoryInfo",
    "errors"
})
@XmlRootElement(name = "OTA_AirFareDisplayRS")
public class OTAAirFareDisplayRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "FareDisplayInfos")
    protected OTAAirFareDisplayRS.FareDisplayInfos fareDisplayInfos;
    @XmlElement(name = "OtherFareInformation")
    protected OTAAirFareDisplayRS.OtherFareInformation otherFareInformation;
    @XmlElement(name = "ExchangeRates")
    protected OTAAirFareDisplayRS.ExchangeRates exchangeRates;
    @XmlElement(name = "AdvisoryInfo")
    protected List<FreeTextType> advisoryInfo;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad fareDisplayInfos.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirFareDisplayRS.FareDisplayInfos }
     *     
     */
    public OTAAirFareDisplayRS.FareDisplayInfos getFareDisplayInfos() {
        return fareDisplayInfos;
    }

    /**
     * Define el valor de la propiedad fareDisplayInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirFareDisplayRS.FareDisplayInfos }
     *     
     */
    public void setFareDisplayInfos(OTAAirFareDisplayRS.FareDisplayInfos value) {
        this.fareDisplayInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad otherFareInformation.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirFareDisplayRS.OtherFareInformation }
     *     
     */
    public OTAAirFareDisplayRS.OtherFareInformation getOtherFareInformation() {
        return otherFareInformation;
    }

    /**
     * Define el valor de la propiedad otherFareInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirFareDisplayRS.OtherFareInformation }
     *     
     */
    public void setOtherFareInformation(OTAAirFareDisplayRS.OtherFareInformation value) {
        this.otherFareInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeRates.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirFareDisplayRS.ExchangeRates }
     *     
     */
    public OTAAirFareDisplayRS.ExchangeRates getExchangeRates() {
        return exchangeRates;
    }

    /**
     * Define el valor de la propiedad exchangeRates.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirFareDisplayRS.ExchangeRates }
     *     
     */
    public void setExchangeRates(OTAAirFareDisplayRS.ExchangeRates value) {
        this.exchangeRates = value;
    }

    /**
     * Gets the value of the advisoryInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the advisoryInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAdvisoryInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FreeTextType }
     * 
     * 
     */
    public List<FreeTextType> getAdvisoryInfo() {
        if (advisoryInfo == null) {
            advisoryInfo = new ArrayList<FreeTextType>();
        }
        return this.advisoryInfo;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ExchangeRate" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exchangeRate"
    })
    public static class ExchangeRates {

        @XmlElement(name = "ExchangeRate", required = true)
        protected List<OTAAirFareDisplayRS.ExchangeRates.ExchangeRate> exchangeRate;

        /**
         * Gets the value of the exchangeRate property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the exchangeRate property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getExchangeRate().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirFareDisplayRS.ExchangeRates.ExchangeRate }
         * 
         * 
         */
        public List<OTAAirFareDisplayRS.ExchangeRates.ExchangeRate> getExchangeRate() {
            if (exchangeRate == null) {
                exchangeRate = new ArrayList<OTAAirFareDisplayRS.ExchangeRates.ExchangeRate>();
            }
            return this.exchangeRate;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ExchangeRate {

            @XmlAttribute(name = "FromCurrency")
            protected String fromCurrency;
            @XmlAttribute(name = "ToCurrency")
            protected String toCurrency;
            @XmlAttribute(name = "Rate")
            protected BigDecimal rate;
            @XmlAttribute(name = "Date")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;

            /**
             * Obtiene el valor de la propiedad fromCurrency.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFromCurrency() {
                return fromCurrency;
            }

            /**
             * Define el valor de la propiedad fromCurrency.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFromCurrency(String value) {
                this.fromCurrency = value;
            }

            /**
             * Obtiene el valor de la propiedad toCurrency.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getToCurrency() {
                return toCurrency;
            }

            /**
             * Define el valor de la propiedad toCurrency.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setToCurrency(String value) {
                this.toCurrency = value;
            }

            /**
             * Obtiene el valor de la propiedad rate.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getRate() {
                return rate;
            }

            /**
             * Define el valor de la propiedad rate.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setRate(BigDecimal value) {
                this.rate = value;
            }

            /**
             * Obtiene el valor de la propiedad date.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDate() {
                return date;
            }

            /**
             * Define el valor de la propiedad date.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDate(XMLGregorianCalendar value) {
                this.date = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FareDisplayInfo" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="TravelDates"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="DepartureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                           &amp;lt;attribute name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                           &amp;lt;attribute name="BlackoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="FareReference" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8"/&amp;gt;
     *                   &amp;lt;element name="RuleInfo"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
     *                           &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="Tariff" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="FilingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
     *                   &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="DepartureLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *                   &amp;lt;element name="ArrivalLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
     *                   &amp;lt;element name="Restrictions" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Restriction" maxOccurs="99"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="DateRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
     *                                               &amp;lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                                               &amp;lt;attribute name="EndDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                                               &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="DaysOfWeekRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
     *                                               &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
     *                                               &amp;lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                               &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="TimeRestrictions" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="TimeRestriction" maxOccurs="10"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                         &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                         &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Flights" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Flight" maxOccurs="99"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
     *                                                         &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
     *                                                         &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="FlightNumberRange" maxOccurs="5" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
     *                                               &amp;lt;attribute name="RangeStart" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
     *                                               &amp;lt;attribute name="RangeEnd" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
     *                                               &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="ConnectionLocations" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="ConnectionLocation" maxOccurs="99"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;simpleContent&amp;gt;
     *                                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
     *                                                         &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/simpleContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="SaleDateRestriction" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="StartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                                               &amp;lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                                               &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="PricingInfo"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="SeasonalInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="SeasonalRange" maxOccurs="2" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                                               &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="Comment"&amp;gt;
     *                                       &amp;lt;simpleType&amp;gt;
     *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                           &amp;lt;enumeration value="NoComment"/&amp;gt;
     *                                           &amp;lt;enumeration value="AllYear"/&amp;gt;
     *                                           &amp;lt;enumeration value="MoreSeasonsInRule"/&amp;gt;
     *                                           &amp;lt;enumeration value="PastDate"/&amp;gt;
     *                                           &amp;lt;enumeration value="UnableToDisplaySeason"/&amp;gt;
     *                                           &amp;lt;enumeration value="NewFare"/&amp;gt;
     *                                         &amp;lt;/restriction&amp;gt;
     *                                       &amp;lt;/simpleType&amp;gt;
     *                                     &amp;lt;/attribute&amp;gt;
     *                                     &amp;lt;attribute name="VariationByDatesExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
     *                           &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="ConvertedToEuroInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="CodeOnlyFaresInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="CurrencyOverrideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="NetFare" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="MinAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="MaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="DistributorName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                           &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                           &amp;lt;attribute name="FareAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *                           &amp;lt;attribute name="UnlimitedAmountInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="UpdateFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="SellInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="TicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="ViewInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="RedistributeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="FareRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="NotAvailableForFareQuotation" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="FareApplicationType" type="{http://www.opentravel.org/OTA/2003/05}FareApplicationType" /&amp;gt;
     *                 &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength1to2" /&amp;gt;
     *                 &amp;lt;attribute name="MileageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="LastFareUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="FareIndication"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="private"/&amp;gt;
     *                       &amp;lt;enumeration value="public"/&amp;gt;
     *                       &amp;lt;enumeration value="net"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}FareStatusType" /&amp;gt;
     *                 &amp;lt;attribute name="E_TicketRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="RestrictAutoPriceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="NewFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="RoutingNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Routing" maxOccurs="99" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Info" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                           &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *       &amp;lt;attribute name="SystemFaresLoadDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="GMT_FaresLoadDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="OriginationArea" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="DestinationArea" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *       &amp;lt;attribute name="JointFareDisplayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="FareAccess" type="{http://www.opentravel.org/OTA/2003/05}FareAccessPrefType" /&amp;gt;
     *       &amp;lt;attribute name="MoneySaverDisplayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fareDisplayInfo",
        "routing",
        "globalDirection"
    })
    public static class FareDisplayInfos {

        @XmlElement(name = "FareDisplayInfo", required = true)
        protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo> fareDisplayInfo;
        @XmlElement(name = "Routing")
        protected List<OTAAirFareDisplayRS.FareDisplayInfos.Routing> routing;
        @XmlElement(name = "GlobalDirection")
        protected List<OTAAirFareDisplayRS.FareDisplayInfos.GlobalDirection> globalDirection;
        @XmlAttribute(name = "SystemFaresLoadDate")
        protected String systemFaresLoadDate;
        @XmlAttribute(name = "GMT_FaresLoadDate")
        protected String gmtFaresLoadDate;
        @XmlAttribute(name = "OriginationArea")
        protected String originationArea;
        @XmlAttribute(name = "DestinationArea")
        protected String destinationArea;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "JointFareDisplayInd")
        protected Boolean jointFareDisplayInd;
        @XmlAttribute(name = "FareAccess")
        protected FareAccessPrefType fareAccess;
        @XmlAttribute(name = "MoneySaverDisplayInd")
        protected Boolean moneySaverDisplayInd;
        @XmlAttribute(name = "CompanyShortName")
        protected String companyShortName;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;

        /**
         * Gets the value of the fareDisplayInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareDisplayInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFareDisplayInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo }
         * 
         * 
         */
        public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo> getFareDisplayInfo() {
            if (fareDisplayInfo == null) {
                fareDisplayInfo = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo>();
            }
            return this.fareDisplayInfo;
        }

        /**
         * Gets the value of the routing property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the routing property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRouting().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirFareDisplayRS.FareDisplayInfos.Routing }
         * 
         * 
         */
        public List<OTAAirFareDisplayRS.FareDisplayInfos.Routing> getRouting() {
            if (routing == null) {
                routing = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.Routing>();
            }
            return this.routing;
        }

        /**
         * Gets the value of the globalDirection property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the globalDirection property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getGlobalDirection().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirFareDisplayRS.FareDisplayInfos.GlobalDirection }
         * 
         * 
         */
        public List<OTAAirFareDisplayRS.FareDisplayInfos.GlobalDirection> getGlobalDirection() {
            if (globalDirection == null) {
                globalDirection = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.GlobalDirection>();
            }
            return this.globalDirection;
        }

        /**
         * Obtiene el valor de la propiedad systemFaresLoadDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSystemFaresLoadDate() {
            return systemFaresLoadDate;
        }

        /**
         * Define el valor de la propiedad systemFaresLoadDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSystemFaresLoadDate(String value) {
            this.systemFaresLoadDate = value;
        }

        /**
         * Obtiene el valor de la propiedad gmtFaresLoadDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGMTFaresLoadDate() {
            return gmtFaresLoadDate;
        }

        /**
         * Define el valor de la propiedad gmtFaresLoadDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGMTFaresLoadDate(String value) {
            this.gmtFaresLoadDate = value;
        }

        /**
         * Obtiene el valor de la propiedad originationArea.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginationArea() {
            return originationArea;
        }

        /**
         * Define el valor de la propiedad originationArea.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginationArea(String value) {
            this.originationArea = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationArea.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationArea() {
            return destinationArea;
        }

        /**
         * Define el valor de la propiedad destinationArea.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationArea(String value) {
            this.destinationArea = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad jointFareDisplayInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isJointFareDisplayInd() {
            return jointFareDisplayInd;
        }

        /**
         * Define el valor de la propiedad jointFareDisplayInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setJointFareDisplayInd(Boolean value) {
            this.jointFareDisplayInd = value;
        }

        /**
         * Obtiene el valor de la propiedad fareAccess.
         * 
         * @return
         *     possible object is
         *     {@link FareAccessPrefType }
         *     
         */
        public FareAccessPrefType getFareAccess() {
            return fareAccess;
        }

        /**
         * Define el valor de la propiedad fareAccess.
         * 
         * @param value
         *     allowed object is
         *     {@link FareAccessPrefType }
         *     
         */
        public void setFareAccess(FareAccessPrefType value) {
            this.fareAccess = value;
        }

        /**
         * Obtiene el valor de la propiedad moneySaverDisplayInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMoneySaverDisplayInd() {
            return moneySaverDisplayInd;
        }

        /**
         * Define el valor de la propiedad moneySaverDisplayInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMoneySaverDisplayInd(Boolean value) {
            this.moneySaverDisplayInd = value;
        }

        /**
         * Obtiene el valor de la propiedad companyShortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyShortName() {
            return companyShortName;
        }

        /**
         * Define el valor de la propiedad companyShortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyShortName(String value) {
            this.companyShortName = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad countryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * Define el valor de la propiedad countryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }


        /**
         * Rules for this priced option.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="TravelDates"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="DepartureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                 &amp;lt;attribute name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                 &amp;lt;attribute name="BlackoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="FareReference" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8"/&amp;gt;
         *         &amp;lt;element name="RuleInfo"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
         *                 &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="Tariff" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="FilingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType"/&amp;gt;
         *         &amp;lt;element name="MarketingAirline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="5" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="DepartureLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
         *         &amp;lt;element name="ArrivalLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType"/&amp;gt;
         *         &amp;lt;element name="Restrictions" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Restriction" maxOccurs="99"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="DateRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
         *                                     &amp;lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *                                     &amp;lt;attribute name="EndDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *                                     &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="DaysOfWeekRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
         *                                     &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
         *                                     &amp;lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                     &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="TimeRestrictions" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="TimeRestriction" maxOccurs="10"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                               &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                               &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Flights" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Flight" maxOccurs="99"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
         *                                               &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
         *                                               &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="FlightNumberRange" maxOccurs="5" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
         *                                     &amp;lt;attribute name="RangeStart" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
         *                                     &amp;lt;attribute name="RangeEnd" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
         *                                     &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="ConnectionLocations" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="ConnectionLocation" maxOccurs="99"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;simpleContent&amp;gt;
         *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
         *                                               &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/simpleContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="SaleDateRestriction" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="StartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                                     &amp;lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                                     &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="PricingInfo"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="SeasonalInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="SeasonalRange" maxOccurs="2" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                                     &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="Comment"&amp;gt;
         *                             &amp;lt;simpleType&amp;gt;
         *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                 &amp;lt;enumeration value="NoComment"/&amp;gt;
         *                                 &amp;lt;enumeration value="AllYear"/&amp;gt;
         *                                 &amp;lt;enumeration value="MoreSeasonsInRule"/&amp;gt;
         *                                 &amp;lt;enumeration value="PastDate"/&amp;gt;
         *                                 &amp;lt;enumeration value="UnableToDisplaySeason"/&amp;gt;
         *                                 &amp;lt;enumeration value="NewFare"/&amp;gt;
         *                               &amp;lt;/restriction&amp;gt;
         *                             &amp;lt;/simpleType&amp;gt;
         *                           &amp;lt;/attribute&amp;gt;
         *                           &amp;lt;attribute name="VariationByDatesExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
         *                 &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="ConvertedToEuroInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="CodeOnlyFaresInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="CurrencyOverrideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="NetFare" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="MinAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="MaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="DistributorName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                 &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                 &amp;lt;attribute name="FareAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
         *                 &amp;lt;attribute name="UnlimitedAmountInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="UpdateFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="SellInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="TicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="ViewInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="RedistributeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="FareRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="NotAvailableForFareQuotation" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FareApplicationType" type="{http://www.opentravel.org/OTA/2003/05}FareApplicationType" /&amp;gt;
         *       &amp;lt;attribute name="ResBookDesigCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength1to2" /&amp;gt;
         *       &amp;lt;attribute name="MileageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="LastFareUpdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="FareIndication"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="private"/&amp;gt;
         *             &amp;lt;enumeration value="public"/&amp;gt;
         *             &amp;lt;enumeration value="net"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="FareStatus" type="{http://www.opentravel.org/OTA/2003/05}FareStatusType" /&amp;gt;
         *       &amp;lt;attribute name="E_TicketRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="RestrictAutoPriceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="NewFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="RoutingNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "travelDates",
            "fareReference",
            "ruleInfo",
            "filingAirline",
            "marketingAirline",
            "departureLocation",
            "arrivalLocation",
            "restrictions",
            "pricingInfo",
            "netFare"
        })
        public static class FareDisplayInfo {

            @XmlElement(name = "TravelDates", required = true)
            protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.TravelDates travelDates;
            @XmlElement(name = "FareReference", required = true)
            protected String fareReference;
            @XmlElement(name = "RuleInfo", required = true)
            protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.RuleInfo ruleInfo;
            @XmlElement(name = "FilingAirline", required = true)
            protected CompanyNameType filingAirline;
            @XmlElement(name = "MarketingAirline")
            protected List<CompanyNameType> marketingAirline;
            @XmlElement(name = "DepartureLocation", required = true)
            protected LocationType departureLocation;
            @XmlElement(name = "ArrivalLocation", required = true)
            protected LocationType arrivalLocation;
            @XmlElement(name = "Restrictions")
            protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions restrictions;
            @XmlElement(name = "PricingInfo", required = true)
            protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo pricingInfo;
            @XmlElement(name = "NetFare")
            protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.NetFare> netFare;
            @XmlAttribute(name = "FareRPH")
            protected String fareRPH;
            @XmlAttribute(name = "NotAvailableForFareQuotation")
            protected Boolean notAvailableForFareQuotation;
            @XmlAttribute(name = "FareApplicationType")
            protected FareApplicationType fareApplicationType;
            @XmlAttribute(name = "ResBookDesigCode")
            protected String resBookDesigCode;
            @XmlAttribute(name = "MileageIndicator")
            protected Boolean mileageIndicator;
            @XmlAttribute(name = "LastFareUpdate")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar lastFareUpdate;
            @XmlAttribute(name = "FareIndication")
            protected String fareIndication;
            @XmlAttribute(name = "FareStatus")
            protected FareStatusType fareStatus;
            @XmlAttribute(name = "E_TicketRequiredInd")
            protected Boolean eTicketRequiredInd;
            @XmlAttribute(name = "RestrictAutoPriceInd")
            protected Boolean restrictAutoPriceInd;
            @XmlAttribute(name = "PenaltyInd")
            protected Boolean penaltyInd;
            @XmlAttribute(name = "NewFareInd")
            protected Boolean newFareInd;
            @XmlAttribute(name = "RoutingNumber")
            protected Integer routingNumber;

            /**
             * Obtiene el valor de la propiedad travelDates.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.TravelDates }
             *     
             */
            public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.TravelDates getTravelDates() {
                return travelDates;
            }

            /**
             * Define el valor de la propiedad travelDates.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.TravelDates }
             *     
             */
            public void setTravelDates(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.TravelDates value) {
                this.travelDates = value;
            }

            /**
             * Obtiene el valor de la propiedad fareReference.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareReference() {
                return fareReference;
            }

            /**
             * Define el valor de la propiedad fareReference.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareReference(String value) {
                this.fareReference = value;
            }

            /**
             * Obtiene el valor de la propiedad ruleInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.RuleInfo }
             *     
             */
            public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.RuleInfo getRuleInfo() {
                return ruleInfo;
            }

            /**
             * Define el valor de la propiedad ruleInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.RuleInfo }
             *     
             */
            public void setRuleInfo(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.RuleInfo value) {
                this.ruleInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad filingAirline.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getFilingAirline() {
                return filingAirline;
            }

            /**
             * Define el valor de la propiedad filingAirline.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setFilingAirline(CompanyNameType value) {
                this.filingAirline = value;
            }

            /**
             * Gets the value of the marketingAirline property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingAirline property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMarketingAirline().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link CompanyNameType }
             * 
             * 
             */
            public List<CompanyNameType> getMarketingAirline() {
                if (marketingAirline == null) {
                    marketingAirline = new ArrayList<CompanyNameType>();
                }
                return this.marketingAirline;
            }

            /**
             * Obtiene el valor de la propiedad departureLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getDepartureLocation() {
                return departureLocation;
            }

            /**
             * Define el valor de la propiedad departureLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setDepartureLocation(LocationType value) {
                this.departureLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalLocation.
             * 
             * @return
             *     possible object is
             *     {@link LocationType }
             *     
             */
            public LocationType getArrivalLocation() {
                return arrivalLocation;
            }

            /**
             * Define el valor de la propiedad arrivalLocation.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationType }
             *     
             */
            public void setArrivalLocation(LocationType value) {
                this.arrivalLocation = value;
            }

            /**
             * Obtiene el valor de la propiedad restrictions.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions }
             *     
             */
            public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions getRestrictions() {
                return restrictions;
            }

            /**
             * Define el valor de la propiedad restrictions.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions }
             *     
             */
            public void setRestrictions(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions value) {
                this.restrictions = value;
            }

            /**
             * Obtiene el valor de la propiedad pricingInfo.
             * 
             * @return
             *     possible object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo }
             *     
             */
            public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo getPricingInfo() {
                return pricingInfo;
            }

            /**
             * Define el valor de la propiedad pricingInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo }
             *     
             */
            public void setPricingInfo(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo value) {
                this.pricingInfo = value;
            }

            /**
             * Gets the value of the netFare property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the netFare property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getNetFare().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.NetFare }
             * 
             * 
             */
            public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.NetFare> getNetFare() {
                if (netFare == null) {
                    netFare = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.NetFare>();
                }
                return this.netFare;
            }

            /**
             * Obtiene el valor de la propiedad fareRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareRPH() {
                return fareRPH;
            }

            /**
             * Define el valor de la propiedad fareRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareRPH(String value) {
                this.fareRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad notAvailableForFareQuotation.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isNotAvailableForFareQuotation() {
                return notAvailableForFareQuotation;
            }

            /**
             * Define el valor de la propiedad notAvailableForFareQuotation.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setNotAvailableForFareQuotation(Boolean value) {
                this.notAvailableForFareQuotation = value;
            }

            /**
             * Obtiene el valor de la propiedad fareApplicationType.
             * 
             * @return
             *     possible object is
             *     {@link FareApplicationType }
             *     
             */
            public FareApplicationType getFareApplicationType() {
                return fareApplicationType;
            }

            /**
             * Define el valor de la propiedad fareApplicationType.
             * 
             * @param value
             *     allowed object is
             *     {@link FareApplicationType }
             *     
             */
            public void setFareApplicationType(FareApplicationType value) {
                this.fareApplicationType = value;
            }

            /**
             * Obtiene el valor de la propiedad resBookDesigCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResBookDesigCode() {
                return resBookDesigCode;
            }

            /**
             * Define el valor de la propiedad resBookDesigCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResBookDesigCode(String value) {
                this.resBookDesigCode = value;
            }

            /**
             * Obtiene el valor de la propiedad mileageIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isMileageIndicator() {
                return mileageIndicator;
            }

            /**
             * Define el valor de la propiedad mileageIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setMileageIndicator(Boolean value) {
                this.mileageIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad lastFareUpdate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLastFareUpdate() {
                return lastFareUpdate;
            }

            /**
             * Define el valor de la propiedad lastFareUpdate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLastFareUpdate(XMLGregorianCalendar value) {
                this.lastFareUpdate = value;
            }

            /**
             * Obtiene el valor de la propiedad fareIndication.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareIndication() {
                return fareIndication;
            }

            /**
             * Define el valor de la propiedad fareIndication.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareIndication(String value) {
                this.fareIndication = value;
            }

            /**
             * Obtiene el valor de la propiedad fareStatus.
             * 
             * @return
             *     possible object is
             *     {@link FareStatusType }
             *     
             */
            public FareStatusType getFareStatus() {
                return fareStatus;
            }

            /**
             * Define el valor de la propiedad fareStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link FareStatusType }
             *     
             */
            public void setFareStatus(FareStatusType value) {
                this.fareStatus = value;
            }

            /**
             * Obtiene el valor de la propiedad eTicketRequiredInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isETicketRequiredInd() {
                return eTicketRequiredInd;
            }

            /**
             * Define el valor de la propiedad eTicketRequiredInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setETicketRequiredInd(Boolean value) {
                this.eTicketRequiredInd = value;
            }

            /**
             * Obtiene el valor de la propiedad restrictAutoPriceInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRestrictAutoPriceInd() {
                return restrictAutoPriceInd;
            }

            /**
             * Define el valor de la propiedad restrictAutoPriceInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRestrictAutoPriceInd(Boolean value) {
                this.restrictAutoPriceInd = value;
            }

            /**
             * Obtiene el valor de la propiedad penaltyInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPenaltyInd() {
                return penaltyInd;
            }

            /**
             * Define el valor de la propiedad penaltyInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPenaltyInd(Boolean value) {
                this.penaltyInd = value;
            }

            /**
             * Obtiene el valor de la propiedad newFareInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isNewFareInd() {
                return newFareInd;
            }

            /**
             * Define el valor de la propiedad newFareInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setNewFareInd(Boolean value) {
                this.newFareInd = value;
            }

            /**
             * Obtiene el valor de la propiedad routingNumber.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getRoutingNumber() {
                return routingNumber;
            }

            /**
             * Define el valor de la propiedad routingNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setRoutingNumber(Integer value) {
                this.routingNumber = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="MinAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="MaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="DistributorName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *       &amp;lt;attribute name="FareAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="CurrencyCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
             *       &amp;lt;attribute name="UnlimitedAmountInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="UpdateFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="SellInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="TicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="ViewInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="RedistributeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class NetFare {

                @XmlAttribute(name = "MinAmount")
                protected BigDecimal minAmount;
                @XmlAttribute(name = "MaxAmount")
                protected BigDecimal maxAmount;
                @XmlAttribute(name = "DistributorName")
                protected String distributorName;
                @XmlAttribute(name = "FareBasisCode")
                protected String fareBasisCode;
                @XmlAttribute(name = "FareAmount")
                protected BigDecimal fareAmount;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "UnlimitedAmountInd")
                protected Boolean unlimitedAmountInd;
                @XmlAttribute(name = "UpdateFareInd")
                protected Boolean updateFareInd;
                @XmlAttribute(name = "SellInd")
                protected Boolean sellInd;
                @XmlAttribute(name = "TicketInd")
                protected Boolean ticketInd;
                @XmlAttribute(name = "ViewInd")
                protected Boolean viewInd;
                @XmlAttribute(name = "RedistributeInd")
                protected Boolean redistributeInd;

                /**
                 * Obtiene el valor de la propiedad minAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getMinAmount() {
                    return minAmount;
                }

                /**
                 * Define el valor de la propiedad minAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setMinAmount(BigDecimal value) {
                    this.minAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad maxAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getMaxAmount() {
                    return maxAmount;
                }

                /**
                 * Define el valor de la propiedad maxAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setMaxAmount(BigDecimal value) {
                    this.maxAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad distributorName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDistributorName() {
                    return distributorName;
                }

                /**
                 * Define el valor de la propiedad distributorName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDistributorName(String value) {
                    this.distributorName = value;
                }

                /**
                 * Obtiene el valor de la propiedad fareBasisCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFareBasisCode() {
                    return fareBasisCode;
                }

                /**
                 * Define el valor de la propiedad fareBasisCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFareBasisCode(String value) {
                    this.fareBasisCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad fareAmount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getFareAmount() {
                    return fareAmount;
                }

                /**
                 * Define el valor de la propiedad fareAmount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setFareAmount(BigDecimal value) {
                    this.fareAmount = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad unlimitedAmountInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isUnlimitedAmountInd() {
                    return unlimitedAmountInd;
                }

                /**
                 * Define el valor de la propiedad unlimitedAmountInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setUnlimitedAmountInd(Boolean value) {
                    this.unlimitedAmountInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad updateFareInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isUpdateFareInd() {
                    return updateFareInd;
                }

                /**
                 * Define el valor de la propiedad updateFareInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setUpdateFareInd(Boolean value) {
                    this.updateFareInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad sellInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isSellInd() {
                    return sellInd;
                }

                /**
                 * Define el valor de la propiedad sellInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setSellInd(Boolean value) {
                    this.sellInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad ticketInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isTicketInd() {
                    return ticketInd;
                }

                /**
                 * Define el valor de la propiedad ticketInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setTicketInd(Boolean value) {
                    this.ticketInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad viewInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isViewInd() {
                    return viewInd;
                }

                /**
                 * Define el valor de la propiedad viewInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setViewInd(Boolean value) {
                    this.viewInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad redistributeInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isRedistributeInd() {
                    return redistributeInd;
                }

                /**
                 * Define el valor de la propiedad redistributeInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setRedistributeInd(Boolean value) {
                    this.redistributeInd = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="SeasonalInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="SeasonalRange" maxOccurs="2" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *                           &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="Comment"&amp;gt;
             *                   &amp;lt;simpleType&amp;gt;
             *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                       &amp;lt;enumeration value="NoComment"/&amp;gt;
             *                       &amp;lt;enumeration value="AllYear"/&amp;gt;
             *                       &amp;lt;enumeration value="MoreSeasonsInRule"/&amp;gt;
             *                       &amp;lt;enumeration value="PastDate"/&amp;gt;
             *                       &amp;lt;enumeration value="UnableToDisplaySeason"/&amp;gt;
             *                       &amp;lt;enumeration value="NewFare"/&amp;gt;
             *                     &amp;lt;/restriction&amp;gt;
             *                   &amp;lt;/simpleType&amp;gt;
             *                 &amp;lt;/attribute&amp;gt;
             *                 &amp;lt;attribute name="VariationByDatesExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="PassengerTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
             *       &amp;lt;attribute name="CodeContext" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="ConvertedToEuroInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="CodeOnlyFaresInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="AccountCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *       &amp;lt;attribute name="CurrencyOverrideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "seasonalInfo"
            })
            public static class PricingInfo
                extends FareType
            {

                @XmlElement(name = "SeasonalInfo")
                protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo seasonalInfo;
                @XmlAttribute(name = "PassengerTypeCode")
                protected String passengerTypeCode;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "ConvertedToEuroInd")
                protected Boolean convertedToEuroInd;
                @XmlAttribute(name = "CodeOnlyFaresInd")
                protected Boolean codeOnlyFaresInd;
                @XmlAttribute(name = "AccountCode")
                protected String accountCode;
                @XmlAttribute(name = "CurrencyOverrideInd")
                protected Boolean currencyOverrideInd;

                /**
                 * Obtiene el valor de la propiedad seasonalInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo }
                 *     
                 */
                public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo getSeasonalInfo() {
                    return seasonalInfo;
                }

                /**
                 * Define el valor de la propiedad seasonalInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo }
                 *     
                 */
                public void setSeasonalInfo(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo value) {
                    this.seasonalInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad passengerTypeCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPassengerTypeCode() {
                    return passengerTypeCode;
                }

                /**
                 * Define el valor de la propiedad passengerTypeCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPassengerTypeCode(String value) {
                    this.passengerTypeCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad convertedToEuroInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isConvertedToEuroInd() {
                    return convertedToEuroInd;
                }

                /**
                 * Define el valor de la propiedad convertedToEuroInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setConvertedToEuroInd(Boolean value) {
                    this.convertedToEuroInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeOnlyFaresInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isCodeOnlyFaresInd() {
                    return codeOnlyFaresInd;
                }

                /**
                 * Define el valor de la propiedad codeOnlyFaresInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setCodeOnlyFaresInd(Boolean value) {
                    this.codeOnlyFaresInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad accountCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAccountCode() {
                    return accountCode;
                }

                /**
                 * Define el valor de la propiedad accountCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAccountCode(String value) {
                    this.accountCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyOverrideInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isCurrencyOverrideInd() {
                    return currencyOverrideInd;
                }

                /**
                 * Define el valor de la propiedad currencyOverrideInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setCurrencyOverrideInd(Boolean value) {
                    this.currencyOverrideInd = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="SeasonalRange" maxOccurs="2" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="Comment"&amp;gt;
                 *         &amp;lt;simpleType&amp;gt;
                 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *             &amp;lt;enumeration value="NoComment"/&amp;gt;
                 *             &amp;lt;enumeration value="AllYear"/&amp;gt;
                 *             &amp;lt;enumeration value="MoreSeasonsInRule"/&amp;gt;
                 *             &amp;lt;enumeration value="PastDate"/&amp;gt;
                 *             &amp;lt;enumeration value="UnableToDisplaySeason"/&amp;gt;
                 *             &amp;lt;enumeration value="NewFare"/&amp;gt;
                 *           &amp;lt;/restriction&amp;gt;
                 *         &amp;lt;/simpleType&amp;gt;
                 *       &amp;lt;/attribute&amp;gt;
                 *       &amp;lt;attribute name="VariationByDatesExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "seasonalRange"
                })
                public static class SeasonalInfo {

                    @XmlElement(name = "SeasonalRange")
                    protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo.SeasonalRange> seasonalRange;
                    @XmlAttribute(name = "Comment")
                    protected String comment;
                    @XmlAttribute(name = "VariationByDatesExistInd")
                    protected Boolean variationByDatesExistInd;

                    /**
                     * Gets the value of the seasonalRange property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the seasonalRange property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getSeasonalRange().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo.SeasonalRange }
                     * 
                     * 
                     */
                    public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo.SeasonalRange> getSeasonalRange() {
                        if (seasonalRange == null) {
                            seasonalRange = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.PricingInfo.SeasonalInfo.SeasonalRange>();
                        }
                        return this.seasonalRange;
                    }

                    /**
                     * Obtiene el valor de la propiedad comment.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getComment() {
                        return comment;
                    }

                    /**
                     * Define el valor de la propiedad comment.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setComment(String value) {
                        this.comment = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad variationByDatesExistInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isVariationByDatesExistInd() {
                        return variationByDatesExistInd;
                    }

                    /**
                     * Define el valor de la propiedad variationByDatesExistInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setVariationByDatesExistInd(Boolean value) {
                        this.variationByDatesExistInd = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
                     *       &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class SeasonalRange {

                        @XmlAttribute(name = "Direction")
                        protected AirTripType direction;
                        @XmlAttribute(name = "Start")
                        protected String start;
                        @XmlAttribute(name = "Duration")
                        protected String duration;
                        @XmlAttribute(name = "End")
                        protected String end;

                        /**
                         * Obtiene el valor de la propiedad direction.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirTripType }
                         *     
                         */
                        public AirTripType getDirection() {
                            return direction;
                        }

                        /**
                         * Define el valor de la propiedad direction.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirTripType }
                         *     
                         */
                        public void setDirection(AirTripType value) {
                            this.direction = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad start.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getStart() {
                            return start;
                        }

                        /**
                         * Define el valor de la propiedad start.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setStart(String value) {
                            this.start = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad duration.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getDuration() {
                            return duration;
                        }

                        /**
                         * Define el valor de la propiedad duration.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setDuration(String value) {
                            this.duration = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad end.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getEnd() {
                            return end;
                        }

                        /**
                         * Define el valor de la propiedad end.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEnd(String value) {
                            this.end = value;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Restriction" maxOccurs="99"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="DateRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
             *                           &amp;lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *                           &amp;lt;attribute name="EndDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="DaysOfWeekRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
             *                           &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
             *                           &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
             *                           &amp;lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                           &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="TimeRestrictions" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="TimeRestriction" maxOccurs="10"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                     &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                     &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Flights" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Flight" maxOccurs="99"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
             *                                     &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
             *                                     &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="FlightNumberRange" maxOccurs="5" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
             *                           &amp;lt;attribute name="RangeStart" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
             *                           &amp;lt;attribute name="RangeEnd" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
             *                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="ConnectionLocations" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="ConnectionLocation" maxOccurs="99"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;simpleContent&amp;gt;
             *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
             *                                     &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/simpleContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="SaleDateRestriction" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="StartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *                           &amp;lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "restriction"
            })
            public static class Restrictions {

                @XmlElement(name = "Restriction", required = true)
                protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction> restriction;

                /**
                 * Gets the value of the restriction property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restriction property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getRestriction().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction }
                 * 
                 * 
                 */
                public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction> getRestriction() {
                    if (restriction == null) {
                        restriction = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction>();
                    }
                    return this.restriction;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="DateRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
                 *                 &amp;lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
                 *                 &amp;lt;attribute name="EndDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
                 *                 &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="DaysOfWeekRestriction" maxOccurs="3" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
                 *                 &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
                 *                 &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
                 *                 &amp;lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                 &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="TimeRestrictions" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="TimeRestriction" maxOccurs="10"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                           &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                           &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Flights" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Flight" maxOccurs="99"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                 *                           &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                 *                           &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="FlightNumberRange" maxOccurs="5" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                 *                 &amp;lt;attribute name="RangeStart" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                 *                 &amp;lt;attribute name="RangeEnd" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                 *                 &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="ConnectionLocations" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="ConnectionLocation" maxOccurs="99"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;simpleContent&amp;gt;
                 *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
                 *                           &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/simpleContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="GlobalDirection" maxOccurs="5" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="SaleDateRestriction" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="StartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                 *                 &amp;lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                 *                 &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "dateRestriction",
                    "daysOfWeekRestriction",
                    "timeRestrictions",
                    "flights",
                    "flightNumberRange",
                    "connectionLocations",
                    "globalDirection",
                    "saleDateRestriction"
                })
                public static class Restriction {

                    @XmlElement(name = "DateRestriction")
                    protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DateRestriction> dateRestriction;
                    @XmlElement(name = "DaysOfWeekRestriction")
                    protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DaysOfWeekRestriction> daysOfWeekRestriction;
                    @XmlElement(name = "TimeRestrictions")
                    protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions timeRestrictions;
                    @XmlElement(name = "Flights")
                    protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights flights;
                    @XmlElement(name = "FlightNumberRange")
                    protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.FlightNumberRange> flightNumberRange;
                    @XmlElement(name = "ConnectionLocations")
                    protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations connectionLocations;
                    @XmlElement(name = "GlobalDirection")
                    protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.GlobalDirection> globalDirection;
                    @XmlElement(name = "SaleDateRestriction")
                    protected OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.SaleDateRestriction saleDateRestriction;

                    /**
                     * Gets the value of the dateRestriction property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the dateRestriction property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getDateRestriction().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DateRestriction }
                     * 
                     * 
                     */
                    public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DateRestriction> getDateRestriction() {
                        if (dateRestriction == null) {
                            dateRestriction = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DateRestriction>();
                        }
                        return this.dateRestriction;
                    }

                    /**
                     * Gets the value of the daysOfWeekRestriction property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the daysOfWeekRestriction property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getDaysOfWeekRestriction().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DaysOfWeekRestriction }
                     * 
                     * 
                     */
                    public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DaysOfWeekRestriction> getDaysOfWeekRestriction() {
                        if (daysOfWeekRestriction == null) {
                            daysOfWeekRestriction = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.DaysOfWeekRestriction>();
                        }
                        return this.daysOfWeekRestriction;
                    }

                    /**
                     * Obtiene el valor de la propiedad timeRestrictions.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions }
                     *     
                     */
                    public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions getTimeRestrictions() {
                        return timeRestrictions;
                    }

                    /**
                     * Define el valor de la propiedad timeRestrictions.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions }
                     *     
                     */
                    public void setTimeRestrictions(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions value) {
                        this.timeRestrictions = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad flights.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights }
                     *     
                     */
                    public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights getFlights() {
                        return flights;
                    }

                    /**
                     * Define el valor de la propiedad flights.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights }
                     *     
                     */
                    public void setFlights(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights value) {
                        this.flights = value;
                    }

                    /**
                     * Gets the value of the flightNumberRange property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightNumberRange property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getFlightNumberRange().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.FlightNumberRange }
                     * 
                     * 
                     */
                    public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.FlightNumberRange> getFlightNumberRange() {
                        if (flightNumberRange == null) {
                            flightNumberRange = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.FlightNumberRange>();
                        }
                        return this.flightNumberRange;
                    }

                    /**
                     * Obtiene el valor de la propiedad connectionLocations.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations }
                     *     
                     */
                    public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations getConnectionLocations() {
                        return connectionLocations;
                    }

                    /**
                     * Define el valor de la propiedad connectionLocations.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations }
                     *     
                     */
                    public void setConnectionLocations(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations value) {
                        this.connectionLocations = value;
                    }

                    /**
                     * Gets the value of the globalDirection property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the globalDirection property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getGlobalDirection().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.GlobalDirection }
                     * 
                     * 
                     */
                    public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.GlobalDirection> getGlobalDirection() {
                        if (globalDirection == null) {
                            globalDirection = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.GlobalDirection>();
                        }
                        return this.globalDirection;
                    }

                    /**
                     * Obtiene el valor de la propiedad saleDateRestriction.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.SaleDateRestriction }
                     *     
                     */
                    public OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.SaleDateRestriction getSaleDateRestriction() {
                        return saleDateRestriction;
                    }

                    /**
                     * Define el valor de la propiedad saleDateRestriction.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.SaleDateRestriction }
                     *     
                     */
                    public void setSaleDateRestriction(OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.SaleDateRestriction value) {
                        this.saleDateRestriction = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="ConnectionLocation" maxOccurs="99"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;simpleContent&amp;gt;
                     *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
                     *                 &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/simpleContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "connectionLocation"
                    })
                    public static class ConnectionLocations {

                        @XmlElement(name = "ConnectionLocation", required = true)
                        protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations.ConnectionLocation> connectionLocation;

                        /**
                         * Gets the value of the connectionLocation property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the connectionLocation property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getConnectionLocation().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations.ConnectionLocation }
                         * 
                         * 
                         */
                        public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations.ConnectionLocation> getConnectionLocation() {
                            if (connectionLocation == null) {
                                connectionLocation = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.ConnectionLocations.ConnectionLocation>();
                            }
                            return this.connectionLocation;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;simpleContent&amp;gt;
                         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;LocationType"&amp;gt;
                         *       &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/simpleContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class ConnectionLocation
                            extends LocationType
                        {

                            @XmlAttribute(name = "IncludeIndicator")
                            protected Boolean includeIndicator;

                            /**
                             * Obtiene el valor de la propiedad includeIndicator.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isIncludeIndicator() {
                                return includeIndicator;
                            }

                            /**
                             * Define el valor de la propiedad includeIndicator.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setIncludeIndicator(Boolean value) {
                                this.includeIndicator = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
                     *       &amp;lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
                     *       &amp;lt;attribute name="EndDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
                     *       &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class DateRestriction {

                        @XmlAttribute(name = "Application")
                        protected AirTripDirectionType application;
                        @XmlAttribute(name = "StartDate", required = true)
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar startDate;
                        @XmlAttribute(name = "EndDate", required = true)
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar endDate;
                        @XmlAttribute(name = "IncludeIndicator")
                        protected Boolean includeIndicator;

                        /**
                         * Obtiene el valor de la propiedad application.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirTripDirectionType }
                         *     
                         */
                        public AirTripDirectionType getApplication() {
                            return application;
                        }

                        /**
                         * Define el valor de la propiedad application.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirTripDirectionType }
                         *     
                         */
                        public void setApplication(AirTripDirectionType value) {
                            this.application = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad startDate.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getStartDate() {
                            return startDate;
                        }

                        /**
                         * Define el valor de la propiedad startDate.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setStartDate(XMLGregorianCalendar value) {
                            this.startDate = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad endDate.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getEndDate() {
                            return endDate;
                        }

                        /**
                         * Define el valor de la propiedad endDate.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setEndDate(XMLGregorianCalendar value) {
                            this.endDate = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad includeIndicator.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isIncludeIndicator() {
                            return includeIndicator;
                        }

                        /**
                         * Define el valor de la propiedad includeIndicator.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setIncludeIndicator(Boolean value) {
                            this.includeIndicator = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DOW_PatternGroup"/&amp;gt;
                     *       &amp;lt;attribute name="Application" type="{http://www.opentravel.org/OTA/2003/05}AirTripDirectionType" /&amp;gt;
                     *       &amp;lt;attribute name="TripType" type="{http://www.opentravel.org/OTA/2003/05}AirTripType" /&amp;gt;
                     *       &amp;lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *       &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class DaysOfWeekRestriction {

                        @XmlAttribute(name = "Application")
                        protected AirTripDirectionType application;
                        @XmlAttribute(name = "TripType")
                        protected AirTripType tripType;
                        @XmlAttribute(name = "RestrictionExistsInd")
                        protected Boolean restrictionExistsInd;
                        @XmlAttribute(name = "IncludeExcludeUseInd")
                        protected Boolean includeExcludeUseInd;
                        @XmlAttribute(name = "Mon")
                        protected Boolean mon;
                        @XmlAttribute(name = "Tue")
                        protected Boolean tue;
                        @XmlAttribute(name = "Weds")
                        protected Boolean weds;
                        @XmlAttribute(name = "Thur")
                        protected Boolean thur;
                        @XmlAttribute(name = "Fri")
                        protected Boolean fri;
                        @XmlAttribute(name = "Sat")
                        protected Boolean sat;
                        @XmlAttribute(name = "Sun")
                        protected Boolean sun;

                        /**
                         * Obtiene el valor de la propiedad application.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirTripDirectionType }
                         *     
                         */
                        public AirTripDirectionType getApplication() {
                            return application;
                        }

                        /**
                         * Define el valor de la propiedad application.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirTripDirectionType }
                         *     
                         */
                        public void setApplication(AirTripDirectionType value) {
                            this.application = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad tripType.
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirTripType }
                         *     
                         */
                        public AirTripType getTripType() {
                            return tripType;
                        }

                        /**
                         * Define el valor de la propiedad tripType.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirTripType }
                         *     
                         */
                        public void setTripType(AirTripType value) {
                            this.tripType = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad restrictionExistsInd.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isRestrictionExistsInd() {
                            return restrictionExistsInd;
                        }

                        /**
                         * Define el valor de la propiedad restrictionExistsInd.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setRestrictionExistsInd(Boolean value) {
                            this.restrictionExistsInd = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad includeExcludeUseInd.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isIncludeExcludeUseInd() {
                            return includeExcludeUseInd;
                        }

                        /**
                         * Define el valor de la propiedad includeExcludeUseInd.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setIncludeExcludeUseInd(Boolean value) {
                            this.includeExcludeUseInd = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad mon.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isMon() {
                            return mon;
                        }

                        /**
                         * Define el valor de la propiedad mon.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setMon(Boolean value) {
                            this.mon = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad tue.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isTue() {
                            return tue;
                        }

                        /**
                         * Define el valor de la propiedad tue.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setTue(Boolean value) {
                            this.tue = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad weds.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isWeds() {
                            return weds;
                        }

                        /**
                         * Define el valor de la propiedad weds.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setWeds(Boolean value) {
                            this.weds = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad thur.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isThur() {
                            return thur;
                        }

                        /**
                         * Define el valor de la propiedad thur.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setThur(Boolean value) {
                            this.thur = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad fri.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isFri() {
                            return fri;
                        }

                        /**
                         * Define el valor de la propiedad fri.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setFri(Boolean value) {
                            this.fri = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad sat.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isSat() {
                            return sat;
                        }

                        /**
                         * Define el valor de la propiedad sat.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setSat(Boolean value) {
                            this.sat = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad sun.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isSun() {
                            return sun;
                        }

                        /**
                         * Define el valor de la propiedad sun.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setSun(Boolean value) {
                            this.sun = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                     *       &amp;lt;attribute name="RangeStart" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                     *       &amp;lt;attribute name="RangeEnd" use="required" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                     *       &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class FlightNumberRange {

                        @XmlAttribute(name = "AirlineCode")
                        protected String airlineCode;
                        @XmlAttribute(name = "RangeStart", required = true)
                        protected String rangeStart;
                        @XmlAttribute(name = "RangeEnd", required = true)
                        protected String rangeEnd;
                        @XmlAttribute(name = "IncludeIndicator")
                        protected Boolean includeIndicator;

                        /**
                         * Obtiene el valor de la propiedad airlineCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getAirlineCode() {
                            return airlineCode;
                        }

                        /**
                         * Define el valor de la propiedad airlineCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setAirlineCode(String value) {
                            this.airlineCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad rangeStart.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRangeStart() {
                            return rangeStart;
                        }

                        /**
                         * Define el valor de la propiedad rangeStart.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRangeStart(String value) {
                            this.rangeStart = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad rangeEnd.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRangeEnd() {
                            return rangeEnd;
                        }

                        /**
                         * Define el valor de la propiedad rangeEnd.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRangeEnd(String value) {
                            this.rangeEnd = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad includeIndicator.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isIncludeIndicator() {
                            return includeIndicator;
                        }

                        /**
                         * Define el valor de la propiedad includeIndicator.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setIncludeIndicator(Boolean value) {
                            this.includeIndicator = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Flight" maxOccurs="99"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                     *                 &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                     *                 &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "flight"
                    })
                    public static class Flights {

                        @XmlElement(name = "Flight", required = true)
                        protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights.Flight> flight;

                        /**
                         * Gets the value of the flight property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flight property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getFlight().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights.Flight }
                         * 
                         * 
                         */
                        public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights.Flight> getFlight() {
                            if (flight == null) {
                                flight = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.Flights.Flight>();
                            }
                            return this.flight;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attribute name="AirlineCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                         *       &amp;lt;attribute name="FlightNumber" type="{http://www.opentravel.org/OTA/2003/05}FlightNumberType" /&amp;gt;
                         *       &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class Flight {

                            @XmlAttribute(name = "AirlineCode")
                            protected String airlineCode;
                            @XmlAttribute(name = "FlightNumber")
                            protected String flightNumber;
                            @XmlAttribute(name = "IncludeExcludeUseInd")
                            protected Boolean includeExcludeUseInd;

                            /**
                             * Obtiene el valor de la propiedad airlineCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAirlineCode() {
                                return airlineCode;
                            }

                            /**
                             * Define el valor de la propiedad airlineCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAirlineCode(String value) {
                                this.airlineCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad flightNumber.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getFlightNumber() {
                                return flightNumber;
                            }

                            /**
                             * Define el valor de la propiedad flightNumber.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setFlightNumber(String value) {
                                this.flightNumber = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad includeExcludeUseInd.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isIncludeExcludeUseInd() {
                                return includeExcludeUseInd;
                            }

                            /**
                             * Define el valor de la propiedad includeExcludeUseInd.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setIncludeExcludeUseInd(Boolean value) {
                                this.includeExcludeUseInd = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class GlobalDirection {

                        @XmlAttribute(name = "GlobalIndicatorCode")
                        protected GlobalIndicatorType globalIndicatorCode;
                        @XmlAttribute(name = "MaximumPermittedMileage")
                        protected BigInteger maximumPermittedMileage;
                        @XmlAttribute(name = "IncludeIndicator")
                        protected Boolean includeIndicator;

                        /**
                         * Obtiene el valor de la propiedad globalIndicatorCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link GlobalIndicatorType }
                         *     
                         */
                        public GlobalIndicatorType getGlobalIndicatorCode() {
                            return globalIndicatorCode;
                        }

                        /**
                         * Define el valor de la propiedad globalIndicatorCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link GlobalIndicatorType }
                         *     
                         */
                        public void setGlobalIndicatorCode(GlobalIndicatorType value) {
                            this.globalIndicatorCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad maximumPermittedMileage.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getMaximumPermittedMileage() {
                            return maximumPermittedMileage;
                        }

                        /**
                         * Define el valor de la propiedad maximumPermittedMileage.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setMaximumPermittedMileage(BigInteger value) {
                            this.maximumPermittedMileage = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad includeIndicator.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isIncludeIndicator() {
                            return includeIndicator;
                        }

                        /**
                         * Define el valor de la propiedad includeIndicator.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setIncludeIndicator(Boolean value) {
                            this.includeIndicator = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="StartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                     *       &amp;lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
                     *       &amp;lt;attribute name="IncludeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class SaleDateRestriction {

                        @XmlAttribute(name = "StartDate")
                        @XmlSchemaType(name = "date")
                        protected XMLGregorianCalendar startDate;
                        @XmlAttribute(name = "EndDate")
                        @XmlSchemaType(name = "date")
                        protected XMLGregorianCalendar endDate;
                        @XmlAttribute(name = "IncludeIndicator")
                        protected Boolean includeIndicator;

                        /**
                         * Obtiene el valor de la propiedad startDate.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getStartDate() {
                            return startDate;
                        }

                        /**
                         * Define el valor de la propiedad startDate.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setStartDate(XMLGregorianCalendar value) {
                            this.startDate = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad endDate.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getEndDate() {
                            return endDate;
                        }

                        /**
                         * Define el valor de la propiedad endDate.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setEndDate(XMLGregorianCalendar value) {
                            this.endDate = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad includeIndicator.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isIncludeIndicator() {
                            return includeIndicator;
                        }

                        /**
                         * Define el valor de la propiedad includeIndicator.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setIncludeIndicator(Boolean value) {
                            this.includeIndicator = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="TimeRestriction" maxOccurs="10"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                 &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                 &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "timeRestriction"
                    })
                    public static class TimeRestrictions {

                        @XmlElement(name = "TimeRestriction", required = true)
                        protected List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions.TimeRestriction> timeRestriction;

                        /**
                         * Gets the value of the timeRestriction property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the timeRestriction property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getTimeRestriction().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions.TimeRestriction }
                         * 
                         * 
                         */
                        public List<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions.TimeRestriction> getTimeRestriction() {
                            if (timeRestriction == null) {
                                timeRestriction = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.FareDisplayInfo.Restrictions.Restriction.TimeRestrictions.TimeRestriction>();
                            }
                            return this.timeRestriction;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;attribute name="StartTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *       &amp;lt;attribute name="EndTime" use="required" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *       &amp;lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class TimeRestriction {

                            @XmlAttribute(name = "StartTime", required = true)
                            @XmlSchemaType(name = "time")
                            protected XMLGregorianCalendar startTime;
                            @XmlAttribute(name = "EndTime", required = true)
                            @XmlSchemaType(name = "time")
                            protected XMLGregorianCalendar endTime;
                            @XmlAttribute(name = "IncludeExcludeUseInd")
                            protected Boolean includeExcludeUseInd;

                            /**
                             * Obtiene el valor de la propiedad startTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getStartTime() {
                                return startTime;
                            }

                            /**
                             * Define el valor de la propiedad startTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setStartTime(XMLGregorianCalendar value) {
                                this.startTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad endTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getEndTime() {
                                return endTime;
                            }

                            /**
                             * Define el valor de la propiedad endTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setEndTime(XMLGregorianCalendar value) {
                                this.endTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad includeExcludeUseInd.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isIncludeExcludeUseInd() {
                                return includeExcludeUseInd;
                            }

                            /**
                             * Define el valor de la propiedad includeExcludeUseInd.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setIncludeExcludeUseInd(Boolean value) {
                                this.includeExcludeUseInd = value;
                            }

                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RuleInfoType"&amp;gt;
             *       &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="Tariff" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class RuleInfo
                extends RuleInfoType
            {

                @XmlAttribute(name = "RuleNumber")
                protected String ruleNumber;
                @XmlAttribute(name = "Tariff")
                protected String tariff;

                /**
                 * Obtiene el valor de la propiedad ruleNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRuleNumber() {
                    return ruleNumber;
                }

                /**
                 * Define el valor de la propiedad ruleNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRuleNumber(String value) {
                    this.ruleNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad tariff.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTariff() {
                    return tariff;
                }

                /**
                 * Define el valor de la propiedad tariff.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTariff(String value) {
                    this.tariff = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="DepartureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *       &amp;lt;attribute name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *       &amp;lt;attribute name="BlackoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class TravelDates {

                @XmlAttribute(name = "DepartureDate", required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar departureDate;
                @XmlAttribute(name = "ArrivalDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar arrivalDate;
                @XmlAttribute(name = "BlackoutDatesInd")
                protected Boolean blackoutDatesInd;

                /**
                 * Obtiene el valor de la propiedad departureDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDepartureDate() {
                    return departureDate;
                }

                /**
                 * Define el valor de la propiedad departureDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDepartureDate(XMLGregorianCalendar value) {
                    this.departureDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad arrivalDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getArrivalDate() {
                    return arrivalDate;
                }

                /**
                 * Define el valor de la propiedad arrivalDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setArrivalDate(XMLGregorianCalendar value) {
                    this.arrivalDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad blackoutDatesInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isBlackoutDatesInd() {
                    return blackoutDatesInd;
                }

                /**
                 * Define el valor de la propiedad blackoutDatesInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setBlackoutDatesInd(Boolean value) {
                    this.blackoutDatesInd = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GlobalDirectionGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class GlobalDirection {

            @XmlAttribute(name = "GlobalIndicatorCode")
            protected GlobalIndicatorType globalIndicatorCode;
            @XmlAttribute(name = "MaximumPermittedMileage")
            protected BigInteger maximumPermittedMileage;
            @XmlAttribute(name = "IncludeIndicator")
            protected Boolean includeIndicator;

            /**
             * Obtiene el valor de la propiedad globalIndicatorCode.
             * 
             * @return
             *     possible object is
             *     {@link GlobalIndicatorType }
             *     
             */
            public GlobalIndicatorType getGlobalIndicatorCode() {
                return globalIndicatorCode;
            }

            /**
             * Define el valor de la propiedad globalIndicatorCode.
             * 
             * @param value
             *     allowed object is
             *     {@link GlobalIndicatorType }
             *     
             */
            public void setGlobalIndicatorCode(GlobalIndicatorType value) {
                this.globalIndicatorCode = value;
            }

            /**
             * Obtiene el valor de la propiedad maximumPermittedMileage.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaximumPermittedMileage() {
                return maximumPermittedMileage;
            }

            /**
             * Define el valor de la propiedad maximumPermittedMileage.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaximumPermittedMileage(BigInteger value) {
                this.maximumPermittedMileage = value;
            }

            /**
             * Obtiene el valor de la propiedad includeIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIncludeIndicator() {
                return includeIndicator;
            }

            /**
             * Define el valor de la propiedad includeIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIncludeIndicator(Boolean value) {
                this.includeIndicator = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Info" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                 &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to9999" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "info"
        })
        public static class Routing {

            @XmlElement(name = "Info")
            protected List<OTAAirFareDisplayRS.FareDisplayInfos.Routing.Info> info;
            @XmlAttribute(name = "Number", required = true)
            protected int number;

            /**
             * Gets the value of the info property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the info property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirFareDisplayRS.FareDisplayInfos.Routing.Info }
             * 
             * 
             */
            public List<OTAAirFareDisplayRS.FareDisplayInfos.Routing.Info> getInfo() {
                if (info == null) {
                    info = new ArrayList<OTAAirFareDisplayRS.FareDisplayInfos.Routing.Info>();
                }
                return this.info;
            }

            /**
             * Obtiene el valor de la propiedad number.
             * 
             */
            public int getNumber() {
                return number;
            }

            /**
             * Define el valor de la propiedad number.
             * 
             */
            public void setNumber(int value) {
                this.number = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Direction" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *       &amp;lt;attribute name="Text" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Info {

                @XmlAttribute(name = "Direction")
                protected String direction;
                @XmlAttribute(name = "Text")
                protected String text;

                /**
                 * Obtiene el valor de la propiedad direction.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDirection() {
                    return direction;
                }

                /**
                 * Define el valor de la propiedad direction.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDirection(String value) {
                    this.direction = value;
                }

                /**
                 * Obtiene el valor de la propiedad text.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getText() {
                    return text;
                }

                /**
                 * Define el valor de la propiedad text.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setText(String value) {
                    this.text = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Airlines" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="OtherCarrierFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="GroupFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="OtherCurrencyFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="UnsellableFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="AirportFaresExistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airlines"
    })
    public static class OtherFareInformation {

        @XmlElement(name = "Airlines")
        protected OTAAirFareDisplayRS.OtherFareInformation.Airlines airlines;
        @XmlAttribute(name = "OtherCarrierFaresExistInd")
        protected Boolean otherCarrierFaresExistInd;
        @XmlAttribute(name = "GroupFaresExistInd")
        protected Boolean groupFaresExistInd;
        @XmlAttribute(name = "OtherCurrencyFaresExistInd")
        protected Boolean otherCurrencyFaresExistInd;
        @XmlAttribute(name = "UnsellableFaresExistInd")
        protected Boolean unsellableFaresExistInd;
        @XmlAttribute(name = "AirportFaresExistInd")
        protected Boolean airportFaresExistInd;

        /**
         * Obtiene el valor de la propiedad airlines.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirFareDisplayRS.OtherFareInformation.Airlines }
         *     
         */
        public OTAAirFareDisplayRS.OtherFareInformation.Airlines getAirlines() {
            return airlines;
        }

        /**
         * Define el valor de la propiedad airlines.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirFareDisplayRS.OtherFareInformation.Airlines }
         *     
         */
        public void setAirlines(OTAAirFareDisplayRS.OtherFareInformation.Airlines value) {
            this.airlines = value;
        }

        /**
         * Obtiene el valor de la propiedad otherCarrierFaresExistInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOtherCarrierFaresExistInd() {
            return otherCarrierFaresExistInd;
        }

        /**
         * Define el valor de la propiedad otherCarrierFaresExistInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOtherCarrierFaresExistInd(Boolean value) {
            this.otherCarrierFaresExistInd = value;
        }

        /**
         * Obtiene el valor de la propiedad groupFaresExistInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGroupFaresExistInd() {
            return groupFaresExistInd;
        }

        /**
         * Define el valor de la propiedad groupFaresExistInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGroupFaresExistInd(Boolean value) {
            this.groupFaresExistInd = value;
        }

        /**
         * Obtiene el valor de la propiedad otherCurrencyFaresExistInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOtherCurrencyFaresExistInd() {
            return otherCurrencyFaresExistInd;
        }

        /**
         * Define el valor de la propiedad otherCurrencyFaresExistInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOtherCurrencyFaresExistInd(Boolean value) {
            this.otherCurrencyFaresExistInd = value;
        }

        /**
         * Obtiene el valor de la propiedad unsellableFaresExistInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isUnsellableFaresExistInd() {
            return unsellableFaresExistInd;
        }

        /**
         * Define el valor de la propiedad unsellableFaresExistInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setUnsellableFaresExistInd(Boolean value) {
            this.unsellableFaresExistInd = value;
        }

        /**
         * Obtiene el valor de la propiedad airportFaresExistInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAirportFaresExistInd() {
            return airportFaresExistInd;
        }

        /**
         * Define el valor de la propiedad airportFaresExistInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAirportFaresExistInd(Boolean value) {
            this.airportFaresExistInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Airline" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "airline"
        })
        public static class Airlines {

            @XmlElement(name = "Airline", required = true)
            protected List<CompanyNameType> airline;

            /**
             * Gets the value of the airline property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airline property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAirline().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link CompanyNameType }
             * 
             * 
             */
            public List<CompanyNameType> getAirline() {
                if (airline == null) {
                    airline = new ArrayList<CompanyNameType>();
                }
                return this.airline;
            }

        }

    }

}
