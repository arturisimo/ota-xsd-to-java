
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * A container for electronic miscellaneous document information
 * 
 * &lt;p&gt;Clase Java para EMD_Type complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="EMD_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="TravelerRefNumber" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerRefNumberGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AgentID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="4" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PaymentDetail" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="OriginDestination" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CustLoyalty" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Endorsement" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Info" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AddReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BaseFare" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *                 &amp;lt;attribute name="FareAmountType" type="{http://www.opentravel.org/OTA/2003/05}FareAmountType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="EquivFare" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *                 &amp;lt;attribute name="FareAmountType" type="{http://www.opentravel.org/OTA/2003/05}FareAmountType" /&amp;gt;
 *                 &amp;lt;attribute name="BankExchangeRate" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TotalFare" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *                 &amp;lt;attribute name="FareAmountType" type="{http://www.opentravel.org/OTA/2003/05}FareAmountType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Taxes" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Tax" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;simpleContent&amp;gt;
 *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AirTaxType"&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/simpleContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="UnstructuredFareCalc" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                 &amp;lt;attribute name="FareCalcMode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
 *                 &amp;lt;attribute name="Operation" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *                 &amp;lt;attribute name="ReportingCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
 *                 &amp;lt;attribute name="Info" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FareInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ET_FareInfo"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PenaltyAmount" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" maxOccurs="3" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                 &amp;lt;attribute name="NonEndorsableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NonRefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="PenaltyRestrictionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="PresentCreditCardInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="AroundTheWorldFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NonInterlineableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NonCommissionableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="NonReissuableNonExchgInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TicketDocument" maxOccurs="99"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CouponInfo" maxOccurs="4"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="SoldFlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;choice&amp;gt;
 *                               &amp;lt;element name="CheckedInAirlineRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="FlownAirlineSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/choice&amp;gt;
 *                             &amp;lt;element name="ExcessBaggage" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="PresentInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="To" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                     &amp;lt;attribute name="At" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ReasonForIssuance" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReasonForIssuanceGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="ValidatingAirline" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="FiledFeeInfo" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="BSR_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
 *                           &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
 *                           &amp;lt;attribute name="InConnectionNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
 *                           &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="Start" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="Duration" type="{http://www.opentravel.org/OTA/2003/05}DurationType" /&amp;gt;
 *                           &amp;lt;attribute name="End" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                           &amp;lt;attribute name="InvoluntaryIndCode"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="I"/&amp;gt;
 *                                 &amp;lt;enumeration value="L"/&amp;gt;
 *                                 &amp;lt;enumeration value="S"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="SettlementAuthCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                           &amp;lt;attribute name="Value" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="AssociateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="PromotionalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
 *                           &amp;lt;attribute name="Remark" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                           &amp;lt;attribute name="TaxOnEMD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="AssocFareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="ConsumedAtIssuanceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="DateOfService" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                 &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="Type"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Ticket"/&amp;gt;
 *                       &amp;lt;enumeration value="EMD"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="PrimaryDocInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="InConnectionDocNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="ExchangeTktNbrInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Commission" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="Percent" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="FareComponent" type="{http://www.opentravel.org/OTA/2003/05}FareComponentType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="CarrierFeeInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PaymentDetail" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="CarrierFee" maxOccurs="9" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="FeeAmount" maxOccurs="9"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
 *                                     &amp;lt;attribute name="Amount" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                                     &amp;lt;attribute name="ApplicationCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                           &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
 *                           &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
 *                           &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="RuleCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
 *                           &amp;lt;attribute name="FareClassCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="ReportingCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Taxes" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ExchResidualFareComponent" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareComponentType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Taxes" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TotalAmount" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                           &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OriginalIssueInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Information"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;minLength value="0"/&amp;gt;
 *                       &amp;lt;maxLength value="34"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
 *                 &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ReissuedFlown" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="FlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
 *                 &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *                 &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="TicketDocumentNbr" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                 &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="WaiverCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to19" /&amp;gt;
 *                 &amp;lt;attribute name="TicketDesignatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ResponseComment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="PresentInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="To" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                 &amp;lt;attribute name="At" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ReasonForIssuance" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReasonForIssuanceGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ValidatingAirline" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TaxCouponInformation" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="TicketDocument" maxOccurs="4"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="CouponNumber" maxOccurs="4"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="TaxCouponInfo" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="Cabin" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
 *                                               &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;element name="UnticketedPointInfo" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;attribute name="CityAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                                               &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                               &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                               &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="TicketDocumentNbr" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="JourneyTurnaroundCityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
 *       &amp;lt;attribute name="TotalFltSegQty" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *       &amp;lt;attribute name="SpecificData" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *       &amp;lt;attribute name="TaxOnCommissionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TicketingModeCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
 *       &amp;lt;attribute name="EMD_Type"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="EMD-S"/&amp;gt;
 *             &amp;lt;enumeration value="EMD-A"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="QuoteInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="Operation" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EMD_Type", propOrder = {
    "travelerRefNumber",
    "agentID",
    "paymentDetail",
    "originDestination",
    "custLoyalty",
    "endorsement",
    "addReferenceID",
    "baseFare",
    "equivFare",
    "totalFare",
    "taxes",
    "unstructuredFareCalc",
    "fareInfo",
    "ticketDocument",
    "commission",
    "fareComponent",
    "carrierFeeInfo",
    "exchResidualFareComponent",
    "originalIssueInfo",
    "reissuedFlown",
    "responseComment",
    "presentInfo",
    "reasonForIssuance",
    "validatingAirline",
    "taxCouponInformation"
})
public class EMDType {

    @XmlElement(name = "TravelerRefNumber")
    protected EMDType.TravelerRefNumber travelerRefNumber;
    @XmlElement(name = "AgentID")
    protected List<UniqueIDType> agentID;
    @XmlElement(name = "PaymentDetail")
    protected List<PaymentDetailType> paymentDetail;
    @XmlElement(name = "OriginDestination")
    protected EMDType.OriginDestination originDestination;
    @XmlElement(name = "CustLoyalty")
    protected List<EMDType.CustLoyalty> custLoyalty;
    @XmlElement(name = "Endorsement")
    protected EMDType.Endorsement endorsement;
    @XmlElement(name = "AddReferenceID")
    protected List<UniqueIDType> addReferenceID;
    @XmlElement(name = "BaseFare")
    protected List<EMDType.BaseFare> baseFare;
    @XmlElement(name = "EquivFare")
    protected List<EMDType.EquivFare> equivFare;
    @XmlElement(name = "TotalFare")
    protected List<EMDType.TotalFare> totalFare;
    @XmlElement(name = "Taxes")
    protected EMDType.Taxes taxes;
    @XmlElement(name = "UnstructuredFareCalc")
    protected List<EMDType.UnstructuredFareCalc> unstructuredFareCalc;
    @XmlElement(name = "FareInfo")
    protected EMDType.FareInfo fareInfo;
    @XmlElement(name = "TicketDocument", required = true)
    protected List<EMDType.TicketDocument> ticketDocument;
    @XmlElement(name = "Commission")
    protected EMDType.Commission commission;
    @XmlElement(name = "FareComponent")
    protected FareComponentType fareComponent;
    @XmlElement(name = "CarrierFeeInfo")
    protected EMDType.CarrierFeeInfo carrierFeeInfo;
    @XmlElement(name = "ExchResidualFareComponent")
    protected List<EMDType.ExchResidualFareComponent> exchResidualFareComponent;
    @XmlElement(name = "OriginalIssueInfo")
    protected EMDType.OriginalIssueInfo originalIssueInfo;
    @XmlElement(name = "ReissuedFlown")
    protected List<EMDType.ReissuedFlown> reissuedFlown;
    @XmlElement(name = "ResponseComment")
    protected FreeTextType responseComment;
    @XmlElement(name = "PresentInfo")
    protected EMDType.PresentInfo presentInfo;
    @XmlElement(name = "ReasonForIssuance")
    protected EMDType.ReasonForIssuance reasonForIssuance;
    @XmlElement(name = "ValidatingAirline")
    protected EMDType.ValidatingAirline validatingAirline;
    @XmlElement(name = "TaxCouponInformation")
    protected EMDType.TaxCouponInformation taxCouponInformation;
    @XmlAttribute(name = "TotalFltSegQty", required = true)
    protected int totalFltSegQty;
    @XmlAttribute(name = "SpecificData")
    protected String specificData;
    @XmlAttribute(name = "TaxOnCommissionInd")
    protected Boolean taxOnCommissionInd;
    @XmlAttribute(name = "TicketingModeCode")
    protected String ticketingModeCode;
    @XmlAttribute(name = "EMD_Type")
    protected String emdType;
    @XmlAttribute(name = "QuoteInd")
    protected Boolean quoteInd;
    @XmlAttribute(name = "Operation")
    protected ActionType operation;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantity;

    /**
     * Obtiene el valor de la propiedad travelerRefNumber.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.TravelerRefNumber }
     *     
     */
    public EMDType.TravelerRefNumber getTravelerRefNumber() {
        return travelerRefNumber;
    }

    /**
     * Define el valor de la propiedad travelerRefNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.TravelerRefNumber }
     *     
     */
    public void setTravelerRefNumber(EMDType.TravelerRefNumber value) {
        this.travelerRefNumber = value;
    }

    /**
     * Gets the value of the agentID property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the agentID property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAgentID().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link UniqueIDType }
     * 
     * 
     */
    public List<UniqueIDType> getAgentID() {
        if (agentID == null) {
            agentID = new ArrayList<UniqueIDType>();
        }
        return this.agentID;
    }

    /**
     * Gets the value of the paymentDetail property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentDetail property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPaymentDetail().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentDetailType }
     * 
     * 
     */
    public List<PaymentDetailType> getPaymentDetail() {
        if (paymentDetail == null) {
            paymentDetail = new ArrayList<PaymentDetailType>();
        }
        return this.paymentDetail;
    }

    /**
     * Obtiene el valor de la propiedad originDestination.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.OriginDestination }
     *     
     */
    public EMDType.OriginDestination getOriginDestination() {
        return originDestination;
    }

    /**
     * Define el valor de la propiedad originDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.OriginDestination }
     *     
     */
    public void setOriginDestination(EMDType.OriginDestination value) {
        this.originDestination = value;
    }

    /**
     * Gets the value of the custLoyalty property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the custLoyalty property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCustLoyalty().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.CustLoyalty }
     * 
     * 
     */
    public List<EMDType.CustLoyalty> getCustLoyalty() {
        if (custLoyalty == null) {
            custLoyalty = new ArrayList<EMDType.CustLoyalty>();
        }
        return this.custLoyalty;
    }

    /**
     * Obtiene el valor de la propiedad endorsement.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.Endorsement }
     *     
     */
    public EMDType.Endorsement getEndorsement() {
        return endorsement;
    }

    /**
     * Define el valor de la propiedad endorsement.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.Endorsement }
     *     
     */
    public void setEndorsement(EMDType.Endorsement value) {
        this.endorsement = value;
    }

    /**
     * Gets the value of the addReferenceID property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the addReferenceID property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAddReferenceID().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link UniqueIDType }
     * 
     * 
     */
    public List<UniqueIDType> getAddReferenceID() {
        if (addReferenceID == null) {
            addReferenceID = new ArrayList<UniqueIDType>();
        }
        return this.addReferenceID;
    }

    /**
     * Gets the value of the baseFare property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the baseFare property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBaseFare().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.BaseFare }
     * 
     * 
     */
    public List<EMDType.BaseFare> getBaseFare() {
        if (baseFare == null) {
            baseFare = new ArrayList<EMDType.BaseFare>();
        }
        return this.baseFare;
    }

    /**
     * Gets the value of the equivFare property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the equivFare property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getEquivFare().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.EquivFare }
     * 
     * 
     */
    public List<EMDType.EquivFare> getEquivFare() {
        if (equivFare == null) {
            equivFare = new ArrayList<EMDType.EquivFare>();
        }
        return this.equivFare;
    }

    /**
     * Gets the value of the totalFare property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the totalFare property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTotalFare().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.TotalFare }
     * 
     * 
     */
    public List<EMDType.TotalFare> getTotalFare() {
        if (totalFare == null) {
            totalFare = new ArrayList<EMDType.TotalFare>();
        }
        return this.totalFare;
    }

    /**
     * Obtiene el valor de la propiedad taxes.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.Taxes }
     *     
     */
    public EMDType.Taxes getTaxes() {
        return taxes;
    }

    /**
     * Define el valor de la propiedad taxes.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.Taxes }
     *     
     */
    public void setTaxes(EMDType.Taxes value) {
        this.taxes = value;
    }

    /**
     * Gets the value of the unstructuredFareCalc property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the unstructuredFareCalc property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getUnstructuredFareCalc().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.UnstructuredFareCalc }
     * 
     * 
     */
    public List<EMDType.UnstructuredFareCalc> getUnstructuredFareCalc() {
        if (unstructuredFareCalc == null) {
            unstructuredFareCalc = new ArrayList<EMDType.UnstructuredFareCalc>();
        }
        return this.unstructuredFareCalc;
    }

    /**
     * Obtiene el valor de la propiedad fareInfo.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.FareInfo }
     *     
     */
    public EMDType.FareInfo getFareInfo() {
        return fareInfo;
    }

    /**
     * Define el valor de la propiedad fareInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.FareInfo }
     *     
     */
    public void setFareInfo(EMDType.FareInfo value) {
        this.fareInfo = value;
    }

    /**
     * Gets the value of the ticketDocument property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ticketDocument property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTicketDocument().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.TicketDocument }
     * 
     * 
     */
    public List<EMDType.TicketDocument> getTicketDocument() {
        if (ticketDocument == null) {
            ticketDocument = new ArrayList<EMDType.TicketDocument>();
        }
        return this.ticketDocument;
    }

    /**
     * Obtiene el valor de la propiedad commission.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.Commission }
     *     
     */
    public EMDType.Commission getCommission() {
        return commission;
    }

    /**
     * Define el valor de la propiedad commission.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.Commission }
     *     
     */
    public void setCommission(EMDType.Commission value) {
        this.commission = value;
    }

    /**
     * Obtiene el valor de la propiedad fareComponent.
     * 
     * @return
     *     possible object is
     *     {@link FareComponentType }
     *     
     */
    public FareComponentType getFareComponent() {
        return fareComponent;
    }

    /**
     * Define el valor de la propiedad fareComponent.
     * 
     * @param value
     *     allowed object is
     *     {@link FareComponentType }
     *     
     */
    public void setFareComponent(FareComponentType value) {
        this.fareComponent = value;
    }

    /**
     * Obtiene el valor de la propiedad carrierFeeInfo.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.CarrierFeeInfo }
     *     
     */
    public EMDType.CarrierFeeInfo getCarrierFeeInfo() {
        return carrierFeeInfo;
    }

    /**
     * Define el valor de la propiedad carrierFeeInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.CarrierFeeInfo }
     *     
     */
    public void setCarrierFeeInfo(EMDType.CarrierFeeInfo value) {
        this.carrierFeeInfo = value;
    }

    /**
     * Gets the value of the exchResidualFareComponent property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the exchResidualFareComponent property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getExchResidualFareComponent().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.ExchResidualFareComponent }
     * 
     * 
     */
    public List<EMDType.ExchResidualFareComponent> getExchResidualFareComponent() {
        if (exchResidualFareComponent == null) {
            exchResidualFareComponent = new ArrayList<EMDType.ExchResidualFareComponent>();
        }
        return this.exchResidualFareComponent;
    }

    /**
     * Obtiene el valor de la propiedad originalIssueInfo.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.OriginalIssueInfo }
     *     
     */
    public EMDType.OriginalIssueInfo getOriginalIssueInfo() {
        return originalIssueInfo;
    }

    /**
     * Define el valor de la propiedad originalIssueInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.OriginalIssueInfo }
     *     
     */
    public void setOriginalIssueInfo(EMDType.OriginalIssueInfo value) {
        this.originalIssueInfo = value;
    }

    /**
     * Gets the value of the reissuedFlown property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reissuedFlown property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getReissuedFlown().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link EMDType.ReissuedFlown }
     * 
     * 
     */
    public List<EMDType.ReissuedFlown> getReissuedFlown() {
        if (reissuedFlown == null) {
            reissuedFlown = new ArrayList<EMDType.ReissuedFlown>();
        }
        return this.reissuedFlown;
    }

    /**
     * Obtiene el valor de la propiedad responseComment.
     * 
     * @return
     *     possible object is
     *     {@link FreeTextType }
     *     
     */
    public FreeTextType getResponseComment() {
        return responseComment;
    }

    /**
     * Define el valor de la propiedad responseComment.
     * 
     * @param value
     *     allowed object is
     *     {@link FreeTextType }
     *     
     */
    public void setResponseComment(FreeTextType value) {
        this.responseComment = value;
    }

    /**
     * Obtiene el valor de la propiedad presentInfo.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.PresentInfo }
     *     
     */
    public EMDType.PresentInfo getPresentInfo() {
        return presentInfo;
    }

    /**
     * Define el valor de la propiedad presentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.PresentInfo }
     *     
     */
    public void setPresentInfo(EMDType.PresentInfo value) {
        this.presentInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad reasonForIssuance.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.ReasonForIssuance }
     *     
     */
    public EMDType.ReasonForIssuance getReasonForIssuance() {
        return reasonForIssuance;
    }

    /**
     * Define el valor de la propiedad reasonForIssuance.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.ReasonForIssuance }
     *     
     */
    public void setReasonForIssuance(EMDType.ReasonForIssuance value) {
        this.reasonForIssuance = value;
    }

    /**
     * Obtiene el valor de la propiedad validatingAirline.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.ValidatingAirline }
     *     
     */
    public EMDType.ValidatingAirline getValidatingAirline() {
        return validatingAirline;
    }

    /**
     * Define el valor de la propiedad validatingAirline.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.ValidatingAirline }
     *     
     */
    public void setValidatingAirline(EMDType.ValidatingAirline value) {
        this.validatingAirline = value;
    }

    /**
     * Obtiene el valor de la propiedad taxCouponInformation.
     * 
     * @return
     *     possible object is
     *     {@link EMDType.TaxCouponInformation }
     *     
     */
    public EMDType.TaxCouponInformation getTaxCouponInformation() {
        return taxCouponInformation;
    }

    /**
     * Define el valor de la propiedad taxCouponInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link EMDType.TaxCouponInformation }
     *     
     */
    public void setTaxCouponInformation(EMDType.TaxCouponInformation value) {
        this.taxCouponInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad totalFltSegQty.
     * 
     */
    public int getTotalFltSegQty() {
        return totalFltSegQty;
    }

    /**
     * Define el valor de la propiedad totalFltSegQty.
     * 
     */
    public void setTotalFltSegQty(int value) {
        this.totalFltSegQty = value;
    }

    /**
     * Obtiene el valor de la propiedad specificData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificData() {
        return specificData;
    }

    /**
     * Define el valor de la propiedad specificData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificData(String value) {
        this.specificData = value;
    }

    /**
     * Obtiene el valor de la propiedad taxOnCommissionInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxOnCommissionInd() {
        return taxOnCommissionInd;
    }

    /**
     * Define el valor de la propiedad taxOnCommissionInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxOnCommissionInd(Boolean value) {
        this.taxOnCommissionInd = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingModeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingModeCode() {
        return ticketingModeCode;
    }

    /**
     * Define el valor de la propiedad ticketingModeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingModeCode(String value) {
        this.ticketingModeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad emdType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDType() {
        return emdType;
    }

    /**
     * Define el valor de la propiedad emdType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDType(String value) {
        this.emdType = value;
    }

    /**
     * Obtiene el valor de la propiedad quoteInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isQuoteInd() {
        return quoteInd;
    }

    /**
     * Define el valor de la propiedad quoteInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setQuoteInd(Boolean value) {
        this.quoteInd = value;
    }

    /**
     * Obtiene el valor de la propiedad operation.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getOperation() {
        return operation;
    }

    /**
     * Define el valor de la propiedad operation.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setOperation(ActionType value) {
        this.operation = value;
    }

    /**
     * Obtiene el valor de la propiedad rph.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Define el valor de la propiedad rph.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *       &amp;lt;attribute name="FareAmountType" type="{http://www.opentravel.org/OTA/2003/05}FareAmountType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BaseFare {

        @XmlAttribute(name = "Purpose")
        protected PurposeType purpose;
        @XmlAttribute(name = "FareAmountType")
        protected FareAmountType fareAmountType;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad purpose.
         * 
         * @return
         *     possible object is
         *     {@link PurposeType }
         *     
         */
        public PurposeType getPurpose() {
            return purpose;
        }

        /**
         * Define el valor de la propiedad purpose.
         * 
         * @param value
         *     allowed object is
         *     {@link PurposeType }
         *     
         */
        public void setPurpose(PurposeType value) {
            this.purpose = value;
        }

        /**
         * Obtiene el valor de la propiedad fareAmountType.
         * 
         * @return
         *     possible object is
         *     {@link FareAmountType }
         *     
         */
        public FareAmountType getFareAmountType() {
            return fareAmountType;
        }

        /**
         * Define el valor de la propiedad fareAmountType.
         * 
         * @param value
         *     allowed object is
         *     {@link FareAmountType }
         *     
         */
        public void setFareAmountType(FareAmountType value) {
            this.fareAmountType = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PaymentDetail" type="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="CarrierFee" maxOccurs="9" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="FeeAmount" maxOccurs="9"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
     *                           &amp;lt;attribute name="Amount" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                           &amp;lt;attribute name="ApplicationCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
     *                 &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
     *                 &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="RuleCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
     *                 &amp;lt;attribute name="FareClassCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="ReportingCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Taxes" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paymentDetail",
        "carrierFee",
        "taxes"
    })
    public static class CarrierFeeInfo {

        @XmlElement(name = "PaymentDetail")
        protected PaymentDetailType paymentDetail;
        @XmlElement(name = "CarrierFee")
        protected List<EMDType.CarrierFeeInfo.CarrierFee> carrierFee;
        @XmlElement(name = "Taxes")
        protected EMDType.CarrierFeeInfo.Taxes taxes;

        /**
         * Obtiene el valor de la propiedad paymentDetail.
         * 
         * @return
         *     possible object is
         *     {@link PaymentDetailType }
         *     
         */
        public PaymentDetailType getPaymentDetail() {
            return paymentDetail;
        }

        /**
         * Define el valor de la propiedad paymentDetail.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentDetailType }
         *     
         */
        public void setPaymentDetail(PaymentDetailType value) {
            this.paymentDetail = value;
        }

        /**
         * Gets the value of the carrierFee property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the carrierFee property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCarrierFee().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link EMDType.CarrierFeeInfo.CarrierFee }
         * 
         * 
         */
        public List<EMDType.CarrierFeeInfo.CarrierFee> getCarrierFee() {
            if (carrierFee == null) {
                carrierFee = new ArrayList<EMDType.CarrierFeeInfo.CarrierFee>();
            }
            return this.carrierFee;
        }

        /**
         * Obtiene el valor de la propiedad taxes.
         * 
         * @return
         *     possible object is
         *     {@link EMDType.CarrierFeeInfo.Taxes }
         *     
         */
        public EMDType.CarrierFeeInfo.Taxes getTaxes() {
            return taxes;
        }

        /**
         * Define el valor de la propiedad taxes.
         * 
         * @param value
         *     allowed object is
         *     {@link EMDType.CarrierFeeInfo.Taxes }
         *     
         */
        public void setTaxes(EMDType.CarrierFeeInfo.Taxes value) {
            this.taxes = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="FeeAmount" maxOccurs="9"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
         *                 &amp;lt;attribute name="Amount" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *                 &amp;lt;attribute name="ApplicationCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
         *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to4" /&amp;gt;
         *       &amp;lt;attribute name="TariffNumber" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to3" /&amp;gt;
         *       &amp;lt;attribute name="RuleNumber" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="RuleCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
         *       &amp;lt;attribute name="FareClassCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="ReportingCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "feeAmount"
        })
        public static class CarrierFee {

            @XmlElement(name = "FeeAmount", required = true)
            protected List<EMDType.CarrierFeeInfo.CarrierFee.FeeAmount> feeAmount;
            @XmlAttribute(name = "Type", required = true)
            protected String type;
            @XmlAttribute(name = "Number")
            protected Integer number;
            @XmlAttribute(name = "TariffNumber")
            protected String tariffNumber;
            @XmlAttribute(name = "RuleNumber")
            protected String ruleNumber;
            @XmlAttribute(name = "RuleCode")
            protected String ruleCode;
            @XmlAttribute(name = "FareClassCode")
            protected String fareClassCode;
            @XmlAttribute(name = "ReportingCode")
            protected String reportingCode;
            @XmlAttribute(name = "CompanyShortName")
            protected String companyShortName;
            @XmlAttribute(name = "TravelSector")
            protected String travelSector;
            @XmlAttribute(name = "Code")
            protected String code;
            @XmlAttribute(name = "CodeContext")
            protected String codeContext;
            @XmlAttribute(name = "CountryCode")
            protected String countryCode;

            /**
             * Gets the value of the feeAmount property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the feeAmount property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getFeeAmount().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link EMDType.CarrierFeeInfo.CarrierFee.FeeAmount }
             * 
             * 
             */
            public List<EMDType.CarrierFeeInfo.CarrierFee.FeeAmount> getFeeAmount() {
                if (feeAmount == null) {
                    feeAmount = new ArrayList<EMDType.CarrierFeeInfo.CarrierFee.FeeAmount>();
                }
                return this.feeAmount;
            }

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad number.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getNumber() {
                return number;
            }

            /**
             * Define el valor de la propiedad number.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setNumber(Integer value) {
                this.number = value;
            }

            /**
             * Obtiene el valor de la propiedad tariffNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTariffNumber() {
                return tariffNumber;
            }

            /**
             * Define el valor de la propiedad tariffNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTariffNumber(String value) {
                this.tariffNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad ruleNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleNumber() {
                return ruleNumber;
            }

            /**
             * Define el valor de la propiedad ruleNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleNumber(String value) {
                this.ruleNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad ruleCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRuleCode() {
                return ruleCode;
            }

            /**
             * Define el valor de la propiedad ruleCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRuleCode(String value) {
                this.ruleCode = value;
            }

            /**
             * Obtiene el valor de la propiedad fareClassCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareClassCode() {
                return fareClassCode;
            }

            /**
             * Define el valor de la propiedad fareClassCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareClassCode(String value) {
                this.fareClassCode = value;
            }

            /**
             * Obtiene el valor de la propiedad reportingCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReportingCode() {
                return reportingCode;
            }

            /**
             * Define el valor de la propiedad reportingCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReportingCode(String value) {
                this.reportingCode = value;
            }

            /**
             * Obtiene el valor de la propiedad companyShortName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyShortName() {
                return companyShortName;
            }

            /**
             * Define el valor de la propiedad companyShortName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyShortName(String value) {
                this.companyShortName = value;
            }

            /**
             * Obtiene el valor de la propiedad travelSector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelSector() {
                return travelSector;
            }

            /**
             * Define el valor de la propiedad travelSector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelSector(String value) {
                this.travelSector = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad codeContext.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeContext() {
                return codeContext;
            }

            /**
             * Define el valor de la propiedad codeContext.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeContext(String value) {
                this.codeContext = value;
            }

            /**
             * Obtiene el valor de la propiedad countryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Define el valor de la propiedad countryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
             *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to3" /&amp;gt;
             *       &amp;lt;attribute name="Amount" use="required" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
             *       &amp;lt;attribute name="ApplicationCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class FeeAmount {

                @XmlAttribute(name = "Type", required = true)
                protected String type;
                @XmlAttribute(name = "Amount", required = true)
                protected BigDecimal amount;
                @XmlAttribute(name = "ApplicationCode", required = true)
                protected List<String> applicationCode;
                @XmlAttribute(name = "OriginCityCode")
                protected String originCityCode;
                @XmlAttribute(name = "OriginCodeContext")
                protected String originCodeContext;
                @XmlAttribute(name = "DestinationCityCode")
                protected String destinationCityCode;
                @XmlAttribute(name = "DestinationCodeContext")
                protected String destinationCodeContext;

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

                /**
                 * Gets the value of the applicationCode property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the applicationCode property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getApplicationCode().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getApplicationCode() {
                    if (applicationCode == null) {
                        applicationCode = new ArrayList<String>();
                    }
                    return this.applicationCode;
                }

                /**
                 * Obtiene el valor de la propiedad originCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginCityCode() {
                    return originCityCode;
                }

                /**
                 * Define el valor de la propiedad originCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginCityCode(String value) {
                    this.originCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad originCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOriginCodeContext() {
                    return originCodeContext;
                }

                /**
                 * Define el valor de la propiedad originCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOriginCodeContext(String value) {
                    this.originCodeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationCityCode() {
                    return destinationCityCode;
                }

                /**
                 * Define el valor de la propiedad destinationCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationCityCode(String value) {
                    this.destinationCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationCodeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestinationCodeContext() {
                    return destinationCodeContext;
                }

                /**
                 * Define el valor de la propiedad destinationCodeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestinationCodeContext(String value) {
                    this.destinationCodeContext = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tax"
        })
        public static class Taxes {

            @XmlElement(name = "Tax")
            protected List<AirTaxType> tax;

            /**
             * Gets the value of the tax property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the tax property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTax().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AirTaxType }
             * 
             * 
             */
            public List<AirTaxType> getTax() {
                if (tax == null) {
                    tax = new ArrayList<AirTaxType>();
                }
                return this.tax;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="Percent" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Commission {

        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "Percent")
        protected BigDecimal percent;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad percent.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPercent() {
            return percent;
        }

        /**
         * Define el valor de la propiedad percent.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPercent(BigDecimal value) {
            this.percent = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CustLoyalty {

        @XmlAttribute(name = "ShareSynchInd")
        protected String shareSynchInd;
        @XmlAttribute(name = "ShareMarketInd")
        protected String shareMarketInd;
        @XmlAttribute(name = "ProgramID")
        protected String programID;
        @XmlAttribute(name = "MembershipID")
        protected String membershipID;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "VendorCode")
        protected List<String> vendorCode;
        @XmlAttribute(name = "PrimaryLoyaltyIndicator")
        protected Boolean primaryLoyaltyIndicator;
        @XmlAttribute(name = "AllianceLoyaltyLevelName")
        protected String allianceLoyaltyLevelName;
        @XmlAttribute(name = "CustomerType")
        protected String customerType;
        @XmlAttribute(name = "CustomerValue")
        protected String customerValue;
        @XmlAttribute(name = "Password")
        protected String password;
        @XmlAttribute(name = "LoyalLevel")
        protected String loyalLevel;
        @XmlAttribute(name = "LoyalLevelCode")
        protected Integer loyalLevelCode;
        @XmlAttribute(name = "SingleVendorInd")
        protected String singleVendorInd;
        @XmlAttribute(name = "SignupDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar signupDate;
        @XmlAttribute(name = "EffectiveDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar effectiveDate;
        @XmlAttribute(name = "ExpireDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar expireDate;
        @XmlAttribute(name = "ExpireDateExclusiveIndicator")
        protected Boolean expireDateExclusiveIndicator;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Obtiene el valor de la propiedad shareSynchInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShareSynchInd() {
            return shareSynchInd;
        }

        /**
         * Define el valor de la propiedad shareSynchInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShareSynchInd(String value) {
            this.shareSynchInd = value;
        }

        /**
         * Obtiene el valor de la propiedad shareMarketInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShareMarketInd() {
            return shareMarketInd;
        }

        /**
         * Define el valor de la propiedad shareMarketInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShareMarketInd(String value) {
            this.shareMarketInd = value;
        }

        /**
         * Obtiene el valor de la propiedad programID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProgramID() {
            return programID;
        }

        /**
         * Define el valor de la propiedad programID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProgramID(String value) {
            this.programID = value;
        }

        /**
         * Obtiene el valor de la propiedad membershipID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMembershipID() {
            return membershipID;
        }

        /**
         * Define el valor de la propiedad membershipID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMembershipID(String value) {
            this.membershipID = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Gets the value of the vendorCode property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVendorCode().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getVendorCode() {
            if (vendorCode == null) {
                vendorCode = new ArrayList<String>();
            }
            return this.vendorCode;
        }

        /**
         * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPrimaryLoyaltyIndicator() {
            return primaryLoyaltyIndicator;
        }

        /**
         * Define el valor de la propiedad primaryLoyaltyIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPrimaryLoyaltyIndicator(Boolean value) {
            this.primaryLoyaltyIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAllianceLoyaltyLevelName() {
            return allianceLoyaltyLevelName;
        }

        /**
         * Define el valor de la propiedad allianceLoyaltyLevelName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAllianceLoyaltyLevelName(String value) {
            this.allianceLoyaltyLevelName = value;
        }

        /**
         * Obtiene el valor de la propiedad customerType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerType() {
            return customerType;
        }

        /**
         * Define el valor de la propiedad customerType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerType(String value) {
            this.customerType = value;
        }

        /**
         * Obtiene el valor de la propiedad customerValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerValue() {
            return customerValue;
        }

        /**
         * Define el valor de la propiedad customerValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerValue(String value) {
            this.customerValue = value;
        }

        /**
         * Obtiene el valor de la propiedad password.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Define el valor de la propiedad password.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Obtiene el valor de la propiedad loyalLevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyalLevel() {
            return loyalLevel;
        }

        /**
         * Define el valor de la propiedad loyalLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyalLevel(String value) {
            this.loyalLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad loyalLevelCode.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getLoyalLevelCode() {
            return loyalLevelCode;
        }

        /**
         * Define el valor de la propiedad loyalLevelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setLoyalLevelCode(Integer value) {
            this.loyalLevelCode = value;
        }

        /**
         * Obtiene el valor de la propiedad singleVendorInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSingleVendorInd() {
            return singleVendorInd;
        }

        /**
         * Define el valor de la propiedad singleVendorInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSingleVendorInd(String value) {
            this.singleVendorInd = value;
        }

        /**
         * Obtiene el valor de la propiedad signupDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSignupDate() {
            return signupDate;
        }

        /**
         * Define el valor de la propiedad signupDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSignupDate(XMLGregorianCalendar value) {
            this.signupDate = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Define el valor de la propiedad effectiveDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEffectiveDate(XMLGregorianCalendar value) {
            this.effectiveDate = value;
        }

        /**
         * Obtiene el valor de la propiedad expireDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpireDate() {
            return expireDate;
        }

        /**
         * Define el valor de la propiedad expireDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpireDate(XMLGregorianCalendar value) {
            this.expireDate = value;
        }

        /**
         * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExpireDateExclusiveIndicator() {
            return expireDateExclusiveIndicator;
        }

        /**
         * Define el valor de la propiedad expireDateExclusiveIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExpireDateExclusiveIndicator(Boolean value) {
            this.expireDateExclusiveIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Info" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Endorsement {

        @XmlAttribute(name = "Info")
        protected String info;

        /**
         * Obtiene el valor de la propiedad info.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfo() {
            return info;
        }

        /**
         * Define el valor de la propiedad info.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfo(String value) {
            this.info = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *       &amp;lt;attribute name="FareAmountType" type="{http://www.opentravel.org/OTA/2003/05}FareAmountType" /&amp;gt;
     *       &amp;lt;attribute name="BankExchangeRate" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EquivFare {

        @XmlAttribute(name = "Purpose")
        protected PurposeType purpose;
        @XmlAttribute(name = "FareAmountType")
        protected FareAmountType fareAmountType;
        @XmlAttribute(name = "BankExchangeRate")
        protected BigDecimal bankExchangeRate;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad purpose.
         * 
         * @return
         *     possible object is
         *     {@link PurposeType }
         *     
         */
        public PurposeType getPurpose() {
            return purpose;
        }

        /**
         * Define el valor de la propiedad purpose.
         * 
         * @param value
         *     allowed object is
         *     {@link PurposeType }
         *     
         */
        public void setPurpose(PurposeType value) {
            this.purpose = value;
        }

        /**
         * Obtiene el valor de la propiedad fareAmountType.
         * 
         * @return
         *     possible object is
         *     {@link FareAmountType }
         *     
         */
        public FareAmountType getFareAmountType() {
            return fareAmountType;
        }

        /**
         * Define el valor de la propiedad fareAmountType.
         * 
         * @param value
         *     allowed object is
         *     {@link FareAmountType }
         *     
         */
        public void setFareAmountType(FareAmountType value) {
            this.fareAmountType = value;
        }

        /**
         * Obtiene el valor de la propiedad bankExchangeRate.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getBankExchangeRate() {
            return bankExchangeRate;
        }

        /**
         * Define el valor de la propiedad bankExchangeRate.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setBankExchangeRate(BigDecimal value) {
            this.bankExchangeRate = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FareComponentType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Taxes" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TotalAmount" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                 &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxes",
        "totalAmount"
    })
    public static class ExchResidualFareComponent
        extends FareComponentType
    {

        @XmlElement(name = "Taxes")
        protected EMDType.ExchResidualFareComponent.Taxes taxes;
        @XmlElement(name = "TotalAmount")
        protected EMDType.ExchResidualFareComponent.TotalAmount totalAmount;

        /**
         * Obtiene el valor de la propiedad taxes.
         * 
         * @return
         *     possible object is
         *     {@link EMDType.ExchResidualFareComponent.Taxes }
         *     
         */
        public EMDType.ExchResidualFareComponent.Taxes getTaxes() {
            return taxes;
        }

        /**
         * Define el valor de la propiedad taxes.
         * 
         * @param value
         *     allowed object is
         *     {@link EMDType.ExchResidualFareComponent.Taxes }
         *     
         */
        public void setTaxes(EMDType.ExchResidualFareComponent.Taxes value) {
            this.taxes = value;
        }

        /**
         * Obtiene el valor de la propiedad totalAmount.
         * 
         * @return
         *     possible object is
         *     {@link EMDType.ExchResidualFareComponent.TotalAmount }
         *     
         */
        public EMDType.ExchResidualFareComponent.TotalAmount getTotalAmount() {
            return totalAmount;
        }

        /**
         * Define el valor de la propiedad totalAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link EMDType.ExchResidualFareComponent.TotalAmount }
         *     
         */
        public void setTotalAmount(EMDType.ExchResidualFareComponent.TotalAmount value) {
            this.totalAmount = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tax"
        })
        public static class Taxes {

            @XmlElement(name = "Tax", required = true)
            protected List<AirTaxType> tax;

            /**
             * Gets the value of the tax property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the tax property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTax().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link AirTaxType }
             * 
             * 
             */
            public List<AirTaxType> getTax() {
                if (tax == null) {
                    tax = new ArrayList<AirTaxType>();
                }
                return this.tax;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *       &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class TotalAmount {

            @XmlAttribute(name = "Purpose")
            protected PurposeType purpose;
            @XmlAttribute(name = "CurrencyCode")
            protected String currencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;
            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;

            /**
             * Obtiene el valor de la propiedad purpose.
             * 
             * @return
             *     possible object is
             *     {@link PurposeType }
             *     
             */
            public PurposeType getPurpose() {
                return purpose;
            }

            /**
             * Define el valor de la propiedad purpose.
             * 
             * @param value
             *     allowed object is
             *     {@link PurposeType }
             *     
             */
            public void setPurpose(PurposeType value) {
                this.purpose = value;
            }

            /**
             * Obtiene el valor de la propiedad currencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Define el valor de la propiedad currencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ET_FareInfo"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PenaltyAmount" type="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType" maxOccurs="3" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *       &amp;lt;attribute name="NonEndorsableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NonRefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PenaltyRestrictionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="PresentCreditCardInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="AroundTheWorldFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NonInterlineableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NonCommissionableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="NonReissuableNonExchgInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "penaltyAmount"
    })
    public static class FareInfo
        extends ETFareInfo
    {

        @XmlElement(name = "PenaltyAmount")
        protected List<VoluntaryChangesType> penaltyAmount;
        @XmlAttribute(name = "NonEndorsableInd")
        protected Boolean nonEndorsableInd;
        @XmlAttribute(name = "NonRefundableInd")
        protected Boolean nonRefundableInd;
        @XmlAttribute(name = "PenaltyRestrictionInd")
        protected Boolean penaltyRestrictionInd;
        @XmlAttribute(name = "PresentCreditCardInd")
        protected Boolean presentCreditCardInd;
        @XmlAttribute(name = "AroundTheWorldFareInd")
        protected Boolean aroundTheWorldFareInd;
        @XmlAttribute(name = "NonInterlineableInd")
        protected Boolean nonInterlineableInd;
        @XmlAttribute(name = "NonCommissionableInd")
        protected Boolean nonCommissionableInd;
        @XmlAttribute(name = "NonReissuableNonExchgInd")
        protected Boolean nonReissuableNonExchgInd;
        @XmlAttribute(name = "CompanyShortName")
        protected String companyShortName;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;

        /**
         * Gets the value of the penaltyAmount property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the penaltyAmount property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPenaltyAmount().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VoluntaryChangesType }
         * 
         * 
         */
        public List<VoluntaryChangesType> getPenaltyAmount() {
            if (penaltyAmount == null) {
                penaltyAmount = new ArrayList<VoluntaryChangesType>();
            }
            return this.penaltyAmount;
        }

        /**
         * Obtiene el valor de la propiedad nonEndorsableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonEndorsableInd() {
            return nonEndorsableInd;
        }

        /**
         * Define el valor de la propiedad nonEndorsableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonEndorsableInd(Boolean value) {
            this.nonEndorsableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonRefundableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonRefundableInd() {
            return nonRefundableInd;
        }

        /**
         * Define el valor de la propiedad nonRefundableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonRefundableInd(Boolean value) {
            this.nonRefundableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad penaltyRestrictionInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPenaltyRestrictionInd() {
            return penaltyRestrictionInd;
        }

        /**
         * Define el valor de la propiedad penaltyRestrictionInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPenaltyRestrictionInd(Boolean value) {
            this.penaltyRestrictionInd = value;
        }

        /**
         * Obtiene el valor de la propiedad presentCreditCardInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPresentCreditCardInd() {
            return presentCreditCardInd;
        }

        /**
         * Define el valor de la propiedad presentCreditCardInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPresentCreditCardInd(Boolean value) {
            this.presentCreditCardInd = value;
        }

        /**
         * Obtiene el valor de la propiedad aroundTheWorldFareInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAroundTheWorldFareInd() {
            return aroundTheWorldFareInd;
        }

        /**
         * Define el valor de la propiedad aroundTheWorldFareInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAroundTheWorldFareInd(Boolean value) {
            this.aroundTheWorldFareInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonInterlineableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonInterlineableInd() {
            return nonInterlineableInd;
        }

        /**
         * Define el valor de la propiedad nonInterlineableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonInterlineableInd(Boolean value) {
            this.nonInterlineableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonCommissionableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonCommissionableInd() {
            return nonCommissionableInd;
        }

        /**
         * Define el valor de la propiedad nonCommissionableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonCommissionableInd(Boolean value) {
            this.nonCommissionableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad nonReissuableNonExchgInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonReissuableNonExchgInd() {
            return nonReissuableNonExchgInd;
        }

        /**
         * Define el valor de la propiedad nonReissuableNonExchgInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonReissuableNonExchgInd(Boolean value) {
            this.nonReissuableNonExchgInd = value;
        }

        /**
         * Obtiene el valor de la propiedad companyShortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyShortName() {
            return companyShortName;
        }

        /**
         * Define el valor de la propiedad companyShortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyShortName(String value) {
            this.companyShortName = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad countryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * Define el valor de la propiedad countryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OriginDestinationGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OriginDestination {

        @XmlAttribute(name = "OriginCityCode")
        protected String originCityCode;
        @XmlAttribute(name = "OriginCodeContext")
        protected String originCodeContext;
        @XmlAttribute(name = "DestinationCityCode")
        protected String destinationCityCode;
        @XmlAttribute(name = "DestinationCodeContext")
        protected String destinationCodeContext;

        /**
         * Obtiene el valor de la propiedad originCityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginCityCode() {
            return originCityCode;
        }

        /**
         * Define el valor de la propiedad originCityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginCityCode(String value) {
            this.originCityCode = value;
        }

        /**
         * Obtiene el valor de la propiedad originCodeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginCodeContext() {
            return originCodeContext;
        }

        /**
         * Define el valor de la propiedad originCodeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginCodeContext(String value) {
            this.originCodeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationCityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationCityCode() {
            return destinationCityCode;
        }

        /**
         * Define el valor de la propiedad destinationCityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationCityCode(String value) {
            this.destinationCityCode = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationCodeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationCodeContext() {
            return destinationCodeContext;
        }

        /**
         * Define el valor de la propiedad destinationCodeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationCodeContext(String value) {
            this.destinationCodeContext = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Information"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;minLength value="0"/&amp;gt;
     *             &amp;lt;maxLength value="34"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="IssuingAgentID" type="{http://www.opentravel.org/OTA/2003/05}NumericStringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="LocationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OriginalIssueInfo {

        @XmlAttribute(name = "Information")
        protected String information;
        @XmlAttribute(name = "TicketDocumentNbr")
        protected String ticketDocumentNbr;
        @XmlAttribute(name = "IssuingAgentID")
        protected String issuingAgentID;
        @XmlAttribute(name = "DateOfIssue")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateOfIssue;
        @XmlAttribute(name = "LocationCode")
        protected String locationCode;

        /**
         * Obtiene el valor de la propiedad information.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInformation() {
            return information;
        }

        /**
         * Define el valor de la propiedad information.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInformation(String value) {
            this.information = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketDocumentNbr.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketDocumentNbr() {
            return ticketDocumentNbr;
        }

        /**
         * Define el valor de la propiedad ticketDocumentNbr.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketDocumentNbr(String value) {
            this.ticketDocumentNbr = value;
        }

        /**
         * Obtiene el valor de la propiedad issuingAgentID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssuingAgentID() {
            return issuingAgentID;
        }

        /**
         * Define el valor de la propiedad issuingAgentID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssuingAgentID(String value) {
            this.issuingAgentID = value;
        }

        /**
         * Obtiene el valor de la propiedad dateOfIssue.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateOfIssue() {
            return dateOfIssue;
        }

        /**
         * Define el valor de la propiedad dateOfIssue.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateOfIssue(XMLGregorianCalendar value) {
            this.dateOfIssue = value;
        }

        /**
         * Obtiene el valor de la propiedad locationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocationCode() {
            return locationCode;
        }

        /**
         * Define el valor de la propiedad locationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocationCode(String value) {
            this.locationCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="To" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *       &amp;lt;attribute name="At" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PresentInfo {

        @XmlAttribute(name = "To")
        protected String to;
        @XmlAttribute(name = "At")
        protected String at;

        /**
         * Obtiene el valor de la propiedad to.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTo() {
            return to;
        }

        /**
         * Define el valor de la propiedad to.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTo(String value) {
            this.to = value;
        }

        /**
         * Obtiene el valor de la propiedad at.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAt() {
            return at;
        }

        /**
         * Define el valor de la propiedad at.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAt(String value) {
            this.at = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReasonForIssuanceGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ReasonForIssuance {

        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "SubCode")
        protected String subCode;
        @XmlAttribute(name = "Description")
        protected String description;

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad subCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubCode() {
            return subCode;
        }

        /**
         * Define el valor de la propiedad subCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubCode(String value) {
            this.subCode = value;
        }

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
     *       &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="TicketDocumentNbr" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="WaiverCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to19" /&amp;gt;
     *       &amp;lt;attribute name="TicketDesignatorCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightSegmentRPH"
    })
    public static class ReissuedFlown {

        @XmlElement(name = "FlightSegmentRPH")
        protected String flightSegmentRPH;
        @XmlAttribute(name = "Number", required = true)
        protected int number;
        @XmlAttribute(name = "CouponItinerarySeqNbr")
        protected Integer couponItinerarySeqNbr;
        @XmlAttribute(name = "FareBasisCode")
        protected String fareBasisCode;
        @XmlAttribute(name = "TicketDocumentNbr", required = true)
        protected String ticketDocumentNbr;
        @XmlAttribute(name = "DateOfIssue")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateOfIssue;
        @XmlAttribute(name = "WaiverCode")
        protected String waiverCode;
        @XmlAttribute(name = "TicketDesignatorCode")
        protected String ticketDesignatorCode;

        /**
         * Obtiene el valor de la propiedad flightSegmentRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightSegmentRPH() {
            return flightSegmentRPH;
        }

        /**
         * Define el valor de la propiedad flightSegmentRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightSegmentRPH(String value) {
            this.flightSegmentRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad number.
         * 
         */
        public int getNumber() {
            return number;
        }

        /**
         * Define el valor de la propiedad number.
         * 
         */
        public void setNumber(int value) {
            this.number = value;
        }

        /**
         * Obtiene el valor de la propiedad couponItinerarySeqNbr.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCouponItinerarySeqNbr() {
            return couponItinerarySeqNbr;
        }

        /**
         * Define el valor de la propiedad couponItinerarySeqNbr.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCouponItinerarySeqNbr(Integer value) {
            this.couponItinerarySeqNbr = value;
        }

        /**
         * Obtiene el valor de la propiedad fareBasisCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareBasisCode() {
            return fareBasisCode;
        }

        /**
         * Define el valor de la propiedad fareBasisCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareBasisCode(String value) {
            this.fareBasisCode = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketDocumentNbr.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketDocumentNbr() {
            return ticketDocumentNbr;
        }

        /**
         * Define el valor de la propiedad ticketDocumentNbr.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketDocumentNbr(String value) {
            this.ticketDocumentNbr = value;
        }

        /**
         * Obtiene el valor de la propiedad dateOfIssue.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateOfIssue() {
            return dateOfIssue;
        }

        /**
         * Define el valor de la propiedad dateOfIssue.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateOfIssue(XMLGregorianCalendar value) {
            this.dateOfIssue = value;
        }

        /**
         * Obtiene el valor de la propiedad waiverCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWaiverCode() {
            return waiverCode;
        }

        /**
         * Define el valor de la propiedad waiverCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWaiverCode(String value) {
            this.waiverCode = value;
        }

        /**
         * Obtiene el valor de la propiedad ticketDesignatorCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketDesignatorCode() {
            return ticketDesignatorCode;
        }

        /**
         * Define el valor de la propiedad ticketDesignatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketDesignatorCode(String value) {
            this.ticketDesignatorCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="TicketDocument" maxOccurs="4"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="CouponNumber" maxOccurs="4"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="TaxCouponInfo" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="Cabin" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
     *                                     &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="UnticketedPointInfo" maxOccurs="5" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="CityAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                                     &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                                     &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                                     &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="TicketDocumentNbr" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="JourneyTurnaroundCityCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ticketDocument"
    })
    public static class TaxCouponInformation {

        @XmlElement(name = "TicketDocument", required = true)
        protected List<EMDType.TaxCouponInformation.TicketDocument> ticketDocument;
        @XmlAttribute(name = "BirthDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar birthDate;
        @XmlAttribute(name = "JourneyTurnaroundCityCode")
        protected String journeyTurnaroundCityCode;

        /**
         * Gets the value of the ticketDocument property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ticketDocument property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTicketDocument().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link EMDType.TaxCouponInformation.TicketDocument }
         * 
         * 
         */
        public List<EMDType.TaxCouponInformation.TicketDocument> getTicketDocument() {
            if (ticketDocument == null) {
                ticketDocument = new ArrayList<EMDType.TaxCouponInformation.TicketDocument>();
            }
            return this.ticketDocument;
        }

        /**
         * Obtiene el valor de la propiedad birthDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getBirthDate() {
            return birthDate;
        }

        /**
         * Define el valor de la propiedad birthDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setBirthDate(XMLGregorianCalendar value) {
            this.birthDate = value;
        }

        /**
         * Obtiene el valor de la propiedad journeyTurnaroundCityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJourneyTurnaroundCityCode() {
            return journeyTurnaroundCityCode;
        }

        /**
         * Define el valor de la propiedad journeyTurnaroundCityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJourneyTurnaroundCityCode(String value) {
            this.journeyTurnaroundCityCode = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="CouponNumber" maxOccurs="4"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="TaxCouponInfo" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="Cabin" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
         *                           &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="UnticketedPointInfo" maxOccurs="5" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="CityAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *                           &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                           &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                           &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="TicketDocumentNbr" use="required" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "couponNumber"
        })
        public static class TicketDocument {

            @XmlElement(name = "CouponNumber", required = true)
            protected List<EMDType.TaxCouponInformation.TicketDocument.CouponNumber> couponNumber;
            @XmlAttribute(name = "TicketDocumentNbr", required = true)
            protected String ticketDocumentNbr;

            /**
             * Gets the value of the couponNumber property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the couponNumber property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getCouponNumber().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link EMDType.TaxCouponInformation.TicketDocument.CouponNumber }
             * 
             * 
             */
            public List<EMDType.TaxCouponInformation.TicketDocument.CouponNumber> getCouponNumber() {
                if (couponNumber == null) {
                    couponNumber = new ArrayList<EMDType.TaxCouponInformation.TicketDocument.CouponNumber>();
                }
                return this.couponNumber;
            }

            /**
             * Obtiene el valor de la propiedad ticketDocumentNbr.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTicketDocumentNbr() {
                return ticketDocumentNbr;
            }

            /**
             * Define el valor de la propiedad ticketDocumentNbr.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTicketDocumentNbr(String value) {
                this.ticketDocumentNbr = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="TaxCouponInfo" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="Cabin" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
             *                 &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Tax" type="{http://www.opentravel.org/OTA/2003/05}AirTaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="UnticketedPointInfo" maxOccurs="5" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="CityAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
             *                 &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *                 &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *                 &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="Number" use="required" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "taxCouponInfo",
                "tax",
                "unticketedPointInfo"
            })
            public static class CouponNumber {

                @XmlElement(name = "TaxCouponInfo")
                protected EMDType.TaxCouponInformation.TicketDocument.CouponNumber.TaxCouponInfo taxCouponInfo;
                @XmlElement(name = "Tax")
                protected List<AirTaxType> tax;
                @XmlElement(name = "UnticketedPointInfo")
                protected List<EMDType.TaxCouponInformation.TicketDocument.CouponNumber.UnticketedPointInfo> unticketedPointInfo;
                @XmlAttribute(name = "Number", required = true)
                protected int number;

                /**
                 * Obtiene el valor de la propiedad taxCouponInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link EMDType.TaxCouponInformation.TicketDocument.CouponNumber.TaxCouponInfo }
                 *     
                 */
                public EMDType.TaxCouponInformation.TicketDocument.CouponNumber.TaxCouponInfo getTaxCouponInfo() {
                    return taxCouponInfo;
                }

                /**
                 * Define el valor de la propiedad taxCouponInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EMDType.TaxCouponInformation.TicketDocument.CouponNumber.TaxCouponInfo }
                 *     
                 */
                public void setTaxCouponInfo(EMDType.TaxCouponInformation.TicketDocument.CouponNumber.TaxCouponInfo value) {
                    this.taxCouponInfo = value;
                }

                /**
                 * Gets the value of the tax property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the tax property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getTax().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link AirTaxType }
                 * 
                 * 
                 */
                public List<AirTaxType> getTax() {
                    if (tax == null) {
                        tax = new ArrayList<AirTaxType>();
                    }
                    return this.tax;
                }

                /**
                 * Gets the value of the unticketedPointInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the unticketedPointInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getUnticketedPointInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link EMDType.TaxCouponInformation.TicketDocument.CouponNumber.UnticketedPointInfo }
                 * 
                 * 
                 */
                public List<EMDType.TaxCouponInformation.TicketDocument.CouponNumber.UnticketedPointInfo> getUnticketedPointInfo() {
                    if (unticketedPointInfo == null) {
                        unticketedPointInfo = new ArrayList<EMDType.TaxCouponInformation.TicketDocument.CouponNumber.UnticketedPointInfo>();
                    }
                    return this.unticketedPointInfo;
                }

                /**
                 * Obtiene el valor de la propiedad number.
                 * 
                 */
                public int getNumber() {
                    return number;
                }

                /**
                 * Define el valor de la propiedad number.
                 * 
                 */
                public void setNumber(int value) {
                    this.number = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="Cabin" type="{http://www.opentravel.org/OTA/2003/05}CabinType" /&amp;gt;
                 *       &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class TaxCouponInfo {

                    @XmlAttribute(name = "Cabin")
                    protected String cabin;
                    @XmlAttribute(name = "AirEquipType")
                    protected String airEquipType;

                    /**
                     * Obtiene el valor de la propiedad cabin.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCabin() {
                        return cabin;
                    }

                    /**
                     * Define el valor de la propiedad cabin.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCabin(String value) {
                        this.cabin = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad airEquipType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAirEquipType() {
                        return airEquipType;
                    }

                    /**
                     * Define el valor de la propiedad airEquipType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAirEquipType(String value) {
                        this.airEquipType = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="CityAirportCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
                 *       &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
                 *       &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
                 *       &amp;lt;attribute name="AirEquipType" type="{http://www.opentravel.org/OTA/2003/05}StringLength3" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class UnticketedPointInfo {

                    @XmlAttribute(name = "CityAirportCode")
                    protected String cityAirportCode;
                    @XmlAttribute(name = "ArrivalDate")
                    protected String arrivalDate;
                    @XmlAttribute(name = "DepartureDate")
                    protected String departureDate;
                    @XmlAttribute(name = "AirEquipType")
                    protected String airEquipType;

                    /**
                     * Obtiene el valor de la propiedad cityAirportCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCityAirportCode() {
                        return cityAirportCode;
                    }

                    /**
                     * Define el valor de la propiedad cityAirportCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCityAirportCode(String value) {
                        this.cityAirportCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad arrivalDate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getArrivalDate() {
                        return arrivalDate;
                    }

                    /**
                     * Define el valor de la propiedad arrivalDate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setArrivalDate(String value) {
                        this.arrivalDate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad departureDate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDepartureDate() {
                        return departureDate;
                    }

                    /**
                     * Define el valor de la propiedad departureDate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDepartureDate(String value) {
                        this.departureDate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad airEquipType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAirEquipType() {
                        return airEquipType;
                    }

                    /**
                     * Define el valor de la propiedad airEquipType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAirEquipType(String value) {
                        this.airEquipType = value;
                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Tax" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;simpleContent&amp;gt;
     *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AirTaxType"&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/simpleContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tax"
    })
    public static class Taxes {

        @XmlElement(name = "Tax", required = true)
        protected List<EMDType.Taxes.Tax> tax;

        /**
         * Gets the value of the tax property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the tax property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTax().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link EMDType.Taxes.Tax }
         * 
         * 
         */
        public List<EMDType.Taxes.Tax> getTax() {
            if (tax == null) {
                tax = new ArrayList<EMDType.Taxes.Tax>();
            }
            return this.tax;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;AirTaxType"&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Tax
            extends AirTaxType
        {


        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CouponInfo" maxOccurs="4"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="SoldFlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;choice&amp;gt;
     *                     &amp;lt;element name="CheckedInAirlineRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
     *                     &amp;lt;element name="FlownAirlineSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;/choice&amp;gt;
     *                   &amp;lt;element name="ExcessBaggage" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="PresentInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="To" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                           &amp;lt;attribute name="At" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ReasonForIssuance" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReasonForIssuanceGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ValidatingAirline" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="FiledFeeInfo" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                           &amp;lt;attribute name="BSR_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
     *                 &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
     *                 &amp;lt;attribute name="InConnectionNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
     *                 &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="Start" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="Duration" type="{http://www.opentravel.org/OTA/2003/05}DurationType" /&amp;gt;
     *                 &amp;lt;attribute name="End" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *                 &amp;lt;attribute name="InvoluntaryIndCode"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="I"/&amp;gt;
     *                       &amp;lt;enumeration value="L"/&amp;gt;
     *                       &amp;lt;enumeration value="S"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="SettlementAuthCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                 &amp;lt;attribute name="Value" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="AssociateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PromotionalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
     *                 &amp;lt;attribute name="Remark" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;attribute name="TaxOnEMD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="AssocFareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="ConsumedAtIssuanceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="DateOfService" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *       &amp;lt;attribute name="TicketDocumentNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="Type"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Ticket"/&amp;gt;
     *             &amp;lt;enumeration value="EMD"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="PrimaryDocInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="InConnectionDocNbr" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *       &amp;lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="ExchangeTktNbrInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "couponInfo"
    })
    public static class TicketDocument {

        @XmlElement(name = "CouponInfo", required = true)
        protected List<EMDType.TicketDocument.CouponInfo> couponInfo;
        @XmlAttribute(name = "TicketDocumentNbr")
        protected String ticketDocumentNbr;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "PrimaryDocInd")
        protected Boolean primaryDocInd;
        @XmlAttribute(name = "InConnectionDocNbr")
        protected String inConnectionDocNbr;
        @XmlAttribute(name = "DateOfIssue")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateOfIssue;
        @XmlAttribute(name = "ExchangeTktNbrInd")
        protected Boolean exchangeTktNbrInd;
        @XmlAttribute(name = "CompanyShortName")
        protected String companyShortName;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;

        /**
         * Gets the value of the couponInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the couponInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCouponInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link EMDType.TicketDocument.CouponInfo }
         * 
         * 
         */
        public List<EMDType.TicketDocument.CouponInfo> getCouponInfo() {
            if (couponInfo == null) {
                couponInfo = new ArrayList<EMDType.TicketDocument.CouponInfo>();
            }
            return this.couponInfo;
        }

        /**
         * Obtiene el valor de la propiedad ticketDocumentNbr.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketDocumentNbr() {
            return ticketDocumentNbr;
        }

        /**
         * Define el valor de la propiedad ticketDocumentNbr.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketDocumentNbr(String value) {
            this.ticketDocumentNbr = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryDocInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPrimaryDocInd() {
            return primaryDocInd;
        }

        /**
         * Define el valor de la propiedad primaryDocInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPrimaryDocInd(Boolean value) {
            this.primaryDocInd = value;
        }

        /**
         * Obtiene el valor de la propiedad inConnectionDocNbr.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInConnectionDocNbr() {
            return inConnectionDocNbr;
        }

        /**
         * Define el valor de la propiedad inConnectionDocNbr.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInConnectionDocNbr(String value) {
            this.inConnectionDocNbr = value;
        }

        /**
         * Obtiene el valor de la propiedad dateOfIssue.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateOfIssue() {
            return dateOfIssue;
        }

        /**
         * Define el valor de la propiedad dateOfIssue.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateOfIssue(XMLGregorianCalendar value) {
            this.dateOfIssue = value;
        }

        /**
         * Obtiene el valor de la propiedad exchangeTktNbrInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExchangeTktNbrInd() {
            return exchangeTktNbrInd;
        }

        /**
         * Define el valor de la propiedad exchangeTktNbrInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExchangeTktNbrInd(Boolean value) {
            this.exchangeTktNbrInd = value;
        }

        /**
         * Obtiene el valor de la propiedad companyShortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyShortName() {
            return companyShortName;
        }

        /**
         * Define el valor de la propiedad companyShortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyShortName(String value) {
            this.companyShortName = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad countryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * Define el valor de la propiedad countryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="SoldFlightSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;choice&amp;gt;
         *           &amp;lt;element name="CheckedInAirlineRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
         *           &amp;lt;element name="FlownAirlineSegmentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;/choice&amp;gt;
         *         &amp;lt;element name="ExcessBaggage" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="PresentInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="To" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                 &amp;lt;attribute name="At" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ReasonForIssuance" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReasonForIssuanceGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ValidatingAirline" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="FiledFeeInfo" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                 &amp;lt;attribute name="BSR_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
         *       &amp;lt;attribute name="Number" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
         *       &amp;lt;attribute name="InConnectionNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to4" /&amp;gt;
         *       &amp;lt;attribute name="CouponReference" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="FareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="Start" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="Duration" type="{http://www.opentravel.org/OTA/2003/05}DurationType" /&amp;gt;
         *       &amp;lt;attribute name="End" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="Status" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="CouponItinerarySeqNbr" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
         *       &amp;lt;attribute name="InvoluntaryIndCode"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="I"/&amp;gt;
         *             &amp;lt;enumeration value="L"/&amp;gt;
         *             &amp;lt;enumeration value="S"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="SettlementAuthCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *       &amp;lt;attribute name="Value" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="AssociateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PromotionalCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1to14" /&amp;gt;
         *       &amp;lt;attribute name="Remark" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *       &amp;lt;attribute name="TaxOnEMD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="AssocFareBasisCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="ConsumedAtIssuanceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="DateOfService" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "soldFlightSegmentRPH",
            "checkedInAirlineRPH",
            "flownAirlineSegmentRPH",
            "excessBaggage",
            "presentInfo",
            "reasonForIssuance",
            "validatingAirline",
            "filedFeeInfo"
        })
        public static class CouponInfo {

            @XmlElement(name = "SoldFlightSegmentRPH")
            protected String soldFlightSegmentRPH;
            @XmlElement(name = "CheckedInAirlineRPH")
            protected String checkedInAirlineRPH;
            @XmlElement(name = "FlownAirlineSegmentRPH")
            protected String flownAirlineSegmentRPH;
            @XmlElement(name = "ExcessBaggage")
            protected EMDType.TicketDocument.CouponInfo.ExcessBaggage excessBaggage;
            @XmlElement(name = "PresentInfo")
            protected EMDType.TicketDocument.CouponInfo.PresentInfo presentInfo;
            @XmlElement(name = "ReasonForIssuance")
            protected EMDType.TicketDocument.CouponInfo.ReasonForIssuance reasonForIssuance;
            @XmlElement(name = "ValidatingAirline")
            protected EMDType.TicketDocument.CouponInfo.ValidatingAirline validatingAirline;
            @XmlElement(name = "FiledFeeInfo")
            protected EMDType.TicketDocument.CouponInfo.FiledFeeInfo filedFeeInfo;
            @XmlAttribute(name = "Number")
            protected Integer number;
            @XmlAttribute(name = "InConnectionNbr")
            protected Integer inConnectionNbr;
            @XmlAttribute(name = "CouponReference")
            protected String couponReference;
            @XmlAttribute(name = "FareBasisCode")
            protected String fareBasisCode;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;
            @XmlAttribute(name = "Status")
            protected String status;
            @XmlAttribute(name = "CouponItinerarySeqNbr")
            protected Integer couponItinerarySeqNbr;
            @XmlAttribute(name = "InvoluntaryIndCode")
            protected String involuntaryIndCode;
            @XmlAttribute(name = "SettlementAuthCode")
            protected String settlementAuthCode;
            @XmlAttribute(name = "Value")
            protected BigDecimal value;
            @XmlAttribute(name = "AssociateInd")
            protected Boolean associateInd;
            @XmlAttribute(name = "PromotionalCode")
            protected String promotionalCode;
            @XmlAttribute(name = "Remark")
            protected String remark;
            @XmlAttribute(name = "TaxOnEMD_Ind")
            protected Boolean taxOnEMDInd;
            @XmlAttribute(name = "AssocFareBasisCode")
            protected String assocFareBasisCode;
            @XmlAttribute(name = "ConsumedAtIssuanceInd")
            protected Boolean consumedAtIssuanceInd;
            @XmlAttribute(name = "DateOfService")
            protected String dateOfService;
            @XmlAttribute(name = "UnitOfMeasureQuantity")
            protected BigDecimal unitOfMeasureQuantity;
            @XmlAttribute(name = "UnitOfMeasure")
            protected String unitOfMeasure;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;

            /**
             * Obtiene el valor de la propiedad soldFlightSegmentRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSoldFlightSegmentRPH() {
                return soldFlightSegmentRPH;
            }

            /**
             * Define el valor de la propiedad soldFlightSegmentRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSoldFlightSegmentRPH(String value) {
                this.soldFlightSegmentRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad checkedInAirlineRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCheckedInAirlineRPH() {
                return checkedInAirlineRPH;
            }

            /**
             * Define el valor de la propiedad checkedInAirlineRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCheckedInAirlineRPH(String value) {
                this.checkedInAirlineRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad flownAirlineSegmentRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlownAirlineSegmentRPH() {
                return flownAirlineSegmentRPH;
            }

            /**
             * Define el valor de la propiedad flownAirlineSegmentRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlownAirlineSegmentRPH(String value) {
                this.flownAirlineSegmentRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad excessBaggage.
             * 
             * @return
             *     possible object is
             *     {@link EMDType.TicketDocument.CouponInfo.ExcessBaggage }
             *     
             */
            public EMDType.TicketDocument.CouponInfo.ExcessBaggage getExcessBaggage() {
                return excessBaggage;
            }

            /**
             * Define el valor de la propiedad excessBaggage.
             * 
             * @param value
             *     allowed object is
             *     {@link EMDType.TicketDocument.CouponInfo.ExcessBaggage }
             *     
             */
            public void setExcessBaggage(EMDType.TicketDocument.CouponInfo.ExcessBaggage value) {
                this.excessBaggage = value;
            }

            /**
             * Obtiene el valor de la propiedad presentInfo.
             * 
             * @return
             *     possible object is
             *     {@link EMDType.TicketDocument.CouponInfo.PresentInfo }
             *     
             */
            public EMDType.TicketDocument.CouponInfo.PresentInfo getPresentInfo() {
                return presentInfo;
            }

            /**
             * Define el valor de la propiedad presentInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link EMDType.TicketDocument.CouponInfo.PresentInfo }
             *     
             */
            public void setPresentInfo(EMDType.TicketDocument.CouponInfo.PresentInfo value) {
                this.presentInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad reasonForIssuance.
             * 
             * @return
             *     possible object is
             *     {@link EMDType.TicketDocument.CouponInfo.ReasonForIssuance }
             *     
             */
            public EMDType.TicketDocument.CouponInfo.ReasonForIssuance getReasonForIssuance() {
                return reasonForIssuance;
            }

            /**
             * Define el valor de la propiedad reasonForIssuance.
             * 
             * @param value
             *     allowed object is
             *     {@link EMDType.TicketDocument.CouponInfo.ReasonForIssuance }
             *     
             */
            public void setReasonForIssuance(EMDType.TicketDocument.CouponInfo.ReasonForIssuance value) {
                this.reasonForIssuance = value;
            }

            /**
             * Obtiene el valor de la propiedad validatingAirline.
             * 
             * @return
             *     possible object is
             *     {@link EMDType.TicketDocument.CouponInfo.ValidatingAirline }
             *     
             */
            public EMDType.TicketDocument.CouponInfo.ValidatingAirline getValidatingAirline() {
                return validatingAirline;
            }

            /**
             * Define el valor de la propiedad validatingAirline.
             * 
             * @param value
             *     allowed object is
             *     {@link EMDType.TicketDocument.CouponInfo.ValidatingAirline }
             *     
             */
            public void setValidatingAirline(EMDType.TicketDocument.CouponInfo.ValidatingAirline value) {
                this.validatingAirline = value;
            }

            /**
             * Obtiene el valor de la propiedad filedFeeInfo.
             * 
             * @return
             *     possible object is
             *     {@link EMDType.TicketDocument.CouponInfo.FiledFeeInfo }
             *     
             */
            public EMDType.TicketDocument.CouponInfo.FiledFeeInfo getFiledFeeInfo() {
                return filedFeeInfo;
            }

            /**
             * Define el valor de la propiedad filedFeeInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link EMDType.TicketDocument.CouponInfo.FiledFeeInfo }
             *     
             */
            public void setFiledFeeInfo(EMDType.TicketDocument.CouponInfo.FiledFeeInfo value) {
                this.filedFeeInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad number.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getNumber() {
                return number;
            }

            /**
             * Define el valor de la propiedad number.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setNumber(Integer value) {
                this.number = value;
            }

            /**
             * Obtiene el valor de la propiedad inConnectionNbr.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getInConnectionNbr() {
                return inConnectionNbr;
            }

            /**
             * Define el valor de la propiedad inConnectionNbr.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setInConnectionNbr(Integer value) {
                this.inConnectionNbr = value;
            }

            /**
             * Obtiene el valor de la propiedad couponReference.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCouponReference() {
                return couponReference;
            }

            /**
             * Define el valor de la propiedad couponReference.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCouponReference(String value) {
                this.couponReference = value;
            }

            /**
             * Obtiene el valor de la propiedad fareBasisCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareBasisCode() {
                return fareBasisCode;
            }

            /**
             * Define el valor de la propiedad fareBasisCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareBasisCode(String value) {
                this.fareBasisCode = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

            /**
             * Obtiene el valor de la propiedad status.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Define el valor de la propiedad status.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * Obtiene el valor de la propiedad couponItinerarySeqNbr.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getCouponItinerarySeqNbr() {
                return couponItinerarySeqNbr;
            }

            /**
             * Define el valor de la propiedad couponItinerarySeqNbr.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setCouponItinerarySeqNbr(Integer value) {
                this.couponItinerarySeqNbr = value;
            }

            /**
             * Obtiene el valor de la propiedad involuntaryIndCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvoluntaryIndCode() {
                return involuntaryIndCode;
            }

            /**
             * Define el valor de la propiedad involuntaryIndCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvoluntaryIndCode(String value) {
                this.involuntaryIndCode = value;
            }

            /**
             * Obtiene el valor de la propiedad settlementAuthCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSettlementAuthCode() {
                return settlementAuthCode;
            }

            /**
             * Define el valor de la propiedad settlementAuthCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSettlementAuthCode(String value) {
                this.settlementAuthCode = value;
            }

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setValue(BigDecimal value) {
                this.value = value;
            }

            /**
             * Obtiene el valor de la propiedad associateInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAssociateInd() {
                return associateInd;
            }

            /**
             * Define el valor de la propiedad associateInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAssociateInd(Boolean value) {
                this.associateInd = value;
            }

            /**
             * Obtiene el valor de la propiedad promotionalCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPromotionalCode() {
                return promotionalCode;
            }

            /**
             * Define el valor de la propiedad promotionalCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPromotionalCode(String value) {
                this.promotionalCode = value;
            }

            /**
             * Obtiene el valor de la propiedad remark.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRemark() {
                return remark;
            }

            /**
             * Define el valor de la propiedad remark.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRemark(String value) {
                this.remark = value;
            }

            /**
             * Obtiene el valor de la propiedad taxOnEMDInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isTaxOnEMDInd() {
                return taxOnEMDInd;
            }

            /**
             * Define el valor de la propiedad taxOnEMDInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setTaxOnEMDInd(Boolean value) {
                this.taxOnEMDInd = value;
            }

            /**
             * Obtiene el valor de la propiedad assocFareBasisCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAssocFareBasisCode() {
                return assocFareBasisCode;
            }

            /**
             * Define el valor de la propiedad assocFareBasisCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAssocFareBasisCode(String value) {
                this.assocFareBasisCode = value;
            }

            /**
             * Obtiene el valor de la propiedad consumedAtIssuanceInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isConsumedAtIssuanceInd() {
                return consumedAtIssuanceInd;
            }

            /**
             * Define el valor de la propiedad consumedAtIssuanceInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setConsumedAtIssuanceInd(Boolean value) {
                this.consumedAtIssuanceInd = value;
            }

            /**
             * Obtiene el valor de la propiedad dateOfService.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateOfService() {
                return dateOfService;
            }

            /**
             * Define el valor de la propiedad dateOfService.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateOfService(String value) {
                this.dateOfService = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getUnitOfMeasureQuantity() {
                return unitOfMeasureQuantity;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setUnitOfMeasureQuantity(BigDecimal value) {
                this.unitOfMeasureQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasure.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasure() {
                return unitOfMeasure;
            }

            /**
             * Define el valor de la propiedad unitOfMeasure.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasure(String value) {
                this.unitOfMeasure = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UnitsOfMeasureGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ExcessBaggage {

                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;
                @XmlAttribute(name = "UnitOfMeasureQuantity")
                protected BigDecimal unitOfMeasureQuantity;
                @XmlAttribute(name = "UnitOfMeasure")
                protected String unitOfMeasure;
                @XmlAttribute(name = "UnitOfMeasureCode")
                protected String unitOfMeasureCode;

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasureQuantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getUnitOfMeasureQuantity() {
                    return unitOfMeasureQuantity;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasureQuantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setUnitOfMeasureQuantity(BigDecimal value) {
                    this.unitOfMeasureQuantity = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasure.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnitOfMeasure() {
                    return unitOfMeasure;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasure.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnitOfMeasure(String value) {
                    this.unitOfMeasure = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasureCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnitOfMeasureCode() {
                    return unitOfMeasureCode;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasureCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnitOfMeasureCode(String value) {
                    this.unitOfMeasureCode = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *       &amp;lt;attribute name="BSR_Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class FiledFeeInfo {

                @XmlAttribute(name = "BSR_Rate")
                protected BigDecimal bsrRate;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;
                @XmlAttribute(name = "Amount")
                protected BigDecimal amount;

                /**
                 * Obtiene el valor de la propiedad bsrRate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getBSRRate() {
                    return bsrRate;
                }

                /**
                 * Define el valor de la propiedad bsrRate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setBSRRate(BigDecimal value) {
                    this.bsrRate = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="To" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *       &amp;lt;attribute name="At" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class PresentInfo {

                @XmlAttribute(name = "To")
                protected String to;
                @XmlAttribute(name = "At")
                protected String at;

                /**
                 * Obtiene el valor de la propiedad to.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTo() {
                    return to;
                }

                /**
                 * Define el valor de la propiedad to.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTo(String value) {
                    this.to = value;
                }

                /**
                 * Obtiene el valor de la propiedad at.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAt() {
                    return at;
                }

                /**
                 * Define el valor de la propiedad at.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAt(String value) {
                    this.at = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ReasonForIssuanceGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ReasonForIssuance {

                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "SubCode")
                protected String subCode;
                @XmlAttribute(name = "Description")
                protected String description;

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad subCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSubCode() {
                    return subCode;
                }

                /**
                 * Define el valor de la propiedad subCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSubCode(String value) {
                    this.subCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad description.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Define el valor de la propiedad description.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ValidatingAirline {

                @XmlAttribute(name = "CompanyShortName")
                protected String companyShortName;
                @XmlAttribute(name = "TravelSector")
                protected String travelSector;
                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "CodeContext")
                protected String codeContext;
                @XmlAttribute(name = "CountryCode")
                protected String countryCode;

                /**
                 * Obtiene el valor de la propiedad companyShortName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCompanyShortName() {
                    return companyShortName;
                }

                /**
                 * Define el valor de la propiedad companyShortName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCompanyShortName(String value) {
                    this.companyShortName = value;
                }

                /**
                 * Obtiene el valor de la propiedad travelSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTravelSector() {
                    return travelSector;
                }

                /**
                 * Define el valor de la propiedad travelSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTravelSector(String value) {
                    this.travelSector = value;
                }

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * Obtiene el valor de la propiedad codeContext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodeContext() {
                    return codeContext;
                }

                /**
                 * Define el valor de la propiedad codeContext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodeContext(String value) {
                    this.codeContext = value;
                }

                /**
                 * Obtiene el valor de la propiedad countryCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryCode() {
                    return countryCode;
                }

                /**
                 * Define el valor de la propiedad countryCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryCode(String value) {
                    this.countryCode = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *       &amp;lt;attribute name="Purpose" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *       &amp;lt;attribute name="FareAmountType" type="{http://www.opentravel.org/OTA/2003/05}FareAmountType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TotalFare {

        @XmlAttribute(name = "Purpose")
        protected PurposeType purpose;
        @XmlAttribute(name = "FareAmountType")
        protected FareAmountType fareAmountType;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad purpose.
         * 
         * @return
         *     possible object is
         *     {@link PurposeType }
         *     
         */
        public PurposeType getPurpose() {
            return purpose;
        }

        /**
         * Define el valor de la propiedad purpose.
         * 
         * @param value
         *     allowed object is
         *     {@link PurposeType }
         *     
         */
        public void setPurpose(PurposeType value) {
            this.purpose = value;
        }

        /**
         * Obtiene el valor de la propiedad fareAmountType.
         * 
         * @return
         *     possible object is
         *     {@link FareAmountType }
         *     
         */
        public FareAmountType getFareAmountType() {
            return fareAmountType;
        }

        /**
         * Define el valor de la propiedad fareAmountType.
         * 
         * @param value
         *     allowed object is
         *     {@link FareAmountType }
         *     
         */
        public void setFareAmountType(FareAmountType value) {
            this.fareAmountType = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TravelerRefNumberGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TravelerRefNumber {

        @XmlAttribute(name = "RPH")
        protected String rph;
        @XmlAttribute(name = "SurnameRefNumber")
        protected String surnameRefNumber;

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

        /**
         * Obtiene el valor de la propiedad surnameRefNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurnameRefNumber() {
            return surnameRefNumber;
        }

        /**
         * Define el valor de la propiedad surnameRefNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurnameRefNumber(String value) {
            this.surnameRefNumber = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *       &amp;lt;attribute name="FareCalcMode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
     *       &amp;lt;attribute name="Operation" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *       &amp;lt;attribute name="Type" type="{http://www.opentravel.org/OTA/2003/05}PurposeType" /&amp;gt;
     *       &amp;lt;attribute name="ReportingCode" type="{http://www.opentravel.org/OTA/2003/05}AlphaNumericStringLength1" /&amp;gt;
     *       &amp;lt;attribute name="Info" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class UnstructuredFareCalc {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "FareCalcMode")
        protected String fareCalcMode;
        @XmlAttribute(name = "Operation")
        protected ActionType operation;
        @XmlAttribute(name = "Type")
        protected PurposeType type;
        @XmlAttribute(name = "ReportingCode")
        protected String reportingCode;
        @XmlAttribute(name = "Info", required = true)
        protected String info;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad fareCalcMode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareCalcMode() {
            return fareCalcMode;
        }

        /**
         * Define el valor de la propiedad fareCalcMode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareCalcMode(String value) {
            this.fareCalcMode = value;
        }

        /**
         * Obtiene el valor de la propiedad operation.
         * 
         * @return
         *     possible object is
         *     {@link ActionType }
         *     
         */
        public ActionType getOperation() {
            return operation;
        }

        /**
         * Define el valor de la propiedad operation.
         * 
         * @param value
         *     allowed object is
         *     {@link ActionType }
         *     
         */
        public void setOperation(ActionType value) {
            this.operation = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link PurposeType }
         *     
         */
        public PurposeType getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link PurposeType }
         *     
         */
        public void setType(PurposeType value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad reportingCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReportingCode() {
            return reportingCode;
        }

        /**
         * Define el valor de la propiedad reportingCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReportingCode(String value) {
            this.reportingCode = value;
        }

        /**
         * Obtiene el valor de la propiedad info.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfo() {
            return info;
        }

        /**
         * Define el valor de la propiedad info.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfo(String value) {
            this.info = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CompanyID_AttributesGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ValidatingAirline {

        @XmlAttribute(name = "CompanyShortName")
        protected String companyShortName;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "CodeContext")
        protected String codeContext;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;

        /**
         * Obtiene el valor de la propiedad companyShortName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyShortName() {
            return companyShortName;
        }

        /**
         * Define el valor de la propiedad companyShortName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyShortName(String value) {
            this.companyShortName = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad codeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeContext() {
            return codeContext;
        }

        /**
         * Define el valor de la propiedad codeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeContext(String value) {
            this.codeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad countryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * Define el valor de la propiedad countryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }

    }

}
