
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specific itinerary information, including additional contact information, reservation items, air reservation ticket information, pricing and special requests.
 * 
 * &lt;p&gt;Clase Java para ItineraryInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ItineraryInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ContactInfo" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReservationItems" type="{http://www.opentravel.org/OTA/2003/05}ReservationItemsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Ticketing" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type"&amp;gt;
 *                 &amp;lt;attribute name="PlatingCarrier" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ItineraryPricing" type="{http://www.opentravel.org/OTA/2003/05}ItinPricingType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SpecialRequestDetails"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecialReqDetailsType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryInfoType", propOrder = {
    "contactInfo",
    "reservationItems",
    "ticketing",
    "itineraryPricing",
    "specialRequestDetails",
    "tpaExtensions"
})
public class ItineraryInfoType {

    @XmlElement(name = "ContactInfo")
    protected ContactPersonType contactInfo;
    @XmlElement(name = "ReservationItems")
    protected ReservationItemsType reservationItems;
    @XmlElement(name = "Ticketing")
    protected ItineraryInfoType.Ticketing ticketing;
    @XmlElement(name = "ItineraryPricing")
    protected List<ItinPricingType> itineraryPricing;
    @XmlElement(name = "SpecialRequestDetails", required = true)
    protected ItineraryInfoType.SpecialRequestDetails specialRequestDetails;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Obtiene el valor de la propiedad contactInfo.
     * 
     * @return
     *     possible object is
     *     {@link ContactPersonType }
     *     
     */
    public ContactPersonType getContactInfo() {
        return contactInfo;
    }

    /**
     * Define el valor de la propiedad contactInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPersonType }
     *     
     */
    public void setContactInfo(ContactPersonType value) {
        this.contactInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationItems.
     * 
     * @return
     *     possible object is
     *     {@link ReservationItemsType }
     *     
     */
    public ReservationItemsType getReservationItems() {
        return reservationItems;
    }

    /**
     * Define el valor de la propiedad reservationItems.
     * 
     * @param value
     *     allowed object is
     *     {@link ReservationItemsType }
     *     
     */
    public void setReservationItems(ReservationItemsType value) {
        this.reservationItems = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketing.
     * 
     * @return
     *     possible object is
     *     {@link ItineraryInfoType.Ticketing }
     *     
     */
    public ItineraryInfoType.Ticketing getTicketing() {
        return ticketing;
    }

    /**
     * Define el valor de la propiedad ticketing.
     * 
     * @param value
     *     allowed object is
     *     {@link ItineraryInfoType.Ticketing }
     *     
     */
    public void setTicketing(ItineraryInfoType.Ticketing value) {
        this.ticketing = value;
    }

    /**
     * Gets the value of the itineraryPricing property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the itineraryPricing property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getItineraryPricing().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ItinPricingType }
     * 
     * 
     */
    public List<ItinPricingType> getItineraryPricing() {
        if (itineraryPricing == null) {
            itineraryPricing = new ArrayList<ItinPricingType>();
        }
        return this.itineraryPricing;
    }

    /**
     * Obtiene el valor de la propiedad specialRequestDetails.
     * 
     * @return
     *     possible object is
     *     {@link ItineraryInfoType.SpecialRequestDetails }
     *     
     */
    public ItineraryInfoType.SpecialRequestDetails getSpecialRequestDetails() {
        return specialRequestDetails;
    }

    /**
     * Define el valor de la propiedad specialRequestDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link ItineraryInfoType.SpecialRequestDetails }
     *     
     */
    public void setSpecialRequestDetails(ItineraryInfoType.SpecialRequestDetails value) {
        this.specialRequestDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecialReqDetailsType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tpaExtensions"
    })
    public static class SpecialRequestDetails
        extends SpecialReqDetailsType
    {

        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TicketingInfoRS_Type"&amp;gt;
     *       &amp;lt;attribute name="PlatingCarrier" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Ticketing
        extends TicketingInfoRSType
    {

        @XmlAttribute(name = "PlatingCarrier")
        protected String platingCarrier;

        /**
         * Obtiene el valor de la propiedad platingCarrier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlatingCarrier() {
            return platingCarrier;
        }

        /**
         * Define el valor de la propiedad platingCarrier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlatingCarrier(String value) {
            this.platingCarrier = value;
        }

    }

}
