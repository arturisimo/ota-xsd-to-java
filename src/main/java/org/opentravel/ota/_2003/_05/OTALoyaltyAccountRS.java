
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="AccountInfo" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AccountInfoType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                 &amp;lt;/extension&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="Location" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "uniqueID",
    "accountInfo",
    "location",
    "errors"
})
@XmlRootElement(name = "OTA_LoyaltyAccountRS")
public class OTALoyaltyAccountRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "UniqueID")
    protected UniqueIDType uniqueID;
    @XmlElement(name = "AccountInfo")
    protected OTALoyaltyAccountRS.AccountInfo accountInfo;
    @XmlElement(name = "Location")
    protected LocationType location;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad uniqueID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getUniqueID() {
        return uniqueID;
    }

    /**
     * Define el valor de la propiedad uniqueID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setUniqueID(UniqueIDType value) {
        this.uniqueID = value;
    }

    /**
     * Obtiene el valor de la propiedad accountInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTALoyaltyAccountRS.AccountInfo }
     *     
     */
    public OTALoyaltyAccountRS.AccountInfo getAccountInfo() {
        return accountInfo;
    }

    /**
     * Define el valor de la propiedad accountInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTALoyaltyAccountRS.AccountInfo }
     *     
     */
    public void setAccountInfo(OTALoyaltyAccountRS.AccountInfo value) {
        this.accountInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AccountInfoType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CustLoyalty" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Action" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custLoyalty"
    })
    public static class AccountInfo
        extends AccountInfoType
    {

        @XmlElement(name = "CustLoyalty")
        protected OTALoyaltyAccountRS.AccountInfo.CustLoyalty custLoyalty;
        @XmlAttribute(name = "Action")
        protected ActionType action;

        /**
         * Obtiene el valor de la propiedad custLoyalty.
         * 
         * @return
         *     possible object is
         *     {@link OTALoyaltyAccountRS.AccountInfo.CustLoyalty }
         *     
         */
        public OTALoyaltyAccountRS.AccountInfo.CustLoyalty getCustLoyalty() {
            return custLoyalty;
        }

        /**
         * Define el valor de la propiedad custLoyalty.
         * 
         * @param value
         *     allowed object is
         *     {@link OTALoyaltyAccountRS.AccountInfo.CustLoyalty }
         *     
         */
        public void setCustLoyalty(OTALoyaltyAccountRS.AccountInfo.CustLoyalty value) {
            this.custLoyalty = value;
        }

        /**
         * Obtiene el valor de la propiedad action.
         * 
         * @return
         *     possible object is
         *     {@link ActionType }
         *     
         */
        public ActionType getAction() {
            return action;
        }

        /**
         * Define el valor de la propiedad action.
         * 
         * @param value
         *     allowed object is
         *     {@link ActionType }
         *     
         */
        public void setAction(ActionType value) {
            this.action = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CustLoyalty {

            @XmlAttribute(name = "ShareSynchInd")
            protected String shareSynchInd;
            @XmlAttribute(name = "ShareMarketInd")
            protected String shareMarketInd;
            @XmlAttribute(name = "ProgramID")
            protected String programID;
            @XmlAttribute(name = "MembershipID")
            protected String membershipID;
            @XmlAttribute(name = "TravelSector")
            protected String travelSector;
            @XmlAttribute(name = "VendorCode")
            protected List<String> vendorCode;
            @XmlAttribute(name = "PrimaryLoyaltyIndicator")
            protected Boolean primaryLoyaltyIndicator;
            @XmlAttribute(name = "AllianceLoyaltyLevelName")
            protected String allianceLoyaltyLevelName;
            @XmlAttribute(name = "CustomerType")
            protected String customerType;
            @XmlAttribute(name = "CustomerValue")
            protected String customerValue;
            @XmlAttribute(name = "Password")
            protected String password;
            @XmlAttribute(name = "LoyalLevel")
            protected String loyalLevel;
            @XmlAttribute(name = "LoyalLevelCode")
            protected Integer loyalLevelCode;
            @XmlAttribute(name = "SingleVendorInd")
            protected String singleVendorInd;
            @XmlAttribute(name = "SignupDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar signupDate;
            @XmlAttribute(name = "EffectiveDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar effectiveDate;
            @XmlAttribute(name = "ExpireDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar expireDate;
            @XmlAttribute(name = "ExpireDateExclusiveIndicator")
            protected Boolean expireDateExclusiveIndicator;
            @XmlAttribute(name = "RPH")
            protected String rph;

            /**
             * Obtiene el valor de la propiedad shareSynchInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareSynchInd() {
                return shareSynchInd;
            }

            /**
             * Define el valor de la propiedad shareSynchInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareSynchInd(String value) {
                this.shareSynchInd = value;
            }

            /**
             * Obtiene el valor de la propiedad shareMarketInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareMarketInd() {
                return shareMarketInd;
            }

            /**
             * Define el valor de la propiedad shareMarketInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareMarketInd(String value) {
                this.shareMarketInd = value;
            }

            /**
             * Obtiene el valor de la propiedad programID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramID() {
                return programID;
            }

            /**
             * Define el valor de la propiedad programID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramID(String value) {
                this.programID = value;
            }

            /**
             * Obtiene el valor de la propiedad membershipID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMembershipID() {
                return membershipID;
            }

            /**
             * Define el valor de la propiedad membershipID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMembershipID(String value) {
                this.membershipID = value;
            }

            /**
             * Obtiene el valor de la propiedad travelSector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelSector() {
                return travelSector;
            }

            /**
             * Define el valor de la propiedad travelSector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelSector(String value) {
                this.travelSector = value;
            }

            /**
             * Gets the value of the vendorCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getVendorCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getVendorCode() {
                if (vendorCode == null) {
                    vendorCode = new ArrayList<String>();
                }
                return this.vendorCode;
            }

            /**
             * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPrimaryLoyaltyIndicator() {
                return primaryLoyaltyIndicator;
            }

            /**
             * Define el valor de la propiedad primaryLoyaltyIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPrimaryLoyaltyIndicator(Boolean value) {
                this.primaryLoyaltyIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAllianceLoyaltyLevelName() {
                return allianceLoyaltyLevelName;
            }

            /**
             * Define el valor de la propiedad allianceLoyaltyLevelName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAllianceLoyaltyLevelName(String value) {
                this.allianceLoyaltyLevelName = value;
            }

            /**
             * Obtiene el valor de la propiedad customerType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerType() {
                return customerType;
            }

            /**
             * Define el valor de la propiedad customerType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerType(String value) {
                this.customerType = value;
            }

            /**
             * Obtiene el valor de la propiedad customerValue.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerValue() {
                return customerValue;
            }

            /**
             * Define el valor de la propiedad customerValue.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerValue(String value) {
                this.customerValue = value;
            }

            /**
             * Obtiene el valor de la propiedad password.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassword() {
                return password;
            }

            /**
             * Define el valor de la propiedad password.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassword(String value) {
                this.password = value;
            }

            /**
             * Obtiene el valor de la propiedad loyalLevel.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLoyalLevel() {
                return loyalLevel;
            }

            /**
             * Define el valor de la propiedad loyalLevel.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLoyalLevel(String value) {
                this.loyalLevel = value;
            }

            /**
             * Obtiene el valor de la propiedad loyalLevelCode.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getLoyalLevelCode() {
                return loyalLevelCode;
            }

            /**
             * Define el valor de la propiedad loyalLevelCode.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setLoyalLevelCode(Integer value) {
                this.loyalLevelCode = value;
            }

            /**
             * Obtiene el valor de la propiedad singleVendorInd.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSingleVendorInd() {
                return singleVendorInd;
            }

            /**
             * Define el valor de la propiedad singleVendorInd.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSingleVendorInd(String value) {
                this.singleVendorInd = value;
            }

            /**
             * Obtiene el valor de la propiedad signupDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getSignupDate() {
                return signupDate;
            }

            /**
             * Define el valor de la propiedad signupDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setSignupDate(XMLGregorianCalendar value) {
                this.signupDate = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEffectiveDate() {
                return effectiveDate;
            }

            /**
             * Define el valor de la propiedad effectiveDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEffectiveDate(XMLGregorianCalendar value) {
                this.effectiveDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpireDate() {
                return expireDate;
            }

            /**
             * Define el valor de la propiedad expireDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpireDate(XMLGregorianCalendar value) {
                this.expireDate = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExpireDateExclusiveIndicator() {
                return expireDateExclusiveIndicator;
            }

            /**
             * Define el valor de la propiedad expireDateExclusiveIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExpireDateExclusiveIndicator(Boolean value) {
                this.expireDateExclusiveIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad rph.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRPH() {
                return rph;
            }

            /**
             * Define el valor de la propiedad rph.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRPH(String value) {
                this.rph = value;
            }

        }

    }

}
