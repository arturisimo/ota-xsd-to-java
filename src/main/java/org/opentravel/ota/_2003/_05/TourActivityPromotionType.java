
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Promotion details, including coupons and other discount plans.
 * 
 * &lt;p&gt;Clase Java para TourActivityPromotionType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityPromotionType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Promotion" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DiscountProgram" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Detail" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityPromotionType", propOrder = {
    "promotion",
    "discountProgram"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.Discount.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRS.Reservation.ReservationInfo.Discount.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.Discount.class
})
public class TourActivityPromotionType {

    @XmlElement(name = "Promotion")
    protected List<TourActivityPromotionType.Promotion> promotion;
    @XmlElement(name = "DiscountProgram")
    protected TourActivityPromotionType.DiscountProgram discountProgram;

    /**
     * Gets the value of the promotion property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotion property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPromotion().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TourActivityPromotionType.Promotion }
     * 
     * 
     */
    public List<TourActivityPromotionType.Promotion> getPromotion() {
        if (promotion == null) {
            promotion = new ArrayList<TourActivityPromotionType.Promotion>();
        }
        return this.promotion;
    }

    /**
     * Obtiene el valor de la propiedad discountProgram.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityPromotionType.DiscountProgram }
     *     
     */
    public TourActivityPromotionType.DiscountProgram getDiscountProgram() {
        return discountProgram;
    }

    /**
     * Define el valor de la propiedad discountProgram.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityPromotionType.DiscountProgram }
     *     
     */
    public void setDiscountProgram(TourActivityPromotionType.DiscountProgram value) {
        this.discountProgram = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Detail" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ParticipationInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detail",
        "participationInfo"
    })
    public static class DiscountProgram {

        @XmlElement(name = "Detail")
        protected TourActivityPromotionType.DiscountProgram.Detail detail;
        @XmlElement(name = "ParticipationInfo")
        protected TourActivityPromotionType.DiscountProgram.ParticipationInfo participationInfo;

        /**
         * Obtiene el valor de la propiedad detail.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityPromotionType.DiscountProgram.Detail }
         *     
         */
        public TourActivityPromotionType.DiscountProgram.Detail getDetail() {
            return detail;
        }

        /**
         * Define el valor de la propiedad detail.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityPromotionType.DiscountProgram.Detail }
         *     
         */
        public void setDetail(TourActivityPromotionType.DiscountProgram.Detail value) {
            this.detail = value;
        }

        /**
         * Obtiene el valor de la propiedad participationInfo.
         * 
         * @return
         *     possible object is
         *     {@link TourActivityPromotionType.DiscountProgram.ParticipationInfo }
         *     
         */
        public TourActivityPromotionType.DiscountProgram.ParticipationInfo getParticipationInfo() {
            return participationInfo;
        }

        /**
         * Define el valor de la propiedad participationInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link TourActivityPromotionType.DiscountProgram.ParticipationInfo }
         *     
         */
        public void setParticipationInfo(TourActivityPromotionType.DiscountProgram.ParticipationInfo value) {
            this.participationInfo = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Provider" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ProgramID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "provider"
        })
        public static class Detail {

            @XmlElement(name = "Provider")
            protected CompanyNameType provider;
            @XmlAttribute(name = "ProgramID")
            protected String programID;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Description")
            protected String description;

            /**
             * Obtiene el valor de la propiedad provider.
             * 
             * @return
             *     possible object is
             *     {@link CompanyNameType }
             *     
             */
            public CompanyNameType getProvider() {
                return provider;
            }

            /**
             * Define el valor de la propiedad provider.
             * 
             * @param value
             *     allowed object is
             *     {@link CompanyNameType }
             *     
             */
            public void setProvider(CompanyNameType value) {
                this.provider = value;
            }

            /**
             * Obtiene el valor de la propiedad programID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramID() {
                return programID;
            }

            /**
             * Define el valor de la propiedad programID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramID(String value) {
                this.programID = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="ValidFromDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="ValidThroughDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="ProgramNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ParticipationInfo {

            @XmlAttribute(name = "ValidFromDate")
            protected String validFromDate;
            @XmlAttribute(name = "ValidThroughDate")
            protected String validThroughDate;
            @XmlAttribute(name = "ProgramNumber")
            protected String programNumber;

            /**
             * Obtiene el valor de la propiedad validFromDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValidFromDate() {
                return validFromDate;
            }

            /**
             * Define el valor de la propiedad validFromDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValidFromDate(String value) {
                this.validFromDate = value;
            }

            /**
             * Obtiene el valor de la propiedad validThroughDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValidThroughDate() {
                return validThroughDate;
            }

            /**
             * Define el valor de la propiedad validThroughDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValidThroughDate(String value) {
                this.validThroughDate = value;
            }

            /**
             * Obtiene el valor de la propiedad programNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramNumber() {
                return programNumber;
            }

            /**
             * Define el valor de la propiedad programNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramNumber(String value) {
                this.programNumber = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="StartDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="EndDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Promotion {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "StartDate")
        protected String startDate;
        @XmlAttribute(name = "EndDate")
        protected String endDate;
        @XmlAttribute(name = "Disclaimer")
        protected String disclaimer;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad startDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartDate() {
            return startDate;
        }

        /**
         * Define el valor de la propiedad startDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartDate(String value) {
            this.startDate = value;
        }

        /**
         * Obtiene el valor de la propiedad endDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndDate() {
            return endDate;
        }

        /**
         * Define el valor de la propiedad endDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndDate(String value) {
            this.endDate = value;
        }

        /**
         * Obtiene el valor de la propiedad disclaimer.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisclaimer() {
            return disclaimer;
        }

        /**
         * Define el valor de la propiedad disclaimer.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisclaimer(String value) {
            this.disclaimer = value;
        }

    }

}
