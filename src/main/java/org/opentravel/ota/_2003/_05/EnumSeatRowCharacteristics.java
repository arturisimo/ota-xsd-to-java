
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para Enum_SeatRowCharacteristics.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="Enum_SeatRowCharacteristics"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="BufferRow"/&amp;gt;
 *     &amp;lt;enumeration value="ExitLeft"/&amp;gt;
 *     &amp;lt;enumeration value="ExitRight"/&amp;gt;
 *     &amp;lt;enumeration value="ExitRow"/&amp;gt;
 *     &amp;lt;enumeration value="ExitRowWithCabinFacilitiesInDesignatedColumn"/&amp;gt;
 *     &amp;lt;enumeration value="ExitRowWithCabinFacilitiesInUndesignatedColumn"/&amp;gt;
 *     &amp;lt;enumeration value="ExtraLegRoom"/&amp;gt;
 *     &amp;lt;enumeration value="IndifferentRow"/&amp;gt;
 *     &amp;lt;enumeration value="LowerdeckRow"/&amp;gt;
 *     &amp;lt;enumeration value="MaindeckRow"/&amp;gt;
 *     &amp;lt;enumeration value="NosmokingRow"/&amp;gt;
 *     &amp;lt;enumeration value="NotOverwingRow"/&amp;gt;
 *     &amp;lt;enumeration value="OverwingRow"/&amp;gt;
 *     &amp;lt;enumeration value="RowDoesNotExist"/&amp;gt;
 *     &amp;lt;enumeration value="RowWithCabinFacilitiesInDesignatedColumn"/&amp;gt;
 *     &amp;lt;enumeration value="RowWithCabinFacilitiesInUndesignatedColumn"/&amp;gt;
 *     &amp;lt;enumeration value="RowWithMovieScreen"/&amp;gt;
 *     &amp;lt;enumeration value="SeatRestrictionsApply"/&amp;gt;
 *     &amp;lt;enumeration value="SmokingRow"/&amp;gt;
 *     &amp;lt;enumeration value="UpperDeckRow"/&amp;gt;
 *     &amp;lt;enumeration value="Preferred_PreferentialRow"/&amp;gt;
 *     &amp;lt;enumeration value="WingStart"/&amp;gt;
 *     &amp;lt;enumeration value="WingEnd"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "Enum_SeatRowCharacteristics")
@XmlEnum
public enum EnumSeatRowCharacteristics {

    @XmlEnumValue("BufferRow")
    BUFFER_ROW("BufferRow"),
    @XmlEnumValue("ExitLeft")
    EXIT_LEFT("ExitLeft"),
    @XmlEnumValue("ExitRight")
    EXIT_RIGHT("ExitRight"),
    @XmlEnumValue("ExitRow")
    EXIT_ROW("ExitRow"),
    @XmlEnumValue("ExitRowWithCabinFacilitiesInDesignatedColumn")
    EXIT_ROW_WITH_CABIN_FACILITIES_IN_DESIGNATED_COLUMN("ExitRowWithCabinFacilitiesInDesignatedColumn"),
    @XmlEnumValue("ExitRowWithCabinFacilitiesInUndesignatedColumn")
    EXIT_ROW_WITH_CABIN_FACILITIES_IN_UNDESIGNATED_COLUMN("ExitRowWithCabinFacilitiesInUndesignatedColumn"),
    @XmlEnumValue("ExtraLegRoom")
    EXTRA_LEG_ROOM("ExtraLegRoom"),
    @XmlEnumValue("IndifferentRow")
    INDIFFERENT_ROW("IndifferentRow"),
    @XmlEnumValue("LowerdeckRow")
    LOWERDECK_ROW("LowerdeckRow"),
    @XmlEnumValue("MaindeckRow")
    MAINDECK_ROW("MaindeckRow"),
    @XmlEnumValue("NosmokingRow")
    NOSMOKING_ROW("NosmokingRow"),
    @XmlEnumValue("NotOverwingRow")
    NOT_OVERWING_ROW("NotOverwingRow"),
    @XmlEnumValue("OverwingRow")
    OVERWING_ROW("OverwingRow"),
    @XmlEnumValue("RowDoesNotExist")
    ROW_DOES_NOT_EXIST("RowDoesNotExist"),
    @XmlEnumValue("RowWithCabinFacilitiesInDesignatedColumn")
    ROW_WITH_CABIN_FACILITIES_IN_DESIGNATED_COLUMN("RowWithCabinFacilitiesInDesignatedColumn"),
    @XmlEnumValue("RowWithCabinFacilitiesInUndesignatedColumn")
    ROW_WITH_CABIN_FACILITIES_IN_UNDESIGNATED_COLUMN("RowWithCabinFacilitiesInUndesignatedColumn"),
    @XmlEnumValue("RowWithMovieScreen")
    ROW_WITH_MOVIE_SCREEN("RowWithMovieScreen"),
    @XmlEnumValue("SeatRestrictionsApply")
    SEAT_RESTRICTIONS_APPLY("SeatRestrictionsApply"),
    @XmlEnumValue("SmokingRow")
    SMOKING_ROW("SmokingRow"),
    @XmlEnumValue("UpperDeckRow")
    UPPER_DECK_ROW("UpperDeckRow"),
    @XmlEnumValue("Preferred_PreferentialRow")
    PREFERRED_PREFERENTIAL_ROW("Preferred_PreferentialRow"),
    @XmlEnumValue("WingStart")
    WING_START("WingStart"),
    @XmlEnumValue("WingEnd")
    WING_END("WingEnd"),

    /**
     * It is strongly recommended that you submit a comment to have any of your extended list values permanently added to the OpenTravel specification to support maximum trading partner interoperability. http://www.opentraveldevelopersnetwork.com/specificationcomments/2/entercomment.html
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    EnumSeatRowCharacteristics(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumSeatRowCharacteristics fromValue(String v) {
        for (EnumSeatRowCharacteristics c: EnumSeatRowCharacteristics.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
