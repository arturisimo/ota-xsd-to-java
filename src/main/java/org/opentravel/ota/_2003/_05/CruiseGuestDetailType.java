
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides the detail data associated with the guest for a particular sailing.
 * 
 * &lt;p&gt;Clase Java para CruiseGuestDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CruiseGuestDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="SelectedFareCode" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FareGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ContactInfo" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GuestInfoGroup"/&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="GuestTransportation" type="{http://www.opentravel.org/OTA/2003/05}GuestTransportationType" maxOccurs="2" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="LoyaltyInfo" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="LinkedTraveler" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RelatedTravelerType"&amp;gt;
 *                 &amp;lt;attribute name="LinkTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TravelDocument" type="{http://www.opentravel.org/OTA/2003/05}DocumentType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SelectedDining" maxOccurs="2" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CruiseDiningGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SelectedInsurance" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CruiseInsuranceGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SelectedOptions" type="{http://www.opentravel.org/OTA/2003/05}AmenityOptionType" maxOccurs="20" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="SelectedPackages" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SelectedPackage" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CruisePackageType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="AirInfo" type="{http://www.opentravel.org/OTA/2003/05}AirInfoType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SelectedSpecialServices" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SelectedSpecialService" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AirAccommodations" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AirAccommodation" maxOccurs="10" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirInfoType"&amp;gt;
 *                           &amp;lt;attribute name="Comment" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                           &amp;lt;attribute name="AirAccommodationType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CruiseDocument" type="{http://www.opentravel.org/OTA/2003/05}DocumentHandlingType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}CruiseProfileType" maxOccurs="4" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="GuestExistsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="RepeatGuestIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CruiseGuestDetailType", propOrder = {
    "selectedFareCode",
    "contactInfo",
    "guestTransportation",
    "loyaltyInfo",
    "linkedTraveler",
    "travelDocument",
    "selectedDining",
    "selectedInsurance",
    "selectedOptions",
    "selectedPackages",
    "selectedSpecialServices",
    "airAccommodations",
    "cruiseDocument",
    "profiles"
})
public class CruiseGuestDetailType {

    @XmlElement(name = "SelectedFareCode")
    protected List<CruiseGuestDetailType.SelectedFareCode> selectedFareCode;
    @XmlElement(name = "ContactInfo")
    protected List<CruiseGuestDetailType.ContactInfo> contactInfo;
    @XmlElement(name = "GuestTransportation")
    protected List<GuestTransportationType> guestTransportation;
    @XmlElement(name = "LoyaltyInfo")
    protected List<CruiseGuestDetailType.LoyaltyInfo> loyaltyInfo;
    @XmlElement(name = "LinkedTraveler")
    protected List<CruiseGuestDetailType.LinkedTraveler> linkedTraveler;
    @XmlElement(name = "TravelDocument")
    protected List<DocumentType> travelDocument;
    @XmlElement(name = "SelectedDining")
    protected List<CruiseGuestDetailType.SelectedDining> selectedDining;
    @XmlElement(name = "SelectedInsurance")
    protected List<CruiseGuestDetailType.SelectedInsurance> selectedInsurance;
    @XmlElement(name = "SelectedOptions")
    protected List<AmenityOptionType> selectedOptions;
    @XmlElement(name = "SelectedPackages")
    protected CruiseGuestDetailType.SelectedPackages selectedPackages;
    @XmlElement(name = "SelectedSpecialServices")
    protected CruiseGuestDetailType.SelectedSpecialServices selectedSpecialServices;
    @XmlElement(name = "AirAccommodations")
    protected CruiseGuestDetailType.AirAccommodations airAccommodations;
    @XmlElement(name = "CruiseDocument")
    protected List<DocumentHandlingType> cruiseDocument;
    @XmlElement(name = "Profiles")
    protected List<CruiseProfileType> profiles;
    @XmlAttribute(name = "GuestExistsIndicator")
    protected Boolean guestExistsIndicator;
    @XmlAttribute(name = "RepeatGuestIndicator")
    protected Boolean repeatGuestIndicator;

    /**
     * Gets the value of the selectedFareCode property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedFareCode property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSelectedFareCode().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseGuestDetailType.SelectedFareCode }
     * 
     * 
     */
    public List<CruiseGuestDetailType.SelectedFareCode> getSelectedFareCode() {
        if (selectedFareCode == null) {
            selectedFareCode = new ArrayList<CruiseGuestDetailType.SelectedFareCode>();
        }
        return this.selectedFareCode;
    }

    /**
     * Gets the value of the contactInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contactInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getContactInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseGuestDetailType.ContactInfo }
     * 
     * 
     */
    public List<CruiseGuestDetailType.ContactInfo> getContactInfo() {
        if (contactInfo == null) {
            contactInfo = new ArrayList<CruiseGuestDetailType.ContactInfo>();
        }
        return this.contactInfo;
    }

    /**
     * Gets the value of the guestTransportation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the guestTransportation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getGuestTransportation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GuestTransportationType }
     * 
     * 
     */
    public List<GuestTransportationType> getGuestTransportation() {
        if (guestTransportation == null) {
            guestTransportation = new ArrayList<GuestTransportationType>();
        }
        return this.guestTransportation;
    }

    /**
     * Gets the value of the loyaltyInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyaltyInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getLoyaltyInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseGuestDetailType.LoyaltyInfo }
     * 
     * 
     */
    public List<CruiseGuestDetailType.LoyaltyInfo> getLoyaltyInfo() {
        if (loyaltyInfo == null) {
            loyaltyInfo = new ArrayList<CruiseGuestDetailType.LoyaltyInfo>();
        }
        return this.loyaltyInfo;
    }

    /**
     * Gets the value of the linkedTraveler property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the linkedTraveler property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getLinkedTraveler().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseGuestDetailType.LinkedTraveler }
     * 
     * 
     */
    public List<CruiseGuestDetailType.LinkedTraveler> getLinkedTraveler() {
        if (linkedTraveler == null) {
            linkedTraveler = new ArrayList<CruiseGuestDetailType.LinkedTraveler>();
        }
        return this.linkedTraveler;
    }

    /**
     * Gets the value of the travelDocument property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelDocument property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTravelDocument().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentType }
     * 
     * 
     */
    public List<DocumentType> getTravelDocument() {
        if (travelDocument == null) {
            travelDocument = new ArrayList<DocumentType>();
        }
        return this.travelDocument;
    }

    /**
     * Gets the value of the selectedDining property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedDining property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSelectedDining().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseGuestDetailType.SelectedDining }
     * 
     * 
     */
    public List<CruiseGuestDetailType.SelectedDining> getSelectedDining() {
        if (selectedDining == null) {
            selectedDining = new ArrayList<CruiseGuestDetailType.SelectedDining>();
        }
        return this.selectedDining;
    }

    /**
     * Gets the value of the selectedInsurance property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedInsurance property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSelectedInsurance().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseGuestDetailType.SelectedInsurance }
     * 
     * 
     */
    public List<CruiseGuestDetailType.SelectedInsurance> getSelectedInsurance() {
        if (selectedInsurance == null) {
            selectedInsurance = new ArrayList<CruiseGuestDetailType.SelectedInsurance>();
        }
        return this.selectedInsurance;
    }

    /**
     * Gets the value of the selectedOptions property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedOptions property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSelectedOptions().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AmenityOptionType }
     * 
     * 
     */
    public List<AmenityOptionType> getSelectedOptions() {
        if (selectedOptions == null) {
            selectedOptions = new ArrayList<AmenityOptionType>();
        }
        return this.selectedOptions;
    }

    /**
     * Obtiene el valor de la propiedad selectedPackages.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestDetailType.SelectedPackages }
     *     
     */
    public CruiseGuestDetailType.SelectedPackages getSelectedPackages() {
        return selectedPackages;
    }

    /**
     * Define el valor de la propiedad selectedPackages.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestDetailType.SelectedPackages }
     *     
     */
    public void setSelectedPackages(CruiseGuestDetailType.SelectedPackages value) {
        this.selectedPackages = value;
    }

    /**
     * Obtiene el valor de la propiedad selectedSpecialServices.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestDetailType.SelectedSpecialServices }
     *     
     */
    public CruiseGuestDetailType.SelectedSpecialServices getSelectedSpecialServices() {
        return selectedSpecialServices;
    }

    /**
     * Define el valor de la propiedad selectedSpecialServices.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestDetailType.SelectedSpecialServices }
     *     
     */
    public void setSelectedSpecialServices(CruiseGuestDetailType.SelectedSpecialServices value) {
        this.selectedSpecialServices = value;
    }

    /**
     * Obtiene el valor de la propiedad airAccommodations.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestDetailType.AirAccommodations }
     *     
     */
    public CruiseGuestDetailType.AirAccommodations getAirAccommodations() {
        return airAccommodations;
    }

    /**
     * Define el valor de la propiedad airAccommodations.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestDetailType.AirAccommodations }
     *     
     */
    public void setAirAccommodations(CruiseGuestDetailType.AirAccommodations value) {
        this.airAccommodations = value;
    }

    /**
     * Gets the value of the cruiseDocument property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cruiseDocument property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCruiseDocument().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentHandlingType }
     * 
     * 
     */
    public List<DocumentHandlingType> getCruiseDocument() {
        if (cruiseDocument == null) {
            cruiseDocument = new ArrayList<DocumentHandlingType>();
        }
        return this.cruiseDocument;
    }

    /**
     * Gets the value of the profiles property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the profiles property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getProfiles().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseProfileType }
     * 
     * 
     */
    public List<CruiseProfileType> getProfiles() {
        if (profiles == null) {
            profiles = new ArrayList<CruiseProfileType>();
        }
        return this.profiles;
    }

    /**
     * Obtiene el valor de la propiedad guestExistsIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGuestExistsIndicator() {
        return guestExistsIndicator;
    }

    /**
     * Define el valor de la propiedad guestExistsIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuestExistsIndicator(Boolean value) {
        this.guestExistsIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad repeatGuestIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRepeatGuestIndicator() {
        return repeatGuestIndicator;
    }

    /**
     * Define el valor de la propiedad repeatGuestIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRepeatGuestIndicator(Boolean value) {
        this.repeatGuestIndicator = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AirAccommodation" maxOccurs="10" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirInfoType"&amp;gt;
     *                 &amp;lt;attribute name="Comment" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;attribute name="AirAccommodationType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airAccommodation"
    })
    public static class AirAccommodations {

        @XmlElement(name = "AirAccommodation")
        protected List<CruiseGuestDetailType.AirAccommodations.AirAccommodation> airAccommodation;

        /**
         * Gets the value of the airAccommodation property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airAccommodation property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAirAccommodation().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CruiseGuestDetailType.AirAccommodations.AirAccommodation }
         * 
         * 
         */
        public List<CruiseGuestDetailType.AirAccommodations.AirAccommodation> getAirAccommodation() {
            if (airAccommodation == null) {
                airAccommodation = new ArrayList<CruiseGuestDetailType.AirAccommodations.AirAccommodation>();
            }
            return this.airAccommodation;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirInfoType"&amp;gt;
         *       &amp;lt;attribute name="Comment" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *       &amp;lt;attribute name="AirAccommodationType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AirAccommodation
            extends AirInfoType
        {

            @XmlAttribute(name = "Comment")
            protected String comment;
            @XmlAttribute(name = "AirAccommodationType")
            protected String airAccommodationType;

            /**
             * Obtiene el valor de la propiedad comment.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getComment() {
                return comment;
            }

            /**
             * Define el valor de la propiedad comment.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setComment(String value) {
                this.comment = value;
            }

            /**
             * Obtiene el valor de la propiedad airAccommodationType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAirAccommodationType() {
                return airAccommodationType;
            }

            /**
             * Define el valor de la propiedad airAccommodationType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAirAccommodationType(String value) {
                this.airAccommodationType = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}GuestInfoGroup"/&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ContactInfo
        extends ContactPersonType
    {

        @XmlAttribute(name = "GuestRefNumber")
        protected String guestRefNumber;
        @XmlAttribute(name = "Age")
        protected Integer age;
        @XmlAttribute(name = "Nationality")
        protected String nationality;
        @XmlAttribute(name = "GuestOccupation")
        protected String guestOccupation;
        @XmlAttribute(name = "PersonBirthDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar personBirthDate;
        @XmlAttribute(name = "Gender")
        protected String gender;
        @XmlAttribute(name = "LoyaltyMembershipID")
        protected String loyaltyMembershipID;
        @XmlAttribute(name = "LoyalLevel")
        protected String loyalLevel;
        @XmlAttribute(name = "LoyalLevelCode")
        protected Integer loyalLevelCode;

        /**
         * Obtiene el valor de la propiedad guestRefNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGuestRefNumber() {
            return guestRefNumber;
        }

        /**
         * Define el valor de la propiedad guestRefNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGuestRefNumber(String value) {
            this.guestRefNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad age.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAge() {
            return age;
        }

        /**
         * Define el valor de la propiedad age.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAge(Integer value) {
            this.age = value;
        }

        /**
         * Obtiene el valor de la propiedad nationality.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNationality() {
            return nationality;
        }

        /**
         * Define el valor de la propiedad nationality.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNationality(String value) {
            this.nationality = value;
        }

        /**
         * Obtiene el valor de la propiedad guestOccupation.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGuestOccupation() {
            return guestOccupation;
        }

        /**
         * Define el valor de la propiedad guestOccupation.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGuestOccupation(String value) {
            this.guestOccupation = value;
        }

        /**
         * Obtiene el valor de la propiedad personBirthDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getPersonBirthDate() {
            return personBirthDate;
        }

        /**
         * Define el valor de la propiedad personBirthDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setPersonBirthDate(XMLGregorianCalendar value) {
            this.personBirthDate = value;
        }

        /**
         * Obtiene el valor de la propiedad gender.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGender() {
            return gender;
        }

        /**
         * Define el valor de la propiedad gender.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGender(String value) {
            this.gender = value;
        }

        /**
         * Obtiene el valor de la propiedad loyaltyMembershipID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyaltyMembershipID() {
            return loyaltyMembershipID;
        }

        /**
         * Define el valor de la propiedad loyaltyMembershipID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyaltyMembershipID(String value) {
            this.loyaltyMembershipID = value;
        }

        /**
         * Obtiene el valor de la propiedad loyalLevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyalLevel() {
            return loyalLevel;
        }

        /**
         * Define el valor de la propiedad loyalLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyalLevel(String value) {
            this.loyalLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad loyalLevelCode.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getLoyalLevelCode() {
            return loyalLevelCode;
        }

        /**
         * Define el valor de la propiedad loyalLevelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setLoyalLevelCode(Integer value) {
            this.loyalLevelCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RelatedTravelerType"&amp;gt;
     *       &amp;lt;attribute name="LinkTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class LinkedTraveler
        extends RelatedTravelerType
    {

        @XmlAttribute(name = "LinkTypeCode")
        protected String linkTypeCode;

        /**
         * Obtiene el valor de la propiedad linkTypeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLinkTypeCode() {
            return linkTypeCode;
        }

        /**
         * Define el valor de la propiedad linkTypeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLinkTypeCode(String value) {
            this.linkTypeCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CustomerLoyaltyGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class LoyaltyInfo {

        @XmlAttribute(name = "ShareSynchInd")
        protected String shareSynchInd;
        @XmlAttribute(name = "ShareMarketInd")
        protected String shareMarketInd;
        @XmlAttribute(name = "ProgramID")
        protected String programID;
        @XmlAttribute(name = "MembershipID")
        protected String membershipID;
        @XmlAttribute(name = "TravelSector")
        protected String travelSector;
        @XmlAttribute(name = "VendorCode")
        protected List<String> vendorCode;
        @XmlAttribute(name = "PrimaryLoyaltyIndicator")
        protected Boolean primaryLoyaltyIndicator;
        @XmlAttribute(name = "AllianceLoyaltyLevelName")
        protected String allianceLoyaltyLevelName;
        @XmlAttribute(name = "CustomerType")
        protected String customerType;
        @XmlAttribute(name = "CustomerValue")
        protected String customerValue;
        @XmlAttribute(name = "Password")
        protected String password;
        @XmlAttribute(name = "LoyalLevel")
        protected String loyalLevel;
        @XmlAttribute(name = "LoyalLevelCode")
        protected Integer loyalLevelCode;
        @XmlAttribute(name = "SingleVendorInd")
        protected String singleVendorInd;
        @XmlAttribute(name = "SignupDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar signupDate;
        @XmlAttribute(name = "EffectiveDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar effectiveDate;
        @XmlAttribute(name = "ExpireDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar expireDate;
        @XmlAttribute(name = "ExpireDateExclusiveIndicator")
        protected Boolean expireDateExclusiveIndicator;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Obtiene el valor de la propiedad shareSynchInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShareSynchInd() {
            return shareSynchInd;
        }

        /**
         * Define el valor de la propiedad shareSynchInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShareSynchInd(String value) {
            this.shareSynchInd = value;
        }

        /**
         * Obtiene el valor de la propiedad shareMarketInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShareMarketInd() {
            return shareMarketInd;
        }

        /**
         * Define el valor de la propiedad shareMarketInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShareMarketInd(String value) {
            this.shareMarketInd = value;
        }

        /**
         * Obtiene el valor de la propiedad programID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProgramID() {
            return programID;
        }

        /**
         * Define el valor de la propiedad programID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProgramID(String value) {
            this.programID = value;
        }

        /**
         * Obtiene el valor de la propiedad membershipID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMembershipID() {
            return membershipID;
        }

        /**
         * Define el valor de la propiedad membershipID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMembershipID(String value) {
            this.membershipID = value;
        }

        /**
         * Obtiene el valor de la propiedad travelSector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelSector() {
            return travelSector;
        }

        /**
         * Define el valor de la propiedad travelSector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelSector(String value) {
            this.travelSector = value;
        }

        /**
         * Gets the value of the vendorCode property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vendorCode property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVendorCode().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getVendorCode() {
            if (vendorCode == null) {
                vendorCode = new ArrayList<String>();
            }
            return this.vendorCode;
        }

        /**
         * Obtiene el valor de la propiedad primaryLoyaltyIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPrimaryLoyaltyIndicator() {
            return primaryLoyaltyIndicator;
        }

        /**
         * Define el valor de la propiedad primaryLoyaltyIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPrimaryLoyaltyIndicator(Boolean value) {
            this.primaryLoyaltyIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad allianceLoyaltyLevelName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAllianceLoyaltyLevelName() {
            return allianceLoyaltyLevelName;
        }

        /**
         * Define el valor de la propiedad allianceLoyaltyLevelName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAllianceLoyaltyLevelName(String value) {
            this.allianceLoyaltyLevelName = value;
        }

        /**
         * Obtiene el valor de la propiedad customerType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerType() {
            return customerType;
        }

        /**
         * Define el valor de la propiedad customerType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerType(String value) {
            this.customerType = value;
        }

        /**
         * Obtiene el valor de la propiedad customerValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerValue() {
            return customerValue;
        }

        /**
         * Define el valor de la propiedad customerValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerValue(String value) {
            this.customerValue = value;
        }

        /**
         * Obtiene el valor de la propiedad password.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Define el valor de la propiedad password.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

        /**
         * Obtiene el valor de la propiedad loyalLevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoyalLevel() {
            return loyalLevel;
        }

        /**
         * Define el valor de la propiedad loyalLevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoyalLevel(String value) {
            this.loyalLevel = value;
        }

        /**
         * Obtiene el valor de la propiedad loyalLevelCode.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getLoyalLevelCode() {
            return loyalLevelCode;
        }

        /**
         * Define el valor de la propiedad loyalLevelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setLoyalLevelCode(Integer value) {
            this.loyalLevelCode = value;
        }

        /**
         * Obtiene el valor de la propiedad singleVendorInd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSingleVendorInd() {
            return singleVendorInd;
        }

        /**
         * Define el valor de la propiedad singleVendorInd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSingleVendorInd(String value) {
            this.singleVendorInd = value;
        }

        /**
         * Obtiene el valor de la propiedad signupDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSignupDate() {
            return signupDate;
        }

        /**
         * Define el valor de la propiedad signupDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSignupDate(XMLGregorianCalendar value) {
            this.signupDate = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Define el valor de la propiedad effectiveDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEffectiveDate(XMLGregorianCalendar value) {
            this.effectiveDate = value;
        }

        /**
         * Obtiene el valor de la propiedad expireDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpireDate() {
            return expireDate;
        }

        /**
         * Define el valor de la propiedad expireDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpireDate(XMLGregorianCalendar value) {
            this.expireDate = value;
        }

        /**
         * Obtiene el valor de la propiedad expireDateExclusiveIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isExpireDateExclusiveIndicator() {
            return expireDateExclusiveIndicator;
        }

        /**
         * Define el valor de la propiedad expireDateExclusiveIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExpireDateExclusiveIndicator(Boolean value) {
            this.expireDateExclusiveIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CruiseDiningGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SelectedDining {

        @XmlAttribute(name = "SmokingCode")
        protected String smokingCode;
        @XmlAttribute(name = "DiningRoom")
        protected String diningRoom;
        @XmlAttribute(name = "TableSize")
        protected String tableSize;
        @XmlAttribute(name = "AgeCode")
        protected String ageCode;
        @XmlAttribute(name = "Language")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String language;
        @XmlAttribute(name = "Sitting", required = true)
        protected String sitting;
        @XmlAttribute(name = "Status")
        protected String status;
        @XmlAttribute(name = "Preference")
        protected PreferLevelType preference;

        /**
         * Obtiene el valor de la propiedad smokingCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSmokingCode() {
            return smokingCode;
        }

        /**
         * Define el valor de la propiedad smokingCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSmokingCode(String value) {
            this.smokingCode = value;
        }

        /**
         * Obtiene el valor de la propiedad diningRoom.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDiningRoom() {
            return diningRoom;
        }

        /**
         * Define el valor de la propiedad diningRoom.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDiningRoom(String value) {
            this.diningRoom = value;
        }

        /**
         * Obtiene el valor de la propiedad tableSize.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTableSize() {
            return tableSize;
        }

        /**
         * Define el valor de la propiedad tableSize.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTableSize(String value) {
            this.tableSize = value;
        }

        /**
         * Obtiene el valor de la propiedad ageCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAgeCode() {
            return ageCode;
        }

        /**
         * Define el valor de la propiedad ageCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAgeCode(String value) {
            this.ageCode = value;
        }

        /**
         * Obtiene el valor de la propiedad language.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * Define el valor de la propiedad language.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Obtiene el valor de la propiedad sitting.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSitting() {
            return sitting;
        }

        /**
         * Define el valor de la propiedad sitting.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSitting(String value) {
            this.sitting = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Obtiene el valor de la propiedad preference.
         * 
         * @return
         *     possible object is
         *     {@link PreferLevelType }
         *     
         */
        public PreferLevelType getPreference() {
            return preference;
        }

        /**
         * Define el valor de la propiedad preference.
         * 
         * @param value
         *     allowed object is
         *     {@link PreferLevelType }
         *     
         */
        public void setPreference(PreferLevelType value) {
            this.preference = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FareGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SelectedFareCode {

        @XmlAttribute(name = "FareCode")
        protected String fareCode;
        @XmlAttribute(name = "GroupCode")
        protected String groupCode;

        /**
         * Obtiene el valor de la propiedad fareCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFareCode() {
            return fareCode;
        }

        /**
         * Define el valor de la propiedad fareCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFareCode(String value) {
            this.fareCode = value;
        }

        /**
         * Obtiene el valor de la propiedad groupCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupCode() {
            return groupCode;
        }

        /**
         * Define el valor de la propiedad groupCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupCode(String value) {
            this.groupCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CruiseInsuranceGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SelectedInsurance {

        @XmlAttribute(name = "InsuranceCode")
        protected String insuranceCode;
        @XmlAttribute(name = "SelectedOptionIndicator")
        protected Boolean selectedOptionIndicator;
        @XmlAttribute(name = "DefaultIndicator")
        protected Boolean defaultIndicator;
        @XmlAttribute(name = "Status")
        protected String status;

        /**
         * Obtiene el valor de la propiedad insuranceCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInsuranceCode() {
            return insuranceCode;
        }

        /**
         * Define el valor de la propiedad insuranceCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInsuranceCode(String value) {
            this.insuranceCode = value;
        }

        /**
         * Obtiene el valor de la propiedad selectedOptionIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSelectedOptionIndicator() {
            return selectedOptionIndicator;
        }

        /**
         * Define el valor de la propiedad selectedOptionIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSelectedOptionIndicator(Boolean value) {
            this.selectedOptionIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad defaultIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDefaultIndicator() {
            return defaultIndicator;
        }

        /**
         * Define el valor de la propiedad defaultIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDefaultIndicator(Boolean value) {
            this.defaultIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SelectedPackage" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CruisePackageType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="AirInfo" type="{http://www.opentravel.org/OTA/2003/05}AirInfoType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "selectedPackage"
    })
    public static class SelectedPackages {

        @XmlElement(name = "SelectedPackage")
        protected List<CruiseGuestDetailType.SelectedPackages.SelectedPackage> selectedPackage;

        /**
         * Gets the value of the selectedPackage property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedPackage property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSelectedPackage().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CruiseGuestDetailType.SelectedPackages.SelectedPackage }
         * 
         * 
         */
        public List<CruiseGuestDetailType.SelectedPackages.SelectedPackage> getSelectedPackage() {
            if (selectedPackage == null) {
                selectedPackage = new ArrayList<CruiseGuestDetailType.SelectedPackages.SelectedPackage>();
            }
            return this.selectedPackage;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CruisePackageType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="AirInfo" type="{http://www.opentravel.org/OTA/2003/05}AirInfoType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "airInfo"
        })
        public static class SelectedPackage
            extends CruisePackageType
        {

            @XmlElement(name = "AirInfo")
            protected AirInfoType airInfo;

            /**
             * Obtiene el valor de la propiedad airInfo.
             * 
             * @return
             *     possible object is
             *     {@link AirInfoType }
             *     
             */
            public AirInfoType getAirInfo() {
                return airInfo;
            }

            /**
             * Define el valor de la propiedad airInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link AirInfoType }
             *     
             */
            public void setAirInfo(AirInfoType value) {
                this.airInfo = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SelectedSpecialService" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "selectedSpecialService"
    })
    public static class SelectedSpecialServices {

        @XmlElement(name = "SelectedSpecialService")
        protected List<SpecialServiceType> selectedSpecialService;

        /**
         * Gets the value of the selectedSpecialService property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedSpecialService property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSelectedSpecialService().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link SpecialServiceType }
         * 
         * 
         */
        public List<SpecialServiceType> getSelectedSpecialService() {
            if (selectedSpecialService == null) {
                selectedSpecialService = new ArrayList<SpecialServiceType>();
            }
            return this.selectedSpecialService;
        }

    }

}
