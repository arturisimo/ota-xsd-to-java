
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides event site information.
 * 
 * &lt;p&gt;Clase Java para PostEventSiteReportType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PostEventSiteReportType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}RFP_ResponseDetailType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Event_ID" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                 &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Date" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Contact" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Amenities" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Amenity" type="{http://www.opentravel.org/OTA/2003/05}RoomAmenityPrefType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="VIP_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AttendeeInfo" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="EventDays" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="EventDay" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence minOccurs="0"&amp;gt;
 *                                                 &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Contact" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                 &amp;lt;/extension&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="AudioVisuals" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="AudioVisual" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                   &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="RoomSetup" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                         &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="SessionTimes" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                         &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="FoodAndBeverages" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="FoodAndBeverage" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                     &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
 *                                                                                 &amp;lt;complexType&amp;gt;
 *                                                                                   &amp;lt;complexContent&amp;gt;
 *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                       &amp;lt;sequence&amp;gt;
 *                                                                                         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                                                           &amp;lt;complexType&amp;gt;
 *                                                                                             &amp;lt;complexContent&amp;gt;
 *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                 &amp;lt;sequence&amp;gt;
 *                                                                                                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                                                                                 &amp;lt;/sequence&amp;gt;
 *                                                                                               &amp;lt;/restriction&amp;gt;
 *                                                                                             &amp;lt;/complexContent&amp;gt;
 *                                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                                         &amp;lt;/element&amp;gt;
 *                                                                                       &amp;lt;/sequence&amp;gt;
 *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                                       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                                                                     &amp;lt;/restriction&amp;gt;
 *                                                                                   &amp;lt;/complexContent&amp;gt;
 *                                                                                 &amp;lt;/complexType&amp;gt;
 *                                                                               &amp;lt;/element&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                   &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                                                                   &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                   &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                   &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                   &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Services" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Service" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
 *                                                                                 &amp;lt;complexType&amp;gt;
 *                                                                                   &amp;lt;complexContent&amp;gt;
 *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                       &amp;lt;sequence&amp;gt;
 *                                                                                         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                                                           &amp;lt;complexType&amp;gt;
 *                                                                                             &amp;lt;complexContent&amp;gt;
 *                                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                                 &amp;lt;sequence&amp;gt;
 *                                                                                                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                                                                                 &amp;lt;/sequence&amp;gt;
 *                                                                                               &amp;lt;/restriction&amp;gt;
 *                                                                                             &amp;lt;/complexContent&amp;gt;
 *                                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                                         &amp;lt;/element&amp;gt;
 *                                                                                       &amp;lt;/sequence&amp;gt;
 *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
 *                                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                                       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                                       &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                                     &amp;lt;/restriction&amp;gt;
 *                                                                                   &amp;lt;/complexContent&amp;gt;
 *                                                                                 &amp;lt;/complexType&amp;gt;
 *                                                                               &amp;lt;/element&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                     &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                                       &amp;lt;complexType&amp;gt;
 *                                                                         &amp;lt;complexContent&amp;gt;
 *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                             &amp;lt;sequence&amp;gt;
 *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                                                             &amp;lt;/sequence&amp;gt;
 *                                                                           &amp;lt;/restriction&amp;gt;
 *                                                                         &amp;lt;/complexContent&amp;gt;
 *                                                                       &amp;lt;/complexType&amp;gt;
 *                                                                     &amp;lt;/element&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                   &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                   &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                                                   &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                   &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="FunctionCharges" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="FunctionCharge" maxOccurs="unbounded"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
 *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="FunctionName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                               &amp;lt;attribute name="FunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                               &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                               &amp;lt;attribute name="LocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                               &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                               &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
 *                                               &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="RequiredKeyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="ReKeyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;attribute name="PostIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                               &amp;lt;attribute name="ExhibitionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="FirstEventDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Exhibition" maxOccurs="5" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ExhibitDetails" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ExhibitDetail" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DimensionGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="FoodAndBeverageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="FoodAndBeverageBoothQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="ExhibitorInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AdditionalDates" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="AdditionalDate" maxOccurs="unbounded"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="Type"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Contracted"/&amp;gt;
 *                                           &amp;lt;enumeration value="ContractorMoveIn"/&amp;gt;
 *                                           &amp;lt;enumeration value="ContractorMoveOut"/&amp;gt;
 *                                           &amp;lt;enumeration value="ExhibitorMoveIn"/&amp;gt;
 *                                           &amp;lt;enumeration value="ExhibitorMoveOut"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="Type"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Public"/&amp;gt;
 *                       &amp;lt;enumeration value="Private"/&amp;gt;
 *                       &amp;lt;enumeration value="PublicPrivate"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="GrossExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="NetExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="UnitOfMeasureCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="ExhibitQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="CompanyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="GeneralServiceContractorInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="SecuredAreaIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RoomBlocks" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomBlock" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ReservationMethod" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="StayDays" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;complexContent&amp;gt;
 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                               &amp;lt;sequence&amp;gt;
 *                                                 &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;sequence&amp;gt;
 *                                                           &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
 *                                                             &amp;lt;complexType&amp;gt;
 *                                                               &amp;lt;complexContent&amp;gt;
 *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                   &amp;lt;sequence&amp;gt;
 *                                                                     &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
 *                                                                     &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
 *                                                                   &amp;lt;/sequence&amp;gt;
 *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
 *                                                                   &amp;lt;attribute name="OccupancyRate"&amp;gt;
 *                                                                     &amp;lt;simpleType&amp;gt;
 *                                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                                                         &amp;lt;enumeration value="Flat"/&amp;gt;
 *                                                                         &amp;lt;enumeration value="Single"/&amp;gt;
 *                                                                         &amp;lt;enumeration value="Double"/&amp;gt;
 *                                                                         &amp;lt;enumeration value="Triple"/&amp;gt;
 *                                                                         &amp;lt;enumeration value="Quad"/&amp;gt;
 *                                                                       &amp;lt;/restriction&amp;gt;
 *                                                                     &amp;lt;/simpleType&amp;gt;
 *                                                                   &amp;lt;/attribute&amp;gt;
 *                                                                 &amp;lt;/restriction&amp;gt;
 *                                                               &amp;lt;/complexContent&amp;gt;
 *                                                             &amp;lt;/complexType&amp;gt;
 *                                                           &amp;lt;/element&amp;gt;
 *                                                         &amp;lt;/sequence&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;element name="RoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
 *                                                   &amp;lt;complexType&amp;gt;
 *                                                     &amp;lt;complexContent&amp;gt;
 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
 *                                                       &amp;lt;/restriction&amp;gt;
 *                                                     &amp;lt;/complexContent&amp;gt;
 *                                                   &amp;lt;/complexType&amp;gt;
 *                                                 &amp;lt;/element&amp;gt;
 *                                               &amp;lt;/sequence&amp;gt;
 *                                               &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
 *                                               &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                               &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                                               &amp;lt;attribute name="RoomTypeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                               &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                                               &amp;lt;attribute name="ContractedRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="FinalRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                             &amp;lt;/restriction&amp;gt;
 *                                           &amp;lt;/complexContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="TotalRoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="HousingProviderName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
 *                           &amp;lt;attribute name="InvBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="CompRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="StaffRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                           &amp;lt;attribute name="ContractedDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="CutoffDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="CutoffDateExercisedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="RequestedOversellPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="TotalBlockQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Transportations" type="{http://www.opentravel.org/OTA/2003/05}TransportationType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TaxExemptInfo" maxOccurs="3" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TaxExemptGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyCodeGroup"/&amp;gt;
 *       &amp;lt;attribute name="LocationCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="PrimaryFacilityIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="HeadquarterHotelIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="PropertyTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostEventSiteReportType", propOrder = {
    "responseType",
    "eventID",
    "date",
    "contacts",
    "attendeeInfo",
    "eventDays",
    "exhibition",
    "roomBlocks",
    "transportations",
    "taxExemptInfo",
    "comments"
})
public class PostEventSiteReportType {

    @XmlElement(name = "ResponseType")
    protected RFPResponseDetailType responseType;
    @XmlElement(name = "Event_ID")
    protected List<PostEventSiteReportType.EventID> eventID;
    @XmlElement(name = "Date")
    protected PostEventSiteReportType.Date date;
    @XmlElement(name = "Contacts")
    protected PostEventSiteReportType.Contacts contacts;
    @XmlElement(name = "AttendeeInfo")
    protected List<PostEventSiteReportType.AttendeeInfo> attendeeInfo;
    @XmlElement(name = "EventDays")
    protected PostEventSiteReportType.EventDays eventDays;
    @XmlElement(name = "Exhibition")
    protected List<PostEventSiteReportType.Exhibition> exhibition;
    @XmlElement(name = "RoomBlocks")
    protected PostEventSiteReportType.RoomBlocks roomBlocks;
    @XmlElement(name = "Transportations")
    protected TransportationType transportations;
    @XmlElement(name = "TaxExemptInfo")
    protected List<PostEventSiteReportType.TaxExemptInfo> taxExemptInfo;
    @XmlElement(name = "Comments")
    protected PostEventSiteReportType.Comments comments;
    @XmlAttribute(name = "LocationCategoryCode")
    protected String locationCategoryCode;
    @XmlAttribute(name = "PrimaryFacilityIndicator")
    protected Boolean primaryFacilityIndicator;
    @XmlAttribute(name = "HeadquarterHotelIndicator")
    protected Boolean headquarterHotelIndicator;
    @XmlAttribute(name = "PropertyTypeCode")
    protected String propertyTypeCode;
    @XmlAttribute(name = "ChainCode")
    protected String chainCode;
    @XmlAttribute(name = "BrandCode")
    protected String brandCode;
    @XmlAttribute(name = "HotelCode")
    protected String hotelCode;
    @XmlAttribute(name = "HotelCityCode")
    protected String hotelCityCode;
    @XmlAttribute(name = "HotelName")
    protected String hotelName;
    @XmlAttribute(name = "HotelCodeContext")
    protected String hotelCodeContext;
    @XmlAttribute(name = "ChainName")
    protected String chainName;
    @XmlAttribute(name = "BrandName")
    protected String brandName;
    @XmlAttribute(name = "AreaID")
    protected String areaID;
    @XmlAttribute(name = "TTIcode")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger ttIcode;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;
    @XmlAttribute(name = "DecimalPlaces")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger decimalPlaces;

    /**
     * Obtiene el valor de la propiedad responseType.
     * 
     * @return
     *     possible object is
     *     {@link RFPResponseDetailType }
     *     
     */
    public RFPResponseDetailType getResponseType() {
        return responseType;
    }

    /**
     * Define el valor de la propiedad responseType.
     * 
     * @param value
     *     allowed object is
     *     {@link RFPResponseDetailType }
     *     
     */
    public void setResponseType(RFPResponseDetailType value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the eventID property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventID property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getEventID().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PostEventSiteReportType.EventID }
     * 
     * 
     */
    public List<PostEventSiteReportType.EventID> getEventID() {
        if (eventID == null) {
            eventID = new ArrayList<PostEventSiteReportType.EventID>();
        }
        return this.eventID;
    }

    /**
     * Obtiene el valor de la propiedad date.
     * 
     * @return
     *     possible object is
     *     {@link PostEventSiteReportType.Date }
     *     
     */
    public PostEventSiteReportType.Date getDate() {
        return date;
    }

    /**
     * Define el valor de la propiedad date.
     * 
     * @param value
     *     allowed object is
     *     {@link PostEventSiteReportType.Date }
     *     
     */
    public void setDate(PostEventSiteReportType.Date value) {
        this.date = value;
    }

    /**
     * Obtiene el valor de la propiedad contacts.
     * 
     * @return
     *     possible object is
     *     {@link PostEventSiteReportType.Contacts }
     *     
     */
    public PostEventSiteReportType.Contacts getContacts() {
        return contacts;
    }

    /**
     * Define el valor de la propiedad contacts.
     * 
     * @param value
     *     allowed object is
     *     {@link PostEventSiteReportType.Contacts }
     *     
     */
    public void setContacts(PostEventSiteReportType.Contacts value) {
        this.contacts = value;
    }

    /**
     * Gets the value of the attendeeInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the attendeeInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAttendeeInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PostEventSiteReportType.AttendeeInfo }
     * 
     * 
     */
    public List<PostEventSiteReportType.AttendeeInfo> getAttendeeInfo() {
        if (attendeeInfo == null) {
            attendeeInfo = new ArrayList<PostEventSiteReportType.AttendeeInfo>();
        }
        return this.attendeeInfo;
    }

    /**
     * Obtiene el valor de la propiedad eventDays.
     * 
     * @return
     *     possible object is
     *     {@link PostEventSiteReportType.EventDays }
     *     
     */
    public PostEventSiteReportType.EventDays getEventDays() {
        return eventDays;
    }

    /**
     * Define el valor de la propiedad eventDays.
     * 
     * @param value
     *     allowed object is
     *     {@link PostEventSiteReportType.EventDays }
     *     
     */
    public void setEventDays(PostEventSiteReportType.EventDays value) {
        this.eventDays = value;
    }

    /**
     * Gets the value of the exhibition property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the exhibition property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getExhibition().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PostEventSiteReportType.Exhibition }
     * 
     * 
     */
    public List<PostEventSiteReportType.Exhibition> getExhibition() {
        if (exhibition == null) {
            exhibition = new ArrayList<PostEventSiteReportType.Exhibition>();
        }
        return this.exhibition;
    }

    /**
     * Obtiene el valor de la propiedad roomBlocks.
     * 
     * @return
     *     possible object is
     *     {@link PostEventSiteReportType.RoomBlocks }
     *     
     */
    public PostEventSiteReportType.RoomBlocks getRoomBlocks() {
        return roomBlocks;
    }

    /**
     * Define el valor de la propiedad roomBlocks.
     * 
     * @param value
     *     allowed object is
     *     {@link PostEventSiteReportType.RoomBlocks }
     *     
     */
    public void setRoomBlocks(PostEventSiteReportType.RoomBlocks value) {
        this.roomBlocks = value;
    }

    /**
     * Obtiene el valor de la propiedad transportations.
     * 
     * @return
     *     possible object is
     *     {@link TransportationType }
     *     
     */
    public TransportationType getTransportations() {
        return transportations;
    }

    /**
     * Define el valor de la propiedad transportations.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportationType }
     *     
     */
    public void setTransportations(TransportationType value) {
        this.transportations = value;
    }

    /**
     * Gets the value of the taxExemptInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxExemptInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTaxExemptInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PostEventSiteReportType.TaxExemptInfo }
     * 
     * 
     */
    public List<PostEventSiteReportType.TaxExemptInfo> getTaxExemptInfo() {
        if (taxExemptInfo == null) {
            taxExemptInfo = new ArrayList<PostEventSiteReportType.TaxExemptInfo>();
        }
        return this.taxExemptInfo;
    }

    /**
     * Obtiene el valor de la propiedad comments.
     * 
     * @return
     *     possible object is
     *     {@link PostEventSiteReportType.Comments }
     *     
     */
    public PostEventSiteReportType.Comments getComments() {
        return comments;
    }

    /**
     * Define el valor de la propiedad comments.
     * 
     * @param value
     *     allowed object is
     *     {@link PostEventSiteReportType.Comments }
     *     
     */
    public void setComments(PostEventSiteReportType.Comments value) {
        this.comments = value;
    }

    /**
     * Obtiene el valor de la propiedad locationCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationCategoryCode() {
        return locationCategoryCode;
    }

    /**
     * Define el valor de la propiedad locationCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationCategoryCode(String value) {
        this.locationCategoryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryFacilityIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrimaryFacilityIndicator() {
        return primaryFacilityIndicator;
    }

    /**
     * Define el valor de la propiedad primaryFacilityIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimaryFacilityIndicator(Boolean value) {
        this.primaryFacilityIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad headquarterHotelIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHeadquarterHotelIndicator() {
        return headquarterHotelIndicator;
    }

    /**
     * Define el valor de la propiedad headquarterHotelIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHeadquarterHotelIndicator(Boolean value) {
        this.headquarterHotelIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad propertyTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyTypeCode() {
        return propertyTypeCode;
    }

    /**
     * Define el valor de la propiedad propertyTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyTypeCode(String value) {
        this.propertyTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad chainCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainCode() {
        return chainCode;
    }

    /**
     * Define el valor de la propiedad chainCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainCode(String value) {
        this.chainCode = value;
    }

    /**
     * Obtiene el valor de la propiedad brandCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * Define el valor de la propiedad brandCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCode(String value) {
        this.brandCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Define el valor de la propiedad hotelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCityCode() {
        return hotelCityCode;
    }

    /**
     * Define el valor de la propiedad hotelCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCityCode(String value) {
        this.hotelCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * Define el valor de la propiedad hotelName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelName(String value) {
        this.hotelName = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCodeContext() {
        return hotelCodeContext;
    }

    /**
     * Define el valor de la propiedad hotelCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCodeContext(String value) {
        this.hotelCodeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad chainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainName() {
        return chainName;
    }

    /**
     * Define el valor de la propiedad chainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainName(String value) {
        this.chainName = value;
    }

    /**
     * Obtiene el valor de la propiedad brandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Define el valor de la propiedad brandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Obtiene el valor de la propiedad areaID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaID() {
        return areaID;
    }

    /**
     * Define el valor de la propiedad areaID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaID(String value) {
        this.areaID = value;
    }

    /**
     * Obtiene el valor de la propiedad ttIcode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTTIcode() {
        return ttIcode;
    }

    /**
     * Define el valor de la propiedad ttIcode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTTIcode(BigInteger value) {
        this.ttIcode = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad decimalPlaces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * Define el valor de la propiedad decimalPlaces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDecimalPlaces(BigInteger value) {
        this.decimalPlaces = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "comments"
    })
    public static class AttendeeInfo {

        @XmlElement(name = "Comments")
        protected PostEventSiteReportType.AttendeeInfo.Comments comments;
        @XmlAttribute(name = "TotalQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger totalQuantity;
        @XmlAttribute(name = "DomesticQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger domesticQuantity;
        @XmlAttribute(name = "InternationalQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger internationalQuantity;
        @XmlAttribute(name = "PreRegisteredQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger preRegisteredQuantity;
        @XmlAttribute(name = "OnsiteRegisteredQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger onsiteRegisteredQuantity;
        @XmlAttribute(name = "NoShowQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger noShowQuantity;
        @XmlAttribute(name = "ExhibitorQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger exhibitorQuantity;
        @XmlAttribute(name = "QuantityType")
        protected String quantityType;

        /**
         * Obtiene el valor de la propiedad comments.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.AttendeeInfo.Comments }
         *     
         */
        public PostEventSiteReportType.AttendeeInfo.Comments getComments() {
            return comments;
        }

        /**
         * Define el valor de la propiedad comments.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.AttendeeInfo.Comments }
         *     
         */
        public void setComments(PostEventSiteReportType.AttendeeInfo.Comments value) {
            this.comments = value;
        }

        /**
         * Obtiene el valor de la propiedad totalQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalQuantity() {
            return totalQuantity;
        }

        /**
         * Define el valor de la propiedad totalQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalQuantity(BigInteger value) {
            this.totalQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad domesticQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDomesticQuantity() {
            return domesticQuantity;
        }

        /**
         * Define el valor de la propiedad domesticQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDomesticQuantity(BigInteger value) {
            this.domesticQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad internationalQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getInternationalQuantity() {
            return internationalQuantity;
        }

        /**
         * Define el valor de la propiedad internationalQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setInternationalQuantity(BigInteger value) {
            this.internationalQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad preRegisteredQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPreRegisteredQuantity() {
            return preRegisteredQuantity;
        }

        /**
         * Define el valor de la propiedad preRegisteredQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPreRegisteredQuantity(BigInteger value) {
            this.preRegisteredQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad onsiteRegisteredQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getOnsiteRegisteredQuantity() {
            return onsiteRegisteredQuantity;
        }

        /**
         * Define el valor de la propiedad onsiteRegisteredQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setOnsiteRegisteredQuantity(BigInteger value) {
            this.onsiteRegisteredQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad noShowQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNoShowQuantity() {
            return noShowQuantity;
        }

        /**
         * Define el valor de la propiedad noShowQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNoShowQuantity(BigInteger value) {
            this.noShowQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad exhibitorQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getExhibitorQuantity() {
            return exhibitorQuantity;
        }

        /**
         * Define el valor de la propiedad exhibitorQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setExhibitorQuantity(BigInteger value) {
            this.exhibitorQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad quantityType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuantityType() {
            return quantityType;
        }

        /**
         * Define el valor de la propiedad quantityType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuantityType(String value) {
            this.quantityType = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comment"
        })
        public static class Comments {

            @XmlElement(name = "Comment", required = true)
            protected List<ParagraphType> comment;

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<ParagraphType>();
                }
                return this.comment;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "comment"
    })
    public static class Comments {

        @XmlElement(name = "Comment")
        protected List<ParagraphType> comment;

        /**
         * Gets the value of the comment property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getComment().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ParagraphType }
         * 
         * 
         */
        public List<ParagraphType> getComment() {
            if (comment == null) {
                comment = new ArrayList<ParagraphType>();
            }
            return this.comment;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Contact" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Amenities" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Amenity" type="{http://www.opentravel.org/OTA/2003/05}RoomAmenityPrefType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="VIP_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contact"
    })
    public static class Contacts {

        @XmlElement(name = "Contact", required = true)
        protected List<PostEventSiteReportType.Contacts.Contact> contact;

        /**
         * Gets the value of the contact property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getContact().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PostEventSiteReportType.Contacts.Contact }
         * 
         * 
         */
        public List<PostEventSiteReportType.Contacts.Contact> getContact() {
            if (contact == null) {
                contact = new ArrayList<PostEventSiteReportType.Contacts.Contact>();
            }
            return this.contact;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Amenities" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Amenity" type="{http://www.opentravel.org/OTA/2003/05}RoomAmenityPrefType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="VIP_Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="ArrivalDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="DepartureDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "amenities",
            "comments"
        })
        public static class Contact
            extends ContactPersonType
        {

            @XmlElement(name = "Amenities")
            protected PostEventSiteReportType.Contacts.Contact.Amenities amenities;
            @XmlElement(name = "Comments")
            protected PostEventSiteReportType.Contacts.Contact.Comments comments;
            @XmlAttribute(name = "VIP_Indicator")
            protected Boolean vipIndicator;
            @XmlAttribute(name = "ArrivalDate")
            protected String arrivalDate;
            @XmlAttribute(name = "DepartureDate")
            protected String departureDate;

            /**
             * Obtiene el valor de la propiedad amenities.
             * 
             * @return
             *     possible object is
             *     {@link PostEventSiteReportType.Contacts.Contact.Amenities }
             *     
             */
            public PostEventSiteReportType.Contacts.Contact.Amenities getAmenities() {
                return amenities;
            }

            /**
             * Define el valor de la propiedad amenities.
             * 
             * @param value
             *     allowed object is
             *     {@link PostEventSiteReportType.Contacts.Contact.Amenities }
             *     
             */
            public void setAmenities(PostEventSiteReportType.Contacts.Contact.Amenities value) {
                this.amenities = value;
            }

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link PostEventSiteReportType.Contacts.Contact.Comments }
             *     
             */
            public PostEventSiteReportType.Contacts.Contact.Comments getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link PostEventSiteReportType.Contacts.Contact.Comments }
             *     
             */
            public void setComments(PostEventSiteReportType.Contacts.Contact.Comments value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad vipIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isVIPIndicator() {
                return vipIndicator;
            }

            /**
             * Define el valor de la propiedad vipIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setVIPIndicator(Boolean value) {
                this.vipIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad arrivalDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getArrivalDate() {
                return arrivalDate;
            }

            /**
             * Define el valor de la propiedad arrivalDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setArrivalDate(String value) {
                this.arrivalDate = value;
            }

            /**
             * Obtiene el valor de la propiedad departureDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDepartureDate() {
                return departureDate;
            }

            /**
             * Define el valor de la propiedad departureDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDepartureDate(String value) {
                this.departureDate = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Amenity" type="{http://www.opentravel.org/OTA/2003/05}RoomAmenityPrefType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amenity"
            })
            public static class Amenities {

                @XmlElement(name = "Amenity", required = true)
                protected List<RoomAmenityPrefType> amenity;

                /**
                 * Gets the value of the amenity property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the amenity property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getAmenity().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link RoomAmenityPrefType }
                 * 
                 * 
                 */
                public List<RoomAmenityPrefType> getAmenity() {
                    if (amenity == null) {
                        amenity = new ArrayList<RoomAmenityPrefType>();
                    }
                    return this.amenity;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class Comments {

                @XmlElement(name = "Comment", required = true)
                protected List<ParagraphType> comment;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ParagraphType }
                 * 
                 * 
                 */
                public List<ParagraphType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<ParagraphType>();
                    }
                    return this.comment;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Date {

        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="EventDay" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence minOccurs="0"&amp;gt;
     *                                       &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Contact" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="AudioVisuals" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="AudioVisual" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="RoomSetup" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                               &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="SessionTimes" maxOccurs="5" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                               &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="FoodAndBeverages" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="FoodAndBeverage" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
     *                                                                       &amp;lt;complexType&amp;gt;
     *                                                                         &amp;lt;complexContent&amp;gt;
     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                             &amp;lt;sequence&amp;gt;
     *                                                                               &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                                                                 &amp;lt;complexType&amp;gt;
     *                                                                                   &amp;lt;complexContent&amp;gt;
     *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                       &amp;lt;sequence&amp;gt;
     *                                                                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                                                                       &amp;lt;/sequence&amp;gt;
     *                                                                                     &amp;lt;/restriction&amp;gt;
     *                                                                                   &amp;lt;/complexContent&amp;gt;
     *                                                                                 &amp;lt;/complexType&amp;gt;
     *                                                                               &amp;lt;/element&amp;gt;
     *                                                                             &amp;lt;/sequence&amp;gt;
     *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                                             &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                                                                             &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                             &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                         &amp;lt;/complexContent&amp;gt;
     *                                                                       &amp;lt;/complexType&amp;gt;
     *                                                                     &amp;lt;/element&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                         &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                         &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                         &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Services" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Service" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
     *                                                                       &amp;lt;complexType&amp;gt;
     *                                                                         &amp;lt;complexContent&amp;gt;
     *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                             &amp;lt;sequence&amp;gt;
     *                                                                               &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                                                                 &amp;lt;complexType&amp;gt;
     *                                                                                   &amp;lt;complexContent&amp;gt;
     *                                                                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                                       &amp;lt;sequence&amp;gt;
     *                                                                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                                                                       &amp;lt;/sequence&amp;gt;
     *                                                                                     &amp;lt;/restriction&amp;gt;
     *                                                                                   &amp;lt;/complexContent&amp;gt;
     *                                                                                 &amp;lt;/complexType&amp;gt;
     *                                                                               &amp;lt;/element&amp;gt;
     *                                                                             &amp;lt;/sequence&amp;gt;
     *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
     *                                                                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                                             &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                             &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                                                                             &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                             &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                                           &amp;lt;/restriction&amp;gt;
     *                                                                         &amp;lt;/complexContent&amp;gt;
     *                                                                       &amp;lt;/complexType&amp;gt;
     *                                                                     &amp;lt;/element&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;sequence&amp;gt;
     *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                                                   &amp;lt;/sequence&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                         &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                         &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                                         &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="FunctionCharges" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="FunctionCharge" maxOccurs="unbounded"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="FunctionName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                     &amp;lt;attribute name="FunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                     &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                                     &amp;lt;attribute name="LocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                     &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                     &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
     *                                     &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="RequiredKeyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="ReKeyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="PostIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                     &amp;lt;attribute name="ExhibitionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="FirstEventDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "eventDay"
    })
    public static class EventDays {

        @XmlElement(name = "EventDay", required = true)
        protected List<PostEventSiteReportType.EventDays.EventDay> eventDay;
        @XmlAttribute(name = "FirstEventDayOfWeek")
        protected DayOfWeekType firstEventDayOfWeek;

        /**
         * Gets the value of the eventDay property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventDay property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getEventDay().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PostEventSiteReportType.EventDays.EventDay }
         * 
         * 
         */
        public List<PostEventSiteReportType.EventDays.EventDay> getEventDay() {
            if (eventDay == null) {
                eventDay = new ArrayList<PostEventSiteReportType.EventDays.EventDay>();
            }
            return this.eventDay;
        }

        /**
         * Obtiene el valor de la propiedad firstEventDayOfWeek.
         * 
         * @return
         *     possible object is
         *     {@link DayOfWeekType }
         *     
         */
        public DayOfWeekType getFirstEventDayOfWeek() {
            return firstEventDayOfWeek;
        }

        /**
         * Define el valor de la propiedad firstEventDayOfWeek.
         * 
         * @param value
         *     allowed object is
         *     {@link DayOfWeekType }
         *     
         */
        public void setFirstEventDayOfWeek(DayOfWeekType value) {
            this.firstEventDayOfWeek = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="EventDayFunctions" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence minOccurs="0"&amp;gt;
         *                             &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Contact" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="AudioVisuals" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="AudioVisual" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                               &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="RoomSetup" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                     &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="SessionTimes" maxOccurs="5" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                     &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="FoodAndBeverages" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="FoodAndBeverage" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
         *                                                             &amp;lt;complexType&amp;gt;
         *                                                               &amp;lt;complexContent&amp;gt;
         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                   &amp;lt;sequence&amp;gt;
         *                                                                     &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                                                                       &amp;lt;complexType&amp;gt;
         *                                                                         &amp;lt;complexContent&amp;gt;
         *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                             &amp;lt;sequence&amp;gt;
         *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                                                                             &amp;lt;/sequence&amp;gt;
         *                                                                           &amp;lt;/restriction&amp;gt;
         *                                                                         &amp;lt;/complexContent&amp;gt;
         *                                                                       &amp;lt;/complexType&amp;gt;
         *                                                                     &amp;lt;/element&amp;gt;
         *                                                                   &amp;lt;/sequence&amp;gt;
         *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                                                   &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                                                                   &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                   &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                                                                 &amp;lt;/restriction&amp;gt;
         *                                                               &amp;lt;/complexContent&amp;gt;
         *                                                             &amp;lt;/complexType&amp;gt;
         *                                                           &amp;lt;/element&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                               &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *                                               &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                               &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                               &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                               &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Services" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Service" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
         *                                                             &amp;lt;complexType&amp;gt;
         *                                                               &amp;lt;complexContent&amp;gt;
         *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                   &amp;lt;sequence&amp;gt;
         *                                                                     &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                                                                       &amp;lt;complexType&amp;gt;
         *                                                                         &amp;lt;complexContent&amp;gt;
         *                                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                                             &amp;lt;sequence&amp;gt;
         *                                                                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                                                                             &amp;lt;/sequence&amp;gt;
         *                                                                           &amp;lt;/restriction&amp;gt;
         *                                                                         &amp;lt;/complexContent&amp;gt;
         *                                                                       &amp;lt;/complexType&amp;gt;
         *                                                                     &amp;lt;/element&amp;gt;
         *                                                                   &amp;lt;/sequence&amp;gt;
         *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
         *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                                                   &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                   &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                                                                   &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                                   &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                                                 &amp;lt;/restriction&amp;gt;
         *                                                               &amp;lt;/complexContent&amp;gt;
         *                                                             &amp;lt;/complexType&amp;gt;
         *                                                           &amp;lt;/element&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;sequence&amp;gt;
         *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                                                         &amp;lt;/sequence&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                               &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                               &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                               &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="FunctionCharges" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="FunctionCharge" maxOccurs="unbounded"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="FunctionName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                           &amp;lt;attribute name="FunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                           &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                           &amp;lt;attribute name="LocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                           &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                           &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
         *                           &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="RequiredKeyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="ReKeyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="PostIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                           &amp;lt;attribute name="ExhibitionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "eventDayFunctions"
        })
        public static class EventDay {

            @XmlElement(name = "EventDayFunctions")
            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions eventDayFunctions;
            @XmlAttribute(name = "DayNumber", required = true)
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger dayNumber;
            @XmlAttribute(name = "DayType")
            protected EventDayType dayType;

            /**
             * Obtiene el valor de la propiedad eventDayFunctions.
             * 
             * @return
             *     possible object is
             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions }
             *     
             */
            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions getEventDayFunctions() {
                return eventDayFunctions;
            }

            /**
             * Define el valor de la propiedad eventDayFunctions.
             * 
             * @param value
             *     allowed object is
             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions }
             *     
             */
            public void setEventDayFunctions(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions value) {
                this.eventDayFunctions = value;
            }

            /**
             * Obtiene el valor de la propiedad dayNumber.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDayNumber() {
                return dayNumber;
            }

            /**
             * Define el valor de la propiedad dayNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDayNumber(BigInteger value) {
                this.dayNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad dayType.
             * 
             * @return
             *     possible object is
             *     {@link EventDayType }
             *     
             */
            public EventDayType getDayType() {
                return dayType;
            }

            /**
             * Define el valor de la propiedad dayType.
             * 
             * @param value
             *     allowed object is
             *     {@link EventDayType }
             *     
             */
            public void setDayType(EventDayType value) {
                this.dayType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="EventDayFunction" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence minOccurs="0"&amp;gt;
             *                   &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Contact" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="AudioVisuals" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="AudioVisual" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                     &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="RoomSetup" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                           &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="SessionTimes" maxOccurs="5" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                           &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="FoodAndBeverages" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="FoodAndBeverage" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
             *                                                   &amp;lt;complexType&amp;gt;
             *                                                     &amp;lt;complexContent&amp;gt;
             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                         &amp;lt;sequence&amp;gt;
             *                                                           &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                                                             &amp;lt;complexType&amp;gt;
             *                                                               &amp;lt;complexContent&amp;gt;
             *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                   &amp;lt;sequence&amp;gt;
             *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                                                                   &amp;lt;/sequence&amp;gt;
             *                                                                 &amp;lt;/restriction&amp;gt;
             *                                                               &amp;lt;/complexContent&amp;gt;
             *                                                             &amp;lt;/complexType&amp;gt;
             *                                                           &amp;lt;/element&amp;gt;
             *                                                         &amp;lt;/sequence&amp;gt;
             *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                                         &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *                                                         &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                         &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *                                                       &amp;lt;/restriction&amp;gt;
             *                                                     &amp;lt;/complexContent&amp;gt;
             *                                                   &amp;lt;/complexType&amp;gt;
             *                                                 &amp;lt;/element&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                     &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
             *                                     &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                     &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                     &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                     &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Services" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Service" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
             *                                                   &amp;lt;complexType&amp;gt;
             *                                                     &amp;lt;complexContent&amp;gt;
             *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                         &amp;lt;sequence&amp;gt;
             *                                                           &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                                                             &amp;lt;complexType&amp;gt;
             *                                                               &amp;lt;complexContent&amp;gt;
             *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                                                   &amp;lt;sequence&amp;gt;
             *                                                                     &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                                                                   &amp;lt;/sequence&amp;gt;
             *                                                                 &amp;lt;/restriction&amp;gt;
             *                                                               &amp;lt;/complexContent&amp;gt;
             *                                                             &amp;lt;/complexType&amp;gt;
             *                                                           &amp;lt;/element&amp;gt;
             *                                                         &amp;lt;/sequence&amp;gt;
             *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
             *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                                         &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                         &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *                                                         &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                                         &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                                       &amp;lt;/restriction&amp;gt;
             *                                                     &amp;lt;/complexContent&amp;gt;
             *                                                   &amp;lt;/complexType&amp;gt;
             *                                                 &amp;lt;/element&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;sequence&amp;gt;
             *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                                               &amp;lt;/sequence&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                     &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                     &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                     &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="FunctionCharges" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="FunctionCharge" maxOccurs="unbounded"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="FunctionName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                 &amp;lt;attribute name="FunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                 &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *                 &amp;lt;attribute name="LocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *                 &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                 &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
             *                 &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="RequiredKeyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="ReKeyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="PostIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *                 &amp;lt;attribute name="ExhibitionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "eventDayFunction"
            })
            public static class EventDayFunctions {

                @XmlElement(name = "EventDayFunction", required = true)
                protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction> eventDayFunction;

                /**
                 * Gets the value of the eventDayFunction property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the eventDayFunction property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getEventDayFunction().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction }
                 * 
                 * 
                 */
                public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction> getEventDayFunction() {
                    if (eventDayFunction == null) {
                        eventDayFunction = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction>();
                    }
                    return this.eventDayFunction;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence minOccurs="0"&amp;gt;
                 *         &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Contact" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="AudioVisuals" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="AudioVisual" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                           &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="RoomSetup" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                 &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="SessionTimes" maxOccurs="5" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                 &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="FoodAndBeverages" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="FoodAndBeverage" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
                 *                                         &amp;lt;complexType&amp;gt;
                 *                                           &amp;lt;complexContent&amp;gt;
                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                               &amp;lt;sequence&amp;gt;
                 *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *                                                   &amp;lt;complexType&amp;gt;
                 *                                                     &amp;lt;complexContent&amp;gt;
                 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                         &amp;lt;sequence&amp;gt;
                 *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *                                                         &amp;lt;/sequence&amp;gt;
                 *                                                       &amp;lt;/restriction&amp;gt;
                 *                                                     &amp;lt;/complexContent&amp;gt;
                 *                                                   &amp;lt;/complexType&amp;gt;
                 *                                                 &amp;lt;/element&amp;gt;
                 *                                               &amp;lt;/sequence&amp;gt;
                 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                                               &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                 *                                               &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                               &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                 *                                             &amp;lt;/restriction&amp;gt;
                 *                                           &amp;lt;/complexContent&amp;gt;
                 *                                         &amp;lt;/complexType&amp;gt;
                 *                                       &amp;lt;/element&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                           &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
                 *                           &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                           &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                           &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                           &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Services" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Service" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
                 *                                         &amp;lt;complexType&amp;gt;
                 *                                           &amp;lt;complexContent&amp;gt;
                 *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                               &amp;lt;sequence&amp;gt;
                 *                                                 &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *                                                   &amp;lt;complexType&amp;gt;
                 *                                                     &amp;lt;complexContent&amp;gt;
                 *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                                         &amp;lt;sequence&amp;gt;
                 *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *                                                         &amp;lt;/sequence&amp;gt;
                 *                                                       &amp;lt;/restriction&amp;gt;
                 *                                                     &amp;lt;/complexContent&amp;gt;
                 *                                                   &amp;lt;/complexType&amp;gt;
                 *                                                 &amp;lt;/element&amp;gt;
                 *                                               &amp;lt;/sequence&amp;gt;
                 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                 *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                               &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                 *                                               &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                               &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                                             &amp;lt;/restriction&amp;gt;
                 *                                           &amp;lt;/complexContent&amp;gt;
                 *                                         &amp;lt;/complexType&amp;gt;
                 *                                       &amp;lt;/element&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;sequence&amp;gt;
                 *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *                                     &amp;lt;/sequence&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                           &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                           &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                           &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="FunctionCharges" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="FunctionCharge" maxOccurs="unbounded"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="FunctionName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *       &amp;lt;attribute name="FunctionType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *       &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                 *       &amp;lt;attribute name="LocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *       &amp;lt;attribute name="TwentyFourHourHoldInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *       &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                 *       &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="RequiredKeyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="ReKeyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="PostIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *       &amp;lt;attribute name="ExhibitionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "contacts",
                    "audioVisuals",
                    "roomSetup",
                    "sessionTimes",
                    "foodAndBeverages",
                    "services",
                    "functionCharges",
                    "comments"
                })
                public static class EventDayFunction {

                    @XmlElement(name = "Contacts")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts contacts;
                    @XmlElement(name = "AudioVisuals")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals audioVisuals;
                    @XmlElement(name = "RoomSetup")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.RoomSetup roomSetup;
                    @XmlElement(name = "SessionTimes")
                    protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.SessionTimes> sessionTimes;
                    @XmlElement(name = "FoodAndBeverages")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages foodAndBeverages;
                    @XmlElement(name = "Services")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services services;
                    @XmlElement(name = "FunctionCharges")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges functionCharges;
                    @XmlElement(name = "Comments")
                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Comments comments;
                    @XmlAttribute(name = "FunctionName")
                    protected String functionName;
                    @XmlAttribute(name = "FunctionType")
                    protected String functionType;
                    @XmlAttribute(name = "LocationName")
                    protected String locationName;
                    @XmlAttribute(name = "LocationID")
                    protected String locationID;
                    @XmlAttribute(name = "TwentyFourHourHoldInd")
                    protected Boolean twentyFourHourHoldInd;
                    @XmlAttribute(name = "StartTime")
                    @XmlSchemaType(name = "time")
                    protected XMLGregorianCalendar startTime;
                    @XmlAttribute(name = "EndTime")
                    @XmlSchemaType(name = "time")
                    protected XMLGregorianCalendar endTime;
                    @XmlAttribute(name = "AttendeeQuantity")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger attendeeQuantity;
                    @XmlAttribute(name = "RequiredKeyQuantity")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger requiredKeyQuantity;
                    @XmlAttribute(name = "ReKeyIndicator")
                    protected Boolean reKeyIndicator;
                    @XmlAttribute(name = "PostIndicator")
                    protected Boolean postIndicator;
                    @XmlAttribute(name = "ID")
                    protected String id;
                    @XmlAttribute(name = "ExhibitionIndicator")
                    protected Boolean exhibitionIndicator;

                    /**
                     * Obtiene el valor de la propiedad contacts.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts getContacts() {
                        return contacts;
                    }

                    /**
                     * Define el valor de la propiedad contacts.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts }
                     *     
                     */
                    public void setContacts(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts value) {
                        this.contacts = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad audioVisuals.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals getAudioVisuals() {
                        return audioVisuals;
                    }

                    /**
                     * Define el valor de la propiedad audioVisuals.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals }
                     *     
                     */
                    public void setAudioVisuals(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals value) {
                        this.audioVisuals = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad roomSetup.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.RoomSetup }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.RoomSetup getRoomSetup() {
                        return roomSetup;
                    }

                    /**
                     * Define el valor de la propiedad roomSetup.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.RoomSetup }
                     *     
                     */
                    public void setRoomSetup(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.RoomSetup value) {
                        this.roomSetup = value;
                    }

                    /**
                     * Gets the value of the sessionTimes property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the sessionTimes property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getSessionTimes().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.SessionTimes }
                     * 
                     * 
                     */
                    public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.SessionTimes> getSessionTimes() {
                        if (sessionTimes == null) {
                            sessionTimes = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.SessionTimes>();
                        }
                        return this.sessionTimes;
                    }

                    /**
                     * Obtiene el valor de la propiedad foodAndBeverages.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages getFoodAndBeverages() {
                        return foodAndBeverages;
                    }

                    /**
                     * Define el valor de la propiedad foodAndBeverages.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages }
                     *     
                     */
                    public void setFoodAndBeverages(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages value) {
                        this.foodAndBeverages = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad services.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services getServices() {
                        return services;
                    }

                    /**
                     * Define el valor de la propiedad services.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services }
                     *     
                     */
                    public void setServices(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services value) {
                        this.services = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad functionCharges.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges getFunctionCharges() {
                        return functionCharges;
                    }

                    /**
                     * Define el valor de la propiedad functionCharges.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges }
                     *     
                     */
                    public void setFunctionCharges(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges value) {
                        this.functionCharges = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad comments.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Comments }
                     *     
                     */
                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Comments getComments() {
                        return comments;
                    }

                    /**
                     * Define el valor de la propiedad comments.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Comments }
                     *     
                     */
                    public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Comments value) {
                        this.comments = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad functionName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFunctionName() {
                        return functionName;
                    }

                    /**
                     * Define el valor de la propiedad functionName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFunctionName(String value) {
                        this.functionName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad functionType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFunctionType() {
                        return functionType;
                    }

                    /**
                     * Define el valor de la propiedad functionType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFunctionType(String value) {
                        this.functionType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad locationName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLocationName() {
                        return locationName;
                    }

                    /**
                     * Define el valor de la propiedad locationName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLocationName(String value) {
                        this.locationName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad locationID.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLocationID() {
                        return locationID;
                    }

                    /**
                     * Define el valor de la propiedad locationID.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLocationID(String value) {
                        this.locationID = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad twentyFourHourHoldInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isTwentyFourHourHoldInd() {
                        return twentyFourHourHoldInd;
                    }

                    /**
                     * Define el valor de la propiedad twentyFourHourHoldInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setTwentyFourHourHoldInd(Boolean value) {
                        this.twentyFourHourHoldInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad startTime.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getStartTime() {
                        return startTime;
                    }

                    /**
                     * Define el valor de la propiedad startTime.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setStartTime(XMLGregorianCalendar value) {
                        this.startTime = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad endTime.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getEndTime() {
                        return endTime;
                    }

                    /**
                     * Define el valor de la propiedad endTime.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setEndTime(XMLGregorianCalendar value) {
                        this.endTime = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad attendeeQuantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getAttendeeQuantity() {
                        return attendeeQuantity;
                    }

                    /**
                     * Define el valor de la propiedad attendeeQuantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setAttendeeQuantity(BigInteger value) {
                        this.attendeeQuantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad requiredKeyQuantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getRequiredKeyQuantity() {
                        return requiredKeyQuantity;
                    }

                    /**
                     * Define el valor de la propiedad requiredKeyQuantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setRequiredKeyQuantity(BigInteger value) {
                        this.requiredKeyQuantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad reKeyIndicator.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isReKeyIndicator() {
                        return reKeyIndicator;
                    }

                    /**
                     * Define el valor de la propiedad reKeyIndicator.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setReKeyIndicator(Boolean value) {
                        this.reKeyIndicator = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad postIndicator.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isPostIndicator() {
                        return postIndicator;
                    }

                    /**
                     * Define el valor de la propiedad postIndicator.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setPostIndicator(Boolean value) {
                        this.postIndicator = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad id.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getID() {
                        return id;
                    }

                    /**
                     * Define el valor de la propiedad id.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setID(String value) {
                        this.id = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad exhibitionIndicator.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isExhibitionIndicator() {
                        return exhibitionIndicator;
                    }

                    /**
                     * Define el valor de la propiedad exhibitionIndicator.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setExhibitionIndicator(Boolean value) {
                        this.exhibitionIndicator = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="AudioVisual" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                 &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "audioVisual"
                    })
                    public static class AudioVisuals {

                        @XmlElement(name = "AudioVisual", required = true)
                        protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual> audioVisual;
                        @XmlAttribute(name = "ProvidedBy")
                        protected ProviderType providedBy;
                        @XmlAttribute(name = "ProviderName")
                        protected String providerName;

                        /**
                         * Gets the value of the audioVisual property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the audioVisual property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getAudioVisual().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual }
                         * 
                         * 
                         */
                        public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual> getAudioVisual() {
                            if (audioVisual == null) {
                                audioVisual = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual>();
                            }
                            return this.audioVisual;
                        }

                        /**
                         * Obtiene el valor de la propiedad providedBy.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ProviderType }
                         *     
                         */
                        public ProviderType getProvidedBy() {
                            return providedBy;
                        }

                        /**
                         * Define el valor de la propiedad providedBy.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ProviderType }
                         *     
                         */
                        public void setProvidedBy(ProviderType value) {
                            this.providedBy = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad providerName.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getProviderName() {
                            return providerName;
                        }

                        /**
                         * Define el valor de la propiedad providerName.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setProviderName(String value) {
                            this.providerName = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *       &amp;lt;attribute name="AudioVisualPref" type="{http://www.opentravel.org/OTA/2003/05}PreferLevelType" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "comments"
                        })
                        public static class AudioVisual {

                            @XmlElement(name = "Comments")
                            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual.Comments comments;
                            @XmlAttribute(name = "Code")
                            protected String code;
                            @XmlAttribute(name = "AudioVisualPref")
                            protected PreferLevelType audioVisualPref;
                            @XmlAttribute(name = "Quantity")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger quantity;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "DecimalPlaces")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger decimalPlaces;
                            @XmlAttribute(name = "Amount")
                            protected BigDecimal amount;

                            /**
                             * Obtiene el valor de la propiedad comments.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual.Comments }
                             *     
                             */
                            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual.Comments getComments() {
                                return comments;
                            }

                            /**
                             * Define el valor de la propiedad comments.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual.Comments }
                             *     
                             */
                            public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.AudioVisuals.AudioVisual.Comments value) {
                                this.comments = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad code.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode() {
                                return code;
                            }

                            /**
                             * Define el valor de la propiedad code.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode(String value) {
                                this.code = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad audioVisualPref.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PreferLevelType }
                             *     
                             */
                            public PreferLevelType getAudioVisualPref() {
                                return audioVisualPref;
                            }

                            /**
                             * Define el valor de la propiedad audioVisualPref.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PreferLevelType }
                             *     
                             */
                            public void setAudioVisualPref(PreferLevelType value) {
                                this.audioVisualPref = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad quantity.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getQuantity() {
                                return quantity;
                            }

                            /**
                             * Define el valor de la propiedad quantity.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setQuantity(BigInteger value) {
                                this.quantity = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad currencyCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Define el valor de la propiedad currencyCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad decimalPlaces.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDecimalPlaces() {
                                return decimalPlaces;
                            }

                            /**
                             * Define el valor de la propiedad decimalPlaces.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDecimalPlaces(BigInteger value) {
                                this.decimalPlaces = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setAmount(BigDecimal value) {
                                this.amount = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "comment"
                            })
                            public static class Comments {

                                @XmlElement(name = "Comment", required = true)
                                protected List<ParagraphType> comment;

                                /**
                                 * Gets the value of the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getComment().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link ParagraphType }
                                 * 
                                 * 
                                 */
                                public List<ParagraphType> getComment() {
                                    if (comment == null) {
                                        comment = new ArrayList<ParagraphType>();
                                    }
                                    return this.comment;
                                }

                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "comment"
                    })
                    public static class Comments {

                        @XmlElement(name = "Comment", required = true)
                        protected List<ParagraphType> comment;

                        /**
                         * Gets the value of the comment property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getComment().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link ParagraphType }
                         * 
                         * 
                         */
                        public List<ParagraphType> getComment() {
                            if (comment == null) {
                                comment = new ArrayList<ParagraphType>();
                            }
                            return this.comment;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Contact" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "contact"
                    })
                    public static class Contacts {

                        @XmlElement(name = "Contact", required = true)
                        protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact> contact;

                        /**
                         * Gets the value of the contact property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getContact().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact }
                         * 
                         * 
                         */
                        public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact> getContact() {
                            if (contact == null) {
                                contact = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact>();
                            }
                            return this.contact;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "comments"
                        })
                        public static class Contact
                            extends ContactPersonType
                        {

                            @XmlElement(name = "Comments")
                            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact.Comments comments;

                            /**
                             * Obtiene el valor de la propiedad comments.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact.Comments }
                             *     
                             */
                            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact.Comments getComments() {
                                return comments;
                            }

                            /**
                             * Define el valor de la propiedad comments.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact.Comments }
                             *     
                             */
                            public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Contacts.Contact.Comments value) {
                                this.comments = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "comment"
                            })
                            public static class Comments {

                                @XmlElement(name = "Comment", required = true)
                                protected List<ParagraphType> comment;

                                /**
                                 * Gets the value of the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getComment().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link ParagraphType }
                                 * 
                                 * 
                                 */
                                public List<ParagraphType> getComment() {
                                    if (comment == null) {
                                        comment = new ArrayList<ParagraphType>();
                                    }
                                    return this.comment;
                                }

                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="FoodAndBeverage" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
                     *                               &amp;lt;complexType&amp;gt;
                     *                                 &amp;lt;complexContent&amp;gt;
                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                     &amp;lt;sequence&amp;gt;
                     *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                     *                                         &amp;lt;complexType&amp;gt;
                     *                                           &amp;lt;complexContent&amp;gt;
                     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                               &amp;lt;sequence&amp;gt;
                     *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                     *                                               &amp;lt;/sequence&amp;gt;
                     *                                             &amp;lt;/restriction&amp;gt;
                     *                                           &amp;lt;/complexContent&amp;gt;
                     *                                         &amp;lt;/complexType&amp;gt;
                     *                                       &amp;lt;/element&amp;gt;
                     *                                     &amp;lt;/sequence&amp;gt;
                     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                                     &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                     *                                     &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                     &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                     *                                   &amp;lt;/restriction&amp;gt;
                     *                                 &amp;lt;/complexContent&amp;gt;
                     *                               &amp;lt;/complexType&amp;gt;
                     *                             &amp;lt;/element&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                 &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
                     *                 &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                 &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                 &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                 &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "foodAndBeverage"
                    })
                    public static class FoodAndBeverages {

                        @XmlElement(name = "FoodAndBeverage", required = true)
                        protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage> foodAndBeverage;
                        @XmlAttribute(name = "ProvidedBy")
                        protected ProviderType providedBy;
                        @XmlAttribute(name = "ProviderName")
                        protected String providerName;

                        /**
                         * Gets the value of the foodAndBeverage property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the foodAndBeverage property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getFoodAndBeverage().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage }
                         * 
                         * 
                         */
                        public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage> getFoodAndBeverage() {
                            if (foodAndBeverage == null) {
                                foodAndBeverage = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage>();
                            }
                            return this.foodAndBeverage;
                        }

                        /**
                         * Obtiene el valor de la propiedad providedBy.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ProviderType }
                         *     
                         */
                        public ProviderType getProvidedBy() {
                            return providedBy;
                        }

                        /**
                         * Define el valor de la propiedad providedBy.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ProviderType }
                         *     
                         */
                        public void setProvidedBy(ProviderType value) {
                            this.providedBy = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad providerName.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getProviderName() {
                            return providerName;
                        }

                        /**
                         * Define el valor de la propiedad providerName.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setProviderName(String value) {
                            this.providerName = value;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="Menus" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
                         *                     &amp;lt;complexType&amp;gt;
                         *                       &amp;lt;complexContent&amp;gt;
                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                           &amp;lt;sequence&amp;gt;
                         *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                         *                               &amp;lt;complexType&amp;gt;
                         *                                 &amp;lt;complexContent&amp;gt;
                         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                     &amp;lt;sequence&amp;gt;
                         *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                         *                                     &amp;lt;/sequence&amp;gt;
                         *                                   &amp;lt;/restriction&amp;gt;
                         *                                 &amp;lt;/complexContent&amp;gt;
                         *                               &amp;lt;/complexType&amp;gt;
                         *                             &amp;lt;/element&amp;gt;
                         *                           &amp;lt;/sequence&amp;gt;
                         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *                           &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                         *                           &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                           &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                         *                         &amp;lt;/restriction&amp;gt;
                         *                       &amp;lt;/complexContent&amp;gt;
                         *                     &amp;lt;/complexType&amp;gt;
                         *                   &amp;lt;/element&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *       &amp;lt;attribute name="ServiceTime" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
                         *       &amp;lt;attribute name="AttendeeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *       &amp;lt;attribute name="GuaranteeQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *       &amp;lt;attribute name="MealTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *       &amp;lt;attribute name="ServiceTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "comments",
                            "menus"
                        })
                        public static class FoodAndBeverage {

                            @XmlElement(name = "Comments")
                            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Comments comments;
                            @XmlElement(name = "Menus")
                            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus menus;
                            @XmlAttribute(name = "SetForQuantity")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger setForQuantity;
                            @XmlAttribute(name = "ServiceTime")
                            protected String serviceTime;
                            @XmlAttribute(name = "AttendeeQuantity")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger attendeeQuantity;
                            @XmlAttribute(name = "GuaranteeQuantity")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger guaranteeQuantity;
                            @XmlAttribute(name = "MealTypeCode")
                            protected String mealTypeCode;
                            @XmlAttribute(name = "ServiceTypeCode")
                            protected String serviceTypeCode;

                            /**
                             * Obtiene el valor de la propiedad comments.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Comments }
                             *     
                             */
                            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Comments getComments() {
                                return comments;
                            }

                            /**
                             * Define el valor de la propiedad comments.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Comments }
                             *     
                             */
                            public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Comments value) {
                                this.comments = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad menus.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus }
                             *     
                             */
                            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus getMenus() {
                                return menus;
                            }

                            /**
                             * Define el valor de la propiedad menus.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus }
                             *     
                             */
                            public void setMenus(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus value) {
                                this.menus = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad setForQuantity.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getSetForQuantity() {
                                return setForQuantity;
                            }

                            /**
                             * Define el valor de la propiedad setForQuantity.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setSetForQuantity(BigInteger value) {
                                this.setForQuantity = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad serviceTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getServiceTime() {
                                return serviceTime;
                            }

                            /**
                             * Define el valor de la propiedad serviceTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setServiceTime(String value) {
                                this.serviceTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad attendeeQuantity.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getAttendeeQuantity() {
                                return attendeeQuantity;
                            }

                            /**
                             * Define el valor de la propiedad attendeeQuantity.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setAttendeeQuantity(BigInteger value) {
                                this.attendeeQuantity = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad guaranteeQuantity.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getGuaranteeQuantity() {
                                return guaranteeQuantity;
                            }

                            /**
                             * Define el valor de la propiedad guaranteeQuantity.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setGuaranteeQuantity(BigInteger value) {
                                this.guaranteeQuantity = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad mealTypeCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getMealTypeCode() {
                                return mealTypeCode;
                            }

                            /**
                             * Define el valor de la propiedad mealTypeCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setMealTypeCode(String value) {
                                this.mealTypeCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad serviceTypeCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getServiceTypeCode() {
                                return serviceTypeCode;
                            }

                            /**
                             * Define el valor de la propiedad serviceTypeCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setServiceTypeCode(String value) {
                                this.serviceTypeCode = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "comment"
                            })
                            public static class Comments {

                                @XmlElement(name = "Comment", required = true)
                                protected List<ParagraphType> comment;

                                /**
                                 * Gets the value of the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getComment().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link ParagraphType }
                                 * 
                                 * 
                                 */
                                public List<ParagraphType> getComment() {
                                    if (comment == null) {
                                        comment = new ArrayList<ParagraphType>();
                                    }
                                    return this.comment;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="Menu" maxOccurs="99"&amp;gt;
                             *           &amp;lt;complexType&amp;gt;
                             *             &amp;lt;complexContent&amp;gt;
                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                 &amp;lt;sequence&amp;gt;
                             *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                             *                     &amp;lt;complexType&amp;gt;
                             *                       &amp;lt;complexContent&amp;gt;
                             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                           &amp;lt;sequence&amp;gt;
                             *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                             *                           &amp;lt;/sequence&amp;gt;
                             *                         &amp;lt;/restriction&amp;gt;
                             *                       &amp;lt;/complexContent&amp;gt;
                             *                     &amp;lt;/complexType&amp;gt;
                             *                   &amp;lt;/element&amp;gt;
                             *                 &amp;lt;/sequence&amp;gt;
                             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                             *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                             *                 &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                 &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                             *               &amp;lt;/restriction&amp;gt;
                             *             &amp;lt;/complexContent&amp;gt;
                             *           &amp;lt;/complexType&amp;gt;
                             *         &amp;lt;/element&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "menu"
                            })
                            public static class Menus {

                                @XmlElement(name = "Menu", required = true)
                                protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu> menu;

                                /**
                                 * Gets the value of the menu property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the menu property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getMenu().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu }
                                 * 
                                 * 
                                 */
                                public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu> getMenu() {
                                    if (menu == null) {
                                        menu = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu>();
                                    }
                                    return this.menu;
                                }


                                /**
                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                 * 
                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                 * 
                                 * &lt;pre&gt;
                                 * &amp;lt;complexType&amp;gt;
                                 *   &amp;lt;complexContent&amp;gt;
                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *       &amp;lt;sequence&amp;gt;
                                 *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                                 *           &amp;lt;complexType&amp;gt;
                                 *             &amp;lt;complexContent&amp;gt;
                                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                 &amp;lt;sequence&amp;gt;
                                 *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                                 *                 &amp;lt;/sequence&amp;gt;
                                 *               &amp;lt;/restriction&amp;gt;
                                 *             &amp;lt;/complexContent&amp;gt;
                                 *           &amp;lt;/complexType&amp;gt;
                                 *         &amp;lt;/element&amp;gt;
                                 *       &amp;lt;/sequence&amp;gt;
                                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                 *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                                 *       &amp;lt;attribute name="ChargeUnit" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
                                 *     &amp;lt;/restriction&amp;gt;
                                 *   &amp;lt;/complexContent&amp;gt;
                                 * &amp;lt;/complexType&amp;gt;
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "comments"
                                })
                                public static class Menu {

                                    @XmlElement(name = "Comments")
                                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu.Comments comments;
                                    @XmlAttribute(name = "Name")
                                    protected String name;
                                    @XmlAttribute(name = "ChargeUnit")
                                    protected String chargeUnit;
                                    @XmlAttribute(name = "Quantity")
                                    protected BigDecimal quantity;
                                    @XmlAttribute(name = "CurrencyCode")
                                    protected String currencyCode;
                                    @XmlAttribute(name = "DecimalPlaces")
                                    @XmlSchemaType(name = "nonNegativeInteger")
                                    protected BigInteger decimalPlaces;
                                    @XmlAttribute(name = "Amount")
                                    protected BigDecimal amount;

                                    /**
                                     * Obtiene el valor de la propiedad comments.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu.Comments }
                                     *     
                                     */
                                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu.Comments getComments() {
                                        return comments;
                                    }

                                    /**
                                     * Define el valor de la propiedad comments.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu.Comments }
                                     *     
                                     */
                                    public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FoodAndBeverages.FoodAndBeverage.Menus.Menu.Comments value) {
                                        this.comments = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad name.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getName() {
                                        return name;
                                    }

                                    /**
                                     * Define el valor de la propiedad name.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setName(String value) {
                                        this.name = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad chargeUnit.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getChargeUnit() {
                                        return chargeUnit;
                                    }

                                    /**
                                     * Define el valor de la propiedad chargeUnit.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setChargeUnit(String value) {
                                        this.chargeUnit = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad quantity.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigDecimal }
                                     *     
                                     */
                                    public BigDecimal getQuantity() {
                                        return quantity;
                                    }

                                    /**
                                     * Define el valor de la propiedad quantity.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigDecimal }
                                     *     
                                     */
                                    public void setQuantity(BigDecimal value) {
                                        this.quantity = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad currencyCode.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getCurrencyCode() {
                                        return currencyCode;
                                    }

                                    /**
                                     * Define el valor de la propiedad currencyCode.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setCurrencyCode(String value) {
                                        this.currencyCode = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad decimalPlaces.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public BigInteger getDecimalPlaces() {
                                        return decimalPlaces;
                                    }

                                    /**
                                     * Define el valor de la propiedad decimalPlaces.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public void setDecimalPlaces(BigInteger value) {
                                        this.decimalPlaces = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad amount.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigDecimal }
                                     *     
                                     */
                                    public BigDecimal getAmount() {
                                        return amount;
                                    }

                                    /**
                                     * Define el valor de la propiedad amount.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigDecimal }
                                     *     
                                     */
                                    public void setAmount(BigDecimal value) {
                                        this.amount = value;
                                    }


                                    /**
                                     * &lt;p&gt;Clase Java para anonymous complex type.
                                     * 
                                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                     * 
                                     * &lt;pre&gt;
                                     * &amp;lt;complexType&amp;gt;
                                     *   &amp;lt;complexContent&amp;gt;
                                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *       &amp;lt;sequence&amp;gt;
                                     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                                     *       &amp;lt;/sequence&amp;gt;
                                     *     &amp;lt;/restriction&amp;gt;
                                     *   &amp;lt;/complexContent&amp;gt;
                                     * &amp;lt;/complexType&amp;gt;
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     */
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @XmlType(name = "", propOrder = {
                                        "comment"
                                    })
                                    public static class Comments {

                                        @XmlElement(name = "Comment", required = true)
                                        protected List<ParagraphType> comment;

                                        /**
                                         * Gets the value of the comment property.
                                         * 
                                         * &lt;p&gt;
                                         * This accessor method returns a reference to the live list,
                                         * not a snapshot. Therefore any modification you make to the
                                         * returned list will be present inside the JAXB object.
                                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                         * 
                                         * &lt;p&gt;
                                         * For example, to add a new item, do as follows:
                                         * &lt;pre&gt;
                                         *    getComment().add(newItem);
                                         * &lt;/pre&gt;
                                         * 
                                         * 
                                         * &lt;p&gt;
                                         * Objects of the following type(s) are allowed in the list
                                         * {@link ParagraphType }
                                         * 
                                         * 
                                         */
                                        public List<ParagraphType> getComment() {
                                            if (comment == null) {
                                                comment = new ArrayList<ParagraphType>();
                                            }
                                            return this.comment;
                                        }

                                    }

                                }

                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="FunctionCharge" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "functionCharge"
                    })
                    public static class FunctionCharges {

                        @XmlElement(name = "FunctionCharge", required = true)
                        protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges.FunctionCharge> functionCharge;

                        /**
                         * Gets the value of the functionCharge property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the functionCharge property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getFunctionCharge().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges.FunctionCharge }
                         * 
                         * 
                         */
                        public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges.FunctionCharge> getFunctionCharge() {
                            if (functionCharge == null) {
                                functionCharge = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.FunctionCharges.FunctionCharge>();
                            }
                            return this.functionCharge;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *       &amp;lt;attribute name="FunctionChargeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "taxes",
                            "comment"
                        })
                        public static class FunctionCharge {

                            @XmlElement(name = "Taxes")
                            protected TaxesType taxes;
                            @XmlElement(name = "Comment")
                            protected ParagraphType comment;
                            @XmlAttribute(name = "FunctionChargeCode")
                            protected String functionChargeCode;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "DecimalPlaces")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger decimalPlaces;
                            @XmlAttribute(name = "Amount")
                            protected BigDecimal amount;

                            /**
                             * Obtiene el valor de la propiedad taxes.
                             * 
                             * @return
                             *     possible object is
                             *     {@link TaxesType }
                             *     
                             */
                            public TaxesType getTaxes() {
                                return taxes;
                            }

                            /**
                             * Define el valor de la propiedad taxes.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link TaxesType }
                             *     
                             */
                            public void setTaxes(TaxesType value) {
                                this.taxes = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad comment.
                             * 
                             * @return
                             *     possible object is
                             *     {@link ParagraphType }
                             *     
                             */
                            public ParagraphType getComment() {
                                return comment;
                            }

                            /**
                             * Define el valor de la propiedad comment.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link ParagraphType }
                             *     
                             */
                            public void setComment(ParagraphType value) {
                                this.comment = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad functionChargeCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getFunctionChargeCode() {
                                return functionChargeCode;
                            }

                            /**
                             * Define el valor de la propiedad functionChargeCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setFunctionChargeCode(String value) {
                                this.functionChargeCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad currencyCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Define el valor de la propiedad currencyCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad decimalPlaces.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDecimalPlaces() {
                                return decimalPlaces;
                            }

                            /**
                             * Define el valor de la propiedad decimalPlaces.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDecimalPlaces(BigInteger value) {
                                this.decimalPlaces = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setAmount(BigDecimal value) {
                                this.amount = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="RoomSetupCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *       &amp;lt;attribute name="SetForQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class RoomSetup {

                        @XmlAttribute(name = "RoomSetupCode")
                        protected String roomSetupCode;
                        @XmlAttribute(name = "SetForQuantity")
                        @XmlSchemaType(name = "nonNegativeInteger")
                        protected BigInteger setForQuantity;

                        /**
                         * Obtiene el valor de la propiedad roomSetupCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRoomSetupCode() {
                            return roomSetupCode;
                        }

                        /**
                         * Define el valor de la propiedad roomSetupCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRoomSetupCode(String value) {
                            this.roomSetupCode = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad setForQuantity.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getSetForQuantity() {
                            return setForQuantity;
                        }

                        /**
                         * Define el valor de la propiedad setForQuantity.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setSetForQuantity(BigInteger value) {
                            this.setForQuantity = value;
                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Service" maxOccurs="unbounded"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
                     *                               &amp;lt;complexType&amp;gt;
                     *                                 &amp;lt;complexContent&amp;gt;
                     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                     &amp;lt;sequence&amp;gt;
                     *                                       &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                     *                                         &amp;lt;complexType&amp;gt;
                     *                                           &amp;lt;complexContent&amp;gt;
                     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                                               &amp;lt;sequence&amp;gt;
                     *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                     *                                               &amp;lt;/sequence&amp;gt;
                     *                                             &amp;lt;/restriction&amp;gt;
                     *                                           &amp;lt;/complexContent&amp;gt;
                     *                                         &amp;lt;/complexType&amp;gt;
                     *                                       &amp;lt;/element&amp;gt;
                     *                                     &amp;lt;/sequence&amp;gt;
                     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                     &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                     *                                     &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                                     &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                                   &amp;lt;/restriction&amp;gt;
                     *                                 &amp;lt;/complexContent&amp;gt;
                     *                               &amp;lt;/complexType&amp;gt;
                     *                             &amp;lt;/element&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;sequence&amp;gt;
                     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                     *                           &amp;lt;/sequence&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *                 &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                 &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                 &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "service"
                    })
                    public static class Services {

                        @XmlElement(name = "Service", required = true)
                        protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service> service;

                        /**
                         * Gets the value of the service property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the service property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getService().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service }
                         * 
                         * 
                         */
                        public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service> getService() {
                            if (service == null) {
                                service = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service>();
                            }
                            return this.service;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Utilities" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
                         *                     &amp;lt;complexType&amp;gt;
                         *                       &amp;lt;complexContent&amp;gt;
                         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                           &amp;lt;sequence&amp;gt;
                         *                             &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                         *                               &amp;lt;complexType&amp;gt;
                         *                                 &amp;lt;complexContent&amp;gt;
                         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                                     &amp;lt;sequence&amp;gt;
                         *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                         *                                     &amp;lt;/sequence&amp;gt;
                         *                                   &amp;lt;/restriction&amp;gt;
                         *                                 &amp;lt;/complexContent&amp;gt;
                         *                               &amp;lt;/complexType&amp;gt;
                         *                             &amp;lt;/element&amp;gt;
                         *                           &amp;lt;/sequence&amp;gt;
                         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                           &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                         *                           &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *                           &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *                         &amp;lt;/restriction&amp;gt;
                         *                       &amp;lt;/complexContent&amp;gt;
                         *                     &amp;lt;/complexType&amp;gt;
                         *                   &amp;lt;/element&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;sequence&amp;gt;
                         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                         *                 &amp;lt;/sequence&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ProviderGroup"/&amp;gt;
                         *       &amp;lt;attribute name="LocationName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *       &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *       &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                         *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *       &amp;lt;attribute name="CodeDetail" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "utilities",
                            "comments"
                        })
                        public static class Service {

                            @XmlElement(name = "Utilities")
                            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities utilities;
                            @XmlElement(name = "Comments")
                            protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Comments comments;
                            @XmlAttribute(name = "LocationName")
                            protected String locationName;
                            @XmlAttribute(name = "StartTime")
                            @XmlSchemaType(name = "time")
                            protected XMLGregorianCalendar startTime;
                            @XmlAttribute(name = "EndTime")
                            @XmlSchemaType(name = "time")
                            protected XMLGregorianCalendar endTime;
                            @XmlAttribute(name = "Code")
                            protected String code;
                            @XmlAttribute(name = "CodeDetail")
                            protected String codeDetail;
                            @XmlAttribute(name = "ProvidedBy")
                            protected ProviderType providedBy;
                            @XmlAttribute(name = "ProviderName")
                            protected String providerName;

                            /**
                             * Obtiene el valor de la propiedad utilities.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities }
                             *     
                             */
                            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities getUtilities() {
                                return utilities;
                            }

                            /**
                             * Define el valor de la propiedad utilities.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities }
                             *     
                             */
                            public void setUtilities(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities value) {
                                this.utilities = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad comments.
                             * 
                             * @return
                             *     possible object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Comments }
                             *     
                             */
                            public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Comments getComments() {
                                return comments;
                            }

                            /**
                             * Define el valor de la propiedad comments.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Comments }
                             *     
                             */
                            public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Comments value) {
                                this.comments = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad locationName.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getLocationName() {
                                return locationName;
                            }

                            /**
                             * Define el valor de la propiedad locationName.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setLocationName(String value) {
                                this.locationName = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad startTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getStartTime() {
                                return startTime;
                            }

                            /**
                             * Define el valor de la propiedad startTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setStartTime(XMLGregorianCalendar value) {
                                this.startTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad endTime.
                             * 
                             * @return
                             *     possible object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public XMLGregorianCalendar getEndTime() {
                                return endTime;
                            }

                            /**
                             * Define el valor de la propiedad endTime.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link XMLGregorianCalendar }
                             *     
                             */
                            public void setEndTime(XMLGregorianCalendar value) {
                                this.endTime = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad code.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode() {
                                return code;
                            }

                            /**
                             * Define el valor de la propiedad code.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode(String value) {
                                this.code = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad codeDetail.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCodeDetail() {
                                return codeDetail;
                            }

                            /**
                             * Define el valor de la propiedad codeDetail.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCodeDetail(String value) {
                                this.codeDetail = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad providedBy.
                             * 
                             * @return
                             *     possible object is
                             *     {@link ProviderType }
                             *     
                             */
                            public ProviderType getProvidedBy() {
                                return providedBy;
                            }

                            /**
                             * Define el valor de la propiedad providedBy.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link ProviderType }
                             *     
                             */
                            public void setProvidedBy(ProviderType value) {
                                this.providedBy = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad providerName.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getProviderName() {
                                return providerName;
                            }

                            /**
                             * Define el valor de la propiedad providerName.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setProviderName(String value) {
                                this.providerName = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "comment"
                            })
                            public static class Comments {

                                @XmlElement(name = "Comment", required = true)
                                protected List<ParagraphType> comment;

                                /**
                                 * Gets the value of the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getComment().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link ParagraphType }
                                 * 
                                 * 
                                 */
                                public List<ParagraphType> getComment() {
                                    if (comment == null) {
                                        comment = new ArrayList<ParagraphType>();
                                    }
                                    return this.comment;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;sequence&amp;gt;
                             *         &amp;lt;element name="Utility" maxOccurs="99"&amp;gt;
                             *           &amp;lt;complexType&amp;gt;
                             *             &amp;lt;complexContent&amp;gt;
                             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                 &amp;lt;sequence&amp;gt;
                             *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                             *                     &amp;lt;complexType&amp;gt;
                             *                       &amp;lt;complexContent&amp;gt;
                             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *                           &amp;lt;sequence&amp;gt;
                             *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                             *                           &amp;lt;/sequence&amp;gt;
                             *                         &amp;lt;/restriction&amp;gt;
                             *                       &amp;lt;/complexContent&amp;gt;
                             *                     &amp;lt;/complexType&amp;gt;
                             *                   &amp;lt;/element&amp;gt;
                             *                 &amp;lt;/sequence&amp;gt;
                             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                             *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                 &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                             *                 &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *                 &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                             *               &amp;lt;/restriction&amp;gt;
                             *             &amp;lt;/complexContent&amp;gt;
                             *           &amp;lt;/complexType&amp;gt;
                             *         &amp;lt;/element&amp;gt;
                             *       &amp;lt;/sequence&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "utility"
                            })
                            public static class Utilities {

                                @XmlElement(name = "Utility", required = true)
                                protected List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility> utility;

                                /**
                                 * Gets the value of the utility property.
                                 * 
                                 * &lt;p&gt;
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the utility property.
                                 * 
                                 * &lt;p&gt;
                                 * For example, to add a new item, do as follows:
                                 * &lt;pre&gt;
                                 *    getUtility().add(newItem);
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 * &lt;p&gt;
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility }
                                 * 
                                 * 
                                 */
                                public List<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility> getUtility() {
                                    if (utility == null) {
                                        utility = new ArrayList<PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility>();
                                    }
                                    return this.utility;
                                }


                                /**
                                 * &lt;p&gt;Clase Java para anonymous complex type.
                                 * 
                                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                 * 
                                 * &lt;pre&gt;
                                 * &amp;lt;complexType&amp;gt;
                                 *   &amp;lt;complexContent&amp;gt;
                                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *       &amp;lt;sequence&amp;gt;
                                 *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
                                 *           &amp;lt;complexType&amp;gt;
                                 *             &amp;lt;complexContent&amp;gt;
                                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                 *                 &amp;lt;sequence&amp;gt;
                                 *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                                 *                 &amp;lt;/sequence&amp;gt;
                                 *               &amp;lt;/restriction&amp;gt;
                                 *             &amp;lt;/complexContent&amp;gt;
                                 *           &amp;lt;/complexType&amp;gt;
                                 *         &amp;lt;/element&amp;gt;
                                 *       &amp;lt;/sequence&amp;gt;
                                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}QuantityGroup"/&amp;gt;
                                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                                 *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                                 *       &amp;lt;attribute name="TelecommunicationCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                                 *       &amp;lt;attribute name="Description" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
                                 *     &amp;lt;/restriction&amp;gt;
                                 *   &amp;lt;/complexContent&amp;gt;
                                 * &amp;lt;/complexType&amp;gt;
                                 * &lt;/pre&gt;
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "comments"
                                })
                                public static class Utility {

                                    @XmlElement(name = "Comments")
                                    protected PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility.Comments comments;
                                    @XmlAttribute(name = "Code")
                                    protected String code;
                                    @XmlAttribute(name = "Name")
                                    protected String name;
                                    @XmlAttribute(name = "TelecommunicationCode")
                                    protected String telecommunicationCode;
                                    @XmlAttribute(name = "Description")
                                    protected String description;
                                    @XmlAttribute(name = "Quantity")
                                    @XmlSchemaType(name = "nonNegativeInteger")
                                    protected BigInteger quantity;
                                    @XmlAttribute(name = "CurrencyCode")
                                    protected String currencyCode;
                                    @XmlAttribute(name = "DecimalPlaces")
                                    @XmlSchemaType(name = "nonNegativeInteger")
                                    protected BigInteger decimalPlaces;
                                    @XmlAttribute(name = "Amount")
                                    protected BigDecimal amount;

                                    /**
                                     * Obtiene el valor de la propiedad comments.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility.Comments }
                                     *     
                                     */
                                    public PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility.Comments getComments() {
                                        return comments;
                                    }

                                    /**
                                     * Define el valor de la propiedad comments.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility.Comments }
                                     *     
                                     */
                                    public void setComments(PostEventSiteReportType.EventDays.EventDay.EventDayFunctions.EventDayFunction.Services.Service.Utilities.Utility.Comments value) {
                                        this.comments = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad code.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getCode() {
                                        return code;
                                    }

                                    /**
                                     * Define el valor de la propiedad code.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setCode(String value) {
                                        this.code = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad name.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getName() {
                                        return name;
                                    }

                                    /**
                                     * Define el valor de la propiedad name.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setName(String value) {
                                        this.name = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad telecommunicationCode.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getTelecommunicationCode() {
                                        return telecommunicationCode;
                                    }

                                    /**
                                     * Define el valor de la propiedad telecommunicationCode.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setTelecommunicationCode(String value) {
                                        this.telecommunicationCode = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad description.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getDescription() {
                                        return description;
                                    }

                                    /**
                                     * Define el valor de la propiedad description.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setDescription(String value) {
                                        this.description = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad quantity.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public BigInteger getQuantity() {
                                        return quantity;
                                    }

                                    /**
                                     * Define el valor de la propiedad quantity.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public void setQuantity(BigInteger value) {
                                        this.quantity = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad currencyCode.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getCurrencyCode() {
                                        return currencyCode;
                                    }

                                    /**
                                     * Define el valor de la propiedad currencyCode.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setCurrencyCode(String value) {
                                        this.currencyCode = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad decimalPlaces.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public BigInteger getDecimalPlaces() {
                                        return decimalPlaces;
                                    }

                                    /**
                                     * Define el valor de la propiedad decimalPlaces.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigInteger }
                                     *     
                                     */
                                    public void setDecimalPlaces(BigInteger value) {
                                        this.decimalPlaces = value;
                                    }

                                    /**
                                     * Obtiene el valor de la propiedad amount.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link BigDecimal }
                                     *     
                                     */
                                    public BigDecimal getAmount() {
                                        return amount;
                                    }

                                    /**
                                     * Define el valor de la propiedad amount.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link BigDecimal }
                                     *     
                                     */
                                    public void setAmount(BigDecimal value) {
                                        this.amount = value;
                                    }


                                    /**
                                     * &lt;p&gt;Clase Java para anonymous complex type.
                                     * 
                                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                                     * 
                                     * &lt;pre&gt;
                                     * &amp;lt;complexType&amp;gt;
                                     *   &amp;lt;complexContent&amp;gt;
                                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                                     *       &amp;lt;sequence&amp;gt;
                                     *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
                                     *       &amp;lt;/sequence&amp;gt;
                                     *     &amp;lt;/restriction&amp;gt;
                                     *   &amp;lt;/complexContent&amp;gt;
                                     * &amp;lt;/complexType&amp;gt;
                                     * &lt;/pre&gt;
                                     * 
                                     * 
                                     */
                                    @XmlAccessorType(XmlAccessType.FIELD)
                                    @XmlType(name = "", propOrder = {
                                        "comment"
                                    })
                                    public static class Comments {

                                        @XmlElement(name = "Comment", required = true)
                                        protected List<ParagraphType> comment;

                                        /**
                                         * Gets the value of the comment property.
                                         * 
                                         * &lt;p&gt;
                                         * This accessor method returns a reference to the live list,
                                         * not a snapshot. Therefore any modification you make to the
                                         * returned list will be present inside the JAXB object.
                                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                                         * 
                                         * &lt;p&gt;
                                         * For example, to add a new item, do as follows:
                                         * &lt;pre&gt;
                                         *    getComment().add(newItem);
                                         * &lt;/pre&gt;
                                         * 
                                         * 
                                         * &lt;p&gt;
                                         * Objects of the following type(s) are allowed in the list
                                         * {@link ParagraphType }
                                         * 
                                         * 
                                         */
                                        public List<ParagraphType> getComment() {
                                            if (comment == null) {
                                                comment = new ArrayList<ParagraphType>();
                                            }
                                            return this.comment;
                                        }

                                    }

                                }

                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *       &amp;lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class SessionTimes {

                        @XmlAttribute(name = "StartTime")
                        @XmlSchemaType(name = "time")
                        protected XMLGregorianCalendar startTime;
                        @XmlAttribute(name = "EndTime")
                        @XmlSchemaType(name = "time")
                        protected XMLGregorianCalendar endTime;

                        /**
                         * Obtiene el valor de la propiedad startTime.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getStartTime() {
                            return startTime;
                        }

                        /**
                         * Define el valor de la propiedad startTime.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setStartTime(XMLGregorianCalendar value) {
                            this.startTime = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad endTime.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getEndTime() {
                            return endTime;
                        }

                        /**
                         * Define el valor de la propiedad endTime.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setEndTime(XMLGregorianCalendar value) {
                            this.endTime = value;
                        }

                    }

                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *       &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EventID
        extends UniqueIDType
    {

        @XmlAttribute(name = "MeetingName")
        protected String meetingName;

        /**
         * Obtiene el valor de la propiedad meetingName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMeetingName() {
            return meetingName;
        }

        /**
         * Define el valor de la propiedad meetingName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMeetingName(String value) {
            this.meetingName = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ExhibitDetails" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ExhibitDetail" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DimensionGroup"/&amp;gt;
     *                           &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="FoodAndBeverageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="FoodAndBeverageBoothQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="ExhibitorInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AdditionalDates" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="AdditionalDate" maxOccurs="unbounded"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                           &amp;lt;attribute name="Type"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Contracted"/&amp;gt;
     *                                 &amp;lt;enumeration value="ContractorMoveIn"/&amp;gt;
     *                                 &amp;lt;enumeration value="ContractorMoveOut"/&amp;gt;
     *                                 &amp;lt;enumeration value="ExhibitorMoveIn"/&amp;gt;
     *                                 &amp;lt;enumeration value="ExhibitorMoveOut"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="Type"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Public"/&amp;gt;
     *             &amp;lt;enumeration value="Private"/&amp;gt;
     *             &amp;lt;enumeration value="PublicPrivate"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="GrossExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="NetExhibitionSpace" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="UnitOfMeasureCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="ExhibitQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="CompanyQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="GeneralServiceContractorInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="SecuredAreaIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exhibitDetails",
        "exhibitorInfo",
        "additionalDates",
        "comments"
    })
    public static class Exhibition {

        @XmlElement(name = "ExhibitDetails")
        protected PostEventSiteReportType.Exhibition.ExhibitDetails exhibitDetails;
        @XmlElement(name = "ExhibitorInfo")
        protected PostEventSiteReportType.Exhibition.ExhibitorInfo exhibitorInfo;
        @XmlElement(name = "AdditionalDates")
        protected PostEventSiteReportType.Exhibition.AdditionalDates additionalDates;
        @XmlElement(name = "Comments")
        protected PostEventSiteReportType.Exhibition.Comments comments;
        @XmlAttribute(name = "Type")
        protected String type;
        @XmlAttribute(name = "GrossExhibitionSpace")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger grossExhibitionSpace;
        @XmlAttribute(name = "NetExhibitionSpace")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger netExhibitionSpace;
        @XmlAttribute(name = "UnitOfMeasureCode")
        protected String unitOfMeasureCode;
        @XmlAttribute(name = "ExhibitQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger exhibitQuantity;
        @XmlAttribute(name = "CompanyQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger companyQuantity;
        @XmlAttribute(name = "GeneralServiceContractorInd")
        protected Boolean generalServiceContractorInd;
        @XmlAttribute(name = "SecuredAreaIndicator")
        protected Boolean securedAreaIndicator;

        /**
         * Obtiene el valor de la propiedad exhibitDetails.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.Exhibition.ExhibitDetails }
         *     
         */
        public PostEventSiteReportType.Exhibition.ExhibitDetails getExhibitDetails() {
            return exhibitDetails;
        }

        /**
         * Define el valor de la propiedad exhibitDetails.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.Exhibition.ExhibitDetails }
         *     
         */
        public void setExhibitDetails(PostEventSiteReportType.Exhibition.ExhibitDetails value) {
            this.exhibitDetails = value;
        }

        /**
         * Obtiene el valor de la propiedad exhibitorInfo.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.Exhibition.ExhibitorInfo }
         *     
         */
        public PostEventSiteReportType.Exhibition.ExhibitorInfo getExhibitorInfo() {
            return exhibitorInfo;
        }

        /**
         * Define el valor de la propiedad exhibitorInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.Exhibition.ExhibitorInfo }
         *     
         */
        public void setExhibitorInfo(PostEventSiteReportType.Exhibition.ExhibitorInfo value) {
            this.exhibitorInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad additionalDates.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.Exhibition.AdditionalDates }
         *     
         */
        public PostEventSiteReportType.Exhibition.AdditionalDates getAdditionalDates() {
            return additionalDates;
        }

        /**
         * Define el valor de la propiedad additionalDates.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.Exhibition.AdditionalDates }
         *     
         */
        public void setAdditionalDates(PostEventSiteReportType.Exhibition.AdditionalDates value) {
            this.additionalDates = value;
        }

        /**
         * Obtiene el valor de la propiedad comments.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.Exhibition.Comments }
         *     
         */
        public PostEventSiteReportType.Exhibition.Comments getComments() {
            return comments;
        }

        /**
         * Define el valor de la propiedad comments.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.Exhibition.Comments }
         *     
         */
        public void setComments(PostEventSiteReportType.Exhibition.Comments value) {
            this.comments = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad grossExhibitionSpace.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getGrossExhibitionSpace() {
            return grossExhibitionSpace;
        }

        /**
         * Define el valor de la propiedad grossExhibitionSpace.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setGrossExhibitionSpace(BigInteger value) {
            this.grossExhibitionSpace = value;
        }

        /**
         * Obtiene el valor de la propiedad netExhibitionSpace.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNetExhibitionSpace() {
            return netExhibitionSpace;
        }

        /**
         * Define el valor de la propiedad netExhibitionSpace.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNetExhibitionSpace(BigInteger value) {
            this.netExhibitionSpace = value;
        }

        /**
         * Obtiene el valor de la propiedad unitOfMeasureCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasureCode() {
            return unitOfMeasureCode;
        }

        /**
         * Define el valor de la propiedad unitOfMeasureCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasureCode(String value) {
            this.unitOfMeasureCode = value;
        }

        /**
         * Obtiene el valor de la propiedad exhibitQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getExhibitQuantity() {
            return exhibitQuantity;
        }

        /**
         * Define el valor de la propiedad exhibitQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setExhibitQuantity(BigInteger value) {
            this.exhibitQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad companyQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCompanyQuantity() {
            return companyQuantity;
        }

        /**
         * Define el valor de la propiedad companyQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCompanyQuantity(BigInteger value) {
            this.companyQuantity = value;
        }

        /**
         * Obtiene el valor de la propiedad generalServiceContractorInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGeneralServiceContractorInd() {
            return generalServiceContractorInd;
        }

        /**
         * Define el valor de la propiedad generalServiceContractorInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGeneralServiceContractorInd(Boolean value) {
            this.generalServiceContractorInd = value;
        }

        /**
         * Obtiene el valor de la propiedad securedAreaIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSecuredAreaIndicator() {
            return securedAreaIndicator;
        }

        /**
         * Define el valor de la propiedad securedAreaIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSecuredAreaIndicator(Boolean value) {
            this.securedAreaIndicator = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="AdditionalDate" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *                 &amp;lt;attribute name="Type"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Contracted"/&amp;gt;
         *                       &amp;lt;enumeration value="ContractorMoveIn"/&amp;gt;
         *                       &amp;lt;enumeration value="ContractorMoveOut"/&amp;gt;
         *                       &amp;lt;enumeration value="ExhibitorMoveIn"/&amp;gt;
         *                       &amp;lt;enumeration value="ExhibitorMoveOut"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "additionalDate"
        })
        public static class AdditionalDates {

            @XmlElement(name = "AdditionalDate", required = true)
            protected List<PostEventSiteReportType.Exhibition.AdditionalDates.AdditionalDate> additionalDate;

            /**
             * Gets the value of the additionalDate property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the additionalDate property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getAdditionalDate().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PostEventSiteReportType.Exhibition.AdditionalDates.AdditionalDate }
             * 
             * 
             */
            public List<PostEventSiteReportType.Exhibition.AdditionalDates.AdditionalDate> getAdditionalDate() {
                if (additionalDate == null) {
                    additionalDate = new ArrayList<PostEventSiteReportType.Exhibition.AdditionalDates.AdditionalDate>();
                }
                return this.additionalDate;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
             *       &amp;lt;attribute name="Type"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Contracted"/&amp;gt;
             *             &amp;lt;enumeration value="ContractorMoveIn"/&amp;gt;
             *             &amp;lt;enumeration value="ContractorMoveOut"/&amp;gt;
             *             &amp;lt;enumeration value="ExhibitorMoveIn"/&amp;gt;
             *             &amp;lt;enumeration value="ExhibitorMoveOut"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AdditionalDate {

                @XmlAttribute(name = "Type")
                protected String type;
                @XmlAttribute(name = "Start")
                protected String start;
                @XmlAttribute(name = "Duration")
                protected String duration;
                @XmlAttribute(name = "End")
                protected String end;

                /**
                 * Obtiene el valor de la propiedad type.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Define el valor de la propiedad type.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Obtiene el valor de la propiedad start.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStart() {
                    return start;
                }

                /**
                 * Define el valor de la propiedad start.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStart(String value) {
                    this.start = value;
                }

                /**
                 * Obtiene el valor de la propiedad duration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDuration() {
                    return duration;
                }

                /**
                 * Define el valor de la propiedad duration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDuration(String value) {
                    this.duration = value;
                }

                /**
                 * Obtiene el valor de la propiedad end.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Define el valor de la propiedad end.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comment"
        })
        public static class Comments {

            @XmlElement(name = "Comment", required = true)
            protected List<ParagraphType> comment;

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<ParagraphType>();
                }
                return this.comment;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ExhibitDetail" maxOccurs="unbounded"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DimensionGroup"/&amp;gt;
         *                 &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="FoodAndBeverageIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="FoodAndBeverageBoothQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "exhibitDetail"
        })
        public static class ExhibitDetails {

            @XmlElement(name = "ExhibitDetail", required = true)
            protected List<PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail> exhibitDetail;
            @XmlAttribute(name = "FoodAndBeverageIndicator")
            protected Boolean foodAndBeverageIndicator;
            @XmlAttribute(name = "FoodAndBeverageBoothQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger foodAndBeverageBoothQuantity;

            /**
             * Gets the value of the exhibitDetail property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the exhibitDetail property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getExhibitDetail().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail }
             * 
             * 
             */
            public List<PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail> getExhibitDetail() {
                if (exhibitDetail == null) {
                    exhibitDetail = new ArrayList<PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail>();
                }
                return this.exhibitDetail;
            }

            /**
             * Obtiene el valor de la propiedad foodAndBeverageIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFoodAndBeverageIndicator() {
                return foodAndBeverageIndicator;
            }

            /**
             * Define el valor de la propiedad foodAndBeverageIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFoodAndBeverageIndicator(Boolean value) {
                this.foodAndBeverageIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad foodAndBeverageBoothQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFoodAndBeverageBoothQuantity() {
                return foodAndBeverageBoothQuantity;
            }

            /**
             * Define el valor de la propiedad foodAndBeverageBoothQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFoodAndBeverageBoothQuantity(BigInteger value) {
                this.foodAndBeverageBoothQuantity = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DimensionGroup"/&amp;gt;
             *       &amp;lt;attribute name="ExhibitTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "contacts"
            })
            public static class ExhibitDetail {

                @XmlElement(name = "Contacts")
                protected PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail.Contacts contacts;
                @XmlAttribute(name = "ExhibitTypeCode")
                protected String exhibitTypeCode;
                @XmlAttribute(name = "Area")
                protected BigDecimal area;
                @XmlAttribute(name = "Height")
                protected BigDecimal height;
                @XmlAttribute(name = "Length")
                protected BigDecimal length;
                @XmlAttribute(name = "Width")
                protected BigDecimal width;
                @XmlAttribute(name = "Units")
                protected String units;
                @XmlAttribute(name = "UnitOfMeasureCode")
                protected String unitOfMeasureCode;

                /**
                 * Obtiene el valor de la propiedad contacts.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail.Contacts }
                 *     
                 */
                public PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail.Contacts getContacts() {
                    return contacts;
                }

                /**
                 * Define el valor de la propiedad contacts.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail.Contacts }
                 *     
                 */
                public void setContacts(PostEventSiteReportType.Exhibition.ExhibitDetails.ExhibitDetail.Contacts value) {
                    this.contacts = value;
                }

                /**
                 * Obtiene el valor de la propiedad exhibitTypeCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExhibitTypeCode() {
                    return exhibitTypeCode;
                }

                /**
                 * Define el valor de la propiedad exhibitTypeCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExhibitTypeCode(String value) {
                    this.exhibitTypeCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad area.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getArea() {
                    return area;
                }

                /**
                 * Define el valor de la propiedad area.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setArea(BigDecimal value) {
                    this.area = value;
                }

                /**
                 * Obtiene el valor de la propiedad height.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getHeight() {
                    return height;
                }

                /**
                 * Define el valor de la propiedad height.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setHeight(BigDecimal value) {
                    this.height = value;
                }

                /**
                 * Obtiene el valor de la propiedad length.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getLength() {
                    return length;
                }

                /**
                 * Define el valor de la propiedad length.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setLength(BigDecimal value) {
                    this.length = value;
                }

                /**
                 * Obtiene el valor de la propiedad width.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getWidth() {
                    return width;
                }

                /**
                 * Define el valor de la propiedad width.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setWidth(BigDecimal value) {
                    this.width = value;
                }

                /**
                 * Obtiene el valor de la propiedad units.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnits() {
                    return units;
                }

                /**
                 * Define el valor de la propiedad units.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnits(String value) {
                    this.units = value;
                }

                /**
                 * Obtiene el valor de la propiedad unitOfMeasureCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUnitOfMeasureCode() {
                    return unitOfMeasureCode;
                }

                /**
                 * Define el valor de la propiedad unitOfMeasureCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUnitOfMeasureCode(String value) {
                    this.unitOfMeasureCode = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "contact"
                })
                public static class Contacts {

                    @XmlElement(name = "Contact", required = true)
                    protected List<ContactPersonType> contact;

                    /**
                     * Gets the value of the contact property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getContact().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link ContactPersonType }
                     * 
                     * 
                     */
                    public List<ContactPersonType> getContact() {
                        if (contact == null) {
                            contact = new ArrayList<ContactPersonType>();
                        }
                        return this.contact;
                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PostEventAttendanceGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comments"
        })
        public static class ExhibitorInfo {

            @XmlElement(name = "Comments")
            protected PostEventSiteReportType.Exhibition.ExhibitorInfo.Comments comments;
            @XmlAttribute(name = "TotalQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger totalQuantity;
            @XmlAttribute(name = "DomesticQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger domesticQuantity;
            @XmlAttribute(name = "InternationalQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger internationalQuantity;
            @XmlAttribute(name = "PreRegisteredQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger preRegisteredQuantity;
            @XmlAttribute(name = "OnsiteRegisteredQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger onsiteRegisteredQuantity;
            @XmlAttribute(name = "NoShowQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger noShowQuantity;
            @XmlAttribute(name = "ExhibitorQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger exhibitorQuantity;
            @XmlAttribute(name = "QuantityType")
            protected String quantityType;

            /**
             * Obtiene el valor de la propiedad comments.
             * 
             * @return
             *     possible object is
             *     {@link PostEventSiteReportType.Exhibition.ExhibitorInfo.Comments }
             *     
             */
            public PostEventSiteReportType.Exhibition.ExhibitorInfo.Comments getComments() {
                return comments;
            }

            /**
             * Define el valor de la propiedad comments.
             * 
             * @param value
             *     allowed object is
             *     {@link PostEventSiteReportType.Exhibition.ExhibitorInfo.Comments }
             *     
             */
            public void setComments(PostEventSiteReportType.Exhibition.ExhibitorInfo.Comments value) {
                this.comments = value;
            }

            /**
             * Obtiene el valor de la propiedad totalQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTotalQuantity() {
                return totalQuantity;
            }

            /**
             * Define el valor de la propiedad totalQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTotalQuantity(BigInteger value) {
                this.totalQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad domesticQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDomesticQuantity() {
                return domesticQuantity;
            }

            /**
             * Define el valor de la propiedad domesticQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDomesticQuantity(BigInteger value) {
                this.domesticQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad internationalQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getInternationalQuantity() {
                return internationalQuantity;
            }

            /**
             * Define el valor de la propiedad internationalQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setInternationalQuantity(BigInteger value) {
                this.internationalQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad preRegisteredQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPreRegisteredQuantity() {
                return preRegisteredQuantity;
            }

            /**
             * Define el valor de la propiedad preRegisteredQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPreRegisteredQuantity(BigInteger value) {
                this.preRegisteredQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad onsiteRegisteredQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getOnsiteRegisteredQuantity() {
                return onsiteRegisteredQuantity;
            }

            /**
             * Define el valor de la propiedad onsiteRegisteredQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setOnsiteRegisteredQuantity(BigInteger value) {
                this.onsiteRegisteredQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad noShowQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNoShowQuantity() {
                return noShowQuantity;
            }

            /**
             * Define el valor de la propiedad noShowQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNoShowQuantity(BigInteger value) {
                this.noShowQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad exhibitorQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getExhibitorQuantity() {
                return exhibitorQuantity;
            }

            /**
             * Define el valor de la propiedad exhibitorQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setExhibitorQuantity(BigInteger value) {
                this.exhibitorQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad quantityType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuantityType() {
                return quantityType;
            }

            /**
             * Define el valor de la propiedad quantityType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuantityType(String value) {
                this.quantityType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment"
            })
            public static class Comments {

                @XmlElement(name = "Comment", required = true)
                protected List<ParagraphType> comment;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link ParagraphType }
                 * 
                 * 
                 */
                public List<ParagraphType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<ParagraphType>();
                    }
                    return this.comment;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomBlock" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ReservationMethod" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="StayDays" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="OccupancyRate"&amp;gt;
     *                                                           &amp;lt;simpleType&amp;gt;
     *                                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                                               &amp;lt;enumeration value="Flat"/&amp;gt;
     *                                                               &amp;lt;enumeration value="Single"/&amp;gt;
     *                                                               &amp;lt;enumeration value="Double"/&amp;gt;
     *                                                               &amp;lt;enumeration value="Triple"/&amp;gt;
     *                                                               &amp;lt;enumeration value="Quad"/&amp;gt;
     *                                                             &amp;lt;/restriction&amp;gt;
     *                                                           &amp;lt;/simpleType&amp;gt;
     *                                                         &amp;lt;/attribute&amp;gt;
     *                                                       &amp;lt;/restriction&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="RoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
     *                                     &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                                     &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                                     &amp;lt;attribute name="RoomTypeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                     &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                                     &amp;lt;attribute name="ContractedRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="FinalRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="TotalRoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="HousingProviderName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
     *                 &amp;lt;attribute name="InvBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="CompRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="StaffRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                 &amp;lt;attribute name="ContractedDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="CutoffDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="CutoffDateExercisedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="RequestedOversellPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="TotalBlockQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomBlock",
        "comments"
    })
    public static class RoomBlocks {

        @XmlElement(name = "RoomBlock", required = true)
        protected List<PostEventSiteReportType.RoomBlocks.RoomBlock> roomBlock;
        @XmlElement(name = "Comments")
        protected PostEventSiteReportType.RoomBlocks.Comments comments;
        @XmlAttribute(name = "TotalBlockQuantity")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger totalBlockQuantity;

        /**
         * Gets the value of the roomBlock property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomBlock property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomBlock().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link PostEventSiteReportType.RoomBlocks.RoomBlock }
         * 
         * 
         */
        public List<PostEventSiteReportType.RoomBlocks.RoomBlock> getRoomBlock() {
            if (roomBlock == null) {
                roomBlock = new ArrayList<PostEventSiteReportType.RoomBlocks.RoomBlock>();
            }
            return this.roomBlock;
        }

        /**
         * Obtiene el valor de la propiedad comments.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.RoomBlocks.Comments }
         *     
         */
        public PostEventSiteReportType.RoomBlocks.Comments getComments() {
            return comments;
        }

        /**
         * Define el valor de la propiedad comments.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.RoomBlocks.Comments }
         *     
         */
        public void setComments(PostEventSiteReportType.RoomBlocks.Comments value) {
            this.comments = value;
        }

        /**
         * Obtiene el valor de la propiedad totalBlockQuantity.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalBlockQuantity() {
            return totalBlockQuantity;
        }

        /**
         * Define el valor de la propiedad totalBlockQuantity.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalBlockQuantity(BigInteger value) {
            this.totalBlockQuantity = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="99"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comment"
        })
        public static class Comments {

            @XmlElement(name = "Comment", required = true)
            protected List<ParagraphType> comment;

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<ParagraphType>();
                }
                return this.comment;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ReservationMethod" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="StayDays" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="OccupancyRate"&amp;gt;
         *                                                 &amp;lt;simpleType&amp;gt;
         *                                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                                                     &amp;lt;enumeration value="Flat"/&amp;gt;
         *                                                     &amp;lt;enumeration value="Single"/&amp;gt;
         *                                                     &amp;lt;enumeration value="Double"/&amp;gt;
         *                                                     &amp;lt;enumeration value="Triple"/&amp;gt;
         *                                                     &amp;lt;enumeration value="Quad"/&amp;gt;
         *                                                   &amp;lt;/restriction&amp;gt;
         *                                                 &amp;lt;/simpleType&amp;gt;
         *                                               &amp;lt;/attribute&amp;gt;
         *                                             &amp;lt;/restriction&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="RoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
         *                           &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *                           &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *                           &amp;lt;attribute name="RoomTypeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                           &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *                           &amp;lt;attribute name="ContractedRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                           &amp;lt;attribute name="FinalRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="TotalRoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="TotalRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="PeakRoomNightQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="HousingProviderName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to255" /&amp;gt;
         *       &amp;lt;attribute name="InvBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="CompRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="StaffRoomQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *       &amp;lt;attribute name="ContractedDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="CutoffDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="CutoffDateExercisedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="RequestedOversellPercentage" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "reservationMethod",
            "stayDays",
            "totalRoomPickUp"
        })
        public static class RoomBlock {

            @XmlElement(name = "ReservationMethod")
            protected List<PostEventSiteReportType.RoomBlocks.RoomBlock.ReservationMethod> reservationMethod;
            @XmlElement(name = "StayDays")
            protected PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays stayDays;
            @XmlElement(name = "TotalRoomPickUp")
            protected List<PostEventSiteReportType.RoomBlocks.RoomBlock.TotalRoomPickUp> totalRoomPickUp;
            @XmlAttribute(name = "TotalRoomNightQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger totalRoomNightQuantity;
            @XmlAttribute(name = "PeakRoomNightQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger peakRoomNightQuantity;
            @XmlAttribute(name = "HousingProviderName")
            protected String housingProviderName;
            @XmlAttribute(name = "InvBlockCode")
            protected String invBlockCode;
            @XmlAttribute(name = "CompRoomQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger compRoomQuantity;
            @XmlAttribute(name = "StaffRoomQuantity")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger staffRoomQuantity;
            @XmlAttribute(name = "ContractedDate")
            protected String contractedDate;
            @XmlAttribute(name = "CutoffDate")
            protected String cutoffDate;
            @XmlAttribute(name = "CutoffDateExercisedIndicator")
            protected Boolean cutoffDateExercisedIndicator;
            @XmlAttribute(name = "RequestedOversellPercentage")
            protected BigDecimal requestedOversellPercentage;

            /**
             * Gets the value of the reservationMethod property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reservationMethod property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getReservationMethod().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PostEventSiteReportType.RoomBlocks.RoomBlock.ReservationMethod }
             * 
             * 
             */
            public List<PostEventSiteReportType.RoomBlocks.RoomBlock.ReservationMethod> getReservationMethod() {
                if (reservationMethod == null) {
                    reservationMethod = new ArrayList<PostEventSiteReportType.RoomBlocks.RoomBlock.ReservationMethod>();
                }
                return this.reservationMethod;
            }

            /**
             * Obtiene el valor de la propiedad stayDays.
             * 
             * @return
             *     possible object is
             *     {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays }
             *     
             */
            public PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays getStayDays() {
                return stayDays;
            }

            /**
             * Define el valor de la propiedad stayDays.
             * 
             * @param value
             *     allowed object is
             *     {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays }
             *     
             */
            public void setStayDays(PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays value) {
                this.stayDays = value;
            }

            /**
             * Gets the value of the totalRoomPickUp property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the totalRoomPickUp property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTotalRoomPickUp().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PostEventSiteReportType.RoomBlocks.RoomBlock.TotalRoomPickUp }
             * 
             * 
             */
            public List<PostEventSiteReportType.RoomBlocks.RoomBlock.TotalRoomPickUp> getTotalRoomPickUp() {
                if (totalRoomPickUp == null) {
                    totalRoomPickUp = new ArrayList<PostEventSiteReportType.RoomBlocks.RoomBlock.TotalRoomPickUp>();
                }
                return this.totalRoomPickUp;
            }

            /**
             * Obtiene el valor de la propiedad totalRoomNightQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTotalRoomNightQuantity() {
                return totalRoomNightQuantity;
            }

            /**
             * Define el valor de la propiedad totalRoomNightQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTotalRoomNightQuantity(BigInteger value) {
                this.totalRoomNightQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad peakRoomNightQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPeakRoomNightQuantity() {
                return peakRoomNightQuantity;
            }

            /**
             * Define el valor de la propiedad peakRoomNightQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPeakRoomNightQuantity(BigInteger value) {
                this.peakRoomNightQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad housingProviderName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousingProviderName() {
                return housingProviderName;
            }

            /**
             * Define el valor de la propiedad housingProviderName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousingProviderName(String value) {
                this.housingProviderName = value;
            }

            /**
             * Obtiene el valor de la propiedad invBlockCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvBlockCode() {
                return invBlockCode;
            }

            /**
             * Define el valor de la propiedad invBlockCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvBlockCode(String value) {
                this.invBlockCode = value;
            }

            /**
             * Obtiene el valor de la propiedad compRoomQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCompRoomQuantity() {
                return compRoomQuantity;
            }

            /**
             * Define el valor de la propiedad compRoomQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCompRoomQuantity(BigInteger value) {
                this.compRoomQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad staffRoomQuantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getStaffRoomQuantity() {
                return staffRoomQuantity;
            }

            /**
             * Define el valor de la propiedad staffRoomQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setStaffRoomQuantity(BigInteger value) {
                this.staffRoomQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad contractedDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContractedDate() {
                return contractedDate;
            }

            /**
             * Define el valor de la propiedad contractedDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContractedDate(String value) {
                this.contractedDate = value;
            }

            /**
             * Obtiene el valor de la propiedad cutoffDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCutoffDate() {
                return cutoffDate;
            }

            /**
             * Define el valor de la propiedad cutoffDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCutoffDate(String value) {
                this.cutoffDate = value;
            }

            /**
             * Obtiene el valor de la propiedad cutoffDateExercisedIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isCutoffDateExercisedIndicator() {
                return cutoffDateExercisedIndicator;
            }

            /**
             * Define el valor de la propiedad cutoffDateExercisedIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setCutoffDateExercisedIndicator(Boolean value) {
                this.cutoffDateExercisedIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad requestedOversellPercentage.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getRequestedOversellPercentage() {
                return requestedOversellPercentage;
            }

            /**
             * Define el valor de la propiedad requestedOversellPercentage.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setRequestedOversellPercentage(BigDecimal value) {
                this.requestedOversellPercentage = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class ReservationMethod {

                @XmlAttribute(name = "Code")
                protected String code;

                /**
                 * Obtiene el valor de la propiedad code.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * Define el valor de la propiedad code.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="StayDay" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="OccupancyRate"&amp;gt;
             *                                       &amp;lt;simpleType&amp;gt;
             *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *                                           &amp;lt;enumeration value="Flat"/&amp;gt;
             *                                           &amp;lt;enumeration value="Single"/&amp;gt;
             *                                           &amp;lt;enumeration value="Double"/&amp;gt;
             *                                           &amp;lt;enumeration value="Triple"/&amp;gt;
             *                                           &amp;lt;enumeration value="Quad"/&amp;gt;
             *                                         &amp;lt;/restriction&amp;gt;
             *                                       &amp;lt;/simpleType&amp;gt;
             *                                     &amp;lt;/attribute&amp;gt;
             *                                   &amp;lt;/restriction&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="RoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
             *                 &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *                 &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
             *                 &amp;lt;attribute name="RoomTypeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *                 &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
             *                 &amp;lt;attribute name="ContractedRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                 &amp;lt;attribute name="FinalRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="FirstStayDayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "stayDay"
            })
            public static class StayDays {

                @XmlElement(name = "StayDay", required = true)
                protected List<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay> stayDay;
                @XmlAttribute(name = "FirstStayDayOfWeek")
                protected DayOfWeekType firstStayDayOfWeek;

                /**
                 * Gets the value of the stayDay property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stayDay property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getStayDay().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay }
                 * 
                 * 
                 */
                public List<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay> getStayDay() {
                    if (stayDay == null) {
                        stayDay = new ArrayList<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay>();
                    }
                    return this.stayDay;
                }

                /**
                 * Obtiene el valor de la propiedad firstStayDayOfWeek.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public DayOfWeekType getFirstStayDayOfWeek() {
                    return firstStayDayOfWeek;
                }

                /**
                 * Define el valor de la propiedad firstStayDayOfWeek.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public void setFirstStayDayOfWeek(DayOfWeekType value) {
                    this.firstStayDayOfWeek = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Rates" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="OccupancyRate"&amp;gt;
                 *                             &amp;lt;simpleType&amp;gt;
                 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                 *                                 &amp;lt;enumeration value="Flat"/&amp;gt;
                 *                                 &amp;lt;enumeration value="Single"/&amp;gt;
                 *                                 &amp;lt;enumeration value="Double"/&amp;gt;
                 *                                 &amp;lt;enumeration value="Triple"/&amp;gt;
                 *                                 &amp;lt;enumeration value="Quad"/&amp;gt;
                 *                               &amp;lt;/restriction&amp;gt;
                 *                             &amp;lt;/simpleType&amp;gt;
                 *                           &amp;lt;/attribute&amp;gt;
                 *                         &amp;lt;/restriction&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="RoomPickUp" maxOccurs="8" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="DayNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="DayType" type="{http://www.opentravel.org/OTA/2003/05}EventDayType" /&amp;gt;
                 *       &amp;lt;attribute name="GuestQuantity" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="TotalNumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
                 *       &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
                 *       &amp;lt;attribute name="RoomTypeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *       &amp;lt;attribute name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
                 *       &amp;lt;attribute name="ContractedRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *       &amp;lt;attribute name="FinalRoomBlock" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "rates",
                    "roomPickUp"
                })
                public static class StayDay {

                    @XmlElement(name = "Rates")
                    protected PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates rates;
                    @XmlElement(name = "RoomPickUp")
                    protected List<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.RoomPickUp> roomPickUp;
                    @XmlAttribute(name = "DayNumber", required = true)
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger dayNumber;
                    @XmlAttribute(name = "DayType")
                    protected EventDayType dayType;
                    @XmlAttribute(name = "GuestQuantity")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger guestQuantity;
                    @XmlAttribute(name = "TotalNumberOfUnits")
                    protected BigInteger totalNumberOfUnits;
                    @XmlAttribute(name = "RoomTypeCode")
                    protected String roomTypeCode;
                    @XmlAttribute(name = "RoomTypeName")
                    protected String roomTypeName;
                    @XmlAttribute(name = "RoomType")
                    protected String roomType;
                    @XmlAttribute(name = "ContractedRoomBlock")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger contractedRoomBlock;
                    @XmlAttribute(name = "FinalRoomBlock")
                    @XmlSchemaType(name = "nonNegativeInteger")
                    protected BigInteger finalRoomBlock;

                    /**
                     * Obtiene el valor de la propiedad rates.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates }
                     *     
                     */
                    public PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates getRates() {
                        return rates;
                    }

                    /**
                     * Define el valor de la propiedad rates.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates }
                     *     
                     */
                    public void setRates(PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates value) {
                        this.rates = value;
                    }

                    /**
                     * Gets the value of the roomPickUp property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomPickUp property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getRoomPickUp().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.RoomPickUp }
                     * 
                     * 
                     */
                    public List<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.RoomPickUp> getRoomPickUp() {
                        if (roomPickUp == null) {
                            roomPickUp = new ArrayList<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.RoomPickUp>();
                        }
                        return this.roomPickUp;
                    }

                    /**
                     * Obtiene el valor de la propiedad dayNumber.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getDayNumber() {
                        return dayNumber;
                    }

                    /**
                     * Define el valor de la propiedad dayNumber.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setDayNumber(BigInteger value) {
                        this.dayNumber = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dayType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link EventDayType }
                     *     
                     */
                    public EventDayType getDayType() {
                        return dayType;
                    }

                    /**
                     * Define el valor de la propiedad dayType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link EventDayType }
                     *     
                     */
                    public void setDayType(EventDayType value) {
                        this.dayType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad guestQuantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getGuestQuantity() {
                        return guestQuantity;
                    }

                    /**
                     * Define el valor de la propiedad guestQuantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setGuestQuantity(BigInteger value) {
                        this.guestQuantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad totalNumberOfUnits.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getTotalNumberOfUnits() {
                        return totalNumberOfUnits;
                    }

                    /**
                     * Define el valor de la propiedad totalNumberOfUnits.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setTotalNumberOfUnits(BigInteger value) {
                        this.totalNumberOfUnits = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad roomTypeCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRoomTypeCode() {
                        return roomTypeCode;
                    }

                    /**
                     * Define el valor de la propiedad roomTypeCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRoomTypeCode(String value) {
                        this.roomTypeCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad roomTypeName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRoomTypeName() {
                        return roomTypeName;
                    }

                    /**
                     * Define el valor de la propiedad roomTypeName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRoomTypeName(String value) {
                        this.roomTypeName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad roomType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRoomType() {
                        return roomType;
                    }

                    /**
                     * Define el valor de la propiedad roomType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRoomType(String value) {
                        this.roomType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad contractedRoomBlock.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getContractedRoomBlock() {
                        return contractedRoomBlock;
                    }

                    /**
                     * Define el valor de la propiedad contractedRoomBlock.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setContractedRoomBlock(BigInteger value) {
                        this.contractedRoomBlock = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad finalRoomBlock.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getFinalRoomBlock() {
                        return finalRoomBlock;
                    }

                    /**
                     * Define el valor de la propiedad finalRoomBlock.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setFinalRoomBlock(BigInteger value) {
                        this.finalRoomBlock = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="Rate" maxOccurs="99"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="OccupancyRate"&amp;gt;
                     *                   &amp;lt;simpleType&amp;gt;
                     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                     *                       &amp;lt;enumeration value="Flat"/&amp;gt;
                     *                       &amp;lt;enumeration value="Single"/&amp;gt;
                     *                       &amp;lt;enumeration value="Double"/&amp;gt;
                     *                       &amp;lt;enumeration value="Triple"/&amp;gt;
                     *                       &amp;lt;enumeration value="Quad"/&amp;gt;
                     *                     &amp;lt;/restriction&amp;gt;
                     *                   &amp;lt;/simpleType&amp;gt;
                     *                 &amp;lt;/attribute&amp;gt;
                     *               &amp;lt;/restriction&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "rate"
                    })
                    public static class Rates {

                        @XmlElement(name = "Rate", required = true)
                        protected List<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates.Rate> rate;

                        /**
                         * Gets the value of the rate property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rate property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getRate().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates.Rate }
                         * 
                         * 
                         */
                        public List<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates.Rate> getRate() {
                            if (rate == null) {
                                rate = new ArrayList<PostEventSiteReportType.RoomBlocks.RoomBlock.StayDays.StayDay.Rates.Rate>();
                            }
                            return this.rate;
                        }


                        /**
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="Taxes" type="{http://www.opentravel.org/OTA/2003/05}TaxesType" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}FeesType" minOccurs="0"/&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}CurrencyAmountGroup"/&amp;gt;
                         *       &amp;lt;attribute name="OccupancyRate"&amp;gt;
                         *         &amp;lt;simpleType&amp;gt;
                         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
                         *             &amp;lt;enumeration value="Flat"/&amp;gt;
                         *             &amp;lt;enumeration value="Single"/&amp;gt;
                         *             &amp;lt;enumeration value="Double"/&amp;gt;
                         *             &amp;lt;enumeration value="Triple"/&amp;gt;
                         *             &amp;lt;enumeration value="Quad"/&amp;gt;
                         *           &amp;lt;/restriction&amp;gt;
                         *         &amp;lt;/simpleType&amp;gt;
                         *       &amp;lt;/attribute&amp;gt;
                         *     &amp;lt;/restriction&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "taxes",
                            "fees"
                        })
                        public static class Rate {

                            @XmlElement(name = "Taxes")
                            protected TaxesType taxes;
                            @XmlElement(name = "Fees")
                            protected FeesType fees;
                            @XmlAttribute(name = "OccupancyRate")
                            protected String occupancyRate;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "DecimalPlaces")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger decimalPlaces;
                            @XmlAttribute(name = "Amount")
                            protected BigDecimal amount;

                            /**
                             * Obtiene el valor de la propiedad taxes.
                             * 
                             * @return
                             *     possible object is
                             *     {@link TaxesType }
                             *     
                             */
                            public TaxesType getTaxes() {
                                return taxes;
                            }

                            /**
                             * Define el valor de la propiedad taxes.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link TaxesType }
                             *     
                             */
                            public void setTaxes(TaxesType value) {
                                this.taxes = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad fees.
                             * 
                             * @return
                             *     possible object is
                             *     {@link FeesType }
                             *     
                             */
                            public FeesType getFees() {
                                return fees;
                            }

                            /**
                             * Define el valor de la propiedad fees.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link FeesType }
                             *     
                             */
                            public void setFees(FeesType value) {
                                this.fees = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad occupancyRate.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getOccupancyRate() {
                                return occupancyRate;
                            }

                            /**
                             * Define el valor de la propiedad occupancyRate.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setOccupancyRate(String value) {
                                this.occupancyRate = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad currencyCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Define el valor de la propiedad currencyCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad decimalPlaces.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDecimalPlaces() {
                                return decimalPlaces;
                            }

                            /**
                             * Define el valor de la propiedad decimalPlaces.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDecimalPlaces(BigInteger value) {
                                this.decimalPlaces = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad amount.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getAmount() {
                                return amount;
                            }

                            /**
                             * Define el valor de la propiedad amount.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setAmount(BigDecimal value) {
                                this.amount = value;
                            }

                        }

                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class RoomPickUp {

                        @XmlAttribute(name = "TotalNumberOfUnits")
                        protected BigInteger totalNumberOfUnits;
                        @XmlAttribute(name = "PickUpParameter")
                        protected String pickUpParameter;

                        /**
                         * Obtiene el valor de la propiedad totalNumberOfUnits.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getTotalNumberOfUnits() {
                            return totalNumberOfUnits;
                        }

                        /**
                         * Define el valor de la propiedad totalNumberOfUnits.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setTotalNumberOfUnits(BigInteger value) {
                            this.totalNumberOfUnits = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad pickUpParameter.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPickUpParameter() {
                            return pickUpParameter;
                        }

                        /**
                         * Define el valor de la propiedad pickUpParameter.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPickUpParameter(String value) {
                            this.pickUpParameter = value;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RoomPickUpGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class TotalRoomPickUp {

                @XmlAttribute(name = "TotalNumberOfUnits")
                protected BigInteger totalNumberOfUnits;
                @XmlAttribute(name = "PickUpParameter")
                protected String pickUpParameter;

                /**
                 * Obtiene el valor de la propiedad totalNumberOfUnits.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTotalNumberOfUnits() {
                    return totalNumberOfUnits;
                }

                /**
                 * Define el valor de la propiedad totalNumberOfUnits.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTotalNumberOfUnits(BigInteger value) {
                    this.totalNumberOfUnits = value;
                }

                /**
                 * Obtiene el valor de la propiedad pickUpParameter.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPickUpParameter() {
                    return pickUpParameter;
                }

                /**
                 * Define el valor de la propiedad pickUpParameter.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPickUpParameter(String value) {
                    this.pickUpParameter = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Comments" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TaxExemptGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "comments"
    })
    public static class TaxExemptInfo {

        @XmlElement(name = "Comments")
        protected PostEventSiteReportType.TaxExemptInfo.Comments comments;
        @XmlAttribute(name = "TaxExemptIndicator")
        protected Boolean taxExemptIndicator;
        @XmlAttribute(name = "TaxExemptID")
        protected String taxExemptID;
        @XmlAttribute(name = "TaxExemptType")
        protected String taxExemptType;

        /**
         * Obtiene el valor de la propiedad comments.
         * 
         * @return
         *     possible object is
         *     {@link PostEventSiteReportType.TaxExemptInfo.Comments }
         *     
         */
        public PostEventSiteReportType.TaxExemptInfo.Comments getComments() {
            return comments;
        }

        /**
         * Define el valor de la propiedad comments.
         * 
         * @param value
         *     allowed object is
         *     {@link PostEventSiteReportType.TaxExemptInfo.Comments }
         *     
         */
        public void setComments(PostEventSiteReportType.TaxExemptInfo.Comments value) {
            this.comments = value;
        }

        /**
         * Obtiene el valor de la propiedad taxExemptIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isTaxExemptIndicator() {
            return taxExemptIndicator;
        }

        /**
         * Define el valor de la propiedad taxExemptIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setTaxExemptIndicator(Boolean value) {
            this.taxExemptIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad taxExemptID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxExemptID() {
            return taxExemptID;
        }

        /**
         * Define el valor de la propiedad taxExemptID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxExemptID(String value) {
            this.taxExemptID = value;
        }

        /**
         * Obtiene el valor de la propiedad taxExemptType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxExemptType() {
            return taxExemptType;
        }

        /**
         * Define el valor de la propiedad taxExemptType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxExemptType(String value) {
            this.taxExemptType = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="unbounded"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "comment"
        })
        public static class Comments {

            @XmlElement(name = "Comment", required = true)
            protected List<ParagraphType> comment;

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link ParagraphType }
             * 
             * 
             */
            public List<ParagraphType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<ParagraphType>();
                }
                return this.comment;
            }

        }

    }

}
