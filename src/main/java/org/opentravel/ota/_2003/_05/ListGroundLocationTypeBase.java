
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_GroundLocationType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_GroundLocationType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Airport"/&amp;gt;
 *     &amp;lt;enumeration value="City"/&amp;gt;
 *     &amp;lt;enumeration value="Company"/&amp;gt;
 *     &amp;lt;enumeration value="Hotel"/&amp;gt;
 *     &amp;lt;enumeration value="PointOfInterest"/&amp;gt;
 *     &amp;lt;enumeration value="Port"/&amp;gt;
 *     &amp;lt;enumeration value="Region"/&amp;gt;
 *     &amp;lt;enumeration value="HomeResidence"/&amp;gt;
 *     &amp;lt;enumeration value="TrainStation"/&amp;gt;
 *     &amp;lt;enumeration value="Vicinity"/&amp;gt;
 *     &amp;lt;enumeration value="BusStation"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_GroundLocationType_Base")
@XmlEnum
public enum ListGroundLocationTypeBase {

    @XmlEnumValue("Airport")
    AIRPORT("Airport"),
    @XmlEnumValue("City")
    CITY("City"),
    @XmlEnumValue("Company")
    COMPANY("Company"),
    @XmlEnumValue("Hotel")
    HOTEL("Hotel"),
    @XmlEnumValue("PointOfInterest")
    POINT_OF_INTEREST("PointOfInterest"),
    @XmlEnumValue("Port")
    PORT("Port"),
    @XmlEnumValue("Region")
    REGION("Region"),
    @XmlEnumValue("HomeResidence")
    HOME_RESIDENCE("HomeResidence"),
    @XmlEnumValue("TrainStation")
    TRAIN_STATION("TrainStation"),
    @XmlEnumValue("Vicinity")
    VICINITY("Vicinity"),
    @XmlEnumValue("BusStation")
    BUS_STATION("BusStation"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListGroundLocationTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListGroundLocationTypeBase fromValue(String v) {
        for (ListGroundLocationTypeBase c: ListGroundLocationTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
