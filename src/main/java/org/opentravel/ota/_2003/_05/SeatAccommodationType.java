
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para SeatAccommodationType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="SeatAccommodationType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NotSignificant"/&amp;gt;
 *     &amp;lt;enumeration value="Seat"/&amp;gt;
 *     &amp;lt;enumeration value="Sleeperette"/&amp;gt;
 *     &amp;lt;enumeration value="NoSeat"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "SeatAccommodationType")
@XmlEnum
public enum SeatAccommodationType {

    @XmlEnumValue("NotSignificant")
    NOT_SIGNIFICANT("NotSignificant"),
    @XmlEnumValue("Seat")
    SEAT("Seat"),
    @XmlEnumValue("Sleeperette")
    SLEEPERETTE("Sleeperette"),
    @XmlEnumValue("NoSeat")
    NO_SEAT("NoSeat");
    private final String value;

    SeatAccommodationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeatAccommodationType fromValue(String v) {
        for (SeatAccommodationType c: SeatAccommodationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
