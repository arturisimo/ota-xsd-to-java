
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para BerthAccommodationType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="BerthAccommodationType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="NotSignificant"/&amp;gt;
 *     &amp;lt;enumeration value="Berth"/&amp;gt;
 *     &amp;lt;enumeration value="Couchette"/&amp;gt;
 *     &amp;lt;enumeration value="Sleeper"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "BerthAccommodationType")
@XmlEnum
public enum BerthAccommodationType {

    @XmlEnumValue("NotSignificant")
    NOT_SIGNIFICANT("NotSignificant"),
    @XmlEnumValue("Berth")
    BERTH("Berth"),
    @XmlEnumValue("Couchette")
    COUCHETTE("Couchette"),
    @XmlEnumValue("Sleeper")
    SLEEPER("Sleeper");
    private final String value;

    BerthAccommodationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BerthAccommodationType fromValue(String v) {
        for (BerthAccommodationType c: BerthAccommodationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
