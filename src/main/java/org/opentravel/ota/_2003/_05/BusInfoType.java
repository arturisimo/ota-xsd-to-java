
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines bus Information.
 * 
 * &lt;p&gt;Clase Java para BusInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="BusInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Bus" type="{http://www.opentravel.org/OTA/2003/05}BusIdentificationType"/&amp;gt;
 *         &amp;lt;element name="ValidDate" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="DelayTime" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="ScheduleCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusInfoType", propOrder = {
    "bus",
    "validDate"
})
public class BusInfoType {

    @XmlElement(name = "Bus", required = true)
    protected BusIdentificationType bus;
    @XmlElement(name = "ValidDate")
    protected BusInfoType.ValidDate validDate;
    @XmlAttribute(name = "DelayTime")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger delayTime;
    @XmlAttribute(name = "ScheduleCode")
    protected String scheduleCode;

    /**
     * Obtiene el valor de la propiedad bus.
     * 
     * @return
     *     possible object is
     *     {@link BusIdentificationType }
     *     
     */
    public BusIdentificationType getBus() {
        return bus;
    }

    /**
     * Define el valor de la propiedad bus.
     * 
     * @param value
     *     allowed object is
     *     {@link BusIdentificationType }
     *     
     */
    public void setBus(BusIdentificationType value) {
        this.bus = value;
    }

    /**
     * Obtiene el valor de la propiedad validDate.
     * 
     * @return
     *     possible object is
     *     {@link BusInfoType.ValidDate }
     *     
     */
    public BusInfoType.ValidDate getValidDate() {
        return validDate;
    }

    /**
     * Define el valor de la propiedad validDate.
     * 
     * @param value
     *     allowed object is
     *     {@link BusInfoType.ValidDate }
     *     
     */
    public void setValidDate(BusInfoType.ValidDate value) {
        this.validDate = value;
    }

    /**
     * Obtiene el valor de la propiedad delayTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDelayTime() {
        return delayTime;
    }

    /**
     * Define el valor de la propiedad delayTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDelayTime(BigInteger value) {
        this.delayTime = value;
    }

    /**
     * Obtiene el valor de la propiedad scheduleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleCode() {
        return scheduleCode;
    }

    /**
     * Define el valor de la propiedad scheduleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleCode(String value) {
        this.scheduleCode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DatePeriodGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ValidDate {

        @XmlAttribute(name = "StartPeriod")
        protected String startPeriod;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "EndPeriod")
        protected String endPeriod;

        /**
         * Obtiene el valor de la propiedad startPeriod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartPeriod() {
            return startPeriod;
        }

        /**
         * Define el valor de la propiedad startPeriod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartPeriod(String value) {
            this.startPeriod = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad endPeriod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndPeriod() {
            return endPeriod;
        }

        /**
         * Define el valor de la propiedad endPeriod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndPeriod(String value) {
            this.endPeriod = value;
        }

    }

}
