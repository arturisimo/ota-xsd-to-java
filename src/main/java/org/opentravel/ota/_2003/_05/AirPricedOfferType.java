
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Priced ancillary offer information.
 * 
 * &lt;p&gt;Clase Java para AirPricedOfferType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AirPricedOfferType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ServiceFamily" type="{http://www.opentravel.org/OTA/2003/05}AncillaryServiceDetailType"/&amp;gt;
 *         &amp;lt;element name="ShortDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="LongDescription" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Pricing"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PricingDetail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="OfferPricingRefID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                           &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="ApplyTo" type="{http://www.opentravel.org/OTA/2003/05}ApplyPriceToType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
 *                 &amp;lt;attribute name="OfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="PassengerQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                 &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
 *                 &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                 &amp;lt;attribute name="OfferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OriginDestination" maxOccurs="10" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                           &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SeatInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSeatMarketingClassType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="OtherServices" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirLandProductType"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TripInsurance" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="CoveredTraveler" maxOccurs="9" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SearchTravelerType"&amp;gt;
 *                           &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="PlanCost" type="{http://www.opentravel.org/OTA/2003/05}PlanCostType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                 &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="SellingComponentCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BookingInstruction" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="UpgradeMethod"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
 *                                 &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="BookingMethod"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
 *                       &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
 *                       &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="EMD_Type"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="EMD-S"/&amp;gt;
 *                       &amp;lt;enumeration value="EMD-A"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Restriction" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="TripMinOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="TripMaxOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerMinOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="TravelerMaxOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                 &amp;lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="TermsAndConditions" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="VoluntaryChanges" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType"&amp;gt;
 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="VoluntaryRefunds" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType"&amp;gt;
 *                           &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Other" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="ReusableFundsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Multimedia" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ImageDescriptionType"&amp;gt;
 *                 &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="ContentUsageType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BookingReferenceID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="BundleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="BundleID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="MandatoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="AcceptInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TripInsuranceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirPricedOfferType", propOrder = {
    "serviceFamily",
    "shortDescription",
    "longDescription",
    "pricing",
    "originDestination",
    "seatInfo",
    "otherServices",
    "tripInsurance",
    "bookingInstruction",
    "restriction",
    "termsAndConditions",
    "commission",
    "multimedia",
    "bookingReferenceID",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.AirOfferChoiceType.Priced.class
})
public class AirPricedOfferType {

    @XmlElement(name = "ServiceFamily", required = true)
    protected AncillaryServiceDetailType serviceFamily;
    @XmlElement(name = "ShortDescription")
    protected List<FormattedTextTextType> shortDescription;
    @XmlElement(name = "LongDescription")
    protected List<FormattedTextType> longDescription;
    @XmlElement(name = "Pricing", required = true)
    protected AirPricedOfferType.Pricing pricing;
    @XmlElement(name = "OriginDestination")
    protected List<AirPricedOfferType.OriginDestination> originDestination;
    @XmlElement(name = "SeatInfo")
    protected AirPricedOfferType.SeatInfo seatInfo;
    @XmlElement(name = "OtherServices")
    protected List<AirPricedOfferType.OtherServices> otherServices;
    @XmlElement(name = "TripInsurance")
    protected AirPricedOfferType.TripInsurance tripInsurance;
    @XmlElement(name = "BookingInstruction")
    protected AirPricedOfferType.BookingInstruction bookingInstruction;
    @XmlElement(name = "Restriction")
    protected List<AirPricedOfferType.Restriction> restriction;
    @XmlElement(name = "TermsAndConditions")
    protected List<AirPricedOfferType.TermsAndConditions> termsAndConditions;
    @XmlElement(name = "Commission")
    protected List<CommissionType> commission;
    @XmlElement(name = "Multimedia")
    protected List<AirPricedOfferType.Multimedia> multimedia;
    @XmlElement(name = "BookingReferenceID")
    protected List<UniqueIDType> bookingReferenceID;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "ID")
    protected String id;
    @XmlAttribute(name = "BundleInd")
    protected Boolean bundleInd;
    @XmlAttribute(name = "BundleID")
    protected String bundleID;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "MandatoryInd")
    protected Boolean mandatoryInd;
    @XmlAttribute(name = "AcceptInd")
    protected Boolean acceptInd;
    @XmlAttribute(name = "TripInsuranceInd")
    protected Boolean tripInsuranceInd;

    /**
     * Obtiene el valor de la propiedad serviceFamily.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServiceDetailType }
     *     
     */
    public AncillaryServiceDetailType getServiceFamily() {
        return serviceFamily;
    }

    /**
     * Define el valor de la propiedad serviceFamily.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServiceDetailType }
     *     
     */
    public void setServiceFamily(AncillaryServiceDetailType value) {
        this.serviceFamily = value;
    }

    /**
     * Gets the value of the shortDescription property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the shortDescription property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getShortDescription().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FormattedTextTextType }
     * 
     * 
     */
    public List<FormattedTextTextType> getShortDescription() {
        if (shortDescription == null) {
            shortDescription = new ArrayList<FormattedTextTextType>();
        }
        return this.shortDescription;
    }

    /**
     * Gets the value of the longDescription property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the longDescription property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getLongDescription().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FormattedTextType }
     * 
     * 
     */
    public List<FormattedTextType> getLongDescription() {
        if (longDescription == null) {
            longDescription = new ArrayList<FormattedTextType>();
        }
        return this.longDescription;
    }

    /**
     * Obtiene el valor de la propiedad pricing.
     * 
     * @return
     *     possible object is
     *     {@link AirPricedOfferType.Pricing }
     *     
     */
    public AirPricedOfferType.Pricing getPricing() {
        return pricing;
    }

    /**
     * Define el valor de la propiedad pricing.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricedOfferType.Pricing }
     *     
     */
    public void setPricing(AirPricedOfferType.Pricing value) {
        this.pricing = value;
    }

    /**
     * Gets the value of the originDestination property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestination property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOriginDestination().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPricedOfferType.OriginDestination }
     * 
     * 
     */
    public List<AirPricedOfferType.OriginDestination> getOriginDestination() {
        if (originDestination == null) {
            originDestination = new ArrayList<AirPricedOfferType.OriginDestination>();
        }
        return this.originDestination;
    }

    /**
     * Obtiene el valor de la propiedad seatInfo.
     * 
     * @return
     *     possible object is
     *     {@link AirPricedOfferType.SeatInfo }
     *     
     */
    public AirPricedOfferType.SeatInfo getSeatInfo() {
        return seatInfo;
    }

    /**
     * Define el valor de la propiedad seatInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricedOfferType.SeatInfo }
     *     
     */
    public void setSeatInfo(AirPricedOfferType.SeatInfo value) {
        this.seatInfo = value;
    }

    /**
     * Gets the value of the otherServices property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the otherServices property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOtherServices().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPricedOfferType.OtherServices }
     * 
     * 
     */
    public List<AirPricedOfferType.OtherServices> getOtherServices() {
        if (otherServices == null) {
            otherServices = new ArrayList<AirPricedOfferType.OtherServices>();
        }
        return this.otherServices;
    }

    /**
     * Obtiene el valor de la propiedad tripInsurance.
     * 
     * @return
     *     possible object is
     *     {@link AirPricedOfferType.TripInsurance }
     *     
     */
    public AirPricedOfferType.TripInsurance getTripInsurance() {
        return tripInsurance;
    }

    /**
     * Define el valor de la propiedad tripInsurance.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricedOfferType.TripInsurance }
     *     
     */
    public void setTripInsurance(AirPricedOfferType.TripInsurance value) {
        this.tripInsurance = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingInstruction.
     * 
     * @return
     *     possible object is
     *     {@link AirPricedOfferType.BookingInstruction }
     *     
     */
    public AirPricedOfferType.BookingInstruction getBookingInstruction() {
        return bookingInstruction;
    }

    /**
     * Define el valor de la propiedad bookingInstruction.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricedOfferType.BookingInstruction }
     *     
     */
    public void setBookingInstruction(AirPricedOfferType.BookingInstruction value) {
        this.bookingInstruction = value;
    }

    /**
     * Gets the value of the restriction property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restriction property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRestriction().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPricedOfferType.Restriction }
     * 
     * 
     */
    public List<AirPricedOfferType.Restriction> getRestriction() {
        if (restriction == null) {
            restriction = new ArrayList<AirPricedOfferType.Restriction>();
        }
        return this.restriction;
    }

    /**
     * Gets the value of the termsAndConditions property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the termsAndConditions property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTermsAndConditions().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPricedOfferType.TermsAndConditions }
     * 
     * 
     */
    public List<AirPricedOfferType.TermsAndConditions> getTermsAndConditions() {
        if (termsAndConditions == null) {
            termsAndConditions = new ArrayList<AirPricedOfferType.TermsAndConditions>();
        }
        return this.termsAndConditions;
    }

    /**
     * Gets the value of the commission property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the commission property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCommission().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CommissionType }
     * 
     * 
     */
    public List<CommissionType> getCommission() {
        if (commission == null) {
            commission = new ArrayList<CommissionType>();
        }
        return this.commission;
    }

    /**
     * Gets the value of the multimedia property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the multimedia property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getMultimedia().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AirPricedOfferType.Multimedia }
     * 
     * 
     */
    public List<AirPricedOfferType.Multimedia> getMultimedia() {
        if (multimedia == null) {
            multimedia = new ArrayList<AirPricedOfferType.Multimedia>();
        }
        return this.multimedia;
    }

    /**
     * Gets the value of the bookingReferenceID property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingReferenceID property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBookingReferenceID().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link UniqueIDType }
     * 
     * 
     */
    public List<UniqueIDType> getBookingReferenceID() {
        if (bookingReferenceID == null) {
            bookingReferenceID = new ArrayList<UniqueIDType>();
        }
        return this.bookingReferenceID;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad bundleInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBundleInd() {
        return bundleInd;
    }

    /**
     * Define el valor de la propiedad bundleInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBundleInd(Boolean value) {
        this.bundleInd = value;
    }

    /**
     * Obtiene el valor de la propiedad bundleID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundleID() {
        return bundleID;
    }

    /**
     * Define el valor de la propiedad bundleID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundleID(String value) {
        this.bundleID = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad mandatoryInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMandatoryInd() {
        return mandatoryInd;
    }

    /**
     * Define el valor de la propiedad mandatoryInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMandatoryInd(Boolean value) {
        this.mandatoryInd = value;
    }

    /**
     * Obtiene el valor de la propiedad acceptInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAcceptInd() {
        return acceptInd;
    }

    /**
     * Define el valor de la propiedad acceptInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAcceptInd(Boolean value) {
        this.acceptInd = value;
    }

    /**
     * Obtiene el valor de la propiedad tripInsuranceInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTripInsuranceInd() {
        return tripInsuranceInd;
    }

    /**
     * Define el valor de la propiedad tripInsuranceInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTripInsuranceInd(Boolean value) {
        this.tripInsuranceInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SSR_Info" type="{http://www.opentravel.org/OTA/2003/05}SpecialServiceRequestType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="OSI_Info" type="{http://www.opentravel.org/OTA/2003/05}OtherServiceInfoType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Upgrade" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="UpgradeMethod"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
     *                       &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="BookingMethod"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="SSR_SpecialServiceRequest"/&amp;gt;
     *             &amp;lt;enumeration value="OSI_OtherServiceInformation"/&amp;gt;
     *             &amp;lt;enumeration value="API_AdvancedPassengerInformation"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="EMD_Type"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="EMD-S"/&amp;gt;
     *             &amp;lt;enumeration value="EMD-A"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ssrInfo",
        "osiInfo",
        "upgrade"
    })
    public static class BookingInstruction {

        @XmlElement(name = "SSR_Info")
        protected List<SpecialServiceRequestType> ssrInfo;
        @XmlElement(name = "OSI_Info")
        protected List<OtherServiceInfoType> osiInfo;
        @XmlElement(name = "Upgrade")
        protected AirPricedOfferType.BookingInstruction.Upgrade upgrade;
        @XmlAttribute(name = "BookingMethod")
        protected String bookingMethod;
        @XmlAttribute(name = "EMD_Type")
        protected String emdType;

        /**
         * Gets the value of the ssrInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ssrInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getSSRInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link SpecialServiceRequestType }
         * 
         * 
         */
        public List<SpecialServiceRequestType> getSSRInfo() {
            if (ssrInfo == null) {
                ssrInfo = new ArrayList<SpecialServiceRequestType>();
            }
            return this.ssrInfo;
        }

        /**
         * Gets the value of the osiInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the osiInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOSIInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OtherServiceInfoType }
         * 
         * 
         */
        public List<OtherServiceInfoType> getOSIInfo() {
            if (osiInfo == null) {
                osiInfo = new ArrayList<OtherServiceInfoType>();
            }
            return this.osiInfo;
        }

        /**
         * Obtiene el valor de la propiedad upgrade.
         * 
         * @return
         *     possible object is
         *     {@link AirPricedOfferType.BookingInstruction.Upgrade }
         *     
         */
        public AirPricedOfferType.BookingInstruction.Upgrade getUpgrade() {
            return upgrade;
        }

        /**
         * Define el valor de la propiedad upgrade.
         * 
         * @param value
         *     allowed object is
         *     {@link AirPricedOfferType.BookingInstruction.Upgrade }
         *     
         */
        public void setUpgrade(AirPricedOfferType.BookingInstruction.Upgrade value) {
            this.upgrade = value;
        }

        /**
         * Obtiene el valor de la propiedad bookingMethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBookingMethod() {
            return bookingMethod;
        }

        /**
         * Define el valor de la propiedad bookingMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBookingMethod(String value) {
            this.bookingMethod = value;
        }

        /**
         * Obtiene el valor de la propiedad emdType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEMDType() {
            return emdType;
        }

        /**
         * Define el valor de la propiedad emdType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEMDType(String value) {
            this.emdType = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Instruction" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="UpgradeMethod"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="S_SpecialServiceRequest"/&amp;gt;
         *             &amp;lt;enumeration value="A_AutoUpgrade"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="UpgradeDesigCode" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "instruction"
        })
        public static class Upgrade {

            @XmlElement(name = "Instruction")
            protected List<String> instruction;
            @XmlAttribute(name = "UpgradeMethod")
            protected String upgradeMethod;
            @XmlAttribute(name = "UpgradeDesigCode")
            protected String upgradeDesigCode;

            /**
             * Gets the value of the instruction property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the instruction property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getInstruction().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getInstruction() {
                if (instruction == null) {
                    instruction = new ArrayList<String>();
                }
                return this.instruction;
            }

            /**
             * Obtiene el valor de la propiedad upgradeMethod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUpgradeMethod() {
                return upgradeMethod;
            }

            /**
             * Define el valor de la propiedad upgradeMethod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUpgradeMethod(String value) {
                this.upgradeMethod = value;
            }

            /**
             * Obtiene el valor de la propiedad upgradeDesigCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUpgradeDesigCode() {
                return upgradeDesigCode;
            }

            /**
             * Define el valor de la propiedad upgradeDesigCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUpgradeDesigCode(String value) {
                this.upgradeDesigCode = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ImageDescriptionType"&amp;gt;
     *       &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="ContentUsageType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Multimedia
        extends ImageDescriptionType
    {

        @XmlAttribute(name = "Sequence")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger sequence;
        @XmlAttribute(name = "ContentUsageType")
        protected String contentUsageType;

        /**
         * Obtiene el valor de la propiedad sequence.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequence() {
            return sequence;
        }

        /**
         * Define el valor de la propiedad sequence.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequence(BigInteger value) {
            this.sequence = value;
        }

        /**
         * Obtiene el valor de la propiedad contentUsageType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContentUsageType() {
            return contentUsageType;
        }

        /**
         * Define el valor de la propiedad contentUsageType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContentUsageType(String value) {
            this.contentUsageType = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AlternateLocationInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *                 &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alternateLocationInfo",
        "tpaExtensions"
    })
    public static class OriginDestination
        extends OriginDestinationInformationType
    {

        @XmlElement(name = "AlternateLocationInfo")
        protected AirPricedOfferType.OriginDestination.AlternateLocationInfo alternateLocationInfo;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Obtiene el valor de la propiedad alternateLocationInfo.
         * 
         * @return
         *     possible object is
         *     {@link AirPricedOfferType.OriginDestination.AlternateLocationInfo }
         *     
         */
        public AirPricedOfferType.OriginDestination.AlternateLocationInfo getAlternateLocationInfo() {
            return alternateLocationInfo;
        }

        /**
         * Define el valor de la propiedad alternateLocationInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AirPricedOfferType.OriginDestination.AlternateLocationInfo }
         *     
         */
        public void setAlternateLocationInfo(AirPricedOfferType.OriginDestination.AlternateLocationInfo value) {
            this.alternateLocationInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *       &amp;lt;attribute name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}ListOfStringLength1to8" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AlternateLocationInfo {

            @XmlAttribute(name = "OriginLocation")
            protected List<String> originLocation;
            @XmlAttribute(name = "DestinationLocation")
            protected List<String> destinationLocation;

            /**
             * Gets the value of the originLocation property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originLocation property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getOriginLocation().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getOriginLocation() {
                if (originLocation == null) {
                    originLocation = new ArrayList<String>();
                }
                return this.originLocation;
            }

            /**
             * Gets the value of the destinationLocation property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the destinationLocation property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDestinationLocation().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getDestinationLocation() {
                if (destinationLocation == null) {
                    destinationLocation = new ArrayList<String>();
                }
                return this.destinationLocation;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirLandProductType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OtherServices
        extends AirLandProductType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PricingDetail" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="OfferPricingRefID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                 &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="AppliedRule" type="{http://www.opentravel.org/OTA/2003/05}AppliedRuleType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PricingQualifier" type="{http://www.opentravel.org/OTA/2003/05}AirPricingQualifierType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="ApplyTo" type="{http://www.opentravel.org/OTA/2003/05}ApplyPriceToType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ExchangeRateGroup"/&amp;gt;
     *       &amp;lt;attribute name="OfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="PassengerQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *       &amp;lt;attribute name="PricingCurrency" type="{http://www.opentravel.org/OTA/2003/05}AlphaLength3" /&amp;gt;
     *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="BaseNUC_Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *       &amp;lt;attribute name="OfferRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}ListOfRPH" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pricingDetail",
        "taxInfo",
        "redemptionPoints",
        "appliedRule",
        "pricingQualifier",
        "applyTo"
    })
    public static class Pricing {

        @XmlElement(name = "PricingDetail")
        protected List<AirPricedOfferType.Pricing.PricingDetail> pricingDetail;
        @XmlElement(name = "TaxInfo")
        protected List<TaxType> taxInfo;
        @XmlElement(name = "RedemptionPoints")
        protected AirRedemptionMilesType redemptionPoints;
        @XmlElement(name = "AppliedRule")
        protected List<AppliedRuleType> appliedRule;
        @XmlElement(name = "PricingQualifier")
        protected List<AirPricingQualifierType> pricingQualifier;
        @XmlElement(name = "ApplyTo")
        protected ApplyPriceToType applyTo;
        @XmlAttribute(name = "OfferQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger offerQty;
        @XmlAttribute(name = "PassengerQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger passengerQty;
        @XmlAttribute(name = "PreTaxAmount")
        protected BigDecimal preTaxAmount;
        @XmlAttribute(name = "TaxAmount")
        protected BigDecimal taxAmount;
        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;
        @XmlAttribute(name = "PricingCurrency")
        protected String pricingCurrency;
        @XmlAttribute(name = "DecimalPlaces")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger decimalPlaces;
        @XmlAttribute(name = "BaseNUC_Amount")
        protected BigDecimal baseNUCAmount;
        @XmlAttribute(name = "OfferRPH")
        protected String offerRPH;
        @XmlAttribute(name = "TravelerRPH")
        protected List<String> travelerRPH;
        @XmlAttribute(name = "FromCurrency")
        protected String fromCurrency;
        @XmlAttribute(name = "ToCurrency")
        protected String toCurrency;
        @XmlAttribute(name = "Rate")
        protected BigDecimal rate;
        @XmlAttribute(name = "Date")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar date;

        /**
         * Gets the value of the pricingDetail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pricingDetail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPricingDetail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirPricedOfferType.Pricing.PricingDetail }
         * 
         * 
         */
        public List<AirPricedOfferType.Pricing.PricingDetail> getPricingDetail() {
            if (pricingDetail == null) {
                pricingDetail = new ArrayList<AirPricedOfferType.Pricing.PricingDetail>();
            }
            return this.pricingDetail;
        }

        /**
         * Gets the value of the taxInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTaxInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link TaxType }
         * 
         * 
         */
        public List<TaxType> getTaxInfo() {
            if (taxInfo == null) {
                taxInfo = new ArrayList<TaxType>();
            }
            return this.taxInfo;
        }

        /**
         * Obtiene el valor de la propiedad redemptionPoints.
         * 
         * @return
         *     possible object is
         *     {@link AirRedemptionMilesType }
         *     
         */
        public AirRedemptionMilesType getRedemptionPoints() {
            return redemptionPoints;
        }

        /**
         * Define el valor de la propiedad redemptionPoints.
         * 
         * @param value
         *     allowed object is
         *     {@link AirRedemptionMilesType }
         *     
         */
        public void setRedemptionPoints(AirRedemptionMilesType value) {
            this.redemptionPoints = value;
        }

        /**
         * Gets the value of the appliedRule property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the appliedRule property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAppliedRule().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AppliedRuleType }
         * 
         * 
         */
        public List<AppliedRuleType> getAppliedRule() {
            if (appliedRule == null) {
                appliedRule = new ArrayList<AppliedRuleType>();
            }
            return this.appliedRule;
        }

        /**
         * Gets the value of the pricingQualifier property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the pricingQualifier property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPricingQualifier().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirPricingQualifierType }
         * 
         * 
         */
        public List<AirPricingQualifierType> getPricingQualifier() {
            if (pricingQualifier == null) {
                pricingQualifier = new ArrayList<AirPricingQualifierType>();
            }
            return this.pricingQualifier;
        }

        /**
         * Obtiene el valor de la propiedad applyTo.
         * 
         * @return
         *     possible object is
         *     {@link ApplyPriceToType }
         *     
         */
        public ApplyPriceToType getApplyTo() {
            return applyTo;
        }

        /**
         * Define el valor de la propiedad applyTo.
         * 
         * @param value
         *     allowed object is
         *     {@link ApplyPriceToType }
         *     
         */
        public void setApplyTo(ApplyPriceToType value) {
            this.applyTo = value;
        }

        /**
         * Obtiene el valor de la propiedad offerQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getOfferQty() {
            return offerQty;
        }

        /**
         * Define el valor de la propiedad offerQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setOfferQty(BigInteger value) {
            this.offerQty = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPassengerQty() {
            return passengerQty;
        }

        /**
         * Define el valor de la propiedad passengerQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPassengerQty(BigInteger value) {
            this.passengerQty = value;
        }

        /**
         * Obtiene el valor de la propiedad preTaxAmount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPreTaxAmount() {
            return preTaxAmount;
        }

        /**
         * Define el valor de la propiedad preTaxAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPreTaxAmount(BigDecimal value) {
            this.preTaxAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad taxAmount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTaxAmount() {
            return taxAmount;
        }

        /**
         * Define el valor de la propiedad taxAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTaxAmount(BigDecimal value) {
            this.taxAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad pricingCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPricingCurrency() {
            return pricingCurrency;
        }

        /**
         * Define el valor de la propiedad pricingCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPricingCurrency(String value) {
            this.pricingCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad decimalPlaces.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDecimalPlaces() {
            return decimalPlaces;
        }

        /**
         * Define el valor de la propiedad decimalPlaces.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDecimalPlaces(BigInteger value) {
            this.decimalPlaces = value;
        }

        /**
         * Obtiene el valor de la propiedad baseNUCAmount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getBaseNUCAmount() {
            return baseNUCAmount;
        }

        /**
         * Define el valor de la propiedad baseNUCAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setBaseNUCAmount(BigDecimal value) {
            this.baseNUCAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad offerRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfferRPH() {
            return offerRPH;
        }

        /**
         * Define el valor de la propiedad offerRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfferRPH(String value) {
            this.offerRPH = value;
        }

        /**
         * Gets the value of the travelerRPH property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the travelerRPH property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getTravelerRPH().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getTravelerRPH() {
            if (travelerRPH == null) {
                travelerRPH = new ArrayList<String>();
            }
            return this.travelerRPH;
        }

        /**
         * Obtiene el valor de la propiedad fromCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFromCurrency() {
            return fromCurrency;
        }

        /**
         * Define el valor de la propiedad fromCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFromCurrency(String value) {
            this.fromCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad toCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToCurrency() {
            return toCurrency;
        }

        /**
         * Define el valor de la propiedad toCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToCurrency(String value) {
            this.toCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad rate.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getRate() {
            return rate;
        }

        /**
         * Define el valor de la propiedad rate.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setRate(BigDecimal value) {
            this.rate = value;
        }

        /**
         * Obtiene el valor de la propiedad date.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Define el valor de la propiedad date.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="TaxInfo" type="{http://www.opentravel.org/OTA/2003/05}TaxType" maxOccurs="99" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RedemptionPoints" type="{http://www.opentravel.org/OTA/2003/05}AirRedemptionMilesType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="OfferPricingRefID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *       &amp;lt;attribute name="PreTaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="TaxAmount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "taxInfo",
            "redemptionPoints"
        })
        public static class PricingDetail {

            @XmlElement(name = "TaxInfo")
            protected List<TaxType> taxInfo;
            @XmlElement(name = "RedemptionPoints")
            protected AirRedemptionMilesType redemptionPoints;
            @XmlAttribute(name = "TravelerRPH")
            protected String travelerRPH;
            @XmlAttribute(name = "OfferPricingRefID")
            protected String offerPricingRefID;
            @XmlAttribute(name = "PreTaxAmount")
            protected BigDecimal preTaxAmount;
            @XmlAttribute(name = "TaxAmount")
            protected BigDecimal taxAmount;
            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;

            /**
             * Gets the value of the taxInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getTaxInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link TaxType }
             * 
             * 
             */
            public List<TaxType> getTaxInfo() {
                if (taxInfo == null) {
                    taxInfo = new ArrayList<TaxType>();
                }
                return this.taxInfo;
            }

            /**
             * Obtiene el valor de la propiedad redemptionPoints.
             * 
             * @return
             *     possible object is
             *     {@link AirRedemptionMilesType }
             *     
             */
            public AirRedemptionMilesType getRedemptionPoints() {
                return redemptionPoints;
            }

            /**
             * Define el valor de la propiedad redemptionPoints.
             * 
             * @param value
             *     allowed object is
             *     {@link AirRedemptionMilesType }
             *     
             */
            public void setRedemptionPoints(AirRedemptionMilesType value) {
                this.redemptionPoints = value;
            }

            /**
             * Obtiene el valor de la propiedad travelerRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelerRPH() {
                return travelerRPH;
            }

            /**
             * Define el valor de la propiedad travelerRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelerRPH(String value) {
                this.travelerRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad offerPricingRefID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOfferPricingRefID() {
                return offerPricingRefID;
            }

            /**
             * Define el valor de la propiedad offerPricingRefID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOfferPricingRefID(String value) {
                this.offerPricingRefID = value;
            }

            /**
             * Obtiene el valor de la propiedad preTaxAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPreTaxAmount() {
                return preTaxAmount;
            }

            /**
             * Define el valor de la propiedad preTaxAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPreTaxAmount(BigDecimal value) {
                this.preTaxAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad taxAmount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTaxAmount() {
                return taxAmount;
            }

            /**
             * Define el valor de la propiedad taxAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTaxAmount(BigDecimal value) {
                this.taxAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="TripMinOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="TripMaxOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="TravelerMinOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="TravelerMaxOfferQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *       &amp;lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "description"
    })
    public static class Restriction {

        @XmlElement(name = "Description")
        protected String description;
        @XmlAttribute(name = "TripMinOfferQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger tripMinOfferQty;
        @XmlAttribute(name = "TripMaxOfferQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger tripMaxOfferQty;
        @XmlAttribute(name = "TravelerMinOfferQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger travelerMinOfferQty;
        @XmlAttribute(name = "TravelerMaxOfferQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger travelerMaxOfferQty;
        @XmlAttribute(name = "EffectiveDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar effectiveDate;
        @XmlAttribute(name = "ExpireDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar expireDate;

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad tripMinOfferQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTripMinOfferQty() {
            return tripMinOfferQty;
        }

        /**
         * Define el valor de la propiedad tripMinOfferQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTripMinOfferQty(BigInteger value) {
            this.tripMinOfferQty = value;
        }

        /**
         * Obtiene el valor de la propiedad tripMaxOfferQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTripMaxOfferQty() {
            return tripMaxOfferQty;
        }

        /**
         * Define el valor de la propiedad tripMaxOfferQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTripMaxOfferQty(BigInteger value) {
            this.tripMaxOfferQty = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerMinOfferQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTravelerMinOfferQty() {
            return travelerMinOfferQty;
        }

        /**
         * Define el valor de la propiedad travelerMinOfferQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTravelerMinOfferQty(BigInteger value) {
            this.travelerMinOfferQty = value;
        }

        /**
         * Obtiene el valor de la propiedad travelerMaxOfferQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTravelerMaxOfferQty() {
            return travelerMaxOfferQty;
        }

        /**
         * Define el valor de la propiedad travelerMaxOfferQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTravelerMaxOfferQty(BigInteger value) {
            this.travelerMaxOfferQty = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Define el valor de la propiedad effectiveDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEffectiveDate(XMLGregorianCalendar value) {
            this.effectiveDate = value;
        }

        /**
         * Obtiene el valor de la propiedad expireDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpireDate() {
            return expireDate;
        }

        /**
         * Define el valor de la propiedad expireDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpireDate(XMLGregorianCalendar value) {
            this.expireDate = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AirSeatMarketingClassType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SeatInfo
        extends AirSeatMarketingClassType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VoluntaryChanges" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType"&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="VoluntaryRefunds" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType"&amp;gt;
     *                 &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Other" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="ReusableFundsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "voluntaryChanges",
        "voluntaryRefunds",
        "other"
    })
    public static class TermsAndConditions {

        @XmlElement(name = "VoluntaryChanges")
        protected AirPricedOfferType.TermsAndConditions.VoluntaryChanges voluntaryChanges;
        @XmlElement(name = "VoluntaryRefunds")
        protected AirPricedOfferType.TermsAndConditions.VoluntaryRefunds voluntaryRefunds;
        @XmlElement(name = "Other")
        protected String other;
        @XmlAttribute(name = "RefundableInd")
        protected Boolean refundableInd;
        @XmlAttribute(name = "ReusableFundsInd")
        protected Boolean reusableFundsInd;

        /**
         * Obtiene el valor de la propiedad voluntaryChanges.
         * 
         * @return
         *     possible object is
         *     {@link AirPricedOfferType.TermsAndConditions.VoluntaryChanges }
         *     
         */
        public AirPricedOfferType.TermsAndConditions.VoluntaryChanges getVoluntaryChanges() {
            return voluntaryChanges;
        }

        /**
         * Define el valor de la propiedad voluntaryChanges.
         * 
         * @param value
         *     allowed object is
         *     {@link AirPricedOfferType.TermsAndConditions.VoluntaryChanges }
         *     
         */
        public void setVoluntaryChanges(AirPricedOfferType.TermsAndConditions.VoluntaryChanges value) {
            this.voluntaryChanges = value;
        }

        /**
         * Obtiene el valor de la propiedad voluntaryRefunds.
         * 
         * @return
         *     possible object is
         *     {@link AirPricedOfferType.TermsAndConditions.VoluntaryRefunds }
         *     
         */
        public AirPricedOfferType.TermsAndConditions.VoluntaryRefunds getVoluntaryRefunds() {
            return voluntaryRefunds;
        }

        /**
         * Define el valor de la propiedad voluntaryRefunds.
         * 
         * @param value
         *     allowed object is
         *     {@link AirPricedOfferType.TermsAndConditions.VoluntaryRefunds }
         *     
         */
        public void setVoluntaryRefunds(AirPricedOfferType.TermsAndConditions.VoluntaryRefunds value) {
            this.voluntaryRefunds = value;
        }

        /**
         * Obtiene el valor de la propiedad other.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOther() {
            return other;
        }

        /**
         * Define el valor de la propiedad other.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOther(String value) {
            this.other = value;
        }

        /**
         * Obtiene el valor de la propiedad refundableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRefundableInd() {
            return refundableInd;
        }

        /**
         * Define el valor de la propiedad refundableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRefundableInd(Boolean value) {
            this.refundableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad reusableFundsInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReusableFundsInd() {
            return reusableFundsInd;
        }

        /**
         * Define el valor de la propiedad reusableFundsInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReusableFundsInd(Boolean value) {
            this.reusableFundsInd = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType"&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class VoluntaryChanges
            extends VoluntaryChangesType
        {

            @XmlAttribute(name = "Description")
            protected String description;

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VoluntaryChangesType"&amp;gt;
         *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class VoluntaryRefunds
            extends VoluntaryChangesType
        {

            @XmlAttribute(name = "Description")
            protected String description;

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CoveredTraveler" maxOccurs="9" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SearchTravelerType"&amp;gt;
     *                 &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CoverageLimit" type="{http://www.opentravel.org/OTA/2003/05}CoverageLimitType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="PlanCost" type="{http://www.opentravel.org/OTA/2003/05}PlanCostType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *       &amp;lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="SellingComponentCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "coveredTraveler",
        "coverageLimit",
        "planCost"
    })
    public static class TripInsurance {

        @XmlElement(name = "CoveredTraveler")
        protected List<AirPricedOfferType.TripInsurance.CoveredTraveler> coveredTraveler;
        @XmlElement(name = "CoverageLimit")
        protected CoverageLimitType coverageLimit;
        @XmlElement(name = "PlanCost")
        protected PlanCostType planCost;
        @XmlAttribute(name = "Code")
        protected String code;
        @XmlAttribute(name = "SellingComponentCode")
        protected String sellingComponentCode;
        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;

        /**
         * Gets the value of the coveredTraveler property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the coveredTraveler property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCoveredTraveler().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link AirPricedOfferType.TripInsurance.CoveredTraveler }
         * 
         * 
         */
        public List<AirPricedOfferType.TripInsurance.CoveredTraveler> getCoveredTraveler() {
            if (coveredTraveler == null) {
                coveredTraveler = new ArrayList<AirPricedOfferType.TripInsurance.CoveredTraveler>();
            }
            return this.coveredTraveler;
        }

        /**
         * Obtiene el valor de la propiedad coverageLimit.
         * 
         * @return
         *     possible object is
         *     {@link CoverageLimitType }
         *     
         */
        public CoverageLimitType getCoverageLimit() {
            return coverageLimit;
        }

        /**
         * Define el valor de la propiedad coverageLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link CoverageLimitType }
         *     
         */
        public void setCoverageLimit(CoverageLimitType value) {
            this.coverageLimit = value;
        }

        /**
         * Obtiene el valor de la propiedad planCost.
         * 
         * @return
         *     possible object is
         *     {@link PlanCostType }
         *     
         */
        public PlanCostType getPlanCost() {
            return planCost;
        }

        /**
         * Define el valor de la propiedad planCost.
         * 
         * @param value
         *     allowed object is
         *     {@link PlanCostType }
         *     
         */
        public void setPlanCost(PlanCostType value) {
            this.planCost = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad sellingComponentCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSellingComponentCode() {
            return sellingComponentCode;
        }

        /**
         * Define el valor de la propiedad sellingComponentCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSellingComponentCode(String value) {
            this.sellingComponentCode = value;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SearchTravelerType"&amp;gt;
         *       &amp;lt;attribute name="TravelerRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CoveredTraveler
            extends SearchTravelerType
        {

            @XmlAttribute(name = "TravelerRPH")
            protected String travelerRPH;

            /**
             * Obtiene el valor de la propiedad travelerRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTravelerRPH() {
                return travelerRPH;
            }

            /**
             * Define el valor de la propiedad travelerRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTravelerRPH(String value) {
                this.travelerRPH = value;
            }

        }

    }

}
