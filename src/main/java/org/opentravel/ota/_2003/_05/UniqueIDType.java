
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * An identifier used to uniquely reference an object in a system (e.g. an airline reservation reference, customer profile reference, booking confirmation number, or a reference to a previous availability quote).
 * 
 * &lt;p&gt;Clase Java para UniqueID_Type complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="UniqueID_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="CompanyName" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}UniqueID_Group"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UniqueID_Type", propOrder = {
    "companyName",
    "tpaExtensions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirBaggageRQ.PNR.class,
    org.opentravel.ota._2003._05.AirCheckInType.PassengerFlightInfo.BookingInfo.BookingReferenceID.class,
    org.opentravel.ota._2003._05.AirReservationType.BookingReferenceID.class,
    org.opentravel.ota._2003._05.AuthorizationType.BookingReferenceID.class,
    org.opentravel.ota._2003._05.OTACancelRQ.UniqueID.class,
    org.opentravel.ota._2003._05.OTACruiseBookingDocumentRS.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruiseBookingHistoryRS.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruiseBookRS.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruiseCancellationPricingRQ.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruiseCancellationPricingRS.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruisePaymentRQ.ReservationPayment.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruisePaymentRS.ReservationPayment.ReservationID.class,
    ReservationIDType.class,
    org.opentravel.ota._2003._05.OTACruisePNRUpdateNotifRS.ReservationID.class,
    org.opentravel.ota._2003._05.OTACruisePriceBookingRS.ReservationID.class,
    org.opentravel.ota._2003._05.OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay.Reference.class,
    org.opentravel.ota._2003._05.OTAGolfCourseAvailRQ.GolfFacility.class,
    org.opentravel.ota._2003._05.OTAGroundAvailRQ.Passengers.CustomerID.class,
    org.opentravel.ota._2003._05.OTAGroundModifyRQ.Reservation.ReferenceID.class,
    org.opentravel.ota._2003._05.OTAHotelAvailRS.RoomStays.RoomStay.Reference.class,
    org.opentravel.ota._2003._05.OTAHotelPostEventRQ.Requestor.class,
    org.opentravel.ota._2003._05.RFPResponseType.MessageID.class,
    org.opentravel.ota._2003._05.OTAHotelRFPMeetingNotifRS.MessageID.RFPIDs.RFPID.class,
    org.opentravel.ota._2003._05.OTAHotelRFPMeetingNotifRS.MessageID.class,
    org.opentravel.ota._2003._05.RFPRequestSegmentsType.RFPRequestSegment.RFPID.class,
    org.opentravel.ota._2003._05.RFPTransientDetailsType.MessageID.class,
    org.opentravel.ota._2003._05.OTAHotelRFPTransientNotifRS.MessageID.class,
    org.opentravel.ota._2003._05.OTAPkgCostRQ.ProvisionalID.class,
    org.opentravel.ota._2003._05.ProfileType.UserID.class,
    org.opentravel.ota._2003._05.OTARailPriceRQ.BookingReferenceID.class,
    org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.HotelReadRequest.UserID.class,
    org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ProfileReadRequest.UniqueID.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRQ.Confirmation.class,
    org.opentravel.ota._2003._05.OTATourActivityModifyRQ.BookingInfo.Confirmation.class,
    org.opentravel.ota._2003._05.VehicleReservationRQAdditionalInfoType.Reference.class,
    org.opentravel.ota._2003._05.SourceType.RequestorID.class,
    org.opentravel.ota._2003._05.VehicleAvailCoreType.Reference.class,
    org.opentravel.ota._2003._05.VehicleReservationSummaryType.ConfID.class,
    org.opentravel.ota._2003._05.VehicleSegmentCoreType.ConfID.class,
    org.opentravel.ota._2003._05.RoomStaysType.RoomStay.Reference.class,
    org.opentravel.ota._2003._05.GolfFacilityType.UniqueID.class,
    org.opentravel.ota._2003._05.PostEventSiteReportType.EventID.class,
    org.opentravel.ota._2003._05.VehicleModifyRQAdditionalInfoType.Reference.class
})
public class UniqueIDType {

    @XmlElement(name = "CompanyName")
    protected CompanyNameType companyName;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "URL")
    @XmlSchemaType(name = "anyURI")
    protected String url;
    @XmlAttribute(name = "Type", required = true)
    protected String type;
    @XmlAttribute(name = "Instance")
    protected String instance;
    @XmlAttribute(name = "ID", required = true)
    protected String id;
    @XmlAttribute(name = "ID_Context")
    protected String idContext;

    /**
     * Obtiene el valor de la propiedad companyName.
     * 
     * @return
     *     possible object is
     *     {@link CompanyNameType }
     *     
     */
    public CompanyNameType getCompanyName() {
        return companyName;
    }

    /**
     * Define el valor de la propiedad companyName.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyNameType }
     *     
     */
    public void setCompanyName(CompanyNameType value) {
        this.companyName = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad url.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURL() {
        return url;
    }

    /**
     * Define el valor de la propiedad url.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURL(String value) {
        this.url = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad instance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstance() {
        return instance;
    }

    /**
     * Define el valor de la propiedad instance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstance(String value) {
        this.instance = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad idContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDContext() {
        return idContext;
    }

    /**
     * Define el valor de la propiedad idContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDContext(String value) {
        this.idContext = value;
    }

}
