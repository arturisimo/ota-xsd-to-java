
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * The ProfileCertification element identifies the travel agency to be paid by providing the IATA number or ARC certifying number. (Implementation note: This attribute is optional, but if it is not set, the Profile attribute must be set.)
 * 
 * &lt;p&gt;Clase Java para ProfileCertificationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ProfileCertificationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="CertificationType" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *       &amp;lt;attribute name="CertificationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileCertificationType")
public class ProfileCertificationType {

    @XmlAttribute(name = "CertificationType")
    protected String certificationType;
    @XmlAttribute(name = "CertificationID")
    protected String certificationID;

    /**
     * Obtiene el valor de la propiedad certificationType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificationType() {
        return certificationType;
    }

    /**
     * Define el valor de la propiedad certificationType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificationType(String value) {
        this.certificationType = value;
    }

    /**
     * Obtiene el valor de la propiedad certificationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificationID() {
        return certificationID;
    }

    /**
     * Define el valor de la propiedad certificationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificationID(String value) {
        this.certificationID = value;
    }

}
