
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="OriginDestinationOptions" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="FlightSegment" maxOccurs="4"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="DaysOfOperation" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="BookingClass" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="MealCode" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="ResBookDesigCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                         &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                 &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
 *                                       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                                       &amp;lt;attribute name="ScheduleValidStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                       &amp;lt;attribute name="ScheduleValidEndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
 *                                       &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                       &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                       &amp;lt;attribute name="FLIFO_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="OriginCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                   &amp;lt;attribute name="DestinationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "originDestinationOptions",
    "errors"
})
@XmlRootElement(name = "OTA_AirScheduleRS")
public class OTAAirScheduleRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "OriginDestinationOptions")
    protected OTAAirScheduleRS.OriginDestinationOptions originDestinationOptions;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad originDestinationOptions.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirScheduleRS.OriginDestinationOptions }
     *     
     */
    public OTAAirScheduleRS.OriginDestinationOptions getOriginDestinationOptions() {
        return originDestinationOptions;
    }

    /**
     * Define el valor de la propiedad originDestinationOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirScheduleRS.OriginDestinationOptions }
     *     
     */
    public void setOriginDestinationOptions(OTAAirScheduleRS.OriginDestinationOptions value) {
        this.originDestinationOptions = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * Holds the collection of flight options for this origin-destination pair. There maybe limits imposed by individual supplier that are below the maximum occurrences for this element.
     * 
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="FlightSegment" maxOccurs="4"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="DaysOfOperation" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="BookingClass" maxOccurs="99" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="MealCode" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="ResBookDesigCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="9" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                     &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
     *                           &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                           &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                           &amp;lt;attribute name="ScheduleValidStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                           &amp;lt;attribute name="ScheduleValidEndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
     *                           &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                           &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                           &amp;lt;attribute name="FLIFO_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="OriginCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *       &amp;lt;attribute name="DestinationCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to8" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestinationOption"
    })
    public static class OriginDestinationOptions {

        @XmlElement(name = "OriginDestinationOption", required = true)
        protected List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption> originDestinationOption;
        @XmlAttribute(name = "OriginCode")
        protected String originCode;
        @XmlAttribute(name = "DestinationCode")
        protected String destinationCode;

        /**
         * Gets the value of the originDestinationOption property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationOption property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getOriginDestinationOption().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption }
         * 
         * 
         */
        public List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption> getOriginDestinationOption() {
            if (originDestinationOption == null) {
                originDestinationOption = new ArrayList<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption>();
            }
            return this.originDestinationOption;
        }

        /**
         * Obtiene el valor de la propiedad originCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginCode() {
            return originCode;
        }

        /**
         * Define el valor de la propiedad originCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginCode(String value) {
            this.originCode = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinationCode() {
            return destinationCode;
        }

        /**
         * Define el valor de la propiedad destinationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinationCode(String value) {
            this.destinationCode = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="FlightSegment" maxOccurs="4"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="DaysOfOperation" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="BookingClass" maxOccurs="99" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="MealCode" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="ResBookDesigCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="9" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                           &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
         *                 &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *                 &amp;lt;attribute name="ScheduleValidStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                 &amp;lt;attribute name="ScheduleValidEndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
         *                 &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                 &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                 &amp;lt;attribute name="FLIFO_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "flightSegment",
            "comment"
        })
        public static class OriginDestinationOption {

            @XmlElement(name = "FlightSegment", required = true)
            protected List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment> flightSegment;
            @XmlElement(name = "Comment")
            protected List<FreeTextType> comment;

            /**
             * Gets the value of the flightSegment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightSegment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getFlightSegment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment }
             * 
             * 
             */
            public List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment> getFlightSegment() {
                if (flightSegment == null) {
                    flightSegment = new ArrayList<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment>();
                }
                return this.flightSegment;
            }

            /**
             * Gets the value of the comment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getComment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link FreeTextType }
             * 
             * 
             */
            public List<FreeTextType> getComment() {
                if (comment == null) {
                    comment = new ArrayList<FreeTextType>();
                }
                return this.comment;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="DaysOfOperation" type="{http://www.opentravel.org/OTA/2003/05}OperationSchedulesType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="BookingClass" maxOccurs="99" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="MealCode" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="ResBookDesigCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="9" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                 &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
             *       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
             *       &amp;lt;attribute name="ScheduleValidStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *       &amp;lt;attribute name="ScheduleValidEndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&amp;gt;
             *       &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *       &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *       &amp;lt;attribute name="FLIFO_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "comment",
                "marketingCabin",
                "daysOfOperation",
                "bookingClass",
                "trafficRestrictionInfo"
            })
            public static class FlightSegment
                extends FlightSegmentType
            {

                @XmlElement(name = "Comment")
                protected List<FreeTextType> comment;
                @XmlElement(name = "MarketingCabin")
                protected List<MarketingCabinType> marketingCabin;
                @XmlElement(name = "DaysOfOperation")
                protected OperationSchedulesType daysOfOperation;
                @XmlElement(name = "BookingClass")
                protected List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass> bookingClass;
                @XmlElement(name = "TrafficRestrictionInfo")
                protected List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo> trafficRestrictionInfo;
                @XmlAttribute(name = "JourneyDuration")
                protected Duration journeyDuration;
                @XmlAttribute(name = "OnTimeRate")
                protected BigDecimal onTimeRate;
                @XmlAttribute(name = "ScheduleValidStartDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar scheduleValidStartDate;
                @XmlAttribute(name = "ScheduleValidEndDate")
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar scheduleValidEndDate;
                @XmlAttribute(name = "ParticipationLevelCode")
                protected String participationLevelCode;
                @XmlAttribute(name = "DateChangeNbr")
                protected String dateChangeNbr;
                @XmlAttribute(name = "FLIFO_Ind")
                protected Boolean flifoInd;
                @XmlAttribute(name = "SmokingAllowed")
                protected Boolean smokingAllowed;

                /**
                 * Gets the value of the comment property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getComment().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link FreeTextType }
                 * 
                 * 
                 */
                public List<FreeTextType> getComment() {
                    if (comment == null) {
                        comment = new ArrayList<FreeTextType>();
                    }
                    return this.comment;
                }

                /**
                 * Gets the value of the marketingCabin property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingCabin property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getMarketingCabin().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link MarketingCabinType }
                 * 
                 * 
                 */
                public List<MarketingCabinType> getMarketingCabin() {
                    if (marketingCabin == null) {
                        marketingCabin = new ArrayList<MarketingCabinType>();
                    }
                    return this.marketingCabin;
                }

                /**
                 * Obtiene el valor de la propiedad daysOfOperation.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OperationSchedulesType }
                 *     
                 */
                public OperationSchedulesType getDaysOfOperation() {
                    return daysOfOperation;
                }

                /**
                 * Define el valor de la propiedad daysOfOperation.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OperationSchedulesType }
                 *     
                 */
                public void setDaysOfOperation(OperationSchedulesType value) {
                    this.daysOfOperation = value;
                }

                /**
                 * Gets the value of the bookingClass property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingClass property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getBookingClass().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass }
                 * 
                 * 
                 */
                public List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass> getBookingClass() {
                    if (bookingClass == null) {
                        bookingClass = new ArrayList<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass>();
                    }
                    return this.bookingClass;
                }

                /**
                 * Gets the value of the trafficRestrictionInfo property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the trafficRestrictionInfo property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getTrafficRestrictionInfo().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo }
                 * 
                 * 
                 */
                public List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo> getTrafficRestrictionInfo() {
                    if (trafficRestrictionInfo == null) {
                        trafficRestrictionInfo = new ArrayList<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo>();
                    }
                    return this.trafficRestrictionInfo;
                }

                /**
                 * Obtiene el valor de la propiedad journeyDuration.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Duration }
                 *     
                 */
                public Duration getJourneyDuration() {
                    return journeyDuration;
                }

                /**
                 * Define el valor de la propiedad journeyDuration.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Duration }
                 *     
                 */
                public void setJourneyDuration(Duration value) {
                    this.journeyDuration = value;
                }

                /**
                 * Obtiene el valor de la propiedad onTimeRate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getOnTimeRate() {
                    return onTimeRate;
                }

                /**
                 * Define el valor de la propiedad onTimeRate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setOnTimeRate(BigDecimal value) {
                    this.onTimeRate = value;
                }

                /**
                 * Obtiene el valor de la propiedad scheduleValidStartDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getScheduleValidStartDate() {
                    return scheduleValidStartDate;
                }

                /**
                 * Define el valor de la propiedad scheduleValidStartDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setScheduleValidStartDate(XMLGregorianCalendar value) {
                    this.scheduleValidStartDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad scheduleValidEndDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getScheduleValidEndDate() {
                    return scheduleValidEndDate;
                }

                /**
                 * Define el valor de la propiedad scheduleValidEndDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setScheduleValidEndDate(XMLGregorianCalendar value) {
                    this.scheduleValidEndDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad participationLevelCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParticipationLevelCode() {
                    return participationLevelCode;
                }

                /**
                 * Define el valor de la propiedad participationLevelCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParticipationLevelCode(String value) {
                    this.participationLevelCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad dateChangeNbr.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDateChangeNbr() {
                    return dateChangeNbr;
                }

                /**
                 * Define el valor de la propiedad dateChangeNbr.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDateChangeNbr(String value) {
                    this.dateChangeNbr = value;
                }

                /**
                 * Obtiene el valor de la propiedad flifoInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isFLIFOInd() {
                    return flifoInd;
                }

                /**
                 * Define el valor de la propiedad flifoInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setFLIFOInd(Boolean value) {
                    this.flifoInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad smokingAllowed.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isSmokingAllowed() {
                    return smokingAllowed;
                }

                /**
                 * Define el valor de la propiedad smokingAllowed.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setSmokingAllowed(Boolean value) {
                    this.smokingAllowed = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Meal" maxOccurs="5" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="MealCode" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="ResBookDesigCode" use="required" type="{http://www.opentravel.org/OTA/2003/05}UpperCaseAlphaLength1to2" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "meal"
                })
                public static class BookingClass {

                    @XmlElement(name = "Meal")
                    protected List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass.Meal> meal;
                    @XmlAttribute(name = "ResBookDesigCode", required = true)
                    protected String resBookDesigCode;

                    /**
                     * Gets the value of the meal property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the meal property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getMeal().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass.Meal }
                     * 
                     * 
                     */
                    public List<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass.Meal> getMeal() {
                        if (meal == null) {
                            meal = new ArrayList<OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClass.Meal>();
                        }
                        return this.meal;
                    }

                    /**
                     * Obtiene el valor de la propiedad resBookDesigCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getResBookDesigCode() {
                        return resBookDesigCode;
                    }

                    /**
                     * Define el valor de la propiedad resBookDesigCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setResBookDesigCode(String value) {
                        this.resBookDesigCode = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="MealCode" type="{http://www.opentravel.org/OTA/2003/05}MealServiceType" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Meal {

                        @XmlAttribute(name = "MealCode")
                        protected String mealCode;

                        /**
                         * Obtiene el valor de la propiedad mealCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getMealCode() {
                            return mealCode;
                        }

                        /**
                         * Define el valor de la propiedad mealCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setMealCode(String value) {
                            this.mealCode = value;
                        }

                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *       &amp;lt;attribute name="Text" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class TrafficRestrictionInfo {

                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "Text")
                    protected String text;

                    /**
                     * Obtiene el valor de la propiedad code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Define el valor de la propiedad code.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad text.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getText() {
                        return text;
                    }

                    /**
                     * Define el valor de la propiedad text.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setText(String value) {
                        this.text = value;
                    }

                }

            }

        }

    }

}
