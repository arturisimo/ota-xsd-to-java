
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para CustomQuestionType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CustomQuestionType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="QuestionText" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ProvidedAnswerChoices" maxOccurs="10" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
 *                 &amp;lt;attribute name="AnswerID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="AnswerText" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextTextType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="QuestionID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="QuestionTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="QuestionCategoryCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *       &amp;lt;attribute name="AnswerRequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomQuestionType", propOrder = {
    "questionText",
    "providedAnswerChoices",
    "answerText"
})
public class CustomQuestionType {

    @XmlElement(name = "QuestionText")
    protected FormattedTextTextType questionText;
    @XmlElement(name = "ProvidedAnswerChoices")
    protected List<CustomQuestionType.ProvidedAnswerChoices> providedAnswerChoices;
    @XmlElement(name = "AnswerText")
    protected FormattedTextTextType answerText;
    @XmlAttribute(name = "QuestionID")
    protected String questionID;
    @XmlAttribute(name = "QuestionTypeCode")
    protected String questionTypeCode;
    @XmlAttribute(name = "QuestionCategoryCode")
    protected String questionCategoryCode;
    @XmlAttribute(name = "AnswerRequiredInd")
    protected Boolean answerRequiredInd;

    /**
     * Obtiene el valor de la propiedad questionText.
     * 
     * @return
     *     possible object is
     *     {@link FormattedTextTextType }
     *     
     */
    public FormattedTextTextType getQuestionText() {
        return questionText;
    }

    /**
     * Define el valor de la propiedad questionText.
     * 
     * @param value
     *     allowed object is
     *     {@link FormattedTextTextType }
     *     
     */
    public void setQuestionText(FormattedTextTextType value) {
        this.questionText = value;
    }

    /**
     * Gets the value of the providedAnswerChoices property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the providedAnswerChoices property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getProvidedAnswerChoices().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CustomQuestionType.ProvidedAnswerChoices }
     * 
     * 
     */
    public List<CustomQuestionType.ProvidedAnswerChoices> getProvidedAnswerChoices() {
        if (providedAnswerChoices == null) {
            providedAnswerChoices = new ArrayList<CustomQuestionType.ProvidedAnswerChoices>();
        }
        return this.providedAnswerChoices;
    }

    /**
     * Obtiene el valor de la propiedad answerText.
     * 
     * @return
     *     possible object is
     *     {@link FormattedTextTextType }
     *     
     */
    public FormattedTextTextType getAnswerText() {
        return answerText;
    }

    /**
     * Define el valor de la propiedad answerText.
     * 
     * @param value
     *     allowed object is
     *     {@link FormattedTextTextType }
     *     
     */
    public void setAnswerText(FormattedTextTextType value) {
        this.answerText = value;
    }

    /**
     * Obtiene el valor de la propiedad questionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionID() {
        return questionID;
    }

    /**
     * Define el valor de la propiedad questionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionID(String value) {
        this.questionID = value;
    }

    /**
     * Obtiene el valor de la propiedad questionTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionTypeCode() {
        return questionTypeCode;
    }

    /**
     * Define el valor de la propiedad questionTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionTypeCode(String value) {
        this.questionTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad questionCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionCategoryCode() {
        return questionCategoryCode;
    }

    /**
     * Define el valor de la propiedad questionCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionCategoryCode(String value) {
        this.questionCategoryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad answerRequiredInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAnswerRequiredInd() {
        return answerRequiredInd;
    }

    /**
     * Define el valor de la propiedad answerRequiredInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAnswerRequiredInd(Boolean value) {
        this.answerRequiredInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FormattedTextTextType"&amp;gt;
     *       &amp;lt;attribute name="AnswerID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProvidedAnswerChoices
        extends FormattedTextTextType
    {

        @XmlAttribute(name = "AnswerID")
        protected String answerID;

        /**
         * Obtiene el valor de la propiedad answerID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAnswerID() {
            return answerID;
        }

        /**
         * Define el valor de la propiedad answerID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAnswerID(String value) {
            this.answerID = value;
        }

    }

}
