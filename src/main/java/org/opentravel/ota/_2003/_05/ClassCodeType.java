
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines a specific code representing the availability of one specific class of service. Such class of service may represent First class, reserved seats, sleeper accommodations, etc.
 * 
 * &lt;p&gt;Clase Java para ClassCodeType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ClassCodeType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}RailCodeGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassCodeType")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.TrainSegment.AvailabilityDetail.ClassCode.class,
    org.opentravel.ota._2003._05.OTARailAvailRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.ClassCodes.class,
    org.opentravel.ota._2003._05.OTARailScheduleRQ.RailScheduleQuery.RailPrefs.ClassCodes.class,
    org.opentravel.ota._2003._05.OTARailShopRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.AvailabilityDetail.ClassCode.class,
    org.opentravel.ota._2003._05.OTARailShopRS.OriginDestinationInformation.OriginDestinationOption.JourneySegment.BusSegment.ClassCodes.class
})
public class ClassCodeType {

    @XmlAttribute(name = "Code", required = true)
    protected String code;
    @XmlAttribute(name = "CodeContext")
    protected String codeContext;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantity;

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad codeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeContext() {
        return codeContext;
    }

    /**
     * Define el valor de la propiedad codeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeContext(String value) {
        this.codeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }

}
