
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPaymentStatus.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPaymentStatus"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Guaranteed"/&amp;gt;
 *     &amp;lt;enumeration value="Prepaid"/&amp;gt;
 *     &amp;lt;enumeration value="Unpaid"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPaymentStatus")
@XmlEnum
public enum ListOfferPaymentStatus {


    /**
     * Reservation has been guaranteed.
     * 
     */
    @XmlEnumValue("Guaranteed")
    GUARANTEED("Guaranteed"),
    @XmlEnumValue("Prepaid")
    PREPAID("Prepaid"),
    @XmlEnumValue("Unpaid")
    UNPAID("Unpaid"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferPaymentStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPaymentStatus fromValue(String v) {
        for (ListOfferPaymentStatus c: ListOfferPaymentStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
