
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para TourActivityCategoryEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="TourActivityCategoryEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Accessible"/&amp;gt;
 *     &amp;lt;enumeration value="Adventure"/&amp;gt;
 *     &amp;lt;enumeration value="Cultural"/&amp;gt;
 *     &amp;lt;enumeration value="EcoAdventure"/&amp;gt;
 *     &amp;lt;enumeration value="Educational"/&amp;gt;
 *     &amp;lt;enumeration value="EscortedTour"/&amp;gt;
 *     &amp;lt;enumeration value="Family"/&amp;gt;
 *     &amp;lt;enumeration value="FoodAndBeverage"/&amp;gt;
 *     &amp;lt;enumeration value="GuidedTour"/&amp;gt;
 *     &amp;lt;enumeration value="Group"/&amp;gt;
 *     &amp;lt;enumeration value="Independent"/&amp;gt;
 *     &amp;lt;enumeration value="Romantic"/&amp;gt;
 *     &amp;lt;enumeration value="SportsAndRecreation"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "TourActivityCategoryEnum")
@XmlEnum
public enum TourActivityCategoryEnum {

    @XmlEnumValue("Accessible")
    ACCESSIBLE("Accessible"),
    @XmlEnumValue("Adventure")
    ADVENTURE("Adventure"),
    @XmlEnumValue("Cultural")
    CULTURAL("Cultural"),
    @XmlEnumValue("EcoAdventure")
    ECO_ADVENTURE("EcoAdventure"),
    @XmlEnumValue("Educational")
    EDUCATIONAL("Educational"),

    /**
     * Escorted tours are typically conducted by a tour director who takes care of all services from beginning to end of the tour, which may include transportation to and from the tour location.
     * 
     */
    @XmlEnumValue("EscortedTour")
    ESCORTED_TOUR("EscortedTour"),

    /**
     * The tour and/or activity content and extertion level are family friendly for children, adults and seniors.
     * 
     */
    @XmlEnumValue("Family")
    FAMILY("Family"),
    @XmlEnumValue("FoodAndBeverage")
    FOOD_AND_BEVERAGE("FoodAndBeverage"),

    /**
     * Guided tours are somewhat similar to escorted tours but customers are greeted by a local representative rather than a designated tour director and will not be escorted to all activities and functions. Local guides will be available to answer questions through the duration of the tour. Some meals, accommodations and sightseeing may be included in the tour. 
     * 
     */
    @XmlEnumValue("GuidedTour")
    GUIDED_TOUR("GuidedTour"),

    /**
     * A day tour or activity that is well-suited for larger parties, e.g. 8+ attendees within a group.
     * 
     */
    @XmlEnumValue("Group")
    GROUP("Group"),

    /**
     * Guides and escorts are not available to participants.
     * 
     */
    @XmlEnumValue("Independent")
    INDEPENDENT("Independent"),
    @XmlEnumValue("Romantic")
    ROMANTIC("Romantic"),
    @XmlEnumValue("SportsAndRecreation")
    SPORTS_AND_RECREATION("SportsAndRecreation"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support to support additional compartment type. The Value corresponding to "Other_" will be specified in the  "Value" attribute. See CompartmentType.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    TourActivityCategoryEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TourActivityCategoryEnum fromValue(String v) {
        for (TourActivityCategoryEnum c: TourActivityCategoryEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
