
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * Used to define the details of an inventory block.
 * 
 * &lt;p&gt;Clase Java para InvBlockType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="InvBlockType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HotelRef" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="InvBlockDates" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockDatesGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RoomTypes" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}InvBlockRoomType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MethodInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="BillingInstruction" type="{http://www.opentravel.org/OTA/2003/05}BillingInstructionType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MethodInfoGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BlockDescriptions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="BlockDescription" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Contacts" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DestinationSystemCodes" type="{http://www.opentravel.org/OTA/2003/05}DestinationSystemCodesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TaxInformation" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="TaxType"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;enumeration value="Taxable"/&amp;gt;
 *                       &amp;lt;enumeration value="RoomExempt"/&amp;gt;
 *                       &amp;lt;enumeration value="FullExempt"/&amp;gt;
 *                       &amp;lt;enumeration value=""/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;attribute name="TaxReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                 &amp;lt;attribute name="TaxID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvBlockType", propOrder = {
    "hotelRef",
    "invBlockDates",
    "roomTypes",
    "methodInfo",
    "blockDescriptions",
    "contacts",
    "destinationSystemCodes",
    "taxInformation"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAHotelInvBlockNotifRQ.InvBlocks.InvBlock.class
})
public class InvBlockType {

    @XmlElement(name = "HotelRef")
    protected InvBlockType.HotelRef hotelRef;
    @XmlElement(name = "InvBlockDates")
    protected InvBlockType.InvBlockDates invBlockDates;
    @XmlElement(name = "RoomTypes")
    protected InvBlockType.RoomTypes roomTypes;
    @XmlElement(name = "MethodInfo")
    protected InvBlockType.MethodInfo methodInfo;
    @XmlElement(name = "BlockDescriptions")
    protected InvBlockType.BlockDescriptions blockDescriptions;
    @XmlElement(name = "Contacts")
    protected InvBlockType.Contacts contacts;
    @XmlElement(name = "DestinationSystemCodes")
    protected DestinationSystemCodesType destinationSystemCodes;
    @XmlElement(name = "TaxInformation")
    protected InvBlockType.TaxInformation taxInformation;
    @XmlAttribute(name = "BookingStatus")
    protected String bookingStatus;
    @XmlAttribute(name = "InvBlockTypeCode")
    protected String invBlockTypeCode;
    @XmlAttribute(name = "InvBlockCode")
    protected String invBlockCode;
    @XmlAttribute(name = "InvBlockGroupingCode")
    protected String invBlockGroupingCode;
    @XmlAttribute(name = "InvBlockName")
    protected String invBlockName;
    @XmlAttribute(name = "InvBlockLongName")
    protected String invBlockLongName;
    @XmlAttribute(name = "InvBlockStatusCode")
    protected String invBlockStatusCode;
    @XmlAttribute(name = "PMS_InvBlockID")
    protected String pmsInvBlockID;
    @XmlAttribute(name = "OpportunityID")
    protected String opportunityID;
    @XmlAttribute(name = "InvBlockCompanyID")
    protected String invBlockCompanyID;
    @XmlAttribute(name = "RestrictedBookingCodeList")
    protected List<String> restrictedBookingCodeList;
    @XmlAttribute(name = "RestrictedViewingCodeList")
    protected List<String> restrictedViewingCodeList;
    @XmlAttribute(name = "TransactionAction")
    protected TransactionActionType transactionAction;
    @XmlAttribute(name = "TransactionDetail")
    protected String transactionDetail;
    @XmlAttribute(name = "QuoteID")
    protected String quoteID;

    /**
     * Obtiene el valor de la propiedad hotelRef.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.HotelRef }
     *     
     */
    public InvBlockType.HotelRef getHotelRef() {
        return hotelRef;
    }

    /**
     * Define el valor de la propiedad hotelRef.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.HotelRef }
     *     
     */
    public void setHotelRef(InvBlockType.HotelRef value) {
        this.hotelRef = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockDates.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.InvBlockDates }
     *     
     */
    public InvBlockType.InvBlockDates getInvBlockDates() {
        return invBlockDates;
    }

    /**
     * Define el valor de la propiedad invBlockDates.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.InvBlockDates }
     *     
     */
    public void setInvBlockDates(InvBlockType.InvBlockDates value) {
        this.invBlockDates = value;
    }

    /**
     * Obtiene el valor de la propiedad roomTypes.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.RoomTypes }
     *     
     */
    public InvBlockType.RoomTypes getRoomTypes() {
        return roomTypes;
    }

    /**
     * Define el valor de la propiedad roomTypes.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.RoomTypes }
     *     
     */
    public void setRoomTypes(InvBlockType.RoomTypes value) {
        this.roomTypes = value;
    }

    /**
     * Obtiene el valor de la propiedad methodInfo.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.MethodInfo }
     *     
     */
    public InvBlockType.MethodInfo getMethodInfo() {
        return methodInfo;
    }

    /**
     * Define el valor de la propiedad methodInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.MethodInfo }
     *     
     */
    public void setMethodInfo(InvBlockType.MethodInfo value) {
        this.methodInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad blockDescriptions.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.BlockDescriptions }
     *     
     */
    public InvBlockType.BlockDescriptions getBlockDescriptions() {
        return blockDescriptions;
    }

    /**
     * Define el valor de la propiedad blockDescriptions.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.BlockDescriptions }
     *     
     */
    public void setBlockDescriptions(InvBlockType.BlockDescriptions value) {
        this.blockDescriptions = value;
    }

    /**
     * Obtiene el valor de la propiedad contacts.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.Contacts }
     *     
     */
    public InvBlockType.Contacts getContacts() {
        return contacts;
    }

    /**
     * Define el valor de la propiedad contacts.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.Contacts }
     *     
     */
    public void setContacts(InvBlockType.Contacts value) {
        this.contacts = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationSystemCodes.
     * 
     * @return
     *     possible object is
     *     {@link DestinationSystemCodesType }
     *     
     */
    public DestinationSystemCodesType getDestinationSystemCodes() {
        return destinationSystemCodes;
    }

    /**
     * Define el valor de la propiedad destinationSystemCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationSystemCodesType }
     *     
     */
    public void setDestinationSystemCodes(DestinationSystemCodesType value) {
        this.destinationSystemCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad taxInformation.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockType.TaxInformation }
     *     
     */
    public InvBlockType.TaxInformation getTaxInformation() {
        return taxInformation;
    }

    /**
     * Define el valor de la propiedad taxInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockType.TaxInformation }
     *     
     */
    public void setTaxInformation(InvBlockType.TaxInformation value) {
        this.taxInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * Define el valor de la propiedad bookingStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStatus(String value) {
        this.bookingStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockTypeCode() {
        return invBlockTypeCode;
    }

    /**
     * Define el valor de la propiedad invBlockTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockTypeCode(String value) {
        this.invBlockTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockCode() {
        return invBlockCode;
    }

    /**
     * Define el valor de la propiedad invBlockCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockCode(String value) {
        this.invBlockCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockGroupingCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockGroupingCode() {
        return invBlockGroupingCode;
    }

    /**
     * Define el valor de la propiedad invBlockGroupingCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockGroupingCode(String value) {
        this.invBlockGroupingCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockName() {
        return invBlockName;
    }

    /**
     * Define el valor de la propiedad invBlockName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockName(String value) {
        this.invBlockName = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockLongName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockLongName() {
        return invBlockLongName;
    }

    /**
     * Define el valor de la propiedad invBlockLongName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockLongName(String value) {
        this.invBlockLongName = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockStatusCode() {
        return invBlockStatusCode;
    }

    /**
     * Define el valor de la propiedad invBlockStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockStatusCode(String value) {
        this.invBlockStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad pmsInvBlockID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMSInvBlockID() {
        return pmsInvBlockID;
    }

    /**
     * Define el valor de la propiedad pmsInvBlockID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMSInvBlockID(String value) {
        this.pmsInvBlockID = value;
    }

    /**
     * Obtiene el valor de la propiedad opportunityID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityID() {
        return opportunityID;
    }

    /**
     * Define el valor de la propiedad opportunityID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityID(String value) {
        this.opportunityID = value;
    }

    /**
     * Obtiene el valor de la propiedad invBlockCompanyID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvBlockCompanyID() {
        return invBlockCompanyID;
    }

    /**
     * Define el valor de la propiedad invBlockCompanyID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvBlockCompanyID(String value) {
        this.invBlockCompanyID = value;
    }

    /**
     * Gets the value of the restrictedBookingCodeList property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restrictedBookingCodeList property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRestrictedBookingCodeList().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRestrictedBookingCodeList() {
        if (restrictedBookingCodeList == null) {
            restrictedBookingCodeList = new ArrayList<String>();
        }
        return this.restrictedBookingCodeList;
    }

    /**
     * Gets the value of the restrictedViewingCodeList property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restrictedViewingCodeList property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRestrictedViewingCodeList().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRestrictedViewingCodeList() {
        if (restrictedViewingCodeList == null) {
            restrictedViewingCodeList = new ArrayList<String>();
        }
        return this.restrictedViewingCodeList;
    }

    /**
     * Obtiene el valor de la propiedad transactionAction.
     * 
     * @return
     *     possible object is
     *     {@link TransactionActionType }
     *     
     */
    public TransactionActionType getTransactionAction() {
        return transactionAction;
    }

    /**
     * Define el valor de la propiedad transactionAction.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionActionType }
     *     
     */
    public void setTransactionAction(TransactionActionType value) {
        this.transactionAction = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionDetail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDetail() {
        return transactionDetail;
    }

    /**
     * Define el valor de la propiedad transactionDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDetail(String value) {
        this.transactionDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad quoteID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteID() {
        return quoteID;
    }

    /**
     * Define el valor de la propiedad quoteID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteID(String value) {
        this.quoteID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BlockDescription" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "blockDescription"
    })
    public static class BlockDescriptions {

        @XmlElement(name = "BlockDescription", required = true)
        protected List<InvBlockType.BlockDescriptions.BlockDescription> blockDescription;

        /**
         * Gets the value of the blockDescription property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the blockDescription property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getBlockDescription().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link InvBlockType.BlockDescriptions.BlockDescription }
         * 
         * 
         */
        public List<InvBlockType.BlockDescriptions.BlockDescription> getBlockDescription() {
            if (blockDescription == null) {
                blockDescription = new ArrayList<InvBlockType.BlockDescriptions.BlockDescription>();
            }
            return this.blockDescription;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ParagraphType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class BlockDescription
            extends ParagraphType
        {

            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Contact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contact"
    })
    public static class Contacts {

        @XmlElement(name = "Contact", required = true)
        protected List<ContactPersonType> contact;

        /**
         * Gets the value of the contact property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the contact property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getContact().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link ContactPersonType }
         * 
         * 
         */
        public List<ContactPersonType> getContact() {
            if (contact == null) {
                contact = new ArrayList<ContactPersonType>();
            }
            return this.contact;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class HotelRef {

        @XmlAttribute(name = "ChainCode")
        protected String chainCode;
        @XmlAttribute(name = "BrandCode")
        protected String brandCode;
        @XmlAttribute(name = "HotelCode")
        protected String hotelCode;
        @XmlAttribute(name = "HotelCityCode")
        protected String hotelCityCode;
        @XmlAttribute(name = "HotelName")
        protected String hotelName;
        @XmlAttribute(name = "HotelCodeContext")
        protected String hotelCodeContext;
        @XmlAttribute(name = "ChainName")
        protected String chainName;
        @XmlAttribute(name = "BrandName")
        protected String brandName;
        @XmlAttribute(name = "AreaID")
        protected String areaID;
        @XmlAttribute(name = "TTIcode")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger ttIcode;

        /**
         * Obtiene el valor de la propiedad chainCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChainCode() {
            return chainCode;
        }

        /**
         * Define el valor de la propiedad chainCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChainCode(String value) {
            this.chainCode = value;
        }

        /**
         * Obtiene el valor de la propiedad brandCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandCode() {
            return brandCode;
        }

        /**
         * Define el valor de la propiedad brandCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandCode(String value) {
            this.brandCode = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCode() {
            return hotelCode;
        }

        /**
         * Define el valor de la propiedad hotelCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCode(String value) {
            this.hotelCode = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelCityCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCityCode() {
            return hotelCityCode;
        }

        /**
         * Define el valor de la propiedad hotelCityCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCityCode(String value) {
            this.hotelCityCode = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelName() {
            return hotelName;
        }

        /**
         * Define el valor de la propiedad hotelName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelName(String value) {
            this.hotelName = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelCodeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHotelCodeContext() {
            return hotelCodeContext;
        }

        /**
         * Define el valor de la propiedad hotelCodeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHotelCodeContext(String value) {
            this.hotelCodeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad chainName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChainName() {
            return chainName;
        }

        /**
         * Define el valor de la propiedad chainName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChainName(String value) {
            this.chainName = value;
        }

        /**
         * Obtiene el valor de la propiedad brandName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBrandName() {
            return brandName;
        }

        /**
         * Define el valor de la propiedad brandName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBrandName(String value) {
            this.brandName = value;
        }

        /**
         * Obtiene el valor de la propiedad areaID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAreaID() {
            return areaID;
        }

        /**
         * Define el valor de la propiedad areaID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAreaID(String value) {
            this.areaID = value;
        }

        /**
         * Obtiene el valor de la propiedad ttIcode.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTTIcode() {
            return ttIcode;
        }

        /**
         * Define el valor de la propiedad ttIcode.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTTIcode(BigInteger value) {
            this.ttIcode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}InvBlockDatesGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class InvBlockDates {

        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;
        @XmlAttribute(name = "EndDateExtensionIndicator")
        protected Boolean endDateExtensionIndicator;
        @XmlAttribute(name = "AbsoluteCutoff")
        protected String absoluteCutoff;
        @XmlAttribute(name = "OffsetDuration")
        protected Duration offsetDuration;
        @XmlAttribute(name = "OffsetCalculationMode")
        protected String offsetCalculationMode;

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

        /**
         * Obtiene el valor de la propiedad endDateExtensionIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isEndDateExtensionIndicator() {
            return endDateExtensionIndicator;
        }

        /**
         * Define el valor de la propiedad endDateExtensionIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setEndDateExtensionIndicator(Boolean value) {
            this.endDateExtensionIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad absoluteCutoff.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAbsoluteCutoff() {
            return absoluteCutoff;
        }

        /**
         * Define el valor de la propiedad absoluteCutoff.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAbsoluteCutoff(String value) {
            this.absoluteCutoff = value;
        }

        /**
         * Obtiene el valor de la propiedad offsetDuration.
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getOffsetDuration() {
            return offsetDuration;
        }

        /**
         * Define el valor de la propiedad offsetDuration.
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setOffsetDuration(Duration value) {
            this.offsetDuration = value;
        }

        /**
         * Obtiene el valor de la propiedad offsetCalculationMode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOffsetCalculationMode() {
            return offsetCalculationMode;
        }

        /**
         * Define el valor de la propiedad offsetCalculationMode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOffsetCalculationMode(String value) {
            this.offsetCalculationMode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="BillingInstruction" type="{http://www.opentravel.org/OTA/2003/05}BillingInstructionType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MethodInfoGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "billingInstruction"
    })
    public static class MethodInfo {

        @XmlElement(name = "BillingInstruction")
        protected BillingInstructionType billingInstruction;
        @XmlAttribute(name = "BillingType")
        protected String billingType;
        @XmlAttribute(name = "SignFoodAndBev")
        protected Boolean signFoodAndBev;
        @XmlAttribute(name = "ReservationMethodCode")
        protected String reservationMethodCode;

        /**
         * Obtiene el valor de la propiedad billingInstruction.
         * 
         * @return
         *     possible object is
         *     {@link BillingInstructionType }
         *     
         */
        public BillingInstructionType getBillingInstruction() {
            return billingInstruction;
        }

        /**
         * Define el valor de la propiedad billingInstruction.
         * 
         * @param value
         *     allowed object is
         *     {@link BillingInstructionType }
         *     
         */
        public void setBillingInstruction(BillingInstructionType value) {
            this.billingInstruction = value;
        }

        /**
         * Obtiene el valor de la propiedad billingType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillingType() {
            return billingType;
        }

        /**
         * Define el valor de la propiedad billingType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillingType(String value) {
            this.billingType = value;
        }

        /**
         * Obtiene el valor de la propiedad signFoodAndBev.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSignFoodAndBev() {
            return signFoodAndBev;
        }

        /**
         * Define el valor de la propiedad signFoodAndBev.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSignFoodAndBev(Boolean value) {
            this.signFoodAndBev = value;
        }

        /**
         * Obtiene el valor de la propiedad reservationMethodCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationMethodCode() {
            return reservationMethodCode;
        }

        /**
         * Define el valor de la propiedad reservationMethodCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationMethodCode(String value) {
            this.reservationMethodCode = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomType" type="{http://www.opentravel.org/OTA/2003/05}InvBlockRoomType" maxOccurs="99"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomType"
    })
    public static class RoomTypes {

        @XmlElement(name = "RoomType", required = true)
        protected List<InvBlockRoomType> roomType;

        /**
         * Gets the value of the roomType property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomType property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomType().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link InvBlockRoomType }
         * 
         * 
         */
        public List<InvBlockRoomType> getRoomType() {
            if (roomType == null) {
                roomType = new ArrayList<InvBlockRoomType>();
            }
            return this.roomType;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="TaxType"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;enumeration value="Taxable"/&amp;gt;
     *             &amp;lt;enumeration value="RoomExempt"/&amp;gt;
     *             &amp;lt;enumeration value="FullExempt"/&amp;gt;
     *             &amp;lt;enumeration value=""/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *       &amp;lt;attribute name="TaxReason" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *       &amp;lt;attribute name="TaxID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TaxInformation {

        @XmlAttribute(name = "TaxType")
        protected String taxType;
        @XmlAttribute(name = "TaxReason")
        protected String taxReason;
        @XmlAttribute(name = "TaxID")
        protected String taxID;

        /**
         * Obtiene el valor de la propiedad taxType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxType() {
            return taxType;
        }

        /**
         * Define el valor de la propiedad taxType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxType(String value) {
            this.taxType = value;
        }

        /**
         * Obtiene el valor de la propiedad taxReason.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxReason() {
            return taxReason;
        }

        /**
         * Define el valor de la propiedad taxReason.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxReason(String value) {
            this.taxReason = value;
        }

        /**
         * Obtiene el valor de la propiedad taxID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxID() {
            return taxID;
        }

        /**
         * Define el valor de la propiedad taxID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxID(String value) {
            this.taxID = value;
        }

    }

}
