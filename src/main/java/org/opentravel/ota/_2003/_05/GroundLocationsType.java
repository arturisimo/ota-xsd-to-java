
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Pickup, interim stops and drop-off details.
 * 
 * &lt;p&gt;Clase Java para GroundLocationsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundLocationsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Pickup" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"/&amp;gt;
 *         &amp;lt;element name="Stops" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
 *                           &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Dropoff" type="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundLocationsType", propOrder = {
    "pickup",
    "stops",
    "dropoff"
})
public class GroundLocationsType {

    @XmlElement(name = "Pickup", required = true)
    protected GroundLocationType pickup;
    @XmlElement(name = "Stops")
    protected GroundLocationsType.Stops stops;
    @XmlElement(name = "Dropoff", required = true)
    protected GroundLocationType dropoff;

    /**
     * Obtiene el valor de la propiedad pickup.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationType }
     *     
     */
    public GroundLocationType getPickup() {
        return pickup;
    }

    /**
     * Define el valor de la propiedad pickup.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationType }
     *     
     */
    public void setPickup(GroundLocationType value) {
        this.pickup = value;
    }

    /**
     * Obtiene el valor de la propiedad stops.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationsType.Stops }
     *     
     */
    public GroundLocationsType.Stops getStops() {
        return stops;
    }

    /**
     * Define el valor de la propiedad stops.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationsType.Stops }
     *     
     */
    public void setStops(GroundLocationsType.Stops value) {
        this.stops = value;
    }

    /**
     * Obtiene el valor de la propiedad dropoff.
     * 
     * @return
     *     possible object is
     *     {@link GroundLocationType }
     *     
     */
    public GroundLocationType getDropoff() {
        return dropoff;
    }

    /**
     * Define el valor de la propiedad dropoff.
     * 
     * @param value
     *     allowed object is
     *     {@link GroundLocationType }
     *     
     */
    public void setDropoff(GroundLocationType value) {
        this.dropoff = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Stop" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
     *                 &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stop"
    })
    public static class Stops {

        @XmlElement(name = "Stop", required = true)
        protected List<GroundLocationsType.Stops.Stop> stop;

        /**
         * Gets the value of the stop property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stop property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getStop().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link GroundLocationsType.Stops.Stop }
         * 
         * 
         */
        public List<GroundLocationsType.Stops.Stop> getStop() {
            if (stop == null) {
                stop = new ArrayList<GroundLocationsType.Stops.Stop>();
            }
            return this.stop;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundLocationType"&amp;gt;
         *       &amp;lt;attribute name="Sequence" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Stop
            extends GroundLocationType
        {

            @XmlAttribute(name = "Sequence")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger sequence;

            /**
             * Obtiene el valor de la propiedad sequence.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSequence() {
                return sequence;
            }

            /**
             * Define el valor de la propiedad sequence.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSequence(BigInteger value) {
                this.sequence = value;
            }

        }

    }

}
