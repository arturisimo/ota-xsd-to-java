
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_UnitOfMeasure_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_UnitOfMeasure_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Acre(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Ampere(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Arpent(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Bi-Week(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Bottle"/&amp;gt;
 *     &amp;lt;enumeration value="Celsius"/&amp;gt;
 *     &amp;lt;enumeration value="Centare(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Centigram(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Centiliter(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Centimeter(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Column(s)"/&amp;gt;
 *     &amp;lt;enumeration value="CubicFeet"/&amp;gt;
 *     &amp;lt;enumeration value="CubicFoot"/&amp;gt;
 *     &amp;lt;enumeration value="CubicInch(es)"/&amp;gt;
 *     &amp;lt;enumeration value="CubicKilometer(s)"/&amp;gt;
 *     &amp;lt;enumeration value="CubicMeter(s)"/&amp;gt;
 *     &amp;lt;enumeration value="CubicYard(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Day(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Degree(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Dozen"/&amp;gt;
 *     &amp;lt;enumeration value="dpi-DotsPerInch"/&amp;gt;
 *     &amp;lt;enumeration value="Each"/&amp;gt;
 *     &amp;lt;enumeration value="Fahrenheit"/&amp;gt;
 *     &amp;lt;enumeration value="Feet"/&amp;gt;
 *     &amp;lt;enumeration value="FluidOunce(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Foot"/&amp;gt;
 *     &amp;lt;enumeration value="FullDuration"/&amp;gt;
 *     &amp;lt;enumeration value="Gallon(s)"/&amp;gt;
 *     &amp;lt;enumeration value="GB-Gigabyte"/&amp;gt;
 *     &amp;lt;enumeration value="Gbps-GigabitsPerSecond"/&amp;gt;
 *     &amp;lt;enumeration value="GBps-GigabytesPerSecond"/&amp;gt;
 *     &amp;lt;enumeration value="GHz-Gigahertz"/&amp;gt;
 *     &amp;lt;enumeration value="Glass"/&amp;gt;
 *     &amp;lt;enumeration value="Gram(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Hectare(s)"/&amp;gt;
 *     &amp;lt;enumeration value="HorsePower"/&amp;gt;
 *     &amp;lt;enumeration value="Hour(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Inch(es)"/&amp;gt;
 *     &amp;lt;enumeration value="KB-Kilobyte"/&amp;gt;
 *     &amp;lt;enumeration value="Kbps-KilobitsPerSecond"/&amp;gt;
 *     &amp;lt;enumeration value="kHz-Kilohertz"/&amp;gt;
 *     &amp;lt;enumeration value="Kilogram(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Kiloliter(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Kilometer(s)"/&amp;gt;
 *     &amp;lt;enumeration value="KilometersPerHour"/&amp;gt;
 *     &amp;lt;enumeration value="Kilowatt(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Knot(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Litre(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Magnum"/&amp;gt;
 *     &amp;lt;enumeration value="MB-Megabyte"/&amp;gt;
 *     &amp;lt;enumeration value="Mbps-MegabitsPerSecond"/&amp;gt;
 *     &amp;lt;enumeration value="MBps-MegabytesPerSecond"/&amp;gt;
 *     &amp;lt;enumeration value="Metre(s)"/&amp;gt;
 *     &amp;lt;enumeration value="MHz-Megahertz"/&amp;gt;
 *     &amp;lt;enumeration value="Microgram(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Mile(s)"/&amp;gt;
 *     &amp;lt;enumeration value="MilesPerGallon"/&amp;gt;
 *     &amp;lt;enumeration value="MilesPerHour"/&amp;gt;
 *     &amp;lt;enumeration value="Milligram(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Milliliter(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Millimeter(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Million(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Minute(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Month(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Morgan(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Nanogram(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Nanometer(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Ohm(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Ounce(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Pica(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Piece(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Pint(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Pixel(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Pixels/CM"/&amp;gt;
 *     &amp;lt;enumeration value="Pixels/Inch"/&amp;gt;
 *     &amp;lt;enumeration value="Point(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Pound(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Quart(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Resolution"/&amp;gt;
 *     &amp;lt;enumeration value="Rod(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Second(s)"/&amp;gt;
 *     &amp;lt;enumeration value="SquareAcre(s)"/&amp;gt;
 *     &amp;lt;enumeration value="SquareFeet"/&amp;gt;
 *     &amp;lt;enumeration value="SquareFoot"/&amp;gt;
 *     &amp;lt;enumeration value="SquareInch(es)"/&amp;gt;
 *     &amp;lt;enumeration value="SquareKilometer(s)"/&amp;gt;
 *     &amp;lt;enumeration value="SquareMetre(s)"/&amp;gt;
 *     &amp;lt;enumeration value="SquareMile(s)"/&amp;gt;
 *     &amp;lt;enumeration value="SquareYard(s)"/&amp;gt;
 *     &amp;lt;enumeration value="TB-Terabyte"/&amp;gt;
 *     &amp;lt;enumeration value="Volt(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Watt(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Week(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Yard(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Year(s)"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_UnitOfMeasure_Base")
@XmlEnum
public enum ListUnitOfMeasureBase {

    @XmlEnumValue("Acre(s)")
    ACRE_S("Acre(s)"),
    @XmlEnumValue("Ampere(s)")
    AMPERE_S("Ampere(s)"),
    @XmlEnumValue("Arpent(s)")
    ARPENT_S("Arpent(s)"),
    @XmlEnumValue("Bi-Week(s)")
    BI_WEEK_S("Bi-Week(s)"),
    @XmlEnumValue("Bottle")
    BOTTLE("Bottle"),
    @XmlEnumValue("Celsius")
    CELSIUS("Celsius"),
    @XmlEnumValue("Centare(s)")
    CENTARE_S("Centare(s)"),
    @XmlEnumValue("Centigram(s)")
    CENTIGRAM_S("Centigram(s)"),
    @XmlEnumValue("Centiliter(s)")
    CENTILITER_S("Centiliter(s)"),
    @XmlEnumValue("Centimeter(s)")
    CENTIMETER_S("Centimeter(s)"),
    @XmlEnumValue("Column(s)")
    COLUMN_S("Column(s)"),
    @XmlEnumValue("CubicFeet")
    CUBIC_FEET("CubicFeet"),
    @XmlEnumValue("CubicFoot")
    CUBIC_FOOT("CubicFoot"),
    @XmlEnumValue("CubicInch(es)")
    CUBIC_INCH_ES("CubicInch(es)"),
    @XmlEnumValue("CubicKilometer(s)")
    CUBIC_KILOMETER_S("CubicKilometer(s)"),
    @XmlEnumValue("CubicMeter(s)")
    CUBIC_METER_S("CubicMeter(s)"),
    @XmlEnumValue("CubicYard(s)")
    CUBIC_YARD_S("CubicYard(s)"),
    @XmlEnumValue("Day(s)")
    DAY_S("Day(s)"),
    @XmlEnumValue("Degree(s)")
    DEGREE_S("Degree(s)"),
    @XmlEnumValue("Dozen")
    DOZEN("Dozen"),
    @XmlEnumValue("dpi-DotsPerInch")
    DPI_DOTS_PER_INCH("dpi-DotsPerInch"),
    @XmlEnumValue("Each")
    EACH("Each"),
    @XmlEnumValue("Fahrenheit")
    FAHRENHEIT("Fahrenheit"),
    @XmlEnumValue("Feet")
    FEET("Feet"),
    @XmlEnumValue("FluidOunce(s)")
    FLUID_OUNCE_S("FluidOunce(s)"),
    @XmlEnumValue("Foot")
    FOOT("Foot"),
    @XmlEnumValue("FullDuration")
    FULL_DURATION("FullDuration"),
    @XmlEnumValue("Gallon(s)")
    GALLON_S("Gallon(s)"),
    @XmlEnumValue("GB-Gigabyte")
    GB_GIGABYTE("GB-Gigabyte"),
    @XmlEnumValue("Gbps-GigabitsPerSecond")
    GBPS_GIGABITS_PER_SECOND("Gbps-GigabitsPerSecond"),
    @XmlEnumValue("GBps-GigabytesPerSecond")
    G_BPS_GIGABYTES_PER_SECOND("GBps-GigabytesPerSecond"),
    @XmlEnumValue("GHz-Gigahertz")
    G_HZ_GIGAHERTZ("GHz-Gigahertz"),
    @XmlEnumValue("Glass")
    GLASS("Glass"),
    @XmlEnumValue("Gram(s)")
    GRAM_S("Gram(s)"),
    @XmlEnumValue("Hectare(s)")
    HECTARE_S("Hectare(s)"),
    @XmlEnumValue("HorsePower")
    HORSE_POWER("HorsePower"),
    @XmlEnumValue("Hour(s)")
    HOUR_S("Hour(s)"),
    @XmlEnumValue("Inch(es)")
    INCH_ES("Inch(es)"),
    @XmlEnumValue("KB-Kilobyte")
    KB_KILOBYTE("KB-Kilobyte"),
    @XmlEnumValue("Kbps-KilobitsPerSecond")
    KBPS_KILOBITS_PER_SECOND("Kbps-KilobitsPerSecond"),
    @XmlEnumValue("kHz-Kilohertz")
    K_HZ_KILOHERTZ("kHz-Kilohertz"),
    @XmlEnumValue("Kilogram(s)")
    KILOGRAM_S("Kilogram(s)"),
    @XmlEnumValue("Kiloliter(s)")
    KILOLITER_S("Kiloliter(s)"),
    @XmlEnumValue("Kilometer(s)")
    KILOMETER_S("Kilometer(s)"),
    @XmlEnumValue("KilometersPerHour")
    KILOMETERS_PER_HOUR("KilometersPerHour"),
    @XmlEnumValue("Kilowatt(s)")
    KILOWATT_S("Kilowatt(s)"),
    @XmlEnumValue("Knot(s)")
    KNOT_S("Knot(s)"),
    @XmlEnumValue("Litre(s)")
    LITRE_S("Litre(s)"),
    @XmlEnumValue("Magnum")
    MAGNUM("Magnum"),
    @XmlEnumValue("MB-Megabyte")
    MB_MEGABYTE("MB-Megabyte"),
    @XmlEnumValue("Mbps-MegabitsPerSecond")
    MBPS_MEGABITS_PER_SECOND("Mbps-MegabitsPerSecond"),
    @XmlEnumValue("MBps-MegabytesPerSecond")
    M_BPS_MEGABYTES_PER_SECOND("MBps-MegabytesPerSecond"),
    @XmlEnumValue("Metre(s)")
    METRE_S("Metre(s)"),
    @XmlEnumValue("MHz-Megahertz")
    M_HZ_MEGAHERTZ("MHz-Megahertz"),
    @XmlEnumValue("Microgram(s)")
    MICROGRAM_S("Microgram(s)"),
    @XmlEnumValue("Mile(s)")
    MILE_S("Mile(s)"),
    @XmlEnumValue("MilesPerGallon")
    MILES_PER_GALLON("MilesPerGallon"),
    @XmlEnumValue("MilesPerHour")
    MILES_PER_HOUR("MilesPerHour"),
    @XmlEnumValue("Milligram(s)")
    MILLIGRAM_S("Milligram(s)"),
    @XmlEnumValue("Milliliter(s)")
    MILLILITER_S("Milliliter(s)"),
    @XmlEnumValue("Millimeter(s)")
    MILLIMETER_S("Millimeter(s)"),
    @XmlEnumValue("Million(s)")
    MILLION_S("Million(s)"),
    @XmlEnumValue("Minute(s)")
    MINUTE_S("Minute(s)"),
    @XmlEnumValue("Month(s)")
    MONTH_S("Month(s)"),
    @XmlEnumValue("Morgan(s)")
    MORGAN_S("Morgan(s)"),
    @XmlEnumValue("Nanogram(s)")
    NANOGRAM_S("Nanogram(s)"),
    @XmlEnumValue("Nanometer(s)")
    NANOMETER_S("Nanometer(s)"),
    @XmlEnumValue("Ohm(s)")
    OHM_S("Ohm(s)"),
    @XmlEnumValue("Ounce(s)")
    OUNCE_S("Ounce(s)"),
    @XmlEnumValue("Pica(s)")
    PICA_S("Pica(s)"),
    @XmlEnumValue("Piece(s)")
    PIECE_S("Piece(s)"),
    @XmlEnumValue("Pint(s)")
    PINT_S("Pint(s)"),
    @XmlEnumValue("Pixel(s)")
    PIXEL_S("Pixel(s)"),
    @XmlEnumValue("Pixels/CM")
    PIXELS_CM("Pixels/CM"),
    @XmlEnumValue("Pixels/Inch")
    PIXELS_INCH("Pixels/Inch"),
    @XmlEnumValue("Point(s)")
    POINT_S("Point(s)"),
    @XmlEnumValue("Pound(s)")
    POUND_S("Pound(s)"),
    @XmlEnumValue("Quart(s)")
    QUART_S("Quart(s)"),
    @XmlEnumValue("Resolution")
    RESOLUTION("Resolution"),
    @XmlEnumValue("Rod(s)")
    ROD_S("Rod(s)"),
    @XmlEnumValue("Second(s)")
    SECOND_S("Second(s)"),
    @XmlEnumValue("SquareAcre(s)")
    SQUARE_ACRE_S("SquareAcre(s)"),
    @XmlEnumValue("SquareFeet")
    SQUARE_FEET("SquareFeet"),
    @XmlEnumValue("SquareFoot")
    SQUARE_FOOT("SquareFoot"),
    @XmlEnumValue("SquareInch(es)")
    SQUARE_INCH_ES("SquareInch(es)"),
    @XmlEnumValue("SquareKilometer(s)")
    SQUARE_KILOMETER_S("SquareKilometer(s)"),
    @XmlEnumValue("SquareMetre(s)")
    SQUARE_METRE_S("SquareMetre(s)"),
    @XmlEnumValue("SquareMile(s)")
    SQUARE_MILE_S("SquareMile(s)"),
    @XmlEnumValue("SquareYard(s)")
    SQUARE_YARD_S("SquareYard(s)"),
    @XmlEnumValue("TB-Terabyte")
    TB_TERABYTE("TB-Terabyte"),
    @XmlEnumValue("Volt(s)")
    VOLT_S("Volt(s)"),
    @XmlEnumValue("Watt(s)")
    WATT_S("Watt(s)"),
    @XmlEnumValue("Week(s)")
    WEEK_S("Week(s)"),
    @XmlEnumValue("Yard(s)")
    YARD_S("Yard(s)"),
    @XmlEnumValue("Year(s)")
    YEAR_S("Year(s)"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListUnitOfMeasureBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListUnitOfMeasureBase fromValue(String v) {
        for (ListUnitOfMeasureBase c: ListUnitOfMeasureBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
