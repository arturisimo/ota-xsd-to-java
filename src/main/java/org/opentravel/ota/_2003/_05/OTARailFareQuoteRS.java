
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="FareInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="FilingOperator" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *                     &amp;lt;element name="FareOption" maxOccurs="999"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Fare" type="{http://www.opentravel.org/OTA/2003/05}RailFareType"/&amp;gt;
 *                               &amp;lt;element name="OriginDestination" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                                         &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TimeWindowGroup"/&amp;gt;
 *                             &amp;lt;attribute name="TrainTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                             &amp;lt;attribute name="BlockoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="Note" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "fareInfo",
    "note",
    "errors"
})
@XmlRootElement(name = "OTA_RailFareQuoteRS")
public class OTARailFareQuoteRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "FareInfo")
    protected List<OTARailFareQuoteRS.FareInfo> fareInfo;
    @XmlElement(name = "Note")
    protected List<FreeTextType> note;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Gets the value of the fareInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFareInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTARailFareQuoteRS.FareInfo }
     * 
     * 
     */
    public List<OTARailFareQuoteRS.FareInfo> getFareInfo() {
        if (fareInfo == null) {
            fareInfo = new ArrayList<OTARailFareQuoteRS.FareInfo>();
        }
        return this.fareInfo;
    }

    /**
     * Gets the value of the note property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the note property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getNote().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link FreeTextType }
     * 
     * 
     */
    public List<FreeTextType> getNote() {
        if (note == null) {
            note = new ArrayList<FreeTextType>();
        }
        return this.note;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="FilingOperator" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="FareOption" maxOccurs="999"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Fare" type="{http://www.opentravel.org/OTA/2003/05}RailFareType"/&amp;gt;
     *                   &amp;lt;element name="OriginDestination" maxOccurs="99" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                             &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TimeWindowGroup"/&amp;gt;
     *                 &amp;lt;attribute name="TrainTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                 &amp;lt;attribute name="BlockoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originLocation",
        "destinationLocation",
        "filingOperator",
        "fareOption"
    })
    public static class FareInfo {

        @XmlElement(name = "OriginLocation")
        protected LocationType originLocation;
        @XmlElement(name = "DestinationLocation")
        protected LocationType destinationLocation;
        @XmlElement(name = "FilingOperator")
        protected CompanyNameType filingOperator;
        @XmlElement(name = "FareOption", required = true)
        protected List<OTARailFareQuoteRS.FareInfo.FareOption> fareOption;

        /**
         * Obtiene el valor de la propiedad originLocation.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getOriginLocation() {
            return originLocation;
        }

        /**
         * Define el valor de la propiedad originLocation.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setOriginLocation(LocationType value) {
            this.originLocation = value;
        }

        /**
         * Obtiene el valor de la propiedad destinationLocation.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getDestinationLocation() {
            return destinationLocation;
        }

        /**
         * Define el valor de la propiedad destinationLocation.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setDestinationLocation(LocationType value) {
            this.destinationLocation = value;
        }

        /**
         * Obtiene el valor de la propiedad filingOperator.
         * 
         * @return
         *     possible object is
         *     {@link CompanyNameType }
         *     
         */
        public CompanyNameType getFilingOperator() {
            return filingOperator;
        }

        /**
         * Define el valor de la propiedad filingOperator.
         * 
         * @param value
         *     allowed object is
         *     {@link CompanyNameType }
         *     
         */
        public void setFilingOperator(CompanyNameType value) {
            this.filingOperator = value;
        }

        /**
         * Gets the value of the fareOption property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fareOption property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFareOption().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTARailFareQuoteRS.FareInfo.FareOption }
         * 
         * 
         */
        public List<OTARailFareQuoteRS.FareInfo.FareOption> getFareOption() {
            if (fareOption == null) {
                fareOption = new ArrayList<OTARailFareQuoteRS.FareInfo.FareOption>();
            }
            return this.fareOption;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ClassCode" type="{http://www.opentravel.org/OTA/2003/05}ClassCodeType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Accommodation" type="{http://www.opentravel.org/OTA/2003/05}AccommodationType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Fare" type="{http://www.opentravel.org/OTA/2003/05}RailFareType"/&amp;gt;
         *         &amp;lt;element name="OriginDestination" maxOccurs="99" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *                   &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}TimeWindowGroup"/&amp;gt;
         *       &amp;lt;attribute name="TrainTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="BlockoutDatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "classCode",
            "accommodation",
            "fare",
            "originDestination"
        })
        public static class FareOption {

            @XmlElement(name = "ClassCode")
            protected ClassCodeType classCode;
            @XmlElement(name = "Accommodation")
            protected AccommodationType accommodation;
            @XmlElement(name = "Fare", required = true)
            protected RailFareType fare;
            @XmlElement(name = "OriginDestination")
            protected List<OTARailFareQuoteRS.FareInfo.FareOption.OriginDestination> originDestination;
            @XmlAttribute(name = "TrainTypeCode")
            protected String trainTypeCode;
            @XmlAttribute(name = "BlockoutDatesInd")
            protected Boolean blockoutDatesInd;
            @XmlAttribute(name = "EarliestDate")
            protected String earliestDate;
            @XmlAttribute(name = "LatestDate")
            protected String latestDate;
            @XmlAttribute(name = "DOW")
            protected DayOfWeekType dow;

            /**
             * Obtiene el valor de la propiedad classCode.
             * 
             * @return
             *     possible object is
             *     {@link ClassCodeType }
             *     
             */
            public ClassCodeType getClassCode() {
                return classCode;
            }

            /**
             * Define el valor de la propiedad classCode.
             * 
             * @param value
             *     allowed object is
             *     {@link ClassCodeType }
             *     
             */
            public void setClassCode(ClassCodeType value) {
                this.classCode = value;
            }

            /**
             * Obtiene el valor de la propiedad accommodation.
             * 
             * @return
             *     possible object is
             *     {@link AccommodationType }
             *     
             */
            public AccommodationType getAccommodation() {
                return accommodation;
            }

            /**
             * Define el valor de la propiedad accommodation.
             * 
             * @param value
             *     allowed object is
             *     {@link AccommodationType }
             *     
             */
            public void setAccommodation(AccommodationType value) {
                this.accommodation = value;
            }

            /**
             * Obtiene el valor de la propiedad fare.
             * 
             * @return
             *     possible object is
             *     {@link RailFareType }
             *     
             */
            public RailFareType getFare() {
                return fare;
            }

            /**
             * Define el valor de la propiedad fare.
             * 
             * @param value
             *     allowed object is
             *     {@link RailFareType }
             *     
             */
            public void setFare(RailFareType value) {
                this.fare = value;
            }

            /**
             * Gets the value of the originDestination property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestination property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getOriginDestination().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTARailFareQuoteRS.FareInfo.FareOption.OriginDestination }
             * 
             * 
             */
            public List<OTARailFareQuoteRS.FareInfo.FareOption.OriginDestination> getOriginDestination() {
                if (originDestination == null) {
                    originDestination = new ArrayList<OTARailFareQuoteRS.FareInfo.FareOption.OriginDestination>();
                }
                return this.originDestination;
            }

            /**
             * Obtiene el valor de la propiedad trainTypeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTrainTypeCode() {
                return trainTypeCode;
            }

            /**
             * Define el valor de la propiedad trainTypeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTrainTypeCode(String value) {
                this.trainTypeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad blockoutDatesInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isBlockoutDatesInd() {
                return blockoutDatesInd;
            }

            /**
             * Define el valor de la propiedad blockoutDatesInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setBlockoutDatesInd(Boolean value) {
                this.blockoutDatesInd = value;
            }

            /**
             * Obtiene el valor de la propiedad earliestDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEarliestDate() {
                return earliestDate;
            }

            /**
             * Define el valor de la propiedad earliestDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEarliestDate(String value) {
                this.earliestDate = value;
            }

            /**
             * Obtiene el valor de la propiedad latestDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLatestDate() {
                return latestDate;
            }

            /**
             * Define el valor de la propiedad latestDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLatestDate(String value) {
                this.latestDate = value;
            }

            /**
             * Obtiene el valor de la propiedad dow.
             * 
             * @return
             *     possible object is
             *     {@link DayOfWeekType }
             *     
             */
            public DayOfWeekType getDOW() {
                return dow;
            }

            /**
             * Define el valor de la propiedad dow.
             * 
             * @param value
             *     allowed object is
             *     {@link DayOfWeekType }
             *     
             */
            public void setDOW(DayOfWeekType value) {
                this.dow = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="OriginLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
             *         &amp;lt;element name="DestinationLocation" type="{http://www.opentravel.org/OTA/2003/05}LocationType" minOccurs="0"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "originLocation",
                "destinationLocation"
            })
            public static class OriginDestination {

                @XmlElement(name = "OriginLocation")
                protected LocationType originLocation;
                @XmlElement(name = "DestinationLocation")
                protected LocationType destinationLocation;

                /**
                 * Obtiene el valor de la propiedad originLocation.
                 * 
                 * @return
                 *     possible object is
                 *     {@link LocationType }
                 *     
                 */
                public LocationType getOriginLocation() {
                    return originLocation;
                }

                /**
                 * Define el valor de la propiedad originLocation.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link LocationType }
                 *     
                 */
                public void setOriginLocation(LocationType value) {
                    this.originLocation = value;
                }

                /**
                 * Obtiene el valor de la propiedad destinationLocation.
                 * 
                 * @return
                 *     possible object is
                 *     {@link LocationType }
                 *     
                 */
                public LocationType getDestinationLocation() {
                    return destinationLocation;
                }

                /**
                 * Define el valor de la propiedad destinationLocation.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link LocationType }
                 *     
                 */
                public void setDestinationLocation(LocationType value) {
                    this.destinationLocation = value;
                }

            }

        }

    }

}
