
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferTripStage.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferTripStage"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="PreTrip"/&amp;gt;
 *     &amp;lt;enumeration value="InTrip"/&amp;gt;
 *     &amp;lt;enumeration value="PostTrip"/&amp;gt;
 *     &amp;lt;enumeration value="_Other"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferTripStage")
@XmlEnum
public enum ListOfferTripStage {


    /**
     * Passenger is checking in at airport.
     * 
     */
    @XmlEnumValue("PreTrip")
    PRE_TRIP("PreTrip"),

    /**
     * The traveler has started, but not completed, this trip mode.
     * 
     */
    @XmlEnumValue("InTrip")
    IN_TRIP("InTrip"),

    /**
     * The traveler has completed the trip mode.
     * 
     */
    @XmlEnumValue("PostTrip")
    POST_TRIP("PostTrip"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("_Other")
    OTHER("_Other");
    private final String value;

    ListOfferTripStage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferTripStage fromValue(String v) {
        for (ListOfferTripStage c: ListOfferTripStage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
