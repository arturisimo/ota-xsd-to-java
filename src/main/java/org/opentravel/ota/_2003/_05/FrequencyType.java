
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para FrequencyType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="FrequencyType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="OneTimeOnly"/&amp;gt;
 *     &amp;lt;enumeration value="Biennial"/&amp;gt;
 *     &amp;lt;enumeration value="Annual"/&amp;gt;
 *     &amp;lt;enumeration value="Semi-annual"/&amp;gt;
 *     &amp;lt;enumeration value="Quarterly"/&amp;gt;
 *     &amp;lt;enumeration value="Monthly"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "FrequencyType")
@XmlEnum
public enum FrequencyType {

    @XmlEnumValue("OneTimeOnly")
    ONE_TIME_ONLY("OneTimeOnly"),
    @XmlEnumValue("Biennial")
    BIENNIAL("Biennial"),
    @XmlEnumValue("Annual")
    ANNUAL("Annual"),
    @XmlEnumValue("Semi-annual")
    SEMI_ANNUAL("Semi-annual"),
    @XmlEnumValue("Quarterly")
    QUARTERLY("Quarterly"),
    @XmlEnumValue("Monthly")
    MONTHLY("Monthly");
    private final String value;

    FrequencyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FrequencyType fromValue(String v) {
        for (FrequencyType c: FrequencyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
