
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para AncillaryServiceFamilyEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AncillaryServiceFamilyEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="All"/&amp;gt;
 *     &amp;lt;enumeration value="A_FreeBaggageAllowance"/&amp;gt;
 *     &amp;lt;enumeration value="C_BaggageCharges"/&amp;gt;
 *     &amp;lt;enumeration value="F_FlightRelated"/&amp;gt;
 *     &amp;lt;enumeration value="T_TicketRelated"/&amp;gt;
 *     &amp;lt;enumeration value="M_MerchandiseRelated"/&amp;gt;
 *     &amp;lt;enumeration value="R_RuleBuster"/&amp;gt;
 *     &amp;lt;enumeration value="P_PrepaidBaggage"/&amp;gt;
 *     &amp;lt;enumeration value="E_Embargos"/&amp;gt;
 *     &amp;lt;enumeration value="GT_GroundTransportationNonAirServices"/&amp;gt;
 *     &amp;lt;enumeration value="IE_InFlightEntertainment"/&amp;gt;
 *     &amp;lt;enumeration value="LG_Lounge"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AncillaryServiceFamilyEnum")
@XmlEnum
public enum AncillaryServiceFamilyEnum {


    /**
     * All service family types.
     * 
     */
    @XmlEnumValue("All")
    ALL("All"),

    /**
     * Free Baggage Allowance
     * 
     */
    @XmlEnumValue("A_FreeBaggageAllowance")
    A_FREE_BAGGAGE_ALLOWANCE("A_FreeBaggageAllowance"),

    /**
     * Baggage Charges
     * 
     */
    @XmlEnumValue("C_BaggageCharges")
    C_BAGGAGE_CHARGES("C_BaggageCharges"),

    /**
     * Flight-Related.
     * 
     */
    @XmlEnumValue("F_FlightRelated")
    F_FLIGHT_RELATED("F_FlightRelated"),

    /**
     * Ticket-Related.
     * 
     */
    @XmlEnumValue("T_TicketRelated")
    T_TICKET_RELATED("T_TicketRelated"),

    /**
     * Merchandise-Related
     * 
     */
    @XmlEnumValue("M_MerchandiseRelated")
    M_MERCHANDISE_RELATED("M_MerchandiseRelated"),

    /**
     * Rule Buster (fare override)
     * 
     */
    @XmlEnumValue("R_RuleBuster")
    R_RULE_BUSTER("R_RuleBuster"),

    /**
     * Pre-paid Baggage
     * 
     */
    @XmlEnumValue("P_PrepaidBaggage")
    P_PREPAID_BAGGAGE("P_PrepaidBaggage"),

    /**
     * Embargos
     * 
     */
    @XmlEnumValue("E_Embargos")
    E_EMBARGOS("E_Embargos"),
    @XmlEnumValue("GT_GroundTransportationNonAirServices")
    GT_GROUND_TRANSPORTATION_NON_AIR_SERVICES("GT_GroundTransportationNonAirServices"),
    @XmlEnumValue("IE_InFlightEntertainment")
    IE_IN_FLIGHT_ENTERTAINMENT("IE_InFlightEntertainment"),
    @XmlEnumValue("LG_Lounge")
    LG_LOUNGE("LG_Lounge"),

    /**
     * OpenTravel Reserved words, "Other_" as enumerated value for  open enumeration support to support additional compartment type. The Value corresponding to "Other_" will be specified in the  "Value" attribute. See CompartmentType.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    AncillaryServiceFamilyEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AncillaryServiceFamilyEnum fromValue(String v) {
        for (AncillaryServiceFamilyEnum c: AncillaryServiceFamilyEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
