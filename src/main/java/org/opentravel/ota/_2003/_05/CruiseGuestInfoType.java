
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains guest information for the specified sailing.
 * 
 * &lt;p&gt;Clase Java para CruiseGuestInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CruiseGuestInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ReservationID" type="{http://www.opentravel.org/OTA/2003/05}ReservationID_Type" maxOccurs="2" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="GuestDetails"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="GuestDetail" type="{http://www.opentravel.org/OTA/2003/05}CruiseGuestDetailType" maxOccurs="9"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="LinkedBookings" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="LinkedBooking" maxOccurs="32"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RelatedTravelerType"&amp;gt;
 *                           &amp;lt;attribute name="LinkTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PaymentOptions" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PaymentOption" maxOccurs="9"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
 *                           &amp;lt;attribute name="ExtendedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="PaymentPurpose"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;enumeration value="GiftOrder"/&amp;gt;
 *                                 &amp;lt;enumeration value="Reservation"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="ExtendedDepositDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                           &amp;lt;attribute name="ReferenceNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="CancellationPenalty" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CruiseGuestInfoType", propOrder = {
    "reservationID",
    "guestDetails",
    "linkedBookings",
    "paymentOptions",
    "cancellationPenalty"
})
public class CruiseGuestInfoType {

    @XmlElement(name = "ReservationID")
    protected List<ReservationIDType> reservationID;
    @XmlElement(name = "GuestDetails", required = true)
    protected CruiseGuestInfoType.GuestDetails guestDetails;
    @XmlElement(name = "LinkedBookings")
    protected CruiseGuestInfoType.LinkedBookings linkedBookings;
    @XmlElement(name = "PaymentOptions")
    protected CruiseGuestInfoType.PaymentOptions paymentOptions;
    @XmlElement(name = "CancellationPenalty")
    protected CruiseGuestInfoType.CancellationPenalty cancellationPenalty;

    /**
     * Gets the value of the reservationID property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the reservationID property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getReservationID().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ReservationIDType }
     * 
     * 
     */
    public List<ReservationIDType> getReservationID() {
        if (reservationID == null) {
            reservationID = new ArrayList<ReservationIDType>();
        }
        return this.reservationID;
    }

    /**
     * Obtiene el valor de la propiedad guestDetails.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestInfoType.GuestDetails }
     *     
     */
    public CruiseGuestInfoType.GuestDetails getGuestDetails() {
        return guestDetails;
    }

    /**
     * Define el valor de la propiedad guestDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestInfoType.GuestDetails }
     *     
     */
    public void setGuestDetails(CruiseGuestInfoType.GuestDetails value) {
        this.guestDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad linkedBookings.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestInfoType.LinkedBookings }
     *     
     */
    public CruiseGuestInfoType.LinkedBookings getLinkedBookings() {
        return linkedBookings;
    }

    /**
     * Define el valor de la propiedad linkedBookings.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestInfoType.LinkedBookings }
     *     
     */
    public void setLinkedBookings(CruiseGuestInfoType.LinkedBookings value) {
        this.linkedBookings = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentOptions.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestInfoType.PaymentOptions }
     *     
     */
    public CruiseGuestInfoType.PaymentOptions getPaymentOptions() {
        return paymentOptions;
    }

    /**
     * Define el valor de la propiedad paymentOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestInfoType.PaymentOptions }
     *     
     */
    public void setPaymentOptions(CruiseGuestInfoType.PaymentOptions value) {
        this.paymentOptions = value;
    }

    /**
     * Obtiene el valor de la propiedad cancellationPenalty.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestInfoType.CancellationPenalty }
     *     
     */
    public CruiseGuestInfoType.CancellationPenalty getCancellationPenalty() {
        return cancellationPenalty;
    }

    /**
     * Define el valor de la propiedad cancellationPenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestInfoType.CancellationPenalty }
     *     
     */
    public void setCancellationPenalty(CruiseGuestInfoType.CancellationPenalty value) {
        this.cancellationPenalty = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CancellationPenalty {

        @XmlAttribute(name = "Amount")
        protected BigDecimal amount;

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="GuestDetail" type="{http://www.opentravel.org/OTA/2003/05}CruiseGuestDetailType" maxOccurs="9"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "guestDetail"
    })
    public static class GuestDetails {

        @XmlElement(name = "GuestDetail", required = true)
        protected List<CruiseGuestDetailType> guestDetail;

        /**
         * Gets the value of the guestDetail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the guestDetail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getGuestDetail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CruiseGuestDetailType }
         * 
         * 
         */
        public List<CruiseGuestDetailType> getGuestDetail() {
            if (guestDetail == null) {
                guestDetail = new ArrayList<CruiseGuestDetailType>();
            }
            return this.guestDetail;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="LinkedBooking" maxOccurs="32"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RelatedTravelerType"&amp;gt;
     *                 &amp;lt;attribute name="LinkTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "linkedBooking"
    })
    public static class LinkedBookings {

        @XmlElement(name = "LinkedBooking", required = true)
        protected List<CruiseGuestInfoType.LinkedBookings.LinkedBooking> linkedBooking;

        /**
         * Gets the value of the linkedBooking property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the linkedBooking property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLinkedBooking().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CruiseGuestInfoType.LinkedBookings.LinkedBooking }
         * 
         * 
         */
        public List<CruiseGuestInfoType.LinkedBookings.LinkedBooking> getLinkedBooking() {
            if (linkedBooking == null) {
                linkedBooking = new ArrayList<CruiseGuestInfoType.LinkedBookings.LinkedBooking>();
            }
            return this.linkedBooking;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RelatedTravelerType"&amp;gt;
         *       &amp;lt;attribute name="LinkTypeCode" type="{http://www.opentravel.org/OTA/2003/05}ListOfOTA_CodeType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class LinkedBooking
            extends RelatedTravelerType
        {

            @XmlAttribute(name = "LinkTypeCode")
            protected List<String> linkTypeCode;

            /**
             * Gets the value of the linkTypeCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the linkTypeCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getLinkTypeCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getLinkTypeCode() {
                if (linkTypeCode == null) {
                    linkTypeCode = new ArrayList<String>();
                }
                return this.linkTypeCode;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PaymentOption" maxOccurs="9"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
     *                 &amp;lt;attribute name="ExtendedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="PaymentPurpose"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;enumeration value="GiftOrder"/&amp;gt;
     *                       &amp;lt;enumeration value="Reservation"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="ExtendedDepositDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                 &amp;lt;attribute name="ReferenceNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paymentOption"
    })
    public static class PaymentOptions {

        @XmlElement(name = "PaymentOption", required = true)
        protected List<CruiseGuestInfoType.PaymentOptions.PaymentOption> paymentOption;

        /**
         * Gets the value of the paymentOption property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentOption property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPaymentOption().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CruiseGuestInfoType.PaymentOptions.PaymentOption }
         * 
         * 
         */
        public List<CruiseGuestInfoType.PaymentOptions.PaymentOption> getPaymentOption() {
            if (paymentOption == null) {
                paymentOption = new ArrayList<CruiseGuestInfoType.PaymentOptions.PaymentOption>();
            }
            return this.paymentOption;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentDetailType"&amp;gt;
         *       &amp;lt;attribute name="ExtendedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="PaymentPurpose"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;enumeration value="GiftOrder"/&amp;gt;
         *             &amp;lt;enumeration value="Reservation"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="ExtendedDepositDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *       &amp;lt;attribute name="ReferenceNumber" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PaymentOption
            extends PaymentDetailType
        {

            @XmlAttribute(name = "ExtendedIndicator")
            protected Boolean extendedIndicator;
            @XmlAttribute(name = "PaymentPurpose")
            protected String paymentPurpose;
            @XmlAttribute(name = "ExtendedDepositDate")
            protected String extendedDepositDate;
            @XmlAttribute(name = "ReferenceNumber")
            protected String referenceNumber;

            /**
             * Obtiene el valor de la propiedad extendedIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isExtendedIndicator() {
                return extendedIndicator;
            }

            /**
             * Define el valor de la propiedad extendedIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setExtendedIndicator(Boolean value) {
                this.extendedIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentPurpose.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentPurpose() {
                return paymentPurpose;
            }

            /**
             * Define el valor de la propiedad paymentPurpose.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentPurpose(String value) {
                this.paymentPurpose = value;
            }

            /**
             * Obtiene el valor de la propiedad extendedDepositDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExtendedDepositDate() {
                return extendedDepositDate;
            }

            /**
             * Define el valor de la propiedad extendedDepositDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExtendedDepositDate(String value) {
                this.extendedDepositDate = value;
            }

            /**
             * Obtiene el valor de la propiedad referenceNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReferenceNumber() {
                return referenceNumber;
            }

            /**
             * Define el valor de la propiedad referenceNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReferenceNumber(String value) {
                this.referenceNumber = value;
            }

        }

    }

}
