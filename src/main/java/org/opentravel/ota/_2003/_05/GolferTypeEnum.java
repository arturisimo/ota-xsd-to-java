
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para GolferTypeEnum.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="GolferTypeEnum"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="ClubMember"/&amp;gt;
 *     &amp;lt;enumeration value="GuestOfMember"/&amp;gt;
 *     &amp;lt;enumeration value="GuestOfResort"/&amp;gt;
 *     &amp;lt;enumeration value="LocalResident"/&amp;gt;
 *     &amp;lt;enumeration value="LoyaltyProgramMember"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "GolferTypeEnum")
@XmlEnum
public enum GolferTypeEnum {


    /**
     * TBC...
     * 
     */
    @XmlEnumValue("ClubMember")
    CLUB_MEMBER("ClubMember"),

    /**
     * TBC...
     * 
     */
    @XmlEnumValue("GuestOfMember")
    GUEST_OF_MEMBER("GuestOfMember"),

    /**
     * TBC...
     * 
     */
    @XmlEnumValue("GuestOfResort")
    GUEST_OF_RESORT("GuestOfResort"),

    /**
     * TBC...
     * 
     */
    @XmlEnumValue("LocalResident")
    LOCAL_RESIDENT("LocalResident"),

    /**
     * TBC...
     * 
     */
    @XmlEnumValue("LoyaltyProgramMember")
    LOYALTY_PROGRAM_MEMBER("LoyaltyProgramMember"),

    /**
     * This is an OpenTravel reserved word used to support an open enumeration that is known between trading partners. If you select this value, enter your golfer type into the &#064;extension attribute.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    GolferTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GolferTypeEnum fromValue(String v) {
        for (GolferTypeEnum c: GolferTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
