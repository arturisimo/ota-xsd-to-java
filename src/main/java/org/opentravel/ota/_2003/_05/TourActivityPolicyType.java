
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Contains policies associated with the tour/activity.
 * 
 * &lt;p&gt;Clase Java para TourActivityPolicyType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TourActivityPolicyType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Participant" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="MinParticipants" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaxParticipants" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Cancel" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                 &amp;lt;attribute name="CancelDeadline" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *                 &amp;lt;attribute name="BookingDeadline" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Other" maxOccurs="9" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                 &amp;lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="InsuranceReqInd"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="NotRequired"/&amp;gt;
 *             &amp;lt;enumeration value="Required_MustBePurchasedFromSupplier"/&amp;gt;
 *             &amp;lt;enumeration value="Required_MayBeProvidedByParticipant"/&amp;gt;
 *             &amp;lt;enumeration value="Unknown"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="InsuranceOverview" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="WaiverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="WaiverOverview" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TourActivityPolicyType", propOrder = {
    "participant",
    "cancel",
    "other"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTATourActivityBookRS.ReservationDetails.Policy.class,
    org.opentravel.ota._2003._05.OTATourActivityCancelRS.Reservation.ReservationInfo.Policy.class,
    org.opentravel.ota._2003._05.OTATourActivityResRetrieveRS.Detail.Policy.class
})
public class TourActivityPolicyType {

    @XmlElement(name = "Participant")
    protected TourActivityPolicyType.Participant participant;
    @XmlElement(name = "Cancel")
    protected TourActivityPolicyType.Cancel cancel;
    @XmlElement(name = "Other")
    protected List<TourActivityPolicyType.Other> other;
    @XmlAttribute(name = "InsuranceReqInd")
    protected String insuranceReqInd;
    @XmlAttribute(name = "InsuranceOverview")
    protected String insuranceOverview;
    @XmlAttribute(name = "WaiverInd")
    protected Boolean waiverInd;
    @XmlAttribute(name = "WaiverOverview")
    protected String waiverOverview;

    /**
     * Obtiene el valor de la propiedad participant.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityPolicyType.Participant }
     *     
     */
    public TourActivityPolicyType.Participant getParticipant() {
        return participant;
    }

    /**
     * Define el valor de la propiedad participant.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityPolicyType.Participant }
     *     
     */
    public void setParticipant(TourActivityPolicyType.Participant value) {
        this.participant = value;
    }

    /**
     * Obtiene el valor de la propiedad cancel.
     * 
     * @return
     *     possible object is
     *     {@link TourActivityPolicyType.Cancel }
     *     
     */
    public TourActivityPolicyType.Cancel getCancel() {
        return cancel;
    }

    /**
     * Define el valor de la propiedad cancel.
     * 
     * @param value
     *     allowed object is
     *     {@link TourActivityPolicyType.Cancel }
     *     
     */
    public void setCancel(TourActivityPolicyType.Cancel value) {
        this.cancel = value;
    }

    /**
     * Gets the value of the other property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the other property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOther().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TourActivityPolicyType.Other }
     * 
     * 
     */
    public List<TourActivityPolicyType.Other> getOther() {
        if (other == null) {
            other = new ArrayList<TourActivityPolicyType.Other>();
        }
        return this.other;
    }

    /**
     * Obtiene el valor de la propiedad insuranceReqInd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceReqInd() {
        return insuranceReqInd;
    }

    /**
     * Define el valor de la propiedad insuranceReqInd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceReqInd(String value) {
        this.insuranceReqInd = value;
    }

    /**
     * Obtiene el valor de la propiedad insuranceOverview.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceOverview() {
        return insuranceOverview;
    }

    /**
     * Define el valor de la propiedad insuranceOverview.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceOverview(String value) {
        this.insuranceOverview = value;
    }

    /**
     * Obtiene el valor de la propiedad waiverInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWaiverInd() {
        return waiverInd;
    }

    /**
     * Define el valor de la propiedad waiverInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWaiverInd(Boolean value) {
        this.waiverInd = value;
    }

    /**
     * Obtiene el valor de la propiedad waiverOverview.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiverOverview() {
        return waiverOverview;
    }

    /**
     * Define el valor de la propiedad waiverOverview.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiverOverview(String value) {
        this.waiverOverview = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *       &amp;lt;attribute name="CancelDeadline" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *       &amp;lt;attribute name="BookingDeadline" type="{http://www.opentravel.org/OTA/2003/05}DateOrTimeOrDateTimeType" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Cancel {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "CancelDeadline")
        protected String cancelDeadline;
        @XmlAttribute(name = "BookingDeadline")
        protected String bookingDeadline;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad cancelDeadline.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelDeadline() {
            return cancelDeadline;
        }

        /**
         * Define el valor de la propiedad cancelDeadline.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelDeadline(String value) {
            this.cancelDeadline = value;
        }

        /**
         * Obtiene el valor de la propiedad bookingDeadline.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBookingDeadline() {
            return bookingDeadline;
        }

        /**
         * Define el valor de la propiedad bookingDeadline.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBookingDeadline(String value) {
            this.bookingDeadline = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *       &amp;lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Other {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Type")
        protected String type;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="MinParticipants" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaxParticipants" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Participant {

        @XmlAttribute(name = "MinParticipants")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger minParticipants;
        @XmlAttribute(name = "MaxParticipants")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxParticipants;
        @XmlAttribute(name = "MinAge")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger minAge;
        @XmlAttribute(name = "MaxAge")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger maxAge;

        /**
         * Obtiene el valor de la propiedad minParticipants.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMinParticipants() {
            return minParticipants;
        }

        /**
         * Define el valor de la propiedad minParticipants.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMinParticipants(BigInteger value) {
            this.minParticipants = value;
        }

        /**
         * Obtiene el valor de la propiedad maxParticipants.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxParticipants() {
            return maxParticipants;
        }

        /**
         * Define el valor de la propiedad maxParticipants.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxParticipants(BigInteger value) {
            this.maxParticipants = value;
        }

        /**
         * Obtiene el valor de la propiedad minAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMinAge() {
            return minAge;
        }

        /**
         * Define el valor de la propiedad minAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMinAge(BigInteger value) {
            this.minAge = value;
        }

        /**
         * Obtiene el valor de la propiedad maxAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxAge() {
            return maxAge;
        }

        /**
         * Define el valor de la propiedad maxAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxAge(BigInteger value) {
            this.maxAge = value;
        }

    }

}
