
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Location information with ontology reference.
 * 
 * &lt;p&gt;Clase Java para OntologyLocationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OntologyLocationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Type"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;simpleContent&amp;gt;
 *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferLocationType"&amp;gt;
 *                 &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/simpleContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;element name="GeneralLocation" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCodeType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/extension&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="Geocode" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;choice&amp;gt;
 *                       &amp;lt;sequence&amp;gt;
 *                         &amp;lt;element name="Latitude"&amp;gt;
 *                           &amp;lt;simpleType&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                               &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/simpleType&amp;gt;
 *                         &amp;lt;/element&amp;gt;
 *                         &amp;lt;element name="Longitude"&amp;gt;
 *                           &amp;lt;simpleType&amp;gt;
 *                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                               &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
 *                             &amp;lt;/restriction&amp;gt;
 *                           &amp;lt;/simpleType&amp;gt;
 *                         &amp;lt;/element&amp;gt;
 *                       &amp;lt;/sequence&amp;gt;
 *                       &amp;lt;element name="UniversalAddress"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;simpleContent&amp;gt;
 *                             &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
 *                               &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                                 &amp;lt;simpleType&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                     &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/simpleType&amp;gt;
 *                               &amp;lt;/attribute&amp;gt;
 *                             &amp;lt;/extension&amp;gt;
 *                           &amp;lt;/simpleContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/choice&amp;gt;
 *                     &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;attribute name="OntologyRefID"&amp;gt;
 *                     &amp;lt;simpleType&amp;gt;
 *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                         &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                       &amp;lt;/restriction&amp;gt;
 *                     &amp;lt;/simpleType&amp;gt;
 *                   &amp;lt;/attribute&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="PhysicalLocation" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyAddressType"&amp;gt;
 *                 &amp;lt;/extension&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OntologyLocationType", propOrder = {
    "type",
    "generalLocation",
    "geocode",
    "physicalLocation",
    "ontologyExtension"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.MultiModalOfferType.TripCharacteristics.Location.class
})
public class OntologyLocationType {

    @XmlElement(name = "Type", required = true)
    protected OntologyLocationType.Type type;
    @XmlElement(name = "GeneralLocation")
    protected OntologyLocationType.GeneralLocation generalLocation;
    @XmlElement(name = "Geocode")
    protected OntologyLocationType.Geocode geocode;
    @XmlElement(name = "PhysicalLocation")
    protected OntologyLocationType.PhysicalLocation physicalLocation;
    @XmlElement(name = "OntologyExtension")
    protected OntologyExtensionType ontologyExtension;

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link OntologyLocationType.Type }
     *     
     */
    public OntologyLocationType.Type getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyLocationType.Type }
     *     
     */
    public void setType(OntologyLocationType.Type value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad generalLocation.
     * 
     * @return
     *     possible object is
     *     {@link OntologyLocationType.GeneralLocation }
     *     
     */
    public OntologyLocationType.GeneralLocation getGeneralLocation() {
        return generalLocation;
    }

    /**
     * Define el valor de la propiedad generalLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyLocationType.GeneralLocation }
     *     
     */
    public void setGeneralLocation(OntologyLocationType.GeneralLocation value) {
        this.generalLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad geocode.
     * 
     * @return
     *     possible object is
     *     {@link OntologyLocationType.Geocode }
     *     
     */
    public OntologyLocationType.Geocode getGeocode() {
        return geocode;
    }

    /**
     * Define el valor de la propiedad geocode.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyLocationType.Geocode }
     *     
     */
    public void setGeocode(OntologyLocationType.Geocode value) {
        this.geocode = value;
    }

    /**
     * Obtiene el valor de la propiedad physicalLocation.
     * 
     * @return
     *     possible object is
     *     {@link OntologyLocationType.PhysicalLocation }
     *     
     */
    public OntologyLocationType.PhysicalLocation getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Define el valor de la propiedad physicalLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyLocationType.PhysicalLocation }
     *     
     */
    public void setPhysicalLocation(OntologyLocationType.PhysicalLocation value) {
        this.physicalLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad ontologyExtension.
     * 
     * @return
     *     possible object is
     *     {@link OntologyExtensionType }
     *     
     */
    public OntologyExtensionType getOntologyExtension() {
        return ontologyExtension;
    }

    /**
     * Define el valor de la propiedad ontologyExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link OntologyExtensionType }
     *     
     */
    public void setOntologyExtension(OntologyExtensionType value) {
        this.ontologyExtension = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyCodeType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ontologyExtension"
    })
    public static class GeneralLocation
        extends OntologyCodeType
    {

        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;choice&amp;gt;
     *           &amp;lt;sequence&amp;gt;
     *             &amp;lt;element name="Latitude"&amp;gt;
     *               &amp;lt;simpleType&amp;gt;
     *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                   &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                 &amp;lt;/restriction&amp;gt;
     *               &amp;lt;/simpleType&amp;gt;
     *             &amp;lt;/element&amp;gt;
     *             &amp;lt;element name="Longitude"&amp;gt;
     *               &amp;lt;simpleType&amp;gt;
     *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                   &amp;lt;pattern value="[a-zA-Z0-9]{1,64}"/&amp;gt;
     *                 &amp;lt;/restriction&amp;gt;
     *               &amp;lt;/simpleType&amp;gt;
     *             &amp;lt;/element&amp;gt;
     *           &amp;lt;/sequence&amp;gt;
     *           &amp;lt;element name="UniversalAddress"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;simpleContent&amp;gt;
     *                 &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
     *                   &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *                     &amp;lt;simpleType&amp;gt;
     *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                         &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *                       &amp;lt;/restriction&amp;gt;
     *                     &amp;lt;/simpleType&amp;gt;
     *                   &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;/extension&amp;gt;
     *               &amp;lt;/simpleContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *         &amp;lt;/choice&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}OntologyExtension" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "latitude",
        "longitude",
        "universalAddress",
        "ontologyExtension"
    })
    public static class Geocode {

        @XmlElement(name = "Latitude")
        protected String latitude;
        @XmlElement(name = "Longitude")
        protected String longitude;
        @XmlElement(name = "UniversalAddress")
        protected OntologyLocationType.Geocode.UniversalAddress universalAddress;
        @XmlElement(name = "OntologyExtension")
        protected OntologyExtensionType ontologyExtension;
        @XmlAttribute(name = "OntologyRefID")
        protected String ontologyRefID;

        /**
         * Obtiene el valor de la propiedad latitude.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLatitude() {
            return latitude;
        }

        /**
         * Define el valor de la propiedad latitude.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLatitude(String value) {
            this.latitude = value;
        }

        /**
         * Obtiene el valor de la propiedad longitude.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLongitude() {
            return longitude;
        }

        /**
         * Define el valor de la propiedad longitude.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLongitude(String value) {
            this.longitude = value;
        }

        /**
         * Obtiene el valor de la propiedad universalAddress.
         * 
         * @return
         *     possible object is
         *     {@link OntologyLocationType.Geocode.UniversalAddress }
         *     
         */
        public OntologyLocationType.Geocode.UniversalAddress getUniversalAddress() {
            return universalAddress;
        }

        /**
         * Define el valor de la propiedad universalAddress.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyLocationType.Geocode.UniversalAddress }
         *     
         */
        public void setUniversalAddress(OntologyLocationType.Geocode.UniversalAddress value) {
            this.universalAddress = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyExtension.
         * 
         * @return
         *     possible object is
         *     {@link OntologyExtensionType }
         *     
         */
        public OntologyExtensionType getOntologyExtension() {
            return ontologyExtension;
        }

        /**
         * Define el valor de la propiedad ontologyExtension.
         * 
         * @param value
         *     allowed object is
         *     {@link OntologyExtensionType }
         *     
         */
        public void setOntologyExtension(OntologyExtensionType value) {
            this.ontologyExtension = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyRefID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOntologyRefID() {
            return ontologyRefID;
        }

        /**
         * Define el valor de la propiedad ontologyRefID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOntologyRefID(String value) {
            this.ontologyRefID = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.w3.org/2001/XMLSchema&amp;gt;string"&amp;gt;
         *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class UniversalAddress {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "OntologyRefID")
            protected String ontologyRefID;

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Obtiene el valor de la propiedad ontologyRefID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOntologyRefID() {
                return ontologyRefID;
            }

            /**
             * Define el valor de la propiedad ontologyRefID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOntologyRefID(String value) {
                this.ontologyRefID = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OntologyAddressType"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PhysicalLocation
        extends OntologyAddressType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;simpleContent&amp;gt;
     *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_OfferLocationType"&amp;gt;
     *       &amp;lt;attribute name="OntologyRefID"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/simpleContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Type {

        @XmlValue
        protected ListOfferLocationType value;
        @XmlAttribute(name = "OntologyRefID")
        protected String ontologyRefID;

        /**
         * Source: Coverage Type (COV) OpenTravel codelist.
         * 
         * @return
         *     possible object is
         *     {@link ListOfferLocationType }
         *     
         */
        public ListOfferLocationType getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfferLocationType }
         *     
         */
        public void setValue(ListOfferLocationType value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad ontologyRefID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOntologyRefID() {
            return ontologyRefID;
        }

        /**
         * Define el valor de la propiedad ontologyRefID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOntologyRefID(String value) {
            this.ontologyRefID = value;
        }

    }

}
