
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines rail availability and schedule search criteria, including origin-destination information and train number and network code.
 * 
 * &lt;p&gt;Clase Java para RailAvailScheduleQueryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RailAvailScheduleQueryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="OriginDestinationInformation" type="{http://www.opentravel.org/OTA/2003/05}RailOriginDestinationInformationType"/&amp;gt;
 *         &amp;lt;element name="RailSearchCriteria" type="{http://www.opentravel.org/OTA/2003/05}TrainQueryType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="MaxConnections" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *       &amp;lt;attribute name="TrainTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailAvailScheduleQueryType", propOrder = {
    "originDestinationInformation",
    "railSearchCriteria"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTARailFareQuoteRQ.FareQuoteCriteria.class,
    org.opentravel.ota._2003._05.OTARailScheduleRQ.RailScheduleQuery.class
})
public class RailAvailScheduleQueryType {

    @XmlElement(name = "OriginDestinationInformation", required = true)
    protected RailOriginDestinationInformationType originDestinationInformation;
    @XmlElement(name = "RailSearchCriteria")
    protected List<TrainQueryType> railSearchCriteria;
    @XmlAttribute(name = "MaxConnections")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger maxConnections;
    @XmlAttribute(name = "TrainTypeCode")
    protected String trainTypeCode;

    /**
     * Obtiene el valor de la propiedad originDestinationInformation.
     * 
     * @return
     *     possible object is
     *     {@link RailOriginDestinationInformationType }
     *     
     */
    public RailOriginDestinationInformationType getOriginDestinationInformation() {
        return originDestinationInformation;
    }

    /**
     * Define el valor de la propiedad originDestinationInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link RailOriginDestinationInformationType }
     *     
     */
    public void setOriginDestinationInformation(RailOriginDestinationInformationType value) {
        this.originDestinationInformation = value;
    }

    /**
     * Gets the value of the railSearchCriteria property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the railSearchCriteria property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRailSearchCriteria().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TrainQueryType }
     * 
     * 
     */
    public List<TrainQueryType> getRailSearchCriteria() {
        if (railSearchCriteria == null) {
            railSearchCriteria = new ArrayList<TrainQueryType>();
        }
        return this.railSearchCriteria;
    }

    /**
     * Obtiene el valor de la propiedad maxConnections.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxConnections() {
        return maxConnections;
    }

    /**
     * Define el valor de la propiedad maxConnections.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxConnections(BigInteger value) {
        this.maxConnections = value;
    }

    /**
     * Obtiene el valor de la propiedad trainTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrainTypeCode() {
        return trainTypeCode;
    }

    /**
     * Define el valor de la propiedad trainTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrainTypeCode(String value) {
        this.trainTypeCode = value;
    }

}
