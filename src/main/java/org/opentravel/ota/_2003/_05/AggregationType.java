
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Aggregate information regarding reviews.
 * 
 * &lt;p&gt;Clase Java para AggregationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AggregationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Aggregation" maxOccurs="unbounded"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="TravelerType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="MonthYearOfTravel" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
 *                 &amp;lt;attribute name="QuarterOfTravel" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="AggregationRef" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                 &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                 &amp;lt;attribute name="RatingCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                 &amp;lt;attribute name="ReviewCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregationType", propOrder = {
    "aggregation"
})
public class AggregationType {

    @XmlElement(name = "Aggregation", required = true)
    protected List<AggregationType.Aggregation> aggregation;

    /**
     * Gets the value of the aggregation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the aggregation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAggregation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link AggregationType.Aggregation }
     * 
     * 
     */
    public List<AggregationType.Aggregation> getAggregation() {
        if (aggregation == null) {
            aggregation = new ArrayList<AggregationType.Aggregation>();
        }
        return this.aggregation;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="TravelerType" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="MonthYearOfTravel" type="{http://www.opentravel.org/OTA/2003/05}YearOrYearMonthType" /&amp;gt;
     *       &amp;lt;attribute name="QuarterOfTravel" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="AggregationRef" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *       &amp;lt;attribute name="AggregateRating" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *       &amp;lt;attribute name="RatingCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *       &amp;lt;attribute name="ReviewCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Aggregation {

        @XmlAttribute(name = "TravelerType")
        protected String travelerType;
        @XmlAttribute(name = "MonthYearOfTravel")
        protected String monthYearOfTravel;
        @XmlAttribute(name = "QuarterOfTravel")
        protected String quarterOfTravel;
        @XmlAttribute(name = "AggregationRef")
        protected String aggregationRef;
        @XmlAttribute(name = "AggregateRating")
        protected BigDecimal aggregateRating;
        @XmlAttribute(name = "RatingCount")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger ratingCount;
        @XmlAttribute(name = "ReviewCount")
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger reviewCount;

        /**
         * Obtiene el valor de la propiedad travelerType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelerType() {
            return travelerType;
        }

        /**
         * Define el valor de la propiedad travelerType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelerType(String value) {
            this.travelerType = value;
        }

        /**
         * Obtiene el valor de la propiedad monthYearOfTravel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMonthYearOfTravel() {
            return monthYearOfTravel;
        }

        /**
         * Define el valor de la propiedad monthYearOfTravel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMonthYearOfTravel(String value) {
            this.monthYearOfTravel = value;
        }

        /**
         * Obtiene el valor de la propiedad quarterOfTravel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuarterOfTravel() {
            return quarterOfTravel;
        }

        /**
         * Define el valor de la propiedad quarterOfTravel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuarterOfTravel(String value) {
            this.quarterOfTravel = value;
        }

        /**
         * Obtiene el valor de la propiedad aggregationRef.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAggregationRef() {
            return aggregationRef;
        }

        /**
         * Define el valor de la propiedad aggregationRef.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAggregationRef(String value) {
            this.aggregationRef = value;
        }

        /**
         * Obtiene el valor de la propiedad aggregateRating.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAggregateRating() {
            return aggregateRating;
        }

        /**
         * Define el valor de la propiedad aggregateRating.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAggregateRating(BigDecimal value) {
            this.aggregateRating = value;
        }

        /**
         * Obtiene el valor de la propiedad ratingCount.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRatingCount() {
            return ratingCount;
        }

        /**
         * Define el valor de la propiedad ratingCount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRatingCount(BigInteger value) {
            this.ratingCount = value;
        }

        /**
         * Obtiene el valor de la propiedad reviewCount.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getReviewCount() {
            return reviewCount;
        }

        /**
         * Define el valor de la propiedad reviewCount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setReviewCount(BigInteger value) {
            this.reviewCount = value;
        }

    }

}
