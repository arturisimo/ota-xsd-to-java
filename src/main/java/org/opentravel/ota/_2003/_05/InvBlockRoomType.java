
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to define the room types and all of their supporting data within a room block.
 * 
 * &lt;p&gt;Clase Java para InvBlockRoomType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="InvBlockRoomType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="RoomTypeAllocations" maxOccurs="10" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomTypeAllocation" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="RoomTypeAllocByGuest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="NumberOfGuests" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="NumberOfUnits" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                           &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
 *                           &amp;lt;attribute name="CompRoomQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                           &amp;lt;attribute name="CompRoomFactor" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                           &amp;lt;attribute name="EndDateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="SellLimit" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="ProcureBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="AllocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
 *                           &amp;lt;attribute name="AllowGeneralInvInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RoomTypePickUpStatus" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RatePlans" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RatePlan" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateUploadType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="MarketCode" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                     &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                     &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="MethodInfo" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MethodInfoGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="DaysOfWeeks" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="DaysOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DOW_RulesType" maxOccurs="unbounded"/&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PromotionCodeGroup"/&amp;gt;
 *                           &amp;lt;attribute name="RatePlanCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="BookingCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *                           &amp;lt;attribute name="UpgradeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="DaysOfWeeks" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="DaysOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DOW_RulesType" maxOccurs="unbounded"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *       &amp;lt;attribute name="RoomTypeCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvBlockRoomType", propOrder = {
    "roomTypeAllocations",
    "ratePlans",
    "daysOfWeeks"
})
public class InvBlockRoomType {

    @XmlElement(name = "RoomTypeAllocations")
    protected List<InvBlockRoomType.RoomTypeAllocations> roomTypeAllocations;
    @XmlElement(name = "RatePlans")
    protected InvBlockRoomType.RatePlans ratePlans;
    @XmlElement(name = "DaysOfWeeks")
    protected InvBlockRoomType.DaysOfWeeks daysOfWeeks;
    @XmlAttribute(name = "RoomTypeCode")
    protected String roomTypeCode;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;

    /**
     * Gets the value of the roomTypeAllocations property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomTypeAllocations property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getRoomTypeAllocations().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link InvBlockRoomType.RoomTypeAllocations }
     * 
     * 
     */
    public List<InvBlockRoomType.RoomTypeAllocations> getRoomTypeAllocations() {
        if (roomTypeAllocations == null) {
            roomTypeAllocations = new ArrayList<InvBlockRoomType.RoomTypeAllocations>();
        }
        return this.roomTypeAllocations;
    }

    /**
     * Obtiene el valor de la propiedad ratePlans.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockRoomType.RatePlans }
     *     
     */
    public InvBlockRoomType.RatePlans getRatePlans() {
        return ratePlans;
    }

    /**
     * Define el valor de la propiedad ratePlans.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockRoomType.RatePlans }
     *     
     */
    public void setRatePlans(InvBlockRoomType.RatePlans value) {
        this.ratePlans = value;
    }

    /**
     * Obtiene el valor de la propiedad daysOfWeeks.
     * 
     * @return
     *     possible object is
     *     {@link InvBlockRoomType.DaysOfWeeks }
     *     
     */
    public InvBlockRoomType.DaysOfWeeks getDaysOfWeeks() {
        return daysOfWeeks;
    }

    /**
     * Define el valor de la propiedad daysOfWeeks.
     * 
     * @param value
     *     allowed object is
     *     {@link InvBlockRoomType.DaysOfWeeks }
     *     
     */
    public void setDaysOfWeeks(InvBlockRoomType.DaysOfWeeks value) {
        this.daysOfWeeks = value;
    }

    /**
     * Obtiene el valor de la propiedad roomTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomTypeCode() {
        return roomTypeCode;
    }

    /**
     * Define el valor de la propiedad roomTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomTypeCode(String value) {
        this.roomTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="DaysOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DOW_RulesType" maxOccurs="unbounded"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "daysOfWeek"
    })
    public static class DaysOfWeeks {

        @XmlElement(name = "DaysOfWeek", required = true)
        protected List<DOWRulesType> daysOfWeek;

        /**
         * Gets the value of the daysOfWeek property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the daysOfWeek property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDaysOfWeek().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link DOWRulesType }
         * 
         * 
         */
        public List<DOWRulesType> getDaysOfWeek() {
            if (daysOfWeek == null) {
                daysOfWeek = new ArrayList<DOWRulesType>();
            }
            return this.daysOfWeek;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RatePlan" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateUploadType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="MarketCode" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                           &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                           &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="MethodInfo" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MethodInfoGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="DaysOfWeeks" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="DaysOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DOW_RulesType" maxOccurs="unbounded"/&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PromotionCodeGroup"/&amp;gt;
     *                 &amp;lt;attribute name="RatePlanCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="BookingCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="UpgradeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ratePlan"
    })
    public static class RatePlans {

        @XmlElement(name = "RatePlan", required = true)
        protected List<InvBlockRoomType.RatePlans.RatePlan> ratePlan;

        /**
         * Gets the value of the ratePlan property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the ratePlan property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRatePlan().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link InvBlockRoomType.RatePlans.RatePlan }
         * 
         * 
         */
        public List<InvBlockRoomType.RatePlans.RatePlan> getRatePlan() {
            if (ratePlan == null) {
                ratePlan = new ArrayList<InvBlockRoomType.RatePlans.RatePlan>();
            }
            return this.ratePlan;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RateUploadType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="MarketCode" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                 &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *                 &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Commission" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="MethodInfo" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MethodInfoGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="DaysOfWeeks" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="DaysOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DOW_RulesType" maxOccurs="unbounded"/&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PromotionCodeGroup"/&amp;gt;
         *       &amp;lt;attribute name="RatePlanCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="BookingCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="UpgradeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "marketCode",
            "commission",
            "methodInfo",
            "daysOfWeeks"
        })
        public static class RatePlan
            extends RateUploadType
        {

            @XmlElement(name = "MarketCode")
            protected List<InvBlockRoomType.RatePlans.RatePlan.MarketCode> marketCode;
            @XmlElement(name = "Commission")
            protected CommissionType commission;
            @XmlElement(name = "MethodInfo")
            protected List<InvBlockRoomType.RatePlans.RatePlan.MethodInfo> methodInfo;
            @XmlElement(name = "DaysOfWeeks")
            protected InvBlockRoomType.RatePlans.RatePlan.DaysOfWeeks daysOfWeeks;
            @XmlAttribute(name = "RatePlanCode")
            protected String ratePlanCode;
            @XmlAttribute(name = "BookingCode")
            protected String bookingCode;
            @XmlAttribute(name = "UpgradeIndicator")
            protected Boolean upgradeIndicator;
            @XmlAttribute(name = "PromotionCode")
            protected String promotionCode;
            @XmlAttribute(name = "PromotionVendorCode")
            protected List<String> promotionVendorCode;

            /**
             * Gets the value of the marketCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMarketCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link InvBlockRoomType.RatePlans.RatePlan.MarketCode }
             * 
             * 
             */
            public List<InvBlockRoomType.RatePlans.RatePlan.MarketCode> getMarketCode() {
                if (marketCode == null) {
                    marketCode = new ArrayList<InvBlockRoomType.RatePlans.RatePlan.MarketCode>();
                }
                return this.marketCode;
            }

            /**
             * Obtiene el valor de la propiedad commission.
             * 
             * @return
             *     possible object is
             *     {@link CommissionType }
             *     
             */
            public CommissionType getCommission() {
                return commission;
            }

            /**
             * Define el valor de la propiedad commission.
             * 
             * @param value
             *     allowed object is
             *     {@link CommissionType }
             *     
             */
            public void setCommission(CommissionType value) {
                this.commission = value;
            }

            /**
             * Gets the value of the methodInfo property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the methodInfo property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getMethodInfo().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link InvBlockRoomType.RatePlans.RatePlan.MethodInfo }
             * 
             * 
             */
            public List<InvBlockRoomType.RatePlans.RatePlan.MethodInfo> getMethodInfo() {
                if (methodInfo == null) {
                    methodInfo = new ArrayList<InvBlockRoomType.RatePlans.RatePlan.MethodInfo>();
                }
                return this.methodInfo;
            }

            /**
             * Obtiene el valor de la propiedad daysOfWeeks.
             * 
             * @return
             *     possible object is
             *     {@link InvBlockRoomType.RatePlans.RatePlan.DaysOfWeeks }
             *     
             */
            public InvBlockRoomType.RatePlans.RatePlan.DaysOfWeeks getDaysOfWeeks() {
                return daysOfWeeks;
            }

            /**
             * Define el valor de la propiedad daysOfWeeks.
             * 
             * @param value
             *     allowed object is
             *     {@link InvBlockRoomType.RatePlans.RatePlan.DaysOfWeeks }
             *     
             */
            public void setDaysOfWeeks(InvBlockRoomType.RatePlans.RatePlan.DaysOfWeeks value) {
                this.daysOfWeeks = value;
            }

            /**
             * Obtiene el valor de la propiedad ratePlanCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatePlanCode() {
                return ratePlanCode;
            }

            /**
             * Define el valor de la propiedad ratePlanCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatePlanCode(String value) {
                this.ratePlanCode = value;
            }

            /**
             * Obtiene el valor de la propiedad bookingCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBookingCode() {
                return bookingCode;
            }

            /**
             * Define el valor de la propiedad bookingCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBookingCode(String value) {
                this.bookingCode = value;
            }

            /**
             * Obtiene el valor de la propiedad upgradeIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isUpgradeIndicator() {
                return upgradeIndicator;
            }

            /**
             * Define el valor de la propiedad upgradeIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setUpgradeIndicator(Boolean value) {
                this.upgradeIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad promotionCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPromotionCode() {
                return promotionCode;
            }

            /**
             * Define el valor de la propiedad promotionCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPromotionCode(String value) {
                this.promotionCode = value;
            }

            /**
             * Gets the value of the promotionVendorCode property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotionVendorCode property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotionVendorCode().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getPromotionVendorCode() {
                if (promotionVendorCode == null) {
                    promotionVendorCode = new ArrayList<String>();
                }
                return this.promotionVendorCode;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="DaysOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DOW_RulesType" maxOccurs="unbounded"/&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "daysOfWeek"
            })
            public static class DaysOfWeeks {

                @XmlElement(name = "DaysOfWeek", required = true)
                protected List<DOWRulesType> daysOfWeek;

                /**
                 * Gets the value of the daysOfWeek property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the daysOfWeek property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getDaysOfWeek().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link DOWRulesType }
                 * 
                 * 
                 */
                public List<DOWRulesType> getDaysOfWeek() {
                    if (daysOfWeek == null) {
                        daysOfWeek = new ArrayList<DOWRulesType>();
                    }
                    return this.daysOfWeek;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="MarketCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *       &amp;lt;attribute name="MarketCodeName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *       &amp;lt;attribute name="CommissionableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="SourceOfBusiness" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MarketCode {

                @XmlAttribute(name = "MarketCode")
                protected String marketCode;
                @XmlAttribute(name = "MarketCodeName")
                protected String marketCodeName;
                @XmlAttribute(name = "CommissionableIndicator")
                protected Boolean commissionableIndicator;
                @XmlAttribute(name = "SourceOfBusiness")
                protected String sourceOfBusiness;

                /**
                 * Obtiene el valor de la propiedad marketCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMarketCode() {
                    return marketCode;
                }

                /**
                 * Define el valor de la propiedad marketCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMarketCode(String value) {
                    this.marketCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad marketCodeName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMarketCodeName() {
                    return marketCodeName;
                }

                /**
                 * Define el valor de la propiedad marketCodeName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMarketCodeName(String value) {
                    this.marketCodeName = value;
                }

                /**
                 * Obtiene el valor de la propiedad commissionableIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isCommissionableIndicator() {
                    return commissionableIndicator;
                }

                /**
                 * Define el valor de la propiedad commissionableIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setCommissionableIndicator(Boolean value) {
                    this.commissionableIndicator = value;
                }

                /**
                 * Obtiene el valor de la propiedad sourceOfBusiness.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSourceOfBusiness() {
                    return sourceOfBusiness;
                }

                /**
                 * Define el valor de la propiedad sourceOfBusiness.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSourceOfBusiness(String value) {
                    this.sourceOfBusiness = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MethodInfoGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class MethodInfo {

                @XmlAttribute(name = "BillingType")
                protected String billingType;
                @XmlAttribute(name = "SignFoodAndBev")
                protected Boolean signFoodAndBev;
                @XmlAttribute(name = "ReservationMethodCode")
                protected String reservationMethodCode;

                /**
                 * Obtiene el valor de la propiedad billingType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBillingType() {
                    return billingType;
                }

                /**
                 * Define el valor de la propiedad billingType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBillingType(String value) {
                    this.billingType = value;
                }

                /**
                 * Obtiene el valor de la propiedad signFoodAndBev.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isSignFoodAndBev() {
                    return signFoodAndBev;
                }

                /**
                 * Define el valor de la propiedad signFoodAndBev.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setSignFoodAndBev(Boolean value) {
                    this.signFoodAndBev = value;
                }

                /**
                 * Obtiene el valor de la propiedad reservationMethodCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReservationMethodCode() {
                    return reservationMethodCode;
                }

                /**
                 * Define el valor de la propiedad reservationMethodCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReservationMethodCode(String value) {
                    this.reservationMethodCode = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomTypeAllocation" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RoomTypeAllocByGuest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="NumberOfGuests" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                           &amp;lt;attribute name="NumberOfUnits" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *                 &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
     *                 &amp;lt;attribute name="CompRoomQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *                 &amp;lt;attribute name="CompRoomFactor" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *                 &amp;lt;attribute name="EndDateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="SellLimit" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="ProcureBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
     *                 &amp;lt;attribute name="AllocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
     *                 &amp;lt;attribute name="AllowGeneralInvInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RoomTypePickUpStatus" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomTypeAllocation"
    })
    public static class RoomTypeAllocations {

        @XmlElement(name = "RoomTypeAllocation", required = true)
        protected List<InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation> roomTypeAllocation;
        @XmlAttribute(name = "RoomTypePickUpStatus")
        protected String roomTypePickUpStatus;

        /**
         * Gets the value of the roomTypeAllocation property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomTypeAllocation property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomTypeAllocation().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation }
         * 
         * 
         */
        public List<InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation> getRoomTypeAllocation() {
            if (roomTypeAllocation == null) {
                roomTypeAllocation = new ArrayList<InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation>();
            }
            return this.roomTypeAllocation;
        }

        /**
         * Obtiene el valor de la propiedad roomTypePickUpStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRoomTypePickUpStatus() {
            return roomTypePickUpStatus;
        }

        /**
         * Define el valor de la propiedad roomTypePickUpStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRoomTypePickUpStatus(String value) {
            this.roomTypePickUpStatus = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RoomTypeAllocByGuest" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="NumberOfGuests" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                 &amp;lt;attribute name="NumberOfUnits" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
         *       &amp;lt;attribute name="NumberOfUnits" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
         *       &amp;lt;attribute name="CompRoomQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
         *       &amp;lt;attribute name="CompRoomFactor" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
         *       &amp;lt;attribute name="EndDateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="SellLimit" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="ProcureBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
         *       &amp;lt;attribute name="AllocationID" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to32" /&amp;gt;
         *       &amp;lt;attribute name="AllowGeneralInvInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomTypeAllocByGuest"
        })
        public static class RoomTypeAllocation {

            @XmlElement(name = "RoomTypeAllocByGuest")
            protected List<InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation.RoomTypeAllocByGuest> roomTypeAllocByGuest;
            @XmlAttribute(name = "NumberOfUnits")
            protected BigInteger numberOfUnits;
            @XmlAttribute(name = "CompRoomQuantity")
            protected Integer compRoomQuantity;
            @XmlAttribute(name = "CompRoomFactor")
            protected Integer compRoomFactor;
            @XmlAttribute(name = "EndDateIndicator")
            protected Boolean endDateIndicator;
            @XmlAttribute(name = "SellLimit")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger sellLimit;
            @XmlAttribute(name = "ProcureBlockCode")
            protected String procureBlockCode;
            @XmlAttribute(name = "AllocationID")
            protected String allocationID;
            @XmlAttribute(name = "AllowGeneralInvInd")
            protected Boolean allowGeneralInvInd;
            @XmlAttribute(name = "Start")
            protected String start;
            @XmlAttribute(name = "Duration")
            protected String duration;
            @XmlAttribute(name = "End")
            protected String end;

            /**
             * Gets the value of the roomTypeAllocByGuest property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomTypeAllocByGuest property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRoomTypeAllocByGuest().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation.RoomTypeAllocByGuest }
             * 
             * 
             */
            public List<InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation.RoomTypeAllocByGuest> getRoomTypeAllocByGuest() {
                if (roomTypeAllocByGuest == null) {
                    roomTypeAllocByGuest = new ArrayList<InvBlockRoomType.RoomTypeAllocations.RoomTypeAllocation.RoomTypeAllocByGuest>();
                }
                return this.roomTypeAllocByGuest;
            }

            /**
             * Obtiene el valor de la propiedad numberOfUnits.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNumberOfUnits() {
                return numberOfUnits;
            }

            /**
             * Define el valor de la propiedad numberOfUnits.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNumberOfUnits(BigInteger value) {
                this.numberOfUnits = value;
            }

            /**
             * Obtiene el valor de la propiedad compRoomQuantity.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getCompRoomQuantity() {
                return compRoomQuantity;
            }

            /**
             * Define el valor de la propiedad compRoomQuantity.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setCompRoomQuantity(Integer value) {
                this.compRoomQuantity = value;
            }

            /**
             * Obtiene el valor de la propiedad compRoomFactor.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getCompRoomFactor() {
                return compRoomFactor;
            }

            /**
             * Define el valor de la propiedad compRoomFactor.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setCompRoomFactor(Integer value) {
                this.compRoomFactor = value;
            }

            /**
             * Obtiene el valor de la propiedad endDateIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isEndDateIndicator() {
                return endDateIndicator;
            }

            /**
             * Define el valor de la propiedad endDateIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setEndDateIndicator(Boolean value) {
                this.endDateIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad sellLimit.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSellLimit() {
                return sellLimit;
            }

            /**
             * Define el valor de la propiedad sellLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSellLimit(BigInteger value) {
                this.sellLimit = value;
            }

            /**
             * Obtiene el valor de la propiedad procureBlockCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProcureBlockCode() {
                return procureBlockCode;
            }

            /**
             * Define el valor de la propiedad procureBlockCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProcureBlockCode(String value) {
                this.procureBlockCode = value;
            }

            /**
             * Obtiene el valor de la propiedad allocationID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAllocationID() {
                return allocationID;
            }

            /**
             * Define el valor de la propiedad allocationID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAllocationID(String value) {
                this.allocationID = value;
            }

            /**
             * Obtiene el valor de la propiedad allowGeneralInvInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAllowGeneralInvInd() {
                return allowGeneralInvInd;
            }

            /**
             * Define el valor de la propiedad allowGeneralInvInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAllowGeneralInvInd(Boolean value) {
                this.allowGeneralInvInd = value;
            }

            /**
             * Obtiene el valor de la propiedad start.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStart() {
                return start;
            }

            /**
             * Define el valor de la propiedad start.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStart(String value) {
                this.start = value;
            }

            /**
             * Obtiene el valor de la propiedad duration.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDuration() {
                return duration;
            }

            /**
             * Define el valor de la propiedad duration.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDuration(String value) {
                this.duration = value;
            }

            /**
             * Obtiene el valor de la propiedad end.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEnd() {
                return end;
            }

            /**
             * Define el valor de la propiedad end.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEnd(String value) {
                this.end = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="NumberOfGuests" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *       &amp;lt;attribute name="NumberOfUnits" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class RoomTypeAllocByGuest {

                @XmlAttribute(name = "NumberOfGuests", required = true)
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger numberOfGuests;
                @XmlAttribute(name = "NumberOfUnits", required = true)
                protected BigInteger numberOfUnits;

                /**
                 * Obtiene el valor de la propiedad numberOfGuests.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNumberOfGuests() {
                    return numberOfGuests;
                }

                /**
                 * Define el valor de la propiedad numberOfGuests.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNumberOfGuests(BigInteger value) {
                    this.numberOfGuests = value;
                }

                /**
                 * Obtiene el valor de la propiedad numberOfUnits.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNumberOfUnits() {
                    return numberOfUnits;
                }

                /**
                 * Define el valor de la propiedad numberOfUnits.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNumberOfUnits(BigInteger value) {
                    this.numberOfUnits = value;
                }

            }

        }

    }

}
