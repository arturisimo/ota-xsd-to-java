
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para DisplayOrderType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="DisplayOrderType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="ByDepartureTime"/&amp;gt;
 *     &amp;lt;enumeration value="ByArrivalTime"/&amp;gt;
 *     &amp;lt;enumeration value="ByJourneyTime"/&amp;gt;
 *     &amp;lt;enumeration value="ByPriceHighToLow"/&amp;gt;
 *     &amp;lt;enumeration value="ByPriceLowToHigh"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "DisplayOrderType")
@XmlEnum
public enum DisplayOrderType {


    /**
     * Display products by departure time
     * 
     */
    @XmlEnumValue("ByDepartureTime")
    BY_DEPARTURE_TIME("ByDepartureTime"),

    /**
     * Display products by arrival time
     * 
     */
    @XmlEnumValue("ByArrivalTime")
    BY_ARRIVAL_TIME("ByArrivalTime"),

    /**
     * Display products by journey time
     * 
     */
    @XmlEnumValue("ByJourneyTime")
    BY_JOURNEY_TIME("ByJourneyTime"),
    @XmlEnumValue("ByPriceHighToLow")
    BY_PRICE_HIGH_TO_LOW("ByPriceHighToLow"),
    @XmlEnumValue("ByPriceLowToHigh")
    BY_PRICE_LOW_TO_HIGH("ByPriceLowToHigh");
    private final String value;

    DisplayOrderType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DisplayOrderType fromValue(String v) {
        for (DisplayOrderType c: DisplayOrderType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
