
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type"/&amp;gt;
 *         &amp;lt;element name="ProcessingInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirProcessingInfoGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MultimodalOffer" type="{http://www.opentravel.org/OTA/2003/05}MultiModalOfferType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="OriginDestinationInformation" maxOccurs="99"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="SpecificFlightInfo" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType"&amp;gt;
 *                           &amp;lt;attribute name="PollingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="TravelPreferences" type="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="SpecificFlightInfo" type="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TravelPreferences" type="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TravelerInfoSummary" type="{http://www.opentravel.org/OTA/2003/05}TravelerInfoSummaryType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BookedFlightSegment" type="{http://www.opentravel.org/OTA/2003/05}BookFlightSegmentType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Offer" type="{http://www.opentravel.org/OTA/2003/05}AirOfferChoiceType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DirectAndStopsGroup"/&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}MaxResponsesGroup"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "processingInfo",
    "multimodalOffer",
    "originDestinationInformation",
    "specificFlightInfo",
    "travelPreferences",
    "travelerInfoSummary",
    "bookedFlightSegment",
    "offer"
})
@XmlRootElement(name = "OTA_AirAvailRQ")
public class OTAAirAvailRQ {

    @XmlElement(name = "POS", required = true)
    protected POSType pos;
    @XmlElement(name = "ProcessingInfo")
    protected OTAAirAvailRQ.ProcessingInfo processingInfo;
    @XmlElement(name = "MultimodalOffer")
    protected MultiModalOfferType multimodalOffer;
    @XmlElement(name = "OriginDestinationInformation", required = true)
    protected List<OTAAirAvailRQ.OriginDestinationInformation> originDestinationInformation;
    @XmlElement(name = "SpecificFlightInfo")
    protected SpecificFlightInfoType specificFlightInfo;
    @XmlElement(name = "TravelPreferences")
    protected AirSearchPrefsType travelPreferences;
    @XmlElement(name = "TravelerInfoSummary")
    protected TravelerInfoSummaryType travelerInfoSummary;
    @XmlElement(name = "BookedFlightSegment")
    protected List<BookFlightSegmentType> bookedFlightSegment;
    @XmlElement(name = "Offer")
    protected AirOfferChoiceType offer;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "DirectFlightsOnly")
    protected Boolean directFlightsOnly;
    @XmlAttribute(name = "NumberStops")
    protected Integer numberStops;
    @XmlAttribute(name = "MaxResponses")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxResponses;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAAirAvailRQ.ProcessingInfo }
     *     
     */
    public OTAAirAvailRQ.ProcessingInfo getProcessingInfo() {
        return processingInfo;
    }

    /**
     * Define el valor de la propiedad processingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAAirAvailRQ.ProcessingInfo }
     *     
     */
    public void setProcessingInfo(OTAAirAvailRQ.ProcessingInfo value) {
        this.processingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad multimodalOffer.
     * 
     * @return
     *     possible object is
     *     {@link MultiModalOfferType }
     *     
     */
    public MultiModalOfferType getMultimodalOffer() {
        return multimodalOffer;
    }

    /**
     * Define el valor de la propiedad multimodalOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiModalOfferType }
     *     
     */
    public void setMultimodalOffer(MultiModalOfferType value) {
        this.multimodalOffer = value;
    }

    /**
     * Gets the value of the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationInformation property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getOriginDestinationInformation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTAAirAvailRQ.OriginDestinationInformation }
     * 
     * 
     */
    public List<OTAAirAvailRQ.OriginDestinationInformation> getOriginDestinationInformation() {
        if (originDestinationInformation == null) {
            originDestinationInformation = new ArrayList<OTAAirAvailRQ.OriginDestinationInformation>();
        }
        return this.originDestinationInformation;
    }

    /**
     * Obtiene el valor de la propiedad specificFlightInfo.
     * 
     * @return
     *     possible object is
     *     {@link SpecificFlightInfoType }
     *     
     */
    public SpecificFlightInfoType getSpecificFlightInfo() {
        return specificFlightInfo;
    }

    /**
     * Define el valor de la propiedad specificFlightInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificFlightInfoType }
     *     
     */
    public void setSpecificFlightInfo(SpecificFlightInfoType value) {
        this.specificFlightInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad travelPreferences.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchPrefsType }
     *     
     */
    public AirSearchPrefsType getTravelPreferences() {
        return travelPreferences;
    }

    /**
     * Define el valor de la propiedad travelPreferences.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchPrefsType }
     *     
     */
    public void setTravelPreferences(AirSearchPrefsType value) {
        this.travelPreferences = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerInfoSummary.
     * 
     * @return
     *     possible object is
     *     {@link TravelerInfoSummaryType }
     *     
     */
    public TravelerInfoSummaryType getTravelerInfoSummary() {
        return travelerInfoSummary;
    }

    /**
     * Define el valor de la propiedad travelerInfoSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelerInfoSummaryType }
     *     
     */
    public void setTravelerInfoSummary(TravelerInfoSummaryType value) {
        this.travelerInfoSummary = value;
    }

    /**
     * Gets the value of the bookedFlightSegment property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookedFlightSegment property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getBookedFlightSegment().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link BookFlightSegmentType }
     * 
     * 
     */
    public List<BookFlightSegmentType> getBookedFlightSegment() {
        if (bookedFlightSegment == null) {
            bookedFlightSegment = new ArrayList<BookFlightSegmentType>();
        }
        return this.bookedFlightSegment;
    }

    /**
     * Obtiene el valor de la propiedad offer.
     * 
     * @return
     *     possible object is
     *     {@link AirOfferChoiceType }
     *     
     */
    public AirOfferChoiceType getOffer() {
        return offer;
    }

    /**
     * Define el valor de la propiedad offer.
     * 
     * @param value
     *     allowed object is
     *     {@link AirOfferChoiceType }
     *     
     */
    public void setOffer(AirOfferChoiceType value) {
        this.offer = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad directFlightsOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectFlightsOnly() {
        return directFlightsOnly;
    }

    /**
     * Define el valor de la propiedad directFlightsOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectFlightsOnly(Boolean value) {
        this.directFlightsOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad numberStops.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberStops() {
        return numberStops;
    }

    /**
     * Define el valor de la propiedad numberStops.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberStops(Integer value) {
        this.numberStops = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        return maxResponses;
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="SpecificFlightInfo" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType"&amp;gt;
     *                 &amp;lt;attribute name="PollingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="TravelPreferences" type="{http://www.opentravel.org/OTA/2003/05}AirSearchPrefsType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "specificFlightInfo",
        "travelPreferences"
    })
    public static class OriginDestinationInformation
        extends OriginDestinationInformationType
    {

        @XmlElement(name = "SpecificFlightInfo")
        protected OTAAirAvailRQ.OriginDestinationInformation.SpecificFlightInfo specificFlightInfo;
        @XmlElement(name = "TravelPreferences")
        protected AirSearchPrefsType travelPreferences;
        @XmlAttribute(name = "SameAirportInd")
        protected Boolean sameAirportInd;
        @XmlAttribute(name = "RPH")
        protected String rph;

        /**
         * Obtiene el valor de la propiedad specificFlightInfo.
         * 
         * @return
         *     possible object is
         *     {@link OTAAirAvailRQ.OriginDestinationInformation.SpecificFlightInfo }
         *     
         */
        public OTAAirAvailRQ.OriginDestinationInformation.SpecificFlightInfo getSpecificFlightInfo() {
            return specificFlightInfo;
        }

        /**
         * Define el valor de la propiedad specificFlightInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAAirAvailRQ.OriginDestinationInformation.SpecificFlightInfo }
         *     
         */
        public void setSpecificFlightInfo(OTAAirAvailRQ.OriginDestinationInformation.SpecificFlightInfo value) {
            this.specificFlightInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad travelPreferences.
         * 
         * @return
         *     possible object is
         *     {@link AirSearchPrefsType }
         *     
         */
        public AirSearchPrefsType getTravelPreferences() {
            return travelPreferences;
        }

        /**
         * Define el valor de la propiedad travelPreferences.
         * 
         * @param value
         *     allowed object is
         *     {@link AirSearchPrefsType }
         *     
         */
        public void setTravelPreferences(AirSearchPrefsType value) {
            this.travelPreferences = value;
        }

        /**
         * Obtiene el valor de la propiedad sameAirportInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSameAirportInd() {
            return sameAirportInd;
        }

        /**
         * Define el valor de la propiedad sameAirportInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSameAirportInd(Boolean value) {
            this.sameAirportInd = value;
        }

        /**
         * Obtiene el valor de la propiedad rph.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRPH() {
            return rph;
        }

        /**
         * Define el valor de la propiedad rph.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRPH(String value) {
            this.rph = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}SpecificFlightInfoType"&amp;gt;
         *       &amp;lt;attribute name="PollingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SpecificFlightInfo
            extends SpecificFlightInfoType
        {

            @XmlAttribute(name = "PollingIndicator")
            protected Boolean pollingIndicator;

            /**
             * Obtiene el valor de la propiedad pollingIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPollingIndicator() {
                return pollingIndicator;
            }

            /**
             * Define el valor de la propiedad pollingIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPollingIndicator(Boolean value) {
                this.pollingIndicator = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}AirProcessingInfoGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInfo {

        @XmlAttribute(name = "TargetSource")
        protected String targetSource;
        @XmlAttribute(name = "FlightSvcInfoIndicator")
        protected Boolean flightSvcInfoIndicator;
        @XmlAttribute(name = "DisplayOrder")
        protected DisplayOrderType displayOrder;
        @XmlAttribute(name = "ReducedDataIndicator")
        protected Boolean reducedDataIndicator;
        @XmlAttribute(name = "BaseFaresOnlyIndicator")
        protected Boolean baseFaresOnlyIndicator;
        @XmlAttribute(name = "SearchType")
        protected String searchType;
        @XmlAttribute(name = "AvailabilityIndicator")
        protected Boolean availabilityIndicator;

        /**
         * Obtiene el valor de la propiedad targetSource.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTargetSource() {
            return targetSource;
        }

        /**
         * Define el valor de la propiedad targetSource.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTargetSource(String value) {
            this.targetSource = value;
        }

        /**
         * Obtiene el valor de la propiedad flightSvcInfoIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFlightSvcInfoIndicator() {
            return flightSvcInfoIndicator;
        }

        /**
         * Define el valor de la propiedad flightSvcInfoIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFlightSvcInfoIndicator(Boolean value) {
            this.flightSvcInfoIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad displayOrder.
         * 
         * @return
         *     possible object is
         *     {@link DisplayOrderType }
         *     
         */
        public DisplayOrderType getDisplayOrder() {
            return displayOrder;
        }

        /**
         * Define el valor de la propiedad displayOrder.
         * 
         * @param value
         *     allowed object is
         *     {@link DisplayOrderType }
         *     
         */
        public void setDisplayOrder(DisplayOrderType value) {
            this.displayOrder = value;
        }

        /**
         * Obtiene el valor de la propiedad reducedDataIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReducedDataIndicator() {
            return reducedDataIndicator;
        }

        /**
         * Define el valor de la propiedad reducedDataIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReducedDataIndicator(Boolean value) {
            this.reducedDataIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad baseFaresOnlyIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isBaseFaresOnlyIndicator() {
            return baseFaresOnlyIndicator;
        }

        /**
         * Define el valor de la propiedad baseFaresOnlyIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setBaseFaresOnlyIndicator(Boolean value) {
            this.baseFaresOnlyIndicator = value;
        }

        /**
         * Obtiene el valor de la propiedad searchType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSearchType() {
            return searchType;
        }

        /**
         * Define el valor de la propiedad searchType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSearchType(String value) {
            this.searchType = value;
        }

        /**
         * Obtiene el valor de la propiedad availabilityIndicator.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAvailabilityIndicator() {
            return availabilityIndicator;
        }

        /**
         * Define el valor de la propiedad availabilityIndicator.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAvailabilityIndicator(Boolean value) {
            this.availabilityIndicator = value;
        }

    }

}
