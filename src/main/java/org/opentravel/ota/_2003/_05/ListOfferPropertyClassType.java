
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferPropertyClassType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferPropertyClassType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="ConferenceMeeting"/&amp;gt;
 *     &amp;lt;enumeration value="Corporate"/&amp;gt;
 *     &amp;lt;enumeration value="Budget/Economy/Hostel"/&amp;gt;
 *     &amp;lt;enumeration value="ExtendedStay"/&amp;gt;
 *     &amp;lt;enumeration value="Golf"/&amp;gt;
 *     &amp;lt;enumeration value="Luxury/Upscale/Resort"/&amp;gt;
 *     &amp;lt;enumeration value="Midscale"/&amp;gt;
 *     &amp;lt;enumeration value="Ski"/&amp;gt;
 *     &amp;lt;enumeration value="Spa"/&amp;gt;
 *     &amp;lt;enumeration value="VacationRental"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferPropertyClassType")
@XmlEnum
public enum ListOfferPropertyClassType {


    /**
     * A property with a combination of conference facilities and guest rooms.
     * 
     */
    @XmlEnumValue("ConferenceMeeting")
    CONFERENCE_MEETING("ConferenceMeeting"),

    /**
     * A property that accommodates corporate negotiated rates and/or is company policy mandated for traveling employees.
     * 
     */
    @XmlEnumValue("Corporate")
    CORPORATE("Corporate"),

    /**
     * A property with room rates that are lower than the area average.
     * 
     */
    @XmlEnumValue("Budget/Economy/Hostel")
    BUDGET_ECONOMY_HOSTEL("Budget/Economy/Hostel"),

    /**
     * A property that offers extended stay accommodations with typical reservations two weeks or greater in length.
     * 
     */
    @XmlEnumValue("ExtendedStay")
    EXTENDED_STAY("ExtendedStay"),

    /**
     * A property that has a golf facility onsite or nearby.
     * 
     */
    @XmlEnumValue("Golf")
    GOLF("Golf"),

    /**
     * A property with higher than average features, amenities, services and room rates.
     * 
     */
    @XmlEnumValue("Luxury/Upscale/Resort")
    LUXURY_UPSCALE_RESORT("Luxury/Upscale/Resort"),

    /**
     * A property with room rates that are average for the area.
     * 
     */
    @XmlEnumValue("Midscale")
    MIDSCALE("Midscale"),

    /**
     * A property that has a ski facility onsite or nearby.
     * 
     */
    @XmlEnumValue("Ski")
    SKI("Ski"),

    /**
     * A property that has a spa onsite or nearby.
     * 
     */
    @XmlEnumValue("Spa")
    SPA("Spa"),

    /**
     * A private house or timeshare property that typically accommodates vacationing travelers.
     * 
     */
    @XmlEnumValue("VacationRental")
    VACATION_RENTAL("VacationRental"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferPropertyClassType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferPropertyClassType fromValue(String v) {
        for (ListOfferPropertyClassType c: ListOfferPropertyClassType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
