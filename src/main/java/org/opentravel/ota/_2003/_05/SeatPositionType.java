
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para SeatPositionType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="SeatPositionType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="None"/&amp;gt;
 *     &amp;lt;enumeration value="Together"/&amp;gt;
 *     &amp;lt;enumeration value="Aisle"/&amp;gt;
 *     &amp;lt;enumeration value="Center"/&amp;gt;
 *     &amp;lt;enumeration value="Window"/&amp;gt;
 *     &amp;lt;enumeration value="Specific"/&amp;gt;
 *     &amp;lt;enumeration value="Exit"/&amp;gt;
 *     &amp;lt;enumeration value="Table"/&amp;gt;
 *     &amp;lt;enumeration value="AdjacentAisle"/&amp;gt;
 *     &amp;lt;enumeration value="Individual"/&amp;gt;
 *     &amp;lt;enumeration value="Middle"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "SeatPositionType")
@XmlEnum
public enum SeatPositionType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Together")
    TOGETHER("Together"),
    @XmlEnumValue("Aisle")
    AISLE("Aisle"),
    @XmlEnumValue("Center")
    CENTER("Center"),
    @XmlEnumValue("Window")
    WINDOW("Window"),
    @XmlEnumValue("Specific")
    SPECIFIC("Specific"),
    @XmlEnumValue("Exit")
    EXIT("Exit"),
    @XmlEnumValue("Table")
    TABLE("Table"),
    @XmlEnumValue("AdjacentAisle")
    ADJACENT_AISLE("AdjacentAisle"),
    @XmlEnumValue("Individual")
    INDIVIDUAL("Individual"),
    @XmlEnumValue("Middle")
    MIDDLE("Middle");
    private final String value;

    SeatPositionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeatPositionType fromValue(String v) {
        for (SeatPositionType c: SeatPositionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
