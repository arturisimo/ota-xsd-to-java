
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}POS_Type"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ProcessingInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="SummaryResultsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
 *                 &amp;lt;attribute name="MaxResultsQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Criteria" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;choice&amp;gt;
 *                     &amp;lt;element name="Location" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                     &amp;lt;element name="Proximity" maxOccurs="99" minOccurs="0"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;simpleContent&amp;gt;
 *                           &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;StringLength0to64"&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
 *                             &amp;lt;attribute name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
 *                             &amp;lt;attribute name="CityName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
 *                             &amp;lt;attribute name="StateProv" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
 *                             &amp;lt;attribute name="CountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
 *                             &amp;lt;attribute name="ResortName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                             &amp;lt;attribute name="RefPointType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                             &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                           &amp;lt;/extension&amp;gt;
 *                         &amp;lt;/simpleContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/choice&amp;gt;
 *                   &amp;lt;element name="Features" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="DrivingRangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="DiningInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="GolfProInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                           &amp;lt;attribute name="CourseDesigner" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Amenities" type="{http://www.opentravel.org/OTA/2003/05}GolfAmenitySummaryType" maxOccurs="99" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to32" /&amp;gt;
 *                 &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "processingInfo",
    "criteria",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_GolfCourseSearchRQ")
public class OTAGolfCourseSearchRQ {

    @XmlElement(name = "POS")
    protected OTAGolfCourseSearchRQ.POS pos;
    @XmlElement(name = "ProcessingInfo")
    protected OTAGolfCourseSearchRQ.ProcessingInfo processingInfo;
    @XmlElement(name = "Criteria")
    protected OTAGolfCourseSearchRQ.Criteria criteria;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link OTAGolfCourseSearchRQ.POS }
     *     
     */
    public OTAGolfCourseSearchRQ.POS getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAGolfCourseSearchRQ.POS }
     *     
     */
    public void setPOS(OTAGolfCourseSearchRQ.POS value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad processingInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTAGolfCourseSearchRQ.ProcessingInfo }
     *     
     */
    public OTAGolfCourseSearchRQ.ProcessingInfo getProcessingInfo() {
        return processingInfo;
    }

    /**
     * Define el valor de la propiedad processingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAGolfCourseSearchRQ.ProcessingInfo }
     *     
     */
    public void setProcessingInfo(OTAGolfCourseSearchRQ.ProcessingInfo value) {
        this.processingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad criteria.
     * 
     * @return
     *     possible object is
     *     {@link OTAGolfCourseSearchRQ.Criteria }
     *     
     */
    public OTAGolfCourseSearchRQ.Criteria getCriteria() {
        return criteria;
    }

    /**
     * Define el valor de la propiedad criteria.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAGolfCourseSearchRQ.Criteria }
     *     
     */
    public void setCriteria(OTAGolfCourseSearchRQ.Criteria value) {
        this.criteria = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;choice&amp;gt;
     *           &amp;lt;element name="Location" minOccurs="0"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;complexContent&amp;gt;
     *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
     *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
     *                 &amp;lt;/extension&amp;gt;
     *               &amp;lt;/complexContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *           &amp;lt;element name="Proximity" maxOccurs="99" minOccurs="0"&amp;gt;
     *             &amp;lt;complexType&amp;gt;
     *               &amp;lt;simpleContent&amp;gt;
     *                 &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;StringLength0to64"&amp;gt;
     *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
     *                   &amp;lt;attribute name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
     *                   &amp;lt;attribute name="CityName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
     *                   &amp;lt;attribute name="StateProv" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
     *                   &amp;lt;attribute name="CountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
     *                   &amp;lt;attribute name="ResortName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                   &amp;lt;attribute name="RefPointType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                   &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                 &amp;lt;/extension&amp;gt;
     *               &amp;lt;/simpleContent&amp;gt;
     *             &amp;lt;/complexType&amp;gt;
     *           &amp;lt;/element&amp;gt;
     *         &amp;lt;/choice&amp;gt;
     *         &amp;lt;element name="Features" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="DrivingRangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="DiningInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="GolfProInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                 &amp;lt;attribute name="CourseDesigner" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Amenities" type="{http://www.opentravel.org/OTA/2003/05}GolfAmenitySummaryType" maxOccurs="99" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="ID" type="{http://www.opentravel.org/OTA/2003/05}StringLength0to32" /&amp;gt;
     *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location",
        "proximity",
        "features",
        "amenities"
    })
    public static class Criteria {

        @XmlElement(name = "Location")
        protected OTAGolfCourseSearchRQ.Criteria.Location location;
        @XmlElement(name = "Proximity")
        protected List<OTAGolfCourseSearchRQ.Criteria.Proximity> proximity;
        @XmlElement(name = "Features")
        protected OTAGolfCourseSearchRQ.Criteria.Features features;
        @XmlElement(name = "Amenities")
        protected List<GolfAmenitySummaryType> amenities;
        @XmlAttribute(name = "ID")
        protected String id;
        @XmlAttribute(name = "Name")
        protected String name;

        /**
         * Obtiene el valor de la propiedad location.
         * 
         * @return
         *     possible object is
         *     {@link OTAGolfCourseSearchRQ.Criteria.Location }
         *     
         */
        public OTAGolfCourseSearchRQ.Criteria.Location getLocation() {
            return location;
        }

        /**
         * Define el valor de la propiedad location.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGolfCourseSearchRQ.Criteria.Location }
         *     
         */
        public void setLocation(OTAGolfCourseSearchRQ.Criteria.Location value) {
            this.location = value;
        }

        /**
         * Gets the value of the proximity property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the proximity property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getProximity().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAGolfCourseSearchRQ.Criteria.Proximity }
         * 
         * 
         */
        public List<OTAGolfCourseSearchRQ.Criteria.Proximity> getProximity() {
            if (proximity == null) {
                proximity = new ArrayList<OTAGolfCourseSearchRQ.Criteria.Proximity>();
            }
            return this.proximity;
        }

        /**
         * Obtiene el valor de la propiedad features.
         * 
         * @return
         *     possible object is
         *     {@link OTAGolfCourseSearchRQ.Criteria.Features }
         *     
         */
        public OTAGolfCourseSearchRQ.Criteria.Features getFeatures() {
            return features;
        }

        /**
         * Define el valor de la propiedad features.
         * 
         * @param value
         *     allowed object is
         *     {@link OTAGolfCourseSearchRQ.Criteria.Features }
         *     
         */
        public void setFeatures(OTAGolfCourseSearchRQ.Criteria.Features value) {
            this.features = value;
        }

        /**
         * Gets the value of the amenities property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the amenities property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAmenities().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link GolfAmenitySummaryType }
         * 
         * 
         */
        public List<GolfAmenitySummaryType> getAmenities() {
            if (amenities == null) {
                amenities = new ArrayList<GolfAmenitySummaryType>();
            }
            return this.amenities;
        }

        /**
         * Obtiene el valor de la propiedad id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getID() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setID(String value) {
            this.id = value;
        }

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="DrivingRangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="DiningInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="GolfProInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *       &amp;lt;attribute name="CourseDesigner" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Features {

            @XmlAttribute(name = "DrivingRangeInd")
            protected Boolean drivingRangeInd;
            @XmlAttribute(name = "DiningInd")
            protected Boolean diningInd;
            @XmlAttribute(name = "GolfProInd")
            protected Boolean golfProInd;
            @XmlAttribute(name = "CourseDesigner")
            protected String courseDesigner;

            /**
             * Obtiene el valor de la propiedad drivingRangeInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDrivingRangeInd() {
                return drivingRangeInd;
            }

            /**
             * Define el valor de la propiedad drivingRangeInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDrivingRangeInd(Boolean value) {
                this.drivingRangeInd = value;
            }

            /**
             * Obtiene el valor de la propiedad diningInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDiningInd() {
                return diningInd;
            }

            /**
             * Define el valor de la propiedad diningInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDiningInd(Boolean value) {
                this.diningInd = value;
            }

            /**
             * Obtiene el valor de la propiedad golfProInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isGolfProInd() {
                return golfProInd;
            }

            /**
             * Define el valor de la propiedad golfProInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setGolfProInd(Boolean value) {
                this.golfProInd = value;
            }

            /**
             * Obtiene el valor de la propiedad courseDesigner.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCourseDesigner() {
                return courseDesigner;
            }

            /**
             * Define el valor de la propiedad courseDesigner.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCourseDesigner(String value) {
                this.courseDesigner = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AddressType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}PositionGroup"/&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Location
            extends AddressType
        {

            @XmlAttribute(name = "Latitude")
            protected String latitude;
            @XmlAttribute(name = "Longitude")
            protected String longitude;
            @XmlAttribute(name = "Altitude")
            protected String altitude;
            @XmlAttribute(name = "AltitudeUnitOfMeasureCode")
            protected String altitudeUnitOfMeasureCode;
            @XmlAttribute(name = "PositionAccuracyCode")
            protected String positionAccuracyCode;

            /**
             * Obtiene el valor de la propiedad latitude.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLatitude() {
                return latitude;
            }

            /**
             * Define el valor de la propiedad latitude.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLatitude(String value) {
                this.latitude = value;
            }

            /**
             * Obtiene el valor de la propiedad longitude.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLongitude() {
                return longitude;
            }

            /**
             * Define el valor de la propiedad longitude.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLongitude(String value) {
                this.longitude = value;
            }

            /**
             * Obtiene el valor de la propiedad altitude.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAltitude() {
                return altitude;
            }

            /**
             * Define el valor de la propiedad altitude.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAltitude(String value) {
                this.altitude = value;
            }

            /**
             * Obtiene el valor de la propiedad altitudeUnitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAltitudeUnitOfMeasureCode() {
                return altitudeUnitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad altitudeUnitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAltitudeUnitOfMeasureCode(String value) {
                this.altitudeUnitOfMeasureCode = value;
            }

            /**
             * Obtiene el valor de la propiedad positionAccuracyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPositionAccuracyCode() {
                return positionAccuracyCode;
            }

            /**
             * Define el valor de la propiedad positionAccuracyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPositionAccuracyCode(String value) {
                this.positionAccuracyCode = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;simpleContent&amp;gt;
         *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;StringLength0to64"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DistanceAttributesGroup"/&amp;gt;
         *       &amp;lt;attribute name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&amp;gt;
         *       &amp;lt;attribute name="CityName" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to64" /&amp;gt;
         *       &amp;lt;attribute name="StateProv" type="{http://www.opentravel.org/OTA/2003/05}StateProvCodeType" /&amp;gt;
         *       &amp;lt;attribute name="CountryCode" type="{http://www.opentravel.org/OTA/2003/05}ISO3166" /&amp;gt;
         *       &amp;lt;attribute name="ResortName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="RefPointType" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *       &amp;lt;attribute name="Name" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/simpleContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Proximity {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "PostalCode")
            @XmlSchemaType(name = "anySimpleType")
            protected String postalCode;
            @XmlAttribute(name = "CityName")
            protected String cityName;
            @XmlAttribute(name = "StateProv")
            protected String stateProv;
            @XmlAttribute(name = "CountryCode")
            protected String countryCode;
            @XmlAttribute(name = "ResortName")
            protected String resortName;
            @XmlAttribute(name = "RefPointType")
            protected String refPointType;
            @XmlAttribute(name = "Name")
            protected String name;
            @XmlAttribute(name = "Distance")
            protected String distance;
            @XmlAttribute(name = "DistanceMeasure")
            protected String distanceMeasure;
            @XmlAttribute(name = "Direction")
            protected String direction;
            @XmlAttribute(name = "DistanceMax")
            protected String distanceMax;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;

            /**
             * Used for Character Strings, length 0 to 64.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Obtiene el valor de la propiedad postalCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostalCode() {
                return postalCode;
            }

            /**
             * Define el valor de la propiedad postalCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostalCode(String value) {
                this.postalCode = value;
            }

            /**
             * Obtiene el valor de la propiedad cityName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityName() {
                return cityName;
            }

            /**
             * Define el valor de la propiedad cityName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityName(String value) {
                this.cityName = value;
            }

            /**
             * Obtiene el valor de la propiedad stateProv.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStateProv() {
                return stateProv;
            }

            /**
             * Define el valor de la propiedad stateProv.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStateProv(String value) {
                this.stateProv = value;
            }

            /**
             * Obtiene el valor de la propiedad countryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Define el valor de la propiedad countryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad resortName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResortName() {
                return resortName;
            }

            /**
             * Define el valor de la propiedad resortName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResortName(String value) {
                this.resortName = value;
            }

            /**
             * Obtiene el valor de la propiedad refPointType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRefPointType() {
                return refPointType;
            }

            /**
             * Define el valor de la propiedad refPointType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRefPointType(String value) {
                this.refPointType = value;
            }

            /**
             * Obtiene el valor de la propiedad name.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Define el valor de la propiedad name.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Obtiene el valor de la propiedad distance.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDistance() {
                return distance;
            }

            /**
             * Define el valor de la propiedad distance.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDistance(String value) {
                this.distance = value;
            }

            /**
             * Obtiene el valor de la propiedad distanceMeasure.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDistanceMeasure() {
                return distanceMeasure;
            }

            /**
             * Define el valor de la propiedad distanceMeasure.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDistanceMeasure(String value) {
                this.distanceMeasure = value;
            }

            /**
             * Obtiene el valor de la propiedad direction.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDirection() {
                return direction;
            }

            /**
             * Define el valor de la propiedad direction.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDirection(String value) {
                this.direction = value;
            }

            /**
             * Obtiene el valor de la propiedad distanceMax.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDistanceMax() {
                return distanceMax;
            }

            /**
             * Define el valor de la propiedad distanceMax.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDistanceMax(String value) {
                this.distanceMax = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}POS_Type"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class POS
        extends POSType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="SummaryResultsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DisplayCurrency" type="{http://www.opentravel.org/OTA/2003/05}ISO4217" /&amp;gt;
     *       &amp;lt;attribute name="MaxResultsQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProcessingInfo {

        @XmlAttribute(name = "SummaryResultsInd")
        protected Boolean summaryResultsInd;
        @XmlAttribute(name = "DisplayCurrency")
        protected String displayCurrency;
        @XmlAttribute(name = "MaxResultsQty")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxResultsQty;

        /**
         * Obtiene el valor de la propiedad summaryResultsInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSummaryResultsInd() {
            return summaryResultsInd;
        }

        /**
         * Define el valor de la propiedad summaryResultsInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSummaryResultsInd(Boolean value) {
            this.summaryResultsInd = value;
        }

        /**
         * Obtiene el valor de la propiedad displayCurrency.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayCurrency() {
            return displayCurrency;
        }

        /**
         * Define el valor de la propiedad displayCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayCurrency(String value) {
            this.displayCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad maxResultsQty.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxResultsQty() {
            return maxResultsQty;
        }

        /**
         * Define el valor de la propiedad maxResultsQty.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxResultsQty(BigInteger value) {
            this.maxResultsQty = value;
        }

    }

}
