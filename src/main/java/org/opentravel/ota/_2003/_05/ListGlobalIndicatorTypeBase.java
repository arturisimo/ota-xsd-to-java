
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_GlobalIndicatorType_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_GlobalIndicatorType_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AP"/&amp;gt;
 *     &amp;lt;enumeration value="AT"/&amp;gt;
 *     &amp;lt;enumeration value="CT"/&amp;gt;
 *     &amp;lt;enumeration value="DO"/&amp;gt;
 *     &amp;lt;enumeration value="EH"/&amp;gt;
 *     &amp;lt;enumeration value="FE"/&amp;gt;
 *     &amp;lt;enumeration value="PA"/&amp;gt;
 *     &amp;lt;enumeration value="PN"/&amp;gt;
 *     &amp;lt;enumeration value="PO"/&amp;gt;
 *     &amp;lt;enumeration value="RU"/&amp;gt;
 *     &amp;lt;enumeration value="RW"/&amp;gt;
 *     &amp;lt;enumeration value="SA"/&amp;gt;
 *     &amp;lt;enumeration value="TS"/&amp;gt;
 *     &amp;lt;enumeration value="WH"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_GlobalIndicatorType_Base")
@XmlEnum
public enum ListGlobalIndicatorTypeBase {


    /**
     * Atlantic/Pacific Round-the-World
     * 
     */
    AP("AP"),

    /**
     * Atlantic Ocean
     * 
     */
    AT("AT"),

    /**
     * Circle trip
     * 
     */
    CT("CT"),

    /**
     * Domestic
     * 
     */
    DO("DO"),

    /**
     * Eastern Hemisphere
     * 
     */
    EH("EH"),

    /**
     * Within the Far East
     * 
     */
    FE("FE"),

    /**
     * Pacific Ocean
     * 
     */
    PA("PA"),

    /**
     * TC1-TC3 via Pacific/N. America
     * 
     */
    PN("PN"),

    /**
     * Polar Route
     * 
     */
    PO("PO"),

    /**
     * Russia Area 3
     * 
     */
    RU("RU"),

    /**
     * Round the world
     * 
     */
    RW("RW"),

    /**
     * South Atlantic only
     * 
     */
    SA("SA"),

    /**
     * Trans Siberia Route
     * 
     */
    TS("TS"),

    /**
     * Western Hemisphere
     * 
     */
    WH("WH"),

    /**
     * Use: Select this enumeration to exchange a value that is not in the enumerated list by entering the value information in the Code Extension fields.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListGlobalIndicatorTypeBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListGlobalIndicatorTypeBase fromValue(String v) {
        for (ListGlobalIndicatorTypeBase c: ListGlobalIndicatorTypeBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
