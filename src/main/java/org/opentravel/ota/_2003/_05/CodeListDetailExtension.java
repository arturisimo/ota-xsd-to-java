
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Detail Code List extension structure that includes all Summary information plus quantity, proximity, charge, schedule and apply to level information.
 * 
 * &lt;p&gt;Clase Java para CodeListDetailExtension complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CodeListDetailExtension"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}CodeListSummaryExtension"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ApplyToLevel" type="{http://www.opentravel.org/OTA/2003/05}List_ApplyToLevel" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Charge" type="{http://www.opentravel.org/OTA/2003/05}CodeListFeeType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Schedule" type="{http://www.opentravel.org/OTA/2003/05}CodeListScheduleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="Quantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="Proximity"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="Nearby"/&amp;gt;
 *             &amp;lt;enumeration value="NotSpecified"/&amp;gt;
 *             &amp;lt;enumeration value="Offsite"/&amp;gt;
 *             &amp;lt;enumeration value="Onsite"/&amp;gt;
 *             &amp;lt;enumeration value="OnsiteAndOffsite"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodeListDetailExtension", propOrder = {
    "applyToLevel",
    "charge",
    "schedule"
})
public class CodeListDetailExtension
    extends CodeListSummaryExtension
{

    @XmlElement(name = "ApplyToLevel")
    protected List<ListApplyToLevel> applyToLevel;
    @XmlElement(name = "Charge")
    protected List<CodeListFeeType> charge;
    @XmlElement(name = "Schedule")
    protected List<CodeListScheduleType> schedule;
    @XmlAttribute(name = "Quantity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger quantity;
    @XmlAttribute(name = "Proximity")
    protected String proximity;

    /**
     * Gets the value of the applyToLevel property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the applyToLevel property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getApplyToLevel().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ListApplyToLevel }
     * 
     * 
     */
    public List<ListApplyToLevel> getApplyToLevel() {
        if (applyToLevel == null) {
            applyToLevel = new ArrayList<ListApplyToLevel>();
        }
        return this.applyToLevel;
    }

    /**
     * Gets the value of the charge property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the charge property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getCharge().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CodeListFeeType }
     * 
     * 
     */
    public List<CodeListFeeType> getCharge() {
        if (charge == null) {
            charge = new ArrayList<CodeListFeeType>();
        }
        return this.charge;
    }

    /**
     * Gets the value of the schedule property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the schedule property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSchedule().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CodeListScheduleType }
     * 
     * 
     */
    public List<CodeListScheduleType> getSchedule() {
        if (schedule == null) {
            schedule = new ArrayList<CodeListScheduleType>();
        }
        return this.schedule;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantity(BigInteger value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad proximity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProximity() {
        return proximity;
    }

    /**
     * Define el valor de la propiedad proximity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProximity(String value) {
        this.proximity = value;
    }

}
