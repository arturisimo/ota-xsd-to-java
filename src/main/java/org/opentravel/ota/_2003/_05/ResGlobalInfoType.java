
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * ResGlobalInfo is a container for various information that affects the Reservation as a whole. These include global comments, counts, reservation IDs, loyalty programs, and payment methods.
 * 
 * &lt;p&gt;Clase Java para ResGlobalInfoType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ResGlobalInfoType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ResCommonDetailType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HotelReservationIDs" type="{http://www.opentravel.org/OTA/2003/05}HotelReservationIDsType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RoutingHops" type="{http://www.opentravel.org/OTA/2003/05}RoutingHopType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Profiles" type="{http://www.opentravel.org/OTA/2003/05}ProfilesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="BookingRules" type="{http://www.opentravel.org/OTA/2003/05}BookingRulesType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TotalCommissions" type="{http://www.opentravel.org/OTA/2003/05}CommissionType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResGlobalInfoType", propOrder = {
    "hotelReservationIDs",
    "routingHops",
    "profiles",
    "bookingRules",
    "totalCommissions"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.HotelReservationType.ResGlobalInfo.class,
    DynamicPkgGlobalInfoType.class
})
public class ResGlobalInfoType
    extends ResCommonDetailType
{

    @XmlElement(name = "HotelReservationIDs")
    protected HotelReservationIDsType hotelReservationIDs;
    @XmlElement(name = "RoutingHops")
    protected RoutingHopType routingHops;
    @XmlElement(name = "Profiles")
    protected ProfilesType profiles;
    @XmlElement(name = "BookingRules")
    protected BookingRulesType bookingRules;
    @XmlElement(name = "TotalCommissions")
    protected CommissionType totalCommissions;

    /**
     * Obtiene el valor de la propiedad hotelReservationIDs.
     * 
     * @return
     *     possible object is
     *     {@link HotelReservationIDsType }
     *     
     */
    public HotelReservationIDsType getHotelReservationIDs() {
        return hotelReservationIDs;
    }

    /**
     * Define el valor de la propiedad hotelReservationIDs.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelReservationIDsType }
     *     
     */
    public void setHotelReservationIDs(HotelReservationIDsType value) {
        this.hotelReservationIDs = value;
    }

    /**
     * Obtiene el valor de la propiedad routingHops.
     * 
     * @return
     *     possible object is
     *     {@link RoutingHopType }
     *     
     */
    public RoutingHopType getRoutingHops() {
        return routingHops;
    }

    /**
     * Define el valor de la propiedad routingHops.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingHopType }
     *     
     */
    public void setRoutingHops(RoutingHopType value) {
        this.routingHops = value;
    }

    /**
     * Obtiene el valor de la propiedad profiles.
     * 
     * @return
     *     possible object is
     *     {@link ProfilesType }
     *     
     */
    public ProfilesType getProfiles() {
        return profiles;
    }

    /**
     * Define el valor de la propiedad profiles.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfilesType }
     *     
     */
    public void setProfiles(ProfilesType value) {
        this.profiles = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingRules.
     * 
     * @return
     *     possible object is
     *     {@link BookingRulesType }
     *     
     */
    public BookingRulesType getBookingRules() {
        return bookingRules;
    }

    /**
     * Define el valor de la propiedad bookingRules.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingRulesType }
     *     
     */
    public void setBookingRules(BookingRulesType value) {
        this.bookingRules = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCommissions.
     * 
     * @return
     *     possible object is
     *     {@link CommissionType }
     *     
     */
    public CommissionType getTotalCommissions() {
        return totalCommissions;
    }

    /**
     * Define el valor de la propiedad totalCommissions.
     * 
     * @param value
     *     allowed object is
     *     {@link CommissionType }
     *     
     */
    public void setTotalCommissions(CommissionType value) {
        this.totalCommissions = value;
    }

}
