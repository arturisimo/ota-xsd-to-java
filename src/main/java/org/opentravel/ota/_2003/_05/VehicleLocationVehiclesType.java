
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * The Vehicle Location Vehicles Type is used to define information on the vehicles that are offered for rental at this facility.
 * 
 * &lt;p&gt;Clase Java para VehicleLocationVehiclesType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleLocationVehiclesType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="VehicleInfos" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="VehicleInfo" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
 *                           &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailVehicleInfoType" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Vehicle" maxOccurs="99" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Text" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="IsConfirmableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="DistanceUnit" type="{http://www.opentravel.org/OTA/2003/05}DistanceUnitNameType" /&amp;gt;
 *                 &amp;lt;attribute name="DistancePerFuelUnit" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleLocationVehiclesType", propOrder = {
    "vehicleInfos",
    "vehicle"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAVehLocDetailsNotifRQ.LocationDetails.LocationDetail.Vehicles.class
})
public class VehicleLocationVehiclesType {

    @XmlElement(name = "VehicleInfos")
    protected VehicleLocationVehiclesType.VehicleInfos vehicleInfos;
    @XmlElement(name = "Vehicle")
    protected List<VehicleLocationVehiclesType.Vehicle> vehicle;

    /**
     * Obtiene el valor de la propiedad vehicleInfos.
     * 
     * @return
     *     possible object is
     *     {@link VehicleLocationVehiclesType.VehicleInfos }
     *     
     */
    public VehicleLocationVehiclesType.VehicleInfos getVehicleInfos() {
        return vehicleInfos;
    }

    /**
     * Define el valor de la propiedad vehicleInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleLocationVehiclesType.VehicleInfos }
     *     
     */
    public void setVehicleInfos(VehicleLocationVehiclesType.VehicleInfos value) {
        this.vehicleInfos = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicle property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getVehicle().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleLocationVehiclesType.Vehicle }
     * 
     * 
     */
    public List<VehicleLocationVehiclesType.Vehicle> getVehicle() {
        if (vehicle == null) {
            vehicle = new ArrayList<VehicleLocationVehiclesType.Vehicle>();
        }
        return this.vehicle;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}VehicleType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Text" type="{http://www.opentravel.org/OTA/2003/05}FormattedTextType" maxOccurs="5" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="IsConfirmableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="DistanceUnit" type="{http://www.opentravel.org/OTA/2003/05}DistanceUnitNameType" /&amp;gt;
     *       &amp;lt;attribute name="DistancePerFuelUnit" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to99" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "text"
    })
    public static class Vehicle
        extends VehicleType
    {

        @XmlElement(name = "Text")
        protected List<FormattedTextType> text;
        @XmlAttribute(name = "IsConfirmableInd")
        protected Boolean isConfirmableInd;
        @XmlAttribute(name = "DistanceUnit")
        protected DistanceUnitNameType distanceUnit;
        @XmlAttribute(name = "DistancePerFuelUnit")
        protected Integer distancePerFuelUnit;

        /**
         * Gets the value of the text property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the text property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getText().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link FormattedTextType }
         * 
         * 
         */
        public List<FormattedTextType> getText() {
            if (text == null) {
                text = new ArrayList<FormattedTextType>();
            }
            return this.text;
        }

        /**
         * Obtiene el valor de la propiedad isConfirmableInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsConfirmableInd() {
            return isConfirmableInd;
        }

        /**
         * Define el valor de la propiedad isConfirmableInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsConfirmableInd(Boolean value) {
            this.isConfirmableInd = value;
        }

        /**
         * Obtiene el valor de la propiedad distanceUnit.
         * 
         * @return
         *     possible object is
         *     {@link DistanceUnitNameType }
         *     
         */
        public DistanceUnitNameType getDistanceUnit() {
            return distanceUnit;
        }

        /**
         * Define el valor de la propiedad distanceUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link DistanceUnitNameType }
         *     
         */
        public void setDistanceUnit(DistanceUnitNameType value) {
            this.distanceUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad distancePerFuelUnit.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getDistancePerFuelUnit() {
            return distancePerFuelUnit;
        }

        /**
         * Define el valor de la propiedad distancePerFuelUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setDistancePerFuelUnit(Integer value) {
            this.distancePerFuelUnit = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VehicleInfo" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
     *                 &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailVehicleInfoType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleInfo"
    })
    public static class VehicleInfos {

        @XmlElement(name = "VehicleInfo", required = true)
        protected List<VehicleLocationVehiclesType.VehicleInfos.VehicleInfo> vehicleInfo;

        /**
         * Gets the value of the vehicleInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehicleInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVehicleInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleLocationVehiclesType.VehicleInfos.VehicleInfo }
         * 
         * 
         */
        public List<VehicleLocationVehiclesType.VehicleInfos.VehicleInfo> getVehicleInfo() {
            if (vehicleInfo == null) {
                vehicleInfo = new ArrayList<VehicleLocationVehiclesType.VehicleInfos.VehicleInfo>();
            }
            return this.vehicleInfo;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FormattedTextType"&amp;gt;
         *       &amp;lt;attribute name="Type" use="required" type="{http://www.opentravel.org/OTA/2003/05}LocationDetailVehicleInfoType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class VehicleInfo
            extends FormattedTextType
        {

            @XmlAttribute(name = "Type", required = true)
            protected LocationDetailVehicleInfoType type;

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link LocationDetailVehicleInfoType }
             *     
             */
            public LocationDetailVehicleInfoType getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationDetailVehicleInfoType }
             *     
             */
            public void setType(LocationDetailVehicleInfoType value) {
                this.type = value;
            }

        }

    }

}
