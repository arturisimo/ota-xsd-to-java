
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para MealType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="MealType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="AVML"/&amp;gt;
 *     &amp;lt;enumeration value="BBML"/&amp;gt;
 *     &amp;lt;enumeration value="BLML"/&amp;gt;
 *     &amp;lt;enumeration value="CHML"/&amp;gt;
 *     &amp;lt;enumeration value="DBML"/&amp;gt;
 *     &amp;lt;enumeration value="FPML"/&amp;gt;
 *     &amp;lt;enumeration value="GFML"/&amp;gt;
 *     &amp;lt;enumeration value="HFML"/&amp;gt;
 *     &amp;lt;enumeration value="HNML"/&amp;gt;
 *     &amp;lt;enumeration value="KSML"/&amp;gt;
 *     &amp;lt;enumeration value="LCML"/&amp;gt;
 *     &amp;lt;enumeration value="LFML"/&amp;gt;
 *     &amp;lt;enumeration value="LPML"/&amp;gt;
 *     &amp;lt;enumeration value="LSML"/&amp;gt;
 *     &amp;lt;enumeration value="MOML"/&amp;gt;
 *     &amp;lt;enumeration value="NLML"/&amp;gt;
 *     &amp;lt;enumeration value="ORML"/&amp;gt;
 *     &amp;lt;enumeration value="PRML"/&amp;gt;
 *     &amp;lt;enumeration value="RVML"/&amp;gt;
 *     &amp;lt;enumeration value="SFML"/&amp;gt;
 *     &amp;lt;enumeration value="SPML"/&amp;gt;
 *     &amp;lt;enumeration value="VGML"/&amp;gt;
 *     &amp;lt;enumeration value="VLML"/&amp;gt;
 *     &amp;lt;enumeration value="RGML"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "MealType")
@XmlEnum
public enum MealType {


    /**
     * AVML - Asian Veg
     * 
     */
    AVML,

    /**
     * BBML - Baby/Infant Food
     * 
     */
    BBML,

    /**
     * BLML - Bland Meal
     * 
     */
    BLML,

    /**
     * CHML - Child Meal
     * 
     */
    CHML,

    /**
     * DBML - Diabetic
     * 
     */
    DBML,

    /**
     * FPML - Fruit Meal
     * 
     */
    FPML,

    /**
     * GFML - Gluten Free
     * 
     */
    GFML,

    /**
     * HFML - High Fiber
     * 
     */
    HFML,

    /**
     * HNML - Hindu Meal
     * 
     */
    HNML,

    /**
     * KSML - Kosher
     * 
     */
    KSML,

    /**
     * LCML - Low Calorie
     * 
     */
    LCML,

    /**
     * LFML - Low Cholesterol
     * 
     */
    LFML,

    /**
     * LPML - Low Protein
     * 
     */
    LPML,

    /**
     * LSML - Low Sodium/No Salt
     * 
     */
    LSML,

    /**
     * MOML - Moslem
     * 
     */
    MOML,

    /**
     * NLML - Non-Lactose
     * 
     */
    NLML,

    /**
     * ORML - Oriental
     * 
     */
    ORML,

    /**
     * PRML - Low Purin
     * 
     */
    PRML,

    /**
     * RVML - Raw Vegetarian
     * 
     */
    RVML,

    /**
     * SFML - Seafood
     * 
     */
    SFML,

    /**
     * SPML - Special/Specify
     * 
     */
    SPML,

    /**
     * VGML - Vegetarian/Non Dairy
     * 
     */
    VGML,

    /**
     * VLML - Vegetarian/Milk/Eggs
     * 
     */
    VLML,

    /**
     * Designates a regular meal.
     * 
     */
    RGML;

    public String value() {
        return name();
    }

    public static MealType fromValue(String v) {
        return valueOf(v);
    }

}
