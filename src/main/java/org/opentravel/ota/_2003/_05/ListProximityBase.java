
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_Proximity_Base.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_Proximity_Base"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Nearby"/&amp;gt;
 *     &amp;lt;enumeration value="NotSpecified"/&amp;gt;
 *     &amp;lt;enumeration value="Offsite"/&amp;gt;
 *     &amp;lt;enumeration value="Onsite"/&amp;gt;
 *     &amp;lt;enumeration value="OnsiteAndOffsite"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_Proximity_Base")
@XmlEnum
public enum ListProximityBase {

    @XmlEnumValue("Nearby")
    NEARBY("Nearby"),
    @XmlEnumValue("NotSpecified")
    NOT_SPECIFIED("NotSpecified"),
    @XmlEnumValue("Offsite")
    OFFSITE("Offsite"),
    @XmlEnumValue("Onsite")
    ONSITE("Onsite"),
    @XmlEnumValue("OnsiteAndOffsite")
    ONSITE_AND_OFFSITE("OnsiteAndOffsite");
    private final String value;

    ListProximityBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListProximityBase fromValue(String v) {
        for (ListProximityBase c: ListProximityBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
