
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Form of payment and entire itinerary pricing.
 * 
 * &lt;p&gt;Clase Java para TravelCostType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TravelCostType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FormOfPayment" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" maxOccurs="unbounded"/&amp;gt;
 *         &amp;lt;element name="CostTotals" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TotalType"&amp;gt;
 *                 &amp;lt;attribute name="FormOfPaymentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelCostType", propOrder = {
    "formOfPayment",
    "costTotals",
    "tpaExtensions"
})
public class TravelCostType {

    @XmlElement(name = "FormOfPayment", required = true)
    protected List<PaymentFormType> formOfPayment;
    @XmlElement(name = "CostTotals")
    protected TravelCostType.CostTotals costTotals;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;

    /**
     * Gets the value of the formOfPayment property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the formOfPayment property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getFormOfPayment().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentFormType }
     * 
     * 
     */
    public List<PaymentFormType> getFormOfPayment() {
        if (formOfPayment == null) {
            formOfPayment = new ArrayList<PaymentFormType>();
        }
        return this.formOfPayment;
    }

    /**
     * Obtiene el valor de la propiedad costTotals.
     * 
     * @return
     *     possible object is
     *     {@link TravelCostType.CostTotals }
     *     
     */
    public TravelCostType.CostTotals getCostTotals() {
        return costTotals;
    }

    /**
     * Define el valor de la propiedad costTotals.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelCostType.CostTotals }
     *     
     */
    public void setCostTotals(TravelCostType.CostTotals value) {
        this.costTotals = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TotalType"&amp;gt;
     *       &amp;lt;attribute name="FormOfPaymentRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class CostTotals
        extends TotalType
    {

        @XmlAttribute(name = "FormOfPaymentRPH")
        protected String formOfPaymentRPH;

        /**
         * Obtiene el valor de la propiedad formOfPaymentRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFormOfPaymentRPH() {
            return formOfPaymentRPH;
        }

        /**
         * Define el valor de la propiedad formOfPaymentRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFormOfPaymentRPH(String value) {
            this.formOfPaymentRPH = value;
        }

    }

}
