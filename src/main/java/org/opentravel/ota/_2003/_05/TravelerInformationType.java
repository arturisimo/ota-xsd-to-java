
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specify passenger numbers and types.
 * 
 * &lt;p&gt;Clase Java para TravelerInformationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="TravelerInformationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="PassengerTypeQuantity" type="{http://www.opentravel.org/OTA/2003/05}PassengerTypeQuantityType" maxOccurs="10"/&amp;gt;
 *         &amp;lt;element name="AirTraveler" type="{http://www.opentravel.org/OTA/2003/05}AirTravelerType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelerInformationType", propOrder = {
    "passengerTypeQuantity",
    "airTraveler"
})
public class TravelerInformationType {

    @XmlElement(name = "PassengerTypeQuantity", required = true)
    protected List<PassengerTypeQuantityType> passengerTypeQuantity;
    @XmlElement(name = "AirTraveler")
    protected AirTravelerType airTraveler;

    /**
     * Gets the value of the passengerTypeQuantity property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerTypeQuantity property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPassengerTypeQuantity().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link PassengerTypeQuantityType }
     * 
     * 
     */
    public List<PassengerTypeQuantityType> getPassengerTypeQuantity() {
        if (passengerTypeQuantity == null) {
            passengerTypeQuantity = new ArrayList<PassengerTypeQuantityType>();
        }
        return this.passengerTypeQuantity;
    }

    /**
     * Obtiene el valor de la propiedad airTraveler.
     * 
     * @return
     *     possible object is
     *     {@link AirTravelerType }
     *     
     */
    public AirTravelerType getAirTraveler() {
        return airTraveler;
    }

    /**
     * Define el valor de la propiedad airTraveler.
     * 
     * @param value
     *     allowed object is
     *     {@link AirTravelerType }
     *     
     */
    public void setAirTraveler(AirTravelerType value) {
        this.airTraveler = value;
    }

}
