
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Provides the details of a rooming list.
 * 
 * &lt;p&gt;Clase Java para HotelRoomListType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="HotelRoomListType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Guests" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Guest" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="Loyalty" maxOccurs="5" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="GuaranteePayment" maxOccurs="2" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelPaymentFormType"&amp;gt;
 *                                     &amp;lt;attribute name="DetailType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;enumeration value="Payment"/&amp;gt;
 *                                           &amp;lt;enumeration value="Guarantee"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="GuaranteeType"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
 *                                           &amp;lt;enumeration value="GuaranteeRequired"/&amp;gt;
 *                                           &amp;lt;enumeration value="None"/&amp;gt;
 *                                           &amp;lt;enumeration value="CC/DC/Voucher"/&amp;gt;
 *                                           &amp;lt;enumeration value="Profile"/&amp;gt;
 *                                           &amp;lt;enumeration value="Deposit"/&amp;gt;
 *                                           &amp;lt;enumeration value="PrePay"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="AdditionalDetails" type="{http://www.opentravel.org/OTA/2003/05}AdditionalDetailsType" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="GuestAction" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                           &amp;lt;attribute name="PrintConfoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MasterContact" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="UniqueIDs" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Loyalty" maxOccurs="5" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="MasterAccount" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DirectBillType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BillingType"/&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="RoomStays" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
 *                           &amp;lt;sequence minOccurs="0"&amp;gt;
 *                             &amp;lt;element name="HotelReservationIDs" type="{http://www.opentravel.org/OTA/2003/05}HotelReservationIDsType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="RoomShares" type="{http://www.opentravel.org/OTA/2003/05}RoomSharesType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                             &amp;lt;choice minOccurs="0"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *                                 &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *                             &amp;lt;/choice&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="RoomStay" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Event" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="EventContact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *                 &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}HotelReferenceGroup"/&amp;gt;
 *       &amp;lt;attribute name="GroupBlockCode" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to16" /&amp;gt;
 *       &amp;lt;attribute name="CreationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelRoomListType", propOrder = {
    "uniqueID",
    "guests",
    "masterContact",
    "masterAccount",
    "roomStays",
    "event"
})
public class HotelRoomListType {

    @XmlElement(name = "UniqueID")
    protected UniqueIDType uniqueID;
    @XmlElement(name = "Guests")
    protected HotelRoomListType.Guests guests;
    @XmlElement(name = "MasterContact")
    protected HotelRoomListType.MasterContact masterContact;
    @XmlElement(name = "MasterAccount")
    protected HotelRoomListType.MasterAccount masterAccount;
    @XmlElement(name = "RoomStays")
    protected HotelRoomListType.RoomStays roomStays;
    @XmlElement(name = "Event")
    protected HotelRoomListType.Event event;
    @XmlAttribute(name = "GroupBlockCode")
    protected String groupBlockCode;
    @XmlAttribute(name = "CreationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDate;
    @XmlAttribute(name = "ChainCode")
    protected String chainCode;
    @XmlAttribute(name = "BrandCode")
    protected String brandCode;
    @XmlAttribute(name = "HotelCode")
    protected String hotelCode;
    @XmlAttribute(name = "HotelCityCode")
    protected String hotelCityCode;
    @XmlAttribute(name = "HotelName")
    protected String hotelName;
    @XmlAttribute(name = "HotelCodeContext")
    protected String hotelCodeContext;
    @XmlAttribute(name = "ChainName")
    protected String chainName;
    @XmlAttribute(name = "BrandName")
    protected String brandName;
    @XmlAttribute(name = "AreaID")
    protected String areaID;
    @XmlAttribute(name = "TTIcode")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger ttIcode;

    /**
     * Obtiene el valor de la propiedad uniqueID.
     * 
     * @return
     *     possible object is
     *     {@link UniqueIDType }
     *     
     */
    public UniqueIDType getUniqueID() {
        return uniqueID;
    }

    /**
     * Define el valor de la propiedad uniqueID.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueIDType }
     *     
     */
    public void setUniqueID(UniqueIDType value) {
        this.uniqueID = value;
    }

    /**
     * Obtiene el valor de la propiedad guests.
     * 
     * @return
     *     possible object is
     *     {@link HotelRoomListType.Guests }
     *     
     */
    public HotelRoomListType.Guests getGuests() {
        return guests;
    }

    /**
     * Define el valor de la propiedad guests.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelRoomListType.Guests }
     *     
     */
    public void setGuests(HotelRoomListType.Guests value) {
        this.guests = value;
    }

    /**
     * Obtiene el valor de la propiedad masterContact.
     * 
     * @return
     *     possible object is
     *     {@link HotelRoomListType.MasterContact }
     *     
     */
    public HotelRoomListType.MasterContact getMasterContact() {
        return masterContact;
    }

    /**
     * Define el valor de la propiedad masterContact.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelRoomListType.MasterContact }
     *     
     */
    public void setMasterContact(HotelRoomListType.MasterContact value) {
        this.masterContact = value;
    }

    /**
     * Obtiene el valor de la propiedad masterAccount.
     * 
     * @return
     *     possible object is
     *     {@link HotelRoomListType.MasterAccount }
     *     
     */
    public HotelRoomListType.MasterAccount getMasterAccount() {
        return masterAccount;
    }

    /**
     * Define el valor de la propiedad masterAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelRoomListType.MasterAccount }
     *     
     */
    public void setMasterAccount(HotelRoomListType.MasterAccount value) {
        this.masterAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad roomStays.
     * 
     * @return
     *     possible object is
     *     {@link HotelRoomListType.RoomStays }
     *     
     */
    public HotelRoomListType.RoomStays getRoomStays() {
        return roomStays;
    }

    /**
     * Define el valor de la propiedad roomStays.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelRoomListType.RoomStays }
     *     
     */
    public void setRoomStays(HotelRoomListType.RoomStays value) {
        this.roomStays = value;
    }

    /**
     * Obtiene el valor de la propiedad event.
     * 
     * @return
     *     possible object is
     *     {@link HotelRoomListType.Event }
     *     
     */
    public HotelRoomListType.Event getEvent() {
        return event;
    }

    /**
     * Define el valor de la propiedad event.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelRoomListType.Event }
     *     
     */
    public void setEvent(HotelRoomListType.Event value) {
        this.event = value;
    }

    /**
     * Obtiene el valor de la propiedad groupBlockCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupBlockCode() {
        return groupBlockCode;
    }

    /**
     * Define el valor de la propiedad groupBlockCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupBlockCode(String value) {
        this.groupBlockCode = value;
    }

    /**
     * Obtiene el valor de la propiedad creationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Define el valor de la propiedad creationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad chainCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainCode() {
        return chainCode;
    }

    /**
     * Define el valor de la propiedad chainCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainCode(String value) {
        this.chainCode = value;
    }

    /**
     * Obtiene el valor de la propiedad brandCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandCode() {
        return brandCode;
    }

    /**
     * Define el valor de la propiedad brandCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandCode(String value) {
        this.brandCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Define el valor de la propiedad hotelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCityCode() {
        return hotelCityCode;
    }

    /**
     * Define el valor de la propiedad hotelCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCityCode(String value) {
        this.hotelCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * Define el valor de la propiedad hotelName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelName(String value) {
        this.hotelName = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCodeContext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCodeContext() {
        return hotelCodeContext;
    }

    /**
     * Define el valor de la propiedad hotelCodeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCodeContext(String value) {
        this.hotelCodeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad chainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainName() {
        return chainName;
    }

    /**
     * Define el valor de la propiedad chainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainName(String value) {
        this.chainName = value;
    }

    /**
     * Obtiene el valor de la propiedad brandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Define el valor de la propiedad brandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Obtiene el valor de la propiedad areaID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaID() {
        return areaID;
    }

    /**
     * Define el valor de la propiedad areaID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaID(String value) {
        this.areaID = value;
    }

    /**
     * Obtiene el valor de la propiedad ttIcode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTTIcode() {
        return ttIcode;
    }

    /**
     * Define el valor de la propiedad ttIcode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTTIcode(BigInteger value) {
        this.ttIcode = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="EventContact" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
     *       &amp;lt;attribute name="MeetingName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "eventContact"
    })
    public static class Event {

        @XmlElement(name = "EventContact", required = true)
        protected ContactPersonType eventContact;
        @XmlAttribute(name = "MeetingName")
        protected String meetingName;
        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;

        /**
         * Obtiene el valor de la propiedad eventContact.
         * 
         * @return
         *     possible object is
         *     {@link ContactPersonType }
         *     
         */
        public ContactPersonType getEventContact() {
            return eventContact;
        }

        /**
         * Define el valor de la propiedad eventContact.
         * 
         * @param value
         *     allowed object is
         *     {@link ContactPersonType }
         *     
         */
        public void setEventContact(ContactPersonType value) {
            this.eventContact = value;
        }

        /**
         * Obtiene el valor de la propiedad meetingName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMeetingName() {
            return meetingName;
        }

        /**
         * Define el valor de la propiedad meetingName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMeetingName(String value) {
            this.meetingName = value;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Guest" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Loyalty" maxOccurs="5" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="GuaranteePayment" maxOccurs="2" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelPaymentFormType"&amp;gt;
     *                           &amp;lt;attribute name="DetailType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="Payment"/&amp;gt;
     *                                 &amp;lt;enumeration value="Guarantee"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="GuaranteeType"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
     *                                 &amp;lt;enumeration value="GuaranteeRequired"/&amp;gt;
     *                                 &amp;lt;enumeration value="None"/&amp;gt;
     *                                 &amp;lt;enumeration value="CC/DC/Voucher"/&amp;gt;
     *                                 &amp;lt;enumeration value="Profile"/&amp;gt;
     *                                 &amp;lt;enumeration value="Deposit"/&amp;gt;
     *                                 &amp;lt;enumeration value="PrePay"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AdditionalDetails" type="{http://www.opentravel.org/OTA/2003/05}AdditionalDetailsType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="GuestAction" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *                 &amp;lt;attribute name="PrintConfoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "guest"
    })
    public static class Guests {

        @XmlElement(name = "Guest", required = true)
        protected List<HotelRoomListType.Guests.Guest> guest;

        /**
         * Gets the value of the guest property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the guest property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getGuest().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelRoomListType.Guests.Guest }
         * 
         * 
         */
        public List<HotelRoomListType.Guests.Guest> getGuest() {
            if (guest == null) {
                guest = new ArrayList<HotelRoomListType.Guests.Guest>();
            }
            return this.guest;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Loyalty" maxOccurs="5" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="GuaranteePayment" maxOccurs="2" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelPaymentFormType"&amp;gt;
         *                 &amp;lt;attribute name="DetailType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="Payment"/&amp;gt;
         *                       &amp;lt;enumeration value="Guarantee"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="GuaranteeType"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
         *                       &amp;lt;enumeration value="GuaranteeRequired"/&amp;gt;
         *                       &amp;lt;enumeration value="None"/&amp;gt;
         *                       &amp;lt;enumeration value="CC/DC/Voucher"/&amp;gt;
         *                       &amp;lt;enumeration value="Profile"/&amp;gt;
         *                       &amp;lt;enumeration value="Deposit"/&amp;gt;
         *                       &amp;lt;enumeration value="PrePay"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AdditionalDetails" type="{http://www.opentravel.org/OTA/2003/05}AdditionalDetailsType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="GuestAction" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *       &amp;lt;attribute name="PrintConfoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID",
            "loyalty",
            "guaranteePayment",
            "additionalDetails"
        })
        public static class Guest
            extends ContactPersonType
        {

            @XmlElement(name = "UniqueID")
            protected UniqueIDType uniqueID;
            @XmlElement(name = "Loyalty")
            protected List<HotelRoomListType.Guests.Guest.Loyalty> loyalty;
            @XmlElement(name = "GuaranteePayment")
            protected List<HotelRoomListType.Guests.Guest.GuaranteePayment> guaranteePayment;
            @XmlElement(name = "AdditionalDetails")
            protected AdditionalDetailsType additionalDetails;
            @XmlAttribute(name = "GuestAction")
            protected ActionType guestAction;
            @XmlAttribute(name = "PrintConfoInd")
            protected Boolean printConfoInd;

            /**
             * Obtiene el valor de la propiedad uniqueID.
             * 
             * @return
             *     possible object is
             *     {@link UniqueIDType }
             *     
             */
            public UniqueIDType getUniqueID() {
                return uniqueID;
            }

            /**
             * Define el valor de la propiedad uniqueID.
             * 
             * @param value
             *     allowed object is
             *     {@link UniqueIDType }
             *     
             */
            public void setUniqueID(UniqueIDType value) {
                this.uniqueID = value;
            }

            /**
             * Gets the value of the loyalty property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyalty property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getLoyalty().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link HotelRoomListType.Guests.Guest.Loyalty }
             * 
             * 
             */
            public List<HotelRoomListType.Guests.Guest.Loyalty> getLoyalty() {
                if (loyalty == null) {
                    loyalty = new ArrayList<HotelRoomListType.Guests.Guest.Loyalty>();
                }
                return this.loyalty;
            }

            /**
             * Gets the value of the guaranteePayment property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the guaranteePayment property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getGuaranteePayment().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link HotelRoomListType.Guests.Guest.GuaranteePayment }
             * 
             * 
             */
            public List<HotelRoomListType.Guests.Guest.GuaranteePayment> getGuaranteePayment() {
                if (guaranteePayment == null) {
                    guaranteePayment = new ArrayList<HotelRoomListType.Guests.Guest.GuaranteePayment>();
                }
                return this.guaranteePayment;
            }

            /**
             * Obtiene el valor de la propiedad additionalDetails.
             * 
             * @return
             *     possible object is
             *     {@link AdditionalDetailsType }
             *     
             */
            public AdditionalDetailsType getAdditionalDetails() {
                return additionalDetails;
            }

            /**
             * Define el valor de la propiedad additionalDetails.
             * 
             * @param value
             *     allowed object is
             *     {@link AdditionalDetailsType }
             *     
             */
            public void setAdditionalDetails(AdditionalDetailsType value) {
                this.additionalDetails = value;
            }

            /**
             * Obtiene el valor de la propiedad guestAction.
             * 
             * @return
             *     possible object is
             *     {@link ActionType }
             *     
             */
            public ActionType getGuestAction() {
                return guestAction;
            }

            /**
             * Define el valor de la propiedad guestAction.
             * 
             * @param value
             *     allowed object is
             *     {@link ActionType }
             *     
             */
            public void setGuestAction(ActionType value) {
                this.guestAction = value;
            }

            /**
             * Obtiene el valor de la propiedad printConfoInd.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPrintConfoInd() {
                return printConfoInd;
            }

            /**
             * Define el valor de la propiedad printConfoInd.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPrintConfoInd(Boolean value) {
                this.printConfoInd = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}HotelPaymentFormType"&amp;gt;
             *       &amp;lt;attribute name="DetailType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="Payment"/&amp;gt;
             *             &amp;lt;enumeration value="Guarantee"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="GuaranteeType"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.opentravel.org/OTA/2003/05}StringLength1to32"&amp;gt;
             *             &amp;lt;enumeration value="GuaranteeRequired"/&amp;gt;
             *             &amp;lt;enumeration value="None"/&amp;gt;
             *             &amp;lt;enumeration value="CC/DC/Voucher"/&amp;gt;
             *             &amp;lt;enumeration value="Profile"/&amp;gt;
             *             &amp;lt;enumeration value="Deposit"/&amp;gt;
             *             &amp;lt;enumeration value="PrePay"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class GuaranteePayment
                extends HotelPaymentFormType
            {

                @XmlAttribute(name = "DetailType")
                protected String detailType;
                @XmlAttribute(name = "GuaranteeType")
                protected String guaranteeType;

                /**
                 * Obtiene el valor de la propiedad detailType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDetailType() {
                    return detailType;
                }

                /**
                 * Define el valor de la propiedad detailType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDetailType(String value) {
                    this.detailType = value;
                }

                /**
                 * Obtiene el valor de la propiedad guaranteeType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGuaranteeType() {
                    return guaranteeType;
                }

                /**
                 * Define el valor de la propiedad guaranteeType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGuaranteeType(String value) {
                    this.guaranteeType = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Loyalty {

                @XmlAttribute(name = "ReservationActionType")
                protected String reservationActionType;
                @XmlAttribute(name = "SelectedLoyaltyRPH")
                protected String selectedLoyaltyRPH;
                @XmlAttribute(name = "ProgramCode")
                protected String programCode;
                @XmlAttribute(name = "BonusCode")
                protected String bonusCode;
                @XmlAttribute(name = "AccountID")
                protected String accountID;
                @XmlAttribute(name = "PointsEarned")
                protected String pointsEarned;

                /**
                 * Obtiene el valor de la propiedad reservationActionType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReservationActionType() {
                    return reservationActionType;
                }

                /**
                 * Define el valor de la propiedad reservationActionType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReservationActionType(String value) {
                    this.reservationActionType = value;
                }

                /**
                 * Obtiene el valor de la propiedad selectedLoyaltyRPH.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSelectedLoyaltyRPH() {
                    return selectedLoyaltyRPH;
                }

                /**
                 * Define el valor de la propiedad selectedLoyaltyRPH.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSelectedLoyaltyRPH(String value) {
                    this.selectedLoyaltyRPH = value;
                }

                /**
                 * Obtiene el valor de la propiedad programCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProgramCode() {
                    return programCode;
                }

                /**
                 * Define el valor de la propiedad programCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProgramCode(String value) {
                    this.programCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad bonusCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBonusCode() {
                    return bonusCode;
                }

                /**
                 * Define el valor de la propiedad bonusCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBonusCode(String value) {
                    this.bonusCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad accountID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAccountID() {
                    return accountID;
                }

                /**
                 * Define el valor de la propiedad accountID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAccountID(String value) {
                    this.accountID = value;
                }

                /**
                 * Obtiene el valor de la propiedad pointsEarned.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPointsEarned() {
                    return pointsEarned;
                }

                /**
                 * Define el valor de la propiedad pointsEarned.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPointsEarned(String value) {
                    this.pointsEarned = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DirectBillType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BillingType"/&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MasterAccount
        extends DirectBillType
    {

        @XmlAttribute(name = "BillingType")
        protected String billingType;
        @XmlAttribute(name = "SignFoodAndBev")
        protected Boolean signFoodAndBev;

        /**
         * Obtiene el valor de la propiedad billingType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillingType() {
            return billingType;
        }

        /**
         * Define el valor de la propiedad billingType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillingType(String value) {
            this.billingType = value;
        }

        /**
         * Obtiene el valor de la propiedad signFoodAndBev.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSignFoodAndBev() {
            return signFoodAndBev;
        }

        /**
         * Define el valor de la propiedad signFoodAndBev.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSignFoodAndBev(Boolean value) {
            this.signFoodAndBev = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}ContactPersonType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="UniqueIDs" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Loyalty" maxOccurs="5" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uniqueIDs",
        "loyalty"
    })
    public static class MasterContact
        extends ContactPersonType
    {

        @XmlElement(name = "UniqueIDs")
        protected HotelRoomListType.MasterContact.UniqueIDs uniqueIDs;
        @XmlElement(name = "Loyalty")
        protected List<HotelRoomListType.MasterContact.Loyalty> loyalty;

        /**
         * Obtiene el valor de la propiedad uniqueIDs.
         * 
         * @return
         *     possible object is
         *     {@link HotelRoomListType.MasterContact.UniqueIDs }
         *     
         */
        public HotelRoomListType.MasterContact.UniqueIDs getUniqueIDs() {
            return uniqueIDs;
        }

        /**
         * Define el valor de la propiedad uniqueIDs.
         * 
         * @param value
         *     allowed object is
         *     {@link HotelRoomListType.MasterContact.UniqueIDs }
         *     
         */
        public void setUniqueIDs(HotelRoomListType.MasterContact.UniqueIDs value) {
            this.uniqueIDs = value;
        }

        /**
         * Gets the value of the loyalty property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the loyalty property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getLoyalty().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelRoomListType.MasterContact.Loyalty }
         * 
         * 
         */
        public List<HotelRoomListType.MasterContact.Loyalty> getLoyalty() {
            if (loyalty == null) {
                loyalty = new ArrayList<HotelRoomListType.MasterContact.Loyalty>();
            }
            return this.loyalty;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Loyalty {

            @XmlAttribute(name = "ReservationActionType")
            protected String reservationActionType;
            @XmlAttribute(name = "SelectedLoyaltyRPH")
            protected String selectedLoyaltyRPH;
            @XmlAttribute(name = "ProgramCode")
            protected String programCode;
            @XmlAttribute(name = "BonusCode")
            protected String bonusCode;
            @XmlAttribute(name = "AccountID")
            protected String accountID;
            @XmlAttribute(name = "PointsEarned")
            protected String pointsEarned;

            /**
             * Obtiene el valor de la propiedad reservationActionType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReservationActionType() {
                return reservationActionType;
            }

            /**
             * Define el valor de la propiedad reservationActionType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReservationActionType(String value) {
                this.reservationActionType = value;
            }

            /**
             * Obtiene el valor de la propiedad selectedLoyaltyRPH.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSelectedLoyaltyRPH() {
                return selectedLoyaltyRPH;
            }

            /**
             * Define el valor de la propiedad selectedLoyaltyRPH.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSelectedLoyaltyRPH(String value) {
                this.selectedLoyaltyRPH = value;
            }

            /**
             * Obtiene el valor de la propiedad programCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProgramCode() {
                return programCode;
            }

            /**
             * Define el valor de la propiedad programCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProgramCode(String value) {
                this.programCode = value;
            }

            /**
             * Obtiene el valor de la propiedad bonusCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBonusCode() {
                return bonusCode;
            }

            /**
             * Define el valor de la propiedad bonusCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBonusCode(String value) {
                this.bonusCode = value;
            }

            /**
             * Obtiene el valor de la propiedad accountID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccountID() {
                return accountID;
            }

            /**
             * Define el valor de la propiedad accountID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccountID(String value) {
                this.accountID = value;
            }

            /**
             * Obtiene el valor de la propiedad pointsEarned.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPointsEarned() {
                return pointsEarned;
            }

            /**
             * Define el valor de la propiedad pointsEarned.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPointsEarned(String value) {
                this.pointsEarned = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" maxOccurs="9" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uniqueID"
        })
        public static class UniqueIDs {

            @XmlElement(name = "UniqueID")
            protected List<UniqueIDType> uniqueID;

            /**
             * Gets the value of the uniqueID property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the uniqueID property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getUniqueID().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link UniqueIDType }
             * 
             * 
             */
            public List<UniqueIDType> getUniqueID() {
                if (uniqueID == null) {
                    uniqueID = new ArrayList<UniqueIDType>();
                }
                return this.uniqueID;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
     *                 &amp;lt;sequence minOccurs="0"&amp;gt;
     *                   &amp;lt;element name="HotelReservationIDs" type="{http://www.opentravel.org/OTA/2003/05}HotelReservationIDsType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="RoomShares" type="{http://www.opentravel.org/OTA/2003/05}RoomSharesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;choice minOccurs="0"&amp;gt;
     *                     &amp;lt;sequence&amp;gt;
     *                       &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
     *                       &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
     *                     &amp;lt;/sequence&amp;gt;
     *                     &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
     *                   &amp;lt;/choice&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="RoomStay" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "roomStay"
    })
    public static class RoomStays {

        @XmlElement(name = "RoomStay", required = true)
        protected List<HotelRoomListType.RoomStays.RoomStay> roomStay;

        /**
         * Gets the value of the roomStay property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomStay property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getRoomStay().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link HotelRoomListType.RoomStays.RoomStay }
         * 
         * 
         */
        public List<HotelRoomListType.RoomStays.RoomStay> getRoomStay() {
            if (roomStay == null) {
                roomStay = new ArrayList<HotelRoomListType.RoomStays.RoomStay>();
            }
            return this.roomStay;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
         *       &amp;lt;sequence minOccurs="0"&amp;gt;
         *         &amp;lt;element name="HotelReservationIDs" type="{http://www.opentravel.org/OTA/2003/05}HotelReservationIDsType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="RoomShares" type="{http://www.opentravel.org/OTA/2003/05}RoomSharesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="UniqueID" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;choice minOccurs="0"&amp;gt;
         *           &amp;lt;sequence&amp;gt;
         *             &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
         *             &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
         *           &amp;lt;/sequence&amp;gt;
         *           &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
         *         &amp;lt;/choice&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="RoomStay" type="{http://www.opentravel.org/OTA/2003/05}ActionType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "hotelReservationIDs",
            "roomShares",
            "uniqueID",
            "success",
            "warnings",
            "errors"
        })
        public static class RoomStay
            extends RoomStayType
        {

            @XmlElement(name = "HotelReservationIDs")
            protected HotelReservationIDsType hotelReservationIDs;
            @XmlElement(name = "RoomShares")
            protected RoomSharesType roomShares;
            @XmlElement(name = "UniqueID")
            protected UniqueIDType uniqueID;
            @XmlElement(name = "Success")
            protected SuccessType success;
            @XmlElement(name = "Warnings")
            protected WarningsType warnings;
            @XmlElement(name = "Errors")
            protected ErrorsType errors;
            @XmlAttribute(name = "RoomStay")
            protected ActionType roomStay;

            /**
             * Obtiene el valor de la propiedad hotelReservationIDs.
             * 
             * @return
             *     possible object is
             *     {@link HotelReservationIDsType }
             *     
             */
            public HotelReservationIDsType getHotelReservationIDs() {
                return hotelReservationIDs;
            }

            /**
             * Define el valor de la propiedad hotelReservationIDs.
             * 
             * @param value
             *     allowed object is
             *     {@link HotelReservationIDsType }
             *     
             */
            public void setHotelReservationIDs(HotelReservationIDsType value) {
                this.hotelReservationIDs = value;
            }

            /**
             * Obtiene el valor de la propiedad roomShares.
             * 
             * @return
             *     possible object is
             *     {@link RoomSharesType }
             *     
             */
            public RoomSharesType getRoomShares() {
                return roomShares;
            }

            /**
             * Define el valor de la propiedad roomShares.
             * 
             * @param value
             *     allowed object is
             *     {@link RoomSharesType }
             *     
             */
            public void setRoomShares(RoomSharesType value) {
                this.roomShares = value;
            }

            /**
             * Obtiene el valor de la propiedad uniqueID.
             * 
             * @return
             *     possible object is
             *     {@link UniqueIDType }
             *     
             */
            public UniqueIDType getUniqueID() {
                return uniqueID;
            }

            /**
             * Define el valor de la propiedad uniqueID.
             * 
             * @param value
             *     allowed object is
             *     {@link UniqueIDType }
             *     
             */
            public void setUniqueID(UniqueIDType value) {
                this.uniqueID = value;
            }

            /**
             * Obtiene el valor de la propiedad success.
             * 
             * @return
             *     possible object is
             *     {@link SuccessType }
             *     
             */
            public SuccessType getSuccess() {
                return success;
            }

            /**
             * Define el valor de la propiedad success.
             * 
             * @param value
             *     allowed object is
             *     {@link SuccessType }
             *     
             */
            public void setSuccess(SuccessType value) {
                this.success = value;
            }

            /**
             * Obtiene el valor de la propiedad warnings.
             * 
             * @return
             *     possible object is
             *     {@link WarningsType }
             *     
             */
            public WarningsType getWarnings() {
                return warnings;
            }

            /**
             * Define el valor de la propiedad warnings.
             * 
             * @param value
             *     allowed object is
             *     {@link WarningsType }
             *     
             */
            public void setWarnings(WarningsType value) {
                this.warnings = value;
            }

            /**
             * Obtiene el valor de la propiedad errors.
             * 
             * @return
             *     possible object is
             *     {@link ErrorsType }
             *     
             */
            public ErrorsType getErrors() {
                return errors;
            }

            /**
             * Define el valor de la propiedad errors.
             * 
             * @param value
             *     allowed object is
             *     {@link ErrorsType }
             *     
             */
            public void setErrors(ErrorsType value) {
                this.errors = value;
            }

            /**
             * Obtiene el valor de la propiedad roomStay.
             * 
             * @return
             *     possible object is
             *     {@link ActionType }
             *     
             */
            public ActionType getRoomStay() {
                return roomStay;
            }

            /**
             * Define el valor de la propiedad roomStay.
             * 
             * @param value
             *     allowed object is
             *     {@link ActionType }
             *     
             */
            public void setRoomStay(ActionType value) {
                this.roomStay = value;
            }

        }

    }

}
