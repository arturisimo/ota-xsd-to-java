
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para List_OfferAvailabilityStartFormula.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="List_OfferAvailabilityStartFormula"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="CustomerSpecified"/&amp;gt;
 *     &amp;lt;enumeration value="FlightArrival"/&amp;gt;
 *     &amp;lt;enumeration value="RentalCarDropOff"/&amp;gt;
 *     &amp;lt;enumeration value="TravelGracePeriod_Distance"/&amp;gt;
 *     &amp;lt;enumeration value="TravelGracePeriod_PrivateSchedule"/&amp;gt;
 *     &amp;lt;enumeration value="TravelGracePeriod_PublicSchedule"/&amp;gt;
 *     &amp;lt;enumeration value="Other_"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "List_OfferAvailabilityStartFormula")
@XmlEnum
public enum ListOfferAvailabilityStartFormula {


    /**
     * The customer has specified the earliest start date or date and time.
     * 
     */
    @XmlEnumValue("CustomerSpecified")
    CUSTOMER_SPECIFIED("CustomerSpecified"),
    @XmlEnumValue("FlightArrival")
    FLIGHT_ARRIVAL("FlightArrival"),
    @XmlEnumValue("RentalCarDropOff")
    RENTAL_CAR_DROP_OFF("RentalCarDropOff"),

    /**
     * The travel grace period (travel time between two locations) has been estimated based on the calculated distance between the known start and end locations.
     * 
     */
    @XmlEnumValue("TravelGracePeriod_Distance")
    TRAVEL_GRACE_PERIOD_DISTANCE("TravelGracePeriod_Distance"),

    /**
     * The travel grace period (travel time between two locations) has been estimated based on a known operating schedule for a supplier or prearranged mode of transportation between the start and end locations.
     * 
     */
    @XmlEnumValue("TravelGracePeriod_PrivateSchedule")
    TRAVEL_GRACE_PERIOD_PRIVATE_SCHEDULE("TravelGracePeriod_PrivateSchedule"),

    /**
     * The travel grace period (travel time between two locations) has been estimated based on a known operating schedule for a public mode of transportation between the start and end locations.
     * 
     */
    @XmlEnumValue("TravelGracePeriod_PublicSchedule")
    TRAVEL_GRACE_PERIOD_PUBLIC_SCHEDULE("TravelGracePeriod_PublicSchedule"),

    /**
     * This is a string list of enumerations with an "Other_" literal to support an open enumeration list. Use the "Other_" value in combination with the &#064;OtherType attribute to exchange a literal that is not in the list and is known to your trading partners.
     * 
     */
    @XmlEnumValue("Other_")
    OTHER("Other_");
    private final String value;

    ListOfferAvailabilityStartFormula(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ListOfferAvailabilityStartFormula fromValue(String v) {
        for (ListOfferAvailabilityStartFormula c: ListOfferAvailabilityStartFormula.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
