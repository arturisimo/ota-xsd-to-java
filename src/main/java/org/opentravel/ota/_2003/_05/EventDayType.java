
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para EventDayType.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="EventDayType"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="PreEvent"/&amp;gt;
 *     &amp;lt;enumeration value="PostEvent"/&amp;gt;
 *     &amp;lt;enumeration value="Published"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "EventDayType")
@XmlEnum
public enum EventDayType {


    /**
     * Indicates this day is prior to the published event dates.
     * 
     */
    @XmlEnumValue("PreEvent")
    PRE_EVENT("PreEvent"),

    /**
     * Indicates this day is after the published event dates.
     * 
     */
    @XmlEnumValue("PostEvent")
    POST_EVENT("PostEvent"),

    /**
     * Indicates this day is part of the published event dates.
     * 
     */
    @XmlEnumValue("Published")
    PUBLISHED("Published");
    private final String value;

    EventDayType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EventDayType fromValue(String v) {
        for (EventDayType c: EventDayType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
