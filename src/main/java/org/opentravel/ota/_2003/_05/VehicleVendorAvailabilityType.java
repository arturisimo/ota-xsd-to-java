
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The VehicleVendorAvailabilityType complex type identifies the data that describes the availability of one or more vehicles for a specific vendor, along with supplemental information about the vendor and the facilities of that vendor.
 * 
 * &lt;p&gt;Clase Java para VehicleVendorAvailabilityType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VehicleVendorAvailabilityType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Vendor" type="{http://www.opentravel.org/OTA/2003/05}CompanyNameType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="VehAvails"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="VehAvail" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="VehAvailCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailCoreType"/&amp;gt;
 *                             &amp;lt;element name="VehAvailInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailAdditionalInfoType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
 *                                     &amp;lt;attribute name="RulesApplyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="RateCategory" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                 &amp;lt;attribute name="RatePeriod" type="{http://www.opentravel.org/OTA/2003/05}RatePeriodSimpleType" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Info" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailVendorInfoType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleVendorAvailabilityType", propOrder = {
    "vendor",
    "vehAvails",
    "info"
})
public class VehicleVendorAvailabilityType {

    @XmlElement(name = "Vendor")
    protected CompanyNameType vendor;
    @XmlElement(name = "VehAvails", required = true)
    protected VehicleVendorAvailabilityType.VehAvails vehAvails;
    @XmlElement(name = "Info")
    protected VehicleAvailVendorInfoType info;

    /**
     * Obtiene el valor de la propiedad vendor.
     * 
     * @return
     *     possible object is
     *     {@link CompanyNameType }
     *     
     */
    public CompanyNameType getVendor() {
        return vendor;
    }

    /**
     * Define el valor de la propiedad vendor.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyNameType }
     *     
     */
    public void setVendor(CompanyNameType value) {
        this.vendor = value;
    }

    /**
     * Obtiene el valor de la propiedad vehAvails.
     * 
     * @return
     *     possible object is
     *     {@link VehicleVendorAvailabilityType.VehAvails }
     *     
     */
    public VehicleVendorAvailabilityType.VehAvails getVehAvails() {
        return vehAvails;
    }

    /**
     * Define el valor de la propiedad vehAvails.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleVendorAvailabilityType.VehAvails }
     *     
     */
    public void setVehAvails(VehicleVendorAvailabilityType.VehAvails value) {
        this.vehAvails = value;
    }

    /**
     * Obtiene el valor de la propiedad info.
     * 
     * @return
     *     possible object is
     *     {@link VehicleAvailVendorInfoType }
     *     
     */
    public VehicleAvailVendorInfoType getInfo() {
        return info;
    }

    /**
     * Define el valor de la propiedad info.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleAvailVendorInfoType }
     *     
     */
    public void setInfo(VehicleAvailVendorInfoType value) {
        this.info = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="VehAvail" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="VehAvailCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailCoreType"/&amp;gt;
     *                   &amp;lt;element name="VehAvailInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailAdditionalInfoType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
     *                           &amp;lt;attribute name="RulesApplyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="RateCategory" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *       &amp;lt;attribute name="RatePeriod" type="{http://www.opentravel.org/OTA/2003/05}RatePeriodSimpleType" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehAvail"
    })
    public static class VehAvails {

        @XmlElement(name = "VehAvail", required = true)
        protected List<VehicleVendorAvailabilityType.VehAvails.VehAvail> vehAvail;
        @XmlAttribute(name = "RateCategory")
        protected String rateCategory;
        @XmlAttribute(name = "RatePeriod")
        protected RatePeriodSimpleType ratePeriod;

        /**
         * Gets the value of the vehAvail property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the vehAvail property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getVehAvail().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link VehicleVendorAvailabilityType.VehAvails.VehAvail }
         * 
         * 
         */
        public List<VehicleVendorAvailabilityType.VehAvails.VehAvail> getVehAvail() {
            if (vehAvail == null) {
                vehAvail = new ArrayList<VehicleVendorAvailabilityType.VehAvails.VehAvail>();
            }
            return this.vehAvail;
        }

        /**
         * Obtiene el valor de la propiedad rateCategory.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRateCategory() {
            return rateCategory;
        }

        /**
         * Define el valor de la propiedad rateCategory.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRateCategory(String value) {
            this.rateCategory = value;
        }

        /**
         * Obtiene el valor de la propiedad ratePeriod.
         * 
         * @return
         *     possible object is
         *     {@link RatePeriodSimpleType }
         *     
         */
        public RatePeriodSimpleType getRatePeriod() {
            return ratePeriod;
        }

        /**
         * Define el valor de la propiedad ratePeriod.
         * 
         * @param value
         *     allowed object is
         *     {@link RatePeriodSimpleType }
         *     
         */
        public void setRatePeriod(RatePeriodSimpleType value) {
            this.ratePeriod = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="VehAvailCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailCoreType"/&amp;gt;
         *         &amp;lt;element name="VehAvailInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailAdditionalInfoType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
         *                 &amp;lt;attribute name="RulesApplyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "vehAvailCore",
            "vehAvailInfo",
            "advanceBooking"
        })
        public static class VehAvail {

            @XmlElement(name = "VehAvailCore", required = true)
            protected VehicleAvailCoreType vehAvailCore;
            @XmlElement(name = "VehAvailInfo")
            protected VehicleAvailAdditionalInfoType vehAvailInfo;
            @XmlElement(name = "AdvanceBooking")
            protected VehicleVendorAvailabilityType.VehAvails.VehAvail.AdvanceBooking advanceBooking;

            /**
             * Obtiene el valor de la propiedad vehAvailCore.
             * 
             * @return
             *     possible object is
             *     {@link VehicleAvailCoreType }
             *     
             */
            public VehicleAvailCoreType getVehAvailCore() {
                return vehAvailCore;
            }

            /**
             * Define el valor de la propiedad vehAvailCore.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleAvailCoreType }
             *     
             */
            public void setVehAvailCore(VehicleAvailCoreType value) {
                this.vehAvailCore = value;
            }

            /**
             * Obtiene el valor de la propiedad vehAvailInfo.
             * 
             * @return
             *     possible object is
             *     {@link VehicleAvailAdditionalInfoType }
             *     
             */
            public VehicleAvailAdditionalInfoType getVehAvailInfo() {
                return vehAvailInfo;
            }

            /**
             * Define el valor de la propiedad vehAvailInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleAvailAdditionalInfoType }
             *     
             */
            public void setVehAvailInfo(VehicleAvailAdditionalInfoType value) {
                this.vehAvailInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad advanceBooking.
             * 
             * @return
             *     possible object is
             *     {@link VehicleVendorAvailabilityType.VehAvails.VehAvail.AdvanceBooking }
             *     
             */
            public VehicleVendorAvailabilityType.VehAvails.VehAvail.AdvanceBooking getAdvanceBooking() {
                return advanceBooking;
            }

            /**
             * Define el valor de la propiedad advanceBooking.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleVendorAvailabilityType.VehAvails.VehAvail.AdvanceBooking }
             *     
             */
            public void setAdvanceBooking(VehicleVendorAvailabilityType.VehAvails.VehAvail.AdvanceBooking value) {
                this.advanceBooking = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
             *       &amp;lt;attribute name="RulesApplyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AdvanceBooking {

                @XmlAttribute(name = "RulesApplyInd")
                protected Boolean rulesApplyInd;
                @XmlAttribute(name = "AbsoluteDeadline")
                protected String absoluteDeadline;
                @XmlAttribute(name = "OffsetTimeUnit")
                protected TimeUnitType offsetTimeUnit;
                @XmlAttribute(name = "OffsetUnitMultiplier")
                protected Integer offsetUnitMultiplier;
                @XmlAttribute(name = "OffsetDropTime")
                protected String offsetDropTime;

                /**
                 * Obtiene el valor de la propiedad rulesApplyInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isRulesApplyInd() {
                    return rulesApplyInd;
                }

                /**
                 * Define el valor de la propiedad rulesApplyInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setRulesApplyInd(Boolean value) {
                    this.rulesApplyInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad absoluteDeadline.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAbsoluteDeadline() {
                    return absoluteDeadline;
                }

                /**
                 * Define el valor de la propiedad absoluteDeadline.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAbsoluteDeadline(String value) {
                    this.absoluteDeadline = value;
                }

                /**
                 * Obtiene el valor de la propiedad offsetTimeUnit.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TimeUnitType }
                 *     
                 */
                public TimeUnitType getOffsetTimeUnit() {
                    return offsetTimeUnit;
                }

                /**
                 * Define el valor de la propiedad offsetTimeUnit.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TimeUnitType }
                 *     
                 */
                public void setOffsetTimeUnit(TimeUnitType value) {
                    this.offsetTimeUnit = value;
                }

                /**
                 * Obtiene el valor de la propiedad offsetUnitMultiplier.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getOffsetUnitMultiplier() {
                    return offsetUnitMultiplier;
                }

                /**
                 * Define el valor de la propiedad offsetUnitMultiplier.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setOffsetUnitMultiplier(Integer value) {
                    this.offsetUnitMultiplier = value;
                }

                /**
                 * Obtiene el valor de la propiedad offsetDropTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOffsetDropTime() {
                    return offsetDropTime;
                }

                /**
                 * Define el valor de la propiedad offsetDropTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOffsetDropTime(String value) {
                    this.offsetDropTime = value;
                }

            }

        }

    }

}
