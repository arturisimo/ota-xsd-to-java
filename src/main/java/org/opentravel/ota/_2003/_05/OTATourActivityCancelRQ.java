
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ContactDetail" type="{http://www.opentravel.org/OTA/2003/05}ContactPersonType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Confirmation"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                 &amp;lt;attribute name="OtherConfirmationID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="BasicInfo" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
 *               &amp;lt;/extension&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="ParticipantInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Group" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                           &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
 *                           &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="PaymentInfo" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "contactDetail",
    "confirmation",
    "basicInfo",
    "participantInfo",
    "paymentInfo",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_TourActivityCancelRQ")
public class OTATourActivityCancelRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "ContactDetail")
    protected ContactPersonType contactDetail;
    @XmlElement(name = "Confirmation", required = true)
    protected OTATourActivityCancelRQ.Confirmation confirmation;
    @XmlElement(name = "BasicInfo")
    protected OTATourActivityCancelRQ.BasicInfo basicInfo;
    @XmlElement(name = "ParticipantInfo")
    protected List<OTATourActivityCancelRQ.ParticipantInfo> participantInfo;
    @XmlElement(name = "PaymentInfo")
    protected List<OTATourActivityCancelRQ.PaymentInfo> paymentInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad contactDetail.
     * 
     * @return
     *     possible object is
     *     {@link ContactPersonType }
     *     
     */
    public ContactPersonType getContactDetail() {
        return contactDetail;
    }

    /**
     * Define el valor de la propiedad contactDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPersonType }
     *     
     */
    public void setContactDetail(ContactPersonType value) {
        this.contactDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad confirmation.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityCancelRQ.Confirmation }
     *     
     */
    public OTATourActivityCancelRQ.Confirmation getConfirmation() {
        return confirmation;
    }

    /**
     * Define el valor de la propiedad confirmation.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityCancelRQ.Confirmation }
     *     
     */
    public void setConfirmation(OTATourActivityCancelRQ.Confirmation value) {
        this.confirmation = value;
    }

    /**
     * Obtiene el valor de la propiedad basicInfo.
     * 
     * @return
     *     possible object is
     *     {@link OTATourActivityCancelRQ.BasicInfo }
     *     
     */
    public OTATourActivityCancelRQ.BasicInfo getBasicInfo() {
        return basicInfo;
    }

    /**
     * Define el valor de la propiedad basicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OTATourActivityCancelRQ.BasicInfo }
     *     
     */
    public void setBasicInfo(OTATourActivityCancelRQ.BasicInfo value) {
        this.basicInfo = value;
    }

    /**
     * Gets the value of the participantInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the participantInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getParticipantInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTATourActivityCancelRQ.ParticipantInfo }
     * 
     * 
     */
    public List<OTATourActivityCancelRQ.ParticipantInfo> getParticipantInfo() {
        if (participantInfo == null) {
            participantInfo = new ArrayList<OTATourActivityCancelRQ.ParticipantInfo>();
        }
        return this.participantInfo;
    }

    /**
     * Gets the value of the paymentInfo property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentInfo property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPaymentInfo().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link OTATourActivityCancelRQ.PaymentInfo }
     * 
     * 
     */
    public List<OTATourActivityCancelRQ.PaymentInfo> getPaymentInfo() {
        if (paymentInfo == null) {
            paymentInfo = new ArrayList<OTATourActivityCancelRQ.PaymentInfo>();
        }
        return this.paymentInfo;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}TourActivityID_Type"&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BasicInfo
        extends TourActivityIDType
    {


    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *       &amp;lt;attribute name="OtherConfirmationID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *     &amp;lt;/extension&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Confirmation
        extends UniqueIDType
    {

        @XmlAttribute(name = "OtherConfirmationID")
        protected String otherConfirmationID;

        /**
         * Obtiene el valor de la propiedad otherConfirmationID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOtherConfirmationID() {
            return otherConfirmationID;
        }

        /**
         * Define el valor de la propiedad otherConfirmationID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOtherConfirmationID(String value) {
            this.otherConfirmationID = value;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Group" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                 &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Individual" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
     *                 &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "group",
        "individual"
    })
    public static class ParticipantInfo {

        @XmlElement(name = "Group")
        protected OTATourActivityCancelRQ.ParticipantInfo.Group group;
        @XmlElement(name = "Individual")
        protected OTATourActivityCancelRQ.ParticipantInfo.Individual individual;

        /**
         * Obtiene el valor de la propiedad group.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityCancelRQ.ParticipantInfo.Group }
         *     
         */
        public OTATourActivityCancelRQ.ParticipantInfo.Group getGroup() {
            return group;
        }

        /**
         * Define el valor de la propiedad group.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityCancelRQ.ParticipantInfo.Group }
         *     
         */
        public void setGroup(OTATourActivityCancelRQ.ParticipantInfo.Group value) {
            this.group = value;
        }

        /**
         * Obtiene el valor de la propiedad individual.
         * 
         * @return
         *     possible object is
         *     {@link OTATourActivityCancelRQ.ParticipantInfo.Individual }
         *     
         */
        public OTATourActivityCancelRQ.ParticipantInfo.Individual getIndividual() {
            return individual;
        }

        /**
         * Define el valor de la propiedad individual.
         * 
         * @param value
         *     allowed object is
         *     {@link OTATourActivityCancelRQ.ParticipantInfo.Individual }
         *     
         */
        public void setIndividual(OTATourActivityCancelRQ.ParticipantInfo.Individual value) {
            this.individual = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Group {

            @XmlAttribute(name = "GroupCode")
            protected String groupCode;
            @XmlAttribute(name = "GroupName")
            protected String groupName;
            @XmlAttribute(name = "GroupID")
            protected String groupID;

            /**
             * Obtiene el valor de la propiedad groupCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupCode() {
                return groupCode;
            }

            /**
             * Define el valor de la propiedad groupCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupCode(String value) {
                this.groupCode = value;
            }

            /**
             * Obtiene el valor de la propiedad groupName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupName() {
                return groupName;
            }

            /**
             * Define el valor de la propiedad groupName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupName(String value) {
                this.groupName = value;
            }

            /**
             * Obtiene el valor de la propiedad groupID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupID() {
                return groupID;
            }

            /**
             * Define el valor de la propiedad groupID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupID(String value) {
                this.groupID = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PersonNameType"&amp;gt;
         *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Individual
            extends PersonNameType
        {

            @XmlAttribute(name = "ParticipantID")
            protected String participantID;

            /**
             * Obtiene el valor de la propiedad participantID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParticipantID() {
                return participantID;
            }

            /**
             * Define el valor de la propiedad participantID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParticipantID(String value) {
                this.participantID = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Detail" type="{http://www.opentravel.org/OTA/2003/05}PaymentFormType" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="ParticipantID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *       &amp;lt;attribute name="GroupID" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detail"
    })
    public static class PaymentInfo {

        @XmlElement(name = "Detail")
        protected PaymentFormType detail;
        @XmlAttribute(name = "ParticipantID")
        protected String participantID;
        @XmlAttribute(name = "GroupID")
        protected String groupID;

        /**
         * Obtiene el valor de la propiedad detail.
         * 
         * @return
         *     possible object is
         *     {@link PaymentFormType }
         *     
         */
        public PaymentFormType getDetail() {
            return detail;
        }

        /**
         * Define el valor de la propiedad detail.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentFormType }
         *     
         */
        public void setDetail(PaymentFormType value) {
            this.detail = value;
        }

        /**
         * Obtiene el valor de la propiedad participantID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParticipantID() {
            return participantID;
        }

        /**
         * Define el valor de la propiedad participantID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParticipantID(String value) {
            this.participantID = value;
        }

        /**
         * Obtiene el valor de la propiedad groupID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupID() {
            return groupID;
        }

        /**
         * Define el valor de la propiedad groupID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupID(String value) {
            this.groupID = value;
        }

    }

}
