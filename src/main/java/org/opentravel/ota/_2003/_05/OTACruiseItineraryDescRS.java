
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="SelectedSailing"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedSailingGroup"/&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element name="CruiseItinInfos"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="CruiseItinInfo" maxOccurs="99"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;element name="DateTimeDescription" maxOccurs="9"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;attribute name="DateTimeQualifier" use="required"&amp;gt;
 *                                         &amp;lt;simpleType&amp;gt;
 *                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                             &amp;lt;enumeration value="arrival"/&amp;gt;
 *                                             &amp;lt;enumeration value="departure"/&amp;gt;
 *                                             &amp;lt;enumeration value="stay"/&amp;gt;
 *                                             &amp;lt;enumeration value="boarding"/&amp;gt;
 *                                           &amp;lt;/restriction&amp;gt;
 *                                         &amp;lt;/simpleType&amp;gt;
 *                                       &amp;lt;/attribute&amp;gt;
 *                                       &amp;lt;attribute name="DateTimeDetails" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                                       &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="Information" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                             &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationInfoGroup"/&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "selectedSailing",
    "cruiseItinInfos",
    "errors"
})
@XmlRootElement(name = "OTA_CruiseItineraryDescRS")
public class OTACruiseItineraryDescRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "SelectedSailing")
    protected OTACruiseItineraryDescRS.SelectedSailing selectedSailing;
    @XmlElement(name = "CruiseItinInfos")
    protected OTACruiseItineraryDescRS.CruiseItinInfos cruiseItinInfos;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad selectedSailing.
     * 
     * @return
     *     possible object is
     *     {@link OTACruiseItineraryDescRS.SelectedSailing }
     *     
     */
    public OTACruiseItineraryDescRS.SelectedSailing getSelectedSailing() {
        return selectedSailing;
    }

    /**
     * Define el valor de la propiedad selectedSailing.
     * 
     * @param value
     *     allowed object is
     *     {@link OTACruiseItineraryDescRS.SelectedSailing }
     *     
     */
    public void setSelectedSailing(OTACruiseItineraryDescRS.SelectedSailing value) {
        this.selectedSailing = value;
    }

    /**
     * Obtiene el valor de la propiedad cruiseItinInfos.
     * 
     * @return
     *     possible object is
     *     {@link OTACruiseItineraryDescRS.CruiseItinInfos }
     *     
     */
    public OTACruiseItineraryDescRS.CruiseItinInfos getCruiseItinInfos() {
        return cruiseItinInfos;
    }

    /**
     * Define el valor de la propiedad cruiseItinInfos.
     * 
     * @param value
     *     allowed object is
     *     {@link OTACruiseItineraryDescRS.CruiseItinInfos }
     *     
     */
    public void setCruiseItinInfos(OTACruiseItineraryDescRS.CruiseItinInfos value) {
        this.cruiseItinInfos = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="CruiseItinInfo" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="DateTimeDescription" maxOccurs="9"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="DateTimeQualifier" use="required"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;enumeration value="arrival"/&amp;gt;
     *                                 &amp;lt;enumeration value="departure"/&amp;gt;
     *                                 &amp;lt;enumeration value="stay"/&amp;gt;
     *                                 &amp;lt;enumeration value="boarding"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="DateTimeDetails" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *                           &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Information" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationInfoGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cruiseItinInfo"
    })
    public static class CruiseItinInfos {

        @XmlElement(name = "CruiseItinInfo", required = true)
        protected List<OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo> cruiseItinInfo;

        /**
         * Gets the value of the cruiseItinInfo property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the cruiseItinInfo property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCruiseItinInfo().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo }
         * 
         * 
         */
        public List<OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo> getCruiseItinInfo() {
            if (cruiseItinInfo == null) {
                cruiseItinInfo = new ArrayList<OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo>();
            }
            return this.cruiseItinInfo;
        }


        /**
         * Used to specify detailed cruise itinerary information.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="DateTimeDescription" maxOccurs="9"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="DateTimeQualifier" use="required"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;enumeration value="arrival"/&amp;gt;
         *                       &amp;lt;enumeration value="departure"/&amp;gt;
         *                       &amp;lt;enumeration value="stay"/&amp;gt;
         *                       &amp;lt;enumeration value="boarding"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="DateTimeDetails" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *                 &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Information" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationInfoGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "dateTimeDescription",
            "information"
        })
        public static class CruiseItinInfo {

            @XmlElement(name = "DateTimeDescription", required = true)
            protected List<OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo.DateTimeDescription> dateTimeDescription;
            @XmlElement(name = "Information")
            protected ParagraphType information;
            @XmlAttribute(name = "PortCode")
            protected String portCode;
            @XmlAttribute(name = "PortName")
            protected String portName;
            @XmlAttribute(name = "PortCountryCode")
            protected String portCountryCode;
            @XmlAttribute(name = "DockIndicator")
            protected Boolean dockIndicator;
            @XmlAttribute(name = "ShorexIndicator")
            protected Boolean shorexIndicator;

            /**
             * Gets the value of the dateTimeDescription property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the dateTimeDescription property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getDateTimeDescription().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo.DateTimeDescription }
             * 
             * 
             */
            public List<OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo.DateTimeDescription> getDateTimeDescription() {
                if (dateTimeDescription == null) {
                    dateTimeDescription = new ArrayList<OTACruiseItineraryDescRS.CruiseItinInfos.CruiseItinInfo.DateTimeDescription>();
                }
                return this.dateTimeDescription;
            }

            /**
             * Obtiene el valor de la propiedad information.
             * 
             * @return
             *     possible object is
             *     {@link ParagraphType }
             *     
             */
            public ParagraphType getInformation() {
                return information;
            }

            /**
             * Define el valor de la propiedad information.
             * 
             * @param value
             *     allowed object is
             *     {@link ParagraphType }
             *     
             */
            public void setInformation(ParagraphType value) {
                this.information = value;
            }

            /**
             * Obtiene el valor de la propiedad portCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPortCode() {
                return portCode;
            }

            /**
             * Define el valor de la propiedad portCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPortCode(String value) {
                this.portCode = value;
            }

            /**
             * Obtiene el valor de la propiedad portName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPortName() {
                return portName;
            }

            /**
             * Define el valor de la propiedad portName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPortName(String value) {
                this.portName = value;
            }

            /**
             * Obtiene el valor de la propiedad portCountryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPortCountryCode() {
                return portCountryCode;
            }

            /**
             * Define el valor de la propiedad portCountryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPortCountryCode(String value) {
                this.portCountryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad dockIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isDockIndicator() {
                return dockIndicator;
            }

            /**
             * Define el valor de la propiedad dockIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setDockIndicator(Boolean value) {
                this.dockIndicator = value;
            }

            /**
             * Obtiene el valor de la propiedad shorexIndicator.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isShorexIndicator() {
                return shorexIndicator;
            }

            /**
             * Define el valor de la propiedad shorexIndicator.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setShorexIndicator(Boolean value) {
                this.shorexIndicator = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="DateTimeQualifier" use="required"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;enumeration value="arrival"/&amp;gt;
             *             &amp;lt;enumeration value="departure"/&amp;gt;
             *             &amp;lt;enumeration value="stay"/&amp;gt;
             *             &amp;lt;enumeration value="boarding"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="DateTimeDetails" use="required" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
             *       &amp;lt;attribute name="DayOfWeek" type="{http://www.opentravel.org/OTA/2003/05}DayOfWeekType" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class DateTimeDescription {

                @XmlAttribute(name = "DateTimeQualifier", required = true)
                protected String dateTimeQualifier;
                @XmlAttribute(name = "DateTimeDetails", required = true)
                protected String dateTimeDetails;
                @XmlAttribute(name = "DayOfWeek")
                protected DayOfWeekType dayOfWeek;

                /**
                 * Obtiene el valor de la propiedad dateTimeQualifier.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDateTimeQualifier() {
                    return dateTimeQualifier;
                }

                /**
                 * Define el valor de la propiedad dateTimeQualifier.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDateTimeQualifier(String value) {
                    this.dateTimeQualifier = value;
                }

                /**
                 * Obtiene el valor de la propiedad dateTimeDetails.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDateTimeDetails() {
                    return dateTimeDetails;
                }

                /**
                 * Define el valor de la propiedad dateTimeDetails.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDateTimeDetails(String value) {
                    this.dateTimeDetails = value;
                }

                /**
                 * Obtiene el valor de la propiedad dayOfWeek.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public DayOfWeekType getDayOfWeek() {
                    return dayOfWeek;
                }

                /**
                 * Define el valor de la propiedad dayOfWeek.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DayOfWeekType }
                 *     
                 */
                public void setDayOfWeek(DayOfWeekType value) {
                    this.dayOfWeek = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedSailingGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SelectedSailing {

        @XmlAttribute(name = "VoyageID")
        protected String voyageID;
        @XmlAttribute(name = "Start")
        protected String start;
        @XmlAttribute(name = "Duration")
        protected String duration;
        @XmlAttribute(name = "End")
        protected String end;
        @XmlAttribute(name = "VendorCode")
        protected String vendorCode;
        @XmlAttribute(name = "VendorName")
        protected String vendorName;
        @XmlAttribute(name = "ShipCode")
        protected String shipCode;
        @XmlAttribute(name = "ShipName")
        protected String shipName;
        @XmlAttribute(name = "VendorCodeContext")
        protected String vendorCodeContext;
        @XmlAttribute(name = "Status")
        protected String status;

        /**
         * Obtiene el valor de la propiedad voyageID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVoyageID() {
            return voyageID;
        }

        /**
         * Define el valor de la propiedad voyageID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVoyageID(String value) {
            this.voyageID = value;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStart(String value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad duration.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDuration() {
            return duration;
        }

        /**
         * Define el valor de la propiedad duration.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDuration(String value) {
            this.duration = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

        /**
         * Obtiene el valor de la propiedad vendorCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVendorCode() {
            return vendorCode;
        }

        /**
         * Define el valor de la propiedad vendorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVendorCode(String value) {
            this.vendorCode = value;
        }

        /**
         * Obtiene el valor de la propiedad vendorName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVendorName() {
            return vendorName;
        }

        /**
         * Define el valor de la propiedad vendorName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVendorName(String value) {
            this.vendorName = value;
        }

        /**
         * Obtiene el valor de la propiedad shipCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipCode() {
            return shipCode;
        }

        /**
         * Define el valor de la propiedad shipCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipCode(String value) {
            this.shipCode = value;
        }

        /**
         * Obtiene el valor de la propiedad shipName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipName() {
            return shipName;
        }

        /**
         * Define el valor de la propiedad shipName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipName(String value) {
            this.shipName = value;
        }

        /**
         * Obtiene el valor de la propiedad vendorCodeContext.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVendorCodeContext() {
            return vendorCodeContext;
        }

        /**
         * Define el valor de la propiedad vendorCodeContext.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVendorCodeContext(String value) {
            this.vendorCodeContext = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

    }

}
