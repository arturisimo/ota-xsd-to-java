
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Clase Java para CruiseReservationType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CruiseReservationType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="SailingInfo" type="{http://www.opentravel.org/OTA/2003/05}SailingCategoryInfoType"/&amp;gt;
 *         &amp;lt;element name="SailingProfile" type="{http://www.opentravel.org/OTA/2003/05}CruiseProfileType" maxOccurs="4" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReservationInfo" type="{http://www.opentravel.org/OTA/2003/05}CruiseGuestInfoType"/&amp;gt;
 *         &amp;lt;element name="PaymentsDue" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="PaymentDue" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
 *                           &amp;lt;attribute name="PaymentNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
 *                           &amp;lt;attribute name="DueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Information" type="{http://www.opentravel.org/OTA/2003/05}ParagraphType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CruiseReservationType", propOrder = {
    "sailingInfo",
    "sailingProfile",
    "reservationInfo",
    "paymentsDue",
    "information"
})
@XmlSeeAlso({
    OTACruisePNRUpdateNotifRQ.class
})
public class CruiseReservationType {

    @XmlElement(name = "SailingInfo", required = true)
    protected SailingCategoryInfoType sailingInfo;
    @XmlElement(name = "SailingProfile")
    protected List<CruiseProfileType> sailingProfile;
    @XmlElement(name = "ReservationInfo", required = true)
    protected CruiseGuestInfoType reservationInfo;
    @XmlElement(name = "PaymentsDue")
    protected CruiseReservationType.PaymentsDue paymentsDue;
    @XmlElement(name = "Information")
    protected List<ParagraphType> information;

    /**
     * Obtiene el valor de la propiedad sailingInfo.
     * 
     * @return
     *     possible object is
     *     {@link SailingCategoryInfoType }
     *     
     */
    public SailingCategoryInfoType getSailingInfo() {
        return sailingInfo;
    }

    /**
     * Define el valor de la propiedad sailingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SailingCategoryInfoType }
     *     
     */
    public void setSailingInfo(SailingCategoryInfoType value) {
        this.sailingInfo = value;
    }

    /**
     * Gets the value of the sailingProfile property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the sailingProfile property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSailingProfile().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CruiseProfileType }
     * 
     * 
     */
    public List<CruiseProfileType> getSailingProfile() {
        if (sailingProfile == null) {
            sailingProfile = new ArrayList<CruiseProfileType>();
        }
        return this.sailingProfile;
    }

    /**
     * Obtiene el valor de la propiedad reservationInfo.
     * 
     * @return
     *     possible object is
     *     {@link CruiseGuestInfoType }
     *     
     */
    public CruiseGuestInfoType getReservationInfo() {
        return reservationInfo;
    }

    /**
     * Define el valor de la propiedad reservationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseGuestInfoType }
     *     
     */
    public void setReservationInfo(CruiseGuestInfoType value) {
        this.reservationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentsDue.
     * 
     * @return
     *     possible object is
     *     {@link CruiseReservationType.PaymentsDue }
     *     
     */
    public CruiseReservationType.PaymentsDue getPaymentsDue() {
        return paymentsDue;
    }

    /**
     * Define el valor de la propiedad paymentsDue.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseReservationType.PaymentsDue }
     *     
     */
    public void setPaymentsDue(CruiseReservationType.PaymentsDue value) {
        this.paymentsDue = value;
    }

    /**
     * Gets the value of the information property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the information property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getInformation().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link ParagraphType }
     * 
     * 
     */
    public List<ParagraphType> getInformation() {
        if (information == null) {
            information = new ArrayList<ParagraphType>();
        }
        return this.information;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="PaymentDue" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
     *                 &amp;lt;attribute name="PaymentNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
     *                 &amp;lt;attribute name="DueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paymentDue"
    })
    public static class PaymentsDue {

        @XmlElement(name = "PaymentDue", required = true)
        protected List<CruiseReservationType.PaymentsDue.PaymentDue> paymentDue;

        /**
         * Gets the value of the paymentDue property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the paymentDue property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPaymentDue().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link CruiseReservationType.PaymentsDue.PaymentDue }
         * 
         * 
         */
        public List<CruiseReservationType.PaymentsDue.PaymentDue> getPaymentDue() {
            if (paymentDue == null) {
                paymentDue = new ArrayList<CruiseReservationType.PaymentsDue.PaymentDue>();
            }
            return this.paymentDue;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="Amount" type="{http://www.opentravel.org/OTA/2003/05}Money" /&amp;gt;
         *       &amp;lt;attribute name="PaymentNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to99" /&amp;gt;
         *       &amp;lt;attribute name="DueDate" type="{http://www.opentravel.org/OTA/2003/05}DateOrDateTimeType" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PaymentDue {

            @XmlAttribute(name = "Amount")
            protected BigDecimal amount;
            @XmlAttribute(name = "PaymentNumber")
            protected Integer paymentNumber;
            @XmlAttribute(name = "DueDate")
            protected String dueDate;

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentNumber.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getPaymentNumber() {
                return paymentNumber;
            }

            /**
             * Define el valor de la propiedad paymentNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setPaymentNumber(Integer value) {
                this.paymentNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad dueDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDueDate() {
                return dueDate;
            }

            /**
             * Define el valor de la propiedad dueDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDueDate(String value) {
                this.dueDate = value;
            }

        }

    }

}
