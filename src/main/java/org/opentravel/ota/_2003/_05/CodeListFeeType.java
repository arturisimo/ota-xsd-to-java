
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fees associated with a code item.
 * 
 * &lt;p&gt;Clase Java para CodeListFeeType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CodeListFeeType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Amount" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;choice&amp;gt;
 *                   &amp;lt;element name="Currency"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                           &amp;lt;attribute name="CurrencyCode"&amp;gt;
 *                             &amp;lt;simpleType&amp;gt;
 *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                 &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
 *                               &amp;lt;/restriction&amp;gt;
 *                             &amp;lt;/simpleType&amp;gt;
 *                           &amp;lt;/attribute&amp;gt;
 *                           &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="AlternateCurrency"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
 *                           &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                         &amp;lt;/extension&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Percent"&amp;gt;
 *                     &amp;lt;simpleType&amp;gt;
 *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&amp;gt;
 *                         &amp;lt;minInclusive value="0.00"/&amp;gt;
 *                         &amp;lt;maxInclusive value="100.00"/&amp;gt;
 *                       &amp;lt;/restriction&amp;gt;
 *                     &amp;lt;/simpleType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/choice&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_FeeTaxType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Qualifiers" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="ChargeUnit" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;simpleContent&amp;gt;
 *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
 *                                     &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/simpleContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;simpleContent&amp;gt;
 *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
 *                                     &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                     &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/simpleContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="EffectiveDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                 &amp;lt;attribute name="ExpireDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                 &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                 &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element name="Taxes" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="Amount" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;choice&amp;gt;
 *                             &amp;lt;element name="Currency"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
 *                                     &amp;lt;attribute name="CurrencyCode"&amp;gt;
 *                                       &amp;lt;simpleType&amp;gt;
 *                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                                           &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
 *                                         &amp;lt;/restriction&amp;gt;
 *                                       &amp;lt;/simpleType&amp;gt;
 *                                     &amp;lt;/attribute&amp;gt;
 *                                     &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="AlternateCurrency"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
 *                                     &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                   &amp;lt;/extension&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                             &amp;lt;element name="Percent"&amp;gt;
 *                               &amp;lt;simpleType&amp;gt;
 *                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&amp;gt;
 *                                   &amp;lt;minInclusive value="0.00"/&amp;gt;
 *                                   &amp;lt;maxInclusive value="100.00"/&amp;gt;
 *                                 &amp;lt;/restriction&amp;gt;
 *                               &amp;lt;/simpleType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/choice&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_FeeTaxType" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/&amp;gt;
 *                   &amp;lt;element name="Qualifiers" minOccurs="0"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;sequence&amp;gt;
 *                             &amp;lt;element name="ChargeUnit" minOccurs="0"&amp;gt;
 *                               &amp;lt;complexType&amp;gt;
 *                                 &amp;lt;complexContent&amp;gt;
 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                     &amp;lt;sequence&amp;gt;
 *                                       &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
 *                                               &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                       &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
 *                                         &amp;lt;complexType&amp;gt;
 *                                           &amp;lt;simpleContent&amp;gt;
 *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
 *                                               &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                               &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                                             &amp;lt;/extension&amp;gt;
 *                                           &amp;lt;/simpleContent&amp;gt;
 *                                         &amp;lt;/complexType&amp;gt;
 *                                       &amp;lt;/element&amp;gt;
 *                                     &amp;lt;/sequence&amp;gt;
 *                                   &amp;lt;/restriction&amp;gt;
 *                                 &amp;lt;/complexContent&amp;gt;
 *                               &amp;lt;/complexType&amp;gt;
 *                             &amp;lt;/element&amp;gt;
 *                           &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;attribute name="EffectiveDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                           &amp;lt;attribute name="ExpireDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                           &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                           &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;attribute name="MandatoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="TaxRPH"&amp;gt;
 *                   &amp;lt;simpleType&amp;gt;
 *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *                       &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *                     &amp;lt;/restriction&amp;gt;
 *                   &amp;lt;/simpleType&amp;gt;
 *                 &amp;lt;/attribute&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="MandatoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TaxInclusiveInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="TaxableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="DeterminationMethod"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="Cumulative"/&amp;gt;
 *             &amp;lt;enumeration value="Exclusive"/&amp;gt;
 *             &amp;lt;enumeration value="Inclusive"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="FeeRPH"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodeListFeeType", propOrder = {
    "amount",
    "type",
    "description",
    "qualifiers",
    "taxes"
})
public class CodeListFeeType {

    @XmlElement(name = "Amount")
    protected CodeListFeeType.Amount amount;
    @XmlElement(name = "Type")
    protected ListFeeTaxType type;
    @XmlElement(name = "Description")
    protected List<String> description;
    @XmlElement(name = "Qualifiers")
    protected CodeListFeeType.Qualifiers qualifiers;
    @XmlElement(name = "Taxes")
    protected List<CodeListFeeType.Taxes> taxes;
    @XmlAttribute(name = "MandatoryInd")
    protected Boolean mandatoryInd;
    @XmlAttribute(name = "TaxInclusiveInd")
    protected Boolean taxInclusiveInd;
    @XmlAttribute(name = "TaxableInd")
    protected Boolean taxableInd;
    @XmlAttribute(name = "DeterminationMethod")
    protected String determinationMethod;
    @XmlAttribute(name = "FeeRPH")
    protected String feeRPH;

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link CodeListFeeType.Amount }
     *     
     */
    public CodeListFeeType.Amount getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeListFeeType.Amount }
     *     
     */
    public void setAmount(CodeListFeeType.Amount value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link ListFeeTaxType }
     *     
     */
    public ListFeeTaxType getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link ListFeeTaxType }
     *     
     */
    public void setType(ListFeeTaxType value) {
        this.type = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the description property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getDescription().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDescription() {
        if (description == null) {
            description = new ArrayList<String>();
        }
        return this.description;
    }

    /**
     * Obtiene el valor de la propiedad qualifiers.
     * 
     * @return
     *     possible object is
     *     {@link CodeListFeeType.Qualifiers }
     *     
     */
    public CodeListFeeType.Qualifiers getQualifiers() {
        return qualifiers;
    }

    /**
     * Define el valor de la propiedad qualifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeListFeeType.Qualifiers }
     *     
     */
    public void setQualifiers(CodeListFeeType.Qualifiers value) {
        this.qualifiers = value;
    }

    /**
     * Gets the value of the taxes property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taxes property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTaxes().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link CodeListFeeType.Taxes }
     * 
     * 
     */
    public List<CodeListFeeType.Taxes> getTaxes() {
        if (taxes == null) {
            taxes = new ArrayList<CodeListFeeType.Taxes>();
        }
        return this.taxes;
    }

    /**
     * Obtiene el valor de la propiedad mandatoryInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMandatoryInd() {
        return mandatoryInd;
    }

    /**
     * Define el valor de la propiedad mandatoryInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMandatoryInd(Boolean value) {
        this.mandatoryInd = value;
    }

    /**
     * Obtiene el valor de la propiedad taxInclusiveInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxInclusiveInd() {
        return taxInclusiveInd;
    }

    /**
     * Define el valor de la propiedad taxInclusiveInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxInclusiveInd(Boolean value) {
        this.taxInclusiveInd = value;
    }

    /**
     * Obtiene el valor de la propiedad taxableInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxableInd() {
        return taxableInd;
    }

    /**
     * Define el valor de la propiedad taxableInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxableInd(Boolean value) {
        this.taxableInd = value;
    }

    /**
     * Obtiene el valor de la propiedad determinationMethod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeterminationMethod() {
        return determinationMethod;
    }

    /**
     * Define el valor de la propiedad determinationMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeterminationMethod(String value) {
        this.determinationMethod = value;
    }

    /**
     * Obtiene el valor de la propiedad feeRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeRPH() {
        return feeRPH;
    }

    /**
     * Define el valor de la propiedad feeRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeRPH(String value) {
        this.feeRPH = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;choice&amp;gt;
     *         &amp;lt;element name="Currency"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                 &amp;lt;attribute name="CurrencyCode"&amp;gt;
     *                   &amp;lt;simpleType&amp;gt;
     *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                       &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
     *                     &amp;lt;/restriction&amp;gt;
     *                   &amp;lt;/simpleType&amp;gt;
     *                 &amp;lt;/attribute&amp;gt;
     *                 &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="AlternateCurrency"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
     *                 &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Percent"&amp;gt;
     *           &amp;lt;simpleType&amp;gt;
     *             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&amp;gt;
     *               &amp;lt;minInclusive value="0.00"/&amp;gt;
     *               &amp;lt;maxInclusive value="100.00"/&amp;gt;
     *             &amp;lt;/restriction&amp;gt;
     *           &amp;lt;/simpleType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/choice&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "currency",
        "alternateCurrency",
        "percent"
    })
    public static class Amount {

        @XmlElement(name = "Currency")
        protected CodeListFeeType.Amount.Currency currency;
        @XmlElement(name = "AlternateCurrency")
        protected CodeListFeeType.Amount.AlternateCurrency alternateCurrency;
        @XmlElement(name = "Percent")
        protected BigDecimal percent;

        /**
         * Obtiene el valor de la propiedad currency.
         * 
         * @return
         *     possible object is
         *     {@link CodeListFeeType.Amount.Currency }
         *     
         */
        public CodeListFeeType.Amount.Currency getCurrency() {
            return currency;
        }

        /**
         * Define el valor de la propiedad currency.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeListFeeType.Amount.Currency }
         *     
         */
        public void setCurrency(CodeListFeeType.Amount.Currency value) {
            this.currency = value;
        }

        /**
         * Obtiene el valor de la propiedad alternateCurrency.
         * 
         * @return
         *     possible object is
         *     {@link CodeListFeeType.Amount.AlternateCurrency }
         *     
         */
        public CodeListFeeType.Amount.AlternateCurrency getAlternateCurrency() {
            return alternateCurrency;
        }

        /**
         * Define el valor de la propiedad alternateCurrency.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeListFeeType.Amount.AlternateCurrency }
         *     
         */
        public void setAlternateCurrency(CodeListFeeType.Amount.AlternateCurrency value) {
            this.alternateCurrency = value;
        }

        /**
         * Obtiene el valor de la propiedad percent.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPercent() {
            return percent;
        }

        /**
         * Define el valor de la propiedad percent.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPercent(BigDecimal value) {
            this.percent = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
         *       &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AlternateCurrency
            extends ListLoyaltyPrgCurrency
        {

            @XmlAttribute(name = "Quantity", required = true)
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger quantity;

            /**
             * Obtiene el valor de la propiedad quantity.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getQuantity() {
                return quantity;
            }

            /**
             * Define el valor de la propiedad quantity.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setQuantity(BigInteger value) {
                this.quantity = value;
            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *       &amp;lt;attribute name="CurrencyCode"&amp;gt;
         *         &amp;lt;simpleType&amp;gt;
         *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *             &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
         *           &amp;lt;/restriction&amp;gt;
         *         &amp;lt;/simpleType&amp;gt;
         *       &amp;lt;/attribute&amp;gt;
         *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Currency {

            @XmlAttribute(name = "Amount", required = true)
            protected BigDecimal amount;
            @XmlAttribute(name = "CurrencyCode")
            protected String currencyCode;
            @XmlAttribute(name = "DecimalPlaces")
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger decimalPlaces;

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAmount(BigDecimal value) {
                this.amount = value;
            }

            /**
             * Obtiene el valor de la propiedad currencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Define el valor de la propiedad currencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad decimalPlaces.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDecimalPlaces() {
                return decimalPlaces;
            }

            /**
             * Define el valor de la propiedad decimalPlaces.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDecimalPlaces(BigInteger value) {
                this.decimalPlaces = value;
            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="ChargeUnit" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
     *                           &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;simpleContent&amp;gt;
     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
     *                           &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                           &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/simpleContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="EffectiveDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *       &amp;lt;attribute name="ExpireDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *       &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *       &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chargeUnit"
    })
    public static class Qualifiers {

        @XmlElement(name = "ChargeUnit")
        protected CodeListFeeType.Qualifiers.ChargeUnit chargeUnit;
        @XmlAttribute(name = "EffectiveDateTime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar effectiveDateTime;
        @XmlAttribute(name = "ExpireDateTime")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar expireDateTime;
        @XmlAttribute(name = "MaxAge")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxAge;
        @XmlAttribute(name = "MinAge")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger minAge;

        /**
         * Obtiene el valor de la propiedad chargeUnit.
         * 
         * @return
         *     possible object is
         *     {@link CodeListFeeType.Qualifiers.ChargeUnit }
         *     
         */
        public CodeListFeeType.Qualifiers.ChargeUnit getChargeUnit() {
            return chargeUnit;
        }

        /**
         * Define el valor de la propiedad chargeUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeListFeeType.Qualifiers.ChargeUnit }
         *     
         */
        public void setChargeUnit(CodeListFeeType.Qualifiers.ChargeUnit value) {
            this.chargeUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveDateTime.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEffectiveDateTime() {
            return effectiveDateTime;
        }

        /**
         * Define el valor de la propiedad effectiveDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEffectiveDateTime(XMLGregorianCalendar value) {
            this.effectiveDateTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expireDateTime.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpireDateTime() {
            return expireDateTime;
        }

        /**
         * Define el valor de la propiedad expireDateTime.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpireDateTime(XMLGregorianCalendar value) {
            this.expireDateTime = value;
        }

        /**
         * Obtiene el valor de la propiedad maxAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxAge() {
            return maxAge;
        }

        /**
         * Define el valor de la propiedad maxAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxAge(BigInteger value) {
            this.maxAge = value;
        }

        /**
         * Obtiene el valor de la propiedad minAge.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMinAge() {
            return minAge;
        }

        /**
         * Define el valor de la propiedad minAge.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMinAge(BigInteger value) {
            this.minAge = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
         *                 &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;simpleContent&amp;gt;
         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
         *                 &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                 &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/simpleContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "unit",
            "frequency"
        })
        public static class ChargeUnit {

            @XmlElement(name = "Unit")
            protected CodeListFeeType.Qualifiers.ChargeUnit.Unit unit;
            @XmlElement(name = "Frequency")
            protected CodeListFeeType.Qualifiers.ChargeUnit.Frequency frequency;

            /**
             * Obtiene el valor de la propiedad unit.
             * 
             * @return
             *     possible object is
             *     {@link CodeListFeeType.Qualifiers.ChargeUnit.Unit }
             *     
             */
            public CodeListFeeType.Qualifiers.ChargeUnit.Unit getUnit() {
                return unit;
            }

            /**
             * Define el valor de la propiedad unit.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeListFeeType.Qualifiers.ChargeUnit.Unit }
             *     
             */
            public void setUnit(CodeListFeeType.Qualifiers.ChargeUnit.Unit value) {
                this.unit = value;
            }

            /**
             * Obtiene el valor de la propiedad frequency.
             * 
             * @return
             *     possible object is
             *     {@link CodeListFeeType.Qualifiers.ChargeUnit.Frequency }
             *     
             */
            public CodeListFeeType.Qualifiers.ChargeUnit.Frequency getFrequency() {
                return frequency;
            }

            /**
             * Define el valor de la propiedad frequency.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeListFeeType.Qualifiers.ChargeUnit.Frequency }
             *     
             */
            public void setFrequency(CodeListFeeType.Qualifiers.ChargeUnit.Frequency value) {
                this.frequency = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
             *       &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Frequency
                extends ListChargeFrequency
            {

                @XmlAttribute(name = "ExemptQty")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger exemptQty;
                @XmlAttribute(name = "MaximumQty")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maximumQty;

                /**
                 * Obtiene el valor de la propiedad exemptQty.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getExemptQty() {
                    return exemptQty;
                }

                /**
                 * Define el valor de la propiedad exemptQty.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setExemptQty(BigInteger value) {
                    this.exemptQty = value;
                }

                /**
                 * Obtiene el valor de la propiedad maximumQty.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaximumQty() {
                    return maximumQty;
                }

                /**
                 * Define el valor de la propiedad maximumQty.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaximumQty(BigInteger value) {
                    this.maximumQty = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;simpleContent&amp;gt;
             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
             *       &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *       &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/simpleContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Unit
                extends ListChargeUnit
            {

                @XmlAttribute(name = "ExemptQty")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger exemptQty;
                @XmlAttribute(name = "MaximumQty")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger maximumQty;

                /**
                 * Obtiene el valor de la propiedad exemptQty.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getExemptQty() {
                    return exemptQty;
                }

                /**
                 * Define el valor de la propiedad exemptQty.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setExemptQty(BigInteger value) {
                    this.exemptQty = value;
                }

                /**
                 * Obtiene el valor de la propiedad maximumQty.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMaximumQty() {
                    return maximumQty;
                }

                /**
                 * Define el valor de la propiedad maximumQty.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMaximumQty(BigInteger value) {
                    this.maximumQty = value;
                }

            }

        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="Amount" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;choice&amp;gt;
     *                   &amp;lt;element name="Currency"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
     *                           &amp;lt;attribute name="CurrencyCode"&amp;gt;
     *                             &amp;lt;simpleType&amp;gt;
     *                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *                                 &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
     *                               &amp;lt;/restriction&amp;gt;
     *                             &amp;lt;/simpleType&amp;gt;
     *                           &amp;lt;/attribute&amp;gt;
     *                           &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="AlternateCurrency"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
     *                           &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="Percent"&amp;gt;
     *                     &amp;lt;simpleType&amp;gt;
     *                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&amp;gt;
     *                         &amp;lt;minInclusive value="0.00"/&amp;gt;
     *                         &amp;lt;maxInclusive value="100.00"/&amp;gt;
     *                       &amp;lt;/restriction&amp;gt;
     *                     &amp;lt;/simpleType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/choice&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="Type" type="{http://www.opentravel.org/OTA/2003/05}List_FeeTaxType" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/&amp;gt;
     *         &amp;lt;element name="Qualifiers" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="ChargeUnit" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
     *                                     &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                             &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;simpleContent&amp;gt;
     *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
     *                                     &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                     &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/simpleContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="EffectiveDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="ExpireDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                 &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *                 &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *       &amp;lt;attribute name="MandatoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="TaxRPH"&amp;gt;
     *         &amp;lt;simpleType&amp;gt;
     *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
     *             &amp;lt;pattern value="[0-9]{1,8}"/&amp;gt;
     *           &amp;lt;/restriction&amp;gt;
     *         &amp;lt;/simpleType&amp;gt;
     *       &amp;lt;/attribute&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amount",
        "type",
        "description",
        "qualifiers"
    })
    public static class Taxes {

        @XmlElement(name = "Amount")
        protected CodeListFeeType.Taxes.Amount amount;
        @XmlElement(name = "Type")
        protected ListFeeTaxType type;
        @XmlElement(name = "Description")
        protected List<String> description;
        @XmlElement(name = "Qualifiers")
        protected CodeListFeeType.Taxes.Qualifiers qualifiers;
        @XmlAttribute(name = "MandatoryInd")
        protected Boolean mandatoryInd;
        @XmlAttribute(name = "TaxRPH")
        protected String taxRPH;

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link CodeListFeeType.Taxes.Amount }
         *     
         */
        public CodeListFeeType.Taxes.Amount getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeListFeeType.Taxes.Amount }
         *     
         */
        public void setAmount(CodeListFeeType.Taxes.Amount value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link ListFeeTaxType }
         *     
         */
        public ListFeeTaxType getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link ListFeeTaxType }
         *     
         */
        public void setType(ListFeeTaxType value) {
            this.type = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the description property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getDescription().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDescription() {
            if (description == null) {
                description = new ArrayList<String>();
            }
            return this.description;
        }

        /**
         * Obtiene el valor de la propiedad qualifiers.
         * 
         * @return
         *     possible object is
         *     {@link CodeListFeeType.Taxes.Qualifiers }
         *     
         */
        public CodeListFeeType.Taxes.Qualifiers getQualifiers() {
            return qualifiers;
        }

        /**
         * Define el valor de la propiedad qualifiers.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeListFeeType.Taxes.Qualifiers }
         *     
         */
        public void setQualifiers(CodeListFeeType.Taxes.Qualifiers value) {
            this.qualifiers = value;
        }

        /**
         * Obtiene el valor de la propiedad mandatoryInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMandatoryInd() {
            return mandatoryInd;
        }

        /**
         * Define el valor de la propiedad mandatoryInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMandatoryInd(Boolean value) {
            this.mandatoryInd = value;
        }

        /**
         * Obtiene el valor de la propiedad taxRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxRPH() {
            return taxRPH;
        }

        /**
         * Define el valor de la propiedad taxRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxRPH(String value) {
            this.taxRPH = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;choice&amp;gt;
         *         &amp;lt;element name="Currency"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
         *                 &amp;lt;attribute name="CurrencyCode"&amp;gt;
         *                   &amp;lt;simpleType&amp;gt;
         *                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
         *                       &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
         *                     &amp;lt;/restriction&amp;gt;
         *                   &amp;lt;/simpleType&amp;gt;
         *                 &amp;lt;/attribute&amp;gt;
         *                 &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="AlternateCurrency"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
         *                 &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="Percent"&amp;gt;
         *           &amp;lt;simpleType&amp;gt;
         *             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&amp;gt;
         *               &amp;lt;minInclusive value="0.00"/&amp;gt;
         *               &amp;lt;maxInclusive value="100.00"/&amp;gt;
         *             &amp;lt;/restriction&amp;gt;
         *           &amp;lt;/simpleType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/choice&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "currency",
            "alternateCurrency",
            "percent"
        })
        public static class Amount {

            @XmlElement(name = "Currency")
            protected CodeListFeeType.Taxes.Amount.Currency currency;
            @XmlElement(name = "AlternateCurrency")
            protected CodeListFeeType.Taxes.Amount.AlternateCurrency alternateCurrency;
            @XmlElement(name = "Percent")
            protected BigDecimal percent;

            /**
             * Obtiene el valor de la propiedad currency.
             * 
             * @return
             *     possible object is
             *     {@link CodeListFeeType.Taxes.Amount.Currency }
             *     
             */
            public CodeListFeeType.Taxes.Amount.Currency getCurrency() {
                return currency;
            }

            /**
             * Define el valor de la propiedad currency.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeListFeeType.Taxes.Amount.Currency }
             *     
             */
            public void setCurrency(CodeListFeeType.Taxes.Amount.Currency value) {
                this.currency = value;
            }

            /**
             * Obtiene el valor de la propiedad alternateCurrency.
             * 
             * @return
             *     possible object is
             *     {@link CodeListFeeType.Taxes.Amount.AlternateCurrency }
             *     
             */
            public CodeListFeeType.Taxes.Amount.AlternateCurrency getAlternateCurrency() {
                return alternateCurrency;
            }

            /**
             * Define el valor de la propiedad alternateCurrency.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeListFeeType.Taxes.Amount.AlternateCurrency }
             *     
             */
            public void setAlternateCurrency(CodeListFeeType.Taxes.Amount.AlternateCurrency value) {
                this.alternateCurrency = value;
            }

            /**
             * Obtiene el valor de la propiedad percent.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPercent() {
                return percent;
            }

            /**
             * Define el valor de la propiedad percent.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPercent(BigDecimal value) {
                this.percent = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}List_LoyaltyPrgCurrency"&amp;gt;
             *       &amp;lt;attribute name="Quantity" use="required" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AlternateCurrency
                extends ListLoyaltyPrgCurrency
            {

                @XmlAttribute(name = "Quantity", required = true)
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger quantity;

                /**
                 * Obtiene el valor de la propiedad quantity.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getQuantity() {
                    return quantity;
                }

                /**
                 * Define el valor de la propiedad quantity.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setQuantity(BigInteger value) {
                    this.quantity = value;
                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;attribute name="Amount" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&amp;gt;
             *       &amp;lt;attribute name="CurrencyCode"&amp;gt;
             *         &amp;lt;simpleType&amp;gt;
             *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
             *             &amp;lt;pattern value="[a-zA-Z]{3}"/&amp;gt;
             *           &amp;lt;/restriction&amp;gt;
             *         &amp;lt;/simpleType&amp;gt;
             *       &amp;lt;/attribute&amp;gt;
             *       &amp;lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Currency {

                @XmlAttribute(name = "Amount", required = true)
                protected BigDecimal amount;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;
                @XmlAttribute(name = "DecimalPlaces")
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger decimalPlaces;

                /**
                 * Obtiene el valor de la propiedad amount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getAmount() {
                    return amount;
                }

                /**
                 * Define el valor de la propiedad amount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setAmount(BigDecimal value) {
                    this.amount = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Define el valor de la propiedad currencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad decimalPlaces.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDecimalPlaces() {
                    return decimalPlaces;
                }

                /**
                 * Define el valor de la propiedad decimalPlaces.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDecimalPlaces(BigInteger value) {
                    this.decimalPlaces = value;
                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="ChargeUnit" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
         *                           &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                   &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;simpleContent&amp;gt;
         *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
         *                           &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                           &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/simpleContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="EffectiveDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="ExpireDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *       &amp;lt;attribute name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *       &amp;lt;attribute name="MinAge" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "chargeUnit"
        })
        public static class Qualifiers {

            @XmlElement(name = "ChargeUnit")
            protected CodeListFeeType.Taxes.Qualifiers.ChargeUnit chargeUnit;
            @XmlAttribute(name = "EffectiveDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar effectiveDateTime;
            @XmlAttribute(name = "ExpireDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar expireDateTime;
            @XmlAttribute(name = "MaxAge")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger maxAge;
            @XmlAttribute(name = "MinAge")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger minAge;

            /**
             * Obtiene el valor de la propiedad chargeUnit.
             * 
             * @return
             *     possible object is
             *     {@link CodeListFeeType.Taxes.Qualifiers.ChargeUnit }
             *     
             */
            public CodeListFeeType.Taxes.Qualifiers.ChargeUnit getChargeUnit() {
                return chargeUnit;
            }

            /**
             * Define el valor de la propiedad chargeUnit.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeListFeeType.Taxes.Qualifiers.ChargeUnit }
             *     
             */
            public void setChargeUnit(CodeListFeeType.Taxes.Qualifiers.ChargeUnit value) {
                this.chargeUnit = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEffectiveDateTime() {
                return effectiveDateTime;
            }

            /**
             * Define el valor de la propiedad effectiveDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEffectiveDateTime(XMLGregorianCalendar value) {
                this.effectiveDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expireDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpireDateTime() {
                return expireDateTime;
            }

            /**
             * Define el valor de la propiedad expireDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpireDateTime(XMLGregorianCalendar value) {
                this.expireDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad maxAge.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxAge() {
                return maxAge;
            }

            /**
             * Define el valor de la propiedad maxAge.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxAge(BigInteger value) {
                this.maxAge = value;
            }

            /**
             * Obtiene el valor de la propiedad minAge.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMinAge() {
                return minAge;
            }

            /**
             * Define el valor de la propiedad minAge.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMinAge(BigInteger value) {
                this.minAge = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="Unit" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
             *                 &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *         &amp;lt;element name="Frequency" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;simpleContent&amp;gt;
             *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
             *                 &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *                 &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/simpleContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "unit",
                "frequency"
            })
            public static class ChargeUnit {

                @XmlElement(name = "Unit")
                protected CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Unit unit;
                @XmlElement(name = "Frequency")
                protected CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Frequency frequency;

                /**
                 * Obtiene el valor de la propiedad unit.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Unit }
                 *     
                 */
                public CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Unit getUnit() {
                    return unit;
                }

                /**
                 * Define el valor de la propiedad unit.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Unit }
                 *     
                 */
                public void setUnit(CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Unit value) {
                    this.unit = value;
                }

                /**
                 * Obtiene el valor de la propiedad frequency.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Frequency }
                 *     
                 */
                public CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Frequency getFrequency() {
                    return frequency;
                }

                /**
                 * Define el valor de la propiedad frequency.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Frequency }
                 *     
                 */
                public void setFrequency(CodeListFeeType.Taxes.Qualifiers.ChargeUnit.Frequency value) {
                    this.frequency = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeFrequency"&amp;gt;
                 *       &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Frequency
                    extends ListChargeFrequency
                {

                    @XmlAttribute(name = "ExemptQty")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger exemptQty;
                    @XmlAttribute(name = "MaximumQty")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger maximumQty;

                    /**
                     * Obtiene el valor de la propiedad exemptQty.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getExemptQty() {
                        return exemptQty;
                    }

                    /**
                     * Define el valor de la propiedad exemptQty.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setExemptQty(BigInteger value) {
                        this.exemptQty = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad maximumQty.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMaximumQty() {
                        return maximumQty;
                    }

                    /**
                     * Define el valor de la propiedad maximumQty.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMaximumQty(BigInteger value) {
                        this.maximumQty = value;
                    }

                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;simpleContent&amp;gt;
                 *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;List_ChargeUnit"&amp;gt;
                 *       &amp;lt;attribute name="ExemptQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *       &amp;lt;attribute name="MaximumQty" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/simpleContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Unit
                    extends ListChargeUnit
                {

                    @XmlAttribute(name = "ExemptQty")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger exemptQty;
                    @XmlAttribute(name = "MaximumQty")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger maximumQty;

                    /**
                     * Obtiene el valor de la propiedad exemptQty.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getExemptQty() {
                        return exemptQty;
                    }

                    /**
                     * Define el valor de la propiedad exemptQty.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setExemptQty(BigInteger value) {
                        this.exemptQty = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad maximumQty.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMaximumQty() {
                        return maximumQty;
                    }

                    /**
                     * Define el valor de la propiedad maximumQty.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMaximumQty(BigInteger value) {
                        this.maximumQty = value;
                    }

                }

            }

        }

    }

}
