
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A collection of rental car search criteria.
 * 
 * &lt;p&gt;Clase Java para DynamicPkgCarSearchType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="DynamicPkgCarSearchType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgSearchType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="VehAvailRQCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailRQCoreType"/&amp;gt;
 *         &amp;lt;element name="VehAvailRQInfo" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailRQAdditionalInfoType"/&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="RequestType" type="{http://www.opentravel.org/OTA/2003/05}CarComponentSearchType" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicPkgCarSearchType", propOrder = {
    "vehAvailRQCore",
    "vehAvailRQInfo",
    "tpaExtensions"
})
public class DynamicPkgCarSearchType
    extends DynamicPkgSearchType
{

    @XmlElement(name = "VehAvailRQCore", required = true)
    protected VehicleAvailRQCoreType vehAvailRQCore;
    @XmlElement(name = "VehAvailRQInfo", required = true)
    protected VehicleAvailRQAdditionalInfoType vehAvailRQInfo;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "RequestType")
    protected CarComponentSearchType requestType;

    /**
     * Obtiene el valor de la propiedad vehAvailRQCore.
     * 
     * @return
     *     possible object is
     *     {@link VehicleAvailRQCoreType }
     *     
     */
    public VehicleAvailRQCoreType getVehAvailRQCore() {
        return vehAvailRQCore;
    }

    /**
     * Define el valor de la propiedad vehAvailRQCore.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleAvailRQCoreType }
     *     
     */
    public void setVehAvailRQCore(VehicleAvailRQCoreType value) {
        this.vehAvailRQCore = value;
    }

    /**
     * Obtiene el valor de la propiedad vehAvailRQInfo.
     * 
     * @return
     *     possible object is
     *     {@link VehicleAvailRQAdditionalInfoType }
     *     
     */
    public VehicleAvailRQAdditionalInfoType getVehAvailRQInfo() {
        return vehAvailRQInfo;
    }

    /**
     * Define el valor de la propiedad vehAvailRQInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleAvailRQAdditionalInfoType }
     *     
     */
    public void setVehAvailRQInfo(VehicleAvailRQAdditionalInfoType value) {
        this.vehAvailRQInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad requestType.
     * 
     * @return
     *     possible object is
     *     {@link CarComponentSearchType }
     *     
     */
    public CarComponentSearchType getRequestType() {
        return requestType;
    }

    /**
     * Define el valor de la propiedad requestType.
     * 
     * @param value
     *     allowed object is
     *     {@link CarComponentSearchType }
     *     
     */
    public void setRequestType(CarComponentSearchType value) {
        this.requestType = value;
    }

}
