
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Indicates the specific traveler, itinerary, paid origin/destination or flight segment a priced item applies to.
 * 
 * &lt;p&gt;Clase Java para ApplyPriceToType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="ApplyPriceToType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="SeatRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *       &amp;lt;attribute name="OtherServiceRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplyPriceToType")
public class ApplyPriceToType {

    @XmlAttribute(name = "SeatRPH")
    protected String seatRPH;
    @XmlAttribute(name = "OtherServiceRPH")
    protected String otherServiceRPH;

    /**
     * Obtiene el valor de la propiedad seatRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatRPH() {
        return seatRPH;
    }

    /**
     * Define el valor de la propiedad seatRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatRPH(String value) {
        this.seatRPH = value;
    }

    /**
     * Obtiene el valor de la propiedad otherServiceRPH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherServiceRPH() {
        return otherServiceRPH;
    }

    /**
     * Define el valor de la propiedad otherServiceRPH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherServiceRPH(String value) {
        this.otherServiceRPH = value;
    }

}
