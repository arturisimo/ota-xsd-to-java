
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Point of Sale (POS) identifies the party or connection channel making the request.
 * 
 * &lt;p&gt;Clase Java para POS_Type complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="POS_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Source" type="{http://www.opentravel.org/OTA/2003/05}SourceType" maxOccurs="10"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POS_Type", propOrder = {
    "source"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTAAirBaggageRQ.POS.class,
    org.opentravel.ota._2003._05.OTAAirGetOfferRQ.POS.class,
    org.opentravel.ota._2003._05.OTAGolfCourseAvailRQ.POS.class,
    org.opentravel.ota._2003._05.OTAGolfCourseResModifyRQ.POS.class,
    org.opentravel.ota._2003._05.OTAGolfCourseResRQ.POS.class,
    org.opentravel.ota._2003._05.OTAGolfCourseSearchRQ.POS.class,
    org.opentravel.ota._2003._05.OTAGolfFacilityInfoRQ.POS.class
})
public class POSType {

    @XmlElement(name = "Source", required = true)
    protected List<SourceType> source;

    /**
     * Gets the value of the source property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the source property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSource().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link SourceType }
     * 
     * 
     */
    public List<SourceType> getSource() {
        if (source == null) {
            source = new ArrayList<SourceType>();
        }
        return this.source;
    }

}
