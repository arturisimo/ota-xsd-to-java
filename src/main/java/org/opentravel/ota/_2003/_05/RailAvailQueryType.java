
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines criteria for a rail availability query.
 * 
 * &lt;p&gt;Clase Java para RailAvailQueryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="RailAvailQueryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="AvailBaseQueryCriteria" type="{http://www.opentravel.org/OTA/2003/05}RailAvailScheduleQueryType" maxOccurs="99"/&amp;gt;
 *         &amp;lt;element name="PassengerType" type="{http://www.opentravel.org/OTA/2003/05}RailPassengerCategoryType" maxOccurs="5" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="ReturnInfo" type="{http://www.opentravel.org/OTA/2003/05}TravelDateTimeType" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="RailPrefs" type="{http://www.opentravel.org/OTA/2003/05}RailAvailPrefsType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailAvailQueryType", propOrder = {
    "availBaseQueryCriteria",
    "passengerType",
    "returnInfo",
    "railPrefs"
})
@XmlSeeAlso({
    org.opentravel.ota._2003._05.OTARailAvailRQ.RailAvailQuery.class
})
public class RailAvailQueryType {

    @XmlElement(name = "AvailBaseQueryCriteria", required = true)
    protected List<RailAvailScheduleQueryType> availBaseQueryCriteria;
    @XmlElement(name = "PassengerType")
    protected List<RailPassengerCategoryType> passengerType;
    @XmlElement(name = "ReturnInfo")
    protected TravelDateTimeType returnInfo;
    @XmlElement(name = "RailPrefs")
    protected RailAvailPrefsType railPrefs;

    /**
     * Gets the value of the availBaseQueryCriteria property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the availBaseQueryCriteria property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getAvailBaseQueryCriteria().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RailAvailScheduleQueryType }
     * 
     * 
     */
    public List<RailAvailScheduleQueryType> getAvailBaseQueryCriteria() {
        if (availBaseQueryCriteria == null) {
            availBaseQueryCriteria = new ArrayList<RailAvailScheduleQueryType>();
        }
        return this.availBaseQueryCriteria;
    }

    /**
     * Gets the value of the passengerType property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the passengerType property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getPassengerType().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link RailPassengerCategoryType }
     * 
     * 
     */
    public List<RailPassengerCategoryType> getPassengerType() {
        if (passengerType == null) {
            passengerType = new ArrayList<RailPassengerCategoryType>();
        }
        return this.passengerType;
    }

    /**
     * Obtiene el valor de la propiedad returnInfo.
     * 
     * @return
     *     possible object is
     *     {@link TravelDateTimeType }
     *     
     */
    public TravelDateTimeType getReturnInfo() {
        return returnInfo;
    }

    /**
     * Define el valor de la propiedad returnInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelDateTimeType }
     *     
     */
    public void setReturnInfo(TravelDateTimeType value) {
        this.returnInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad railPrefs.
     * 
     * @return
     *     possible object is
     *     {@link RailAvailPrefsType }
     *     
     */
    public RailAvailPrefsType getRailPrefs() {
        return railPrefs;
    }

    /**
     * Define el valor de la propiedad railPrefs.
     * 
     * @param value
     *     allowed object is
     *     {@link RailAvailPrefsType }
     *     
     */
    public void setRailPrefs(RailAvailPrefsType value) {
        this.railPrefs = value;
    }

}
