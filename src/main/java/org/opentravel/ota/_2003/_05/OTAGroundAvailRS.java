
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;choice&amp;gt;
 *         &amp;lt;sequence&amp;gt;
 *           &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *           &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *           &amp;lt;element name="GroundServices" minOccurs="0"&amp;gt;
 *             &amp;lt;complexType&amp;gt;
 *               &amp;lt;complexContent&amp;gt;
 *                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                   &amp;lt;sequence&amp;gt;
 *                     &amp;lt;element name="GroundService" maxOccurs="unbounded"&amp;gt;
 *                       &amp;lt;complexType&amp;gt;
 *                         &amp;lt;complexContent&amp;gt;
 *                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                             &amp;lt;sequence&amp;gt;
 *                               &amp;lt;choice&amp;gt;
 *                                 &amp;lt;element name="Service" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundServiceDetailType"&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                                 &amp;lt;element name="Shuttle" type="{http://www.opentravel.org/OTA/2003/05}GroundShuttleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/choice&amp;gt;
 *                               &amp;lt;element name="RateQualifier" type="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Promotions" type="{http://www.opentravel.org/OTA/2003/05}GroundPromotionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Restrictions" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
 *                                                 &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                       &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                       &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                     &amp;lt;/restriction&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                               &amp;lt;element name="ServiceCharges" type="{http://www.opentravel.org/OTA/2003/05}GroundServiceChargesType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}GroundFeesType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="TotalCharge" type="{http://www.opentravel.org/OTA/2003/05}GroundTotalChargeType" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="Reference" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
 *                               &amp;lt;element name="PaymentRules" minOccurs="0"&amp;gt;
 *                                 &amp;lt;complexType&amp;gt;
 *                                   &amp;lt;complexContent&amp;gt;
 *                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentRulesType"&amp;gt;
 *                                       &amp;lt;sequence&amp;gt;
 *                                         &amp;lt;element name="AcceptablePayments" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                                           &amp;lt;complexType&amp;gt;
 *                                             &amp;lt;complexContent&amp;gt;
 *                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                 &amp;lt;sequence&amp;gt;
 *                                                   &amp;lt;element name="AcceptablePayment" maxOccurs="unbounded"&amp;gt;
 *                                                     &amp;lt;complexType&amp;gt;
 *                                                       &amp;lt;complexContent&amp;gt;
 *                                                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                           &amp;lt;attribute name="CreditCardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
 *                                                         &amp;lt;/restriction&amp;gt;
 *                                                       &amp;lt;/complexContent&amp;gt;
 *                                                     &amp;lt;/complexType&amp;gt;
 *                                                   &amp;lt;/element&amp;gt;
 *                                                 &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;attribute name="PaymentTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                               &amp;lt;/restriction&amp;gt;
 *                                             &amp;lt;/complexContent&amp;gt;
 *                                           &amp;lt;/complexType&amp;gt;
 *                                         &amp;lt;/element&amp;gt;
 *                                       &amp;lt;/sequence&amp;gt;
 *                                     &amp;lt;/extension&amp;gt;
 *                                   &amp;lt;/complexContent&amp;gt;
 *                                 &amp;lt;/complexType&amp;gt;
 *                               &amp;lt;/element&amp;gt;
 *                             &amp;lt;/sequence&amp;gt;
 *                           &amp;lt;/restriction&amp;gt;
 *                         &amp;lt;/complexContent&amp;gt;
 *                       &amp;lt;/complexType&amp;gt;
 *                     &amp;lt;/element&amp;gt;
 *                   &amp;lt;/sequence&amp;gt;
 *                 &amp;lt;/restriction&amp;gt;
 *               &amp;lt;/complexContent&amp;gt;
 *             &amp;lt;/complexType&amp;gt;
 *           &amp;lt;/element&amp;gt;
 *           &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *         &amp;lt;/sequence&amp;gt;
 *         &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *       &amp;lt;/choice&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "warnings",
    "groundServices",
    "tpaExtensions",
    "errors"
})
@XmlRootElement(name = "OTA_GroundAvailRS")
public class OTAGroundAvailRS {

    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "GroundServices")
    protected OTAGroundAvailRS.GroundServices groundServices;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad groundServices.
     * 
     * @return
     *     possible object is
     *     {@link OTAGroundAvailRS.GroundServices }
     *     
     */
    public OTAGroundAvailRS.GroundServices getGroundServices() {
        return groundServices;
    }

    /**
     * Define el valor de la propiedad groundServices.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAGroundAvailRS.GroundServices }
     *     
     */
    public void setGroundServices(OTAGroundAvailRS.GroundServices value) {
        this.groundServices = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="GroundService" maxOccurs="unbounded"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;choice&amp;gt;
     *                     &amp;lt;element name="Service" minOccurs="0"&amp;gt;
     *                       &amp;lt;complexType&amp;gt;
     *                         &amp;lt;complexContent&amp;gt;
     *                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundServiceDetailType"&amp;gt;
     *                           &amp;lt;/extension&amp;gt;
     *                         &amp;lt;/complexContent&amp;gt;
     *                       &amp;lt;/complexType&amp;gt;
     *                     &amp;lt;/element&amp;gt;
     *                     &amp;lt;element name="Shuttle" type="{http://www.opentravel.org/OTA/2003/05}GroundShuttleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;/choice&amp;gt;
     *                   &amp;lt;element name="RateQualifier" type="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Promotions" type="{http://www.opentravel.org/OTA/2003/05}GroundPromotionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Restrictions" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                   &amp;lt;element name="ServiceCharges" type="{http://www.opentravel.org/OTA/2003/05}GroundServiceChargesType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}GroundFeesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="TotalCharge" type="{http://www.opentravel.org/OTA/2003/05}GroundTotalChargeType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="Reference" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="PaymentRules" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentRulesType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="AcceptablePayments" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="AcceptablePayment" maxOccurs="unbounded"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;attribute name="CreditCardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attribute name="PaymentTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "groundService"
    })
    public static class GroundServices {

        @XmlElement(name = "GroundService", required = true)
        protected List<OTAGroundAvailRS.GroundServices.GroundService> groundService;

        /**
         * Gets the value of the groundService property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the groundService property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getGroundService().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAGroundAvailRS.GroundServices.GroundService }
         * 
         * 
         */
        public List<OTAGroundAvailRS.GroundServices.GroundService> getGroundService() {
            if (groundService == null) {
                groundService = new ArrayList<OTAGroundAvailRS.GroundServices.GroundService>();
            }
            return this.groundService;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;choice&amp;gt;
         *           &amp;lt;element name="Service" minOccurs="0"&amp;gt;
         *             &amp;lt;complexType&amp;gt;
         *               &amp;lt;complexContent&amp;gt;
         *                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundServiceDetailType"&amp;gt;
         *                 &amp;lt;/extension&amp;gt;
         *               &amp;lt;/complexContent&amp;gt;
         *             &amp;lt;/complexType&amp;gt;
         *           &amp;lt;/element&amp;gt;
         *           &amp;lt;element name="Shuttle" type="{http://www.opentravel.org/OTA/2003/05}GroundShuttleType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;/choice&amp;gt;
         *         &amp;lt;element name="RateQualifier" type="{http://www.opentravel.org/OTA/2003/05}GroundRateQualifierType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Promotions" type="{http://www.opentravel.org/OTA/2003/05}GroundPromotionType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Restrictions" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
         *                           &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *         &amp;lt;element name="ServiceCharges" type="{http://www.opentravel.org/OTA/2003/05}GroundServiceChargesType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Fees" type="{http://www.opentravel.org/OTA/2003/05}GroundFeesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="TotalCharge" type="{http://www.opentravel.org/OTA/2003/05}GroundTotalChargeType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="Reference" type="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="PaymentRules" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentRulesType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="AcceptablePayments" maxOccurs="unbounded" minOccurs="0"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="AcceptablePayment" maxOccurs="unbounded"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;attribute name="CreditCardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attribute name="PaymentTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "service",
            "shuttle",
            "rateQualifier",
            "promotions",
            "restrictions",
            "serviceCharges",
            "fees",
            "totalCharge",
            "reference",
            "paymentRules"
        })
        public static class GroundService {

            @XmlElement(name = "Service")
            protected OTAGroundAvailRS.GroundServices.GroundService.Service service;
            @XmlElement(name = "Shuttle")
            protected List<GroundShuttleType> shuttle;
            @XmlElement(name = "RateQualifier")
            protected List<GroundRateQualifierType> rateQualifier;
            @XmlElement(name = "Promotions")
            protected List<GroundPromotionType> promotions;
            @XmlElement(name = "Restrictions")
            protected List<OTAGroundAvailRS.GroundServices.GroundService.Restrictions> restrictions;
            @XmlElement(name = "ServiceCharges")
            protected List<GroundServiceChargesType> serviceCharges;
            @XmlElement(name = "Fees")
            protected GroundFeesType fees;
            @XmlElement(name = "TotalCharge")
            protected GroundTotalChargeType totalCharge;
            @XmlElement(name = "Reference")
            protected UniqueIDType reference;
            @XmlElement(name = "PaymentRules")
            protected OTAGroundAvailRS.GroundServices.GroundService.PaymentRules paymentRules;

            /**
             * Obtiene el valor de la propiedad service.
             * 
             * @return
             *     possible object is
             *     {@link OTAGroundAvailRS.GroundServices.GroundService.Service }
             *     
             */
            public OTAGroundAvailRS.GroundServices.GroundService.Service getService() {
                return service;
            }

            /**
             * Define el valor de la propiedad service.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAGroundAvailRS.GroundServices.GroundService.Service }
             *     
             */
            public void setService(OTAGroundAvailRS.GroundServices.GroundService.Service value) {
                this.service = value;
            }

            /**
             * Gets the value of the shuttle property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the shuttle property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getShuttle().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link GroundShuttleType }
             * 
             * 
             */
            public List<GroundShuttleType> getShuttle() {
                if (shuttle == null) {
                    shuttle = new ArrayList<GroundShuttleType>();
                }
                return this.shuttle;
            }

            /**
             * Gets the value of the rateQualifier property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the rateQualifier property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRateQualifier().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link GroundRateQualifierType }
             * 
             * 
             */
            public List<GroundRateQualifierType> getRateQualifier() {
                if (rateQualifier == null) {
                    rateQualifier = new ArrayList<GroundRateQualifierType>();
                }
                return this.rateQualifier;
            }

            /**
             * Gets the value of the promotions property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the promotions property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPromotions().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link GroundPromotionType }
             * 
             * 
             */
            public List<GroundPromotionType> getPromotions() {
                if (promotions == null) {
                    promotions = new ArrayList<GroundPromotionType>();
                }
                return this.promotions;
            }

            /**
             * Gets the value of the restrictions property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the restrictions property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getRestrictions().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link OTAGroundAvailRS.GroundServices.GroundService.Restrictions }
             * 
             * 
             */
            public List<OTAGroundAvailRS.GroundServices.GroundService.Restrictions> getRestrictions() {
                if (restrictions == null) {
                    restrictions = new ArrayList<OTAGroundAvailRS.GroundServices.GroundService.Restrictions>();
                }
                return this.restrictions;
            }

            /**
             * Gets the value of the serviceCharges property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the serviceCharges property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getServiceCharges().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link GroundServiceChargesType }
             * 
             * 
             */
            public List<GroundServiceChargesType> getServiceCharges() {
                if (serviceCharges == null) {
                    serviceCharges = new ArrayList<GroundServiceChargesType>();
                }
                return this.serviceCharges;
            }

            /**
             * Obtiene el valor de la propiedad fees.
             * 
             * @return
             *     possible object is
             *     {@link GroundFeesType }
             *     
             */
            public GroundFeesType getFees() {
                return fees;
            }

            /**
             * Define el valor de la propiedad fees.
             * 
             * @param value
             *     allowed object is
             *     {@link GroundFeesType }
             *     
             */
            public void setFees(GroundFeesType value) {
                this.fees = value;
            }

            /**
             * Obtiene el valor de la propiedad totalCharge.
             * 
             * @return
             *     possible object is
             *     {@link GroundTotalChargeType }
             *     
             */
            public GroundTotalChargeType getTotalCharge() {
                return totalCharge;
            }

            /**
             * Define el valor de la propiedad totalCharge.
             * 
             * @param value
             *     allowed object is
             *     {@link GroundTotalChargeType }
             *     
             */
            public void setTotalCharge(GroundTotalChargeType value) {
                this.totalCharge = value;
            }

            /**
             * Obtiene el valor de la propiedad reference.
             * 
             * @return
             *     possible object is
             *     {@link UniqueIDType }
             *     
             */
            public UniqueIDType getReference() {
                return reference;
            }

            /**
             * Define el valor de la propiedad reference.
             * 
             * @param value
             *     allowed object is
             *     {@link UniqueIDType }
             *     
             */
            public void setReference(UniqueIDType value) {
                this.reference = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentRules.
             * 
             * @return
             *     possible object is
             *     {@link OTAGroundAvailRS.GroundServices.GroundService.PaymentRules }
             *     
             */
            public OTAGroundAvailRS.GroundServices.GroundService.PaymentRules getPaymentRules() {
                return paymentRules;
            }

            /**
             * Define el valor de la propiedad paymentRules.
             * 
             * @param value
             *     allowed object is
             *     {@link OTAGroundAvailRS.GroundServices.GroundService.PaymentRules }
             *     
             */
            public void setPaymentRules(OTAGroundAvailRS.GroundServices.GroundService.PaymentRules value) {
                this.paymentRules = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}PaymentRulesType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="AcceptablePayments" maxOccurs="unbounded" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="AcceptablePayment" maxOccurs="unbounded"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;attribute name="CreditCardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attribute name="PaymentTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "acceptablePayments"
            })
            public static class PaymentRules
                extends PaymentRulesType
            {

                @XmlElement(name = "AcceptablePayments")
                protected List<OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments> acceptablePayments;

                /**
                 * Gets the value of the acceptablePayments property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the acceptablePayments property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getAcceptablePayments().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments }
                 * 
                 * 
                 */
                public List<OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments> getAcceptablePayments() {
                    if (acceptablePayments == null) {
                        acceptablePayments = new ArrayList<OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments>();
                    }
                    return this.acceptablePayments;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="AcceptablePayment" maxOccurs="unbounded"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;attribute name="CreditCardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attribute name="PaymentTypeCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "acceptablePayment"
                })
                public static class AcceptablePayments {

                    @XmlElement(name = "AcceptablePayment", required = true)
                    protected List<OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments.AcceptablePayment> acceptablePayment;
                    @XmlAttribute(name = "PaymentTypeCode")
                    protected String paymentTypeCode;

                    /**
                     * Gets the value of the acceptablePayment property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the acceptablePayment property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getAcceptablePayment().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments.AcceptablePayment }
                     * 
                     * 
                     */
                    public List<OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments.AcceptablePayment> getAcceptablePayment() {
                        if (acceptablePayment == null) {
                            acceptablePayment = new ArrayList<OTAGroundAvailRS.GroundServices.GroundService.PaymentRules.AcceptablePayments.AcceptablePayment>();
                        }
                        return this.acceptablePayment;
                    }

                    /**
                     * Obtiene el valor de la propiedad paymentTypeCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPaymentTypeCode() {
                        return paymentTypeCode;
                    }

                    /**
                     * Define el valor de la propiedad paymentTypeCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPaymentTypeCode(String value) {
                        this.paymentTypeCode = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;attribute name="CreditCardCode" type="{http://www.opentravel.org/OTA/2003/05}PaymentCardCodeType" /&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class AcceptablePayment {

                        @XmlAttribute(name = "CreditCardCode")
                        protected String creditCardCode;

                        /**
                         * Obtiene el valor de la propiedad creditCardCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCreditCardCode() {
                            return creditCardCode;
                        }

                        /**
                         * Define el valor de la propiedad creditCardCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCreditCardCode(String value) {
                            this.creditCardCode = value;
                        }

                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="AdvanceBooking" minOccurs="0"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
             *                 &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="AdvancedBookingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="CorporateRateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="GuaranteeReqInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="CancellationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="ModificationPenaltyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "advanceBooking"
            })
            public static class Restrictions {

                @XmlElement(name = "AdvanceBooking")
                protected OTAGroundAvailRS.GroundServices.GroundService.Restrictions.AdvanceBooking advanceBooking;
                @XmlAttribute(name = "AdvancedBookingInd")
                protected Boolean advancedBookingInd;
                @XmlAttribute(name = "CorporateRateInd")
                protected Boolean corporateRateInd;
                @XmlAttribute(name = "GuaranteeReqInd")
                protected Boolean guaranteeReqInd;
                @XmlAttribute(name = "CancellationPenaltyInd")
                protected Boolean cancellationPenaltyInd;
                @XmlAttribute(name = "ModificationPenaltyInd")
                protected Boolean modificationPenaltyInd;

                /**
                 * Obtiene el valor de la propiedad advanceBooking.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTAGroundAvailRS.GroundServices.GroundService.Restrictions.AdvanceBooking }
                 *     
                 */
                public OTAGroundAvailRS.GroundServices.GroundService.Restrictions.AdvanceBooking getAdvanceBooking() {
                    return advanceBooking;
                }

                /**
                 * Define el valor de la propiedad advanceBooking.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTAGroundAvailRS.GroundServices.GroundService.Restrictions.AdvanceBooking }
                 *     
                 */
                public void setAdvanceBooking(OTAGroundAvailRS.GroundServices.GroundService.Restrictions.AdvanceBooking value) {
                    this.advanceBooking = value;
                }

                /**
                 * Obtiene el valor de la propiedad advancedBookingInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAdvancedBookingInd() {
                    return advancedBookingInd;
                }

                /**
                 * Define el valor de la propiedad advancedBookingInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAdvancedBookingInd(Boolean value) {
                    this.advancedBookingInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad corporateRateInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isCorporateRateInd() {
                    return corporateRateInd;
                }

                /**
                 * Define el valor de la propiedad corporateRateInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setCorporateRateInd(Boolean value) {
                    this.corporateRateInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad guaranteeReqInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isGuaranteeReqInd() {
                    return guaranteeReqInd;
                }

                /**
                 * Define el valor de la propiedad guaranteeReqInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setGuaranteeReqInd(Boolean value) {
                    this.guaranteeReqInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad cancellationPenaltyInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isCancellationPenaltyInd() {
                    return cancellationPenaltyInd;
                }

                /**
                 * Define el valor de la propiedad cancellationPenaltyInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setCancellationPenaltyInd(Boolean value) {
                    this.cancellationPenaltyInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad modificationPenaltyInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isModificationPenaltyInd() {
                    return modificationPenaltyInd;
                }

                /**
                 * Define el valor de la propiedad modificationPenaltyInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setModificationPenaltyInd(Boolean value) {
                    this.modificationPenaltyInd = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DeadlineGroup"/&amp;gt;
                 *       &amp;lt;attribute name="RequiredInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class AdvanceBooking {

                    @XmlAttribute(name = "RequiredInd")
                    protected Boolean requiredInd;
                    @XmlAttribute(name = "AbsoluteDeadline")
                    protected String absoluteDeadline;
                    @XmlAttribute(name = "OffsetTimeUnit")
                    protected TimeUnitType offsetTimeUnit;
                    @XmlAttribute(name = "OffsetUnitMultiplier")
                    protected Integer offsetUnitMultiplier;
                    @XmlAttribute(name = "OffsetDropTime")
                    protected String offsetDropTime;

                    /**
                     * Obtiene el valor de la propiedad requiredInd.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isRequiredInd() {
                        return requiredInd;
                    }

                    /**
                     * Define el valor de la propiedad requiredInd.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setRequiredInd(Boolean value) {
                        this.requiredInd = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad absoluteDeadline.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAbsoluteDeadline() {
                        return absoluteDeadline;
                    }

                    /**
                     * Define el valor de la propiedad absoluteDeadline.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAbsoluteDeadline(String value) {
                        this.absoluteDeadline = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad offsetTimeUnit.
                     * 
                     * @return
                     *     possible object is
                     *     {@link TimeUnitType }
                     *     
                     */
                    public TimeUnitType getOffsetTimeUnit() {
                        return offsetTimeUnit;
                    }

                    /**
                     * Define el valor de la propiedad offsetTimeUnit.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TimeUnitType }
                     *     
                     */
                    public void setOffsetTimeUnit(TimeUnitType value) {
                        this.offsetTimeUnit = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad offsetUnitMultiplier.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getOffsetUnitMultiplier() {
                        return offsetUnitMultiplier;
                    }

                    /**
                     * Define el valor de la propiedad offsetUnitMultiplier.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setOffsetUnitMultiplier(Integer value) {
                        this.offsetUnitMultiplier = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad offsetDropTime.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOffsetDropTime() {
                        return offsetDropTime;
                    }

                    /**
                     * Define el valor de la propiedad offsetDropTime.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOffsetDropTime(String value) {
                        this.offsetDropTime = value;
                    }

                }

            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}GroundServiceDetailType"&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Service
                extends GroundServiceDetailType
            {


            }

        }

    }

}
