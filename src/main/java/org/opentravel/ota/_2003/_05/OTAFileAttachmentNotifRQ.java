
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FileAttachmentReferences"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;sequence&amp;gt;
 *                   &amp;lt;element name="FileAttachmentReference" maxOccurs="99"&amp;gt;
 *                     &amp;lt;complexType&amp;gt;
 *                       &amp;lt;complexContent&amp;gt;
 *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeStampGroup"/&amp;gt;
 *                         &amp;lt;/restriction&amp;gt;
 *                       &amp;lt;/complexContent&amp;gt;
 *                     &amp;lt;/complexType&amp;gt;
 *                   &amp;lt;/element&amp;gt;
 *                   &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *                 &amp;lt;/sequence&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "fileAttachmentReferences",
    "tpaExtensions"
})
@XmlRootElement(name = "OTA_FileAttachmentNotifRQ")
public class OTAFileAttachmentNotifRQ {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "FileAttachmentReferences", required = true)
    protected OTAFileAttachmentNotifRQ.FileAttachmentReferences fileAttachmentReferences;
    @XmlElement(name = "TPA_Extensions")
    protected TPAExtensionsType tpaExtensions;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad fileAttachmentReferences.
     * 
     * @return
     *     possible object is
     *     {@link OTAFileAttachmentNotifRQ.FileAttachmentReferences }
     *     
     */
    public OTAFileAttachmentNotifRQ.FileAttachmentReferences getFileAttachmentReferences() {
        return fileAttachmentReferences;
    }

    /**
     * Define el valor de la propiedad fileAttachmentReferences.
     * 
     * @param value
     *     allowed object is
     *     {@link OTAFileAttachmentNotifRQ.FileAttachmentReferences }
     *     
     */
    public void setFileAttachmentReferences(OTAFileAttachmentNotifRQ.FileAttachmentReferences value) {
        this.fileAttachmentReferences = value;
    }

    /**
     * Obtiene el valor de la propiedad tpaExtensions.
     * 
     * @return
     *     possible object is
     *     {@link TPAExtensionsType }
     *     
     */
    public TPAExtensionsType getTPAExtensions() {
        return tpaExtensions;
    }

    /**
     * Define el valor de la propiedad tpaExtensions.
     * 
     * @param value
     *     allowed object is
     *     {@link TPAExtensionsType }
     *     
     */
    public void setTPAExtensions(TPAExtensionsType value) {
        this.tpaExtensions = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="FileAttachmentReference" maxOccurs="99"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeStampGroup"/&amp;gt;
     *               &amp;lt;/restriction&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element ref="{http://www.opentravel.org/OTA/2003/05}TPA_Extensions" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fileAttachmentReference",
        "tpaExtensions"
    })
    public static class FileAttachmentReferences {

        @XmlElement(name = "FileAttachmentReference", required = true)
        protected List<OTAFileAttachmentNotifRQ.FileAttachmentReferences.FileAttachmentReference> fileAttachmentReference;
        @XmlElement(name = "TPA_Extensions")
        protected TPAExtensionsType tpaExtensions;

        /**
         * Gets the value of the fileAttachmentReference property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the fileAttachmentReference property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getFileAttachmentReference().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTAFileAttachmentNotifRQ.FileAttachmentReferences.FileAttachmentReference }
         * 
         * 
         */
        public List<OTAFileAttachmentNotifRQ.FileAttachmentReferences.FileAttachmentReference> getFileAttachmentReference() {
            if (fileAttachmentReference == null) {
                fileAttachmentReference = new ArrayList<OTAFileAttachmentNotifRQ.FileAttachmentReferences.FileAttachmentReference>();
            }
            return this.fileAttachmentReference;
        }

        /**
         * Obtiene el valor de la propiedad tpaExtensions.
         * 
         * @return
         *     possible object is
         *     {@link TPAExtensionsType }
         *     
         */
        public TPAExtensionsType getTPAExtensions() {
            return tpaExtensions;
        }

        /**
         * Define el valor de la propiedad tpaExtensions.
         * 
         * @param value
         *     allowed object is
         *     {@link TPAExtensionsType }
         *     
         */
        public void setTPAExtensions(TPAExtensionsType value) {
            this.tpaExtensions = value;
        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}FileAttachmentGroup"/&amp;gt;
         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeStampGroup"/&amp;gt;
         *     &amp;lt;/restriction&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class FileAttachmentReference {

            @XmlAttribute(name = "ContentData")
            protected String contentData;
            @XmlAttribute(name = "Description")
            protected String description;
            @XmlAttribute(name = "PictureCategoryCode")
            protected String pictureCategoryCode;
            @XmlAttribute(name = "Version")
            protected String version;
            @XmlAttribute(name = "ContentTitle")
            protected String contentTitle;
            @XmlAttribute(name = "ContentCaption")
            protected String contentCaption;
            @XmlAttribute(name = "CopyrightNotice")
            protected String copyrightNotice;
            @XmlAttribute(name = "FileName")
            protected String fileName;
            @XmlAttribute(name = "FileSize")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger fileSize;
            @XmlAttribute(name = "MultimediaObjectHeight")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger multimediaObjectHeight;
            @XmlAttribute(name = "MultimediaObjectWidth")
            @XmlSchemaType(name = "positiveInteger")
            protected BigInteger multimediaObjectWidth;
            @XmlAttribute(name = "UnitOfMeasureCode")
            protected String unitOfMeasureCode;
            @XmlAttribute(name = "ContentID")
            protected String contentID;
            @XmlAttribute(name = "ContentCode")
            protected String contentCode;
            @XmlAttribute(name = "ContentFormatCode")
            protected String contentFormatCode;
            @XmlAttribute(name = "RecordID")
            protected String recordID;
            @XmlAttribute(name = "Removal")
            protected Boolean removal;
            @XmlAttribute(name = "CodeDetail")
            protected String codeDetail;
            @XmlAttribute(name = "CreateDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar createDateTime;
            @XmlAttribute(name = "CreatorID")
            protected String creatorID;
            @XmlAttribute(name = "LastModifyDateTime")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar lastModifyDateTime;
            @XmlAttribute(name = "LastModifierID")
            protected String lastModifierID;
            @XmlAttribute(name = "PurgeDate")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar purgeDate;

            /**
             * Obtiene el valor de la propiedad contentData.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentData() {
                return contentData;
            }

            /**
             * Define el valor de la propiedad contentData.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentData(String value) {
                this.contentData = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad pictureCategoryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPictureCategoryCode() {
                return pictureCategoryCode;
            }

            /**
             * Define el valor de la propiedad pictureCategoryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPictureCategoryCode(String value) {
                this.pictureCategoryCode = value;
            }

            /**
             * Obtiene el valor de la propiedad version.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVersion() {
                return version;
            }

            /**
             * Define el valor de la propiedad version.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVersion(String value) {
                this.version = value;
            }

            /**
             * Obtiene el valor de la propiedad contentTitle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentTitle() {
                return contentTitle;
            }

            /**
             * Define el valor de la propiedad contentTitle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentTitle(String value) {
                this.contentTitle = value;
            }

            /**
             * Obtiene el valor de la propiedad contentCaption.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentCaption() {
                return contentCaption;
            }

            /**
             * Define el valor de la propiedad contentCaption.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentCaption(String value) {
                this.contentCaption = value;
            }

            /**
             * Obtiene el valor de la propiedad copyrightNotice.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCopyrightNotice() {
                return copyrightNotice;
            }

            /**
             * Define el valor de la propiedad copyrightNotice.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCopyrightNotice(String value) {
                this.copyrightNotice = value;
            }

            /**
             * Obtiene el valor de la propiedad fileName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFileName() {
                return fileName;
            }

            /**
             * Define el valor de la propiedad fileName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFileName(String value) {
                this.fileName = value;
            }

            /**
             * Obtiene el valor de la propiedad fileSize.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFileSize() {
                return fileSize;
            }

            /**
             * Define el valor de la propiedad fileSize.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFileSize(BigInteger value) {
                this.fileSize = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaObjectHeight.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMultimediaObjectHeight() {
                return multimediaObjectHeight;
            }

            /**
             * Define el valor de la propiedad multimediaObjectHeight.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMultimediaObjectHeight(BigInteger value) {
                this.multimediaObjectHeight = value;
            }

            /**
             * Obtiene el valor de la propiedad multimediaObjectWidth.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMultimediaObjectWidth() {
                return multimediaObjectWidth;
            }

            /**
             * Define el valor de la propiedad multimediaObjectWidth.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMultimediaObjectWidth(BigInteger value) {
                this.multimediaObjectWidth = value;
            }

            /**
             * Obtiene el valor de la propiedad unitOfMeasureCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUnitOfMeasureCode() {
                return unitOfMeasureCode;
            }

            /**
             * Define el valor de la propiedad unitOfMeasureCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUnitOfMeasureCode(String value) {
                this.unitOfMeasureCode = value;
            }

            /**
             * Obtiene el valor de la propiedad contentID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentID() {
                return contentID;
            }

            /**
             * Define el valor de la propiedad contentID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentID(String value) {
                this.contentID = value;
            }

            /**
             * Obtiene el valor de la propiedad contentCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentCode() {
                return contentCode;
            }

            /**
             * Define el valor de la propiedad contentCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentCode(String value) {
                this.contentCode = value;
            }

            /**
             * Obtiene el valor de la propiedad contentFormatCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContentFormatCode() {
                return contentFormatCode;
            }

            /**
             * Define el valor de la propiedad contentFormatCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContentFormatCode(String value) {
                this.contentFormatCode = value;
            }

            /**
             * Obtiene el valor de la propiedad recordID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRecordID() {
                return recordID;
            }

            /**
             * Define el valor de la propiedad recordID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRecordID(String value) {
                this.recordID = value;
            }

            /**
             * Obtiene el valor de la propiedad removal.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRemoval() {
                return removal;
            }

            /**
             * Define el valor de la propiedad removal.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRemoval(Boolean value) {
                this.removal = value;
            }

            /**
             * Obtiene el valor de la propiedad codeDetail.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodeDetail() {
                return codeDetail;
            }

            /**
             * Define el valor de la propiedad codeDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodeDetail(String value) {
                this.codeDetail = value;
            }

            /**
             * Obtiene el valor de la propiedad createDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCreateDateTime() {
                return createDateTime;
            }

            /**
             * Define el valor de la propiedad createDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCreateDateTime(XMLGregorianCalendar value) {
                this.createDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad creatorID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatorID() {
                return creatorID;
            }

            /**
             * Define el valor de la propiedad creatorID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatorID(String value) {
                this.creatorID = value;
            }

            /**
             * Obtiene el valor de la propiedad lastModifyDateTime.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLastModifyDateTime() {
                return lastModifyDateTime;
            }

            /**
             * Define el valor de la propiedad lastModifyDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLastModifyDateTime(XMLGregorianCalendar value) {
                this.lastModifyDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad lastModifierID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastModifierID() {
                return lastModifierID;
            }

            /**
             * Define el valor de la propiedad lastModifierID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastModifierID(String value) {
                this.lastModifierID = value;
            }

            /**
             * Obtiene el valor de la propiedad purgeDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getPurgeDate() {
                return purgeDate;
            }

            /**
             * Define el valor de la propiedad purgeDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setPurgeDate(XMLGregorianCalendar value) {
                this.purgeDate = value;
            }

        }

    }

}
