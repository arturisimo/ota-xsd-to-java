
package org.opentravel.ota._2003._05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Service and vehicle information.
 * 
 * &lt;p&gt;Clase Java para GroundServiceType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GroundServiceType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="Language" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
 *                 &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                 &amp;lt;attribute name="RequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="ChildCarSeatInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="PetFriendlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="FuelEfficientVehInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="MaximumPassengers" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="MaximumBaggage" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&amp;gt;
 *       &amp;lt;attribute name="GreetingSignInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="GreetingSignName" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="MeetAndGreetInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="PersonalGreeterInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="GuideInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ShortDescription"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;minLength value="0"/&amp;gt;
 *             &amp;lt;maxLength value="250"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="MultilingualInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroundServiceType", propOrder = {
    "language"
})
@XmlSeeAlso({
    GroundServiceDetailType.class
})
public class GroundServiceType {

    @XmlElement(name = "Language")
    protected List<GroundServiceType.Language> language;
    @XmlAttribute(name = "ChildCarSeatInd")
    protected Boolean childCarSeatInd;
    @XmlAttribute(name = "PetFriendlyInd")
    protected Boolean petFriendlyInd;
    @XmlAttribute(name = "FuelEfficientVehInd")
    protected Boolean fuelEfficientVehInd;
    @XmlAttribute(name = "MaximumPassengers")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maximumPassengers;
    @XmlAttribute(name = "MaximumBaggage")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maximumBaggage;
    @XmlAttribute(name = "GreetingSignInd")
    protected Boolean greetingSignInd;
    @XmlAttribute(name = "GreetingSignName")
    protected String greetingSignName;
    @XmlAttribute(name = "MeetAndGreetInd")
    protected Boolean meetAndGreetInd;
    @XmlAttribute(name = "PersonalGreeterInd")
    protected Boolean personalGreeterInd;
    @XmlAttribute(name = "GuideInd")
    protected Boolean guideInd;
    @XmlAttribute(name = "ShortDescription")
    protected String shortDescription;
    @XmlAttribute(name = "Notes")
    protected String notes;
    @XmlAttribute(name = "MultilingualInd")
    protected Boolean multilingualInd;

    /**
     * Gets the value of the language property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the language property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getLanguage().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link GroundServiceType.Language }
     * 
     * 
     */
    public List<GroundServiceType.Language> getLanguage() {
        if (language == null) {
            language = new ArrayList<GroundServiceType.Language>();
        }
        return this.language;
    }

    /**
     * Obtiene el valor de la propiedad childCarSeatInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChildCarSeatInd() {
        return childCarSeatInd;
    }

    /**
     * Define el valor de la propiedad childCarSeatInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChildCarSeatInd(Boolean value) {
        this.childCarSeatInd = value;
    }

    /**
     * Obtiene el valor de la propiedad petFriendlyInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPetFriendlyInd() {
        return petFriendlyInd;
    }

    /**
     * Define el valor de la propiedad petFriendlyInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPetFriendlyInd(Boolean value) {
        this.petFriendlyInd = value;
    }

    /**
     * Obtiene el valor de la propiedad fuelEfficientVehInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFuelEfficientVehInd() {
        return fuelEfficientVehInd;
    }

    /**
     * Define el valor de la propiedad fuelEfficientVehInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFuelEfficientVehInd(Boolean value) {
        this.fuelEfficientVehInd = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumPassengers.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumPassengers() {
        return maximumPassengers;
    }

    /**
     * Define el valor de la propiedad maximumPassengers.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumPassengers(BigInteger value) {
        this.maximumPassengers = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumBaggage.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumBaggage() {
        return maximumBaggage;
    }

    /**
     * Define el valor de la propiedad maximumBaggage.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumBaggage(BigInteger value) {
        this.maximumBaggage = value;
    }

    /**
     * Obtiene el valor de la propiedad greetingSignInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGreetingSignInd() {
        return greetingSignInd;
    }

    /**
     * Define el valor de la propiedad greetingSignInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGreetingSignInd(Boolean value) {
        this.greetingSignInd = value;
    }

    /**
     * Obtiene el valor de la propiedad greetingSignName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreetingSignName() {
        return greetingSignName;
    }

    /**
     * Define el valor de la propiedad greetingSignName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreetingSignName(String value) {
        this.greetingSignName = value;
    }

    /**
     * Obtiene el valor de la propiedad meetAndGreetInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMeetAndGreetInd() {
        return meetAndGreetInd;
    }

    /**
     * Define el valor de la propiedad meetAndGreetInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeetAndGreetInd(Boolean value) {
        this.meetAndGreetInd = value;
    }

    /**
     * Obtiene el valor de la propiedad personalGreeterInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPersonalGreeterInd() {
        return personalGreeterInd;
    }

    /**
     * Define el valor de la propiedad personalGreeterInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonalGreeterInd(Boolean value) {
        this.personalGreeterInd = value;
    }

    /**
     * Obtiene el valor de la propiedad guideInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGuideInd() {
        return guideInd;
    }

    /**
     * Define el valor de la propiedad guideInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuideInd(Boolean value) {
        this.guideInd = value;
    }

    /**
     * Obtiene el valor de la propiedad shortDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * Define el valor de la propiedad shortDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortDescription(String value) {
        this.shortDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad notes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Define el valor de la propiedad notes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

    /**
     * Obtiene el valor de la propiedad multilingualInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMultilingualInd() {
        return multilingualInd;
    }

    /**
     * Define el valor de la propiedad multilingualInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultilingualInd(Boolean value) {
        this.multilingualInd = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attribute name="Language" type="{http://www.w3.org/2001/XMLSchema}language" /&amp;gt;
     *       &amp;lt;attribute name="PrimaryLangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *       &amp;lt;attribute name="RequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Language {

        @XmlAttribute(name = "Language")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "language")
        protected String language;
        @XmlAttribute(name = "PrimaryLangInd")
        protected Boolean primaryLangInd;
        @XmlAttribute(name = "RequestedInd")
        protected Boolean requestedInd;

        /**
         * Obtiene el valor de la propiedad language.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * Define el valor de la propiedad language.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryLangInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPrimaryLangInd() {
            return primaryLangInd;
        }

        /**
         * Define el valor de la propiedad primaryLangInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPrimaryLangInd(Boolean value) {
            this.primaryLangInd = value;
        }

        /**
         * Obtiene el valor de la propiedad requestedInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRequestedInd() {
            return requestedInd;
        }

        /**
         * Define el valor de la propiedad requestedInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRequestedInd(Boolean value) {
            this.requestedInd = value;
        }

    }

}
