
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides accommodation location, dates and classifications
 * 
 * &lt;p&gt;Clase Java para AccommodationDetailType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="AccommodationDetailType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}AccommodationInfoType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="FacilityChoices" type="{http://www.opentravel.org/OTA/2003/05}FacilityChoicesType" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}DateTimeSpanGroup"/&amp;gt;
 *       &amp;lt;attribute name="BedQuantity" type="{http://www.opentravel.org/OTA/2003/05}Numeric0to999" /&amp;gt;
 *       &amp;lt;attribute name="TravelOKFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccommodationDetailType", propOrder = {
    "facilityChoices"
})
public class AccommodationDetailType
    extends AccommodationInfoType
{

    @XmlElement(name = "FacilityChoices")
    protected FacilityChoicesType facilityChoices;
    @XmlAttribute(name = "BedQuantity")
    protected Integer bedQuantity;
    @XmlAttribute(name = "TravelOKFlag")
    protected Boolean travelOKFlag;
    @XmlAttribute(name = "Start")
    protected String start;
    @XmlAttribute(name = "Duration")
    protected String duration;
    @XmlAttribute(name = "End")
    protected String end;

    /**
     * Obtiene el valor de la propiedad facilityChoices.
     * 
     * @return
     *     possible object is
     *     {@link FacilityChoicesType }
     *     
     */
    public FacilityChoicesType getFacilityChoices() {
        return facilityChoices;
    }

    /**
     * Define el valor de la propiedad facilityChoices.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityChoicesType }
     *     
     */
    public void setFacilityChoices(FacilityChoicesType value) {
        this.facilityChoices = value;
    }

    /**
     * Obtiene el valor de la propiedad bedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBedQuantity() {
        return bedQuantity;
    }

    /**
     * Define el valor de la propiedad bedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBedQuantity(Integer value) {
        this.bedQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad travelOKFlag.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTravelOKFlag() {
        return travelOKFlag;
    }

    /**
     * Define el valor de la propiedad travelOKFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTravelOKFlag(Boolean value) {
        this.travelOKFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad start.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Define el valor de la propiedad start.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuration(String value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad end.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Define el valor de la propiedad end.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }

}
