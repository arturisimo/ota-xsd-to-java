
package org.opentravel.ota._2003._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A collection of SelectedLoyalty classes that are used to report earned bonuses.
 * 
 * &lt;p&gt;Clase Java para LoyaltyPointsAccrualsType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="LoyaltyPointsAccrualsType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="SelectedLoyalty" maxOccurs="99"&amp;gt;
 *           &amp;lt;complexType&amp;gt;
 *             &amp;lt;complexContent&amp;gt;
 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
 *               &amp;lt;/restriction&amp;gt;
 *             &amp;lt;/complexContent&amp;gt;
 *           &amp;lt;/complexType&amp;gt;
 *         &amp;lt;/element&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoyaltyPointsAccrualsType", propOrder = {
    "selectedLoyalty"
})
public class LoyaltyPointsAccrualsType {

    @XmlElement(name = "SelectedLoyalty", required = true)
    protected List<LoyaltyPointsAccrualsType.SelectedLoyalty> selectedLoyalty;

    /**
     * Gets the value of the selectedLoyalty property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the selectedLoyalty property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getSelectedLoyalty().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link LoyaltyPointsAccrualsType.SelectedLoyalty }
     * 
     * 
     */
    public List<LoyaltyPointsAccrualsType.SelectedLoyalty> getSelectedLoyalty() {
        if (selectedLoyalty == null) {
            selectedLoyalty = new ArrayList<LoyaltyPointsAccrualsType.SelectedLoyalty>();
        }
        return this.selectedLoyalty;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SelectedLoyaltyGroup"/&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SelectedLoyalty {

        @XmlAttribute(name = "ReservationActionType")
        protected String reservationActionType;
        @XmlAttribute(name = "SelectedLoyaltyRPH")
        protected String selectedLoyaltyRPH;
        @XmlAttribute(name = "ProgramCode")
        protected String programCode;
        @XmlAttribute(name = "BonusCode")
        protected String bonusCode;
        @XmlAttribute(name = "AccountID")
        protected String accountID;
        @XmlAttribute(name = "PointsEarned")
        protected String pointsEarned;

        /**
         * Obtiene el valor de la propiedad reservationActionType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReservationActionType() {
            return reservationActionType;
        }

        /**
         * Define el valor de la propiedad reservationActionType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReservationActionType(String value) {
            this.reservationActionType = value;
        }

        /**
         * Obtiene el valor de la propiedad selectedLoyaltyRPH.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSelectedLoyaltyRPH() {
            return selectedLoyaltyRPH;
        }

        /**
         * Define el valor de la propiedad selectedLoyaltyRPH.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSelectedLoyaltyRPH(String value) {
            this.selectedLoyaltyRPH = value;
        }

        /**
         * Obtiene el valor de la propiedad programCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProgramCode() {
            return programCode;
        }

        /**
         * Define el valor de la propiedad programCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProgramCode(String value) {
            this.programCode = value;
        }

        /**
         * Obtiene el valor de la propiedad bonusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBonusCode() {
            return bonusCode;
        }

        /**
         * Define el valor de la propiedad bonusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBonusCode(String value) {
            this.bonusCode = value;
        }

        /**
         * Obtiene el valor de la propiedad accountID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountID() {
            return accountID;
        }

        /**
         * Define el valor de la propiedad accountID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountID(String value) {
            this.accountID = value;
        }

        /**
         * Obtiene el valor de la propiedad pointsEarned.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPointsEarned() {
            return pointsEarned;
        }

        /**
         * Define el valor de la propiedad pointsEarned.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPointsEarned(String value) {
            this.pointsEarned = value;
        }

    }

}
