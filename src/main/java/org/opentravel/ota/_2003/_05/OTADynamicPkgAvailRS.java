
package org.opentravel.ota._2003._05;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Clase Java para anonymous complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="POS" type="{http://www.opentravel.org/OTA/2003/05}POS_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;element name="Success" type="{http://www.opentravel.org/OTA/2003/05}SuccessType"/&amp;gt;
 *             &amp;lt;element name="Warnings" type="{http://www.opentravel.org/OTA/2003/05}WarningsType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="SearchResults" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="AirResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
 *                               &amp;lt;choice&amp;gt;
 *                                 &amp;lt;element name="PricedItineraries" type="{http://www.opentravel.org/OTA/2003/05}PricedItinerariesType" minOccurs="0"/&amp;gt;
 *                                 &amp;lt;element name="OriginDestinationInformation" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="OriginDestinationOptions"&amp;gt;
 *                                             &amp;lt;complexType&amp;gt;
 *                                               &amp;lt;complexContent&amp;gt;
 *                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                   &amp;lt;sequence&amp;gt;
 *                                                     &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
 *                                                       &amp;lt;complexType&amp;gt;
 *                                                         &amp;lt;complexContent&amp;gt;
 *                                                           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                             &amp;lt;sequence&amp;gt;
 *                                                               &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
 *                                                                 &amp;lt;complexType&amp;gt;
 *                                                                   &amp;lt;complexContent&amp;gt;
 *                                                                     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
 *                                                                       &amp;lt;sequence&amp;gt;
 *                                                                         &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                                           &amp;lt;complexType&amp;gt;
 *                                                                             &amp;lt;simpleContent&amp;gt;
 *                                                                               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
 *                                                                                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                               &amp;lt;/extension&amp;gt;
 *                                                                             &amp;lt;/simpleContent&amp;gt;
 *                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                         &amp;lt;/element&amp;gt;
 *                                                                         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
 *                                                                         &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
 *                                                                         &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
 *                                                                           &amp;lt;complexType&amp;gt;
 *                                                                             &amp;lt;complexContent&amp;gt;
 *                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
 *                                                                                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                                               &amp;lt;/restriction&amp;gt;
 *                                                                             &amp;lt;/complexContent&amp;gt;
 *                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                         &amp;lt;/element&amp;gt;
 *                                                                         &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
 *                                                                           &amp;lt;complexType&amp;gt;
 *                                                                             &amp;lt;complexContent&amp;gt;
 *                                                                               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                                                                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
 *                                                                               &amp;lt;/restriction&amp;gt;
 *                                                                             &amp;lt;/complexContent&amp;gt;
 *                                                                           &amp;lt;/complexType&amp;gt;
 *                                                                         &amp;lt;/element&amp;gt;
 *                                                                       &amp;lt;/sequence&amp;gt;
 *                                                                       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
 *                                                                       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                                                       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
 *                                                                       &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
 *                                                                       &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
 *                                                                       &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                                                       &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
 *                                                                       &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
 *                                                                       &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                       &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                                       &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *                                                                       &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
 *                                                                     &amp;lt;/extension&amp;gt;
 *                                                                   &amp;lt;/complexContent&amp;gt;
 *                                                                 &amp;lt;/complexType&amp;gt;
 *                                                               &amp;lt;/element&amp;gt;
 *                                                             &amp;lt;/sequence&amp;gt;
 *                                                           &amp;lt;/restriction&amp;gt;
 *                                                         &amp;lt;/complexContent&amp;gt;
 *                                                       &amp;lt;/complexType&amp;gt;
 *                                                     &amp;lt;/element&amp;gt;
 *                                                   &amp;lt;/sequence&amp;gt;
 *                                                 &amp;lt;/restriction&amp;gt;
 *                                               &amp;lt;/complexContent&amp;gt;
 *                                             &amp;lt;/complexType&amp;gt;
 *                                           &amp;lt;/element&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                         &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                         &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                       &amp;lt;/extension&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/choice&amp;gt;
 *                               &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}AirComponentSearchType" /&amp;gt;
 *                             &amp;lt;/extension&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="HotelResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="RoomStays" minOccurs="0"&amp;gt;
 *                                   &amp;lt;complexType&amp;gt;
 *                                     &amp;lt;complexContent&amp;gt;
 *                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                                         &amp;lt;sequence&amp;gt;
 *                                           &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
 *                                             &amp;lt;complexType&amp;gt;
 *                                               &amp;lt;complexContent&amp;gt;
 *                                                 &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
 *                                                   &amp;lt;sequence&amp;gt;
 *                                                     &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
 *                                                       &amp;lt;complexType&amp;gt;
 *                                                         &amp;lt;complexContent&amp;gt;
 *                                                           &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
 *                                                             &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
 *                                                           &amp;lt;/extension&amp;gt;
 *                                                         &amp;lt;/complexContent&amp;gt;
 *                                                       &amp;lt;/complexType&amp;gt;
 *                                                     &amp;lt;/element&amp;gt;
 *                                                     &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
 *                                                   &amp;lt;/sequence&amp;gt;
 *                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
 *                                                   &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                   &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
 *                                                   &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                   &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                                   &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
 *                                                   &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
 *                                                   &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *                                                 &amp;lt;/extension&amp;gt;
 *                                               &amp;lt;/complexContent&amp;gt;
 *                                             &amp;lt;/complexType&amp;gt;
 *                                           &amp;lt;/element&amp;gt;
 *                                         &amp;lt;/sequence&amp;gt;
 *                                         &amp;lt;attribute name="MoreIndicator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
 *                                       &amp;lt;/restriction&amp;gt;
 *                                     &amp;lt;/complexContent&amp;gt;
 *                                   &amp;lt;/complexType&amp;gt;
 *                                 &amp;lt;/element&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}HotelComponentSearchType" /&amp;gt;
 *                             &amp;lt;/extension&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="PackageOptionResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="PackageOptionGroup" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionGroupType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionComponentSearchType" /&amp;gt;
 *                             &amp;lt;/extension&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                       &amp;lt;element name="CarResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
 *                         &amp;lt;complexType&amp;gt;
 *                           &amp;lt;complexContent&amp;gt;
 *                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
 *                               &amp;lt;sequence&amp;gt;
 *                                 &amp;lt;element name="VehAvailRSCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailRSCoreType"/&amp;gt;
 *                               &amp;lt;/sequence&amp;gt;
 *                               &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}CarComponentSearchType" /&amp;gt;
 *                             &amp;lt;/extension&amp;gt;
 *                           &amp;lt;/complexContent&amp;gt;
 *                         &amp;lt;/complexType&amp;gt;
 *                       &amp;lt;/element&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *             &amp;lt;element name="DynamicPackage" type="{http://www.opentravel.org/OTA/2003/05}DynamicPkgType" minOccurs="0"/&amp;gt;
 *             &amp;lt;element name="InclusionSets" minOccurs="0"&amp;gt;
 *               &amp;lt;complexType&amp;gt;
 *                 &amp;lt;complexContent&amp;gt;
 *                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *                     &amp;lt;sequence&amp;gt;
 *                       &amp;lt;element name="InclusionSet" type="{http://www.opentravel.org/OTA/2003/05}InclusionSetType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *                     &amp;lt;/sequence&amp;gt;
 *                   &amp;lt;/restriction&amp;gt;
 *                 &amp;lt;/complexContent&amp;gt;
 *               &amp;lt;/complexType&amp;gt;
 *             &amp;lt;/element&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *           &amp;lt;element name="Errors" type="{http://www.opentravel.org/OTA/2003/05}ErrorsType"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}OTA_PayloadStdAttributes"/&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pos",
    "success",
    "warnings",
    "searchResults",
    "dynamicPackage",
    "inclusionSets",
    "errors"
})
@XmlRootElement(name = "OTA_DynamicPkgAvailRS")
public class OTADynamicPkgAvailRS {

    @XmlElement(name = "POS")
    protected POSType pos;
    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "SearchResults")
    protected OTADynamicPkgAvailRS.SearchResults searchResults;
    @XmlElement(name = "DynamicPackage")
    protected DynamicPkgType dynamicPackage;
    @XmlElement(name = "InclusionSets")
    protected OTADynamicPkgAvailRS.InclusionSets inclusionSets;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    protected String target;
    @XmlAttribute(name = "TargetName")
    protected String targetName;
    @XmlAttribute(name = "Version", required = true)
    protected BigDecimal version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;

    /**
     * Obtiene el valor de la propiedad pos.
     * 
     * @return
     *     possible object is
     *     {@link POSType }
     *     
     */
    public POSType getPOS() {
        return pos;
    }

    /**
     * Define el valor de la propiedad pos.
     * 
     * @param value
     *     allowed object is
     *     {@link POSType }
     *     
     */
    public void setPOS(POSType value) {
        this.pos = value;
    }

    /**
     * Obtiene el valor de la propiedad success.
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * Define el valor de la propiedad success.
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * Obtiene el valor de la propiedad warnings.
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * Define el valor de la propiedad warnings.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * Obtiene el valor de la propiedad searchResults.
     * 
     * @return
     *     possible object is
     *     {@link OTADynamicPkgAvailRS.SearchResults }
     *     
     */
    public OTADynamicPkgAvailRS.SearchResults getSearchResults() {
        return searchResults;
    }

    /**
     * Define el valor de la propiedad searchResults.
     * 
     * @param value
     *     allowed object is
     *     {@link OTADynamicPkgAvailRS.SearchResults }
     *     
     */
    public void setSearchResults(OTADynamicPkgAvailRS.SearchResults value) {
        this.searchResults = value;
    }

    /**
     * Obtiene el valor de la propiedad dynamicPackage.
     * 
     * @return
     *     possible object is
     *     {@link DynamicPkgType }
     *     
     */
    public DynamicPkgType getDynamicPackage() {
        return dynamicPackage;
    }

    /**
     * Define el valor de la propiedad dynamicPackage.
     * 
     * @param value
     *     allowed object is
     *     {@link DynamicPkgType }
     *     
     */
    public void setDynamicPackage(DynamicPkgType value) {
        this.dynamicPackage = value;
    }

    /**
     * Obtiene el valor de la propiedad inclusionSets.
     * 
     * @return
     *     possible object is
     *     {@link OTADynamicPkgAvailRS.InclusionSets }
     *     
     */
    public OTADynamicPkgAvailRS.InclusionSets getInclusionSets() {
        return inclusionSets;
    }

    /**
     * Define el valor de la propiedad inclusionSets.
     * 
     * @param value
     *     allowed object is
     *     {@link OTADynamicPkgAvailRS.InclusionSets }
     *     
     */
    public void setInclusionSets(OTADynamicPkgAvailRS.InclusionSets value) {
        this.inclusionSets = value;
    }

    /**
     * Obtiene el valor de la propiedad errors.
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * Define el valor de la propiedad errors.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * Obtiene el valor de la propiedad echoToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * Define el valor de la propiedad echoToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * Obtiene el valor de la propiedad timeStamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Define el valor de la propiedad timeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad target.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Define el valor de la propiedad target.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Obtiene el valor de la propiedad targetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * Define el valor de la propiedad targetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetName(String value) {
        this.targetName = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * Define el valor de la propiedad transactionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceNmbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * Define el valor de la propiedad sequenceNmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * Define el valor de la propiedad transactionStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad retransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad retransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad correlationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Define el valor de la propiedad correlationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * Define el valor de la propiedad primaryLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }

    /**
     * Obtiene el valor de la propiedad altLangID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * Define el valor de la propiedad altLangID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="InclusionSet" type="{http://www.opentravel.org/OTA/2003/05}InclusionSetType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "inclusionSet"
    })
    public static class InclusionSets {

        @XmlElement(name = "InclusionSet")
        protected List<InclusionSetType> inclusionSet;

        /**
         * Gets the value of the inclusionSet property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the inclusionSet property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getInclusionSet().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link InclusionSetType }
         * 
         * 
         */
        public List<InclusionSetType> getInclusionSet() {
            if (inclusionSet == null) {
                inclusionSet = new ArrayList<InclusionSetType>();
            }
            return this.inclusionSet;
        }

    }


    /**
     * &lt;p&gt;Clase Java para anonymous complex type.
     * 
     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * &lt;pre&gt;
     * &amp;lt;complexType&amp;gt;
     *   &amp;lt;complexContent&amp;gt;
     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *       &amp;lt;sequence&amp;gt;
     *         &amp;lt;element name="AirResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
     *                 &amp;lt;choice&amp;gt;
     *                   &amp;lt;element name="PricedItineraries" type="{http://www.opentravel.org/OTA/2003/05}PricedItinerariesType" minOccurs="0"/&amp;gt;
     *                   &amp;lt;element name="OriginDestinationInformation" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="OriginDestinationOptions"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                               &amp;lt;sequence&amp;gt;
     *                                                 &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
     *                                                   &amp;lt;complexType&amp;gt;
     *                                                     &amp;lt;complexContent&amp;gt;
     *                                                       &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
     *                                                         &amp;lt;sequence&amp;gt;
     *                                                           &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;simpleContent&amp;gt;
     *                                                                 &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
     *                                                                   &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                                 &amp;lt;/extension&amp;gt;
     *                                                               &amp;lt;/simpleContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
     *                                                           &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
     *                                                                   &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                           &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
     *                                                             &amp;lt;complexType&amp;gt;
     *                                                               &amp;lt;complexContent&amp;gt;
     *                                                                 &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                                                                   &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
     *                                                                 &amp;lt;/restriction&amp;gt;
     *                                                               &amp;lt;/complexContent&amp;gt;
     *                                                             &amp;lt;/complexType&amp;gt;
     *                                                           &amp;lt;/element&amp;gt;
     *                                                         &amp;lt;/sequence&amp;gt;
     *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
     *                                                         &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                                                         &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
     *                                                         &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
     *                                                         &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
     *                                                         &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                                                         &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
     *                                                         &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
     *                                                         &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                         &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                                         &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
     *                                                         &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
     *                                                       &amp;lt;/extension&amp;gt;
     *                                                     &amp;lt;/complexContent&amp;gt;
     *                                                   &amp;lt;/complexType&amp;gt;
     *                                                 &amp;lt;/element&amp;gt;
     *                                               &amp;lt;/sequence&amp;gt;
     *                                             &amp;lt;/restriction&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                   &amp;lt;/restriction&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                         &amp;lt;/extension&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/choice&amp;gt;
     *                 &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}AirComponentSearchType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="HotelResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="RoomStays" minOccurs="0"&amp;gt;
     *                     &amp;lt;complexType&amp;gt;
     *                       &amp;lt;complexContent&amp;gt;
     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
     *                           &amp;lt;sequence&amp;gt;
     *                             &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
     *                               &amp;lt;complexType&amp;gt;
     *                                 &amp;lt;complexContent&amp;gt;
     *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
     *                                     &amp;lt;sequence&amp;gt;
     *                                       &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
     *                                         &amp;lt;complexType&amp;gt;
     *                                           &amp;lt;complexContent&amp;gt;
     *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
     *                                               &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
     *                                             &amp;lt;/extension&amp;gt;
     *                                           &amp;lt;/complexContent&amp;gt;
     *                                         &amp;lt;/complexType&amp;gt;
     *                                       &amp;lt;/element&amp;gt;
     *                                       &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
     *                                     &amp;lt;/sequence&amp;gt;
     *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
     *                                     &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                     &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
     *                                     &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                                     &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
     *                                     &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
     *                                     &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
     *                                   &amp;lt;/extension&amp;gt;
     *                                 &amp;lt;/complexContent&amp;gt;
     *                               &amp;lt;/complexType&amp;gt;
     *                             &amp;lt;/element&amp;gt;
     *                           &amp;lt;/sequence&amp;gt;
     *                           &amp;lt;attribute name="MoreIndicator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
     *                         &amp;lt;/restriction&amp;gt;
     *                       &amp;lt;/complexContent&amp;gt;
     *                     &amp;lt;/complexType&amp;gt;
     *                   &amp;lt;/element&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}HotelComponentSearchType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="PackageOptionResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="PackageOptionGroup" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionGroupType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionComponentSearchType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *         &amp;lt;element name="CarResults" maxOccurs="unbounded" minOccurs="0"&amp;gt;
     *           &amp;lt;complexType&amp;gt;
     *             &amp;lt;complexContent&amp;gt;
     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
     *                 &amp;lt;sequence&amp;gt;
     *                   &amp;lt;element name="VehAvailRSCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailRSCoreType"/&amp;gt;
     *                 &amp;lt;/sequence&amp;gt;
     *                 &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}CarComponentSearchType" /&amp;gt;
     *               &amp;lt;/extension&amp;gt;
     *             &amp;lt;/complexContent&amp;gt;
     *           &amp;lt;/complexType&amp;gt;
     *         &amp;lt;/element&amp;gt;
     *       &amp;lt;/sequence&amp;gt;
     *     &amp;lt;/restriction&amp;gt;
     *   &amp;lt;/complexContent&amp;gt;
     * &amp;lt;/complexType&amp;gt;
     * &lt;/pre&gt;
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airResults",
        "hotelResults",
        "packageOptionResults",
        "carResults"
    })
    public static class SearchResults {

        @XmlElement(name = "AirResults")
        protected List<OTADynamicPkgAvailRS.SearchResults.AirResults> airResults;
        @XmlElement(name = "HotelResults")
        protected List<OTADynamicPkgAvailRS.SearchResults.HotelResults> hotelResults;
        @XmlElement(name = "PackageOptionResults")
        protected List<OTADynamicPkgAvailRS.SearchResults.PackageOptionResults> packageOptionResults;
        @XmlElement(name = "CarResults")
        protected List<OTADynamicPkgAvailRS.SearchResults.CarResults> carResults;

        /**
         * Gets the value of the airResults property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the airResults property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getAirResults().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTADynamicPkgAvailRS.SearchResults.AirResults }
         * 
         * 
         */
        public List<OTADynamicPkgAvailRS.SearchResults.AirResults> getAirResults() {
            if (airResults == null) {
                airResults = new ArrayList<OTADynamicPkgAvailRS.SearchResults.AirResults>();
            }
            return this.airResults;
        }

        /**
         * Gets the value of the hotelResults property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the hotelResults property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getHotelResults().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTADynamicPkgAvailRS.SearchResults.HotelResults }
         * 
         * 
         */
        public List<OTADynamicPkgAvailRS.SearchResults.HotelResults> getHotelResults() {
            if (hotelResults == null) {
                hotelResults = new ArrayList<OTADynamicPkgAvailRS.SearchResults.HotelResults>();
            }
            return this.hotelResults;
        }

        /**
         * Gets the value of the packageOptionResults property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the packageOptionResults property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getPackageOptionResults().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTADynamicPkgAvailRS.SearchResults.PackageOptionResults }
         * 
         * 
         */
        public List<OTADynamicPkgAvailRS.SearchResults.PackageOptionResults> getPackageOptionResults() {
            if (packageOptionResults == null) {
                packageOptionResults = new ArrayList<OTADynamicPkgAvailRS.SearchResults.PackageOptionResults>();
            }
            return this.packageOptionResults;
        }

        /**
         * Gets the value of the carResults property.
         * 
         * &lt;p&gt;
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the carResults property.
         * 
         * &lt;p&gt;
         * For example, to add a new item, do as follows:
         * &lt;pre&gt;
         *    getCarResults().add(newItem);
         * &lt;/pre&gt;
         * 
         * 
         * &lt;p&gt;
         * Objects of the following type(s) are allowed in the list
         * {@link OTADynamicPkgAvailRS.SearchResults.CarResults }
         * 
         * 
         */
        public List<OTADynamicPkgAvailRS.SearchResults.CarResults> getCarResults() {
            if (carResults == null) {
                carResults = new ArrayList<OTADynamicPkgAvailRS.SearchResults.CarResults>();
            }
            return this.carResults;
        }


        /**
         * A collection of available air inventory.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
         *       &amp;lt;choice&amp;gt;
         *         &amp;lt;element name="PricedItineraries" type="{http://www.opentravel.org/OTA/2003/05}PricedItinerariesType" minOccurs="0"/&amp;gt;
         *         &amp;lt;element name="OriginDestinationInformation" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="OriginDestinationOptions"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                     &amp;lt;sequence&amp;gt;
         *                                       &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
         *                                         &amp;lt;complexType&amp;gt;
         *                                           &amp;lt;complexContent&amp;gt;
         *                                             &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
         *                                               &amp;lt;sequence&amp;gt;
         *                                                 &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;simpleContent&amp;gt;
         *                                                       &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
         *                                                         &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                                       &amp;lt;/extension&amp;gt;
         *                                                     &amp;lt;/simpleContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
         *                                                 &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
         *                                                         &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                                 &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
         *                                                   &amp;lt;complexType&amp;gt;
         *                                                     &amp;lt;complexContent&amp;gt;
         *                                                       &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                                                         &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
         *                                                       &amp;lt;/restriction&amp;gt;
         *                                                     &amp;lt;/complexContent&amp;gt;
         *                                                   &amp;lt;/complexType&amp;gt;
         *                                                 &amp;lt;/element&amp;gt;
         *                                               &amp;lt;/sequence&amp;gt;
         *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
         *                                               &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                                               &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
         *                                               &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
         *                                               &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
         *                                               &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                                               &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
         *                                               &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
         *                                               &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                               &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                                               &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
         *                                               &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
         *                                             &amp;lt;/extension&amp;gt;
         *                                           &amp;lt;/complexContent&amp;gt;
         *                                         &amp;lt;/complexType&amp;gt;
         *                                       &amp;lt;/element&amp;gt;
         *                                     &amp;lt;/sequence&amp;gt;
         *                                   &amp;lt;/restriction&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                         &amp;lt;/restriction&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *               &amp;lt;/extension&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/choice&amp;gt;
         *       &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}AirComponentSearchType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pricedItineraries",
            "originDestinationInformation"
        })
        public static class AirResults
            extends DynamicPkgResponseType
        {

            @XmlElement(name = "PricedItineraries")
            protected PricedItinerariesType pricedItineraries;
            @XmlElement(name = "OriginDestinationInformation")
            protected OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation originDestinationInformation;
            @XmlAttribute(name = "ResponseType")
            protected AirComponentSearchType responseType;

            /**
             * Obtiene el valor de la propiedad pricedItineraries.
             * 
             * @return
             *     possible object is
             *     {@link PricedItinerariesType }
             *     
             */
            public PricedItinerariesType getPricedItineraries() {
                return pricedItineraries;
            }

            /**
             * Define el valor de la propiedad pricedItineraries.
             * 
             * @param value
             *     allowed object is
             *     {@link PricedItinerariesType }
             *     
             */
            public void setPricedItineraries(PricedItinerariesType value) {
                this.pricedItineraries = value;
            }

            /**
             * Obtiene el valor de la propiedad originDestinationInformation.
             * 
             * @return
             *     possible object is
             *     {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation }
             *     
             */
            public OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation getOriginDestinationInformation() {
                return originDestinationInformation;
            }

            /**
             * Define el valor de la propiedad originDestinationInformation.
             * 
             * @param value
             *     allowed object is
             *     {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation }
             *     
             */
            public void setOriginDestinationInformation(OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation value) {
                this.originDestinationInformation = value;
            }

            /**
             * Obtiene el valor de la propiedad responseType.
             * 
             * @return
             *     possible object is
             *     {@link AirComponentSearchType }
             *     
             */
            public AirComponentSearchType getResponseType() {
                return responseType;
            }

            /**
             * Define el valor de la propiedad responseType.
             * 
             * @param value
             *     allowed object is
             *     {@link AirComponentSearchType }
             *     
             */
            public void setResponseType(AirComponentSearchType value) {
                this.responseType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}OriginDestinationInformationType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="OriginDestinationOptions"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                           &amp;lt;sequence&amp;gt;
             *                             &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
             *                               &amp;lt;complexType&amp;gt;
             *                                 &amp;lt;complexContent&amp;gt;
             *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
             *                                     &amp;lt;sequence&amp;gt;
             *                                       &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;simpleContent&amp;gt;
             *                                             &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
             *                                               &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                             &amp;lt;/extension&amp;gt;
             *                                           &amp;lt;/simpleContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
             *                                       &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
             *                                               &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                       &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
             *                                         &amp;lt;complexType&amp;gt;
             *                                           &amp;lt;complexContent&amp;gt;
             *                                             &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *                                               &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
             *                                             &amp;lt;/restriction&amp;gt;
             *                                           &amp;lt;/complexContent&amp;gt;
             *                                         &amp;lt;/complexType&amp;gt;
             *                                       &amp;lt;/element&amp;gt;
             *                                     &amp;lt;/sequence&amp;gt;
             *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
             *                                     &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *                                     &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
             *                                     &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
             *                                     &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
             *                                     &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *                                     &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
             *                                     &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
             *                                     &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                     &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                                     &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
             *                                     &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
             *                                   &amp;lt;/extension&amp;gt;
             *                                 &amp;lt;/complexContent&amp;gt;
             *                               &amp;lt;/complexType&amp;gt;
             *                             &amp;lt;/element&amp;gt;
             *                           &amp;lt;/sequence&amp;gt;
             *                         &amp;lt;/restriction&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *               &amp;lt;/restriction&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="SameAirportInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *     &amp;lt;/extension&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "originDestinationOptions"
            })
            public static class OriginDestinationInformation
                extends OriginDestinationInformationType
            {

                @XmlElement(name = "OriginDestinationOptions", required = true)
                protected OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions;
                @XmlAttribute(name = "SameAirportInd")
                protected Boolean sameAirportInd;
                @XmlAttribute(name = "RPH")
                protected String rph;

                /**
                 * Obtiene el valor de la propiedad originDestinationOptions.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions }
                 *     
                 */
                public OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions getOriginDestinationOptions() {
                    return originDestinationOptions;
                }

                /**
                 * Define el valor de la propiedad originDestinationOptions.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions }
                 *     
                 */
                public void setOriginDestinationOptions(OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions value) {
                    this.originDestinationOptions = value;
                }

                /**
                 * Obtiene el valor de la propiedad sameAirportInd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isSameAirportInd() {
                    return sameAirportInd;
                }

                /**
                 * Define el valor de la propiedad sameAirportInd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setSameAirportInd(Boolean value) {
                    this.sameAirportInd = value;
                }

                /**
                 * Obtiene el valor de la propiedad rph.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRPH() {
                    return rph;
                }

                /**
                 * Define el valor de la propiedad rph.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRPH(String value) {
                    this.rph = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="OriginDestinationOption" maxOccurs="999"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                 &amp;lt;sequence&amp;gt;
                 *                   &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
                 *                     &amp;lt;complexType&amp;gt;
                 *                       &amp;lt;complexContent&amp;gt;
                 *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
                 *                           &amp;lt;sequence&amp;gt;
                 *                             &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;simpleContent&amp;gt;
                 *                                   &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
                 *                                     &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                                   &amp;lt;/extension&amp;gt;
                 *                                 &amp;lt;/simpleContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
                 *                             &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
                 *                                     &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                             &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
                 *                               &amp;lt;complexType&amp;gt;
                 *                                 &amp;lt;complexContent&amp;gt;
                 *                                   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                 *                                     &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
                 *                                   &amp;lt;/restriction&amp;gt;
                 *                                 &amp;lt;/complexContent&amp;gt;
                 *                               &amp;lt;/complexType&amp;gt;
                 *                             &amp;lt;/element&amp;gt;
                 *                           &amp;lt;/sequence&amp;gt;
                 *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
                 *                           &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                 *                           &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
                 *                           &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
                 *                           &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                 *                           &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                 *                           &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                 *                           &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                 *                           &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                           &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *                           &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                 *                           &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
                 *                         &amp;lt;/extension&amp;gt;
                 *                       &amp;lt;/complexContent&amp;gt;
                 *                     &amp;lt;/complexType&amp;gt;
                 *                   &amp;lt;/element&amp;gt;
                 *                 &amp;lt;/sequence&amp;gt;
                 *               &amp;lt;/restriction&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *     &amp;lt;/restriction&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "originDestinationOption"
                })
                public static class OriginDestinationOptions {

                    @XmlElement(name = "OriginDestinationOption", required = true)
                    protected List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption> originDestinationOption;

                    /**
                     * Gets the value of the originDestinationOption property.
                     * 
                     * &lt;p&gt;
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the originDestinationOption property.
                     * 
                     * &lt;p&gt;
                     * For example, to add a new item, do as follows:
                     * &lt;pre&gt;
                     *    getOriginDestinationOption().add(newItem);
                     * &lt;/pre&gt;
                     * 
                     * 
                     * &lt;p&gt;
                     * Objects of the following type(s) are allowed in the list
                     * {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption }
                     * 
                     * 
                     */
                    public List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption> getOriginDestinationOption() {
                        if (originDestinationOption == null) {
                            originDestinationOption = new ArrayList<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption>();
                        }
                        return this.originDestinationOption;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *       &amp;lt;sequence&amp;gt;
                     *         &amp;lt;element name="FlightSegment" maxOccurs="8"&amp;gt;
                     *           &amp;lt;complexType&amp;gt;
                     *             &amp;lt;complexContent&amp;gt;
                     *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
                     *                 &amp;lt;sequence&amp;gt;
                     *                   &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;simpleContent&amp;gt;
                     *                         &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
                     *                           &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                         &amp;lt;/extension&amp;gt;
                     *                       &amp;lt;/simpleContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
                     *                   &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
                     *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                   &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
                     *                     &amp;lt;complexType&amp;gt;
                     *                       &amp;lt;complexContent&amp;gt;
                     *                         &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                     *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
                     *                         &amp;lt;/restriction&amp;gt;
                     *                       &amp;lt;/complexContent&amp;gt;
                     *                     &amp;lt;/complexType&amp;gt;
                     *                   &amp;lt;/element&amp;gt;
                     *                 &amp;lt;/sequence&amp;gt;
                     *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
                     *                 &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                     *                 &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
                     *                 &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
                     *                 &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                     *                 &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                     *                 &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                     *                 &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                     *                 &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *                 &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                     *                 &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                     *                 &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
                     *               &amp;lt;/extension&amp;gt;
                     *             &amp;lt;/complexContent&amp;gt;
                     *           &amp;lt;/complexType&amp;gt;
                     *         &amp;lt;/element&amp;gt;
                     *       &amp;lt;/sequence&amp;gt;
                     *     &amp;lt;/restriction&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "flightSegment"
                    })
                    public static class OriginDestinationOption {

                        @XmlElement(name = "FlightSegment", required = true)
                        protected List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment> flightSegment;

                        /**
                         * Gets the value of the flightSegment property.
                         * 
                         * &lt;p&gt;
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the flightSegment property.
                         * 
                         * &lt;p&gt;
                         * For example, to add a new item, do as follows:
                         * &lt;pre&gt;
                         *    getFlightSegment().add(newItem);
                         * &lt;/pre&gt;
                         * 
                         * 
                         * &lt;p&gt;
                         * Objects of the following type(s) are allowed in the list
                         * {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment }
                         * 
                         * 
                         */
                        public List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment> getFlightSegment() {
                            if (flightSegment == null) {
                                flightSegment = new ArrayList<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment>();
                            }
                            return this.flightSegment;
                        }


                        /**
                         *  Flight segment information returned for an availability request including ancillary information.
                         * 
                         * &lt;p&gt;Clase Java para anonymous complex type.
                         * 
                         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * &lt;pre&gt;
                         * &amp;lt;complexType&amp;gt;
                         *   &amp;lt;complexContent&amp;gt;
                         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}FlightSegmentType"&amp;gt;
                         *       &amp;lt;sequence&amp;gt;
                         *         &amp;lt;element name="TrafficRestrictionInfo" maxOccurs="99" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;simpleContent&amp;gt;
                         *               &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
                         *                 &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *               &amp;lt;/extension&amp;gt;
                         *             &amp;lt;/simpleContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="Comment" type="{http://www.opentravel.org/OTA/2003/05}FreeTextType" maxOccurs="10" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="MarketingCabin" type="{http://www.opentravel.org/OTA/2003/05}MarketingCabinType" maxOccurs="9" minOccurs="0"/&amp;gt;
                         *         &amp;lt;element name="BookingClassAvail" maxOccurs="99" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
                         *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *         &amp;lt;element name="StopLocation" maxOccurs="9" minOccurs="0"&amp;gt;
                         *           &amp;lt;complexType&amp;gt;
                         *             &amp;lt;complexContent&amp;gt;
                         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                         *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
                         *               &amp;lt;/restriction&amp;gt;
                         *             &amp;lt;/complexContent&amp;gt;
                         *           &amp;lt;/complexType&amp;gt;
                         *         &amp;lt;/element&amp;gt;
                         *       &amp;lt;/sequence&amp;gt;
                         *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}SmokingIndicatorGroup"/&amp;gt;
                         *       &amp;lt;attribute name="JourneyDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                         *       &amp;lt;attribute name="OnTimeRate" type="{http://www.opentravel.org/OTA/2003/05}Percentage" /&amp;gt;
                         *       &amp;lt;attribute name="Ticket" type="{http://www.opentravel.org/OTA/2003/05}TicketType" /&amp;gt;
                         *       &amp;lt;attribute name="ParticipationLevelCode" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                         *       &amp;lt;attribute name="GroundDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                         *       &amp;lt;attribute name="AccumulatedDuration" type="{http://www.w3.org/2001/XMLSchema}duration" /&amp;gt;
                         *       &amp;lt;attribute name="Distance" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&amp;gt;
                         *       &amp;lt;attribute name="CodeshareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *       &amp;lt;attribute name="FlifoInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                         *       &amp;lt;attribute name="DateChangeNbr" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
                         *       &amp;lt;attribute name="SequenceNumber" type="{http://www.opentravel.org/OTA/2003/05}Numeric1to999" /&amp;gt;
                         *     &amp;lt;/extension&amp;gt;
                         *   &amp;lt;/complexContent&amp;gt;
                         * &amp;lt;/complexType&amp;gt;
                         * &lt;/pre&gt;
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "trafficRestrictionInfo",
                            "comment",
                            "marketingCabin",
                            "bookingClassAvail",
                            "stopLocation"
                        })
                        public static class FlightSegment
                            extends FlightSegmentType
                        {

                            @XmlElement(name = "TrafficRestrictionInfo")
                            protected List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo> trafficRestrictionInfo;
                            @XmlElement(name = "Comment")
                            protected List<FreeTextType> comment;
                            @XmlElement(name = "MarketingCabin")
                            protected List<MarketingCabinType> marketingCabin;
                            @XmlElement(name = "BookingClassAvail")
                            protected List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail> bookingClassAvail;
                            @XmlElement(name = "StopLocation")
                            protected List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation> stopLocation;
                            @XmlAttribute(name = "JourneyDuration")
                            protected Duration journeyDuration;
                            @XmlAttribute(name = "OnTimeRate")
                            protected BigDecimal onTimeRate;
                            @XmlAttribute(name = "Ticket")
                            protected TicketType ticket;
                            @XmlAttribute(name = "ParticipationLevelCode")
                            protected String participationLevelCode;
                            @XmlAttribute(name = "GroundDuration")
                            protected Duration groundDuration;
                            @XmlAttribute(name = "AccumulatedDuration")
                            protected Duration accumulatedDuration;
                            @XmlAttribute(name = "Distance")
                            @XmlSchemaType(name = "nonNegativeInteger")
                            protected BigInteger distance;
                            @XmlAttribute(name = "CodeshareInd")
                            protected Boolean codeshareInd;
                            @XmlAttribute(name = "FlifoInd")
                            protected Boolean flifoInd;
                            @XmlAttribute(name = "DateChangeNbr")
                            protected String dateChangeNbr;
                            @XmlAttribute(name = "SequenceNumber")
                            protected Integer sequenceNumber;
                            @XmlAttribute(name = "SmokingAllowed")
                            protected Boolean smokingAllowed;

                            /**
                             * Gets the value of the trafficRestrictionInfo property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the trafficRestrictionInfo property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getTrafficRestrictionInfo().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo }
                             * 
                             * 
                             */
                            public List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo> getTrafficRestrictionInfo() {
                                if (trafficRestrictionInfo == null) {
                                    trafficRestrictionInfo = new ArrayList<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.TrafficRestrictionInfo>();
                                }
                                return this.trafficRestrictionInfo;
                            }

                            /**
                             * Gets the value of the comment property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the comment property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getComment().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link FreeTextType }
                             * 
                             * 
                             */
                            public List<FreeTextType> getComment() {
                                if (comment == null) {
                                    comment = new ArrayList<FreeTextType>();
                                }
                                return this.comment;
                            }

                            /**
                             * Gets the value of the marketingCabin property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the marketingCabin property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getMarketingCabin().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link MarketingCabinType }
                             * 
                             * 
                             */
                            public List<MarketingCabinType> getMarketingCabin() {
                                if (marketingCabin == null) {
                                    marketingCabin = new ArrayList<MarketingCabinType>();
                                }
                                return this.marketingCabin;
                            }

                            /**
                             * Gets the value of the bookingClassAvail property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the bookingClassAvail property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getBookingClassAvail().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail }
                             * 
                             * 
                             */
                            public List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail> getBookingClassAvail() {
                                if (bookingClassAvail == null) {
                                    bookingClassAvail = new ArrayList<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail>();
                                }
                                return this.bookingClassAvail;
                            }

                            /**
                             * Gets the value of the stopLocation property.
                             * 
                             * &lt;p&gt;
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the stopLocation property.
                             * 
                             * &lt;p&gt;
                             * For example, to add a new item, do as follows:
                             * &lt;pre&gt;
                             *    getStopLocation().add(newItem);
                             * &lt;/pre&gt;
                             * 
                             * 
                             * &lt;p&gt;
                             * Objects of the following type(s) are allowed in the list
                             * {@link OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation }
                             * 
                             * 
                             */
                            public List<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation> getStopLocation() {
                                if (stopLocation == null) {
                                    stopLocation = new ArrayList<OTADynamicPkgAvailRS.SearchResults.AirResults.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation>();
                                }
                                return this.stopLocation;
                            }

                            /**
                             * Obtiene el valor de la propiedad journeyDuration.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Duration }
                             *     
                             */
                            public Duration getJourneyDuration() {
                                return journeyDuration;
                            }

                            /**
                             * Define el valor de la propiedad journeyDuration.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Duration }
                             *     
                             */
                            public void setJourneyDuration(Duration value) {
                                this.journeyDuration = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad onTimeRate.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public BigDecimal getOnTimeRate() {
                                return onTimeRate;
                            }

                            /**
                             * Define el valor de la propiedad onTimeRate.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigDecimal }
                             *     
                             */
                            public void setOnTimeRate(BigDecimal value) {
                                this.onTimeRate = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad ticket.
                             * 
                             * @return
                             *     possible object is
                             *     {@link TicketType }
                             *     
                             */
                            public TicketType getTicket() {
                                return ticket;
                            }

                            /**
                             * Define el valor de la propiedad ticket.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link TicketType }
                             *     
                             */
                            public void setTicket(TicketType value) {
                                this.ticket = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad participationLevelCode.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getParticipationLevelCode() {
                                return participationLevelCode;
                            }

                            /**
                             * Define el valor de la propiedad participationLevelCode.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setParticipationLevelCode(String value) {
                                this.participationLevelCode = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad groundDuration.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Duration }
                             *     
                             */
                            public Duration getGroundDuration() {
                                return groundDuration;
                            }

                            /**
                             * Define el valor de la propiedad groundDuration.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Duration }
                             *     
                             */
                            public void setGroundDuration(Duration value) {
                                this.groundDuration = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad accumulatedDuration.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Duration }
                             *     
                             */
                            public Duration getAccumulatedDuration() {
                                return accumulatedDuration;
                            }

                            /**
                             * Define el valor de la propiedad accumulatedDuration.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Duration }
                             *     
                             */
                            public void setAccumulatedDuration(Duration value) {
                                this.accumulatedDuration = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad distance.
                             * 
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *     
                             */
                            public BigInteger getDistance() {
                                return distance;
                            }

                            /**
                             * Define el valor de la propiedad distance.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *     
                             */
                            public void setDistance(BigInteger value) {
                                this.distance = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad codeshareInd.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isCodeshareInd() {
                                return codeshareInd;
                            }

                            /**
                             * Define el valor de la propiedad codeshareInd.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setCodeshareInd(Boolean value) {
                                this.codeshareInd = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad flifoInd.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isFlifoInd() {
                                return flifoInd;
                            }

                            /**
                             * Define el valor de la propiedad flifoInd.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setFlifoInd(Boolean value) {
                                this.flifoInd = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad dateChangeNbr.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDateChangeNbr() {
                                return dateChangeNbr;
                            }

                            /**
                             * Define el valor de la propiedad dateChangeNbr.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDateChangeNbr(String value) {
                                this.dateChangeNbr = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad sequenceNumber.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Integer }
                             *     
                             */
                            public Integer getSequenceNumber() {
                                return sequenceNumber;
                            }

                            /**
                             * Define el valor de la propiedad sequenceNumber.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Integer }
                             *     
                             */
                            public void setSequenceNumber(Integer value) {
                                this.sequenceNumber = value;
                            }

                            /**
                             * Obtiene el valor de la propiedad smokingAllowed.
                             * 
                             * @return
                             *     possible object is
                             *     {@link Boolean }
                             *     
                             */
                            public Boolean isSmokingAllowed() {
                                return smokingAllowed;
                            }

                            /**
                             * Define el valor de la propiedad smokingAllowed.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Boolean }
                             *     
                             */
                            public void setSmokingAllowed(Boolean value) {
                                this.smokingAllowed = value;
                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}BookingClassAvailabilityGroup"/&amp;gt;
                             *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class BookingClassAvail {

                                @XmlAttribute(name = "RPH")
                                protected String rph;
                                @XmlAttribute(name = "ResBookDesigCode")
                                protected String resBookDesigCode;
                                @XmlAttribute(name = "ResBookDesigQuantity")
                                protected String resBookDesigQuantity;
                                @XmlAttribute(name = "ResBookDesigStatusCode")
                                protected String resBookDesigStatusCode;

                                /**
                                 * Obtiene el valor de la propiedad rph.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getRPH() {
                                    return rph;
                                }

                                /**
                                 * Define el valor de la propiedad rph.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setRPH(String value) {
                                    this.rph = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad resBookDesigCode.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getResBookDesigCode() {
                                    return resBookDesigCode;
                                }

                                /**
                                 * Define el valor de la propiedad resBookDesigCode.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setResBookDesigCode(String value) {
                                    this.resBookDesigCode = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad resBookDesigQuantity.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getResBookDesigQuantity() {
                                    return resBookDesigQuantity;
                                }

                                /**
                                 * Define el valor de la propiedad resBookDesigQuantity.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setResBookDesigQuantity(String value) {
                                    this.resBookDesigQuantity = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad resBookDesigStatusCode.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getResBookDesigStatusCode() {
                                    return resBookDesigStatusCode;
                                }

                                /**
                                 * Define el valor de la propiedad resBookDesigStatusCode.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setResBookDesigStatusCode(String value) {
                                    this.resBookDesigStatusCode = value;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;complexContent&amp;gt;
                             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
                             *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}LocationGroup"/&amp;gt;
                             *     &amp;lt;/restriction&amp;gt;
                             *   &amp;lt;/complexContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class StopLocation {

                                @XmlAttribute(name = "LocationCode")
                                protected String locationCode;
                                @XmlAttribute(name = "CodeContext")
                                protected String codeContext;

                                /**
                                 * Obtiene el valor de la propiedad locationCode.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLocationCode() {
                                    return locationCode;
                                }

                                /**
                                 * Define el valor de la propiedad locationCode.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLocationCode(String value) {
                                    this.locationCode = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad codeContext.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getCodeContext() {
                                    return codeContext;
                                }

                                /**
                                 * Define el valor de la propiedad codeContext.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setCodeContext(String value) {
                                    this.codeContext = value;
                                }

                            }


                            /**
                             * &lt;p&gt;Clase Java para anonymous complex type.
                             * 
                             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * &lt;pre&gt;
                             * &amp;lt;complexType&amp;gt;
                             *   &amp;lt;simpleContent&amp;gt;
                             *     &amp;lt;extension base="&amp;lt;http://www.opentravel.org/OTA/2003/05&amp;gt;FreeTextType"&amp;gt;
                             *       &amp;lt;attribute name="Code" type="{http://www.opentravel.org/OTA/2003/05}OTA_CodeType" /&amp;gt;
                             *     &amp;lt;/extension&amp;gt;
                             *   &amp;lt;/simpleContent&amp;gt;
                             * &amp;lt;/complexType&amp;gt;
                             * &lt;/pre&gt;
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class TrafficRestrictionInfo
                                extends FreeTextType
                            {

                                @XmlAttribute(name = "Code")
                                protected String code;

                                /**
                                 * Obtiene el valor de la propiedad code.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getCode() {
                                    return code;
                                }

                                /**
                                 * Define el valor de la propiedad code.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setCode(String value) {
                                    this.code = value;
                                }

                            }

                        }

                    }

                }

            }

        }


        /**
         * A collection of available rental car inventory.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="VehAvailRSCore" type="{http://www.opentravel.org/OTA/2003/05}VehicleAvailRSCoreType"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}CarComponentSearchType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "vehAvailRSCore"
        })
        public static class CarResults
            extends DynamicPkgResponseType
        {

            @XmlElement(name = "VehAvailRSCore", required = true)
            protected VehicleAvailRSCoreType vehAvailRSCore;
            @XmlAttribute(name = "ResponseType")
            protected CarComponentSearchType responseType;

            /**
             * Obtiene el valor de la propiedad vehAvailRSCore.
             * 
             * @return
             *     possible object is
             *     {@link VehicleAvailRSCoreType }
             *     
             */
            public VehicleAvailRSCoreType getVehAvailRSCore() {
                return vehAvailRSCore;
            }

            /**
             * Define el valor de la propiedad vehAvailRSCore.
             * 
             * @param value
             *     allowed object is
             *     {@link VehicleAvailRSCoreType }
             *     
             */
            public void setVehAvailRSCore(VehicleAvailRSCoreType value) {
                this.vehAvailRSCore = value;
            }

            /**
             * Obtiene el valor de la propiedad responseType.
             * 
             * @return
             *     possible object is
             *     {@link CarComponentSearchType }
             *     
             */
            public CarComponentSearchType getResponseType() {
                return responseType;
            }

            /**
             * Define el valor de la propiedad responseType.
             * 
             * @param value
             *     allowed object is
             *     {@link CarComponentSearchType }
             *     
             */
            public void setResponseType(CarComponentSearchType value) {
                this.responseType = value;
            }

        }


        /**
         * A collection of available hotel inventory.
         * 
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="RoomStays" minOccurs="0"&amp;gt;
         *           &amp;lt;complexType&amp;gt;
         *             &amp;lt;complexContent&amp;gt;
         *               &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
         *                 &amp;lt;sequence&amp;gt;
         *                   &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
         *                     &amp;lt;complexType&amp;gt;
         *                       &amp;lt;complexContent&amp;gt;
         *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
         *                           &amp;lt;sequence&amp;gt;
         *                             &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
         *                               &amp;lt;complexType&amp;gt;
         *                                 &amp;lt;complexContent&amp;gt;
         *                                   &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
         *                                     &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
         *                                   &amp;lt;/extension&amp;gt;
         *                                 &amp;lt;/complexContent&amp;gt;
         *                               &amp;lt;/complexType&amp;gt;
         *                             &amp;lt;/element&amp;gt;
         *                             &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
         *                           &amp;lt;/sequence&amp;gt;
         *                           &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
         *                           &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                           &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
         *                           &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *                           &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
         *                           &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
         *                           &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
         *                         &amp;lt;/extension&amp;gt;
         *                       &amp;lt;/complexContent&amp;gt;
         *                     &amp;lt;/complexType&amp;gt;
         *                   &amp;lt;/element&amp;gt;
         *                 &amp;lt;/sequence&amp;gt;
         *                 &amp;lt;attribute name="MoreIndicator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
         *               &amp;lt;/restriction&amp;gt;
         *             &amp;lt;/complexContent&amp;gt;
         *           &amp;lt;/complexType&amp;gt;
         *         &amp;lt;/element&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}HotelComponentSearchType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "roomStays"
        })
        public static class HotelResults
            extends DynamicPkgResponseType
        {

            @XmlElement(name = "RoomStays")
            protected OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays roomStays;
            @XmlAttribute(name = "ResponseType")
            protected HotelComponentSearchType responseType;

            /**
             * Obtiene el valor de la propiedad roomStays.
             * 
             * @return
             *     possible object is
             *     {@link OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays }
             *     
             */
            public OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays getRoomStays() {
                return roomStays;
            }

            /**
             * Define el valor de la propiedad roomStays.
             * 
             * @param value
             *     allowed object is
             *     {@link OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays }
             *     
             */
            public void setRoomStays(OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays value) {
                this.roomStays = value;
            }

            /**
             * Obtiene el valor de la propiedad responseType.
             * 
             * @return
             *     possible object is
             *     {@link HotelComponentSearchType }
             *     
             */
            public HotelComponentSearchType getResponseType() {
                return responseType;
            }

            /**
             * Define el valor de la propiedad responseType.
             * 
             * @param value
             *     allowed object is
             *     {@link HotelComponentSearchType }
             *     
             */
            public void setResponseType(HotelComponentSearchType value) {
                this.responseType = value;
            }


            /**
             * &lt;p&gt;Clase Java para anonymous complex type.
             * 
             * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * &lt;pre&gt;
             * &amp;lt;complexType&amp;gt;
             *   &amp;lt;complexContent&amp;gt;
             *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
             *       &amp;lt;sequence&amp;gt;
             *         &amp;lt;element name="RoomStay" maxOccurs="unbounded"&amp;gt;
             *           &amp;lt;complexType&amp;gt;
             *             &amp;lt;complexContent&amp;gt;
             *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
             *                 &amp;lt;sequence&amp;gt;
             *                   &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
             *                     &amp;lt;complexType&amp;gt;
             *                       &amp;lt;complexContent&amp;gt;
             *                         &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
             *                           &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
             *                         &amp;lt;/extension&amp;gt;
             *                       &amp;lt;/complexContent&amp;gt;
             *                     &amp;lt;/complexType&amp;gt;
             *                   &amp;lt;/element&amp;gt;
             *                   &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
             *                 &amp;lt;/sequence&amp;gt;
             *                 &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
             *                 &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *                 &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
             *                 &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *                 &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
             *                 &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
             *                 &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
             *               &amp;lt;/extension&amp;gt;
             *             &amp;lt;/complexContent&amp;gt;
             *           &amp;lt;/complexType&amp;gt;
             *         &amp;lt;/element&amp;gt;
             *       &amp;lt;/sequence&amp;gt;
             *       &amp;lt;attribute name="MoreIndicator" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
             *     &amp;lt;/restriction&amp;gt;
             *   &amp;lt;/complexContent&amp;gt;
             * &amp;lt;/complexType&amp;gt;
             * &lt;/pre&gt;
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "roomStay"
            })
            public static class RoomStays {

                @XmlElement(name = "RoomStay", required = true)
                protected List<OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay> roomStay;
                @XmlAttribute(name = "MoreIndicator")
                protected String moreIndicator;

                /**
                 * Gets the value of the roomStay property.
                 * 
                 * &lt;p&gt;
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the roomStay property.
                 * 
                 * &lt;p&gt;
                 * For example, to add a new item, do as follows:
                 * &lt;pre&gt;
                 *    getRoomStay().add(newItem);
                 * &lt;/pre&gt;
                 * 
                 * 
                 * &lt;p&gt;
                 * Objects of the following type(s) are allowed in the list
                 * {@link OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay }
                 * 
                 * 
                 */
                public List<OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay> getRoomStay() {
                    if (roomStay == null) {
                        roomStay = new ArrayList<OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay>();
                    }
                    return this.roomStay;
                }

                /**
                 * Obtiene el valor de la propiedad moreIndicator.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMoreIndicator() {
                    return moreIndicator;
                }

                /**
                 * Define el valor de la propiedad moreIndicator.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMoreIndicator(String value) {
                    this.moreIndicator = value;
                }


                /**
                 * &lt;p&gt;Clase Java para anonymous complex type.
                 * 
                 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * &lt;pre&gt;
                 * &amp;lt;complexType&amp;gt;
                 *   &amp;lt;complexContent&amp;gt;
                 *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}RoomStayType"&amp;gt;
                 *       &amp;lt;sequence&amp;gt;
                 *         &amp;lt;element name="Reference" minOccurs="0"&amp;gt;
                 *           &amp;lt;complexType&amp;gt;
                 *             &amp;lt;complexContent&amp;gt;
                 *               &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
                 *                 &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
                 *               &amp;lt;/extension&amp;gt;
                 *             &amp;lt;/complexContent&amp;gt;
                 *           &amp;lt;/complexType&amp;gt;
                 *         &amp;lt;/element&amp;gt;
                 *         &amp;lt;element name="ServiceRPHs" type="{http://www.opentravel.org/OTA/2003/05}ServiceRPHsType" minOccurs="0"/&amp;gt;
                 *       &amp;lt;/sequence&amp;gt;
                 *       &amp;lt;attGroup ref="{http://www.opentravel.org/OTA/2003/05}ResponseTypeGroup"/&amp;gt;
                 *       &amp;lt;attribute name="IsAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *       &amp;lt;attribute name="AvailabilityStatus" type="{http://www.opentravel.org/OTA/2003/05}RateIndicatorType" /&amp;gt;
                 *       &amp;lt;attribute name="RoomStayCandidateRPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="MoreDataEchoToken" type="{http://www.opentravel.org/OTA/2003/05}StringLength1to128" /&amp;gt;
                 *       &amp;lt;attribute name="InfoSource" type="{http://www.opentravel.org/OTA/2003/05}InfoSourceType" /&amp;gt;
                 *       &amp;lt;attribute name="RPH" type="{http://www.opentravel.org/OTA/2003/05}RPH_Type" /&amp;gt;
                 *       &amp;lt;attribute name="AvailableIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
                 *     &amp;lt;/extension&amp;gt;
                 *   &amp;lt;/complexContent&amp;gt;
                 * &amp;lt;/complexType&amp;gt;
                 * &lt;/pre&gt;
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "reference",
                    "serviceRPHs"
                })
                public static class RoomStay
                    extends RoomStayType
                {

                    @XmlElement(name = "Reference")
                    protected OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay.Reference reference;
                    @XmlElement(name = "ServiceRPHs")
                    protected ServiceRPHsType serviceRPHs;
                    @XmlAttribute(name = "IsAlternate")
                    protected Boolean isAlternate;
                    @XmlAttribute(name = "AvailabilityStatus")
                    protected RateIndicatorType availabilityStatus;
                    @XmlAttribute(name = "RoomStayCandidateRPH")
                    protected String roomStayCandidateRPH;
                    @XmlAttribute(name = "MoreDataEchoToken")
                    protected String moreDataEchoToken;
                    @XmlAttribute(name = "InfoSource")
                    protected String infoSource;
                    @XmlAttribute(name = "RPH")
                    protected String rph;
                    @XmlAttribute(name = "AvailableIndicator")
                    protected Boolean availableIndicator;
                    @XmlAttribute(name = "ResponseType")
                    protected String responseType;

                    /**
                     * Obtiene el valor de la propiedad reference.
                     * 
                     * @return
                     *     possible object is
                     *     {@link OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay.Reference }
                     *     
                     */
                    public OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay.Reference getReference() {
                        return reference;
                    }

                    /**
                     * Define el valor de la propiedad reference.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay.Reference }
                     *     
                     */
                    public void setReference(OTADynamicPkgAvailRS.SearchResults.HotelResults.RoomStays.RoomStay.Reference value) {
                        this.reference = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad serviceRPHs.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ServiceRPHsType }
                     *     
                     */
                    public ServiceRPHsType getServiceRPHs() {
                        return serviceRPHs;
                    }

                    /**
                     * Define el valor de la propiedad serviceRPHs.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ServiceRPHsType }
                     *     
                     */
                    public void setServiceRPHs(ServiceRPHsType value) {
                        this.serviceRPHs = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad isAlternate.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isIsAlternate() {
                        return isAlternate;
                    }

                    /**
                     * Define el valor de la propiedad isAlternate.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setIsAlternate(Boolean value) {
                        this.isAlternate = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad availabilityStatus.
                     * 
                     * @return
                     *     possible object is
                     *     {@link RateIndicatorType }
                     *     
                     */
                    public RateIndicatorType getAvailabilityStatus() {
                        return availabilityStatus;
                    }

                    /**
                     * Define el valor de la propiedad availabilityStatus.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link RateIndicatorType }
                     *     
                     */
                    public void setAvailabilityStatus(RateIndicatorType value) {
                        this.availabilityStatus = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad roomStayCandidateRPH.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRoomStayCandidateRPH() {
                        return roomStayCandidateRPH;
                    }

                    /**
                     * Define el valor de la propiedad roomStayCandidateRPH.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRoomStayCandidateRPH(String value) {
                        this.roomStayCandidateRPH = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad moreDataEchoToken.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getMoreDataEchoToken() {
                        return moreDataEchoToken;
                    }

                    /**
                     * Define el valor de la propiedad moreDataEchoToken.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setMoreDataEchoToken(String value) {
                        this.moreDataEchoToken = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad infoSource.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getInfoSource() {
                        return infoSource;
                    }

                    /**
                     * Define el valor de la propiedad infoSource.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setInfoSource(String value) {
                        this.infoSource = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad rph.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRPH() {
                        return rph;
                    }

                    /**
                     * Define el valor de la propiedad rph.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRPH(String value) {
                        this.rph = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad availableIndicator.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isAvailableIndicator() {
                        return availableIndicator;
                    }

                    /**
                     * Define el valor de la propiedad availableIndicator.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setAvailableIndicator(Boolean value) {
                        this.availableIndicator = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad responseType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getResponseType() {
                        return responseType;
                    }

                    /**
                     * Define el valor de la propiedad responseType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setResponseType(String value) {
                        this.responseType = value;
                    }


                    /**
                     * &lt;p&gt;Clase Java para anonymous complex type.
                     * 
                     * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * &lt;pre&gt;
                     * &amp;lt;complexType&amp;gt;
                     *   &amp;lt;complexContent&amp;gt;
                     *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}UniqueID_Type"&amp;gt;
                     *       &amp;lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&amp;gt;
                     *     &amp;lt;/extension&amp;gt;
                     *   &amp;lt;/complexContent&amp;gt;
                     * &amp;lt;/complexType&amp;gt;
                     * &lt;/pre&gt;
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Reference
                        extends UniqueIDType
                    {

                        @XmlAttribute(name = "DateTime")
                        @XmlSchemaType(name = "dateTime")
                        protected XMLGregorianCalendar dateTime;

                        /**
                         * Obtiene el valor de la propiedad dateTime.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getDateTime() {
                            return dateTime;
                        }

                        /**
                         * Define el valor de la propiedad dateTime.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setDateTime(XMLGregorianCalendar value) {
                            this.dateTime = value;
                        }

                    }

                }

            }

        }


        /**
         * &lt;p&gt;Clase Java para anonymous complex type.
         * 
         * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * &lt;pre&gt;
         * &amp;lt;complexType&amp;gt;
         *   &amp;lt;complexContent&amp;gt;
         *     &amp;lt;extension base="{http://www.opentravel.org/OTA/2003/05}DynamicPkgResponseType"&amp;gt;
         *       &amp;lt;sequence&amp;gt;
         *         &amp;lt;element name="PackageOptionGroup" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionGroupType" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
         *       &amp;lt;/sequence&amp;gt;
         *       &amp;lt;attribute name="ResponseType" type="{http://www.opentravel.org/OTA/2003/05}PackageOptionComponentSearchType" /&amp;gt;
         *     &amp;lt;/extension&amp;gt;
         *   &amp;lt;/complexContent&amp;gt;
         * &amp;lt;/complexType&amp;gt;
         * &lt;/pre&gt;
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "packageOptionGroup"
        })
        public static class PackageOptionResults
            extends DynamicPkgResponseType
        {

            @XmlElement(name = "PackageOptionGroup")
            protected List<PackageOptionGroupType> packageOptionGroup;
            @XmlAttribute(name = "ResponseType")
            protected PackageOptionComponentSearchType responseType;

            /**
             * Gets the value of the packageOptionGroup property.
             * 
             * &lt;p&gt;
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the packageOptionGroup property.
             * 
             * &lt;p&gt;
             * For example, to add a new item, do as follows:
             * &lt;pre&gt;
             *    getPackageOptionGroup().add(newItem);
             * &lt;/pre&gt;
             * 
             * 
             * &lt;p&gt;
             * Objects of the following type(s) are allowed in the list
             * {@link PackageOptionGroupType }
             * 
             * 
             */
            public List<PackageOptionGroupType> getPackageOptionGroup() {
                if (packageOptionGroup == null) {
                    packageOptionGroup = new ArrayList<PackageOptionGroupType>();
                }
                return this.packageOptionGroup;
            }

            /**
             * Obtiene el valor de la propiedad responseType.
             * 
             * @return
             *     possible object is
             *     {@link PackageOptionComponentSearchType }
             *     
             */
            public PackageOptionComponentSearchType getResponseType() {
                return responseType;
            }

            /**
             * Define el valor de la propiedad responseType.
             * 
             * @param value
             *     allowed object is
             *     {@link PackageOptionComponentSearchType }
             *     
             */
            public void setResponseType(PackageOptionComponentSearchType value) {
                this.responseType = value;
            }

        }

    }

}
