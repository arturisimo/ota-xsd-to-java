
package org.opentravel.ota._2003._05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Amentities and services requested and or available at the golf facility. Note, no pricing or policy information is included.
 * 
 * &lt;p&gt;Clase Java para GolfAmenitySummaryType complex type.
 * 
 * &lt;p&gt;El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="GolfAmenitySummaryType"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="AmenityCode" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&amp;gt;
 *       &amp;lt;attribute name="PricingType"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;enumeration value="PerPerson"/&amp;gt;
 *             &amp;lt;enumeration value="PerAmenity"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *       &amp;lt;attribute name="IncludedInTeeTimePriceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="ReservableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&amp;gt;
 *       &amp;lt;attribute name="SequentialMask"&amp;gt;
 *         &amp;lt;simpleType&amp;gt;
 *           &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *             &amp;lt;pattern value="[0-1]"/&amp;gt;
 *           &amp;lt;/restriction&amp;gt;
 *         &amp;lt;/simpleType&amp;gt;
 *       &amp;lt;/attribute&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GolfAmenitySummaryType")
@XmlSeeAlso({
    org.opentravel.ota._2003._05.TeeTimeModifyType.Amenity.class,
    org.opentravel.ota._2003._05.TeeTimeResType.Amenity.class
})
public class GolfAmenitySummaryType {

    @XmlAttribute(name = "ID")
    protected String id;
    @XmlAttribute(name = "AmenityCode")
    protected String amenityCode;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "PricingType")
    protected String pricingType;
    @XmlAttribute(name = "IncludedInTeeTimePriceInd")
    protected Boolean includedInTeeTimePriceInd;
    @XmlAttribute(name = "ReservableInd")
    protected Boolean reservableInd;
    @XmlAttribute(name = "SequentialMask")
    protected String sequentialMask;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad amenityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmenityCode() {
        return amenityCode;
    }

    /**
     * Define el valor de la propiedad amenityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmenityCode(String value) {
        this.amenityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingType() {
        return pricingType;
    }

    /**
     * Define el valor de la propiedad pricingType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingType(String value) {
        this.pricingType = value;
    }

    /**
     * Obtiene el valor de la propiedad includedInTeeTimePriceInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludedInTeeTimePriceInd() {
        return includedInTeeTimePriceInd;
    }

    /**
     * Define el valor de la propiedad includedInTeeTimePriceInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludedInTeeTimePriceInd(Boolean value) {
        this.includedInTeeTimePriceInd = value;
    }

    /**
     * Obtiene el valor de la propiedad reservableInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReservableInd() {
        return reservableInd;
    }

    /**
     * Define el valor de la propiedad reservableInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReservableInd(Boolean value) {
        this.reservableInd = value;
    }

    /**
     * Obtiene el valor de la propiedad sequentialMask.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequentialMask() {
        return sequentialMask;
    }

    /**
     * Define el valor de la propiedad sequentialMask.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequentialMask(String value) {
        this.sequentialMask = value;
    }

}
